<?php

class Contact_View_Helper_GetCallbackForm extends Zend_View_Helper_Abstract
{

    public function getCallbackForm()
    {
        $modelContact = new Contact_Model_Contact();
        $form =  new Contact_Form_Callback(array('model' => $modelContact));       
        return $form;
    }

}

