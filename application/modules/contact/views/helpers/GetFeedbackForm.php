<?php

class Contact_View_Helper_GetFeedbackForm extends Zend_View_Helper_Abstract
{

    public function getFeedbackForm()
    {
        $modelContact = new Contact_Model_Contact();
        $form =  new Contact_Form_Feedback(array('model' => $modelContact));       
        return $form;
    }

}

