<?php

/**
 * Contact_Resource_Contactsettings
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Contact_Resource_Contactsetting extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'contact_setting';
    protected $_primary = 'contact_setting_id';
    protected $_rowClass = 'Contact_Resource_Contactsetting_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getContactSettingById($id)
    {
        return $this->find($id)->current();
    }
    
    public function getContactSettingsTranslate($settings_id, $language_code)
    {
       $select = $this->select()
                 ->from($this->_name . '_translation')   
                 ->setIntegrityCheck(false)
                 ->where('language_code = ?', $language_code)
                 ->where('contact_setting_id = ?', $settings_id);
       return $this->fetchRow($select);
    }


    public function getAllContactSettingById($id)
    {
         $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.contact_setting_id = {$this->_name}_translation.contact_setting_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.contact_setting_id = ?', $id);

        return $this->fetchRow($select);
    }

    public function getContactSettingByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.contact_setting_id = {$this->_name}_translation.contact_setting_id")
                
                ->where($this->_name . '.contact_setting_id = ?', $id);

        return $this->fetchAll($select);
    }
    
     public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'contact_setting_id' => $translate_data['contact_setting_id'],
                    'language_code' => $translate_data['language_code'],
                    'city' => $translate_data['city'],
                    'address' => $translate_data['address'],
                    'work_time' => $translate_data['work_time'],   
                    'weekend' => $translate_data['weekend'],
                    'title' => $translate_data['title'],
                    'body' => $translate_data['body'],
                    'map_address' => $translate_data['map_address'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords']
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'city' => $translate_data['city'],
                    'address' => $translate_data['address'],
                    'work_time' => $translate_data['work_time'],
                    'weekend' => $translate_data['weekend'],
                    'title' => $translate_data['title'],
                    'body' => $translate_data['body'],
                    'map_address' => $translate_data['map_address'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords']
                        ), "`contact_setting_id` = {$translate_data['contact_setting_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "contact_setting_id = {$id}");
    }
    
}