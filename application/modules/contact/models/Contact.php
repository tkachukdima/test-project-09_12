<?php

class Contact_Model_Contact extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get settings
     *
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getContactSettings($id = 1)
    {
        return $this->getResource('Contactsetting')->getContactSettingById($id);
    }
    
    /**
     * Get settings
     *
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getAllContactSettingById($id = 1)
    {
        return $this->getResource('Contactsetting')->getAllContactSettingById($id);
    }

    /**
     * Get a Page by its id for edit
     *
     * @param  int $id The id
     * @return Default_Resource_Page_Item
     */
    public function getContactSettingByIdForEdit($id)
    {
        $id = (int) $id;

        $page = $this->getResource('Contactsetting')->getContactSettingByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $key => $value) {
            $data['city_' . $value['language_code']] = $value['city'];
            $data['address_' . $value['language_code']] = $value['address'];
            $data['work_time_' . $value['language_code']] = $value['work_time'];
            $data['weekend_' . $value['language_code']] = $value['weekend'];
            $data['title_' . $value['language_code']] = $value['title'];
            $data['body_' . $value['language_code']] = $value['body'];
            $data['map_address_' . $value['language_code']] = $value['map_address'];
            $data['meta_description_' . $value['language_code']] = $value['meta_description'];
            $data['meta_keywords_' . $value['language_code']] = $value['meta_keywords'];
        }

        return $data;
    }

    /**
     * Save a contact post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveContactSettings($data, $validator = null)
    {
        if (!$this->checkAcl('saveContactSetting')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $validator = $this->getForm('Setting');

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        //print_r($data);
        //die();

        $contact_setting = array_key_exists('contact_setting_id', $data) ?
                $this->getResource('Contactsetting')->getContactSettingById($data['contact_setting_id']) : null;
        //echo "<pre>";
        //print_r($data);
        //print_r($contact_setting);
        //echo "</pre>";
        //die();
        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/contact');


        if (!$contact_setting->map_image_full AND $data['map_image_full'] == '') {
            $new = $this->getResource('Contactsetting')->saveRow($data, $contact_setting);

             $this->getResource('Contactsetting')->deleteTranslatedRows($new);
                     
            foreach (Zend_Registry::get('langList') as $lang) {

                $translate_data = array(
                    'contact_setting_id' => (int) $new,
                    'language_code' => $lang->code,
                    'city' => $data['city_' . $lang->code],
                    'address' => $data['address_' . $lang->code],
                    'work_time' => $data['work_time_' . $lang->code],
                    'weekend' => $data['weekend_' . $lang->code],
                    'title' => $data['title_' . $lang->code],
                    'body' => $data['body_' . $lang->code],
                    'map_address' => $data['map_address_' . $lang->code],                    
                    'meta_description' => $data['meta_description_' . $lang->code],
                    'meta_keywords' => $data['meta_keywords_' . $lang->code],
                );                
                $this->getResource('Contactsetting')->saveTranslatedRows($translate_data, 'insert');
            }

            return $new;
        }


        if ($contact_setting AND $data['delete_image'] == 1 AND $data['map_image_full'] == '') {
            $data['map_image_full'] = '';
            $data['map_image_thumbnail'] = '';
            $data['map_image_preview'] = '';
            unlink($fileDestination . '/' . $contact_setting->map_image_full);
            unlink($fileDestination . '/' . $contact_setting->map_image_thumbnail);
            unlink($fileDestination . '/' . $contact_setting->map_image_preview);
            return $this->getResource('Contactsetting')->saveRow($data, $contact_setting);
        }

        if ($contact_setting->map_image_full != '' AND $data['map_image_full'] == '') {
            $data['map_image_full'] = $contact_setting->map_image_full;
            $data['map_image_thumbnail'] = $contact_setting->map_image_thumbnail;
            $data['map_image_preview'] = $contact_setting->map_image_preview;
        }

        $new = $this->getResource('Contactsetting')->saveRow($data, $contact_setting);


        $data['contact_setting_id'] = $new;

        $filter = new ARTCMF_Filter_ImageSize();


        $path_parts = pathinfo($data['map_image_full']);
        $new_file_name = 'map_image' . '.' . $path_parts['extension'];
        rename($fileDestination . '/' . $data['map_image_full'], $fileDestination . '/' . $new_file_name);


        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel('contact_map');


        $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                ->setHeight($image_resize_setting['height_thumbnail'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                ->filter($fileDestination . '/' . $new_file_name);

        $preview = $filter->setWidth($image_resize_setting['width_preview'])
                ->setHeight($image_resize_setting['height_preview'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_preview']())
                ->filter($fileDestination . '/' . $new_file_name);

        $full = $filter->setWidth($image_resize_setting['width_full'])
                ->setHeight($image_resize_setting['height_full'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_full']())
                ->filter($fileDestination . '/' . $new_file_name);



        $data['map_image_thumbnail'] = basename($thumbnail);
        $data['map_image_preview'] = basename($preview);
        $data['map_image_full'] = basename($full);



        $new_contact_setting = $this->getResource('Contactsetting')->getContactSettingById($new);

        $new = $this->getResource('Contactsetting')->saveRow($data, $new_contact_setting);

        $this->getResource('Contactsetting')->deleteTranslatedRows($new);
                     
        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'contact_setting_id' => (int) $new,
                'language_code' => $lang->code,
                'city' => $data['city_' . $lang->code],
                'address' => $data['address_' . $lang->code],
                'work_time' => $data['work_time_' . $lang->code],
                'weekend' => $data['weekend_' . $lang->code],
                'title' => $data['title_' . $lang->code],
                'body' => $data['body_' . $lang->code],
                'map_address' => $data['map_address_' . $lang->code],                    
                'meta_description' => $data['meta_description_' . $lang->code],
                'meta_keywords' => $data['meta_keywords_' . $lang->code],
            );                
            $this->getResource('Contactsetting')->saveTranslatedRows($translate_data, 'insert');
        }

        return $new;
    }

    /**
     * send a contact post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function sendFeedback($data)
    {
        if (!$this->checkAcl('sendFeedback')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $validator = $this->getForm('Feedback');

        if (!$validator->isValid($data)) {
            return $validator->getMessages();
        }

        $data = $validator->getValues();


        $data['date'] = date('Y-m-d H:i:s');
        
        $site_settings = Zend_Registry::get('site_settings');
        $sender = new Email_Model_Sender();

        $tplVars = array(
            'site' => $_SERVER['HTTP_HOST'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'date' => $data['date'],
            'subject' => $data['subject'],
            'message' => $data['message'],
        );
        
        $emailsTo = explode(";", $site_settings->getSetting('default', 'email_to'));
        foreach ($emailsTo as $emailTo) {
        $params = array(
            'email_from' => $site_settings->getSetting('default', 'email_from'),
            'site_name' => $site_settings->getSetting('default', 'site_name'),
            'email_to' => $emailTo,
            'reply_to' => $site_settings->getSetting('default', 'reply_to'),
            'subject' => $sender->getView()->translate(_("On the site %hostname% left a message"))
        );
        $sender->sendEmail($params, $tplVars, 'feedback_admin');
        }

        return true;
    }        
      
    
    /**
     * send a contact post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function sendCallback($data, $validator)
    {
        
        if (!$this->checkAcl('sendCallback')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }
       

        if (!$validator->isValid($data)) {
           return $validator->getMessages();
        }
       

        $data = $validator->getValues();


        $data['date'] = date('Y-m-d H:i:s');


        $site_settings = Zend_Registry::get('site_settings');
        $sender = new Email_Model_Sender();

        $tplVars = array(
            'site' => $_SERVER['HTTP_HOST'],
            'name' => $data['name'],        
            'phone' => $data['phone'],
            'date' => $data['date'],
            'subject' => $data['subject']  
        );

        $emailsTo = explode(";", $site_settings->getSetting('default', 'email_to'));
        foreach ($emailsTo as $emailTo) {
            $params = array(
                'email_from' => $site_settings->getSetting('default', 'email_from'),
                'site_name' => $site_settings->getSetting('default', 'site_name'),
                'email_to' => $emailTo,
                'reply_to' => $site_settings->getSetting('default', 'reply_to'),
                'subject' => sprintf(_("On the site %s requested a call back"), $_SERVER['HTTP_HOST'])
            );
            $sender->sendEmail($params, $tplVars, 'callback');
        }
               
        return true;
    }


    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Contact';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Guest', $this, array('sendFeedback'))
                    ->allow('Member', $this, array())
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
