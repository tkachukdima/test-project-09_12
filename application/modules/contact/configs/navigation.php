<?php

return array(
    array(
        'label' => _('Contacts'),
        'module' => 'contact',
        'controller' => 'index',
        'action' => 'settings',
        'route' => 'admin',
        'resource' => 'contact:index',
        'privilege' => 'settings',
        'reset_params' => true,
        'class' => 'dashboard-contact',
        'pages' => array(
            array(
                'label' => _('Contacts settings'),
                'module' => 'contact',
                'controller' => 'index',
                'action' => 'settings',
                'route' => 'admin',
                'resource' => 'contact:index',
                'privilege' => 'settings',
                'reset_params' => true
            )
        )
    )
);