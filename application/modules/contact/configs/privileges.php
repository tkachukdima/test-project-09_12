<?php

return array(
    'Guest' => array(
        'allow' => array(
            'index' => array(
                'index',
                'captcha',
                'callback',
                'feedback-ajax'
            )
        )
    ),
    'Member' => array(
        'allow' => array(
        )
    ),
    'Redactor' => array(
        'allow' => array(
            'index' => array(
                'list-feedback',
                'settings',
                'save-setting'
            ),
        )
    ),
    'Manager' => array(
    ),
    'Admin' => array(
    ),
);