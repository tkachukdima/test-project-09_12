<?php

/**
 * The Setting form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Contact_Form_Setting extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/contact');

        $this->setMethod('post');
        $this->setAction('');

        $this->setName('settingsForm');

        foreach (Zend_Registry::get('langList') as $key => $lang) {

            $this->addElement(/*'text'*/'hidden', 'city_' . $lang->code, array(
                'filters' => array('StringTrim'),
                'required' => false,
                //'label' => _('City'),
                'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
            ));

            $this->addElement(/*'text'*/'hidden', 'address_' . $lang->code, array(
                'filters' => array('StringTrim'),
                'required' => false,
                //'label' => _('Address'),
                'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
            ));
        }




        $this->addElement('text', 'phone_code', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'decorators' => array('ViewHelper', 'Label')
        ));

        $this->addElement('text', 'phone_number', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'decorators' => array('ViewHelper')
        ));



        $this->addElement('text', 'fax_code', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'decorators' => array('ViewHelper', 'Label')
        ));

        $this->addElement('text', 'fax_number', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'decorators' => array('ViewHelper')
        ));
        
        $this->addElement('text', 'phone2_code', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'decorators' => array('ViewHelper', 'Label')
        ));

        $this->addElement('text', 'phone2_number', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'decorators' => array('ViewHelper')
        ));


        $this->addElement('text', 'public_email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required' => true,
            'label' => _('Public email'),
        ));
        
         $this->addElement('text', 'skype', array(
            'filters' => array('StringTrim'),
            'required' => false,
             'label' => _('Skype')
        ));


        foreach (Zend_Registry::get('langList') as $key => $lang) {

            $this->addElement('text', 'work_time_' . $lang->code, array(
                'filters' => array('StringTrim'),
                'required' => false,
                'label' => _('Working time')
            ));

            $this->addElement('text', 'weekend_' . $lang->code, array(
                'filters' => array('StringTrim'),
                'required' => false,
                'label' => _('Weekend')
            ));
        }

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Title'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));

            $element_name = 'body_' . $lang->code;
            $this->addElement('textarea', $element_name, array(
                'label' => _('Body'),
                'filters' => array('StringTrim'),
                'required' => true,
                'class' => "ckeditor"
            ));
            //$this->$element_name->addDecorator(new ARTCMF_Form_Decorator_CKEditor);
            
            $element_name2 = 'map_address_' . $lang->code;
            $this->addElement(/*'textarea'*/ 'hidden', $element_name2, array(
                //'label' => _('Address'),
                'filters' => array('StringTrim'),
                'required' => false,
                'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
            ));
            //$this->$element_name2->addDecorator(new ARTCMF_Form_Decorator_CKEditor);

            $this->addElement('textarea', 'meta_description_' . $lang->code, array(
                'label' => _('META Description'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true
            ));

            $this->addElement('textarea', 'meta_keywords_' . $lang->code, array(
                'label' => _('META Keywords'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true,
            ));
        }


        foreach (Zend_Registry::get('langList') as $key => $lang) {

            $this->addElement('text', 'work_time_' . $lang->code, array(
                'filters' => array('StringTrim'),
                'required' => false,
                'label' => _('Working time'),
            ));

            $this->addElement('text', 'weekend_' . $lang->code, array(
                'filters' => array('StringTrim'),
                'required' => false,
                'label' => _('Weekend'),
            ));
        }

/*
        $this->addElement('Checkbox', 'delete_image', array(
            'label' => _('Delete image')
        ));

        $this->addElement('file', 'map_image_full', array(
            'label' => _('Map'),
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg', 'jpeg', 'png', 'gif')),
            ),
        ));
        
        $this->addElement('text', 'hreef_map', array(
                'label' => 'Абсолютная ссылка на карту',
                'filters' => array('StringTrim'),
                'required' => false,
        ));


        $this->getElement('hreef_map')->addValidator(new ARTCMF_Validate_ValidatorUrl);*/
        
        $this->addElement('submit', 'submit', array(
            'label' => _('Send'),
        ));

        $this->addElement('hidden', 'contact_setting_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'city_' . $lang->code,
                'address_' . $lang->code,
                'title_' . $lang->code,
                'body_' . $lang->code,
                'map_address_' . $lang->code,
                'meta_description_' . $lang->code,
                'meta_keywords_' . $lang->code,
                'work_time_' . $lang->code,
                'weekend_' . $lang->code
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }

        $this->addDisplayGroup(array('phone_code', 'phone_number'), 'phone', array('legend' => _('Code | Phone')));
        $this->addDisplayGroup(array('fax_code', 'fax_number'), 'fax', array('legend' => _('Code | Fax / Second Phone')));
        $this->addDisplayGroup(array('phone2_code', 'phone2_number'), 'phone2', array('legend' => _('Code | Phone 2')));
        
        //$this->addDisplayGroup(array('delete_image', 'map_image_full', 'hreef_map'
        //    ), 'map', array('legend' => _('Map')));

        $this->addDisplayGroup(array('public_email', 'skype', 'email', 
            //'delete_image', 'map_image_full', 'hreef_map', 
            'submit', 'contact_setting_id'), 'form_all', array('legend' => _('General Settings')));
    }

}
