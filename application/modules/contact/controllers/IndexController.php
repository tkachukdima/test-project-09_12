<?php

/**
 * Index Controller
 *
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Contact_IndexController extends Zend_Controller_Action
{

    protected $_forms = array();
    protected $_modelContact;

    public function init()
    {
        $this->_modelContact = new Contact_Model_Contact();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {
        $this->_helper->getHelper('layout')->setLayout('layout_contact');
        
        $pageModel = new Default_Model_Page();
        $page = $pageModel->getPageByType('contact');

        if (!is_null($page)) {
            $this->view->headTitle(strip_tags($page->page_title), 'PREPEND');
            $this->view->headMeta()->appendName('description', strip_tags($page->meta_description));
            $this->view->headMeta()->appendName('keywords', strip_tags($page->meta_keywords));
            $this->view->page = $page;
        }


        $request = $this->getRequest();

        $this->view->feedbackForm = $this->_getFeedbackForm();

        if ($request->isPost()) {
            if (false !== $this->_modelContact->sendFeedback($request->getPost())) {
                $this->view->message = $this->view->translate('Thank you! Your message is received.');
                $this->view->feedbackForm->reset();
            } else {
//                print_r( $this->view->feedbackForm->getMessages());
                $this->view->message = $this->view->translate('Error! Maybe you made a mistake when filling out form.');
            }
        }
    }
    
     public function feedbackAjaxAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not found')));
        }

        $feedbackForm = new Contact_Form_Feedback(array('model' => $this->_modelContact));

        $data = $this->_modelContact->sendFeedback($request->getPost(), $feedbackForm);
        $out = array('status' => 'error');
        if (is_array($data)) {
            $out['error_messages'] = $data;
        } else {
            $out = array('status' => 'success', 'message' => $this->view->translate('Thank you! Your message is received.'));
        }

        $this->_helper->json($out);
    }
      
    public function callbackAction()
    {
        $request = $this->getRequest();


        if (!$request->isPost()) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not found')));
        }

        $callbackForm = new Contact_Form_Callback(array('model' => $this->_modelContact));

        $data = $this->_modelContact->sendCallback($request->getPost(), $callbackForm);
        $out = array('status' => 'error');
        if (is_array($data)) {
            $out['error'] = $data;
        } else {
            $out = array('status' => 'success', 'message' => $this->view->translate('Your request has been accepted, wait for the call!'));
        }

        $this->_helper->json($out);
    }

    public function captchaAction()
    {
        if ($this->_request->isPost()) {
            $data = array();

            $feedbackForm = $this->_getFeedbackForm();

            $captcha = $feedbackForm->getElement('captcha')->getCaptcha();

            $data['id'] = $captcha->generate();
            $data['src'] = $captcha->getImgUrl() . $captcha->getId() . $captcha->getSuffix();


            $this->_helper->json($data);
        } else {
             throw new ARTCMF_Exception_404($this->view->translate(_('Page not found')));
        }
    }

    public function settingsAction()
    {
        /* $this->view->contactSetting = $this->_modelContact->getContactSettingByIdForEdit(1);

          $this->view->settingsForm = $this->_getContactSettingForm()->populate($this->view->contactSetting);

          if(!$this->view->contactSetting['map_image_full'] )
          $this->view->settingsForm->removeElement('delete_image'); */

        $_modelSettings = new Default_Model_Settings();

        $this->view->settingsForm = $_modelSettings->getFormWithPopulate('Settings', 'Contact');
    }

    public function saveSettingAction()
    {

        $request = $this->getRequest();

        if (!$request->isPost()) {
            return $this->_helper->redirector('settings');
        }

        if (false === $this->_modelContact->saveContactSettings($request->getPost())) {
            $this->view->settingsForm = $this->_getContactSettingForm();
            $this->view->settingsForm->setDescription($this->view->translate(_('Error! Maybe you made a mistake when filling out form.')));
            return $this->render('settings');
        }

        $message = $this->view->translate(_('Settings updated'));

        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'settings',
                    'controller' => 'index',
                    'module' => 'contact'), 'admin', true);
    }

    protected function _getFeedbackForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Feedback'] = $this->_modelContact->getForm('Feedback');
        $this->_forms['Feedback']->setAction($urlHelper->url(array(
                    'module' => 'contact',
                    'controller' => 'index',
                    'action' => 'index'
                        ), 'default'
                ) . '#feedback_form');
        $this->_forms['Feedback']->setMethod('post');

        return $this->_forms['Feedback'];
    }

    protected function _getContactSettingForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Setting'] = $this->_modelContact->getForm('Setting');
        $this->_forms['Setting']->setAction($urlHelper->url(array(
                    'module' => 'contact',
                    'controller' => 'index',
                    'action' => 'save-setting'
                        ), 'admin', true
        ));
        $this->_forms['Setting']->setMethod('post');

        return $this->_forms['Setting'];
    }

}