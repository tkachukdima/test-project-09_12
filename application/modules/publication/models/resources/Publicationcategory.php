<?php

/**
 * Publication_Resource_Publicationcategory
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Resource_Publicationcategory extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'publication_category';
    protected $_primary = 'publication_category_id';
    protected $_rowClass = 'Publication_Resource_Publicationcategory_Item';
    protected $_lang;
    protected $_referenceMap = array(
        'SubCategoryPublication' => array(
            'columns' => 'parent_id',
            'refTableClass' => 'Publication_Resource_Publicationcategory',
            'refColumns' => 'publication_category_id',
        )
    );

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getPublicationCategoryByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id")
                ->where($this->_name . '.publication_category_id = ?', $id);

        return $this->fetchAll($select);
    }

    public function getPublicationCategoryById($id, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id ")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.publication_category_id = ?', $id)
                ->order($this->_name . '_translation.name');

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchRow($select);


        //return $this->find($id)->current();
    }

    public function getPublicationCategoriesByParentId($parent_id, $group_id, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.publication_group_id = ?', $group_id)
                ->where($this->_name . '.parent_id = ?', $parent_id)
                ->order($this->_name . '.sort_order');

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchAll($select);
    }

    public function getPublicationCategories($status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id ")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '_translation.name');

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchAll($select);
    }

    public function getPublicationCategoriesByGroup($groupId, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id ")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.publication_group_id = ?', (int) $groupId)
                ->order($this->_name . '_translation.name');

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchAll($select)->toArray();
    }

    public function getPublicationCategoriesByGroupId($group_id, $order, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id ")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.publication_group_id = ?', (int) $group_id)
                ->order($this->_name . '_translation.name');

        if (null !== $order)
            $select->order($order);
        else
            $select->order($this->_name . '_translation.name');

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchAll($select);
    }

    public function getPublicationCategoryByIdent($ident, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id ")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.ident = ?', $ident)
                ->order($this->_name . '_translation.name');

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchRow($select);
    }

    public function getCategoriesForTree($parent_id = null, $group_id = null, $status = null)
    {

        // var_dump($group_id); die;
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order(array($this->_name . '.parent_id', $this->_name . '.sort_order', $this->_name . '.publication_category_id'));

        if (null !== $parent_id) {
            $select->where($this->_name . '.parent_id = ?', $parent_id);
        }

        if (null !== $group_id) {
            $select->where($this->_name . '.publication_group_id  = ?', $group_id);
        }

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchAll($select);
    }

    public function getCategoryPublicationByIdent($ident, $ignoreRow = null, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id  = {$this->_name}_translation.publication_category_id ")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.ident = ?', $ident);
        if (null !== $ignoreRow) {
            $select->where($this->_name . '.ident < ?', $ignoreRow->ident);
        }

        if (!is_null($status))
            $select->where($this->_name . '.status = ?', $status);

        return $this->fetchRow($select);
    }
    
    public function getSearchData($lang)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_category_id = {$this->_name}_translation.publication_category_id ")
                ->joinLeft('publication_group', $this->_name . '.publication_group_id = publication_group.publication_group_id', array('publication_group_ident' => 'publication_group.ident'))
                ->where($this->_name . '_translation.language_code = ?', $lang)                
                ->where($this->_name . '.status = ?', 1);

        return $this->fetchAll($select);
    }

    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'publication_category_id' => $translate_data['publication_category_id'],
                    'language_code' => $translate_data['language_code'],
                    'name' => $translate_data['name'],
                    'description' => $translate_data['description'],
                    'page_title' => $translate_data['page_title'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords'],
                    'description_img' => $translate_data['description_img']
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'name' => $translate_data['name'],
                    'description' => $translate_data['description'],
                    'page_title' => $translate_data['page_title'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords'],
                    'description_img' => $translate_data['description_img']
                        ), "`publication_category_id` = {$translate_data['publication_category_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function getPublicationCategoryByIdForSave($id)
    {
        return $this->find($id)->current();
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->query("DELETE FROM `{$this->_name}_translation`
                    WHERE `publication_category_id` = {$id}");
    }

}
