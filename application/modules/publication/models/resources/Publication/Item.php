<?php

/**
 * Publication_Resource_Publication_Item
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Resource_Publication_Item extends ARTCMF_Model_Resource_Db_Table_Row_Abstract
{
	protected $_lang;

	public function __construct($config = array()){

		parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getAdditionalImages()
    {
        $select = $this->select()->from('storage_images')
                ->setIntegrityCheck(false)
                ->joinLeft('storage_images_translation', "storage_images.image_id = storage_images_translation.image_id")
				->joinLeft('storage_images_relations', "storage_images_translation.image_id = storage_images_relations.image_id");
        

        $select->where('storage_images_relations.id = ?', $this->getRow()->publication_id);
        $select->where('storage_images_relations.`key` = ?',  'publication');

        $select->where('storage_images_relations.`main` = 0');

        $select->where('storage_images_translation.language_code = ?', $this->_lang);
        $this->getTable()->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
        return $this->getTable()->getAdapter()->fetchAll($select);
    }

   	/*public function getMainImages()
    {
        $select = $this->select()->from('storage_images')
                ->setIntegrityCheck(false)
                ->joinLeft('storage_images_translation', "storage_images.image_id = storage_images_translation.image_id")
				->joinLeft('storage_images_relations', "storage_images_translation.image_id = storage_images_relations.image_id");
        

        $select->where('storage_images_relations.id = ?', $this->getRow()->publication_id);
        $select->where('storage_images_relations.`key` = ?',  'publication');

        $select->where('storage_images.`main` = 1');
        
        $select->where('storage_images_translation.language_code = ?', $this->_lang);

        return $this->getTable()->getAdapter()->fetchRow($select);
    }*/

}
