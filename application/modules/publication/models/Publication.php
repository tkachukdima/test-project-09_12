<?php

class Publication_Model_Publication extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get all categories
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getPublicationGroups()
    {
        return $this->getResource('Publicationgroup')->getPublicationGroups();
    }

    public function getPublicationGroupsInMenu($in_menu = 1)
    {
        return $this->getResource('Publicationgroup')->getPublicationGroupsInMenu($in_menu);
    }

    public function getPublicationCategory($id, $status = null)
    {
        return $this->getResource('Publicationcategory')->getPublicationCategoryById($id, $status);
    }

    public function getPublicationCategoryById($id, $status = null)
    {
        return $this->getResource('Publicationcategory')->getPublicationCategoryById($id, $status);
    }

    public function getPublicationCategories($status = null)
    {
        return $this->getResource('Publicationcategory')->getPublicationCategories($status);
    }

    public function getRandomPublicationImage($limit = NULL, $publicationGrourIdent = NULL)
    {
        return $this->getResource('Publication')->getRandomPublicationImage($limit, $publicationGrourIdent);
    }

    public function getPublicationCategoriesForSelect($parent_id, $group_id, $level = 0)
    {
        $level++;

        $data = array();
        $results = $this->getPublicationCategoriesByParentId($parent_id, $group_id)->toArray();

        foreach ($results as $result) {

            $data[] = array(
                'label' => $result['publication_category_id'],
                'name' => str_repeat(' - ', $level) . $result['name']
            );

            $children = $this->getPublicationCategoriesForSelect($result['publication_category_id'], $group_id, $level);

            if ($children) {
                $data = array_merge($data, $children);
            }
        }
        return $data;
    }

    public function getPublicationCategoriesByGroup($groupId, $status = null)
    {
        return $this->getResource('Publicationcategory')->getPublicationCategoriesByGroup($groupId, $status);
    }

    public function getPublicationCategoriesByGroupId($group_id, $order = null, $status = null)
    {
        return $this->getResource('Publicationcategory')->getPublicationCategoriesByGroupId($group_id, $order, $status);
    }

    public function getPublicationCategoriesByParentId($parentId, $group_id, $status = null)
    {
        $parentId = (int) $parentId;
        $group_id = (int) $group_id;

        return $this->getResource('Publicationcategory')->getPublicationCategoriesByParentId($parentId, $group_id, $status);
    }

    /**
     * Get all categories tree for admin panel
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getCategoriesTreeAdm($group_id = null, $status = null)
    {
        $row_set = $this->getResource('Publicationcategory')->getCategoriesForTree(null, $group_id, $status)->toArray();

        $tree = array(0 => array('publication_category_id' => 0, 'parent_id' => 0, 'value' => 'root'));
        $temp = array(0 => &$tree[0]);
        foreach ($row_set as $val) {
            $parent = &$temp[$val['parent_id']];

            if (!isset($parent['pages'])) {
                $parent['pages'] = array();
            }
            $parent['pages'][$val['publication_category_id']]['publication_category_id'] = $val['publication_category_id'];
            $parent['pages'][$val['publication_category_id']]['id'] = 'category-' . $val['publication_category_id'];
            $parent['pages'][$val['publication_category_id']]['parent_id'] = $val['parent_id'];
            $parent['pages'][$val['publication_category_id']]['label'] = $val['name'];
            $parent['pages'][$val['publication_category_id']]['title'] = $val['name'];
            $parent['pages'][$val['publication_category_id']]['module'] = 'publication';
            $parent['pages'][$val['publication_category_id']]['controller'] = 'management';
            $parent['pages'][$val['publication_category_id']]['action'] = 'list';
            $parent['pages'][$val['publication_category_id']]['resource'] = 'publication:management';
            $parent['pages'][$val['publication_category_id']]['privilege'] = 'list';
            $parent['pages'][$val['publication_category_id']]['route'] = 'admin';
            $parent['pages'][$val['publication_category_id']]['params'] = array(
                'group_id' => $val['publication_group_id'],
                'category_id' => $val['publication_category_id'],
            );

            if (isset($val['pages']))
                $parent['pages'][$val['publication_category_id']]['pages'] = $val['pages'];


            $temp[$val['publication_category_id']] = &$parent['pages'][$val['publication_category_id']];
        }
        unset($row_set, $temp, $val, $parent);
        //print_r($tree[0]['pages']);exit;
        if (!isset($tree[0]['pages']))
            return null;

        return $tree[0]['pages'];
    }

    /**
     * Get all categories
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getPublicationGroupsForMenu()
    {
        $row_set = $this->getResource('Publicationgroup')->getPublicationGroups();

        $list = array();
        foreach ($row_set as $value) {
            $list[$value->publication_group_id]['publication_group_id'] = $value->publication_group_id;
            $list[$value->publication_group_id]['label'] = $value->name;
            $list[$value->publication_group_id]['title'] = $value->name;
            $list[$value->publication_group_id]['module'] = 'publication';
            $list[$value->publication_group_id]['controller'] = 'management';
            $list[$value->publication_group_id]['action'] = 'list';
            $list[$value->publication_group_id]['resource'] = 'publication:management';
            $list[$value->publication_group_id]['privilege'] = 'list';
      //      $list[$value->publication_group_id]['route'] = 'admin';      
            $list[$value->publication_group_id]['params'] = array('group_id' => $value->publication_group_id);
        }

        return $list;
    }

    public function getPublicationCategoryForMenu()
    {
        $row_set = $this->getResource('Publicationgroup')->getPublicationCategory();

        $list = array();
        foreach ($row_set as $value) {
            $list[$value->publication_group_id]['publication_group_id'] = $value->publication_group_id;
            $list[$value->publication_group_id]['label'] = $value->name;
            $list[$value->publication_group_id]['title'] = $value->name;
            $list[$value->publication_group_id]['module'] = 'publication';
            $list[$value->publication_group_id]['controller'] = 'management';
            $list[$value->publication_group_id]['action'] = 'list';
            $list[$value->publication_group_id]['resource'] = 'publication:management';
            $list[$value->publication_group_id]['privilege'] = 'list';
            $list[$value->publication_group_id]['route'] = 'admin';
            $list[$value->publication_group_id]['params'] = array('group_id' => $value->publication_group_id);
        }

        return $list;
    }

    /**
     * Get the category item by its id
     *
     * @param int $id
     * @return Default_Resource_Category_Item|null
     */
    public function getPublicationGroupById($id)
    {
        return $this->getResource('Publicationgroup')->getPublicationGroupById($id);
    }

    /**
     * Get a PublicationGroup by its id for edit
     *
     * @param  int $id The id
     * @return Default_Resource_Publicationgroup_Item
     */
    public function getPublicationGroupByIdForEdit($id)
    {
        $id = (int) $id;

        $page = $this->getResource('Publicationgroup')->getPublicationGroupByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['name_' . $value['language_code']] = $value['name'];
            $data['description_' . $value['language_code']] = $value['description'];
            $data['page_title_' . $value['language_code']] = $value['page_title'];
            $data['meta_description_' . $value['language_code']] = $value['meta_description'];
            $data['meta_keywords_' . $value['language_code']] = $value['meta_keywords'];
        }

        return $data;
    }

    public function getPublicationCategoryByIdForEdit($id)
    {

        $id = (int) $id;

        $page = $this->getResource('Publicationcategory')->getPublicationCategoryByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['name_' . $value['language_code']] = $value['name'];
            $data['description_' . $value['language_code']] = $value['description'];
            $data['page_title_' . $value['language_code']] = $value['page_title'];
            $data['meta_description_' . $value['language_code']] = $value['meta_description'];
            $data['meta_keywords_' . $value['language_code']] = $value['meta_keywords'];
            $data['description_img_' . $value['language_code']] = $value['description_img'];
        }

        return $data;
    }

    /**
     * Get Category yService by ident
     *
     * @param string $ident The ident string
     * @return Default_Resource_Category_Item|null
     */
    public function getPublicationGroupByIdent($ident, $ignoreRow = null)
    {
        return $this->getResource('Publicationgroup')->getPublicationGroupByIdent($ident, $ignoreRow);
    }

    public function getCountPublicationsByGroupIdent($publication_group_ident = null, $status = null)
    {
        return $this->getResource('Publication')->getCountPublicationsByGroupIdent($publication_group_ident, $status)->num;
    }

    public function getPublicationCategoryByIdent($ident, $status = null)
    {
        return $this->getResource('Publicationcategory')->getPublicationCategoryByIdent($ident, $status);
    }

    /**
     * Save a category
     * 
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function savePublicationGroup($data, $validator = null)
    {
        if (!$this->checkAcl('savePublicationGroup')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('Group' . ucfirst($validator));

        $langList = Zend_Registry::get('langList');

        if ($data['ident'] == '') {
            $filter = new ARTCMF_Filter_Ident();
            $data['ident'] = $filter->filter($data['name_' . $langList[0]->code]);
        }

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $data['last_edit_id'] = $this->getIdentity()->user_id;

        $category = array_key_exists('publication_group_id', $data) ?
                $this->getResource('Publicationgroup')->getPublicationGroupByIdForSave($data['publication_group_id']) : null;

        if(null == $category){
            $data['author_id'] = $this->getIdentity()->user_id;
        }

        $new = $this->getResource('Publicationgroup')->saveRow($data, $category);
        
        $this->getResource('Publicationgroup')->deleteTranslatedRows($new);
            
        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'publication_group_id' => (int) $new,
                'language_code' => $lang->code,
                'name' => $data['name_' . $lang->code],
                'description' => $data['description_' . $lang->code],
                'page_title' => '' != $data['page_title_' . $lang->code] ? $data['page_title_' . $lang->code] : $data['name_' . $lang->code],
                'meta_description' => $data['meta_description_' . $lang->code],
                'meta_keywords' => $data['meta_keywords_' . $lang->code],
            );

            $this->getResource('Publicationgroup')->saveTranslatedRows($translate_data);
        }

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publication_groups')
        );

        return $new;
    }

    /*
     * begin publication category editor timur
     */

    /*
    public function savePublicationCategory($data, $validator = null)
    {
        if (!$this->checkAcl('savePublicatioCategory')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }


        $validator = $this->getForm('Category' . ucfirst($validator));


        $lang = Zend_Registry::get('langList');

        if ($data['ident'] == '') {
            $filter = new ARTCMF_Filter_Ident();
            $data['ident'] = $filter->filter($data['name_' . $lang[0]->code]);
        }

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();



        $publication_category = array_key_exists('publication_category_id', $data) ?
                $this->getResource('Publicationcategory')->getPublicationCategoryById($data['publication_category_id']) : null;


        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication/category');

        if ($data['delete_image'] == 1 AND !is_null($publication_category)) {
            @unlink($fileDestination . '/' . $publication_category->full);
            @unlink($fileDestination . '/' . $publication_category->thumbnail);
            @unlink($fileDestination . '/' . $publication_category->preview);
            $new = $this->getResource('Publicationcategory')->saveRow(array(
                'full' => '',
                'thumbnail' => '',
                'preview' => '',
                    ), $publication_category);
        }

        if ($data['full'] == '') {
            if (!is_null($publication_category)) {
                if ('' != $publication_category->full) {
                    $data['full'] = $publication_category->full;
                    $data['thumbnail'] = $publication_category->thumbnail;
                    $data['preview'] = $publication_category->preview;
                }
            }

            $new = $this->getResource('Publicationcategory')->saveRow($data, $publication_category);
        } else {

            if (!is_null($publication_category)) {
                if ('' != $publication_category->full) {
                    @unlink($fileDestination . '/' . $publication_category->full);
                    @unlink($fileDestination . '/' . $publication_category->thumbnail);
                    @unlink($fileDestination . '/' . $publication_category->preview);
                }
            }

            $new = $this->getResource('Publicationcategory')->saveRow($data, $publication_category);
            
            $filter = new ARTCMF_Filter_ImageSize();

            $path_parts = pathinfo($data['full']);
            $new_file_name = $publication_category->ident . '-' . $publication_category->publication_category_id . '.' . $path_parts['extension'];


            rename($fileDestination . '/' . $data['full'], $fileDestination . '/' . $new_file_name);


            $settingModel = new Default_Model_Settings();
            $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication_category');


            $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                    ->setHeight($image_resize_setting['height_thumbnail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $preview = $filter->setWidth($image_resize_setting['width_preview'])
                    ->setHeight($image_resize_setting['height_preview'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_preview']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $full = $filter->setWidth($image_resize_setting['width_full'])
                    ->setHeight($image_resize_setting['height_full'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_full']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $watermark = new ARTCMF_Filter_Watermark();
            $img = imagecreatefromjpeg($full);
            $settings = Zend_Registry::get('site_settings');
            $im = $watermark->create_watermark($img, $settings->getSetting('default', 'watermark'), APPLICATION_PATH . "/../www/fonts/arial.ttf", 128, 128, 128, 64);
            imagejpeg($im, $full, 100);


            $data['thumbnail'] = basename($thumbnail);
            $data['preview'] = basename($preview);
            $data['full'] = basename($full);


            $new = $this->getResource('Publicationcategory')->saveRow($data, $publication_category);
        }

        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'publication_category_id' => (int) $new,
                'language_code' => $lang->code,
                'name' => $data['name_' . $lang->code],
                'description' => $data['description_' . $lang->code],
                'page_title' => $data['page_title_' . $lang->code] ? $data['page_title_' . $lang->code] : $data['title_' . $lang->code],
                'meta_description' => $data['meta_description_' . $lang->code],
                'meta_keywords' => $data['meta_keywords_' . $lang->code],
                'description_img' => $data['description_img_' . $lang->code]
            );


            if (is_null($publication_category))
                $method = 'insert';
            else
                $method = 'update';
            $this->getResource('Publicationcategory')->saveTranslatedRows($translate_data, $method);
        }

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publication_categories')
        );

        return $new;
    }*/

    public function savePublicationCategory($data, $validator = null)
    {
        if (!$this->checkAcl('savePublicatioCategory')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }


        $validator = $this->getForm('Category' . ucfirst($validator));


        $lang = Zend_Registry::get('langList');

        if ($data['ident'] == '') {
            $filter = new ARTCMF_Filter_Ident();
            $data['ident'] = $filter->filter($data['name_' . $lang[0]->code]);
        }

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $data['last_edit_id'] = $this->getIdentity()->user_id;

        $publication_category = array_key_exists('publication_category_id', $data) ?
                $this->getResource('Publicationcategory')->getPublicationCategoryByIdForSave($data['publication_category_id']) : null;

        if(null == $publication_category){
            $data['author_id'] = $this->getIdentity()->user_id;
        }

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication/category');

        if ($data['delete_image'] == 1 AND !is_null($publication_category)) {
            @unlink($fileDestination . '/' . $publication_category->full);
            @unlink($fileDestination . '/' . $publication_category->thumbnail);
            @unlink($fileDestination . '/' . $publication_category->preview);
            $new = $this->getResource('Publicationcategory')->saveRow(array(
                'full' => '',
                'thumbnail' => '',
                'preview' => '',
                    ), $publication_category);
        }

        if ($data['full'] == '') {
            if (!is_null($publication_category)) {
                if ('' != $publication_category->full) {
                    $data['full'] = $publication_category->full;
                    $data['thumbnail'] = $publication_category->thumbnail;
                    $data['preview'] = $publication_category->preview;
                }
            }

            $new = $this->getResource('Publicationcategory')->saveRow($data, $publication_category);
        } else {

            if (!is_null($publication_category)) {
                if ('' != $publication_category->full) {
                    @unlink($fileDestination . '/' . $publication_category->full);
                    @unlink($fileDestination . '/' . $publication_category->thumbnail);
                    @unlink($fileDestination . '/' . $publication_category->preview);
                }
            }

            $new = $this->getResource('Publicationcategory')->saveRow($data, $publication_category);

            $publication_category_img = $this->getResource('Publicationcategory')->getPublicationCategoryByIdForSave($new);
            
            $filter = new ARTCMF_Filter_ImageSize();

            $path_parts = pathinfo($data['full']);
            $new_file_name = $publication_category->ident . '-' . $publication_category->publication_category_id . '.' . $path_parts['extension'];

            rename($fileDestination . '/' . $data['full'], $fileDestination . '/' . $new_file_name);


            $settingModel = new Default_Model_Settings();
            $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication_category');


            $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                    ->setHeight($image_resize_setting['height_thumbnail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $preview = $filter->setWidth($image_resize_setting['width_preview'])
                    ->setHeight($image_resize_setting['height_preview'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_preview']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $full = $filter->setWidth($image_resize_setting['width_full'])
                    ->setHeight($image_resize_setting['height_full'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_full']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $watermark = new ARTCMF_Filter_Watermark();
            $img = imagecreatefromjpeg($full);
            $settings = Zend_Registry::get('site_settings');
            $im = $watermark->create_watermark($img, $settings->getSetting('default', 'watermark'), APPLICATION_PATH . "/../www/fonts/arial.ttf", 128, 128, 128, 64);
            imagejpeg($im, $full, 100);


            $data['thumbnail'] = basename($thumbnail);
            $data['preview'] = basename($preview);
            $data['full'] = basename($full);

            $new = $this->getResource('Publicationcategory')->saveRow($data, $publication_category_img);//$publication_category);
        }

        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'publication_category_id' => (int) $new,
                'language_code' => $lang->code,
                'name' => $data['name_' . $lang->code],
                'description' => $data['description_' . $lang->code],
                'page_title' => $data['page_title_' . $lang->code] ? $data['page_title_' . $lang->code] : $data['title_' . $lang->code],
                'meta_description' => $data['meta_description_' . $lang->code],
                'meta_keywords' => $data['meta_keywords_' . $lang->code],
                'description_img' => $data['description_img_' . $lang->code]
            );


            if (is_null($publication_category))
                $method = 'insert';
            else
                $method = 'update';
            $this->getResource('Publicationcategory')->saveTranslatedRows($translate_data, $method);
        }

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publication_categories')
        );

        return $new;
    }

    public function getCategoryPublicationByIdent($ident, $ignoreRow = null, $status = null)
    {
        return $this->getResource('Publicationcategory')->getCategoryPublicationByIdent($ident, $ignoreRow, $status);
    }

    /*
     * end publication category editor timur
     */

    public function deletePublicationGroup($publication_group)
    {
        if (!$this->checkAcl('deletePublicationGroup')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if ($publication_group instanceof Default_Resource_Publicationgroup_Item) {
            $publication_group_id = (int) $publication_group->publication_group_id;
        } else {
            $publication_group_id = (int) $publication_group;
        }

        $publication_group = $this->getResource('Publicationgroup')->getPublicationGroupByIdForSave($publication_group_id);

        if (null !== $publication_group) {

            $this->getResource('Publicationgroup')->deleteTranslatedRows($publication_group_id);
            $publication_group->delete();
            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publication_groups')
            );
            return true;
        }

        return false;
    }

    public function deletePublicationCategory($publication_category)
    {
        if (!$this->checkAcl('deletePublicationCategory')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if ($publication_category instanceof Default_Resource_Publicationcategory_Item) {
            $publication_category_id = (int) $publication_category->publication_category_id;
        } else {
            $publication_category_id = (int) $publication_category;
        }

        $publication_category = $this->getPublicationCategoryById($publication_category_id);

        if (null !== $publication_category) {

            $this->getResource('Publicationcategory')->deleteTranslatedRows($publication_category_id);
            $publication_category->delete();
            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publication_categories')
            );
            return true;
        }

        return false;
    }

    /**
     * Get a publication post by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Publication_Item
     */
    public function getPublicationById($id, $with_translate = false)
    {
        $id = (int) $id;
        return $this->getResource('Publication')->getPublicationById($id, $with_translate);
    }

    /**
     * Get a Publication by its id for edit
     *
     * @param  int $id The id
     * @return Default_Resource_Publication_Item
     */
    public function getPublicationByIdForEdit($id)
    {
        $id = (int) $id;

        $page = $this->getResource('Publication')->getPublicationByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['title_' . $value['language_code']] = $value['title'];
            $data['body_' . $value['language_code']] = $value['body'];
            $data['page_title_' . $value['language_code']] = $value['page_title'];
            $data['meta_description_' . $value['language_code']] = $value['meta_description'];
            $data['meta_keywords_' . $value['language_code']] = $value['meta_keywords'];
            $data['description_img_' . $value['language_code']] = $value['description_img'];
        }

        return $data;
    }

    /**
     * Get publication post by ident
     *
     * @param string $ident The ident string
     * @return Default_Resource_Publication_Item|null
     */
    public function getPublicationByIdent($ident, $ignoreRow = null)
    {
        return $this->getResource('Publication')->getPublicationByIdent($ident, $ignoreRow);
    }

    /**
     * Get publication
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getPublications($paged = false, $limit = null, $per_page = 10, $status = null)
    {
        return $this->getResource('Publication')->getPublications($paged, $limit, $per_page, $status);
    }

    /**
     * Get Publications By Groupe
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getPublicationsByGroupId($paged = null, $limit = null, $per_page = 10, $publication_group_id = null, $status = null)
    {
        return $this->getResource('Publication')->getPublicationsByGroupId($paged, $limit, $per_page, $publication_group_id, $status);
    }

    /**
     * Get Publications By Groupe Name
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getPublicationsByGroupIdent($paged = null, $limit = null, $per_page = 10, $publication_group_ident = null, $rand = false, $status = null, $order = null)
    {
        //die("=".$status);
        return $this->getResource('Publication')->getPublicationsByGroupIdent($paged, $limit, $per_page, $publication_group_ident, $rand, $status, $order);
    }
    
    public function getPublicationsForMenu($publication_group_ident)
    {
        //die("=".$status);
        
        $publications = $this->getResource('Publication')->getPublicationsByGroupIdent(null, null, 10, $publication_group_ident, false, null, null);
       //   print_r($publications);die();
        if (!count($publications->count()))
            return false;

        //print_r($products->toArray()); 

        $pages = array();

        foreach ($publications as $publication) {
            $pages[$publication->publication_id]['label'] = $publication->title;
            $pages[$publication->publication_id]['title'] = $publication->title;
            $pages[$publication->publication_id]['module'] = 'publication';
            $pages[$publication->publication_id]['controller'] = 'index';
            $pages[$publication->publication_id]['action'] = 'view';
            $pages[$publication->publication_id]['route'] = 'default_publication_detail';
            $pages[$publication->publication_id]['params'] = array('groupIdent' => $publication->publication_group_ident,'publicationIdent' => $publication->ident);
        }

        return $pages;
    }
    
    /**
     * Get Publications By Groupe Name
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getLastPublicationsByGroupIdent($publication_group_ident = null, $limit = null, $rand = false, $status = null, $order = null)
    {
        if(is_null($limit)) {
            $group = $this->getPublicationGroupByIdent($publication_group_ident);
            $limit = (int) $group->count_on_main;
        }
                
        return $this->getResource('Publication')->getLastPublicationsByGroupIdent($publication_group_ident, $limit, $rand, $status, $order);
    }
    
    
    public function getCountLastPublicationsByGroupIdent($publication_group_ident = null, $status = null, $period = null)
    {
         if(is_null($period)) {
            $group = $this->getPublicationGroupByIdent($publication_group_ident);
            $period = (int) $group->period_newest;
        }
        
        return $this->getResource('Publication')->getCountLastPublicationsByGroupIdent($publication_group_ident, $status, $period)->num;
    }

    /**
     * Get Publications By Category
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getPublicationsByCategoryId($paged = null, $limit = null, $per_page = 10, $publication_category_id = null)
    {
        return $this->getResource('Publication')->getPublicationsByCategoryId($paged, $limit, $per_page, $publication_category_id);
    }

    /**
     * Get Publications By Category Name
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getPublicationsByCategoryIdent($paged = null, $limit = null, $per_page = 10, $publication_category_ident = null, $rand = false)
    {
        return $this->getResource('Publication')->getPublicationsByCategoryIdent($paged, $limit, $per_page, $publication_category_ident, $rand);
    }

    /**
     * Save a publication post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function savePublication($data, $validator = null)
    {

        if (!$this->checkAcl('savePublication')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }


        $validator = $this->getForm(ucfirst($validator));

        $langList = Zend_Registry::get('langList');

        if ($data['ident'] == '') {
            $filter = new ARTCMF_Filter_Ident();
            $data['ident'] = $filter->filter($data['title_' . $langList[0]->code]);
        }


        if (!$validator->isValid($data)) {
            return false;
        }


        $data = $validator->getValues();

        $data['last_edit_id'] = $this->getIdentity()->user_id;

        $publication = array_key_exists('publication_id', $data) ?
                $this->getResource('Publication')->getPublicationById($data['publication_id']) : null;

        if ('' == $data['sort_order']) {
            $lastPublication = $this->getResource('Publication')->getLastPublication();
            $data['sort_order'] = $lastPublication->maxId + 1;
        }
        if(null == $publication){
            $data['author_id'] = $this->getIdentity()->user_id;
        }

        //$fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication');

        $newId = $this->getResource('Publication')->saveRow($data, $publication);

        $modelStorage = new Storage_Model_Image();
        $modelStorage->saveImageRelations(explode(':',$data['images_id']), 'publication', $newId);
/*
        if ($data['delete_image'] == 1 AND !is_null($publication)) {
            unlink($fileDestination . '/' . $publication->full);
            unlink($fileDestination . '/' . $publication->thumbnail);
            unlink($fileDestination . '/' . $publication->preview);
            unlink($fileDestination . '/' . $publication->detail);
            $newId = $this->getResource('Publication')->saveRow(array(
                'full' => '',
                'thumbnail' => '',
                'preview' => '',
                'detail' => ''
                    ), $publication);
        }
*/
      /*  if ($data['full'] == '') {
            if (!is_null($publication)) {
                if ('' != $publication->full) {
                    $data['full'] = $publication->full;
                    $data['thumbnail'] = $publication->thumbnail;
                    $data['preview'] = $publication->preview;
                    $data['detail'] = $publication->detail;
                }
            }

            $newId = $this->getResource('Publication')->saveRow($data, $publication);
        } else {

            if (!is_null($publication)) {
                if ('' != $publication->full) {
                    unlink($fileDestination . '/' . $publication->full);
                    unlink($fileDestination . '/' . $publication->thumbnail);
                    unlink($fileDestination . '/' . $publication->preview);
                    unlink($fileDestination . '/' . $publication->detail);
                }
            }



            $newId = $this->getResource('Publication')->saveRow($data, $publication);

            $new_publication = $this->getResource('Publication')->getPublicationById($newId);



            $filter = new ARTCMF_Filter_ImageSize();

            $path_parts = pathinfo($data['full']);
            $new_file_name = $new_publication->ident . '-' . $new_publication->publication_id . '-' . $newId . '.' . $path_parts['extension'];


            rename($fileDestination . '/' . $data['full'], $fileDestination . '/' . $new_file_name);

            $publicationGroup = $this->getPublicationGroupById($data['publication_group_id']);

            $settingModel = new Default_Model_Settings();
            $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication_' . $publicationGroup->ident);

            if (!$image_resize_setting)
                $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication');

            $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                    ->setHeight($image_resize_setting['height_thumbnail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $preview = $filter->setWidth($image_resize_setting['width_preview'])
                    ->setHeight($image_resize_setting['height_preview'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_preview']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $full = $filter->setWidth($image_resize_setting['width_full'])
                    ->setHeight($image_resize_setting['height_full'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_full']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $detail = $filter->setWidth($image_resize_setting['width_detail'])
                    ->setHeight($image_resize_setting['height_detail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_detail']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $settings = Zend_Registry::get('site_settings');
            if('' != $settings->getSetting('default', 'watermark')){
                $watermark = new ARTCMF_Filter_Watermark();
                $img = imagecreatefromjpeg($full);
                $im = $watermark->create_watermark($img, $settings->getSetting('default', 'watermark'), APPLICATION_PATH . "/../www/fonts/arial.ttf", 128, 128, 128, 64);
                imagejpeg($im, $full, 100);
            }


            $data['thumbnail'] = basename($thumbnail);
            $data['preview'] = basename($preview);
            $data['full'] = basename($full);
            $data['detail'] = basename($detail);

            $newId = $this->getResource('Publication')->saveRow($data, $new_publication);
        }*/

        $this->getResource('Publication')->deleteTranslatedRows($newId);
        
        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'publication_id' => (int) $newId,
                'language_code' => $lang->code,
                'title' => $data['title_' . $lang->code],
                'body' => $data['body_' . $lang->code],
                'page_title' => '' != $data['page_title_' . $lang->code] ? $data['page_title_' . $lang->code] : $data['title_' . $lang->code],
                'meta_description' => '' != $data['meta_description_' . $lang->code] ? $data['meta_description_' . $lang->code] : $data['title_' . $lang->code],
                'meta_keywords' => '' != $data['meta_keywords_' . $lang->code] ? $data['meta_keywords_' . $lang->code] : $data['title_' . $lang->code],
                'description_img' => '' != $data['description_img_' . $lang->code] ? $data['description_img_' . $lang->code] : $data['title_' . $lang->code],
            );

            $this->getResource('Publication')->saveTranslatedRows($translate_data, 'insert');
        }


        /* if (is_null($publication))
          {
          $publicationGroup = $this->getPublicationGroupById($data['publication_group_id']);

          $site_settings = Zend_Registry::get('site_settings');
          $sender = new Email_Model_Sender();

          // витянути всіх юзерів які підписані на публікацію
          $users = new User_Model_User();
          $subsUsers = $users->getUserBySubScription((int)1);

          $params = array(
          'email_from' => $site_settings->getSetting('default', 'email_from'),
          'site_name' => $site_settings->getSetting('default', 'site_name'),
          'reply_to' => $site_settings->getSetting('default', 'reply_to'),
          'subject' => "Рассылка сайта {$_SERVER['HTTP_HOST']}."
          );

          $tplVars = array(
          'site' => $_SERVER['HTTP_HOST'],
          'data' => $data,
          'group' => $publicationGroup->ident
          );

          // в циклі їм розіслати пісьма
          foreach ($subsUsers as $key => $user)
          {
          $params['email_to'] = $user->email;
          $tplVars['firstname'] = $user->firstname;
          $tplVars['firstname'] = $user->firstname;
          $tplVars['email'] = $user->email;

          $sender->sendEmail($params, $tplVars, 'subscribe_publications');
          }


          } */

        // clear the cache
        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publications')
        );

        return $newId;
    }

    /**
     *  
     * @param User_Resource_User_Item_Interface $publication
     * @param array $coordinates
     * @return boolean
     */
    public function cropPhoto($publication, $coordinates)
    {

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication');

        $filter = new ARTCMF_Filter_ImageSize();

        $path_parts = pathinfo($publication->full);
        $new_file_name = $publication->ident . '-' . $publication->publication_id . '-' . $publication->publication_id . '.' . $path_parts['extension'];

        copy($fileDestination . '/' . $publication->full, $fileDestination . '/' . $new_file_name);

        $publicationGroup = $this->getPublicationGroupById($publication->publication_group_id);

        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication_' . $publicationGroup->ident);
        
        if (!$image_resize_setting)
                $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication');

        $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                ->setHeight($image_resize_setting['height_thumbnail'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setCoordinates($coordinates)
                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                ->filter($fileDestination . '/' . $new_file_name);

        $preview = $filter->setWidth($image_resize_setting['width_preview'])
                ->setHeight($image_resize_setting['height_preview'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setCoordinates($coordinates)
                ->setStrategy(new $image_resize_setting['strategy_preview']())
                ->filter($fileDestination . '/' . $new_file_name);


        $coordinates['thumbnail'] = basename($thumbnail);
        $coordinates['preview'] = basename($preview);

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publications')
        );


        return $this->getResource('Publication')->saveRow($coordinates, $publication);
    }

    /*     * ***************************************************************** */

    public function cropPhotoCategory($category, $coordinates)
    {

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication/category');

        $filter = new ARTCMF_Filter_ImageSize();

        $path_parts = pathinfo($category->full);
        $new_file_name = $category->ident . '-' . $category->publication_category_id . '-' . $category->publication_category_id . '.' . $path_parts['extension'];
        copy($fileDestination . '/' . $category->full, $fileDestination . '/' . $new_file_name);

        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication_category');


        $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                ->setHeight($image_resize_setting['height_thumbnail'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setCoordinates($coordinates)
                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                ->filter($fileDestination . '/' . $new_file_name);

        $preview = $filter->setWidth($image_resize_setting['width_preview'])
                ->setHeight($image_resize_setting['height_preview'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setCoordinates($coordinates)
                ->setStrategy(new $image_resize_setting['strategy_preview']())
                ->filter($fileDestination . '/' . $new_file_name);


        $coordinates['thumbnail'] = basename($thumbnail);
        $coordinates['preview'] = basename($preview);

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publication_categories')
        );


        return $this->getResource('Publicationcategory')->saveRow($coordinates, $category);
    }

    /*     * ***************************************************************** */

    public function deletePublication($publication)
    {
        if (!$this->checkAcl('deletePublication')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null !== $publication) {
            $fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication');
            unlink($fileDestination . '/' . $publication->thumbnail);
            unlink($fileDestination . '/' . $publication->preview);
            unlink($fileDestination . '/' . $publication->full);
            $this->getResource('Publication')->deleteTranslatedRows($publication->publication_id);
            $publication->delete();

            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publications')
            );

            return true;
        }

        return false;
    }
    
    /**
     * Return data for Search module
     *   
     * @return array Keys: url, title, body
     */
    
    public function getSearchData()
    {
        $searchData = array();

        foreach (Zend_Registry::get('langList') as $lang) {
            
            //Групы публикаций 
            $rows1 = $this->getResource('Publicationgroup')->getSearchData($lang->code);
            if (!is_null($rows1)) {
                foreach ($rows1 as $row1) {
                    array_push($searchData, array(
                        'url' => '/' . $lang->code . '/publication/' . $row1->ident,
                        'title' => $row1->name,
                        'body' => $row1->description,
                        'lang' => $lang->code
                    ));
                }
            }
            
            //Категории публикаций
            $rows2 = $this->getResource('Publicationcategory')->getSearchData($lang->code);
            if (!is_null($rows2)) {
                foreach ($rows2 as $row2) {
                    array_push($searchData, array(
                        'url' => '/' . $lang->code . '/publication/category/' . $row2->publication_group_ident . '/' . $row2->ident,
                        'title' => $row2->name,
                        'body' => $row2->description,
                        'lang' => $lang->code
                    ));
                }
            }

            //Публикации
            $rows3 = $this->getResource('Publication')->getSearchData($lang->code);
            if (!is_null($rows3)) {
                foreach ($rows3 as $row3) {
                    array_push($searchData, array(
                        'url' => '/' . $lang->code . '/publication/view/' . $row3->publication_group_ident . '/' . $row3->ident . '.html',
                        'title' => $row3->title,
                        'body' => $row3->body,
                        'lang' => $lang->code
                    ));
                }
            }
        }

        return $searchData;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Publication';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}