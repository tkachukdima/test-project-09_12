<?php

/*
 * PublicationController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Publication_IndexController extends Zend_Controller_Action
{

    /**
     * @var Publication_Model_Publication
     */
    protected $_modelPublication;
    protected $_authService;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelPublication = new Publication_Model_Publication();
        $this->_authService = new User_Service_Authentication();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {
        // publication/category/services/elektroenergetika
        // publication/news

        $request = $this->getRequest();

        $site_settings = Zend_Registry::get('site_settings');

        $groupIdent    = $request->getParam('groupIdent', false);
        $categoryIdent = $request->getParam('categoryIdent', false);
        
        if ($groupIdent == 'soiskatelyam') {
            $this->_helper->getHelper('layout')->setLayout('layout_si');}
           elseif($groupIdent == 'rabotodatelyam') {
               $this->_helper->getHelper('layout')->setLayout('layout_rd');
           }

        if (!$groupIdent) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Section not found')) . ' ' . $groupIdent);
        }

        // обєкт групи
        $this->view->publication_group = $this->_modelPublication->getPublicationGroupByIdent($groupIdent);

        if (is_null($this->view->publication_group)) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Section not found')) . ' ' . $request->getParam('groupIdent', null));
        }
        $this->view->publication_group_ident = $groupIdent;
        $this->view->perpage = $this->view->publication_group->items_per_page;

        if ($groupIdent && !$categoryIdent) { // група
            
            $this->view->title = $this->view->publication_group->name;
            
            $page_title = $this->view->publication_group->page_title;
            $meta_description = $this->view->publication_group->meta_description;
            $meta_keywords = $this->view->publication_group->meta_keywords;
            
            $this->view->publications = $this->_modelPublication/*->getCached('publications')*/->getPublicationsByGroupIdent($request->getParam('page', 1), null, $this->view->publication_group->items_per_page, $this->view->publication_group->ident, false, 1);

            $this->view->publication_category = $this->_modelPublication/*->getCached('publications')*/->getPublicationCategoriesByGroupId($this->view->publication_group->publication_group_id);
        } elseif ($categoryIdent) { // категорія

            $this->view->category = $this->_modelPublication->getPublicationCategoryByIdent($categoryIdent);
            $this->view->parent_category = $this->_modelPublication->getPublicationCategoryById($this->view->category->parent_id);
            $this->view->title = $this->view->category->name;
            
            $page_title = $this->view->category->page_title;
            $meta_description = $this->view->category->meta_description;
            $meta_keywords = $this->view->category->meta_keywords;
            

            $this->view->publications = $this->_modelPublication/*->getCached('publications')*/->getPublicationsByCategoryId($request->getParam('page', 1), null, $this->view->publication_group->items_per_page, $this->view->category->publication_category_id, 1);
            $this->view->publication_category = $this->_modelPublication/*->getCached('publications')*/->getPublicationCategoriesByParentId($this->view->category->publication_category_id, $this->view->category->publication_group_id);
        }
                      
        $this->view->headTitle($page_title, 'PREPEND');
        $this->view->headMeta()->prependName('description', $meta_description);
        $this->view->headMeta()->prependName('keywords', $meta_keywords);
               

        $this->view->addHelperPath('ARTCMF/view/helper', 'ARTCMF_View_Helper');
        $this->view->pagination_config = array(
            'total_items'    => count($this->view->publications),
            'items_per_page' => 1,
            'style'          => 'digg2');
        
        /* можливість задавати для різних груп публікацій свої шаблони(view) */
        if (file_exists(dirname(__FILE__) . '/../views/scripts/index/index-' . strtolower($groupIdent) . '.phtml'))
            return $this->render('index-' . strtolower($groupIdent));
    }

    public function category()
    {

        /*
          if (!$request->getParam('categoryIdent', null)) {
          throw new ARTCMF_Exception_404($this->view->translate(_('Section not found')) . ' ' . $request->getParam('categoryIdent', null));
          } */
    }

    public function allAction()
    {

//        $this->_modelPublication = new Publication_Model_Publication();
//        $this->view->publication_group = $this->_modelPublication->getPublicationGroupsInMenu();
    }

    public function rssAction()
    {
        $request = $this->getRequest();

        if (!$request->getParam('groupIdent', null)) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Section not found')) . ' ' . $request->getParam('groupIdent', null));
        }

        $publication_group = $this->_modelPublication->getPublicationGroupByIdent($request->getParam('groupIdent', null));

        if (is_null($publication_group)) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Section not found')) . ' ' . $request->getParam('groupIdent', null));
        }

        $publications = $this->_modelPublication->getPublicationsByGroupIdent(null, null, null, $publication_group->ident, false, 1);

        $urlPrefix = $this->view->serverUrl();

        $site_settings = Zend_Registry::get('site_settings');

        $feedArray = array(
            'title'       => $publication_group->name . ' - ' . $site_settings->getSetting('default', 'site_name'),
            'link'        => $urlPrefix . '/' . $this->view->currentLang . '/' . $item->publication_group_ident,
            'description' => $publication_group->description,
            'charset'     => 'UTF-8',
            'entries'     => array()
        );

        foreach ($publications as $item) {
            $date          = new Zend_Date($item->date_post, 'YYYY-MM-dd HH:mm:ss');
            $itemTimestamp = $date->getTimestamp();

            $feedArray['entries'][] = array(
                'title'       => $item->title,
                'link'        => $urlPrefix . '/' . $this->view->currentLang . '/view/' . $item->publication_group_ident . '/' . $item->ident,
                'description' => $item->body,
                'lastUpdate'  => $itemTimestamp
            );
        }

        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $feed = Zend_Feed::importArray($feedArray, 'rss');

        $feed->send();
    }

    public function viewAction()
    {
        $request = $this->getRequest();

        if (($this->view->print = $request->getParam('print', 0)) == 1) {
            $this->view->layout()->setLayout('print');
        }

        
        
        

        $site_settings = Zend_Registry::get('site_settings');

        $groupIdent    = $request->getParam('groupIdent', false);
        $categoryIdent = $request->getParam('categoryIdent', false);
        
        
        if ($groupIdent == 'rabotodatelyam') {
            $this->_helper->getHelper('layout')->setLayout('layout_publ');}
        
        

        if (!$groupIdent) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Section not found')) . ' ' . $groupIdent);
        }

        // обєкт групи
        $this->view->publication_group = $this->_modelPublication->getPublicationGroupByIdent($groupIdent);

        if (is_null($this->view->publication_group)) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Section not found')) . ' ' . $request->getParam('groupIdent', null));
        }
        $this->view->publication_group_ident = $groupIdent;
        $this->view->perpage = $this->view->publication_group->items_per_page;

        if ($groupIdent && !$categoryIdent) { // група
            
            $this->view->title = $this->view->publication_group->name;
            
            $page_title = $this->view->publication_group->page_title;
            $meta_description = $this->view->publication_group->meta_description;
            $meta_keywords = $this->view->publication_group->meta_keywords;
            
            $this->view->publications = $this->_modelPublication/*->getCached('publications')*/->getPublicationsByGroupIdent($request->getParam('page', 1), null, $this->view->publication_group->items_per_page, $this->view->publication_group->ident, false, 1);

            $this->view->publication_category = $this->_modelPublication/*->getCached('publications')*/->getPublicationCategoriesByGroupId($this->view->publication_group->publication_group_id);
        } elseif ($categoryIdent) { // категорія

            $this->view->category = $this->_modelPublication->getPublicationCategoryByIdent($categoryIdent);
            $this->view->parent_category = $this->_modelPublication->getPublicationCategoryById($this->view->category->parent_id);
            $this->view->title = $this->view->category->name;
            
            $page_title = $this->view->category->page_title;
            $meta_description = $this->view->category->meta_description;
            $meta_keywords = $this->view->category->meta_keywords;
            

            $this->view->publications = $this->_modelPublication/*->getCached('publications')*/->getPublicationsByCategoryId($request->getParam('page', 1), null, $this->view->publication_group->items_per_page, $this->view->category->publication_category_id, 1);
            $this->view->publication_category = $this->_modelPublication/*->getCached('publications')*/->getPublicationCategoriesByParentId($this->view->category->publication_category_id, $this->view->category->publication_group_id);
        }
        
        
        
        
        
        $publicationIdent = $request->getParam('publicationIdent', 0);

        $this->view->publication = $this->_modelPublication->getPublicationByIdent($publicationIdent);

        if (null === $this->view->publication) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Publication not found')) . ' ' . $request->getParam('publicationIdent'));
        }

        $this->view->publication_group = $this->_modelPublication->getPublicationGroupById($this->view->publication->publication_group_id);

        $this->view->category = $this->_modelPublication->getPublicationCategoryById($this->view->publication->publication_category_id);
        if ($this->view->category)
            $this->view->parent_category = $this->_modelPublication->getPublicationCategoryById($this->view->category->parent_id);

        $this->view->headTitle($this->view->publication->page_title, 'PREPEND');
        $this->view->headMeta()->prependName('description', $this->view->publication->meta_description);
        $this->view->headMeta()->prependName('keywords', $this->view->publication->meta_keywords);
        
         /* можливість задавати для різних груп публікацій свої шаблони(view) */
        if (file_exists(dirname(__FILE__) . '/../views/scripts/index/view-' . strtolower($this->view->publication_group->ident) . '.phtml'))
            return $this->render('view-' . strtolower($this->view->publication_group->ident));
       
    }

}
