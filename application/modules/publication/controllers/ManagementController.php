<?php

/*
 * PublicationController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Publication_ManagementController extends Zend_Controller_Action
{

    /**
     * @var Publication_Model_Publication
     */
    protected $_modelPublication;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelPublication = new Publication_Model_Publication();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function listAction()
    {

        $request = $this->getRequest();
        if ($request->getParam('category_id'))
            $this->view->category_id = (int) $request->getParam('category_id', 0);

        if ($request->getParam('group_id'))
            $this->view->group_id = (int) $request->getParam('group_id', 0);

        if (0 != $this->view->category_id) {
            $this->view->publications = $this->_modelPublication->getPublicationsByCategoryId($request->getParam('page', 1), null, 10, $this->view->category_id);
        } elseif (0 != $this->view->group_id) {
            $this->view->publications = $this->_modelPublication->getPublicationsByGroupId($request->getParam('page', 1), null, 10, $this->view->group_id);
        }


        $this->view->addHelperPath('ARTCMF/view/helper', 'ARTCMF_View_Helper');
        $this->view->pagination_config = array('total_items' => count($this->view->publications),
            'items_per_page' => 1,
            'style' => 'digg');
    }

    public function listPublicationGroupAction()
    {
        $this->view->publication_group = $this->_modelPublication->getPublicationGroups();
    }

    public function addPublicationGroupAction()
    {
        $this->view->publicationGroupForm = $this->_getPublicationGroupForm('add');
    }

    public function addPublicationCategoryAction()
    {
        $this->view->publicationCategoryForm = $this->_getPublicationCategoryForm('add');
    }

    public function listPublicationCategoryAction()
    {
        $this->view->publication_category = $this->_modelPublication->getPublicationCategories();
    }

    public function editPublicationGroupAction()
    {
        $request = $this->getRequest();

        if (!$request->getParam('group_id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Group not found')) . ' ' . $request->getParam('group_id'));
        }

        $publication_publication_group_id = (int) $request->getParam('group_id');

        $publication_group = $this->_modelPublication->getPublicationGroupByIdForEdit($publication_publication_group_id);


        $this->view->publicationGroupForm = $this->_getPublicationGroupForm('edit')->populate($publication_group);
    }

    public function editPublicationCategoryAction()
    {
        $request = $this->getRequest();

        if (!$request->getParam('category_id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Category not found')) . ' ' . $request->getParam('category_id'));
        }

        $publication_category_id = (int) $request->getParam('category_id');

        $publication_category = $this->_modelPublication->getPublicationCategoryByIdForEdit($publication_category_id);


        $this->view->publication_category = $publication_category;


        $this->view->publicationCategoryForm = $this->_getPublicationCategoryForm('edit')->populate($publication_category);
    }

    public function savePublicationGroupAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelPublication->savePublicationGroup($request->getPost(), $type)) {
            $this->view->publicationGroupForm = $this->_getPublicationGroupForm($type);
            return $this->render($type . '-publication-group');
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Group added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Group updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list-publication-group',
                    'controller' => 'management',
                    'module' => 'publication'), 'admin', true);
    }

    public function savePublicationCategoryAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }


        if (false === $this->_modelPublication->savePublicationCategory($request->getPost(), $type)) {
            $this->view->publicationCategoryForm = $this->_getPublicationCategoryForm($type);
            return $this->render($type . '-publication-category');
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Category added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Category updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'management',
                    'module' => 'publication',
                    'group_id' => $request->getParam('group_id'),
                    'category_id' => $request->getParam('category_id'),
                        ), 'admin', true);
    }

    public function deletePublicationGroupAction()
    {
        $request = $this->getRequest();
        
        if (false === ($publication_group_id = $request->getParam('group_id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Group not found')) . ' ' . $publication_group_id);
        }

        $this->_modelPublication->deletePublicationGroup($publication_group_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list-publication-group',
                    'controller' => 'management',
                    'module' => 'publication'), 'admin', true);
    }

    public function deletePublicationCategoryAction()
    {
        $request = $this->getRequest();
        if (false === ($publication_category_id = $request->getParam('category_id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Category not found')) . ' ' . $publication_category_id);
        }

        $this->_modelPublication->deletePublicationCategory($publication_category_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list-publication-category',
                    'controller' => 'management',
                    'module' => 'publication'), 'admin', true);
    }

    public function addPublicationAction()
    {
        $this->view->publicationForm = $this->_getPublicationForm('add');
    }

    public function editPublicationAction()
    {
        $request = $this->getRequest();

        $publication_id = $request->getParam('publication_id', false);

        if (!$publication_id) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Publication not found')) . ' ' . $publication_id);
        }
        
        $publication = $this->_modelPublication->getPublicationByIdForEdit((int)$publication_id);

        if (!$publication) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Publication not found')) . ' ' . $publication_id);
        }


        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel('publication');

        $this->view->strategy = $image_resize_setting['strategy_thumbnail'];

        $this->view->publication = $publication;

        $this->view->publicationForm = $this->_getPublicationForm('edit')->populate($publication);
    }

    public function savePublicationAction()
    {
        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelPublication->savePublication($request->getPost(), $type)) {
            $this->view->publicationForm = $this->_getPublicationForm($type);
            return $this->render($type . '-publication');
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Publication added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Publication updated'));
                break;

            default:
                break;
        }

        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'management',
                    'module' => 'publication',
                    'group_id' => $request->getPost('publication_group_id')), 'admin', true);
    }

    public function cropAction()
    {
        $request = $this->getRequest();
        $this->_helper->layout()->disableLayout();

        $publication_id = (int) $request->getParam('publication_id');

        $this->view->publication = $this->_modelPublication->getPublicationById($publication_id);

        $publicationGroup = $this->_modelPublication->getPublicationGroupById($this->view->publication->publication_group_id);

        $settingModel = new Default_Model_Settings();
        $this->view->image_resize_setting = $settingModel->getImageResizeSettingForModel('publication_' . $publicationGroup->ident);

        if (!$this->view->image_resize_setting)
                $this->view->image_resize_setting = $settingModel->getImageResizeSettingForModel('publication');
         
        $request = $this->getRequest();

        if ($request->isPost()) {

            if (null !== ($id = $this->_modelPublication->cropPhoto($this->view->publication, $request->getPost()))) {
                $redirector = $this->getHelper('redirector');
                return $redirector->gotoRoute(array(
                            'publication_id' => $publication_id,
                            'action' => 'edit-publication',
                            'controller' => 'management',
                            'module' => 'publication'), 'admin', true);
            }
        }
    }

    public function cropCategoryAction()
    {
        $request = $this->getRequest();
        $this->_helper->layout()->disableLayout();

        $publication_category_id = (int) $request->getParam('category_id');
        $this->view->publication_category = $this->_modelPublication->getPublicationCategoryById($publication_category_id);
        $settingModel = new Default_Model_Settings();
        $this->view->image_resize_setting = $settingModel->getImageResizeSettingForModel('publication_category');

        $request = $this->getRequest();

        if ($request->isPost()) {

            if (null !== ($id = $this->_modelPublication->cropPhotoCategory($this->view->publication_category, $request->getPost()))) {
                $redirector = $this->getHelper('redirector');
                return $redirector->gotoRoute(array(
                            'category_id' => $publication_category_id,
                            'action' => 'edit-publication-category',
                            'controller' => 'management',
                            'module' => 'publication'), 'admin', true);
            }
        }
    }

    public function deletePublicationAction()
    {
        $request = $this->getRequest();
        if (false === ($publication_id = $request->getParam('publication_id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Publication not found')) . ' ' . $publication_id);
        }

        $publication = $this->_modelPublication->getPublicationById($publication_id);
        
        $group_id = $publication->publication_group_id;
        $this->_modelPublication->deletePublication($publication);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'management',
                    'module' => 'publication',
                    'group_id' => $group_id), 'admin', true);
    }

    public function getCategoriesGroupAjaxAction()
    {
        $request = $this->getRequest();
        $groupId = $request->getParam('groupId', 0);
        $categories = $this->_modelPublication->getPublicationCategoriesForSelect(0, $groupId, 0);

        if (count($categories))
            return $this->_helper->json(array('status' => 'success', 'cetegories' => $categories));
        else
            return $this->_helper->json(array('status' => 'error'));
    }




    public function getGroupImageStatusAjaxAction()
    {
        $request = $this->getRequest();
        $groupId = $request->getParam('groupId', 0);
        $group = $this->_modelPublication->getPublicationGroupById($groupId);

        if (count($group))
            return $this->_helper->json(array('status' => 'success', 'image_status' => $group->image_status));
        else
            return $this->_helper->json(array('status' => 'error'));
    }




    protected function _getPublicationForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[ucfirst($type)] = $this->_modelPublication->getForm(ucfirst($type));
        $this->_forms[ucfirst($type)]->setAction($urlHelper->url(array(
                    'controller' => 'management',
                    'action' => 'save-publication',
                    'type' => $type), 'admin'));
        $this->_forms[ucfirst($type)]->setMethod('post');

        return $this->_forms[ucfirst($type)];
    }

    protected function _getPublicationGroupForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Group' . ucfirst($type)] = $this->_modelPublication->getForm('Group' . ucfirst($type));
        $this->_forms['Group' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'controller' => 'management',
                    'action' => 'save-publication-group',
                    'type' => $type), 'admin'));
        $this->_forms['Group' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Group' . ucfirst($type)];
    }

    protected function _getPublicationCategoryForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Category' . ucfirst($type)] = $this->_modelPublication->getForm('Category' . ucfirst($type));
        $this->_forms['Category' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'controller' => 'management',
                    'action' => 'save-publication-category',
                    'type' => $type), 'admin'));
        $this->_forms['Category' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Category' . ucfirst($type)];
    }

}
