<?php
/**
 * CategoryService Select
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Publication_Form_Category_Selectcategory extends ARTCMF_Form_Abstract
{
    public function init()
    {
        $this->setMethod('post');
        
        $categories=$this->getModel()->getPublicationCategories()->toArray();
   
        $cats = array(0 => _('Root level'));
        
        foreach($categories as $category) {
            $cats[$category['publication_category_id']] = $category['name'];
        }

        $this->addElement('select', 'publication_category_id', array(
            'label' => _('Select category'),
            'multiOptions' => $cats
        ));
        
        $this->addElement('submit', 'View', array(
            'decorators' => array('ViewHelper'),
        ));
    }

}
