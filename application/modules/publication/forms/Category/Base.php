<?php

/**
 * Base Publication category Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Form_Category_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );
        
        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication/category');

        $this->setMethod('post');
        $this->setAction('');


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'name_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));
            
            
            $element_name = 'description_' . $lang->code;
            $this->addElement('textarea', $element_name, array(
                'label' => _('Description'),
                'filters' => array('StringTrim'),
                'required' => true,
                'class' => "ckeditor"                
            ));            
            //$this->$element_name->addDecorator(new ARTCMF_Form_Decorator_CKEditor);
            
            
            $this->addElement('textarea', 'page_title_' . $lang->code, array(
                'label' => _('Page Title'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 2,
                'required' => false
            ));
            
            $this->addElement('textarea', 'meta_description_' . $lang->code, array(
                'label' => _('META description'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true
            ));

            $this->addElement('textarea', 'meta_keywords_' . $lang->code, array(
                'label' => _('META Keywords'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true,
            ));
            
                   $this->addElement('text', 'description_img_' . $lang->code, array(
            'label' => _('Description of the image'),
            'filters' => array('StringTrim'),
            'required' => false,
            ));
                   
        }

            //get select category
        $form3 = new Publication_Form_Category_Selectcategory(
                        array('model' => $this->getModel())
        );
        
        $element3 = $form3->getElement('publication_category_id');
        $element3->clearDecorators()->loadDefaultDecorators()->setName('parent_id');
        $this->addElement($element3, 'parent_id');
        
        // get the select group
        $form2 = new Publication_Form_Category_Selectgroup(
                        array('model' => $this->getModel())
        );
        
        $element2 = $form2->getElement('publication_group_id');
        $element2->clearDecorators()->loadDefaultDecorators();
        $this->addElement($element2, 'publication_group_id');
        
        
         $this->addElement('Checkbox', 'delete_image', array(
            'label' => _('Delete Image')
        ));



        
        $this->addElement('file', 'full', array(
             'label' => _('Image'),
            'required' => false,
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg','jpeg','png','gif')),
            ),
        ));





        $this->addElement('text', 'ident', array(
            'label' => _('Character code'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(
                array('UniqueIdent', true, array($this->getModel(), 'getCategoryPublicationByIdent', 'getPublicationCategoryById', 'publication_category_id'))
            ),
            'required' => false,
        ));

        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array('1' => _('Active'), '0' => _('Not active'),)
        ));


        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('submit', 'submit', array(
        ));

        $this->addElement('hidden', 'publication_category_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
        
        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'name_' . $lang->code,
                'description_' . $lang->code,
                'page_title_' . $lang->code,
                'meta_description_' . $lang->code,
                'meta_keywords_' . $lang->code,
                'description_img_' . $lang->code,
                
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('publication_group_id', 'parent_id', 'delete_image', 'full', 'on_main','ident', 'sort_order', 'status', 'submit'), 'form_all', array('legend' => _('General Settings')));
    }

    
}
