<?php

/**
 * Add new news post
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Form_Add extends Publication_Form_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->removeElement('publication_id');
        $this->removeElement('delete_image');
      //  addElement($element, 'publication_category_id');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
