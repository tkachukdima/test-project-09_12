<?php

/**
 * Add new Publication
 *
 * @CategoryService   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Form_Group_Add extends Publication_Form_Group_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('publication_group_id')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
