<?php

class Publication_View_Helper_GetPublicationByIdent extends Zend_View_Helper_Abstract
{

    public function getPublicationByIdent($ident)
    {
        $publicationModel = new Publication_Model_Publication();

        return $publicationModel->getPublicationsByGroupIdent(null, null, null, $ident);

        //return $this->_publicationModel->getPublicationByIdent($ident);

    }
}