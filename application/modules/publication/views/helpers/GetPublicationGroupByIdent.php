<?php

class Publication_View_Helper_GetPublicationGroupByIdent extends Zend_View_Helper_Abstract
{

    public function getPublicationGroupByIdent($ident)
    {
        $publicationModel = new Publication_Model_Publication();

        return $publicationModel->getPublicationGroupByIdent($ident);

//return $this->_publicationModel->getPublicationByIdent($ident);
    }

}