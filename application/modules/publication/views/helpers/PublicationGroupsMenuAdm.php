<?php

class Publication_View_Helper_PublicationGroupsMenuAdm extends Zend_View_Helper_Abstract
{

    public function publicationGroupsMenuAdm()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        if ('publication' != $request->getModuleName())
            return false;

        $this->_publicationModel = new Publication_Model_Publication();
        $groups = $this->_publicationModel->getPublicationGroupsForMenu();

        if (null == $groups)
            $container = array(
                array('label'      => 'Publications', 'module'     => 'publication', 'controller' => 'management', 'action'     => 'list', 'route'      => 'admin')
            );
        else
            $container   = array(
                array('label'      => 'Publications', 'module'     => 'publication', 'controller' => 'management', 'action'     => 'list', 'route'      => 'admin', 'pages'      => $groups)
            );

        $container = new Zend_Navigation($container);
        return $container;
    }

}
