<?php

class Statistics_Model_Statistics extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    public function getVisitors($type = 'year')
    {
        return $this->getResource('Visitors')->getVisitors($type);
    }

    public function saveVisitors($data, $type = 'year')
    {

        $this->getResource('Visitors')->clearVisitors($type);

        //получаем и обрабатываем результаты
        foreach ($data as $result) {

            $date = ('year' == $type) ? $result->getYear() . '-' . $result->getMonth() : $result->getYear() . '-' . $result->getMonth() . '-' . $result->getDay();

            $row = array(
                'date' => date('Y-m-d H:i:s', strtotime($date)),
                'type' => $type,
                'visitors' => $result->getVisitors(), //посетители
                'pageviews' => $result->getPageviews(), //просмотры
                'visits' => ('year' == $type) ? 0 : $result->getVisits() //посещения
            );
            //пишем в базу            
            $this->getResource('Visitors')->saveRow($row);
        }
    }

    public function getLocations($type = 'country')
    {
        return $this->getResource('Locations')->getLocations($type);
    }

    public function saveLocations($data, $type = 'country')
    {

        $this->getResource('Locations')->clearLocations($type);

        //получаем и обрабатываем результаты
        foreach ($data as $result) {
            $row = array(
                'type' => $type,
                'name' => ('country' == $type) ? $result->getCountry() : $result->getCity(), //названия
                'visits' => $result->getVisits() //посещения
            );
            //пишем в базу            
            $this->getResource('Locations')->saveRow($row);
        }
    }

    public function getSettingById($id = 1)
    {
        return $this->getResource('Settings')->getSettingById($id);
    }

    public function saveSettings($data, $validator = null)
    {

        $validator = $this->getForm('Settings');

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();


        $setting = array_key_exists('setting_id', $data) ?
                $this->getResource('Settings')->getSettingById($data['setting_id']) : null;

        return $this->getResource('Settings')->saveRow($data, $setting);
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Statistics';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
