<?php

/**
 * Statistics_Resource_Locations
 *
 * @category   Default
 * @package    Statistics_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Statistics_Resource_Locations extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'statistics_locations';
    protected $_primary = array('name', 'type');

    public function getLocations($type = 'country')
    {
        $select = $this->select()
                ->where('type = ?', $type)
                ->order('visits');

        return $this->fetchAll($select);
    }
    
    public function clearLocations($type = 'country')
    {               
        $this->getAdapter()->delete($this->_name, "type = '{$type}'");
    }

}