<?php

/**
 * Statistics_Resource_Setting
 *
 * @category   Statistics
 * @package    Statistics_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Statistics_Resource_Settings extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'statistics_settings';
    protected $_primary = 'setting_id';

    public function getSettingById($id)
    {
        return $this->find($id)->current();
    }

}