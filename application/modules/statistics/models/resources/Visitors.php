<?php

/**
 * Statistics_Resource_Visitors
 *
 * @category   Default
 * @package    Statistics_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Statistics_Resource_Visitors extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'statistics_visitors';
    protected $_primary = array('date', 'type');
       
    public function getVisitors($type = 'year')
    { 
       $select = $this->select() 
                ->where('type = ?', $type)
                ->order('date');
                
        return $this->fetchAll($select);
    }   
    
    public function clearVisitors($type = 'year')
    {               
        $this->getAdapter()->delete($this->_name, "type = '{$type}'");
    }
        
}