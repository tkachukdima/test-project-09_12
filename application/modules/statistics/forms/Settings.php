<?php

/**
 * The Statistics form
 *
 * @category   Statistics
 * @package    Statistics_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Statistics_Form_Settings extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->setMethod('post');
        $this->setAction('');

        $this->setName('statisticsSettingsForm');


        $this->addElement('text', 'user', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'required' => true,
            'label' => _('Google email'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            )
        ));

        $this->addElement('password', 'password', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label' => _('Password'),
            'validators' => array(
                array('NotEmpty')
            )
        ));

        $this->addElement('text', 'report_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label' => _('Report Id'),
            'validators' => array(
                array('NotEmpty')
            )
        ));


        $this->addElement('submit', 'submit', array(
            'label' => _('Send'),
        ));

        $this->addElement('hidden', 'setting_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
    }

}
