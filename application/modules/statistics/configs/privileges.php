<?php

return array(
    'Redactor' => array(
        'allow' => array(
            'index' => array(
                'index',
                'refresh'
            )
        )
    ),
    'Manager' => array(),
    'Admin' => array(
        'allow' => array(
            'index' => array(
                'settings',
                'save-settings'
            )
        )
    )
);