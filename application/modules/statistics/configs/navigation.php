<?php

return array(
    array(
        'label' => _('Statistics'),
        'module' => 'statistics',
        'controller' => 'index',
        'action' => 'index',
        'route' => 'admin',
        'resource' => 'statistics:index',
        'privilege' => 'index',
        'reset_params' => true,
        'class' => 'dashboard-statistics',
        'pages' => array(
            array(
                'label' => _('Statistics'),
                'module' => 'statistics',
                'controller' => 'index',
                'action' => 'index',
                'route' => 'admin',
                'resource' => 'statistics:index',
                'privilege' => 'index',
                'reset_params' => true
            ),
            array(
                'label' => _('Refresh'),
                'module' => 'statistics',
                'controller' => 'index',
                'action' => 'refresh',
                'route' => 'admin',
                'resource' => 'statistics:index',
                'privilege' => 'refresh',
                'reset_params' => true
            ),
            array(
                'label' => _('Settings'),
                'module' => 'statistics',
                'controller' => 'index',
                'action' => 'settings',
                'route' => 'admin',
                'resource' => 'statistics:index',
                'privilege' => 'settings',
                'reset_params' => true
            )
        )
    )
);