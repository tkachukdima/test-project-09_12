<?php

/*
 * Statistics_IndexController
 * 
 * @category   Statistics
 * @package    Statistics_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Statistics_IndexController extends Zend_Controller_Action
{

    protected $_modelStatistics = null;

    public function init()
    {
        $this->_modelStatistics = new Statistics_Model_Statistics();
    }

    public function indexAction()
    {

        $this->view->assign(array(
            'visitorsYear' => $this->_modelStatistics->getVisitors('year'),
            'visitorsMonth3' => $this->_modelStatistics->getVisitors('month3'),
            'countries' => $this->_modelStatistics->getLocations('country'),
            'cities' => $this->_modelStatistics->getLocations('city'),
        ));
    }

    public function refreshAction()
    {
        //учетная запись GA
        
        $settings = $this->_modelStatistics->getSettingById(1);
          
        $currentdate = date("Ymd");

        $currentday = date("d");
        $currentmonth = date("m");
        $currentyear = date("Y");

        //дата, начиная с которой необходимо получить данные из GA для отчета. Формат YYYY-MM-DD
        $datestart = $currentyear - 1 . '-' . $currentmonth . '-' . $currentday;
        //дата, заканчивая которой       
        //или вычисляем дату - конец предыдущего месяца        
        $datefinish = date("Y-m-d", mktime(0, 0, 0, $currentmonth, 0, $currentyear));

        //дата 3 месяца назад
        $date3MonthStart = date("Y-m-d", mktime(0, 0, 0, $currentmonth - 3, $currentday - 1, $currentyear));
        $date3MonthFinish = date("Y-m-d", mktime(0, 0, 0, $currentmonth, $currentday - 1, $currentyear));

        //дата месяц назад
        $date1MonthStart = date("Y-m-d", mktime(0, 0, 0, $currentmonth - 1, $currentday - 1, $currentyear));
        $date1MonthFinish = date("Y-m-d", mktime(0, 0, 0, $currentmonth, $currentday - 1, $currentyear));

        //количество стран
        $countryRows = 5;
        //количество городов
        $cityRows = 10;


        //подключаем класс GA API
        include(APPLICATION_PATH . "/../library/GAPI/gapi.class.php");

        $ga = new gapi($settings->user, $settings->password);

        //////получаем пользователи/просмотры за все время
        $ga->requestReportData($settings->report_id, array('month', 'year'), array('visitors', 'pageviews'), 'year', null, $datestart, $datefinish, 1, 1000);
        $this->_modelStatistics->saveVisitors($ga->getResults(), 'year');


        //////получаем пользователи/просмотры/посещения за последние 3 месяца
        $ga->requestReportData($settings->report_id, array('day', 'month', 'year'), array('visitors', 'visits', 'pageviews'), array('year', 'month'), null, $date3MonthStart, $date3MonthFinish, 1, 1000);
        $this->_modelStatistics->saveVisitors($ga->getResults(), 'month3');


        //////получаем географию посещений за последний месяц
        $ga->requestReportData($settings->report_id, array('country'), array('visits'), '-visits', null, $date1MonthStart, $date1MonthFinish, 1, $countryRows);
        $this->_modelStatistics->saveLocations($ga->getResults(), 'country');


        //////получаем ГОРОДА за последний месяц
        $ga->requestReportData($settings->report_id, array('city'), array('visits'), '-visits', null, $date1MonthStart, $date1MonthFinish, 1, $cityRows);
        $this->_modelStatistics->saveLocations($ga->getResults(), 'city');



        $this->_helper->FlashMessenger->setNamespace('success')
                ->addMessage($this->view->translate(_('Statictics is updated')));

        $redirector = $this->getHelper('redirector');


        return $redirector->gotoRoute(array(
                    'module' => 'statistics',
                    'controller' => 'index',
                    'action' => 'index'), 'admin', false);
    }

    public function settingsAction()
    {
        $settings = $this->_modelStatistics->getSettingById(1);

        $this->view->settingsForm = $this->_getSettingForm()->populate($settings->toArray());
    }

    public function saveSettingsAction()
    {

        $request = $this->getRequest();

        if (!$request->isPost()) {
            return $this->_helper->redirector('settings');
        }

        if (false === $this->_modelStatistics->saveSettings($request->getPost())) {
            $this->view->settingsForm = $this->_getSettingForm();
            $this->view->settingsForm->setDescription($this->view->translate(_('Error! Maybe you made a mistake when filling out form.')));
            return $this->render('settings');
        }

        $message = $this->view->translate(_('Settings updated'));

        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'index',
                    'controller' => 'index',
                    'module' => 'statistics'), 'admin', true);
    }

    protected function _getSettingForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Settings'] = $this->_modelStatistics->getForm('Settings');
        $this->_forms['Settings']->setAction($urlHelper->url(array(
                    'controller' => 'index',
                    'action' => 'save-settings',
                    'module' => 'statistics'
                        ), 'admin', true
        ));
        $this->_forms['Settings']->setMethod('post');

        return $this->_forms['Settings'];
    }

}