<?php

/**
 * Default_Service_Settings
 * 
 * 
 * 
 * @category   Default
 * @package    Default_Service
 * @copyright  Copyright (c) 2011 
 * @license    Commercial License
 */
class Default_Service_Settings
{
    protected $_settings;

    public function setAllSettings($allSettings){

        $this->_settings = $allSettings;

        return $this;
    }

    public function getSetting($module, $key){

        if ( isset($this->_settings[$module][$key] ) )
            return $this->_settings[$module][$key];
        else
            return sprintf("#[%s_%s]", $module , $key);
    }

}
