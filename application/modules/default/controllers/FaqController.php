<?php

/*
 * FaqController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class FaqController extends Zend_Controller_Action
{

    /**
     * @var Default_Model_Faq
     */
    protected $_modelFaq;

    /**
     * @var array
     */
    protected $_authService;
    protected $_forms = array();

    public function init()
    {
        $this->view->headTitle('Faq', 'PREPEND');
        $this->_modelFaq = new Default_Model_Faq();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
        $this->_authService = new User_Service_Authentication();
    }

    public function indexAction()
    {


        $pageModel = new Default_Model_Page();
        $this->view->faq_page = $pageModel->getPageByType('faq');

        $this->view->headTitle($this->view->faq_page->title, 'PREPEND');
        $description = $this->view->faq_page->meta_description;
        $keywords    = $this->view->faq_page->meta_keywords;


        /* if ('' == $this->_getParam('categoryIdent')) {

          $pageModel = new Default_Model_Page();
          $this->view->faq_page = $pageModel->getPageByType('faq');

          $this->view->headTitle($this->view->faq_page->title, 'PREPEND');
          $description = $this->view->faq_page->meta_description;
          $keywords = $this->view->faq_page->meta_keywords;
          } else { */

        if (null === $faqs = $this->_modelFaq->getFaqsByCategory(0, null, 'sort_order', 1)) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Category not found')) . ' ' . $this->_getParam('categoryIdent'));
        }

        $this->view->faqs = $faqs;

//			 echo "<pre>";
//		  print_r($this->view->categories);
//die;
        //$this->view->faqs = $this->_modelFaq->getFaqsByCategory($category->category_faq_id);
        //$description = $category->description;
        //$keywords = $category->name;
        /* } */




        //$this->view->categories = $this->_modelFaq->getCategoriesFaq();



        $request = $this->getRequest();

        $user_id       = isset($this->_authService->getIdentity()->user_id) ? $this->_authService->getIdentity()->user_id : 0;
        $name          = isset($this->_authService->getIdentity()->firstname) ? $this->_authService->getIdentity()->lastname . ' ' . $this->_authService->getIdentity()->firstname : '';
        $email         = isset($this->_authService->getIdentity()->email) ? $this->_authService->getIdentity()->email : '';
        $populate_data = array('author'  => $name,
            'email'   => $email);
        $faq_form = $this->_getFaqFormAsk();
        $faq_form->populate($populate_data);
        if ($user_id != 0)
            $faq_form->removeElement('captcha');


        $this->view->faqForm = $faq_form;

//        
//        if ($request->isPost()) {
//            if (false !== $this->_modelFaq->suserFaq($request->getPost())) {
//                $this->view->message = $this->view->translate('Спасибо! Ваше сообщение принято.');
//                $this->view->faqForm->reset();
//            } else {
//                $this->view->message = $this->view->translate('Error! Maybe you made a mistake when filling out form.');
//            }
//        }



        $this->view->headTitle($this->view->title, 'PREPEND');
        $this->view->headMeta()->appendName('description', $description);
        $this->view->headMeta()->appendName('keywords', $keywords);
    }

    public function submitAjaxAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not found')));
        }
        $this->_helper->layout->disableLayout();
        $out = array('status' => 'error');
        $data    = $this->_modelFaq->suserFaq($this->getRequest());
        if (is_array($data)) {
            $out['error'] = $data;
        } else {
            $out = array('status' => 'success');
        }
        return $this->_helper->json($out);
    }

    public function listAction()
    {

        $this->view->faq_category_id = (int) $this->_getParam('id', 0);
        $this->view->faqs = $this->_modelFaq->getFaqsByCategory($this->view->faq_category_id);
    }

    public function listCategoriesAction()
    {
        if (!$this->_helper->acl('Admin')) {
            
        }

        $this->view->categories = $this->_modelFaq->getCategoriesFaq();
    }

    public function addFaqAction()
    {
        if (!$this->_helper->acl('Admin')) {
            
        }

        $this->view->faqForm = $this->_getFaqForm('add');
    }

    public function addCategoryAction()
    {
        if (!$this->_helper->acl('Admin')) {
            
        }

        $this->view->categoryForm = $this->_getCategoryForm('add');
    }

    public function editFaqAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Faq not found')) . ' ' . $this->_getParam('id'));
        }

        $faq_id = (int) $this->_getParam('id');

        $this->view->faq = $this->_modelFaq->getFaqById($faq_id);

        $this->view->faqForm = $this->_getFaqForm('edit')->populate($this->view->faq->toArray());
    }

    public function editprivateFaqAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Faq not found')) . ' ' . $this->_getParam('id'));
        }

        $faq_id = (int) $this->_getParam('id');

        $this->view->faq = $this->_modelFaq->getFaqById($faq_id);

        $this->view->faqForm = $this->_getFaqForm('editprivate')->populate($this->view->faq->toArray());
    }

    public function editCategoryAction()
    {
        if (!$this->_helper->acl('Admin')) {
            
        }

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Category not found')) . ' ' . $this->_getParam('id'));
        }

        $category_faq_id = (int) $this->_getParam('id');

        $this->view->category_faq = $this->_modelFaq->getCategoryFaqById($category_faq_id);

        $this->view->categoryForm = $this->_getCategoryForm('edit')->populate($this->view->category_faq->toArray());
    }

    public function saveFaqAction()
    {
        if (!$this->_helper->acl('Admin')) {
            
        }

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }
        if (false === $this->_modelFaq->editprivateFaq($request->getPost(), $type)) {
            $this->view->faqForm = $this->_getFaqForm($type);
            return $this->render($type . '-faq');
        } else {
            if (false === $this->_modelFaq->saveFaq($request->getPost(), $type)) {
                $this->view->faqForm = $this->_getFaqForm($type);
                return $this->render($type . '-faq');
            }
        }
        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Faq added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Faq updated'));
                break;
            case 'editprivate':
                $message = $this->view->translate('Private response has been sent (question removed).');
                break;
            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action'     => 'list',
                    'controller' => 'faq',
                    'module'     => 'default',
                    'id'         => $request->getPost('category_faq_id')), 'admin', true);
    }

    public function saveCategoryAction()
    {
        if (!$this->_helper->acl('Admin')) {
            
        }

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelFaq->saveCategoryFaq($request->getPost(), $type)) {
            $this->view->сategoryForm = $this->_getCategoryForm($type);
            return $this->render($type . '-category');
        }

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action'     => 'list',
                    'controller' => 'faq',
                    'module'     => 'default'), 'admin', true);
    }

    public function deleteFaqAction()
    {
        if (false === ($id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Category not found')) . ' ' . $id);
        }

        $this->_modelFaq->deleteFaq($id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action'     => 'list',
                    'controller' => 'faq',
                    'module'     => 'default'), 'admin', true);
    }

    public function deleteCategoryAction()
    {
        if (false === ($category_faq_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Category not found')) . ' ' . $category_faq_id);
        }

        $this->_modelFaq->deleteCategoryFaq($category_faq_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action'     => 'list',
                    'controller' => 'faq',
                    'module'     => 'default'), 'admin', true);
    }

    protected function _getCategoryForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[$type . 'Category'] = $this->_modelFaq->getForm('FaqCategory' . ucfirst($type));
        $this->_forms[$type . 'Category']->setAction($urlHelper->url(array(
                    'module'     => 'default',
                    'controller' => 'faq',
                    'action'     => 'save-category',
                    'type'       => $type), 'admin'));
        $this->_forms[$type . 'Category']->setMethod('post');

        return $this->_forms[$type . 'Category'];
    }

    protected function _getFaqForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[$type . 'Faq'] = $this->_modelFaq->getForm('Faq' . ucfirst($type));
        $this->_forms[$type . 'Faq']->setAction($urlHelper->url(array(
                    'module'     => 'default',
                    'controller' => 'faq',
                    'action'     => 'save-faq',
                    'type'       => $type), 'admin'));
        $this->_forms[$type . 'Faq']->setMethod('post');

        return $this->_forms[$type . 'Faq'];
    }

    protected function _getFaqFormAsk()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['askFaq'] = $this->_modelFaq->getForm('Faq' . ucfirst('Ask'));
        $this->_forms['askFaq']->setAction('/default/faq/submit-ajax');
        $this->_forms['askFaq']->setMethod('post');

        return $this->_forms['askFaq'];
    }

}