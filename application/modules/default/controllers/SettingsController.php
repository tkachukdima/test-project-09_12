<?php

/**
 * AdminController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class SettingsController extends Zend_Controller_Action
{

    /**
     * @var Default_Model_Settings
     */
    protected $_modelSettings;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelSettings = new Default_Model_Settings();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {

        $_modelSettings = new Default_Model_Settings();

        $this->view->settingsForm = $_modelSettings->getFormWithPopulate('Settings_Settings', 'Default');

        /*
          $module = $this->getRequest()->getParam('setting_module', 'default');

          $this->view->settings = $this->_modelSettings->getSettings($module);

          $this->view->setting_module = $module;

          $this->view->settings_modules = $this->_modelSettings->getSettingsModules(); */
        //print_r($this->view->settings->toArray());die;
    }

    public function sysinfoAction()
    {
        $this->view->layout()->disableLayout();
    }

    public function addAction()
    {
        $this->view->settingForm = $this->_getSettingForm('add');
    }

    public function editAction()
    {

        $menu_id = $this->_getParam('setting_id', false);

        if (!$menu_id) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Setting not found')));
        }

        $menu = $this->_modelSettings->getSettingByIdForEdit((int) $menu_id);

        $this->view->settingForm = $this->_getSettingForm('edit')->populate($menu);
    }

    public function saveAction()
    {

        $request = $this->getRequest();

        $post = $request->getPost();

        /* только для дефолотового модуля */
        $module_name = isset($post['module']) ? ucfirst($post['module']) : '';
        $form_name = 'Settings';
        $controller = 'index';
        $action = 'settings';

        if ($module_name == 'Default') {
            $form_name .= '_Settings';
            $controller = 'settings';
            $action = 'index';
        }

        if (false === $this->_modelSettings->saveSetting($post, $form_name)) {

            $this->view->settingForm = $this->_modelSettings->getForm($form_name, $module_name);

            // todo view (if error)
            return $this->render('edit');
        }

        $this->_helper->FlashMessenger->setNamespace('success')
                ->addMessage($this->view->translate(_('Setting updated')));

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'module' => $post['module'],
                    'controller' => $controller,
                    'action' => $action
                        ), 'admin', false);
    }

    public function listImageResizeSettingsAction()
    {

        $this->view->imageResizeSettings = $this->_modelSettings->getImageResizeSettings();
    }

    public function addImageResizeSettingAction()
    {

        $this->view->imageResizeSettingForm = $this->_getImageResizeSettingForm('add');
    }

    public function editImageResizeSettingAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Setting could not be found')) . ' ' . $this->_getParam('id'));
        }

        $imageResizeSetting = (int) $this->_getParam('id');

        $price = $this->_modelSettings->getImageResizeSettingById($imageResizeSetting);

        $this->view->imageResizeSettingForm = $this->_getImageResizeSettingForm('edit')->populate($price->toArray());
    }

    public function saveImageResizeSettingAction()
    {


        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelSettings->saveImageResizeSetting($request->getPost(), $type)) {
            $this->view->imageResizeSettingForm = $this->_getImageResizeSettingForm($type);
            $this->view->imageResizeSettingForm->setDescription($this->view->translate('Error! Completed form is incorrect.'));
            return $this->render($type . '-image-resize-setting');
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Setting added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Settings updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list-image-resize-settings',
                    'controller' => 'settings',
                    'module' => 'default'), 'admin', true);
    }

    protected function _getSettingForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Settings' . ucfirst($type)] = $this->_modelSettings->getForm('Settings' . ucfirst($type));
        $this->_forms['Settings' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'controller' => 'settings',
                    'action' => 'save',
                    'type' => $type), 'admin'), false);
        $this->_forms['Settings' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Settings' . ucfirst($type)];
    }

    protected function _getImageResizeSettingForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['SettingsImageResize' . $type] = $this->_modelSettings->getForm('SettingsImageResize' . ucfirst($type));
        $this->_forms['SettingsImageResize' . $type]->setAction($urlHelper->url(array(
                    'controller' => 'settings',
                    'action' => 'save-image-resize-setting',
                    'type' => $type), 'admin'), true);
        $this->_forms['SettingsImageResize' . $type]->setMethod('post');

        return $this->_forms['SettingsImageResize' . $type];
    }

}
