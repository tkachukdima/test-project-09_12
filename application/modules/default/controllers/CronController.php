<?php

/*
 * CronController
 *
 * @menu   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class CronController extends Zend_Controller_Action
{

    protected $_key;

    public function init()
    {
        $this->_key = $this->getCronKey();

        $key = $this->getRequest()->getParam('key');
        if ($key !== $this->_key) {
            throw new ARTCMF_Exception($this->view->translate(_('Key is not valid')));
        }
    }

    public function indexAction()
    {
        $this->checkEndingSubscriptions();
        $this->deleteEndingSubscriptions();
    }

    private function checkEndingSubscriptions()
    {

        $subscriptionModel = new User_Model_Subscription();
        $userModel         = new User_Model_User();
        $sender            = new Email_Model_Sender();

        $endingSubscriptions = $subscriptionModel->getEndingSubscriptions(14);

        $site_settings = Zend_Registry::get('site_settings');

        foreach ($endingSubscriptions as $value) {

            $user = $userModel->getUserById($value->user_id);
    
            $user->notified = 1;
            $user->save();
            
            $tplVars = array(
                'site'                  => $_SERVER['HTTP_HOST'],
                'user_id'               => $user->user_id,
                'subscription_date_end' => $value->subscription_date_end
            );

            $params = array(
                'email_from' => $site_settings->getSetting('default', 'email_from'),
                'site_name'  => $site_settings->getSetting('default', 'site_name'),
                'email_to'   => $user->email,
                'reply_to'   => $site_settings->getSetting('default', 'email_from'),
                'subject'    => _("Your subscription comes to an end")
            );
            $sender->sendEmail($params, $tplVars, 'user_ending_subscribtion');
            
        }
        
                
        $this->_helper->layout->disableLayout();
        $this->getHelper('viewRenderer')->setNoRender();
    }
    
    private function deleteEndingSubscriptions()
    {

        $subscriptionModel = new User_Model_Subscription();
        $userModel         = new User_Model_User();
        $sender            = new Email_Model_Sender();

        $endedSubscriptions = $subscriptionModel->getEndedSubscriptions();

        $site_settings = Zend_Registry::get('site_settings');

        foreach ($endedSubscriptions as $value) {

            $user = $userModel->getUserById($value->user_id);
            $user->subscription_id = 0;
            $user->save();
            if ($user->trial == 1) { //конец триала
                $mail_title = _("Test the subscription period is over");
            } elseif ($user->trial == 2) { // конец платной подписки
                $mail_title = _("Subscription is over");
            }
            
            $tplVars = array(
                'site' => $_SERVER['HTTP_HOST'],
                'user_id' => $user->user_id,
                'subscription_date_end' => $value->subscription_date_end
            );

            $params = array(
                'email_from' => $site_settings->getSetting('default', 'email_from'),
                'site_name'  => $site_settings->getSetting('default', 'site_name'),
                'email_to'   => $user->email,
                'reply_to'   => $site_settings->getSetting('default', 'email_from'),
                'subject'    => $mail_title
            );
            $sender->sendEmail($params, $tplVars, 'user_subscribed_end');
            
        }
        
                
        $this->_helper->layout->disableLayout();
        $this->getHelper('viewRenderer')->setNoRender();
    }

    private function getCronKey()
    {

        if (empty($this->_key)) {
            $config = new Zend_Config_Ini(
                            APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

            $settings = $config->toArray();
            $this->_key = $settings['cron']['key'];
        }
        return $this->_key;
    }

}
