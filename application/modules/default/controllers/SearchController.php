<?php

/*
 * SearchController
 *
 * @menu   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class SearchController extends Zend_Controller_Action
{

    protected $_forms = array();
    protected $_modelSearch = null;

    public function init()
    {
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
        $this->_modelSearch = new Default_Model_Search();
    }

    public function indexAction()
    {
        
        $this->_helper->getHelper('layout')->setLayout('layout');
        
        $this->view->title = $this->view->translate(_('Search phrase')) . ' ';
        $this->view->headTitle($this->view->title, 'PREPEND');
        $query = $this->getRequest()->getParam('query', NULL);
        if (!is_null($query)) {
            $char = array("!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "=", "`", "'", "[", "]", "{", "}", "/", "\\", "<", ">", ";", ":");
            $this->view->query = str_replace($char, '', $query);
            $this->view->hits = $this->_modelSearch->search('"' . $this->view->query . '"~4'); //поиск фразы с разбросом не более 4 слова 
            //print_r($this->view->hits); exit;
        }
    }
    
      
    public function updateIndexAction()
    {

        $this->_modelSearch->updateIndex();
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($this->view->translate(_('Index updated!')));
        return $this->getHelper('redirector')->gotoRoute(array(
            'action' => 'index',
            'controller' => 'settings',
            'module' => 'default'), 'admin', true);
    }

}
