<?php

/*
 * IpblockingController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class IpblockingController extends Zend_Controller_Action
{

    /**
     * @var Default_Model_Ipblocking
     */
    protected $_modelIpblocking;
    protected $_forms = array();

    public function init()
    {
        $this->_modelIpblocking = new Default_Model_Ipblocking();
    }

    public function indexAction()
    {
        
    }

    public function listAction()
    {
        $request = $this->getRequest();
        
        $this->view->ipForm = $this->_getForm();

        if ($request->isPost()) {

            if (false === $this->_modelIpblocking->saveIp($request->getPost())) {
                $this->view->ipForm->populate($request->getPost());
            } else {
                $this->view->ipForm->reset();
            }
        }

        $this->view->list = $this->_modelIpblocking->getList($request->getParam('page', 1));
        $this->view->addHelperPath('ARTCMF/view/helper', 'ARTCMF_View_Helper');
        $this->view->pagination_config = array('total_items' => count($this->view->list),
            'items_per_page' => 1,
            'style' => 'digg');

        
    }

    public function deleteAction()
    {
        if (false === ($ip = $this->_getParam('ip', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Ip address not found')));
        }

        $this->_modelIpblocking->deleteIp($ip);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'ipblocking',
                    'module' => 'default'), 'admin', true);
    }

    protected function _getForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['IpblockingAddress'] = $this->_modelIpblocking->getForm('IpblockingAdd');
        $this->_forms['IpblockingAddress']->setAction($urlHelper->url(array(
                    'controller' => 'ipblocking',
                    'action' => 'list'), 'admin'));
        $this->_forms['IpblockingAddress']->setMethod('post');

        return $this->_forms['IpblockingAddress'];
    }

}