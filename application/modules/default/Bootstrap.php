<?php

/**
 * Default_Bootstrap
 *
 * @category   Bootstrap
 * @package    Default_Bootstrap
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Bootstrap extends ARTCMF_Application_Module_Bootstrap
{
    
    public function __construct($application)
    {
        $options = $application->getOptions();
        $this->setOptions($options); 
        
        parent::__construct($application);
    }  

    public function _initModuleResourceAutoloader()
    {
        $this->getResourceLoader()->addResourceTypes(array(
            'modelResource' => array(
                'path' => 'models/resources',
                'namespace' => 'Resource',
            ),
            'modelFilter' => array(
                'path' => 'models/filter',
                'namespace' => 'Filter',
            )
        ));
    }

    /**
     * Setup the database profiling
     */
    protected function _initCache()
    {
        if ($this->hasPluginResource('cache')) {
            $this->bootstrap('cache');
            $cache = $this->getPluginResource('cache')->getDbAdapter();
        }
    }
    
    


}
