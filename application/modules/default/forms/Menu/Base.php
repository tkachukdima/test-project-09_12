<?php

/**
 * Base Menu Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Menu_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );


        $this->setMethod('post');
        $this->setAction('');


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'name_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true
            ));
        }

       $this->addElement('text', 'type', array(
            'label' => _('Type'),
            'description' => _('This is system field. Do not change it!')
        ));

         $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => 1
        ));

        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));

        $this->addElement('submit', 'submit', array(

        ));

        $this->addElement('hidden', 'menu_id', array(
            'filters'    => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper',array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        $this->addDisplayGroup(array('type', 'sort_order',  'status', 'menu_id', 'submit'), 'form_all', array('legend' => _('General Settings')));

    }

}
