<?php

/**
 * Base  Menu_Item Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Menu_Item_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/linc');
        $this->setMethod('post');
        $this->setAction('');




        foreach (Zend_Registry::get('langList') as $lang) {
            $this->addElement('text', 'name_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));
            
            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Title'),
                'filters' => array('StringTrim'),
                'required' => false,
            ));
        }



        // get category select
        $form = new Default_Form_Menu_Select(
                        array('model' => $this->getModel())
        );
        $element = $form->getElement('menu_id');
        $element->clearDecorators()->loadDefaultDecorators();
        $element->setRequired(true);
        $this->addElement($element);


        $form = new Default_Form_Menu_Item_Parent_Select(
                        array('model' => $this->getModel())
        );
        $element_name = $form->getElement('parent_id');
        $this->addElement($element_name, 'parent_id');

        $this->addElement('select', 'uri', array(
            'label' => _('Page'),
            'multiOptions' => $this->getList()
        ));

        $this->addElement('text', 'uri_absolut', array(
            'label' => _('Absolute reference'),
            'description' => _('In this field, if necessary, be made absolute reference http://example.com/'),
        ));

        $this->addElement('file', 'image', array(
            'label' => _('Image'),
            'required' => false,
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg,jpeg,png,gif')),
            ),
        ));


        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => 1
        ));

        $this->addElement('select', 'status', array(
            'label' => _('Status') . ': ',
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));

        $this->addElement('submit', 'submit', array(
        ));

        $this->addElement('hidden', 'menu_item_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        foreach (Zend_Registry::get('langList') as $lang) {
            $this->addDisplayGroup(array(
                'name_' . $lang->code,
                'title_' . $lang->code,
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('menu_id', 'parent_id', 'uri', 'uri_absolut', 'image', 'dropdown', 'sort_order', 'status', 'submit', 'menu_item_id'), 'form_all', array('legend' => _('General Settings')));
    }

    private function getList()
    {
        // формируем список страниц и разделов для выбора в меню

        $options = array();

        $options['/'] = _('Home page');

        // @TODO перенести настройки списка в базу
        $options[_('Modules')] = array(
            '/contact' => _('Contacts')
        );

        if (is_dir(APPLICATION_PATH . '/modules/catalog') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'catalog:index', 'list')) {
            $options[_('Modules')]['/catalog'] = _('Catalog');
        }
        
        if (is_dir(APPLICATION_PATH . '/modules/location') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'location:object', 'list')) {
            $options[_('Modules')]['/location/object'] = _('Objects');
        }

        //$options[_('Modules')]['/faq'] = _('FAQ');
        $options[_('Modules')]['/sitemap'] = _('Site map');

        $modelPage = new Default_Model_Page();
        $pages = $modelPage->getPages();
        foreach ($pages as $key => $value) {
            $options[_('Pages')]['/page/index/pageIdent/' . $value->ident] = $value->title . ' (' . $value->type . ')';
        }

        if (is_dir(APPLICATION_PATH . '/modules/publication') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'publication:management', 'list')) {
            $modelPublication = new Publication_Model_Publication();
            $groups = $modelPublication->getPublicationGroups();
            foreach ($groups as $key => $value) {
                $options[_('Group publications')]['/publication/index/index/groupIdent/' . $value->ident] = $value->name;
            }
        }

        if (is_dir(APPLICATION_PATH . '/modules/file') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'file:index', 'list')) {
            $modelFile = new File_Model_File();
            $groups = $modelFile->getGroups();
            foreach ($groups as $key => $value) {
                $options[_('Files')]['/file/index/index/group_id/' . $value->group_id] = $value->title;
            }
        }
        
        if (is_dir(APPLICATION_PATH . '/modules/gallery') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'gallery:index', 'list')) {
            
            $options[_('Photo Gallery')]['/gallery'] = _('Photo Gallery');
            
            
            
            $albums = $this->getAlbums(0, 0);
            $options[_('Photo Gallery')]['/gallery'] = _('Root level');            
            foreach ($albums as $album) {
                $options[_('Photo Gallery')]['/' . DEFAULT_LANG . '/gallery/album/1/' . $album['ident'] . '.html'] = $album['name'];
            }
        }

        return $options;
    }
    
    private function getAlbums($parent_id, $level = 0)
    {
        $level++;

        $data = array();
        $modelGallery = new Gallery_Model_Gallery();
        $results = $modelGallery->getAlbumsByParentId($parent_id)->toArray();

        foreach ($results as $result) {
            $data[] = array(
                'album_id' => $result['album_id'],
                'ident' => $result['ident'],
                'name' => str_repeat(' - ', $level) . $result['name']
            );

            $children = $this->getAlbums($result['album_id'], $level);

            if ($children) {
                $data = array_merge($data, $children);
            }
        }

        return $data;
    }

}