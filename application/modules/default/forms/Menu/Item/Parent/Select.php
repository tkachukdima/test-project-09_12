<?php

/**
 * Page Type Select
 *
 * @category   Default
 * @package    Default_Form
 * @author     V
 * @license    Commercial License
 */
class Default_Form_Menu_Item_Parent_Select extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setMethod('post');

        $this->addElement('select', 'parent_id', array(
            'label' => _('Select parent menu:'),
            'required' => false,
            'id' => 'combobox',
            //'multiOptions' => array(""=>"-")
            // ajax load
            'multiOptions' => $this->getOptions()
        ));
    }

    private function getOptions()
    {
        $result = array(0 => _("Not select"));

        $_modelMenu = new Default_Model_Menu();
        $menus = $_modelMenu->getAllMenuitem();
             
        foreach ($menus as $key => $item) {
            $result[$item->menu_item_id] = $item->name;
        }

        return $result;
    }

}

