<?php

/**
 * Add new Menu Item
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Menu_Item_Add extends Default_Form_Menu_Item_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
         $this->getElement('menu_item_id')->setRequired(false);
         $this->getElement('submit')->setLabel(_('Add'));
    }

}
