<?php

/**
 * Add new Menu
 *
 * @Album   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Menu_Add extends Default_Form_Menu_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('menu_id')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
