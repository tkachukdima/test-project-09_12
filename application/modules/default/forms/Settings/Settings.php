<?php

/**
 * Base  Photo Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Settings_Settings extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/gallery/photo');

        $this->setMethod('post');
        $this->setAction('/admin/default/settings/save');

       /* foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'name_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));
        }*/

        /*$this->addElement('Checkbox', 'delete_image', array(
            'label' => _('Delete Image')
        ));
        */

        /*$this->addElement('file', 'full', array(
             'label' => _('Image'),
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg,jpeg,png,gif')),
            ),
        ));*/

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'site_name_' . $lang->code, array(
                'belongsTo' => 'ml',
                'label' => _('Site name'),
                'filters' => array('StringTrim'),
                'required' => true
            ));

            $this->addElement('textarea', 'site_slogan_' . $lang->code, array(
                'belongsTo' => 'ml',
                'label' => _('Site slogan'),
                'filters' => array('StringTrim'),
                'rows' => 3,
                'required' => true
            ));

            $this->addElement('textarea', 'copyright_' . $lang->code, array(
                'belongsTo' => 'ml',
                'label' => _('Copyright'),
                'filters' => array('StringTrim'),
                'rows' => 3,
                'required' => true
            ));           
        }

        $this->addElement('text', 'reply_to', array(
            'label' => _('Reply To'),
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required' => true
        ));

        $this->addElement('text', 'email_from', array(
            'label' => _('Email from'),
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required' => true
        ));

        $this->addElement('text', 'email_to', array(
            'label' => _('Email To'),
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required' => true
        ));

        $this->addElement('text', 'public_email', array(
            'label' => _('Public email'),
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required' => true
        ));

        $this->addElement('text', 'watermark', array(
            'label' => _('Watermark'),
            'filters' => array('StringTrim'),
            'required' => false
        ));
                 
        $this->addElement('textarea', 'tracking_code', array(
            'label' => _('Tracking code (Google)'),
            'filters' => array('StringTrim'),
            'required' => false,
            'rows' => 6
        ));

/*
        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            //'belongsTo' => 'element',
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));
*/
        $this->addElement('submit', 'submit', array(
            'label' => _('Save')
        ));

        $this->addElement('hidden', 'module', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay'))),
            'value' => "default"
        ));

        $this->addElement('hidden', 'type', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay'))),
            'value' => "edit"
        ));

        
        
        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'site_name_'    . $lang->code,
                'site_slogan_'  . $lang->code,
                'copyright_'    . $lang->code                
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('reply_to', 'email_from', 'email_to', 'public_email'), 'form_emails', array('legend' => _('Email Settings')));
        
        $this->addDisplayGroup(array('watermark', 'tracking_code', 'submit', 'module', 'type'), 'form_all', array('legend' => _('General Settings')));

    }

}
