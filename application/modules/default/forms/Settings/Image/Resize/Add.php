<?php

/**
 * Add new page post
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Settings_Image_Resize_Add extends Default_Form_Settings_Image_Resize_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
         $this->removeElement('setting_image_resize_id');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
