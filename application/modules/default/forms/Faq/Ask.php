<?php

/**
 * Ask new Faq
 *
 * @category   Default
 * @package    Default_Form
* * @author    V. office.artcreative@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Ask extends ARTCMF_Form_Abstract {

    public function init() {

	   // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );

        $this->setMethod('post');
	$this->setAttrib("id", "comment_form")
             ->setAttrib('onSubmit', 'return sendFaq(this);');

	   // -------
        $this->addElement('text', 'email', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required'   => true,
            'label'      => 'Email',
        ));
        $this->email->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));        

        $this->addElement('text', 'author', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));
        $this->author->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));        

        $this->addElement('textarea', 'query', array(
            'label' => _('Question'),
            'filters' => array('StringTrim'),
            'rows' => 3,
            'required' => true,
        ));
        $this->query->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));        
        //$this->answer->addDecorator(new ARTCMF_Form_Decorator_CKEditor);

	   $this->addElement('captcha', 'captcha', array(
                    'label' => _('Kod'),
                    'captcha' => array(
                        'captcha' => 'Image',
                        'wordLen' => 4,
                        'font' => 'fonts/arial.ttf',
                        'imgDir' => './images/captcha',
                        'imgUrl' => '/images/captcha',
                        'fontsize' => '10',
                        'width' => 70,
                        'height' => 35,
                        'timeout' => 120,
                        'expiration' => 0,
                        'imgAlt' => 'captcha',
                        'DotNoiseLevel' => 2,
                        'LineNoiseLevel' => 1
                    )
            )
        );
        $this->captcha->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));        
        
	   $this->addElement('submit', 'submit', array(
            'label'    => _('Send'),
            'decorators' => array('ViewHelper', array('HtmlTag', array('tag' => 'dd','class' => 'clr'))),
        ));

           $this->addElement('hash', 'no_csrf', array(
            'salt' => 'unique',
            'timeout' => 300,
            'ignore' => false,
            'required' => true,
        ));
        $this->no_csrf->removeDecorator('DtDdWrapper')->removeDecorator('Label');
    }

}
