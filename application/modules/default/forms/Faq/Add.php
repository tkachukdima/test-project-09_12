<?php

/**
 * Add new Faq
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Add extends Default_Form_Faq_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->removeElement('faq_id');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
