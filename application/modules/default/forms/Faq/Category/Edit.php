<?php

/**
 * Edit category
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Category_Edit extends Default_Form_Faq_Category_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
