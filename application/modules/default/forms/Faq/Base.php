<?php

/**
 * Base  Faq Form
 *
 * @category   Default
 * @package    Default_Form
* * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );

        $this->setMethod('post');
        $this->setAction('');
	   
        
        
        // get category select
        $form = new Default_Form_Faq_Category_Select(
            array('model' => $this->getModel())
        );
        $element = $form->getElement('category_faq_id');
        $element->clearDecorators()->loadDefaultDecorators();
        $element->setRequired(true);
        $this->addElement($element);
        
                              
        $this->addElement('text', 'email', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')                
            ),
            'required'   => false,
            'label'      => 'Email',
        ));   

        $this->addElement('text', 'author', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => false,
        ));
                           
         
        $this->addElement('textarea', 'query', array(
            'label' => _('Query'),
            'filters' => array('StringTrim'),
            'rows' => 3,
            'required' => true,
        ));
        
        
        $this->addElement('textarea', 'answer', array(
            'label' => _('Answer'),
            'filters' => array('StringTrim'),
            'rows' => 10,
            'required' => true,
            'class' => "ckeditor"
        ));
        
        //$this->answer->addDecorator(new ARTCMF_Form_Decorator_CKEditor);
        
	$this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));

       $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));
        
        $this->addElement('submit', 'submit', array(
            
        ));

        $this->addElement('hidden', 'faq_id', array(
            'filters'    => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper',array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
               
    }

}