<?php

/**
 * Add new page post
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Page_Add extends Default_Form_Page_Base
{

    public function init()
    {

        //call the parent init
        // parent::init();
        // $this->removeElement('delete_image');
        //$this->removeElement('page_id');
        // $this->removeElement('menu_item_id');

        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/page');


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));
        }

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $element_name = 'body_' . $lang->code;
            $this->addElement('textarea', $element_name, array(
                'label' => _('Body'),
                'filters' => array('StringTrim'),
                'required' => true,
                'class' => "ckeditor"
            ));
            //$this->$element_name->addDecorator(new ARTCMF_Form_Decorator_CKEditor);
        }


        $this->addElement('file', 'full', array(
            'label' => _('Image'),
            'required' => false,
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg', 'jpeg', 'png', 'gif')),
            ),
        ));

        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive'),
            )
        ));

        // get the select
        $form = new Default_Form_Page_Type_Select(
                        array('model' => $this->getModel())
        );

        $element_name = $form->getElement('type');
        $this->addElement($element_name, 'type');

        $this->addElement('text', 'ident', array(
            'label' => _('Character code'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(
                array('UniqueIdent', true, array($this->getModel(), 'getPageByIdent', 'getPageById', 'page_id'))
            ),
            'required' => false,
        ));

        foreach (Zend_Registry::get('langList') as $key => $lang) {

            $this->addElement('textarea', 'page_title_' . $lang->code, array(
                'label' => _('Page Title'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 2,
                'required' => false
            ));

            $this->addElement('textarea', 'meta_description_' . $lang->code, array(
                'label' => _('META Description'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true
            ));

            $this->addElement('textarea', 'meta_keywords_' . $lang->code, array(
                'label' => _('META Keywords'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true,
            ));

            $this->addElement('text', 'description_img_' . $lang->code, array(
                'label' => _('Description of the image'),
                'filters' => array('StringTrim'),
                'required' => false,
            ));
        }



        $this->addElement('hidden', 'date_post', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => date('Y-m-d H:i:s'),
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        //------------------------------------------------------
        $this->addElement('Checkbox', 'check_menu', array(
            'label' => _('Create a menu item'),
        ));


        // get the select menu
        $form = new Default_Form_Page_Menu_Select(
                        array('model' => $this->getModel())
        );

        $element_name = $form->getElement('menu_item');
        $this->addElement($element_name, 'menu_item');



        $this->addElement('text', 'sort_menu', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'validators' => array('Int'),
            'required' => false,
        ));



        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'title_' . $lang->code,
                'body_' . $lang->code,
                'cart_title_' . $lang->code,
                'cart_' . $lang->code,
                'page_title_' . $lang->code,
                'meta_description_' . $lang->code,
                'meta_keywords_' . $lang->code,
                'description_img_' . $lang->code,
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }

        $this->addElement('submit', 'submit', array(
            'label' => _('Add')
        ));

        $this->addDisplayGroup(array('delete_image', 'full', 'check_menu', 'menu_item', 'sort_menu', 'status', 'type', 'ident', 'submit', 'page_id', 'date_post'), 'form_all', array('legend' => _('General Settings')));
    }

}

