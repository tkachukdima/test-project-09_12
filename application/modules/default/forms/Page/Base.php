<?php

/**
 * Base Page Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Page_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );
        
        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $this->setMethod('post');
        $this->setAction('');
        
        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));
        }
        
        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $element_name = 'body_' . $lang->code;
            $this->addElement('textarea', $element_name, array(
                'label' => _('Full Text'),
                'filters' => array('StringTrim'),
                'required' => true,
                'class' => "ckeditor"                
            ));            
            //$this->$element_name->addDecorator(new ARTCMF_Form_Decorator_CKEditor);
        }
                       
        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));

        // get the select
        $form = new Default_Form_Page_Type_Select(
                        array('model' => $this->getModel())
        );
        $element_name = $form->getElement('type');
        $this->addElement($element_name, 'type');

        $this->addElement('text', 'ident', array(
            'label' => _('Character code'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(
                array('UniqueIdent', true, array($this->getModel(), 'getPageByIdent', 'getPageById', 'page_id'))
            ),
            'required' => false,
        ));
        
      
        
        foreach (Zend_Registry::get('langList') as $key => $lang) {
            
            $this->addElement('textarea', 'page_title_' . $lang->code, array(
                'label' => _('Page Title'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 2,
                'required' => false
            ));
            
             $this->addElement('textarea', 'meta_description_' . $lang->code, array(
                'label' => _('META Description'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => false
            ));

            $this->addElement('textarea', 'meta_keywords_' . $lang->code, array(
                'label' => _('META Keywords'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => false,
            ));
            
            $this->addElement('text', 'description_img_' . $lang->code, array(
            'label' => _('Description of the image'),
            'filters' => array('StringTrim'),
            'required' => false,
            ));
            
            
 
            $this->addElement('submit', 'submit', array(
                )); 
        }
         

       
       

        $this->addElement('hidden', 'page_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        $this->addElement('hidden', 'date_post', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => date('Y-m-d H:i:s'),
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
        
        
        
        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'title_' . $lang->code,                
                'body_' . $lang->code,
                'cart_title_' . $lang->code,
                'cart_' . $lang->code,
                'page_title_' . $lang->code,
                'meta_description_' . $lang->code,
                'meta_keywords_' . $lang->code,
                'description_img_' . $lang->code,
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }
        
      
            
              
        
        $this->addDisplayGroup(array('status', 'type',  'ident', 'submit', 'page_id', 'date_post'), 'form_all', array('legend' => _('General Settings')));
    }


}
