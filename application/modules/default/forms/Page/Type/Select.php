<?php

/**
 * Page Type Select
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Page_Type_Select extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setMethod('post');


        $this->addElement('select', 'type', array(
            'label' => _('Select page type'),
            'required' => true,
            'multiOptions' => $this->getOptions()
        ));
    }

    private function getOptions()
    {
        $result = array();

        $result['static'] = _('Static page');
        $result['main'] = _('Home page');
        $result['about'] = _('About us') . ' ' . _('page');
        $result['seo_soiskatelyam'] = _('Seo_soiskatelyam');
        $result['seo_rabotodatelyam'] = _('Seo_rabotodatelyam');

        // Эти условия - временная мера. Надо сделать "Менеджер модулей"
        if ($this->getModel()->getAcl()->has('contact:index') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'contact:index', 'settings')) {
            $result['contact'] = _('Contact') . ' ' . _('page');
        }

        if ($this->getModel()->getAcl()->has('catalog:index') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'catalog:index', 'list')) {
            $result['manufacturer'] = _('Manufacturers') . ' ' . _('page');
        }
        
        if ($this->getModel()->getAcl()->has('catalog:index') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'catalog:index', 'list')) {
            $result['catalog'] = _('Catalog') . ' ' . _('index page');
        }

        if ($this->getModel()->getAcl()->has('gallery:index') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'gallery:index', 'list')) {
            $result['gallery'] = _('Photo Gallery') . ' ' . _('page');
        }

        if ($this->getModel()->getAcl()->has('location:object') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'location:object', 'list')) {
            $result['object'] = _('Objects') . ' ' . _('page');
        }

        if ($this->getModel()->getAcl()->has('location:representation') AND $this->getModel()->getAcl()->isAllowed($this->getModel()->getIdentity()->role, 'location:representation', 'list')) {
            $result['representation'] = _('Representations') . ' ' . _('page');
        }

        return $result;
    }

}

