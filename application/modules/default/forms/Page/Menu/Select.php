<?php

/**
 * Page Type Select
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Page_Menu_Select extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setMethod('post');

        $menu = new Default_Model_Menu();
        $items = array();

        $menus = $menu->getMenus()->toArray();
        foreach ($menus as $menu) {
            $items[$menu['menu_id']] = $menu['name'];
        }

        $this->addElement('select', 'menu_item', array(
            'label' => _('Choose'),
            //'decorators' => array('ViewHelper', 'Label'),
            'multiOptions' => $items
        ));
    }

}
