<?php
/**
 * The base search form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Search_Base extends ARTCMF_Form_Abstract
{
    public function init()
    {
        $this->setName('searchForm');
        
        $this->addElement('text', 'query', array(
            'filters'    => array('StringTrim'),
            'required'   => true,
            'label'      => _('Search'),
            'value'      => _('Search'),
            'onclick' => "this.value='';",
            'decorators' => array('ViewHelper',array('HtmlTag', array('tag' => 'dd')))
        ));
/*
        $this->addElement('text', 'pricefrom', array(
            'filters'    => array('StringTrim'),
            'required'   => false,
            'label'      => 'Price: ',
            'decorators' => array('ViewHelper', 'Label'),
            'style'      => 'width: 50px; float: left;'
        ));
        $this->addElement('text', 'priceto', array(
            'filters'    => array('StringTrim'),
            'required'   => false,
            'label'      => 'to',
            'decorators' => array('ViewHelper', array('Label', array('style' => 'padding: 5px;'))),
            'style'      => 'width: 50px; float: left;'
        ));
        $this->addDisplayGroup(array('pricefrom','priceto'), 'prange', array(
            'decorators' => array('FormElements', array('HtmlTag', array('tag' => 'dd', 'class' => 'clearfix'))),
        ));
        */
        $this->addElement('image', 'search', array(
            'required' => false,
            'ignore'   => true,
            'class' => 'btn',
            'src' => '/layout/search_btn.jpg',
            'decorators' => array('ViewHelper',array('HtmlTag', array('tag' => 'dd', 'id' => 'form-submit')))
        ));
    }
}
