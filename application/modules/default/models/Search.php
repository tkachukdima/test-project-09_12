<?php

class Default_Model_Search extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    protected $_searchIndexPath; //path to initial data folder
    protected $_db;    //database adapter

    /**
     * Создает новый поисковой индекс
     */

    public function updateIndex()
    {
        if (!$this->checkAcl('updateIndex')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        setlocale(LC_ALL, 'ru_RU.UTF-8');

        $this->_searchIndexPath = APPLICATION_PATH . '/../data/search-index';
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
        set_time_limit(900);
        Zend_Search_Lucene_Analysis_Analyzer::setDefault(
                new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive());

        //удаляем существующий индекс, в большинстве случае эта операция с последующий созданием нового индекса работает гораздо быстрее
        $this->recursive_remove_directory($this->_searchIndexPath, TRUE);

        try {
            $index = Zend_Search_Lucene::create($this->_searchIndexPath);
        } catch (Zend_Search_Lucene_Exception $e) {
            echo "<p class=\"ui-bad-message\">Error: {$e->getMessage()}</p>";
        }

        try {                                           
            
            foreach ($this->getAvalibleModelsList() as $modelClass) {
                $modelObject = new $modelClass;
                $searchData = $modelObject->getSearchData();
                foreach ($searchData as $row) {
                    $doc = new Zend_Search_Lucene_Document();
                    $doc->addField(Zend_Search_Lucene_Field::Keyword('url', $row['url']));
                    $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['title'], 'UTF-8'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['body'], 'UTF-8'));
                    $doc->addField(Zend_Search_Lucene_Field::Keyword('lang', $row['lang']));
                    $index->addDocument($doc);
                }                
            }            
        } catch (Zend_Search_Lucene_Exception $e) {
            echo "<p class=\"ui-bad-message\">Error: {$e->getMessage()}</p>";
        }

        //let's clean up some
        $index->optimize();
    }

    /**
     * recursive_remove_directory( directory to delete, empty )
     * expects path to directory and optional TRUE / FALSE to empty
     *
     * @param $directory
     * @param $empty TRUE - just empty directory
     */
    protected function recursive_remove_directory($directory, $empty = FALSE)
    {
        if (substr($directory, -1) == '/') {
            $directory = substr($directory, 0, -1);
        }
        if (!file_exists($directory) || !is_dir($directory)) {
            return FALSE;
        } elseif (is_readable($directory)) {
            $handle = opendir($directory);
            while (FALSE !== ($item = readdir($handle))) {
                if ($item != '.' && $item != '..') {
                    $path = $directory . '/' . $item;
                    if (is_dir($path)) {
                        $this->recursive_remove_directory($path);
                    } else {
                        unlink($path);
                    }
                }
            }
            closedir($handle);
            if ($empty == FALSE) {
                if (!rmdir($directory)) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    /**
     * Search by query
     *
     * @param $query search query
     * @return array Zend_Search_Lucene_Search_QueryHit
     */
    public function search($query)
    {

        setlocale(LC_ALL, 'ru_RU.UTF-8');

        $this->_searchIndexPath = APPLICATION_PATH . '/../data/search-index';
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
        set_time_limit(900);
        Zend_Search_Lucene_Analysis_Analyzer::setDefault(
                new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive());
        Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding('utf-8');
                
        try {
            $index = Zend_Search_Lucene::open($this->_searchIndexPath);
        } catch (Zend_Search_Lucene_Exception $e) {
            echo "Error:{$e->getMessage()}";
        }
        
        
        $userQuery = Zend_Search_Lucene_Search_QueryParser::parse($query, 'utf-8');
      
        $results = $index->find($userQuery);
        $data = array();
        foreach ($results as $key => $result) {
            $data[$key]["url"] = $result->url;
            $data[$key]["title"] = $result->title;
            $data[$key]["body"] = $userQuery->htmlFragmentHighlightMatches(strip_tags($result->body), 'utf-8');
            //$data[$key]["body"] = $result->body;
        }
        return $data;
    }
    
    
    private function getAvalibleModelsList()
    {
        $avalibleModels = array();

        $modules = Zend_Controller_Front::getInstance()->getControllerDirectory();
     
        foreach ($modules as $module => $dir) {            
            foreach (glob($dir . "/../models/*.php") as $filename) {
                $modelFile = basename($filename, '.php');
                $modelClass = ucfirst($module) . '_Model_' . ucfirst($modelFile);
                if (method_exists($modelClass, 'getSearchData')) {
                    $avalibleModels[] = $modelClass;
                }
            }                        
        }
        return $avalibleModels;  
    }
    
    
    
    
    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Search';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}