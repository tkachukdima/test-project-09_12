<?php

class Default_Model_Faq extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get categories Faq
     *
     * @param int $parentID The parent_id
     * @return Zend_Db_Table_Rowset
     */
    public function getCategoriesFaqByParentId($parentID)
    {
        $parentID = (int) $parentID;

        return $this->getResource('Categoryfaq')->getCategoriesFaqByParentId($parentID);
    }

    /**
     * Get all categories
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getCategoriesFaq()
    {
        return $this->getResource('Categoryfaq')->getCategoriesFaq();
    }

    /**
     * Get the category item by its id
     *
     * @param int $id
     * @return Default_Resource_Category_Item|null
     */
    public function getCategoryFaqById($id)
    {
        return $this->getResource('Categoryfaq')->getCategoryFaqById($id);
    }

    /**
     * Get Category yFaq by ident
     *
     * @param string $ident The ident string
     * @return Default_Resource_Category_Item|null
     */
    public function getCategoryFaqByIdent($ident, $ignoreRow=null)
    {
        return $this->getResource('Categoryfaq')->getCategoryFaqByIdent($ident, $ignoreRow);
    }

    /**
     * Get a categories parents
     * 
     * @param Default_Resource_Category_Item $category
     * @param boolean Append the parent to the cats array?
     * @return array
     */
    public function getParentCategoriesFaq($category, $appendParent = true)
    {
        $cats = $appendParent ? array($category) : array();

        if (0 == $category->parent_id) {
            return $cats;
        }

        $parent = $category->getParentCategoryFaq();
        $cats[] = $parent;

        if (0 != $parent->parent_id) {
            $cats = array_merge($cats, $this->getParentCategoriesFaq($parent, false));
        }

        return $cats;
    }
    
    public function suserFaq($request) {
        $out = array();  
        $data = $request->getPost();
        $validator = $this->getForm('Faq' . ucfirst('ask'));
        $auth = Zend_Auth::getInstance();  
        if ($auth->hasIdentity()) {
            $this->authInfo = $auth->getIdentity();
        }

        if (isset($this->authInfo->user_id))
        {
           $validator->removeElement('captcha');
        }
                
        if (!$validator->isValid($data)) {
            return $validator->getMessages();
        }

        // get post data
        $data = $validator->getValues();

        $data['category_faq_id'] = 8;

        $faq = array_key_exists('faq_id', $data) ?
                $this->getResource('Faq')->getFaqById($data['faq_id']) : null;

       $this->getResource('Faq')->saveRow($data, $faq);
       return true;
    }

    /**
     * Save a category
     * 
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveCategoryFaq($data, $validator = null)
    {
        if (!$this->checkAcl('saveCategoryFaq')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('FaqCategory' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        if ($data['ident'] == '') {
            $filter = new ARTCMF_Filter_Ident();
            $data['ident'] = $filter->filter($data['name']);
        }

        $category_faq = array_key_exists('category_faq_id', $data) ?
                $this->getResource('Categoryfaq')->getCategoryFaqById($data['category_faq_id']) : null;

        return $this->getResource('Categoryfaq')->saveRow($data, $category_faq);
    }

    public function deleteCategoryFaq($category_faq)
    {
        if (!$this->checkAcl('deleteCategoryFaq')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if ($category_faq instanceof Default_Resource_CategoryFaq_Item) {
            $category_id = (int) $category_faq->category_faq_id;
        } else {
            $category_id = (int) $category_faq;
        }

        $category_faq = $this->getCategoryFaqById($category_id);

        if (1 == $category_faq->category_faq_id)
            return false;

        $category_faq->delete();

        return true;
    }

    /**
     * Get all categories tree for admin panel
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getCategoriesTreeAdm($is_help = false)
    {
        $row_set = $this->getResource('Categoryfaq')->getCategoriesForTree()->toArray();

        $tree = array(0 => array('category_faq_id' => 0, 'parent_id' => $is_help ? 1 : 0, 'value' => 'root'));
        $temp = array(0 => &$tree[0]);
        foreach ($row_set as $val) {
                      
            
            $parent = &$temp[$val['parent_id']];

            if (!isset($parent['pages'])) {
                $parent['pages'] = array();
            }
            
            if ($val['category_faq_id'] == 1 AND !$is_help AND $this->getIdentity()->role == 'Redactor')                
                continue;
                                       
                       
            $parent['pages'][$val['category_faq_id']]['category_faq_id'] = $val['category_faq_id'];
            $parent['pages'][$val['category_faq_id']]['id'] = 'category-' . $val['category_faq_id'];
            $parent['pages'][$val['category_faq_id']]['parent_id'] = $val['parent_id'];
            $parent['pages'][$val['category_faq_id']]['label'] = $val['name'];
            $parent['pages'][$val['category_faq_id']]['title'] = $val['name'];
            $parent['pages'][$val['category_faq_id']]['module'] = 'default';
            $parent['pages'][$val['category_faq_id']]['controller'] = $is_help ? 'admin' : 'faq';
            $parent['pages'][$val['category_faq_id']]['action'] = $is_help ? 'help' : 'list';
            $parent['pages'][$val['category_faq_id']]['route'] = 'admin';
            $parent['pages'][$val['category_faq_id']]['params'] = array('id' => $val['category_faq_id']);

            if (isset($val['pages']))
                $parent['pages'][$val['category_faq_id']]['pages'] = $val['pages'];

            
             
            $temp[$val['category_faq_id']] = &$parent['pages'][$val['category_faq_id']];
        }
        unset($row_set, $temp, $val, $parent);
      //  print_r($tree[0]['pages']);exit;
        if (!isset($tree[0]['pages']))
            return null;

        return $is_help ? array($tree[0]['pages'][1]) : $tree[0]['pages'];
    }

    /**
     * Get all categories
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getFaqs()
    {
        return $this->getResource('Faq')->getFaqs();
    }

    /**
     * Get the category item by its id
     *
     * @param int $id
     * @return Default_Resource_Category_Item|null
     */
    public function getFaqById($id)
    {
        return $this->getResource('Faq')->getFaqById($id);
    }

    /**
     * Get products in a category
     *
     * @param int|string  $category The category name or id
     * @param int|boolean $paged    Whether to page results
     * @param array       $order    Order results
     * @param boolean     $deep     Get all products below this category?
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getFaqsByCategory($category_id, $paged=false, $order=null, $status = 0)
    {
        return $this->getResource('Faq')->getFaqsByCategory($category_id, $paged, $order, $status);
    }

    /**
     * Save a product image
     * 
     * @param Default_Resource_Product_Item $product
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function editprivateFaq($data, $validator = null)
    {
        if (!$this->checkAcl('saveFaq')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'editprivate';
        }
        $validator = $this->getForm('Faq' . ucfirst($validator));        
        
        
        if (!$validator->isValid($data)) {
            return false;
        }        
        
        $site_settings = Zend_Registry::get('site_settings');
        $mail_tpl = 'answer_question_private';            
        $tplVars = array(
            'site' => $_SERVER['HTTP_HOST'],
            'message'=>$data['answer']
        );                

        $params = array(
            'email_from' => $site_settings->getSetting('default', 'email_from'),
            'site_name' => $site_settings->getSetting('default', 'site_name'),
            'email_to' => $data['email'],
            'reply_to' => $site_settings->getSetting('default', 'reply_to'),
            'subject' => _("The answer to your question on the website %hostname%")
        );  
//        $sender = new Email_Model_Sender();
//        $sender->sendEmail($params, $tplVars, $mail_tpl);
//        $this->deleteFaq($data['faq_id']);
    }

    public function saveFaq($data, $validator = null)
    {
        if (!$this->checkAcl('saveFaq')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('Faq' . ucfirst($validator));        
        
        
        if (!$validator->isValid($data)) {
            return false;
        }

        // get post data
        $data = $validator->getValues();
        $faq = array_key_exists('faq_id', $data) ?
                $this->getResource('Faq')->getFaqById($data['faq_id']) : null;
        
        //Отправляем письмо с ответом для человека которій задал вопрос
        if (!is_null($faq))
        {
            if ($faq->status == 0 AND $data['status'] == 1)
            {
                $site_settings = Zend_Registry::get('site_settings');
                $mail_tpl = 'answer_question';            
                $tplVars = array(
                    'site' => $_SERVER['HTTP_HOST'],
                );                

                $params = array(
                    'email_from' => $site_settings->getSetting('default', 'email_from'),
                    'site_name' => $site_settings->getSetting('default', 'site_name'),
                    'email_to' => $data['email'],
                    'reply_to' => $site_settings->getSetting('default', 'reply_to'),
                    'subject' => "Опубликован ответ на Ваш вопрос на сайте %hostname%"
                );  
//                $sender = new Email_Model_Sender();
//                $sender->sendEmail($params, $tplVars, $mail_tpl);
            }
        }
        
        //письмо отправлено

        return $this->getResource('Faq')->saveRow($data, $faq);
    }

    public function deleteFaq($faq)
    {
        if (!$this->checkAcl('deleteCategoryFaq')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if ($faq instanceof Default_Resource_Faq_Item) {
            $faq_id = (int) $faq->faq_id;
        } else {
            $faq_id = (int) $faq;
        }

        $faq = $this->getFaqById($faq_id);


        $faq->delete();
        return true;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Faq';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
