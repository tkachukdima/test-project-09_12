<?php

class Default_Model_Page extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get a page post by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Page_Item
     */
    public function getPageById($id)
    {
        $id = (int) $id;
        return $this->getResource('Page')->getPageById($id);
    }

    /**
     * Get a Page by its id for edit
     *
     * @param  int $id The id
     * @return Default_Resource_Page_Item
     */
    public function getPageByIdForEdit($id)
    {
        $id = (int) $id;

        $page = $this->getResource('Page')->getPageByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['title_' . $value['language_code']] = $value['title'];
            $data['body_' . $value['language_code']] = $value['body'];
            $data['page_title_' . $value['language_code']] = $value['page_title'];
            $data['meta_description_' . $value['language_code']] = $value['meta_description'];
            $data['meta_keywords_' . $value['language_code']] = $value['meta_keywords'];
            $data['description_img_' . $value['language_code']] = $value['description_img'];
        }

        return $data;
    }

    /**
     * Get page post by ident
     *
     * @param string $ident The ident string
     * @return Default_Resource_Page_Item|null
     */
    public function getPageByIdent($ident, $ignoreRow = null)
    {
        return $this->getResource('Page')->getPageByIdent($ident, $ignoreRow);
    }

    /**
     * Get page post by ident
     *
     * @param string $ident The ident string
     * @return Default_Resource_Page_Item|null
     */
    public function getPageByType($type)
    {
        return $this->getResource('Page')->getPageByType($type);
    }

    public function getPagesByType($type)
    {
        return $this->getResource('Page')->getPagesByType($type);
    }

    /**
     * Get page
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getPages($paged = null, $limit = null, $per_page = 20)
    {
        return $this->getResource('Page')->getPages($paged, $limit, $per_page);
    }
        
    /**
     * Save a page post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function savePage($data, $validator = null)
    {
        if (!$this->checkAcl('savePage')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('Page' . ucfirst($validator));

        $lang = Zend_Registry::get('langList');

        if ($data['ident'] == '') {
            $filter = new ARTCMF_Filter_Ident();
            $data['ident'] = $filter->filter($data['title_' . $lang[0]->code]);
        }

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $data['last_edit_id'] = $this->getIdentity()->user_id;

        $page = array_key_exists('page_id', $data) ?
                $this->getResource('Page')->getPageById($data['page_id']) : null;

        if(null == $page){
            $data['author_id'] = $this->getIdentity()->user_id;
        }

        /* Проверка  Создания меню */
        if ($data['check_menu']) {

            $menu_d = array(
                'menu_id' => $data['menu_item'],
                'parent_id' => 0,
                'dropdown' => 0,
                'uri' => '/page/index/pageIdent/' . $data['ident'],
                'status' => 1,
                'sort_order' => $data['sort_menu'],
            );

            //print_r($menu_d);die();

            $menu_item_id = $this->getResource('Menuitem')->saveRow($menu_d, null);

            foreach (Zend_Registry::get('langList') as $lang) {

                $m_translate_data = array(
                    'menu_item_id' => (int) $menu_item_id,
                    'name' => $data['title_' . $lang->code],
                    'title' => $data['title_' . $lang->code],
                    'language_code' => $lang->code,
                );



                $this->getResource('Menuitem')->saveTranslatedRows($m_translate_data, 'insert');
            }
        }
        /* Проверка  Создания меню */

        $newId = $this->getResource('Page')->saveRow($data, $page);
 
        $this->getResource('Page')->deleteTranslatedRows($newId);
        
        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'page_id' => (int) $newId,
                'language_code' => $lang->code,
                'title' => $data['title_' . $lang->code],
                'body' => $data['body_' . $lang->code],
                'page_title' => '' != $data['page_title_' . $lang->code] ? $data['page_title_' . $lang->code] : $data['title_' . $lang->code],
                'meta_description' => '' != $data['meta_description_' . $lang->code] ? $data['meta_description_' . $lang->code] : $data['title_' . $lang->code],
                'meta_keywords' => '' != $data['meta_keywords_' . $lang->code] ? $data['meta_keywords_' . $lang->code] : $data['title_' . $lang->code],
                'description_img' => '' != $data['description_img_' . $lang->code] ? $data['description_img_' . $lang->code] : $data['title_' . $lang->code],
            );

            $this->getResource('Page')->saveTranslatedRows($translate_data, 'insert');
        }

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('page')
        );

        return $newId;
    }

    public function deletePage($id)
    {
        if (!$this->checkAcl('deletePage')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $page = $this->getPageById($id);
        if (null !== $page) {           
            $this->getResource('Page')->deleteTranslatedRows($id);
            $page->delete();
            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('page')
            );
            return true;
        }

        return false;
    }
    
    /**
     * Return data for Search module
     *   
     * @return array Keys: url, title, body
     */
    
    public function getSearchData()
    {
        $searchData = array();

        foreach (Zend_Registry::get('langList') as $lang) {

            //выбираем все Страницы  из БД
            $rows = $this->getResource('Page')->getSearchData($lang->code);
            if (!is_null($rows)) {
                foreach ($rows as $row) {
                    array_push($searchData, array(
                        'url' => '/' . $lang->code . '/page/' . $row->ident . '.html',
                        'title' => $row->title,
                        'body' => $row->body,
                        'lang' => $lang->code
                    ));
                }
            }
        }

        return $searchData;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Page';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
