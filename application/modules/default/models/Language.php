<?php

class Default_Model_Language extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get a language post by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Language_Item
     */
    public function getLanguageById($id)
    {
        $id = (int) $id;
        return $this->getResource('Language')->getLanguageById($id);
    }

    /**
     * Get language
     *
     * @param int|boolean   $languaged    Whether to language results
     * @param integer|null  $limit    Order results
     * @param integer       $per_language    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getLanguages()
    {
        return $this->getResource('Language')->getLanguages();
    }
    

    /**
     * Save a language post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveLanguage($data, $validator = null)
    {
        if (!$this->checkAcl('saveLanguage')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }


        $validator = $this->getForm('Language' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $language = array_key_exists('language_id', $data) ?
                $this->getResource('Language')->getLanguageById($data['language_id']) : null;



        return $this->getResource('Language')->saveRow($data, $language);
    }

    public function deleteLanguage($id)
    {
        if (!$this->checkAcl('deleteLanguage')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $language = $this->getLanguageById($id);

        if (null !== $language) {

            $language->delete();
            return true;
        }

        return false;
    }

    public function moveLanguageItem($id, $move)
    {
        $language_item = $this->getLanguageById($id);

        $near_sort = $this->getResource('Language')->getNearSort($language_item->sort_order, $move);

        if (count($near_sort) != 0) {

            $old_sort_order = $language_item->sort_order;
            $language_item->sort_order = $near_sort[0]->sort_order;
            $language_item->save();


            $language_item_near = $this->getLanguageById($near_sort[0]->language_id);

            $language_item_near->sort_order = $old_sort_order;
            $language_item_near->save();
        }
        return $language_item;
    }

    public function setNewSortOrder($id, $sort_order)
    {
        foreach ($id as $key => $id) {
            $language_item = $this->getLanguageById($id);
            $language_item->sort_order = (int) $sort_order[$key];
            $language_item->save();
        }

        return $language_item;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Language';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
