<?php

class Default_Model_Settings extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    public static function getAllModules()
    {
        return array(
            'default' => _('Default module'), 
            'catalog' => _('Catalog module'), 
            'payment' => _('Payment module'),            
        );
    }
    
    public static function getModuleName($module)
    {
        $modules = self::getAllModules();

        $module_name = '';
        if (array_key_exists($module, $modules)) {
            $module_name = $modules[$module];
        }

        return $module_name;
    }


    public function getFormWithPopulate($nform, $module){

        $form = $this->getForm($nform, $module);

        $data = array();
        $elements = $form->getValues();

        if(isset($elements['ml']))
            $elements = $elements + $elements['ml'];
        
        foreach($elements as $key => $val){
            if ($key == 'module' || $key == 'type' || $key == 'ml' || $key == 'submodule') continue;
            $setting = $this->getSettingByKey($key, $elements['module']);
            
            if(!is_null($setting))
                $value = $setting->value;
            else {
                
                /* @TODO:
                 *  - Auto Insert - if not Exist record         = ITS WORK ! 
                 *  - Auto Delete - if not Exist form field
                 */
                //$sql = sprintf("INSERT INTO `settings` (`key`, `module`) VALUES ('" . $key . "', '" . $elements['module'] . "');");
                $value = '#';//"SQL#". $sql;
            }
            $data[$key] = $value;
        }

        return $form->populate($data);
    }

    /**
     * Get a setting post by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Setting_Item
     */
    public function getSettingById($id)
    {
        $id = (int) $id;
        return $this->getResource('Settings')->getSettingById($id);
    }

    public function getSettingByKey($key, $module){
        return $this->getResource('Settings')->getSettingByKey($key, $module);
    }

    /**
     * Get all setting by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Setting_Item
     */
    public function getSettings($module = null)
    {
        return $this->getResource('Settings')->getSettings($module);
    }
    
    /**
     * Get all setting by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Setting_Item
     */
    public function getSettingsModules()
    {
        return $this->getResource('Settings')->getSettingsModules();
    }
    
    /**
     * Get all setting by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Setting_Item
     */
    public function getAllSettings()
    {
        //$settigs = $this->getResource('Contactsetting')->getAllContactSettingById(1)->toArray();
        
        $system_settings = $this->getResource('Settings')->getSettings();

        foreach ($system_settings as $setting) {
            $settigs[$setting->module][$setting->key] = $setting->value;
        }
              
        return $settigs;
    }

    /**
     * Get setting for edit
     *
     * @param int|boolean   $settingd    Whether to setting results
     * @param integer|null  $limit    Order results
     * @param integer       $per_setting    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getSettingByIdForEdit($id)
    {

        $id = (int) $id;

        $setting = $this->getResource('Settings')->getSettingByIdForEdit($id)->toArray();

        $data = $setting[0];

        foreach ($setting as $value) {
            $data['value_' . $value['language_code']] = $value['value'];
        }

        return $data;
    }

    /**
     * Save a setting post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */

    // 
    public function saveSetting($data, $form_name){

        if (!$this->checkAcl('saveSetting')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $validator = $this->getForm($form_name, ucfirst($data['module']));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        if(isset($data['ml']))
            $data = $data + $data['ml'];

        foreach($data as $key => $val){
            if ($key == 'module' || $key == 'type' || $key == 'ml' || $key == 'submodule') continue;
            
            $record = array('key' => $key,
                            'value' => $val,
                            'module' => $data['module']);

            $setting = $this->getResource('Settings')->getSettingByKey($key, $data['module']);
            if ( !is_null($setting) ){
                // update record
                $record['setting_id'] = $setting->setting_id;
                $new = $this->getResource('Settings')->saveRow($record, $setting);
            }
            else {
                // create record
                $new = $this->getResource('Settings')->saveRow($record);
            }

        }



        /*$setting = array_key_exists('setting_id', $data) ?
                $this->getResource('Settings')->getSettingById($data['setting_id']) : null;

        $new = $this->getResource('Settings')->saveRow($data, $setting);
*/
        return $new;
    }

    public function _saveSetting($data, $validator = 'add')
    {
        if (!$this->checkAcl('saveSetting')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $validator = $this->getForm('Settings' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $setting = array_key_exists('setting_id', $data) ?
                $this->getResource('Settings')->getSettingById($data['setting_id']) : null;


        $new = $this->getResource('Settings')->saveRow($data, $setting);

        $this->getResource('Settings')->deleteTranslatedRows($new);

        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'setting_id' => (int) $new,
                'language_code' => $lang->code,
                'value' => $data['value_' . $lang->code]
            );

            $this->getResource('Settings')->saveTranslatedRows($translate_data);
        }

        return $new;
    }

    public function getImageResizeSettingById($id)
    {
        $id = (int) $id;
        return $this->getResource('Settingimageresize')->getImageResizeSettingById($id);
    }

    public function getImageResizeSettingByIdent($ident, $ignoreRow = null)
    {
        return $this->getResource('Settingimageresize')->getImageResizeSettingByIdent($ident, $ignoreRow);
    }

    public function getImageResizeSettings()
    {
        return $this->getResource('Settingimageresize')->getImageResizeSettings();
    }

    public function saveImageResizeSetting($data, $type = 'edit')
    {
        if (!$this->checkAcl('savesaveImageResizeSetting')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $validator = $this->getForm('SettingsImageResize' . ucfirst($type));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $setting = array_key_exists('setting_image_resize_id', $data) ?
                $this->getResource('Settingimageresize')->getImageResizeSettingById($data['setting_image_resize_id']) : null;

        return $this->getResource('Settingimageresize')->saveRow($data, $setting);
    }

    public function getImageResizeStrategyById($id)
    {
        $id = (int) $id;
        return $this->getResource('Settingimageresizestrategy')->getImageResizeStrategyById($id);
    }

    public function getImageResizeStrategyByIdent($ident, $ignoreRow = null)
    {
        return $this->getResource('Settingimageresizestrategy')->getImageResizeStrategyByIdent($ident, $ignoreRow);
    }

    public function getImageResizeStrategies()
    {
        return $this->getResource('Settingimageresizestrategy')->getImageResizeStrategies();
    }

    public function getImageResizeSettingForModel($ident = null)
    {
        $image_resize_setting = $this->getResource('Settingimageresize')->getImageResizeSettingByIdent($ident);

        if (is_null($image_resize_setting))
            return false;

        $settings = array();
        $image_resize_strategies = array();

        foreach ($this->getResource('Settingimageresizestrategy')->getImageResizeStrategies() as $value) {
            $image_resize_strategies[$value->setting_image_resize_strategy_id] = $value->ident;
        }


        $settings['width_thumbnail'] = $image_resize_setting->width_thumbnail;
        $settings['height_thumbnail'] = $image_resize_setting->height_thumbnail;
        $settings['strategy_thumbnail'] = 'ARTCMF_Filter_ImageSize_Strategy_' .
                ucfirst($image_resize_strategies[$image_resize_setting->strategy_thumbnail_id]);


        $settings['width_preview'] = $image_resize_setting->width_preview;
        $settings['height_preview'] = $image_resize_setting->height_preview;
        $settings['strategy_preview'] = 'ARTCMF_Filter_ImageSize_Strategy_' .
                ucfirst($image_resize_strategies[$image_resize_setting->strategy_preview_id]);

        $settings['width_detail'] = $image_resize_setting->width_detail;
        $settings['height_detail'] = $image_resize_setting->height_detail;
        $settings['strategy_detail'] = 'ARTCMF_Filter_ImageSize_Strategy_' .
                ucfirst($image_resize_strategies[$image_resize_setting->strategy_detail_id]);

        $settings['width_full'] = $image_resize_setting->width_full;
        $settings['height_full'] = $image_resize_setting->height_full;
        $settings['strategy_full'] = 'ARTCMF_Filter_ImageSize_Strategy_' .
                ucfirst($image_resize_strategies[$image_resize_setting->strategy_full_id]);

        return $settings;
    }

    public function saveImageResizeStrategy($data, $type = 'edit')
    {
        if (!$this->checkAcl('savesaveImageResizeStrategy')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $validator = $this->getForm('settingImageStrategy' . ucfirst($type));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $setting = array_key_exists('setting_image_resize_strategy_id', $data) ?
                $this->getResource('Settingimageresizestrategy')->getImageResizeStrategyById($data['setting_image_resize_strategy_id']) : null;

        return $this->getResource('Settingimageresizestrategy')->saveRow($data, $setting);
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Settings';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
