<?php
/**
 * Default_Resource_Menu
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Menu extends ARTCMF_Model_Resource_Db_Table_Abstract
{
    protected $_name = 'menu';
    protected $_primary = 'menu_id';
    protected $_rowClass = 'Default_Resource_Menu_Item';
    
       
    public function getMenuByType($type)
    {
        $select = $this->select()
                       ->where('type = ?', $type);

        return $this->fetchRow($select);
    }

    public function getMenuByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.menu_id = {$this->_name}_translation.menu_id")
                
                ->where($this->_name . '.menu_id = ?', (int)$id)
                ->order('sort_order');

        return $this->fetchAll($select);
    }
    
    public function getMenuById($id)
    {
        return $this->find($id)->current();
        /*$select = $this->select()
                       ->where('menu_id = ?', (int)$id);
                       
        return $this->fetchRow($select);*/
    }

    public function getMenus()
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.menu_id = {$this->_name}_translation.menu_id")
                ->order('sort_order');


        return $this->fetchAll($select);
    }


    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'menu_id' => $translate_data['menu_id'],
                    'language_code' => $translate_data['language_code'],
                    'name' => $translate_data['name'],
                    //'title' => $translate_data['title']
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'name' => $translate_data['name'],
                    //'title' => $translate_data['title']
                        ), "`menu_id` = {$translate_data['menu_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "menu_id = {$id}");
    }
}
