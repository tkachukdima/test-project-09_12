<?php

/**
 * Default_Resource_Menuitem
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Menuitem extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'menu_item';
    protected $_primary = 'menu_item_id';
    protected $_rowClass = 'Default_Resource_Menuitem_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);
        
        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    /**
     * Get a photo by its id
     *
     * @param int $id The id to search for
     * @return Default_Resource_Product_Item|null
     */
    public function getMenuitemById($id)
    {
        return $this->find($id)->current();
    }


    public function getMenusByParentId($menu_id, $parent_id){
        $select = $this->select();

        $select->from($this->_name)
               ->setIntegrityCheck(false)
               ->joinLeft($this->_name . '_translation', "{$this->_name}.menu_item_id = {$this->_name}_translation.menu_item_id")
               ->where($this->_name . '_translation.language_code = ?', $this->_lang)
               
               ->where($this->_name . '.parent_id = ?', (int)$parent_id)
               ->where($this->_name . '.menu_id = ?', (int)$menu_id)
                       
               ->order('sort_order');

        return $this->fetchAll($select);
    }

    public function getMenuitemByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.menu_item_id = {$this->_name}_translation.menu_item_id")
                
                ->where($this->_name . '.menu_item_id = ?', (int)$id)
                ->order('sort_order');

        return $this->fetchAll($select);
    }

    public function getAllMenuitem()
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.menu_item_id = {$this->_name}_translation.menu_item_id");

        return $this->fetchAll($select);
    }

    public function getMenuitems()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from($this->_name)
                ->joinUsing('menu', 'menu_id', array('menu_name' => 'menu.name'))
                ->order('menu.sort_order')
                ->order($this->_name . '.sort_order');

        return $this->fetchAll($select);
    }

    public function getMenuitemsByMenu($menu_id, $order)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.menu_item_id = {$this->_name}_translation.menu_item_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where("menu_id = ?", (int)$menu_id)
                ->order('sort_order');

        if (null !== $order) {
            $select->order($order);
        }

        return $this->fetchAll($select);
    }

    public function getMenuitemsForTree($menu_id, $status)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.menu_item_id = {$this->_name}_translation.menu_item_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order(array($this->_name . '.parent_id', $this->_name . '.sort_order', $this->_name . '.menu_item_id'))
                ->where($this->_name . '.menu_id = ?', (int)$menu_id);
        
        if(null !== $status)
            $select->where($this->_name . '.status = ?', $status);

//        print_r($this->fetchAll($select)->toArray()); exit;
        return $this->fetchAll($select);
    }

    public function getNearSort($sort_order, $move, $menu_id, $parent_id)
    {

        $select = $this->select();

        switch ($move) {
            case 'up':
                $select->where('sort_order < ?', (int)$sort_order)
                        ->where('menu_id = ?', (int)$menu_id)
                        ->where('parent_id = ?', (int)$parent_id)
                        ->order('sort_order DESC')
                        ->limit(1, 0);
                break;

            case 'down':
                $select->where('sort_order > ?', (int)$sort_order)
                        ->where('menu_id = ?', (int)$menu_id)
                        ->where('parent_id = ?', (int)$parent_id)
                        ->order('sort_order ASC')
                        ->limit(1, 0);
                break;

            default:
                break;
        }


        return $this->fetchAll($select);
    }
    
    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'menu_item_id' => $translate_data['menu_item_id'],
                    'language_code' => $translate_data['language_code'],
                    'name' => $translate_data['name'],
                    'title' => $translate_data['title']
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'name' => $translate_data['name'],
                    'title' => $translate_data['title']
                        ), "`menu_item_id` = {$translate_data['menu_item_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "menu_item_id = {$id}");
    }

}