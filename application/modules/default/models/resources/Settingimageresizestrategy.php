<?php

/**
 * Default_Resource_Settingimageresizestrategy
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Settingimageresizestrategy extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'setting_image_resize_strategy';
    protected $_primary = 'setting_image_resize_strategy_id';
    protected $_rowClass = 'Default_Resource_Settingimageresizestrategy_Item';

    public function getImageResizeStrategyById($id)
    {
        return $this->find($id)->current();
    }  
    
    
     public function getImageResizeStrategyByIdent($ident, $ignoreRow = null)
    {
        $select = $this->select()
                       ->where('ident = ?', $ident);

        if (null !== $ignoreRow) {
            $select->where('ident < ?', $ignoreRow->ident);
        }

        return $this->fetchRow($select);
    }
    
    
    /**
     * Get a list of setting
     *
     * @param  boolean   $settingd      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getImageResizeStrategies()
    {
        return $this->fetchAll();
    }


}