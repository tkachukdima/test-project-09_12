<?php

/**
 * Default_Resource_Setting
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Settings extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'settings';
    protected $_primary = 'setting_id';
    protected $_rowClass = 'Default_Resource_Settings_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getSettingById($id)
    {
        return $this->find($id)->current();
    }
    public function getSettingByKey($key, $module){
        $select = $this->select();
        $select->from($this->_name)
               ->where($this->_name . '.key = ?', $key)
               ->where($this->_name . '.module = ?', $module);
        
        return $this->fetchRow($select);
    }

    public function getSettingByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                //->setIntegrityCheck(false)
                //->joinLeft($this->_name . '_translation', "{$this->_name}.setting_id = {$this->_name}_translation.setting_id")
                ->where($this->_name . '.setting_id = ?', $id);

        return $this->fetchAll($select);
    }

    public function getSettings($module = null)
    {
        $select = $this->select()
                ->from($this->_name)
                //->setIntegrityCheck(false)
                //->joinLeft($this->_name . '_translation', "{$this->_name}.setting_id = {$this->_name}_translation.setting_id")
                //->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '.module');
                //->order($this->_name . '.name');
                
        if(!is_null($module)){
            $select->where($this->_name . '.module = ?', $module);
        }
        
        //echo $select->__toString();die;

        return $this->fetchAll($select);
    }
    
    public function getSettingsModules()
    {
        $select = $this->select()
                ->from($this->_name, array('module'))
                ->order($this->_name . '.module')
                ->group($this->_name . '.module');               

        return $this->fetchAll($select);
    }



    public function saveTranslatedRows($translate_data)
    {

        $this->getAdapter()->insert($this->_name . '_translation', array(
            'setting_id' => $translate_data['setting_id'],
            'language_code' => $translate_data['language_code'],
            'value' => $translate_data['value']
        ));        
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "setting_id = {$id}");
    }

}