<?php

/**
 * Default_Resource_Language
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Language extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'language';
    protected $_primary = 'language_id';
    protected $_rowClass = 'Default_Resource_Language_Item';

    public function getLanguageById($id)
    {
        return $this->find($id)->current();
    }

    /**
     * Get a list of active languages
     *
     * @param  boolean   $languaged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getLanguages()
    {
        $select = $this->select()
                ->where('status = ?', 1)
                ->order('sort_order');

        return $this->fetchAll($select);
    }
    
     /**
     * Get a list of languages
     *
     * @param  boolean   $languaged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getAllLanguages()
    {
        $select = $this->select()
                ->order('sort_order');

        return $this->fetchAll($select);
    }

    public function getNearSort($sort_order, $move)
    {

        $select = $this->select();

        switch ($move) {
            case 'up':
                $select->where('sort_order < ?', $sort_order)
                        ->order('sort_order DESC')
                        ->limit(1, 0);
                break;

            case 'down':
                $select->where('sort_order > ?', $sort_order)
                        ->order('sort_order ASC')
                        ->limit(1, 0);
                break;

            default:
                break;
        }


        return $this->fetchAll($select);
    }

}