<?php
/**
 * Default_Resource_CategoryFaq
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Categoryfaq extends ARTCMF_Model_Resource_Db_Table_Abstract 
{
    protected $_name = 'faq_category';
    protected $_primary = 'category_faq_id';
    protected $_rowClass = 'Default_Resource_Categoryfaq_Item';
    
    protected $_referenceMap = array(
        'SubCategoryFaq' => array(
            'columns' => 'parent_id',
            'refTableClass' => 'Default_Resource_Categoryfaq',
            'refColumns' => 'category_faq_id',
        )
    );
    
    public function getCategoriesFaqByParentId($parent_id)
    {
        $select = $this->select()
                        ->where('parent_id = ?', $parent_id)
                        ->order('sort_order');

        return $this->fetchAll($select);
    }

     public function getCategoryFaqByIdent($ident, $ignoreRow = null)
    {
        $select = $this->select()
                       ->where('ident = ?', $ident);

        if (null !== $ignoreRow) {
            $select->where('ident < ?', $ignoreRow->ident);
        }

        return $this->fetchRow($select);
    }


    
    public function getCategoryFaqById($id)
    {
        $select = $this->select()
                       ->where('category_faq_id = ?', $id);
                       
        return $this->fetchRow($select);
    }

    public function getCategoriesFaq()
    {
        $select = $this->select()
                       ->order('name');

        return $this->fetchAll($select);
    }
    
      public function getCategoriesForTree()
    {
        $select = $this->select()
                ->order(array('parent_id', 'sort_order', 'category_faq_id'));
        
        return $this->fetchAll($select); 
    }
}
