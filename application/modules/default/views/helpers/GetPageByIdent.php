<?php

class Default_View_Helper_GetPageByIdent extends Zend_View_Helper_Abstract
{

    public function getPageByIdent($ident)
    {
        $pageModel = new Default_Model_Page();

        return $pageModel->getPageByIdent($ident);

    }
}