<?php
/**
 * Default_View_Helper_listSort
 * 
 * Helper for displaying list sort
 * 
 * @category   Default
 * @package    View_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_View_Helper_listSort extends Zend_View_Helper_Abstract
{
    protected $sortList = array();
    
    protected $default;//$sortList[0]['title'];
    
    public function listSort($sortList, $order)
    {
        $this->sortList = $sortList;
        $def = $this->sortList[0]['title'];
        foreach ($this->sortList as $key => $val)
            $def = ($val['order_name'] == $order['name'] && $val['order_sort'] == $order['sort'])? $val['title']:$def;
        
        $this->default = $def;
        //$rez['current'] = $this->default;
        
        return $this;//$this->_modelManuf->getManufacturers(null,null,0);
    }
    
    public function getSortList()
    {
        return $this->sortList;
    }
    
    public function getDefault()
    {
        return $this->default;
    }
    
}
