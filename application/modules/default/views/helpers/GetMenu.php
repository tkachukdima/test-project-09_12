<?php

class Default_View_Helper_GetMenu extends Zend_View_Helper_Abstract
{

    public function getMenu($type = 'main')
    {
        $menuModel = new Default_Model_Menu();
        $tree = $menuModel->getCached('menu')->getMenuTree($type);
        return new Zend_Navigation($tree);
    }

}
