<?php

return array(
    'default_page' => new Zend_Controller_Router_Route_Regex(
            '([a-z]{2})/page/([a-zA-Z-_0-9]+)\.html',
            array(
                'module' => 'default',
                'controller' => 'page',
                'action' => 'index',
                'lang' => DEFAULT_LANG,
                'pageIdent' => ''
            ),
            array(
                1 => 'lang',
                2 => 'pageIdent',
            ),
            '%s/page/%s.html'
    ),
    'sitemap' => new Zend_Controller_Router_Route(
            ':lang/sitemap',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'sitemap',
                'lang' => DEFAULT_LANG,
            ),
            array(
                'lang' => '[a-z]{2}'
            )
    ),
    'sitemap2' => new Zend_Controller_Router_Route(
            'sitemap',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'sitemap'
            )
    ),
    'sitemap_xml' => new Zend_Controller_Router_Route(
            ':lang/sitemap.xml',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'sitemap',
                'lang' => DEFAULT_LANG,
                'xml' => true
            ),
            array(
                'lang' => '[a-z]{2}'
            )
    ),
    'sitemap_xml2' => new Zend_Controller_Router_Route(
            'sitemap.xml',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'sitemap',
                'xml' => true
            )
    ),
    'search' => new Zend_Controller_Router_Route(
            ':lang/search',
            array(
                'module' => 'default',
                'controller' => 'search',
                'action' => 'index',
                'lang' => DEFAULT_LANG
            ),
            array(
                'lang' => '[a-z]{2}'
            )
    ),
    'default_faq' => new Zend_Controller_Router_Route(
            'faq/:categoryIdent',
            array(
                'module' => 'default',
                'controller' => 'faq',
                'action' => 'index',
                'lang' => DEFAULT_LANG,
                'categoryIdent' => ''
            ),
            array(
                'lang' => '[a-z]{2}',
                'categoryIdent' => '[a-zA-Z-_0-9]+'
            )
    ),
);