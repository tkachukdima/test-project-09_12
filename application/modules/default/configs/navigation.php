<?php

return array(
    array(
        'label'        => _('Settings'),
        'module'       => 'default',
        'controller'   => 'settings',
        'action'       => 'index',
        'route'        => 'admin',
        'resource'     => 'default:settings',
        'privilege'    => 'index',
        'reset_params' => true,
        'class'        => 'dashboard-config',
        'pages'        => array(
            array(
                'label'        => _('System Settings'),
                'module'       => 'default',
                'controller'   => 'settings',
                'action'       => 'index',
                'route'        => 'admin',
                'resource'     => 'default:settings',
                'privilege'    => 'index',
                'reset_params' => true,
                'pages'        => array(
                     array(
                        'label'        => _('Edit Settings'),
                        'module'       => 'default',
                        'controller'   => 'settings',
                        'action'       => 'edit',
                        'route'        => 'admin',
                        'resource'     => 'default:settings',
                        'privilege'    => 'edit',
                        'reset_params' => true,
                    ),
                    array(
                        'label'        => _('Save Settings'),
                        'module'       => 'default',
                        'controller'   => 'settings',
                        'action'       => 'save',
                        'route'        => 'admin',
                        'resource'     => 'default:settings',
                        'privilege'    => 'save',
                        'reset_params' => true,
                    ),
                    array(
                        'label'        => _('Error'),
                        'module'       => 'default',
                        'controller'   => 'error',
                        'action'       => 'error',
                        'route'        => 'admin',
                        'resource'     => 'default:error',
                        'privilege'    => 'error',
                        'reset_params' => true,
                    ),
                )                
            ),    
            array(
                'label'        => _('Add setting'),
                'module'       => 'default',
                'controller'   => 'settings',
                'action'       => 'add',
                'route'        => 'admin',
                'resource'     => 'default:settings',
                'privilege'    => 'add',
                'reset_params' => true,
            ),
            array(
                'label' => '---------------------------------------',
                'uri' => '#'
            ),
            array(
                'label'        => _('Resize settings'),
                'module'       => 'default',
                'controller'   => 'settings',
                'action'       => 'list-image-resize-settings',
                'route'        => 'admin',
                'resource'     => 'default:settings',
                'privilege'    => 'list-image-resize-settings',
                'reset_params' => true
            ),
            array(
                'label'        => _('Add resize setting'),
                'module'       => 'default',
                'controller'   => 'settings',
                'action'       => 'add-image-resize-setting',
                'route'        => 'admin',
                'resource'     => 'default:settings',
                'privilege'    => 'add-image-resize-setting',
                'reset_params' => true,
                'pages'        => array(
                    array(
                        'label'        => _('Edit settings resize'),
                        'module'       => 'default',
                        'controller'   => 'settings',
                        'action'       => 'edit-image-resize-setting',
                        'route'        => 'admin',
                        'resource'     => 'default:settings',
                        'privilege'    => 'edit-image-resize-setting',
                        'reset_params' => true
                    ),
                    array(
                        'label'        => _('Edit settings resize'),
                        'module'       => 'default',
                        'controller'   => 'settings',
                        'action'       => 'save-image-resize-setting',
                        'route'        => 'admin',
                        'resource'     => 'default:settings',
                        'privilege'    => 'save-image-resize-setting',
                        'reset_params' => true
                    )
                )
            ),
            array(
                'label'        => _('System info'),
                'module'       => 'default',
                'controller'   => 'settings',
                'action'       => 'sysinfo',
                'route'        => 'admin',
                'resource'     => 'default:settings',
                'privilege'    => 'sysinfo',
                'reset_params' => true,
            ),
            array(
                'label'        => _('Media Files'),
                'uri'          => "#mediafiles",
            ),
            array(
                'label'        => _('Ip Blocking'),
                'module'       => 'default',
                'controller'   => 'ipblocking',
                'action'       => 'list',
                'route'        => 'admin',
                'resource'     => 'default:ipblocking',
                'privilege'    => 'list',
                'reset_params' => true,
            ),
            array(
                'label'        => _('Update Search Index'),
                'module'       => 'default',
                'controller'   => 'search',
                'action'       => 'update-index',
                'route'        => 'admin',
                'resource'     => 'default:search',
                'privilege'    => 'update-index',
                'reset_params' => true,
            ),
        )
    ),
    array(
        'label'        => _('Menu'),
        'module'       => 'default',
        'controller'   => 'menus',
        'action'       => 'list',
        'route'        => 'admin',
        'resource'     => 'default:menus',
        'privilege'    => 'list',
        'reset_params' => true,
        'class'        => 'dashboard-menus',
        'pages'        => array(
            array(
                'label'        => _('Menu'),
                'module'       => 'default',
                'controller'   => 'menus',
                'action'       => 'list',
                'route'        => 'admin',
                'resource'     => 'default:menus',
                'privilege'    => 'list',
                'reset_params' => true,
                'pages'        => array(
                    array(
                        'label'        => _('List of Menu Items'),
                        'module'       => 'default',
                        'controller'   => 'menus',
                        'action'       => 'list-menu-item',
                        'route'        => 'admin',
                        'resource'     => 'default:menus',
                        'privilege'    => 'list-menu-item',
                        'reset_params' => true,
                    ),
                    array(
                        'label'        => _('Edit Menu'),
                        'module'       => 'default',
                        'controller'   => 'menus',
                        'action'       => 'edit',
                        'route'        => 'admin',
                        'resource'     => 'default:menus',
                        'privilege'    => 'edit',
                        'reset_params' => true,
                    ),
                    array(
                        'label'        => _('Edit Menu Item'),
                        'module'       => 'default',
                        'controller'   => 'menus',
                        'action'       => 'edit-menu-item',
                        'route'        => 'admin',
                        'resource'     => 'default:menus',
                        'privilege'    => 'edit-menu-item',
                        'reset_params' => true,
                    )
                )
            ),
            array(
                'label'        => _('Add Menu'),
                'module'       => 'default',
                'controller'   => 'menus',
                'action'       => 'add',
                'route'        => 'admin',
                'resource'     => 'default:menus',
                'privilege'    => 'add',
                'reset_params' => true
            ),
            array(
                'label'        => _('Add a Menu Item'),
                'module'       => 'default',
                'controller'   => 'menus',
                'action'       => 'add-menu-item',
                'route'        => 'admin',
                'resource'     => 'default:menus',
                'privilege'    => 'add-menu-item',
                'reset_params' => true
            )
        )
    ),
    array(
        'label'        => _('Pages'),
        'module'       => 'default',
        'controller'   => 'page',
        'action'       => 'list',
        'route'        => 'admin',
        'resource'     => 'default:page',
        'privilege'    => 'list',
        'reset_params' => true,
        'class'        => 'dashboard-page',
        'pages'        => array(
            array(
                'label'        => _('Pages'),
                'module'       => 'default',
                'controller'   => 'page',
                'action'       => 'list',
                'route'        => 'admin',
                'resource'     => 'default:page',
                'privilege'    => 'list',
                'reset_params' => true,
                'pages'        => array(
                    array(
                        'label'        => _('Edit Page'),
                        'module'       => 'default',
                        'controller'   => 'page',
                        'action'       => 'edit',
                        'route'        => 'admin',
                        'resource'     => 'default:page',
                        'privilege'    => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label'        => _('Edit Page'),
                        'module'       => 'default',
                        'controller'   => 'page',
                        'action'       => 'save',
                        'route'        => 'admin',
                        'resource'     => 'default:page',
                        'privilege'    => 'save',
                        'reset_params' => true
                    )
                )
            ),
            array(
                'label'        => _('Add page'),
                'module'       => 'default',
                'controller'   => 'page',
                'action'       => 'add',
                'route'        => 'admin',
                'resource'     => 'default:page',
                'privilege'    => 'add',
                'reset_params' => true
            )
        )
    ),    
    array(
        'label'        => _('Faq'),
        'module'       => 'default',
        'controller'   => 'faq',
        'action'       => 'list',
        'route'        => 'admin',
        'resource'     => 'default:faq',
        'privilege'    => 'list',
        'reset_params' => true,
        'class'        => 'dashboard-faq',
        'pages'        => array(
            array(
                'label'        => _('Faq'),
                'module'       => 'default',
                'controller'   => 'faq',
                'action'       => 'list',
                'route'        => 'admin',
                'resource'     => 'default:faq',
                'privilege'    => 'list',
                'reset_params' => true,
                'pages'        => array(
                    array(
                        'label'        => _('Edit Faq'),
                        'module'       => 'default',
                        'controller'   => 'faq',
                        'action'       => 'edit-faq',
                        'route'        => 'admin',
                        'resource'     => 'default:faq',
                        'privilege'    => 'edit-faq',
                        'reset_params' => true
                    ),
                    array(
                        'label'        => _('Edit Faq'),
                        'module'       => 'default',
                        'controller'   => 'faq',
                        'action'       => 'save-faq',
                        'route'        => 'admin',
                        'resource'     => 'default:faq',
                        'privilege'    => 'save-faq',
                        'reset_params' => true
                    ),
                    array(
                        'label'        => _('Edit Category'),
                        'module'       => 'default',
                        'controller'   => 'faq',
                        'action'       => 'edit-category',
                        'route'        => 'admin',
                        'resource'     => 'default:faq',
                        'privilege'    => 'edit-category',
                        'reset_params' => true
                    ),
                    array(
                        'label'        => _('Edit Category'),
                        'module'       => 'default',
                        'controller'   => 'faq',
                        'action'       => 'save-category',
                        'route'        => 'admin',
                        'resource'     => 'default:faq',
                        'privilege'    => 'save-category',
                        'reset_params' => true
                    ),
                )
            ),
            array(
                'label'        => _('Add category'),
                'module'       => 'default',
                'controller'   => 'faq',
                'action'       => 'add-category',
                'route'        => 'admin',
                'resource'     => 'default:faq',
                'privilege'    => 'add-category',
                'reset_params' => true
            ),
            array(
                'label'        => _('Add Faq'),
                'module'       => 'default',
                'controller'   => 'faq',
                'action'       => 'add-faq',
                'route'        => 'admin',
                'resource'     => 'default:faq',
                'privilege'    => 'add-faq',
                'reset_params' => true
            ),
        )
    )
);
