<?php

return array(
    'admin',
    'contact',
    'cron',
    'error',  
    'faq',
    'index',
    'menus',
    'page',     
    'representation',
    'search',
    'settings',
    'ipblocking'
);