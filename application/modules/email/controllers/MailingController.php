<?php

/**
 * MailingController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Email_MailingController extends Zend_Controller_Action
{

    protected $_forms = array();
    protected $_modelMailing;

    public function init()
    {
        $this->_modelMailing = new Email_Model_Mailing();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        

        $modelUser = new User_Model_User();
        $this->view->users = $modelUser->getUsers(null);
        $this->view->recipients = $this->_modelMailing->getRecipients();

        $mailingForm = $this->_getMailingForm();

        if ($request->isPost()) {
                       
            if ($mailingForm->isValid($request->getPost())) {

                $data = $mailingForm->getValues();

                $data['date'] = date('Y-m-d H:i:s');
               
                $recipients = $request->getPost('recipients');
                
                $recipientsEmail = array();
                foreach ($this->view->users as $user) {
                    if(array_key_exists($user->user_id, $recipients['users']) AND !in_array($user->email, $recipientsEmail)){
                        $recipientsEmail[] = $user->email;
                    }
                }
                
                foreach ($this->view->recipients as $recipient) {
                    if(array_key_exists($recipient->recipient_id, $recipients['contacts']) AND !in_array($recipient->email, $recipientsEmail)){
                        $recipientsEmail[] = $recipient->email;
                    }
                }
                
                foreach ($recipientsEmail as $email) {
                    $mail = new Zend_Mail('UTF-8');
                    $mail->setFrom($data['email_from'], $data['name_from'])
                            ->setReplyTo($data['email_from'])
                            ->addTo($email)
                            ->setSubject($data['subject'])
                            ->setBodyHtml($data['body']);
                    try {
                        @$mail->send();
                    } catch (Exception $exc) {
                        echo $exc->getTraceAsString();
                    }
                }


                $this->_helper->json(array('success' => true, 'message' => $this->translate(_('A letter was sent to the specified addresses')) ));

            } 
        }

        $this->view->mailingForm = $mailingForm;
    }

    public function addRecipientAction()
    {

        /*$recipientForm = $this->_modelMailing->getForm('Recipient');
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($recipientForm->isValid($request->getPost())) {
                $data = $recipientForm->getValues();
                $data['name'] = $data['email'];
                $recipient_id = $this->_modelMailing->saveRecipient($data);
                $result = array(
                    'success' => true,
                    'recipient_id' => $recipient_id,
                    'name' => $data['name'],
                    'email' => $data['email']
                );
                $this->_helper->json($result);
            }
        }*/


        $this->view->layout()->disableLayout();
        $this->getHelper('viewRenderer')->setNoRender();

        $recipientForm = $this->_modelMailing->getForm('Recipient');
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($recipientForm->isValid($request->getPost())) {
                $data = $recipientForm->getValues();
                $data['name'] = $data['email'];
                $recipient_id = $this->_modelMailing->saveRecipient($data);
                $result = array(
                    'success' => true,
                    'recipient_id' => $recipient_id,
                    'name' => $data['email'], //$data['name'],
                    'email' => $data['email'],
                    'message' => $this->view->translate(_('Thank you! Your email is added.'))
                );
                $this->_helper->json($result);
            } else {
                $this->_helper->json(array('success' => false, 'errors' => $recipientForm->getMessages()));
            }
        }

    }

    public function deleteRecipientAction()
    {
        $recipient_id = $this->getRequest()->getParam('recipient_id', null);

        if (!is_null($recipient_id)) {
            $this->_modelMailing->deleteRecipient($recipient_id);
            $result = array(
                'success' => true,
                'recipient_id' => $recipient_id
            );
            $this->_helper->json($result);
        }
    }

    protected function _getMailingForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Mailing'] = $this->_modelMailing->getForm('Mailing');
        $this->_forms['Mailing']->setAction($urlHelper->url(array(                    
                    'module' => 'email',
                    'controller' => 'mailing',
                    'action' => 'index'
                        ), 'admin'
                ));
        $this->_forms['Mailing']->setMethod('post');

        return $this->_forms['Mailing'];
    }

}
