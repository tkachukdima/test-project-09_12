<?php

/**
 * Base Page Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Email_Form_Mailing extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $this->setMethod('post');
        $this->setAction('');
        $this->setAttrib('id', 'mailing_form');


        $site_settings = Zend_Registry::get('site_settings');
        
        $this->addElement('text', 'name_from', array(
            'label' => _("Senders name"),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => $site_settings->getSetting('default', 'site_name')
        ));
        
         $this->addElement('text', 'email_from', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required' => true,
            'label' => _("Sender's Email"),
             'value' => $site_settings->getSetting('default', 'email_from')
        ));
         

        $this->addElement('text', 'subject', array(
            'label' => _('Subject'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('textarea', 'body', array(
            'label' => _('Text'),
            'filters' => array('StringTrim'),
            'required' => true,
            'class' => 'ckeditor'
        ));
        //$this->body->addDecorator(new ARTCMF_Form_Decorator_CKEditor);


        $this->addElement('submit', 'submit', array());
    }

}
