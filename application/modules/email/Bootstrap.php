<?php

/**
 * Email_Bootstrap
 *
 * @category   Bootstrap
 * @package    Email_Bootstrap
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Email_Bootstrap extends ARTCMF_Application_Module_Bootstrap
{
    public function _initModuleResourceAutoloader()
    {
        $this->getResourceLoader()->addResourceTypes(array(
            'modelResource' => array(
                'path' => 'models/resources',
                'namespace' => 'Resource',
            ),
            'modelFilter' => array(
                'path' => 'models/filter',
                'namespace' => 'Filter',
            )
        ));
    }
 
}
