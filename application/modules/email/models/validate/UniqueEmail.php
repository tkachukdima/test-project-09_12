<?php 

class Email_Validate_UniqueEmail extends Zend_Validate_Abstract
{

    const EMAIL_EXISTS = 'emailExists';

    protected $_messageTemplates = array(
        self::EMAIL_EXISTS => "Email '%value%' already exists in our system",
    );

    public function __construct(Email_Model_Mailing $model)
    {
        $this->_model = $model;
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue((string) $value);

        $recipient = $this->_model->getRecipientByEmail($context['email']);
       
        if (is_null($recipient)) {
            return true;
        }

        $this->_error(self::EMAIL_EXISTS);
        return false;
    }

}
