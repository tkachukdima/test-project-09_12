<?php

/**
 * Email_Resource_Template
 *
 * @category   Default
 * @package    Email_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Email_Resource_Template extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'email_template';
    protected $_primary = 'email_template_id';
    protected $_rowClass = 'Email_Resource_Template_Item';

    public function getTemplateById($id)
    {
        return $this->find($id)->current();
    }  
    
    public function getTemplateByIdent($ident)
    {
        $select = $this->select()
                        ->where('ident = ?', $ident);
        
        return $this->fetchRow($select);
    }  
    

    /**
     * Get a list of template
     *
     * @param  boolean   $templated      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getTemplates()
    {
        return $this->fetchAll();
    }


}