<?php 

class Email_Model_Mailing extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    public function getRecipients()
    {
        return $this->getResource('Recipient')->getRecipients();
    }

    public function getRecipientById($id)
    {
        return $this->getResource('Recipient')->getRecipientById($id);
    }
    
    public function getRecipientByEmail($email)
    {
        return $this->getResource('Recipient')->getRecipientByEmail($email);
    }

    /**
     * Save a Recipient
     *
     * @param array $data
     */
    public function saveRecipient($data, $id = null)
    {

        $recipient = !is_null($id) ? $this->getResource('Page')->getPageById($id) : null;

        return $this->getResource('Recipient')->saveRow(array('email' => $data['email'], 'name' => $data['name']), $recipient);
    }

    public function deleteRecipient($id)
    {

        $recipient = $this->getRecipientById($id);
        if (null !== $recipient) {
            $recipient->delete();
            return true;
        }

        return false;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Mailing';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Guest', $this, array())
                    ->allow('Member', $this, array())
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
