<?php

return array(array(
        'label' => _('Mailing'),
        'module' => 'email',
        'controller' => 'mailing',
        'action' => 'index',
        'route' => 'admin',
        'resource' => 'email:mailing',
        'privilege' => 'index',
        'reset_params' => true,
        'class' => 'dashboard-mailing',
        'pages' => array(
            array(
                'label' => _('Mailing'),
                'module' => 'email',
                'controller' => 'mailing',
                'action' => 'index',
                'route' => 'admin',
                'resource' => 'email:mailing',
                'privilege' => 'index',
                'reset_params' => true
            )
        )
    )
);