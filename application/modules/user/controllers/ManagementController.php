<?php

/*
 * User_IndexController
 * 
 * @category   User
 * @package    User_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class User_ManagementController extends Zend_Controller_Action
{

    protected $_forms;
    protected $_model;
    protected $_authService;

    public function init()
    {
        // get the default model
        $this->_model = new User_Model_User();
        $this->_authService = new User_Service_Authentication();
        // add forms
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {

        $this->view->layout()->setLayout('admin');

        $this->view->user = $this->_model->getUserById($this->_authService->getIdentity()->user_id);

        if (null === $this->view->user) {
            throw new ARTCMF_Exception($this->view->translate(_('User not found')));
        }

        $this->view->userForm = $this->getUserForm()->populate($this->view->user->toArray());
    }

    public function listAction()
    {

        $filter = array(
            'filter_email' => $this->_getParam('filter_email', null),
            'filter_name' => $this->_getParam('filter_name', null),
            'filter_city' => $this->_getParam('filter_city', null),
            'filter_status' => $this->_getParam('filter_status', 'any')
        );
        $this->view->assign($filter);

        $this->view->users = $this->_model->getUsers($this->_getParam('page', 1), null, $filter);
        $this->view->addHelperPath('ARTCMF/view/helper', 'ARTCMF_View_Helper');
        $this->view->pagination_config = array('total_items' => count($this->view->users),
            'items_per_page' => 1,
            'style' => 'digg');
    }

    public function editAction()
    {
        $this->view->userForm = $this->getAdminForm('edit');
        $this->view->user = $this->_model->getUserById($this->_getParam('id'));
        
        $this->view->userForm->role->setMultiOptions(User_Model_User::getRolesOptions($this->_authService->getIdentity()->role));
        $this->view->userForm->populate($this->view->user->toArray());
    }

    public function addAction()
    {
        $this->view->userForm = $this->getAdminForm('add');
    }

    public function saveAction()
    {
        $request = $this->getRequest();
        $redirector = $this->_helper->getHelper('Redirector');

        $type = $request->getParam('type', 'edit');

        $this->view->userForm = $this->getAdminForm($type);

        //если ето не админ или персонал, то проверяем тот ли юзер в форме
        if (!in_array($this->_authService->getIdentity()->role, array('Root', 'Admin', 'Manager', 'Redactor'))) {
            if ($request->getPost('user_id') != $this->_authService->getIdentity()->user_id) {
                throw new ARTCMF_Exception($this->view->translate(_('Transferred invalid user ID')));
            }
        }

        if (false === $this->_model->saveUser($request->getPost(), 'Admin' . ucfirst($type))) {
            return $this->render($type);
        }

        return $redirector->gotoRoute(
                        array(
                    'module' => 'user',
                    'controller' => 'management',
                    'action' => 'list'
                        ), 'admin');
    }

    public function deleteAction()
    {
        if (false === ($user_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('User not found')));
        }

        $this->_model->deleteUser($user_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'module' => 'user',
                    'controller' => 'management',
                    'action' => 'list'), 'admin', true);
    }
    
    
    public function viewAction()
    {
        $user_id = $this->getRequest()->getParam('user_id');
        $this->view->user = $this->_model->getUserById($user_id);

        $resurs = new User_Resource_Session();
        $this->view->sessions = $resurs->getSessionByUserId($user_id);
        $this->view->user_modified = isset($this->view->sessions->modified)? $this->view->sessions->modified: null;
        //print_r($this->view->user_modified);      

        $orderModel = new Shop_Model_Order();

        $totalAmount = $orderModel->getUserTotalAmount($user_id);
        $this->view->orders = $orderModel->getOrdersByUserId($user_id);
        $this->view->total_amount = isset($totalAmount->total_amount)? $totalAmount->total_amount:0;
        //$order = $orderModel->getOrderById($orderId);
        //print_r("1" . $this->view->totalAmount); 

        $discountModel = new Shop_Model_Discount();
        $this->view->userDiscount = $discountModel->getUserDiscount($user_id);

    }

    public function getAdminForm($type = 'edit')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Admin' . ucfirst($type)] = $this->_model->getForm('Admin' . ucfirst($type));
        $this->_forms['Admin' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'management',
                    'action' => 'save',
                    'type' => $type
                        ), 'admin'
                ));
        $this->_forms['Admin' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Admin' . ucfirst($type)];
    }

}
