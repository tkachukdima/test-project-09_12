<?php

/*
 * User_IndexController
 * 
 * @category   User
 * @package    User_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class User_IndexController extends Zend_Controller_Action
{

    protected $_model;
    protected $_authService;

    public function init()
    {
        // get the default model
        $this->_model = new User_Model_User();
        $this->_authService = new User_Service_Authentication();
        // add forms
        $this->view->userForm = $this->getUserForm();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {
                 
        $this->view->layout()->setLayout('layout_profile');
                  
        $this->view->route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        
        $this->view->user = $this->_model->getUserById($this->_authService->getIdentity()->user_id);

        $discountModel = new Shop_Model_Discount();
        $this->view->userDiscount = $discountModel->getUserDiscount($this->_authService->getIdentity()->user_id);

        if (null === $this->view->user) {
            throw new ARTCMF_Exception($this->view->translate(_('User not found')));
        }
    }

    public function settingsAction()
    {
        
        $this->view->user = $this->_model->getUserById($this->_authService->getIdentity()->user_id);
        
        $this->view->layout()->setLayout('layout_profile');
        
        
        if (null === $this->view->user) {
            throw new ARTCMF_Exception($this->view->translate(_('User not found')));
        }
        $this->view->userForm = $this->getUserForm()->populate($this->view->user->toArray());                
    }

    public function subscriptionAction()
    {

        $this->view->layout()->setLayout('layout_profile');
       
        $request = $this->getRequest();
        
        $user = $this->_model->getUserById($this->_authService->getIdentity()->user_id);
        
        $subscribeForm = $this->_model->getForm('Subscribe');
                 
       
        if($request->isPost()) {     
            // print_r($request->getPost());die;
            if($subscribeForm->isValid($request->getPost())){
                $user->subscription = $subscribeForm->getValue('subscription');
                $user->save();
            } 
         }


        $this->view->subscribeForm = $subscribeForm->populate($user->toArray());
    }
    
    public function saveAction()
    {
        $request = $this->getRequest();
        $redirector = $this->_helper->getHelper('Redirector');
        $this->view->layout()->setLayout('layout_profile');

        $validator = 'edit';
        $onFail = 'edit';
        $onSuccess = array(
            'urlOptions' => array(
                'controller' => 'index',
                'action' => 'index'
            ),
            'route' => 'default'
        );

        if ($this->_getParam('isAdmin')) {

            $validator = 'admin';
            $onFail = $this->_getParam('type');
            $onSuccess = array(
                'urlOptions' => array(
                    'module' => 'user',
                    'controller' => 'management',
                    'action' => 'list'
                ),
                'route' => 'admin'
            );

            if ('edit' == $this->_getParam('type')) {
                $this->view->user = $this->_model->getUserById($this->_getParam('id'));
            }

            $this->view->userForm = $this->getAdminForm('admin');
        }


        //если ето не админ или персонал, то проверяем тот ли юзер в форме
        if (!in_array($this->_authService->getIdentity()->role, array('Root', 'Admin', 'Manager', 'Redactor'))) {
            if ($request->getPost('user_id') != $this->_authService->getIdentity()->user_id) {
                throw new ARTCMF_Exception($this->view->translate(_('Transferred invalid user ID')));
            }
        }

        if (false === $this->_model->saveUser($request->getPost(), ucfirst($validator))) {
            return $this->render($onFail);
        }

        return $redirector->gotoRoute($onSuccess['urlOptions'], $onSuccess['route']);
    }

    public function getUserForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Edit'] = $this->_model->getForm('Edit');
        $this->_forms['Edit']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'index',
                    'action' => 'save'
                        ), 'default'
                ));
        $this->_forms['Edit']->setMethod('post');

        return $this->_forms['Edit'];
    }

      public function getAdminForm($type = 'edit')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Admin'] = $this->_model->getForm('Admin');
        $this->_forms['Admin']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'index',
                    'action' => 'save',
                    'type' => $type
                        ), 'admin'
                ));
        $this->_forms['Admin']->setMethod('post');

        return $this->_forms['Admin'];
    }
}
