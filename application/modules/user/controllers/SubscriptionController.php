<?php

class User_SubscriptionController extends Zend_Controller_Action
{

    //put your code here
    protected $_subscriptionModel;
    protected $_authService;
    protected $_modelUser;
    protected $_paymentModel;

    public function init()
    {
        $this->_subscriptionModel = new User_Model_Subscription();
        $this->_paymentModel = new Payment_Model_Payment();
        $this->_modelUser = new User_Model_User();
        $this->_authService = new User_Service_Authentication();
    }

    public function indexAction()
    {
        $this->view->payment_history = $this->_paymentModel->getPaymentHistory($this->_authService->getIdentity()->user_id);
    }
    
    public function subscribePaypalAction()
    {

        $this->view->subscriptions = $this->_subscriptionModel->getSubscriptions();

        $this->view->subscribed = $this->_authService->getIdentity()->subscription_id;
    }

 
    public function subscribeAction()
    {
        
        $subscription_type = $this->getRequest()->getParam('subscription_type', null);
        
        
        $payment_system = $this->getRequest()->getParam('payment_system', null);
        
        $avaible_payment_systems = array('paypal', 'interkassa');
        
        if (!in_array($payment_system, $avaible_payment_systems))
            throw new ARTCMF_Exception($this->view->translate(_('Payment system not found')));
   
        if ($subscription_type == 1) {
            $url = array(
                'module'            => 'payment',
                'controller'        =>  $payment_system,
                'action'            => 'pay',
                'subscription_type' => $subscription_type
            );
        } elseif ($subscription_type == 2) {
            $subscription_type_period = $this->getRequest()->getParam('subscription_type_period', null);
            $url                      = array(
                'module'            => 'payment',
                'controller'        => $payment_system,
                'action'            => 'pay-subscription',
                'subscription_type' => $subscription_type_period
            );
        }
        $redirector = $this->_helper->getHelper('Redirector');
        return $redirector->gotoRoute($url, 'default', true);

//        $subscription_id = $this->getRequest()->getParam('subscription_id', null);
//        
//        if (is_null($subscription_id)) {
//            throw new ARTCMF_Exception($this->view->translate(_('Subscription not found')));
//        }
//        
//        $period = (int) $this->getRequest()->getParam('period', 1);
//        
//        if (!$period) {
//            throw new ARTCMF_Exception($this->view->translate(_('Period not found')));
//        }
//
//        $user_id = $this->_authService->getIdentity()->user_id;
//
//        $result = $this->_subscriptionModel->subscribe($subscription_id, $user_id, $period);
//        
//        $this->_authService->updateSession();
//        $redirector = $this->_helper->getHelper('Redirector');
//        $flashMessenger = $this->_helper->FlashMessenger;
//        if (false !== $result) {
//
//            $flashMessenger->setNamespace('success');
//            $flashMessenger->addMessage('You have been subscribed');
//        } else {
//            $errors         = $this->_subscriptionModel->getErrors();
//            $flashMessenger->setNamespace('error');
//            foreach ($errors as $error) {
//                $flashMessenger->addMessage($error);
//            }
//        }
//        
//        return $redirector->gotoRoute(array(
//                        'module'     => 'user',
//                        'controller' => 'subscription',
//                        'action'     => 'index'
//                            ), 'default', true);
    }
    
    
    public function listAction()
    {
        $this->view->subscriptions = $this->_subscriptionModel->getSubscriptions();
    }

    public function addAction()
    {

        if ($this->getRequest()->isPost()) {
            if ($this->_subscriptionModel->saveSubscription($this->getRequest()->getPost(), 'add')) {
                $message    = _('Paid subscription is added');
                $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);
                $redirector = $this->_helper->getHelper('Redirector');
                $onSuccess  = array(
                    'urlOptions' => array(
                        'action' => 'list'
                    ),
                    'route'  => 'admin'
                );
                return $redirector->gotoRoute($onSuccess['urlOptions'], $onSuccess['route']);
            }
        }

        $form = $this->getSubscriptionForm('add');
        $this->view->form = $form;
    }

    public function editAction()
    {

        if ($this->getRequest()->isPost()) {
            if ($this->_subscriptionModel->saveSubscription($this->getRequest()->getPost(), 'edit')) {
                $message = _('Paid subscription is updated');
                $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

                $redirector = $this->_helper->getHelper('Redirector');
                $onSuccess  = array(
                    'urlOptions' => array(
                        'action'          => 'list',
                        'subscription_id' => NULL
                    ), 'route'           => 'admin'
                );
                return $redirector->gotoRoute($onSuccess['urlOptions'], $onSuccess['route']);
            }
        }

        $subscription_id = $this->getRequest()->getParam('subscription_id');
        $data            = $this->_subscriptionModel->getSubscriptionById($subscription_id)->toArray();

        $form = $this->getSubscriptionForm('edit');
        $form->populate($data);
        $this->view->form = $form;
    }

    public function deleteAction()
    {

        $subscription_id = $this->getRequest()->getParam('subscription_id');
        $this->_subscriptionModel->deleteSubscription($subscription_id);
        $redirector      = $this->_helper->getHelper('Redirector');
        $onSuccess       = array(
            'urlOptions' => array(
                'action'          => 'list',
                'subscription_id' => NULL
            ),
            'route'           => 'admin'
        );
        return $redirector->gotoRoute($onSuccess['urlOptions'], $onSuccess['route']);
    }

    public function getSubscriptionForm($type = 'edit')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Subscription' . ucfirst($type)] = $this->_subscriptionModel->getForm('Subscription' . ucfirst($type));
        $this->_forms['Subscription' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'module'     => 'user',
                    'controller' => 'subscription',
                    'action'     => $type), 'admin'
                ));
        $this->_forms['Subscription' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Subscription' . ucfirst($type)];
    }

}

