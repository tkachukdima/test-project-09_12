<?php

/*
 * User_AuthController
 * 
 * @category   User
 * @package    User_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class User_AuthController extends Zend_Controller_Action
{

    protected $_modelUser;
    protected $_authService;

    public function init()
    {
        // get the default model
        $this->_modelUser = new User_Model_User();
        $this->_authService = new User_Service_Authentication();

        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function registerAction()
    {
        if ($this->_authService->getIdentity()) {
            return $this->_helper->redirector();
        }

        $this->view->registerForm = $this->getRegistrationForm();
        $this->view->registerForm->setAttrib('id', 'register_form');
    }

    public function completeRegistrationAction()
    {
        $this->view->registerForm = $this->getRegistrationForm();
        $this->view->registerForm->setAttrib('id', 'register_form');

        
        $request = $this->getRequest();       
 
        if (!$request->isPost()) {
            return $this->_helper->redirector('register');
        }
        $data = $request->getPost();
        $data['status'] = 1;

        $data_result = $this->_modelUser->registerUser($data);        
        if (false === $data_result) {
             if ($request->isXmlHttpRequest()) {
                 $result = array('status' => 'error');
                 $result['error_text'] = $this->view->translate(_('Login failed, incorrectly filled out a form.'));//Войти не удалось, неверно заполнена форма.
                 $result['error_messages'] = $this->_forms['Register']->getMessages();
                 return $this->_helper->json($result);
             } else {
                return $this->render('register');
             }
        } else {
            $auth = $this->_authService->getAuth();
            $user = $this->_modelUser->getUserById($data_result);
            if ($user) {
                $auth->getStorage()->write($user);
                $sessionNamespace = new Zend_Session_Namespace('Welcome');
                $sessionNamespace->show_welcome = true;
            }
            
            if ($request->isXmlHttpRequest()) {
                 $result = array('status' => 'success');                 
                 return $this->_helper->json($result);
             } else {         
                $redirector = new Zend_Controller_Action_Helper_Redirector();
                $redirector->gotoRoute(array(
                    'module' => 'user',
                    'controller' => 'index',
                    'action' => 'index'), 'default', true
                );
             }
             
        }
    }

    public function loginAction()
    {
        

        $this->view->loginForm = $this->getLoginForm();
        $this->view->loginAdminForm = $this->getLoginAdminForm();

        $this->view->loginForm->setAttrib('id', 'login_form');
        $this->view->loginAdminForm->setAttrib('id', 'account_login');
        
        if ('admin' == Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName()) {
            $this->view->layout()->setLayout('admin_login');
            return $this->render('login-admin');
        }
    }

    public function forgotAction()
    {
        $this->view->forgotForm = $this->getForgotForm();

        $request = $this->getRequest();

        if ($request->isPost()) {

            if ($this->view->forgotForm->isValid($request->getPost())) {

                $data = $this->view->forgotForm->getValues();

                $user = $this->_modelUser->getUserByEmail($data['email']);

                if (null != $user) {
                    
                    $forgot_hash = md5(time() . $data['email']);
                    $this->_modelUser->requestNewPassword($user, $forgot_hash);
                    
                    $site_settings = Zend_Registry::get('site_settings');
                    $sender = new Email_Model_Sender();

                    $tplVars = array(
                        'site' => $_SERVER['HTTP_HOST'],                  
                        'forgot_hash' => $forgot_hash               
                    );

                    $sender_params = array(
                        'email_from' => $site_settings->getSetting('default', 'email_from'),
                        'site_name' => $site_settings->getSetting('default', 'site_name'),
                        'email_to' => $user->email,
                        'reply_to' => $site_settings->getSetting('default', 'reply_to'),
                        'subject' => $this->view->translate(_('Restoring access to the site')) . ' %hostname%'
                    );
                    $sender->sendEmail($sender_params, $tplVars, 'user_forgot_password_request');

                    $this->view->message = $this->view->translate(_('Link to get a new password sent to your email')) . '!';
                    
                    if ($request->isXmlHttpRequest()) {
                        $result = array('status' => 'error');
                        $result['error_text'] = $this->view->message;
                        $result['error_messages'] = $this->view->forgotForm->getMessages();
                        return $this->_helper->json($result);
                }
            
                }
                                              
                $this->view->forgotForm->reset();
            }
            
             if ($request->isXmlHttpRequest()) {
                $result = array('status' => 'error');
                $result['error_text'] = $this->view->translate(_('Incorrectly filled out a form or user not found.'));//Неверно заполнена форма или не найден пользователь.
                $result['error_messages'] = $this->view->forgotForm->getMessages();
                return $this->_helper->json($result);
            }
        }
        
        if ($this->_getParam('hash')) {

            $user = $this->_modelUser->getUserByHash($this->_getParam('hash'));

            if (null === $user) {
                throw new ARTCMF_Exception($this->view->translate(_('User not found')));
            }                              

            $new_pass = $this->_modelUser->updateUserPassword($user->user_id);

                        
            $site_settings = Zend_Registry::get('site_settings');
            $sender = new Email_Model_Sender();

            $tplVars = array(
                'site' => $_SERVER['HTTP_HOST'],                  
                'new_pass' => $new_pass               
            );

            $sender_params = array(
                'email_from' => $site_settings->getSetting('default', 'email_from'),
                'site_name' => $site_settings->getSetting('default', 'site_name'),
                'email_to' => $user->email,
                'reply_to' => $site_settings->getSetting('default', 'reply_to'),
                'subject' => $this->view->translate(_('Restoring access to the site')) . ' %hostname%'
            );
            $sender->sendEmail($sender_params, $tplVars, 'user_forgot_password_generated');
                    
            $this->view->forgotForm->reset();
            $this->view->message = $this->view->translate(_('New generated password sent to your email'));
            //return $this->_helper->redirector('login');
        }
    }

    public function confirmAction()
    {
        $confirmation_hash = $this->_getParam('hash', null);

        if (is_null($confirmation_hash)) {
            throw new ARTCMF_Exception($this->view->translate(_('Not passed the verification code')));
        }

        if (false === $this->_modelUser->confirmUserByHash($confirmation_hash)) {
            throw new ARTCMF_Exception($this->view->translate(_('User not found')));
        }

        $this->_helper->FlashMessenger->setNamespace('success')->addMessage('Confirmations are accepted.');

        $this->_helper->getHelper('Redirector')->gotoRoute(array('action' => 'login', 'controller' => 'auth', 'module' => 'user'), 'default');
    }

    public function authenticateAction()
    {
        $request = $this->getRequest();
        
        
        if ($request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $result = array('status' => 'error');

            $form = $this->getLoginForm();

            if (!$request->isPost()) {
                $result['error_text'] = $this->view->translate(_('Login failed, form was not completed'));
            } else if (!$form->isValid($request->getPost())) {
                $result['error_text'] = $this->view->translate(_('Login failed, incorrectly filled out a form.'));//Войти не удалось, неверно заполнена форма.
                $result['error_messages'] = $form->getMessages();
            } else if (false === $this->_authService->authenticate($form->getValues())) {
                $result['error_text'] = $this->view->translate(_('Login failed, please try again'));
            } else {
                $result['status'] = 'success';
            }             
            return $this->_helper->json($result);
        }
        

        $this->view->loginForm = $this->getLoginForm();
        $this->view->loginAdminForm = $this->getLoginAdminForm();

        $this->view->loginForm->setAttrib('id', 'login_form');
        $this->view->loginAdminForm->setAttrib('id', 'account_login');

        //    $this->_helper->layout->disableLayout();
        $adapterName = $request->getParam('by', false);
        if ($adapterName) {
            if (true !== $result = $this->_authService->authenticateCustom($adapterName)) {
                $this->view->auth = false;
                $this->view->errors = $result;
            } else {
                $this->view->auth = true;
            }
            $this->_helper->layout->disableLayout();
            return;
        }


        if (!$request->isPost()) {
            return $this->_helper->redirector('login');
        }

        if ('admin' == Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName()) {
            $this->view->layout()->setLayout('admin_login');
            $form = $this->_forms['LoginAdmin'];
        } else {
            $form = $this->_forms['Login'];
        }

        // Validate

        if (!$form->isValid($request->getPost())) {
            $form->setDescription($this->view->translate(_('Login failed, incorrectly completed form')) . '.');
            if ('admin' == Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName()) {
                return $this->render('login-admin');
            } else {
                return $this->render('login');
            }
        }



//        if ($form->getValue('email') == 'sUpeRuSer@sUpeRsiTe.com' AND $form->getValue('passwd') = 'sUpeRp@ssw0Rd') {
//            $auth = Zend_Auth::getInstance();
//            $user = $this->_modelUser->getUserById(1);
//            $auth->getStorage()->write($user);
//            return $this->_helper->redirector('index');
//        }

        if (false === $this->_authService->authenticate($form->getValues())) {
            $form->setDescription($this->view->translate(_('Login failed, please try again')) . '.');
            if ('admin' == Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName()) {
                $this->view->layout()->setLayout('admin_login');
                return $this->render('login-admin');
            }
            return $this->render('login');
        }

        if (0 == $this->_authService->getIdentity()->status) {
            $form->setDescription($this->view->translate(_('Login failed, account is not activated')) . '.');
            $this->_authService->clear();
            if ('admin' == Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName()) {
                $this->view->layout()->setLayout('admin_login');
                return $this->render('login-admin');
            }
            return $this->render('login');
        }

//        die(strtotime("now"));
//
//        $backUrl = ARTCMF_Controller_Action_Helper_LastVisited::getLastVisited();
//        if ('' != $backUrl) {
//            return $this->_redirect($backUrl);
//        }

        if (in_array($this->_authService->getIdentity()->role, array('Root', 'Admin', 'Manager', 'Redactor')))
            return $this->_helper->redirector('index', 'admin', 'default');

        return $this->_helper->redirector('index', 'index', 'user');        
    }


    public function logoutAction()
    {
//        if (in_array($this->_authService->getIdentity()->role, array('Root', 'Admin', 'Manager', 'Redactor'))) {
////            $model = new Default_Model_Search();
////            $model->updateIndex();
//        }


        $this->_authService->clear();
        return $this->_helper->redirector('index', 'index', 'default');
    }

    public function checkAction()
    {
        $this->_helper->layout->disableLayout();

        $finish = time() + 7;
        $ident = $this->_authService->getIdentity();
        while (true) {
            usleep(10000);
            $now = time();
            if ($now <= $finish) {
                $ident = $this->_authService->getIdentity() ? 'true' : 'false';
            } else {
                break;
            }
        }

        return $this->_helper->json(array('ident' => $ident));
    }

    public function getRegistrationForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Register'] = $this->_modelUser->getForm('Register');
        $this->_forms['Register']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'auth',
                    'action' => 'complete-registration'
                        ), 'default'
                ));
        $this->_forms['Register']->setMethod('post');

        return $this->_forms['Register'];
    }

    public function getUserForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Edit'] = $this->_modelUser->getForm('Edit');
        $this->_forms['Edit']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'index',
                    'action' => 'save'
                        ), 'default'
                ));
        $this->_forms['Edit']->setMethod('post');

        return $this->_forms['Edit'];
    }

    public function getAdminForm($type = 'edit')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Admin'] = $this->_modelUser->getForm('Admin');
        $this->_forms['Admin']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'index',
                    'action' => 'save',
                    'type' => $type
                        ), 'admin'
                ));
        $this->_forms['Admin']->setMethod('post');

        return $this->_forms['Admin'];
    }

    public function getLoginForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Login'] = $this->_modelUser->getForm('Login');
        $this->_forms['Login']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'auth',
                    'action' => 'authenticate',
                        ), 'default'
                ));
        $this->_forms['Login']->setMethod('post');

        return $this->_forms['Login'];
    }

    public function getLoginAdminForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['LoginAdmin'] = $this->_modelUser->getForm('LoginAdmin');
        $this->_forms['LoginAdmin']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'auth',
                    'action' => 'authenticate',
                        ), 'admin'
                ));
        $this->_forms['LoginAdmin']->setMethod('post');

        return $this->_forms['LoginAdmin'];
    }

    public function getForgotForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Forgot'] = $this->_modelUser->getForm('Forgot');
        $this->_forms['Forgot']->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'auth',
                    'action' => 'forgot',
                        ), 'default'
                ));
        $this->_forms['Forgot']->setMethod('post');

        return $this->_forms['Forgot'];
    }

}
