<?php

/*
 * UserController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class User_GroupController extends Zend_Controller_Action
{

    /**
     * @var User_Model_User
     */
    protected $_modelUser;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelUser = new User_Model_User();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function listAction()
    {
        $this->view->groups = $this->_modelUser->getGroups();
    }

    public function addAction()
    {
        $this->view->groupForm = $this->_getGroupForm('add');
    }

    public function editAction()
    {
        $request = $this->getRequest();

        if (!$request->getParam('group_id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Group not found')));
        }

        $group_id = (int) $request->getParam('group_id');

        $group = $this->_modelUser->getGroupByIdForEdit($group_id);

        $this->view->groupForm = $this->_getGroupForm('edit')->populate($group);
    }

    public function saveAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelUser->saveGroup($request->getPost(), $type)) {
            $this->view->groupForm = $this->_getGroupForm($type);
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Group added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Group updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'group',
                    'module' => 'user'), 'admin', true);
    }

    public function deleteAction()
    {
        $request = $this->getRequest();

        if (false === ($group_id = $request->getParam('group_id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Group not found')));
        }

        $this->_modelUser->deleteGroup($group_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'group',
                    'module' => 'user'), 'admin', true);
    }

    protected function _getGroupForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Group' . ucfirst($type)] = $this->_modelUser->getForm('Group' . ucfirst($type));
        $this->_forms['Group' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'module' => 'user',
                    'controller' => 'group',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms['Group' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Group' . ucfirst($type)];
    }

}
