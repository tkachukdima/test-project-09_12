<?php

return array(
    'Guest' => array(
        'allow' => array(
            'auth' => array(
                'forgot',
                'login',
                'authenticate',
                'register-ajax',
                'register',
                'complete-registration',
                'check'
            )
        )
    ),
    'Member' => array(
        'allow' => array(
            'index' => array(
                'index',
                'save',
                'settings',
                'subscription'
            ),
            'auth' => array(
                'logout'
            ),
//            'subscription' => array(
//                'index',
//                'subscribe',
//                'subscribe-paypal'
//        ),
        ),
        'deny' => array(
            'auth' => array(
                'forgot',
                'login',
                'authenticate',
                'register'
            )
        )
    ),
    'Redactor' => array(
        'allow' => array(
            'management' => array(
                'edit',
                'save',
            )
        )
    ),
    'Manager' => array(
        'allow' => array(
            'group' => array(
                'list',
                'add',
                'edit',
                'save',
                'delete'
            ),
            'management' => array(
                'index',
                'list',
                'add',
                'delete',
                'view'
            ),
//            'subscription' => array(
//                'list',
//                'edit'
//            ),
        )
    )
);