<?php

class User_View_Helper_GetForgotForm extends Zend_View_Helper_Abstract
{

    public function getForgotForm()
    {
        $userModel = new User_Model_User();

        $form = $userModel->getForm('Forgot');
        $form->setAction('/user/auth/forgot');
        $form->setMethod('post');

        return $form;
    }

}