<?php
/**
 * ARTCMF_View_Helper_UserInfo
 * 
 * Access authentication data saved in the session
 * 
 * @category   Default
 * @package    ARTCMF_View_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

/**
 * AuthInfo helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class User_View_Helper_AuthInfo extends Zend_View_Helper_Abstract
{   
    /**
     * @var User_Service_Authentication
     */
    protected $_authService;
    
    /**
     * Get user info from the auth session
     *
     * @param string|null $info The data to fetch, null to chain
     * @return string|Zend_View_Helper_AuthInfo
     */
    public function authInfo ($info = null)
    {
        if (null === $this->_authService) {
            $this->_authService = new User_Service_Authentication();
        }
         
        if (null === $info) {
            return $this;
        }
        
        if (false === $this->isLoggedIn()) {
            return null;
        }
        
        if (false == $this->_authService->getIdentity()) {
            return null;
        }
        
        return $this->_authService->getIdentity()->$info;
    }
    
    /**
     * Check if we are logged in
     *
     * @return boolean
     */
    public function isLoggedIn()
    {
        return $this->_authService->getAuth()->hasIdentity();
    }
}
