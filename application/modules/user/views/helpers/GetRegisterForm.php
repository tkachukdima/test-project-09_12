<?php

class User_View_Helper_GetRegisterForm extends Zend_View_Helper_Abstract
{

    public function getRegisterForm()
    {
        $userModel = new User_Model_User();

        $form = $userModel->getForm('Register');
        $form->setAction('/user/auth/complete-registration');
        $form->setMethod('post');

        return $form;
    }

}