<?php

class User_View_Helper_GetLoginForm extends Zend_View_Helper_Abstract
{

    public function getLoginForm()
    {
        $userModel = new User_Model_User();

        $form = $userModel->getForm('Login');
        $form->setAction('/user/auth/authenticate');
        $form->setMethod('post');

        return $form;
    }

}