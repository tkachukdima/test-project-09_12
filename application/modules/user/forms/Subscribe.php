<?php

/**
 * The base user form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Subscribe extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators
        $this->addPrefixPath(
                'ARTCMF_Form', APPLICATION_PATH . '/../library/ARTCMF/Form/'
        );

        // add path to custom validators
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );
        $this->addElementPrefixPath(
                'User_Validate', APPLICATION_PATH . '/modules/user/models/validate/', 'validate'
        );

        $this->addAttribs(array('id' => 'user_subscribe'));


        $this->addElement('checkbox', 'subscription', array(
            //'required'   => true,
            'label' => _('Subscription'),
            'decorators' => array(array('ViewHelper'))
        ));
   

        $this->addElement('submit', 'submit', array(
            'required' => false,
            'ignore' => true,
            'label' => _('Save'),
            'decorators' => array(
                array('ViewHelper'),
            )
        ));

         $this->setDecorators(array(
            array('ViewScript', array('viewScript' => '_forms/user/subscribe.phtml')),
            'Form'
        ));
         
    }

}
