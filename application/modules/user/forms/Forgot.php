<?php

/**
 * Default_Form_Forgot
 * 
 * The login form
 * 
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Forgot extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $this->setAttrib('id', 'forgot_password');
        $this->setAttrib('class', 'forgot_form');
        $this->setAttrib('onsubmit', 'return $.cmf.submitForm(this);');

        $this->addElement('text', 'email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress'),
            ),
            'required' => true,
            'label' => 'Email',
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Email'))
            )
        ));

        $this->email->setAttrib('placeholder', _('Email'));
        /*
          $this->addElement('captcha', 'captcha', array(
          'label' => _('Enter the code'),
          'captcha' => array(
          'captcha' => 'Image',
          'wordLen' => 4,
          'font' => 'fonts/arial.ttf',
          'imgDir' => './images/captcha',
          'imgUrl' => '/images/captcha',
          'fontsize' => '10',
          'width' => 70,
          'height' => 35,
          'timeout' => 120,
          'expiration' => 0,
          'imgAlt' => 'captcha',
          'DotNoiseLevel' => 2,
          'LineNoiseLevel' => 1
          )
          )
          );
         */

        $this->addElement('submit', 'submit', array(
            'required' => false,
            'ignore' => true,
            'label' => _('Send'),
            'decorators' => array(
                array('ViewHelper'),
            )
        ));
/*
        $this->addElement('hash', 'no_csrf', array(
            'salt' => 'unique',
            'timeout' => 300,
            'ignore' => false,
            'required' => true,
            'decorators' => array(
                array('ViewHelper'),
            )
        ));*/

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => '_forms/user/forgot.phtml')),
            'Form'
        ));
    }

}
