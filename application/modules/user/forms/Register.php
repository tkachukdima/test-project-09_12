<?php

/**
 * The registration form
 * 
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Register extends User_Form_Base
{

    public function init()
    {
        // make sure parent is called!
        parent::init();

        $this->setAttrib('id', 'account_register');
        $this->setAttrib('class', 'reg_form');
        $this->setAttrib('onsubmit', 'return $.cmf.submitForm(this);');

//        $this->addElement('hash', 'no_csrf', array(
//            'salt' => 'unique',
//            'timeout' => 300,
//            'ignore' => false,
//            'required' => true,
//        ));
//        $this->no_csrf->removeDecorator('DtDdWrapper')->removeDecorator('Label');
//        $this->getElement('mobilephone')->setRequired(true);
        //$this->getElement('telephone')->setRequired(true);

//        $this->addElement($this->getElement('captcha'));

         $this->firstname->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->firstname->getLabel()));

        $this->lastname->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->lastname->getLabel()));

        $this->email->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->email->getLabel()));

        $this->passwd->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->passwd->getLabel()));

        $this->passwdVerify->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->passwdVerify->getLabel()));

        $this->ncard->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->ncard->getLabel()));
        
        $this->removeElement('role');
        $this->removeElement('group_id');
        $this->removeElement('status');
        $this->removeElement('user_id');

        $this->removeElement('city');
        $this->removeElement('address');
        $this->removeElement('telephone');
        
        $this->addElement('captcha', 'captcha', array(
                    'label' => _('Enter the code'),
                    'captcha' => array(
                        'captcha' => 'Image',
                        'wordLen' => 4,
                        'font' => 'fonts/arial.ttf',
                        'imgDir' => './images/captcha',
                        'imgUrl' => '/images/captcha',
                        'fontsize' => '10',
                        'width' => 70,
                        'height' => 35,
                        'timeout' => 120,
                        'expiration' => 0,
                        'imgAlt' => 'captcha',
                        'DotNoiseLevel' => 2,
                        'LineNoiseLevel' => 1
                    )
            )
        );
         //$this->captcha->removeDecorator('viewHelper');

          $this->captcha->setDecorators(array(            
            array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),            
            array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
        ));

        $this->addElement('checkbox', 'agreement', array(
            'required' => true,
            //'label' => _('Я принимаю условия <a id="agreement_link" target="_blank" href="/catalog/order/agreement">соглашения</a>'),
            'decorators' => array(array('ViewHelper')),
            'uncheckedValue' => ''
        ));

        $this->getElement('submit')->setLabel(_('Signup'))->setOrder(30)->setDecorators(array(array('ViewHelper')));


        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => '_forms/user/register.phtml')),
            'Form'
        ));
    }

}
