<?php

/**
 * Base category Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Group_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $this->setMethod('post');
        $this->setAction('');


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'name_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));

            $this->addElement('textarea', 'description_' . $lang->code, array(
                'label' => _('Description'),
                'filters' => array('StringTrim'),
                'required' => true,
                'rows' => 6
            ));            
   
        }


        $this->addElement('text', 'color', array(
            'label' => _('Color'),   
            'required' => false
        ));
        
           
        $this->addElement('submit', 'submit', array());

        $this->addElement('hidden', 'group_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'name_' . $lang->code,
                'description_' . $lang->code
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('color', 'submit', 'group_id'), 'form_all', array('legend' => _('General Settings')));
    }    

}
