<?php

/**
 * Add new User
 *
 * @CategoryService   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Group_Add extends User_Form_Group_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('group_id')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
