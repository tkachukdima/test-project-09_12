<?php

/**
 * Default_Form_Register
 *
 * The registration form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Edit extends User_Form_Base
{

    public function init()
    {
        //call the parent init
        parent::init();

        $this->setAttrib('id', 'edit_prodile_form');

        //customize the form
        $this->getElement('passwd')->setRequired(false);
        $this->getElement('passwdVerify')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Save'))->setOrder(30)->setDecorators(array(array('ViewHelper')));

        $this->removeElement('profile_type');
        $this->removeElement('role');
        $this->removeElement('group_id');
        $this->removeElement('status');


        $this->firstname->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->firstname->getLabel()));

        $this->lastname->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->lastname->getLabel()));

        $this->email->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->email->getLabel()));

        $this->passwd->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->passwd->getLabel()));

        $this->passwdVerify->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->passwdVerify->getLabel()));

        $this->city->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->city->getLabel()));

        $this->address->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->address->getLabel()));

        $this->ncard->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->ncard->getLabel()));

        $this->telephone->setDecorators(array(array('ViewHelper')))
                ->setAttribs(array('placeholder' => $this->telephone->getLabel()));

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => '_forms/user/edit.phtml')),
            'Form'
        ));
    }

}