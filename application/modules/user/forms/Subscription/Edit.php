<?php

class User_Form_Subscription_Edit extends User_Form_Subscription_Add
{

    public function init()
    {
        parent::init();

        $validator = new Zend_Validate_Digits();

        $element = new Zend_Form_Element_Hidden('subscription_id', array(
                    'required' => true
                ));
        $element->addValidator($validator);
        $element->removeDecorator('label')
                ->removeDecorator('HtmlTag');
        $this->addElement($element);
    }

}

