<?php

class User_Resource_User_Item extends ARTCMF_Model_Resource_Db_Table_Row_Abstract implements Zend_Acl_Role_Interface
{

    public function getFullname()
    {
        return $this->getRow()->firstname . ' ' . $this->getRow()->lastname;
    }

    public function getGender()
    {
        $gender = (int) $this->getRow()->gender;
        switch ($gender) {
            case User_Model_User::GENDER_MALE:
                return _('Male');
                break;

            case User_Model_User::GENDER_FEMALE:
                return _('Female');
                break;

            default:
                return _('None');
                break;
        }
    }

    public function getRoleId()
    {
        if (null === $this->getRow()->role) {
            return 'Guest';
        }
        return $this->getRow()->role;
    }

}