<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usersocial
 *
 * @author admin
 */
class User_Resource_Usersocial extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'user_socials';
    protected $_primary = array('user_id', 'social_id', 'type');
    protected $_rowClass = 'User_Resource_Usersocial_Item';

    /**
     * 
     * @param array $filter array('user_id', 'social_id', 'type')
     * @return type
     */
    public function getUserSocial($filter)
    {
        $select = $this->select();

        if (!empty($filter['user_id']))
            $select->where('user_id = ?', (int) $filter['user_id']);

        if (!empty($filter['social_id']))
            $select->where('social_id = ?', $filter['social_id']);

        if (!empty($filter['type']))
            $select->where('type = ?', $filter['type']);

        //print_r($select->__toString());die;
        return $this->fetchRow($select);
    }
    
    
    public function deleteSocialAccounts($user_id)
    {
        $this->getAdapter()->delete($this->_name, "user_id = {$user_id}");
    }

}
