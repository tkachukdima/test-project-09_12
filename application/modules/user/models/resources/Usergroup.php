<?php

/**
 * User_Resource_Group
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Resource_Usergroup extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'user_group';
    protected $_primary = 'group_id';
    protected $_rowClass = 'User_Resource_Usergroup_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    

    public function getGroupByIdForSave($id)
    {
        return $this->find($id)->current();
    }

    public function getGroupById($id)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.group_id = {$this->_name}_translation.group_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.group_id = ?', $id);


        return $this->fetchRow($select);
    }

    public function getGroupByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.group_id = {$this->_name}_translation.group_id")
                ->where($this->_name . '.group_id = ?', $id);

        return $this->fetchAll($select);
    }

    public function getGroups()
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.group_id = {$this->_name}_translation.group_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '_translation' . '.name');
        return $this->fetchAll($select);
    }    

    public function saveTranslatedRows($translate_data)
    {

        $this->getAdapter()->insert($this->_name . '_translation', array(
            'group_id' => $translate_data['group_id'],
            'language_code' => $translate_data['language_code'],
            'name' => $translate_data['name'],
            'description' => $translate_data['description']
        ));
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "group_id = {$id}");
    }

}
