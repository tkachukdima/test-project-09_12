<?php

class User_Resource_Subscription extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name     = 'subscriptions';
    protected $_primary  = 'subscription_id';
    protected $_rowClass = 'User_Resource_Subscription_Item';
       
    public function getSubscriptions()
    {
        return $this->fetchAll();
    }

    public function getSubscriptionById($id)
    {
        return $this->find($id)->current();
    }

    public function getSubscriptionsByUserId($user_id)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft('subscriptions_users', $this->_name . '.subscription_id  = subscriptions_users.subscription_id', array('user_id', 'date_start', 'date_end'))
                ->where('subscriptions_users.user_id = ?', (int) $user_id);

        return $this->fetchAll($select);
    }       

}

