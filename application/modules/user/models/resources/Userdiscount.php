<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Userdiscount
 *
 * @author admin
 */
class User_Resource_Userdiscount extends ARTCMF_Model_Resource_Db_Table_Abstract
{
    protected $_name = 'user_discount';
    protected $_primary = 'discount_id';
    protected $_rowClass = 'User_Resource_Userdiscount_Item';

    public function getUserdiscountByUserId($user_id)
    {
      $select = $this->select();
      $select->where('user_id = ?', $user_id);
      return $this->fetchRow($select);
    }
    
    
    
}
