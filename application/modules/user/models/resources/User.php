<?php

/**
 * User_Resource_User
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Resource_User extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name     = 'user';
    protected $_primary  = 'user_id';
    protected $_rowClass = 'User_Resource_User_Item';

    public function getUserById($id)
    {
        return $this->find($id)->current();
    }

    public function getUserByEmail($email, $ignoreUser = null)
    {
        $select = $this->select();
        $select->where('email = ?', $email);
         
        if (!is_null($ignoreUser) AND !empty($ignoreUser->email)) {
            $select->where('email <> ?', $ignoreUser->email);
        }
        
        return $this->fetchRow($select);
    }
    
     public function getUserByHash($hash)
    {
        $select = $this->select();
        $select->where('forgot_hash = ?', $hash);

        return $this->fetchRow($select);
    }
    
    public function getUserByConfirmationHash($confirmation_hash)
    {
        $select = $this->select();
        $select->where('confirmation_hash = ?', $confirmation_hash);

        return $this->fetchRow($select);
    }

    public function getUserBySubScription($subs_status)
    {
        $select = $this->select();
        $select->where('subscription = ?', $subs_status);

        return $this->fetchAll($select);
    }

    public function getUsers($paged = null, $order = null, $filter_data = null)
    {
        $select = $this->select()
                 ->from($this->_name)
                 ->setIntegrityCheck(false)
                 ->joinLeft('session', "{$this->_name}.user_id = session.user_id", array('modified'))
                 ->joinLeft('user_group', "{$this->_name}.group_id = user_group.group_id", array('group_color' => 'color'))
                 ->order('session.modified DESC');

        if (true === is_array($order)) {
            $select->order($order);
        }

        if (isset($filter_data['filter_name']) && !is_null($filter_data['filter_name'])) {
            $select->where("LCASE({$this->_name}.firstname) LIKE ? OR LCASE({$this->_name}.lastname) LIKE ?", strtolower('%' . $filter_data['filter_name'] . '%')  );
        }

        if (isset($filter_data['filter_city']) && !is_null($filter_data['filter_city'])) {
            $select->where("LCASE({$this->_name}.city) LIKE ?", strtolower('%' . $filter_data['filter_city'] . '%'));
        }

        if (isset($filter_data['filter_email']) && !is_null($filter_data['filter_email'])) {
            $select->where("LCASE({$this->_name}.email) LIKE ?", strtolower($filter_data['filter_email'] . '%'));
        }

        if (isset($filter_data['filter_status']) && !is_null($filter_data['filter_status']) && 'any' != $filter_data['filter_status']) {
            $select->where("LCASE({$this->_name}.status) = ?", $filter_data['filter_status']);
        }



        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count   = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->reset(Zend_Db_Select::ORDER);
            $count->from('user', new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'));
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage(30)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }
    
    public function getEndingSubscriptions($period)
    {
        $select = $this->select()
                ->where("subscription_date_end < CURRENT_TIMESTAMP + INTERVAL {$period} DAY")
                ->where("notified = 0")
                ->where("trial = 2");
         return $this->fetchAll($select);
    }
    
    public function getEndedSubscriptions()
    {
        $select = $this->select()
                ->where("subscription_date_end < CURRENT_TIMESTAMP + INTERVAL 0 DAY");
         return $this->fetchAll($select);
    }

}