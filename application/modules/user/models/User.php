<?php

/**
 * User_Model_User
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Model_User extends ARTCMF_Model_Acl_Abstract
{
    
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    public static function getRolesOptions($role = 'Admin') {
        switch ($role) {
            case 'Root':
                return array(
                    'Member' => _('Member'),
                    'Redactor' => _('Redactor'),
                    'Manager' => _('Manager'),
                    'Admin' => _('Admin'),
                    'Root' => _('Root')
                );

                break;
            case 'Admin':
                return array(
                    'Member' => _('Member'),
                    'Redactor' => _('Redactor'),
                    'Manager' => _('Manager'),
                    'Admin' => _('Admin')
                );

                break;

            case 'Manager':
                return array(
                    'Member' => _('Member'),
                    'Redactor' => _('Redactor'),
                    'Manager' => _('Manager')
                );

                break;

            default:
                return array(
                    'Member' => _('Member'),
                    'Redactor' => _('Redactor')
                );
                break;
        }
    }        

    /**
     * Get User by their id
     * 
     * @param  int $id
     * @return null|User_Resource_User_Item 
     */
    public function getUserById($id)
    {
        $id = (int) $id;
        return $this->getResource('User')->getUserById($id);
    }

    
    /**
     * Get User by their email address
     *
     * @param  string $email The email address to search for
     * @param  User_Resource_User_Item $ignoreUser User to ignore from the search
     * @return null|User_Resource_User_Item 
     */
    public function getUserByEmail($email, $ignoreUser = null)
    {
        return $this->getResource('User')->getUserByEmail($email, $ignoreUser);
    }

    /**
     * Get all Users
     * 
     * @param  boolean $paged Return paginator?
     * @param  array   $order The order fields
     * @return Zend_Db_Table_Rowset
     */
    public function getUsers($paged = null, $order = null, $filter_data = null)
    {
        return $this->getResource('User')->getUsers($paged, $order, $filter_data);
    }

    /**
     * Get User by their hash 
     *
     * @param  string $email The email address to search for
     * @param  User_Resource_User_Item $ignoreUser User to ignore from the search
     * @return null|User_Resource_User_Item 
     */
    public function getUserByHash($hash)
    {
        return $this->getResource('User')->getUserByHash($hash);
    }

    /**
     * Get User by subscription 
     *
     * @param  string $email The email address to search for
     * @param  User_Resource_User_Item $ignoreUser User to ignore from the search
     * @return null|User_Resource_User_Item 
     */
    public function getUserBySubScription($subs_status)
    {
        return $this->getResource('User')->getUserBySubScription($subs_status);
    }

    /**
     * Register a new user
     * 
     * @param array $post
     * @return false|int 
     */
    public function registerUser($post)
    {
        if (!$this->checkAcl('register')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $form = $this->getForm('Register');

        return $this->createUser($form, $post);
    }

    /**
     * Create a new user
     * 
     * @param array $post
     * @return false|int 
     */
    public function createUser($form, $post)
    {               
        
        $confirmation_hash = md5($post['email'] . $this->createSalt());
        $created_at = date('Y-m-d H:i:s');
        
        $new_user_id = $this->save($form, $post, array(
            'status' => '1',
            'role' => 'Member',
            'confirmation_hash' => $confirmation_hash,
            'created_at' => $created_at));
        
        if (false === $new_user_id)
            return false;

        $site_settings = Zend_Registry::get('site_settings');
        $sender        = new Email_Model_Sender();

        $tplVars = array(
            'site'              => $_SERVER['HTTP_HOST'],
            'firstname'         => $post['firstname'],
            'lastname'          => $post['lastname'],
            'email'             => $post['email'],
            'ncard'             => $post['ncard'],
            'passwd'            => $post['passwd'],
            'confirmation_hash' => $confirmation_hash
        );

        $emailsTo = explode(";", $site_settings->getSetting('default', 'email_to'));
        foreach ($emailsTo as $emailTo) {
            $params = array(
                'email_from' => $site_settings->getSetting('default', 'email_from'),
                'site_name'  => $site_settings->getSetting('default', 'site_name'),
                'email_to'   => $emailTo,
                'reply_to'   => $site_settings->getSetting('default', 'reply_to'),
                'subject'    => "На сайте %hostname% зарегистрировался новый пользователь"
            );
            $sender->sendEmail($params, $tplVars, 'user_new_admin');
        }

        $params = array(
            'email_from' => $site_settings->getSetting('default', 'email_from'),
            'site_name'  => $site_settings->getSetting('default', 'site_name'),
            'email_to'   => $post['email'],
            'reply_to'   => $site_settings->getSetting('default', 'reply_to'),
            'subject'    => "Вы успешно зарегистрировались на сайте %hostname%"
        );
        $sender->sendEmail($params, $tplVars, 'user_new_register');


        return $new_user_id;
    }

    /**
     * Update a user
     * 
     * @param  array  $post The data
     * @param  string $validator Which validation chain to use
     * @return false|int
     */
    public function saveUser($post, $validator = null)
    {
        if (!$this->checkAcl('saveUser')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'Edit';
        }

        $form = $this->getForm($validator);

        return $this->save($form, $post, array(
            'updated_at' => date('Y-m-d H:i:s'),
            ));
    }

    /**
     * Update a forgot_hash field in user
     * 
     * @param  array  $user User_Resource_User_Item 
     * @param  string $forgot_hash md5 string
     * @return false|int
     */
    public function requestNewPassword($user, $forgot_hash)
    {
        $data                = $user->toArray();
        $data['forgot_hash'] = $forgot_hash;

        return $this->getResource('User')->saveRow($data, $user);
    }
    
    /**
     * Получить пользователя по хешу для подтверждения регистрации
     * 
     * @param string $confirmation_hash 
     * @return false|int
     */
    public function confirmUserByHash($confirmation_hash)
    {
        $user = $this->getResource('User')->getUserByConfirmationHash($confirmation_hash);

        if (null === $user)
            return false;

        if (0 == $user->status)
            $user->status = 1;

        return $user->save();
    }
    
    
    public function updateUserPassword($user_id)
    {
        $new_passwd = substr(md5(time(). $user_id), 0, 8);
        
        $salt   = md5($this->createSalt());
        $passwd = sha1($new_passwd . $salt);
        
        $user = $this->getUserById($user_id);
        $user->salt = $salt;
        $user->passwd = $passwd;
        $user->forgot_hash = '';
        $user->save();
        
        return $new_passwd;
    }

    /**
     * Save the data to db
     *
     * @param  Zend_Form $form The Validator
     * @param  array     $info The data
     * @param  array     $defaults Default values
     * @return false|int 
     */
    protected function save($form, $info, $defaults = array())
    {


        if (!$form->isValid($info)) {
            return false;
        }

        // get filtered values
        $data = $form->getValues();


        // password hashing
        if (array_key_exists('passwd', $data) && '' != $data['passwd']) {
            $data['salt']   = md5($this->createSalt());
            $data['passwd'] = sha1($data['passwd'] . $data['salt']);
        } else {
            unset($data['passwd']);
        }
        
        if(isset($data['date_of_birthday']))
            $data['date_of_birthday'] = date('Y-m-d H:i:s', strtotime($data['date_of_birthday']));
        
        // apply any defaults
        foreach ($defaults as $col => $value) {
            $data[$col] = $value;
        }
        
       // print_r($data); die;

        $user = array_key_exists('user_id', $data) ?
                $this->getResource('User')->getUserById($data['user_id']) : null;



        // если статус изменился отправить письмо пользователю,
        // об изменении его статуса

        if (!is_null($user) AND $data['status'] != $user->status) {

            $new_status = ($data['status'] == 1) ? 'активирован' : 'заблокирован';
            $mail_tpl   = ($data['status'] == 1) ? 'user_activate' : 'user_deactivate';

            $site_settings = Zend_Registry::get('site_settings');
            $sender        = new Email_Model_Sender();

            $tplVars = array(
                'site'       => $_SERVER['HTTP_HOST'],
                'firstname'  => $data['firstname'],
                'lastname'   => $data['lastname'],
                'email'      => $data['email'],
                'new_status' => $new_status
            );


            $params = array(
                'email_from' => $site_settings->getSetting('default', 'email_from'),
                'site_name'  => $site_settings->getSetting('default', 'site_name'),
                'email_to'   => $data['email'],
                'reply_to'   => $site_settings->getSetting('default', 'reply_to'),
                'subject'    => "Ваш аккаунт на сайте %hostname% был {$new_status} администратором"
            );
            $sender->sendEmail($params, $tplVars, $mail_tpl);
        }

        return $this->getResource('User')->saveRow($data, $user);
    }
    
    
     /**
     * Save the data to db
     *
     * @param  Zend_Form $form The Validator
     * @param  array     $info The data
     * @param  array     $defaults Default values
     * @return false|int 
     */
    public function registerUserSocial($data)
    {

        // если такой соц акк уже есть
        $social = $this->getResource('Usersocial')->getUserSocial(array(
            'social_id' => $data['social']['social_id'],
            'type' => $data['social']['type'],
                ));

        //print_r($social);die;


        if (!is_null($social)) {
            $user_id = $social->user_id;
        } else {

            $user_data = array(
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'gender' => $data['gender'],
                'email' => $data['email'],
                'created_at' => date('Y-m-d H:i:s'),
                'status' => 1
            );

            if (!is_null($data['email'])) {

                $user = $this->getUserByEmail($data['email']);
                if (!is_null($user)) {
                    $user_id = $user->user_id;
                } else {
                    $user_id = $this->getResource('User')->saveRow($user_data);
                }
            } else {
                $user_id = $this->getResource('User')->saveRow($user_data);
            }

            $this->getResource('Usersocial')->saveRow(array(
                'user_id' => $user_id,
                'social_id' => $data['social']['social_id'],
                'type' => $data['social']['type'],
                'username' => $data['social']['username'],
            ));
        }

        return $this->getUserById($user_id);
    }

    /**
     * Delete a user
     *
     * @param int|User_Resource_User_Item_Interface $user
     * @return boolean
     */
    public function deleteUser($user)
    {
        if (!$this->checkAcl('deleteUser')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if ($user instanceof User_Resource_User_Item) {
            $user_id = (int) $user->user_id;
        } else {
            $user_id = (int) $user;
        }

        $user = $this->getUserById($user_id);

        if (null !== $user) {
            if ('' != $user->email) {
                $site_settings = Zend_Registry::get('site_settings');
                $sender = new Email_Model_Sender();

                $tplVars = array(
                    'site' => $_SERVER['HTTP_HOST']
                );

                $params = array(
                    'email_from' => $site_settings->getSetting('default', 'email_from'),
                    'site_name' => $site_settings->getSetting('default', 'site_name'),
                    'email_to' => $user->email,
                    'reply_to' => $site_settings->getSetting('default', 'reply_to'),
                    'subject' => "Ваш аккаунт на сайте %hostname% был удален администратором"
                );
                $sender->sendEmail($params, $tplVars, 'user_delete');
            }
            
            $this->getResource('Usersocial')->deleteSocialAccounts($user_id);       
                        
            $user->delete();
            return true;
        }
         

        return false;
    }
    
    
     /**
     * 
     * @param array $filter array('user_id', 'social_id', 'type')
     * @return type
     */
    public function getUserSocial($filter)
    {        
        return $this->getResource('Usersocial')->getUserSocial($filter);        
    }
    
       
    public function getGroups()
    {
        return $this->getResource('Usergroup')->getGroups();
    }
    
    public function getGroupById($id)
    {
        return $this->getResource('Usergroup')->getGroupById($id);
    }
  
    public function getGroupByIdForEdit($id)
    {
        $id = (int) $id;

        $page = $this->getResource('Usergroup')->getGroupByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['name_' . $value['language_code']] = $value['name'];
            $data['description_' . $value['language_code']] = $value['description'];           
        }

        return $data;
    }
    
    /**
     * Save a category
     * 
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveGroup($data, $validator = null)
    {
        
        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('Group' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $group = array_key_exists('group_id', $data) ?
                $this->getResource('Usergroup')->getGroupByIdForSave($data['group_id']) : null;

        $new = $this->getResource('Usergroup')->saveRow($data, $group);

        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'group_id' => (int) $new,
                'language_code' => $lang->code,
                'name' => $data['name_' . $lang->code],
                'description' => $data['description_' . $lang->code]
            );

            $this->getResource('Usergroup')->deleteTranslatedRows($new);

            $this->getResource('Usergroup')->saveTranslatedRows($translate_data);
        }
       
        return $new;
    }
    
     public function deleteGroup($group)
    {
        
        if ($group instanceof User_Resource_Usergroup_Item) {
            $group_id = (int) $group->group_id;
        } else {
            $group_id = (int) $group;
        }

        $group = $this->getResource('Usergroup')->getGroupByIdForSave($group_id);

        if (null !== $group) {
            $this->getResource('Usergroup')->deleteTranslatedRows($group_id);
            $group->delete();           
            return true;
        }

        return false;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     * 
     * @return string The resource id 
     */
    public function getResourceId()
    {
        return 'User';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     * 
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Guest', $this, array('register', 'saveUser'))
                    ->allow('Member', $this, array('updateUser'))
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     * 
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

    /**
     * Create the salt string
     * 
     * @return string 
     */
    private function createSalt()
    {
        $salt = '';
        for ($i    = 0; $i < 50; $i++) {
            $salt .= chr(rand(33, 126));
        }
        return $salt;
    }

}