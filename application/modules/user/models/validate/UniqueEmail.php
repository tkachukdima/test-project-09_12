<?php

class User_Validate_UniqueEmail extends Zend_Validate_Abstract
{

    const EMAIL_EXISTS = 'emailExists';

    protected $_messageTemplates = array(
        self::EMAIL_EXISTS => "Email '%value%' already exists in our system",
    );

    public function __construct(User_Model_User $model)
    {
        $this->_model = $model;
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue((string) $value);

        $currentUser = isset($context['user_id']) ? $this->_model->getUserById($context['user_id']) : null;
        $user        = $this->_model->getUserByEmail($this->_value, $currentUser);

        if (null === $user) {
            return true;
        }

        $this->_error(self::EMAIL_EXISTS);
        return false;
    }

}
