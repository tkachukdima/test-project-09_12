<?php

class User_Model_Subscription extends ARTCMF_Model_Acl_Abstract
{

    protected $errors = array();

    public function getSubscriptions()
    {
        return $this->getResource('Subscription')->getSubscriptions();
    }

    public function getSubscriptionById($id)
    {
        return $this->getResource('Subscription')->getSubscriptionById($id);
    }

    public function saveSubscription($rawdata, $validator = null)
    {

        if (null === $validator) {
            $validator = 'edit';
        }

        $form = $this->getForm('Subscription' . ucfirst($validator));

        if (!$form->isValid($rawdata)) {
            return false;
        };

        $data = $form->getValues();

        $subscription = array_key_exists('subscription_id', $data) ?
                $this->getResource('Subscription')->getSubscriptionById($data['subscription_id']) : null;

        return $this->getResource('Subscription')->saveRow($data, $subscription);
    }

    public function deleteSubscription($subscription_id)
    {
        $subscription = $this->getResource('Subscription')->getSubscriptionById($subscription_id);
        $subscription->delete();
        return true;
    }

    public function getSubscriptionsByUserId($user_id)
    {
        return $this->getResource('Subscription')->getSubscriptionsByUserId($user_id);
    }

    /**
     * Subscribe
     * @param {$subscription_id} subscription_id
     * @param {$user_id}  user_id 
     * @param {$period}  period = 1
     */
    public function subscribe($subscription_id, $user_id, $period = 1)
    {

        $subscription = $this->getResource('Subscription')->getSubscriptionById($subscription_id);
        if (is_null($subscription)) {
            $this->errors[] = _('Subscription not found');
            return false;
        }

        $user = $this->getResource('User')->getUserById($user_id);
        if (is_null($user)) {
            $this->errors[] = _('User not found');
            return false;
        }

        //проверить, не подписан ли юзер
        
        if ($subscription->type == 'regular') {
            if (0 !== (int) $user->subscription_id) {
                $date_end = date('Y-m-d H:i:s', strtotime($user->subscription_date_end) + 60 * 60 * 24 * 30 * $period);
            } else {
                $date_end = date('Y-m-d H:i:s', time() + 60 * 60 * 24 * 30 * $period);
            }
        } else {
            $date_end = date('Y-m-d H:i:s', time() + 60 * 60 * 24 * 30 * 12 * 10); //10 years
        }

        //снять со счета деньги
        $user->subscription_id = $subscription->subscription_id;
        $user->subscription_date_end = $date_end;
        $user->save();


        $site_settings = Zend_Registry::get('site_settings');
        $sender        = new Email_Model_Sender();


        //@TODO send email to admin('s)
        $tplVars = array(
            'site'                  => $_SERVER['HTTP_HOST'],
            'firstname'             => $post['firstname'],
            'lastname'              => $post['lastname'],
            'user_id'               => $user->user_id,
            'amount'                => $subscription->cost,
            'subscription_name'     => $subscription->name,
            'subscription_date_end' => $subscription->subscription_date_end
        );


        $emailsTo = explode(";", $site_settings->getSetting('default', 'email_to'));

        foreach ($emailsTo as $emailTo) {
            $params = array(
                'email_from' => $site_settings->getSetting('default', 'email_from'),
                'site_name'  => $site_settings->getSetting('default', 'site_name'),
                'email_to'   => $emailTo,
                'reply_to'   => $site_settings->getSetting('default', 'reply_to'),
                'subject'    => "На сайте {$_SERVER['HTTP_HOST']} подписался пользователь"
            );
            $sender->sendEmail($params, $tplVars, 'user_subscribed_admin');
        }

        //@TODO send email to user

        $tplVars = array(
            'site'                  => $_SERVER['HTTP_HOST'],
            'firstname'             => $post['firstname'],
            'lastname'              => $post['lastname'],
            'user_id'               => $user->user_id,
            'amount'                => $subscription->cost,
            'subscription_name'     => $subscription->name,
            'subscription_date_end' => $subscription->subscription_date_end
        );

        $params = array(
            'email_from' => $site_settings->getSetting('default', 'email_from'),
            'site_name'  => $site_settings->getSetting('default', 'site_name'),
            'email_to'   => $user->email,
            'reply_to'   => $site_settings->getSetting('default', 'reply_to'),
            'subject'    => "Ваша подписка активирована."
        );
        $sender->sendEmail($params, $tplVars, 'user_subscribed');


        return true;
    }

    /**
     * Return true if user is subscribed and is not out of date
     * @param {$subscription_id} subscription_id
     * @param {$user_id}  user_id 
     * @param {$period}  period = 1
     */
    public function closeFinishedSubscriptions()
    {
        $userModel = new User_Model_User();

        $users = $userModel->getUsers();

        $sender = new Email_Model_Sender();
        
        $site_settings = Zend_Registry::get('site_settings');

        foreach ($users as $user) {

            $subscription = $this->getSubscriptionById($user->subscription_id);

            if (strtotime($user->subscription_date_end) > time()) {

                $user->notified = 1;
                $user->subscription_id = 0;
                $user->subscription_date_end = '0000-00-00 00:00:00';
                $user->save();

                $tplVars = array(
                    'site'                  => $_SERVER['HTTP_HOST'],
                    'user_id'               => $user->user_id,
                    'amount'                => $subscription->cost,
                    'subscription_name'     => $subscription->name,
                    'subscription_period'   => $subscription->period,
                    'subscription_date_end' => $user->subscription_date_end
                );

                $params = array(
                    'email_from' => $site_settings->getSetting('default', 'email_from'),
                    'site_name'  => $site_settings->getSetting('default', 'site_name'),
                    'email_to'   => $user->email,
                    'reply_to'   => $site_settings->getSetting('default', 'email_from'),
                    'subject'    => "Ваша подписка окончена"
                );
                $sender->sendEmail($params, $tplVars, 'user_finished_subscribtion');
            }
        }

        return true;
    }

    /**
     * Return true if user is subscribed and is not out of date
     * @param {$subscription_id} subscription_id
     * @param {$user_id}  user_id 
     * @param {$period}  period = 1
     */
    public function isSubscribed($subscription_id, $user_id)
    {

        $userModel = new User_Model_User();
        $user = $userModel->getUserById($user_id);
        if (!is_null($user)) {
            if ($subscription_id == 1 AND $user->course == 1) { //обучение
                return true;
            } elseif ($subscription_id == $user->subscription_id AND time() < strtotime($user->subscription_date_end)) { //терминал
                return true;            
            } 
        }

        return false;
    }

    /**
     * Return true if user is subscribed and is not out of date
     * @param {$period}  period = 1
     */
    public function getEndingSubscriptions($period)
    {
        return $this->getResource('User')->getEndingSubscriptions($period);
    }
    
    /**
     * Return true if user is subscribed and is not out of date
     * @param {$period}  period = 1
     */
    public function getEndedSubscriptions()
    {
        return $this->getResource('User')->getEndedSubscriptions();
    }
    
 
    public function getErrors()
    {
        $errors = $this->errors;
        $this->errors = array();
        return $errors;
    }

    public function getAcl()
    {
        return $this->_acl;
    }

    public function getResourceId()
    {
        return 'Subscription';
    }

    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }

        $this->_acl = $acl;
        return $this;
    }

}

