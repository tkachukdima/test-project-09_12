<?php

/**
 * File_Resource_Group
 *
 * @category   Default
 * @package    File_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Resource_Group extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    const GROUP_TYPE_FILE = 'file';
    const GROUP_TYPE_IMAGE = 'image';
    const GROUP_TYPE_VIDEO = 'video';

    protected $_name = 'file_group';
    protected $_primary = 'group_id';
    protected $_rowClass = 'File_Resource_Group_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getGroupById($id, $with_translate = false)
    {
        if ($with_translate) {
            $select = $this->select()
                    ->from($this->_name)
                    ->setIntegrityCheck(false)
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.group_id = {$this->_name}_translation.group_id")
                    
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                    ->where("{$this->_name}.group_id = ?", $id);

            return $this->fetchRow($select);
        } else {
            return $this->find($id)->current();
        }
    }


     public function getGroupByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.group_id = {$this->_name}_translation.group_id")
                
                ->where("{$this->_name}.group_id = ?", $id);

        return $this->fetchAll($select);
    }
    

    /**
     * Get a list of file
     *
     * @param  boolean   $filed      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getGroups($only_active = false)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.group_id = {$this->_name}_translation.group_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang);
        
        if ($only_active)
            $select->where($this->_name . '.status = ?', 1);
      
        return $this->fetchAll($select);
    }

     public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'group_id' => $translate_data['group_id'],
                    'language_code' => $translate_data['language_code'],
                    'title' => $translate_data['title'],
                    'description' => $translate_data['description']
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'title' => $translate_data['title'],
                    'description' => $translate_data['description']
                        ), "`group_id` = {$translate_data['group_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "group_id = {$id}");
    }


}