<?php

/**
 * File_Resource_File
 *
 * @category   Default
 * @package    File_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Resource_File extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'file';
    protected $_primary = 'file_id';
    protected $_rowClass = 'File_Resource_File_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getFileById($id, $with_translate = false)
    {
        if ($with_translate) {
            $select = $this->select()
                    ->from($this->_name)
                    ->setIntegrityCheck(false)
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.file_id = {$this->_name}_translation.file_id")
                    
                    ->joinLeft('file_group', $this->_name . '.group_id  = file_group.group_id', array('type'))
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                    ->where("{$this->_name}.file_id = ?", $id);

            return $this->fetchRow($select);
        } else {
            return $this->find($id)->current();
        }
    }

    
    public function getFileByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.file_id = {$this->_name}_translation.file_id")
                
                ->where("{$this->_name}.file_id = ?", $id);

        return $this->fetchAll($select);
    }

    
    /**
     * Get a list of file
     *
     * @param  boolean   $filed      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getFiles($only_active = false)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.file_id = {$this->_name}_translation.file_id")
                
                ->joinLeft('file_group', $this->_name . '.group_id  = file_group.group_id', array('type'))
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '.sort_order');

        if ($only_active)
            $select->where($this->_name . '.status = ?', 1);

        return $this->fetchAll($select);
    }
        
    /**
     * Get a list of file
     *
     * @param  boolean   $filed      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getFilesByGroupId($typeId, $only_active = false)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.file_id = {$this->_name}_translation.file_id")
                
                ->joinLeft('file_group', $this->_name . '.group_id  = file_group.group_id', array('type'))
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.group_id = ?', $typeId)
                ->order($this->_name . '.sort_order');

        if ($only_active)
            $select->where($this->_name . '.status = ?', 1);

        return $this->fetchAll($select);
    }    
    
    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'file_id' => $translate_data['file_id'],
                    'language_code' => $translate_data['language_code'],
                    'name' => $translate_data['name'],
                    'description' => $translate_data['description']
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'name' => $translate_data['name'],
                    'description' => $translate_data['description']
                        ), "`file_id` = {$translate_data['file_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "file_id = {$id}");
    }

    
}