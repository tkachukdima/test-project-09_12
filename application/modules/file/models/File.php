<?php

class File_Model_File extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get a file post by its id
     *
     * @param  string $ident The ident
     * @return File_Resource_File_Item
     */
    public function getFileById($id, $with_translate = false)
    {
        $id = (int) $id;
        return $this->getResource('File')->getFileById($id, $with_translate);
    }

    /**
     * Get a File by its id for edit
     *
     * @param  int $id The id
     * @return File_Resource_File_Item
     */
    public function getFileByIdForEdit($id)
    {
        $id = (int) $id;
        $page = $this->getResource('File')->getFileByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['name_' . $value['language_code']] = $value['name'];
            $data['description_' . $value['language_code']] = $value['description'];
        }
        return $data;
    }


    /**
     * Get file
     *
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getFiles($only_active = false)
    {
        return $this->getResource('File')->getFiles($only_active);
    }
    
    /**
     * Get file
     *
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getFilesByGroupId($groupId, $only_active = false)
    {
            return $this->getResource('File')->getFilesByGroupId($groupId, $only_active);
    }

    /**
     * Save a file post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveFile($data, $validator = null)
    {
        if (!$this->checkAcl('saveFile')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }        

        $validator = $this->getForm('File' . ucfirst($validator));

        $lang = Zend_Registry::get('langList');

        $group = $this->getGroupById($data['group_id']);
        
        if (File_Resource_Group::GROUP_TYPE_IMAGE == $group->type) {
            $validator->getElement('file')->addValidators(array(array('Extension', false, array('jpg', 'jpeg', 'png', 'gif'))));
        } else if (File_Resource_Group::GROUP_TYPE_VIDEO == $group->type) {
            $validator->getElement('file')->addValidators(array(array('Extension', false, array('flv', 'mp4'))));
        } else {
             $validator->getElement('file')->addValidators(array(array('Extension', false, array('zip', 'txt', 'doc', 'docx', 'pdf', 'rtf', 'rar', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif'))));
        }


        if (!$validator->isValid($data)) {            
            return false;
        }

        $data = $validator->getValues();

        $file = array_key_exists('file_id', $data) ?
                $this->getResource('File')->getFileById($data['file_id']) : null;
       
        
        if ($file->p_full != '' AND $data['p_full'] == '') {
            $data['p_full'] = $file->p_full;
            $data['p_thumbnail'] = $file->p_thumbnail;
            $data['p_preview'] = $file->p_preview;
        }
        
        if ($file AND $data['file'] == '') {
            $data['file'] = $file->file;
        }
                              
        $newId =  $this->getResource('File')->saveRow($data, $file);

        $data['file_id'] = $newId;
        
        
        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/file');

        $filter = new ARTCMF_Filter_ImageSize();

        $path_parts = pathinfo($data['p_full']);
        $new_file_name = 'file-image-' .  $newId . '.' . $path_parts['extension'];
        copy($fileDestination . '/' . $data['p_full'], $fileDestination . '/' . $new_file_name);


        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel('file_preview');


        $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                ->setHeight($image_resize_setting['height_thumbnail'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                ->filter($fileDestination . '/' . $new_file_name);

        $preview = $filter->setWidth($image_resize_setting['width_preview'])
                ->setHeight($image_resize_setting['height_preview'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_preview']())
                ->filter($fileDestination . '/' . $new_file_name);

         $full = $filter->setWidth($image_resize_setting['width_full'])
                ->setHeight($image_resize_setting['height_full'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_full']())
                ->filter($fileDestination . '/' . $new_file_name);


        $data['p_thumbnail'] = basename($thumbnail);
        $data['p_preview'] = basename($preview);
        $data['p_full'] = basename($full);
        
        //--------
        
        $fileUpload = realpath(APPLICATION_PATH . '/../www/upload');
        $fileDestination = realpath(APPLICATION_PATH . '/../www/upload/' . $group->type);
       
        $path_parts = pathinfo($data['file']);
        $new_file_name = 'file-' .  $newId . '.' . $path_parts['extension'];
        rename($fileUpload . '/' . $data['file'], $fileDestination . '/' . $new_file_name);
        $data['file'] = $new_file_name;
   
        if(File_Resource_Group::GROUP_TYPE_IMAGE == $group->type){
             $filter = new ARTCMF_Filter_ImageSize();
             $settingModel = new Default_Model_Settings();
             $image_resize_setting = $settingModel->getImageResizeSettingForModel('file');

            $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                    ->setHeight($image_resize_setting['height_thumbnail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                    ->filter($fileDestination . '/' . $new_file_name);

             $preview = $filter->setWidth($image_resize_setting['width_preview'])
                    ->setHeight($image_resize_setting['height_preview'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_preview']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $full = $filter->setWidth($image_resize_setting['width_full'])
                    ->setHeight($image_resize_setting['height_full'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_full']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $settings = Zend_Registry::get('site_settings');
            if('' != $settings->getSetting('default', 'watermark')){
                $watermark = new ARTCMF_Filter_Watermark();
                $img = imagecreatefromjpeg($full);
                $im = $watermark->create_watermark($img, $settings->getSetting('default', 'watermark'), APPLICATION_PATH . "/../www/fonts/arial.ttf", 128, 128, 128, 64);
                imagejpeg($im, $full, 100);
            }
            
            $data['thumbnail'] = basename($thumbnail);
            $data['preview'] = basename($preview);
            $data['full'] = basename($full);
        }

        $new_file = $this->getResource('File')->getFileById($newId);
        
        $this->getResource('File')->deleteTranslatedRows($newId);
        
         foreach (Zend_Registry::get('langList') as $lang) {
            $translate_data = array(
                'file_id' => (int) $newId,
                'language_code' => $lang->code,
                'name' => $data['name_' . $lang->code],
                'description' => $data['description_' . $lang->code]
            );   

            $this->getResource('File')->saveTranslatedRows($translate_data,  'insert');

        }

         $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('file')
        );
         
        return $this->getResource('File')->saveRow($data, $new_file);
        
          
    }

    public function deleteFile($id)
    {
        if (!$this->checkAcl('deleteFile')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $file = $this->getFileById($id);
        if (null !== $file) {
            $fileDestination = realpath(APPLICATION_PATH . '/../www/upload/files');
            unlink($fileDestination . '/' . $file->file);
            $this->getResource('File')->deleteTranslatedRows($id);
            $file->delete();
            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('file')
            );
            return true;
        }

        return false;
    }
    
    
     /**
     * Get a Group post by its id
     *
     * @param  string $ident The ident
     * @return File_Resource_File_Item
     */
    public function getGroupById($id, $with_translate = false)
    {
        $id = (int) $id;
        
        return $this->getResource('Group')->getGroupById($id, $with_translate);
    }

    /**
     * Get a File by its id for edit
     *
     * @param  int $id The id
     * @return File_Resource_File_Item
     */
    public function getGroupByIdForEdit($id)
    {
        $id = (int) $id;
        $page = $this->getResource('Group')->getGroupByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['title_' . $value['language_code']] = $value['title'];
            $data['description_' . $value['language_code']] = $value['description'];
        }
        return $data;
    }


    /**
     * Get file
     *
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getGroups($only_active = false)
    {
        return $this->getResource('Group')->getGroups($only_active);
    }

    /**
     * Save a Group post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveGroup($data, $validator = null)
    {
        if (!$this->checkAcl('saveGroup')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }        

        $validator = $this->getForm('Group' . ucfirst($validator));

        $lang = Zend_Registry::get('langList');

        if (!$validator->isValid($data)) {            
            return false;
        }

        $data = $validator->getValues();

        $group = array_key_exists('group_id', $data) ?
                $this->getResource('Group')->getGroupById($data['group_id']) : null;

                      
        $newId =  $this->getResource('Group')->saveRow($data, $group);
              
         foreach (Zend_Registry::get('langList') as $lang) {
            $translate_data = array(
                'group_id' => (int) $newId,
                'language_code' => $lang->code,
                'title' => $data['title_' . $lang->code],
                'description' => $data['description_' . $lang->code]
            );
            if (is_null($group))
                $method = 'insert';
            else
                $method = 'update';

            $this->getResource('Group')->saveTranslatedRows($translate_data, $method);

        }
        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('file')
        );
         
        return $newId;
        
          
    }

    public function deleteGroup($id)
    {
        if (!$this->checkAcl('deleteGroup')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $group = $this->getGroupById($id);
        if (null !== $group) {
            $this->getResource('Group')->deleteTranslatedRows($id);
            $group->delete();
            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('file')
            );
            return true;
        }

        return false;
    }
    
     /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'File';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Member', $this, array('viewFiles'))
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);;
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
