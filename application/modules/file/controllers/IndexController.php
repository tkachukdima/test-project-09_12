<?php

/*
 * File_IndexController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class File_IndexController extends Zend_Controller_Action
{

    /**
     * @var File_Model_File
     */
    protected $_modelFile;
    protected $_authService;
    
    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelFile = new File_Model_File();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
        $this->_authService = new User_Service_Authentication();
    }

    public function indexAction()
    {
        
        if(null == $group_id = $this->_getParam('group_id', null))
        {
            throw new ARTCMF_Exception_404($this->view->translate('Type is not passed'));
        }             
        
        
        $this->view->group = $this->_modelFile->getGroupById($group_id, true);
        $this->view->headTitle($this->view->group->title, 'PREPEND');
        $this->view->headMeta()->prependName('description', $this->view->group->description);

        $this->view->files = $this->_modelFile->getFilesByGroupId($group_id, true);
    }

    public function downloadAction()
    {
        $this->view->layout()->disableLayout();
        $this->getHelper('viewRenderer')->setNoRender();

        $file_id = (int) $this->_getParam('id');

        $file = $this->_modelFile->getFileById($file_id, true);
        
        if(null == $file)
        {
            throw new ARTCMF_Exception_404($this->view->translate('File not found'));
        }  

//        $this->getResponse()
//                ->setRawHeader("Content-Disposition: attachment; filename=\"{$file->name}-{$file->file}\"");
//        $output_file = fread(fopen(APPLICATION_PATH . "/../www/upload/{$file->type}/{$file->file}", "rb"), filesize(APPLICATION_PATH . "/../www/upload/file/{$file->file}"));
//        print $output_file;
//        exit;
        /* $settings = Zend_Registry::get('site_settings');
            $this->view->layout()->disableLayout();
            $this->getHelper('viewRenderer')->setNoRender();
                   
*/
            if(!file_exists(APPLICATION_PATH . "/../www/upload/$file->type/{$file->file}")) die("I'm sorry, the file doesn't seem to exist.");

            $type = filetype(APPLICATION_PATH . "/../www/upload/$file->type/{$file->file}");

            // Send file headers
            header("Content-type: $type");
            header("Content-Disposition: attachment;filename={$file->file}");
            header("Content-Transfer-Encoding: binary");
            header('Pragma: no-cache');
            header('Expires: 0');
            // Send the file contents.
            set_time_limit(0);
            readfile(APPLICATION_PATH . "/../www/upload/$file->type/{$file->file}");
            exit;   
    }
    
    public function streamAction()
    {
        $this->view->layout()->disableLayout();
        $this->getHelper('viewRenderer')->setNoRender();
       
        
        $video_id = (int) $this->_getParam('id');
        
        $video = $this->_modelFile->getFileById($video_id, true);
       
       
        if(null == $video)
        {
            throw new ARTCMF_Exception_404($this->view->translate(_('File not found')));
        }  

        //print_r($video->toArray()); die;
        $this->getResponse()->setRawHeader("Content-type: video/mp4");
        $output_file = fread(
                fopen(APPLICATION_PATH . "/../www/upload/{$video->type}/{$video->file}", "rb"), 
                filesize(APPLICATION_PATH . "/../www/upload/{$video->type}/{$video->file}"));
        echo $output_file;
       
    }

    public function listAction()
    {
        if (null != $group_id = $this->_getParam('group_id', null)) {
            $this->view->files = $this->_modelFile->getFilesByGroupId($group_id);
        }
    }

    public function addAction()
    {
        $this->view->fileForm = $this->_getForm('add');
    }

    public function editAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('File not found')) . $this->_getParam('id'));
        }

        $file_id = (int) $this->_getParam('id');

        $file = $this->_modelFile->getFileByIdForEdit($file_id);
        
        $this->view->file = $file;

        $this->view->fileForm = $this->_getForm('edit')->populate($file);
    }

    public function saveAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelFile->saveFile($request->getPost(), $type)) {
            $this->view->fileForm = $this->_getForm($type);
           // print_r($this->view->fileForm->getMessages());die;
            $this->view->fileForm->setDescription($this->view->translate('Error! Completed form is incorrect.'));
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('File added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('File updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'index',
                    'module' => 'file',
                    'group_id' => $request->getPost('group_id')), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($file_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('File not found')). $file_id);
        }

        $this->_modelFile->deleteFile($file_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'index',
                    'module' => 'file'), 'admin', true);
    }

    protected function _getForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['File' . ucfirst($type)] = $this->_modelFile->getForm('File' . ucfirst($type));
        $this->_forms['File' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'module' => 'file',
                    'controller' => 'index',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms['File' . ucfirst($type)]->setMethod('post');

        return $this->_forms['File' . ucfirst($type)];
    }

}