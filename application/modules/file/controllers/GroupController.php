<?php

/*
 * File_GroupController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class File_GroupController extends Zend_Controller_Action
{

    /**
     * @var File_Model_File
     */
    protected $_modelFile;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelFile = new File_Model_File();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }
  
    public function listAction()
    {
        $this->view->groups = $this->_modelFile->getGroups();
    }

    public function addAction()
    {
        $this->view->groupForm = $this->_getForm('add');
    }

    public function editAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Type was not found')) . $this->_getParam('id'));
        }

        $group_id = (int) $this->_getParam('id');

        $group = $this->_modelFile->getGroupByIdForEdit($group_id);

        $this->view->groupForm = $this->_getForm('edit')->populate($group);
    }

    public function saveAction()
    {
        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelFile->saveGroup($request->getPost(), $type)) {
            $this->view->groupForm = $this->_getForm($type);
            $this->view->groupForm->setDescription($this->view->translate(_('An error incorrectly completed form.')));
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Type added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Type updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'group',
                    'module' => 'file'), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($group_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('File not found ')) . $group_id);
        }

        $this->_modelFile->deleteGroup($group_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'group',
                    'module' => 'file'), 'admin', true);
    }

      public function getTypeAjaxAction()
    {
        $groupId = $this->_getParam('groupId', 0);
        $group = $this->_modelFile->getGroupById($groupId);
       
        if (!is_null($group))
            return $this->_helper->json(array('status' => 'success', 'type' => $group->type));
        else
            return $this->_helper->json(array('status' => 'error'));
    }

    protected function _getForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Group' . ucfirst($type)] = $this->_modelFile->getForm('Group' . ucfirst($type));
        $this->_forms['Group' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'module' => 'file',
                    'controller' => 'group',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms['Group' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Group' . ucfirst($type)];
    }

}