<?php

return array(
    array(
        'label' => _('Files'),
        'module' => 'file',
        'controller' => 'index',
        'action' => 'list',
        'route' => 'admin',
        'resource' => 'file:index',
        'privilege' => 'list',
        'reset_params' => true,
        'class' => 'dashboard-file',
        'pages' => array(
            array(
                'label' => _('Files'),
                'module' => 'file',
                'controller' => 'index',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'file:index',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit file'),
                        'module' => 'file',
                        'controller' => 'index',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'file:index',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit file'),
                        'module' => 'file',
                        'controller' => 'index',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'file:index',
                        'privilege' => 'save',
                        'reset_params' => true
                    )
                )
            ),
            array(
                'label' => _('Add File'),
                'module' => 'file',
                'controller' => 'index',
                'action' => 'add',
                'route' => 'admin',
                'resource' => 'file:index',
                'privilege' => 'add',
                'reset_params' => true
            ),
            array(
                'label' => _('Groups'),
                'module' => 'file',
                'controller' => 'group',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'file:group',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit group'),
                        'module' => 'file',
                        'controller' => 'group',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'file:group',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit group'),
                        'module' => 'file',
                        'controller' => 'group',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'file:group',
                        'privilege' => 'save',
                        'reset_params' => true
                    )
                )
            ),
            array(
                'label' => _('Add group'),
                'module' => 'file',
                'controller' => 'group',
                'action' => 'add',
                'route' => 'admin',
                'resource' => 'file:group',
                'privilege' => 'add',
                'reset_params' => true
            ),
        )
    ),
);
