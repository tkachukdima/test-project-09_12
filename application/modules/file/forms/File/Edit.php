<?php

/**
 * Edit File
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Form_File_Edit extends File_Form_File_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('file')->setRequired(false);
        $this->getElement('p_full')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
