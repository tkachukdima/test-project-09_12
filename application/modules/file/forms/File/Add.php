<?php

/**
 * Add new page post
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Form_File_Add extends File_Form_File_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->removeElement('file_id');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
