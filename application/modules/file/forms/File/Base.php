<?php

/**
 * Base File Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Form_File_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $fileDestination = realpath(APPLICATION_PATH . '/../www/upload');
        $fileDestinationPreview = realpath(APPLICATION_PATH . '/../www/images/file');

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->setMethod('post');
        $this->setAction('');

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'name_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));

            $this->addElement('textarea', 'description_' . $lang->code, array(
                'label' => _('Description'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 2,
                'required' => false
            ));
        }

         // get category select
        $form = new File_Form_Group_Select(
                        array('model' => $this->getModel())
        );
        $element = $form->getElement('group_id');
        $element->clearDecorators()->loadDefaultDecorators();
        $element->setRequired(true);
        $this->addElement($element);     

        $this->addElement('file', 'file', array(
            'label' => _('File'),            
            'required' => true,
            'description' => 'Formats: txt, doc, docx, pdf, rtf, zip, rar, xls, xlsx, jpg, jpeg, png, gif.',
            'destination' => $fileDestination,
            'validators' => array(
//                array('Count', false, array(1)),
//                array('Size', false, array(3048576 * 300)),
//                array('Extension', false, array('zip', 'txt', 'doc', 'docx', 'pdf', 'rtf', 'rar', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'gif')),
            ),
        ));
        
        
        /*$settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingByIdent('file_preview');
*/
        /*
        ALTER TABLE  `file` ADD  `p_full` VARCHAR( 1024 ) NOT NULL ,
ADD  `p_preview` VARCHAR( 1024 ) NOT NULL ,
ADD  `p_thumbnail` VARCHAR( 1024 ) NOT NULL ;
        */
        $this->addElement('file', 'p_full', array(
             'label' => _('Image Preview'),
            'required' => true,
            'description' => sprintf(_('Supported formats: jpg, jpeg, png, gif.')),
            'destination' => $fileDestinationPreview,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576*5)),
                array('Extension', false, array('jpg', 'jpeg', 'png', 'gif')),
            /*    array('ImageSize', false, array('minwidth' => $image_resize_setting->width_full,
                                                'maxwidth' => $image_resize_setting->width_full,
                                                'minheight' => $image_resize_setting->height_full,
                                                'maxheight' => $image_resize_setting->height_full))
            */),
        ));
        
       
        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => 0
        ));

        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Not active')
            )
        ));


        $this->addElement('submit', 'submit', array(
        ));

        $this->addElement('hidden', 'file_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'name_' . $lang->code,
                'description_' . $lang->code,
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('group_id', 'publication_id', 'file', 'p_full', 'status', 'sort_order', 'submit', 'file_id'), 'form_all', array('legend' => _('General Settings')));
    }
    
}