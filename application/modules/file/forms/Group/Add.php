<?php

/**
 * Add new Group
 *
 * @Album   Default
 * @package    Menu_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Form_Group_Add extends File_Form_Group_Base
{

    public function init()
    {

        //call the parent init
        parent::init();

        //customize the form
        $this->removeElement('group_id');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
