<?php

/**
 * Base Group Form
 *
 * @category   Default
 * @package    Menu_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Form_Group_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Menu_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Menu_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );


        $this->setMethod('post');
        $this->setAction('');

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));
            
            $element_name = 'description_' . $lang->code;
            $this->addElement('textarea', $element_name, array(
                'label' => _('Description'),
                'filters' => array('StringTrim'),
                'required' => true,
                'class' => "ckeditor"
            ));
            //$this->$element_name->addDecorator(new ARTCMF_Form_Decorator_CKEditor);
        }
        
        $this->addElement('select', 'type', array(
            'label' => _('Type'),
            'decorators' => array('ViewHelper', 'Label'),
            'multiOptions' => array(
                File_Resource_Group::GROUP_TYPE_FILE => _('Files'),
                File_Resource_Group::GROUP_TYPE_IMAGE => _('Images'),
                File_Resource_Group::GROUP_TYPE_VIDEO => _('Videos')
            ),
            //'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));


        $this->addElement('submit', 'submit', array());

        $this->addElement('hidden', 'group_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'title_' . $lang->code,
                'description_' . $lang->code,
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('type', 'submit', 'group_id'), 'form_all', array('legend' => _('General Settings')));
    }

}
