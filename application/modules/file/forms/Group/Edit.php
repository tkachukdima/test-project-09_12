<?php

/**
 * Edit group
 *
 * @category   Default
 * @package    Menu_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Form_Group_Edit extends File_Form_Group_Base
{

    public function init()
    {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('type')->setAttrib('disabled', 1);
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
