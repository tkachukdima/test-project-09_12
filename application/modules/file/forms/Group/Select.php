<?php

/**
 * Menu Select
 *
 * @category   Default
 * @package    Menu_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class File_Form_Group_Select extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setMethod('post');

        $groupsRowset = $this->getModel()->getGroups()->toArray();
        $groups = array();
        foreach ($groupsRowset as $group) {
            $groups[$group['group_id']] = $group['title'];
        }

        $this->addElement('select', 'group_id', array(
            'label' => _('Group'),
            'decorators' => array('ViewHelper', 'Label'),
            'multiOptions' => $groups
        ));

        $this->addElement('submit', 'View', array(
            'decorators' => array('ViewHelper'),
        ));
    }

   

}
