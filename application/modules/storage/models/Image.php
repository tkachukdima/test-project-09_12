<?php

class Storage_Model_Image extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get a template by its id
     *
     * @param  string $ident The ident
     * @return Image_Resource_Image_Item
     */
    public function getImageById($image_id)
    {
        return $this->getResource('Image')->getImageById((int) $image_id);
    }

    public function getImageByShortcode($shortcode)
    {
        return $this->getResource('Image')->getImageByShortcode($shortcode);
    }

    public function getImages($embeded = null)
    {
        return $this->getResource('Image')->getImages($embeded);
    }

    public function getImageByIdForEdit($image_id, $relation=null){
        $image_id = (int) $image_id;

        $page = $this->getResource('Image')->getImageByIdForEdit($image_id, $relation)->toArray();

        $data = $page[0];

        foreach ($page as $value) {
            $data['title_' . $value['language_code']] = $value['title'];
            $data['alt_' . $value['language_code']] = $value['alt'];
        }

        return $data;
    }

    public function getImagesForEdit($relation=null, $option=null)
    {
        /*@TODO разные функции для add-publication / list-storage / edit-publication */
        $embeded = null;
        if ( isset($option['select']) ){

            switch ($option['select']) {
                case 'all':
                    $embeded = 0;
                    break;

                case 'empty':
                    return new stdClass();
                    break;

                case '':
                    break;
            }

        }


/*
        if (isset($option['select']) && ('all' != $option['select'] || 'empty' == $option['select'] || '' != $option['select']))
            return new stdClass();
        else
            $embeded = 0;
*/

        $list = $this->getResource('Image')->getImages($embeded, $relation);

        $fileclass = new stdClass();

        foreach($list as $key => $val){

            $data = $this->getImageByIdForEdit($val->image_id, $relation);
            
            //$datas['name'] = '1111111NaME !';//$val->title;//str_replace($user_path, 'New Image Upload Complete:   ', $name);//preg_replace('/\d\//','',)  
            //$datas['size'] = $info['size'];//$adapter->getFileSize($file);  
            //$datas['type'] = $adapter->getMimeType($file);  
            $datas['delete_url'] = '/admin/storage/image/save/image_id/'.$val->image_id; //$info['destination'];
            $datas['delete_type'] = 'DELETE';
            //$datas['title'] = $val->title;

            $datas['thumbnail_url'] = $val->path . '/' . $val->thumbnail;
            $datas['url'] = $val->path . '/' . $val->full; 


            $datas['main'] = $val->main;
            $datas['align'] = $val->align;

            $fileclass->files[] = $datas + $data;
        }
        return $fileclass;
    }

    public function cropPhoto($image, $coordinates)
    {


        $fileDestination = realpath(APPLICATION_PATH . '/../www/' . $image->path); //$image->image_id

        $filter = new ARTCMF_Filter_ImageSize();

        $rsettings_ident = $coordinates['rsettings_ident'];
        $image_type = $coordinates['image_type'];

        $path_parts = pathinfo($image->original);
        //print_r($path_parts);
        //die();
        $new_file_name = $path_parts['filename'] . '-' . $image->image_id . '.' . $path_parts['extension'];

        copy($fileDestination . '/' . $image->full, $fileDestination . '/' . $new_file_name);

        //$publicationGroup = $this->getImageById($image->image_id);



        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel($rsettings_ident);
        
        switch ($image_type) {
            case 'thumbnail':
                $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                                ->setHeight($image_resize_setting['height_thumbnail'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($fileDestination)
                                ->setCoordinates($coordinates)
                                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                                ->filter($fileDestination . '/' . $new_file_name); //. $new_file_name
                $coordinates['thumbnail'] = basename($thumbnail);
                break;

            case 'preview':
                $preview = $filter->setWidth($image_resize_setting['width_preview'])
                    ->setHeight($image_resize_setting['height_preview'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setCoordinates($coordinates)
                    ->setStrategy(new $image_resize_setting['strategy_preview']())
                    ->filter($fileDestination . '/' . $new_file_name); //. $new_file_name
                $coordinates['preview'] = basename($preview);
                break;

            case 'detail':
                $detail = $filter->setWidth($image_resize_setting['width_detail'])
                    ->setHeight($image_resize_setting['height_detail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setCoordinates($coordinates)
                    ->setStrategy(new $image_resize_setting['strategy_detail']())
                    ->filter($fileDestination . '/' . $new_file_name); //. $new_file_name
                $coordinates['detail'] = basename($detail);
                break;
        }


        
        

        /*$this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('publications')
        );*/

        /*print_r($coordinates);
        die();*/


        return $this->getResource('Image')->saveRow($coordinates, $image);
    }

    public function resizeImage($image, $opt)
    {
        $fileDestination = realpath(APPLICATION_PATH . '/../www/' . $image->path); //$image->image_id

        $filter = new ARTCMF_Filter_ImageSize();

        $rsettings_ident = $opt['rsettings_ident'];
        //$image_type = $opt['image_type'];

        $path_parts = pathinfo($image->original);
        $new_file_name = $path_parts['filename'] . '-' . $image->image_id . '.' . $path_parts['extension'];

        copy($fileDestination . '/' . $image->full, $fileDestination . '/' . $new_file_name);

        //$publicationGroup = $this->getImageById($image->image_id);

        $imageSizeFilter = new ARTCMF_Filter_ImageSize();

        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel($rsettings_ident);
        
                    if ($image_resize_setting['width_thumbnail'] != '' AND $image_resize_setting['height_thumbnail'] != '') {
                        $thumbnail = $imageSizeFilter->setWidth($image_resize_setting['width_thumbnail'])
                                ->setHeight($image_resize_setting['height_thumbnail'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($fileDestination)
                                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                                ->filter($fileDestination . '/' . $new_file_name);
                        $data['thumbnail'] = basename($thumbnail);
                    }

                    if ($image_resize_setting['width_preview'] != '' AND $image_resize_setting['height_preview'] != '') {
                        $preview = $imageSizeFilter->setWidth($image_resize_setting['width_preview'])
                                ->setHeight($image_resize_setting['height_preview'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($fileDestination)
                                ->setStrategy(new $image_resize_setting['strategy_preview']())
                                ->filter($fileDestination . '/' . $new_file_name);
                        $data['preview'] = basename($preview);
                    }

                    if ($image_resize_setting['width_full'] != '' AND $image_resize_setting['height_full'] != '') {
                        $full = $imageSizeFilter->setWidth($image_resize_setting['width_full'])
                                ->setHeight($image_resize_setting['height_full'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($fileDestination)
                                ->setStrategy(new $image_resize_setting['strategy_full']())
                                ->filter($fileDestination . '/' . $new_file_name);
                        $data['full'] = basename($full);
                    }

                    if ($image_resize_setting['width_detail'] != '' AND $image_resize_setting['height_detail'] != '') {
                        $detail = $imageSizeFilter->setWidth($image_resize_setting['width_detail'])
                                ->setHeight($image_resize_setting['height_detail'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($fileDestination)
                                ->setStrategy(new $image_resize_setting['strategy_detail']())
                                ->filter($fileDestination . '/' . $new_file_name);
                        $data['detail'] = basename($detail);
                    }


        return $this->getResource('Image')->saveRow($data, $image);
    }

    public function saveImage($data)
    {
        if (!$this->checkAcl('saveImage')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $data['last_edit_id'] = $this->getIdentity()->user_id;

        $image = array_key_exists('image_id', $data) ?
                $this->getResource('Image')->getImageById($data['image_id']) : null;

        if(null == $image){
            $data['author_id'] = $this->getIdentity()->user_id;
        }
        
        if(!isset($data['main'])){
            $data['main'] = 0;
        }

        $new = $this->getResource('Image')->saveRow($data, $image);

        $this->getResource('Image')->deleteTranslatedRows($new);       
        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'image_id' => (int) $new,
                'language_code' => $lang->code,
                'title' => $data['title_' . $lang->code],
                'alt' => $data['alt_' . $lang->code]
            );
            
            $this->getResource('Image')->saveTranslatedRows($translate_data, 'insert');
        }

        if (isset($data['key']) && 0 !== $data['key'] &&  isset($data['id']) && 0 !== $data['id']){
            
            $relation_data = array(
                'image_id' => (int) $new,
                'key' => $data['key'],
                'id' => $data['id'],
                'main' => $data['main']
            );
            //echo "=". $data['main'];
            $this->getResource('Image')->clearMainAll($data['id']);
            $this->getResource('Image')->saveRelationRows($relation_data);
        }
        else{
            $relation_data = array(
                'image_id' => (int) $new,
                'main' => $data['main']
            );
            $this->getResource('Image')->updateRelationRows($relation_data);
        }


        return $new;
    }

    public function deleteImage($image_id)
    {
        if (!$this->checkAcl('deleteImage')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $image = $this->getImageById($image_id);
        if (null !== $image) {
            $fileDestination = realpath(APPLICATION_PATH . '/../www' . $image->path);
            @unlink($fileDestination . '/' . $image->thumbnail);
            @unlink($fileDestination . '/' . $image->preview);
            @unlink($fileDestination . '/' . $image->full);
            @unlink($fileDestination . '/' . $image->detail);
            @unlink($fileDestination . '/' . $image->original);

            $fname = pathinfo($image->original);
            @unlink($fileDestination . '/' . $fname['filename'] . '-' . $image_id . '.' . $fname['extension']);

            //rmdir($fileDestination . '/');
            $image->delete();
            $this->getResource('Image')->deleteTranslatedRows($image_id);

            $this->getResource('Image')->deleteRelationRows($image_id);

            return true;
        }

        return false;
    }

    public function saveImageRelations($images_id, $key, $id){
        //print_r($images_id);die();
        if ( count($images_id) > 0 && $images_id[0] != ''){
            
            $this->getResource('Image')->deleteRelationsRows($images_id);

            foreach ($images_id as $value) {
                $data = array(
                    'image_id' => (int) $value,
                    'key' => $key,
                    'id' => $id
                );

                $this->getResource('Image')->saveRelationRows($data);
            }

            $this->getResource('Image')->updateEmbeded($images_id, 1);
        }
    }


    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Image';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Member', $this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
