<?php

/**
 * Storage_Resource_Upload
 *
 * @category   Default
 * @package    Storage_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Storage_Resource_Image extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'storage_images';
    protected $_primary = 'image_id';
    protected $_rowClass = 'Storage_Resource_Image_Item';
    protected $_lang;
    /*protected $_referenceMap = array(
        'Image' => array(
            'columns' => 'image_id',
            'refTableClass' => 'Publication_Resource_Publication',
            'refColumns' => 'image_id',
            //'onDelete' => self::CASCADE,
            //'onUpdate' => self::RESTRICT
        )
    );*/


    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }


    public function getImageById($id)
    {
        return $this->find($id)->current();
    }

    public function getImageByShortcode($shortcode){

        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.image_id = {$this->_name}_translation.image_id")

                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where('shortcode = ?', $shortcode);

        return $this->fetchRow($select);   
    }

    public function getImageByIdForEdit($id, $relation = null){
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.image_id = {$this->_name}_translation.image_id");
                
                
                if(isset($relation['id']) && 0 != $relation['id'] && isset($relation['key']) && 0 !== $relation['key']){
                    $select->joinLeft('storage_images_relations', "storage_images_translation.image_id = storage_images_relations.image_id");
                    $select->where('storage_images_relations.id = ?', $relation['id']);
                    $select->where('storage_images_relations.`key` = ?',  $relation['key']);
                }

                $select->where($this->_name . '.image_id = ?', $id);

        return $this->fetchAll($select);
    }

    public function getImages($embeded, $relation = null)
    {
        $select = $this->select();
        $select->from($this->_name);

        $select->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.image_id = {$this->_name}_translation.image_id");
                
        if(isset($relation['id']) && 0 != $relation['id'] && isset($relation['key']) && 0 !== $relation['key']){

            //print_r($relation);die;

            $select->joinLeft('storage_images_relations', "storage_images_translation.image_id = storage_images_relations.image_id");
            $select->where('storage_images_relations.id = ?', $relation['id']);
            $select->where('storage_images_relations.`key` = ?',  $relation['key']);
        }

        if(null !== $embeded){
            $select->where("embeded = ?", $embeded);            
        }

        $select->where($this->_name . '_translation.language_code = ?', $this->_lang);

        return $this->fetchAll($select);
    }


    public function saveRelationRows($relation_data, $method = 'insert')
    {
        $relation_data['table'] = 'storage_images_relations';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($relation_data['table'], array(
                    'image_id' => $relation_data['image_id'],
                    'key' => $relation_data['key'],
                    'id' => $relation_data['id'],
                    'main' => $relation_data['main']
                ));

                break;

            /*case 'update':
                $this->getAdapter()->update($relation_data['table'], array(
                    'title' => $relation_data['title'],
                    //'url' => $relation_data['url'],
                    //'description' => $translate_data['description']
                        ), "`image_id` = {$relation_data['image_id']}
                      AND `language_code` = '{$relation_data['language_code']}'");

                break;*/
            default:
                break;
        }
    }

    public function deleteRelationRows($id)
    {
        $this->getAdapter()->delete("storage_images_relations", "image_id = {$id}");
    }

    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'image_id' => $translate_data['image_id'],
                    'language_code' => $translate_data['language_code'],
                    'title' => $translate_data['title'],
                    'alt' => $translate_data['alt']
                    //'url' => $translate_data['url'],
                    //'description' => $translate_data['description']                    
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'title' => $translate_data['title'],
                    'alt' => $translate_data['alt']
                    //'url' => $translate_data['url'],
                    //'description' => $translate_data['description']
                        ), "`image_id` = {$translate_data['image_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "image_id = {$id}");
    }
    
    public function deleteRelationsRows($images_id)
    {
        $this->getAdapter()->delete("storage_images_relations", "image_id IN(" . implode(",", $images_id) . ")");
    }

    public function updateEmbeded($images_id, $embeded)
    {
        $this->getAdapter()->update("{$this->_name}", array('embeded' => $embeded), "`image_id` IN(" . implode(",", $images_id) . ")");
    }
    
    public function updateRelationRows($relation_data){
        
        $data = array(
            'main' => $relation_data['main']
        );
        
        $dbAdapter = $this->getAdapter();
        $where = $dbAdapter->quoteInto('image_id = ?', $relation_data['image_id']);
        
        $dbAdapter->update("storage_images_relations", $data, $where);
        
        //this->getAdapter()->query("UPDATE `storage_images_relations` SET `main`='".$relation_data['main']."' WHERE `image_id`='".$relation_data['image_id']."'");
       
    }
    
    public function clearMain($images_id){
        $this->getAdapter()->update("{$this->_name}", array('main' => '0'), "`image_id` IN(" . implode(",", $images_id) . ")");
    }

    public function clearMainAll($id){
        $data = array(
            'main' => 0
        );
        
        $dbAdapter = $this->getAdapter();
        $where = $dbAdapter->quoteInto('id = ?', $id);
        
        $dbAdapter->update("storage_images_relations", $data, $where);
    }
}