<?php

return array(
    'Redactor' => array(
        'allow' => array(
            'image' => array(
                'index',
                'upload',
                'update',
                'resize',
                'delete',
                'crop',
                'save',
                'resize'
            )
        )
    ),
    'Manager' => array(
    ),
    'Admin' => array(
    ),
);