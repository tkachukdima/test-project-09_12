<?php

return array(
    array(
        'label' => _('Storage'),
        'module' => 'storage',
        'controller' => 'image',
        'action' => 'index',
        'route' => 'admin',
        'resource' => 'storage:image',
        'privilege' => 'index',
        'reset_params' => true,
        'class' => 'dashboard-slider',
        'pages' => array(

            array(
                'label' => _('Storage list'),
                'module' => 'storage',
                'controller' => 'image',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'storage:image',
                'privilege' => 'list',
                'reset_params' => true,
                /*'pages' => array(
                    array(
                        'label' => _('Edit slide'),
                        'module' => 'slider',
                        'controller' => 'image',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'storage:image',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit slide'),
                        'module' => 'slider',
                        'controller' => 'image',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'storage:image',
                        'privilege' => 'save',
                        'reset_params' => true
                    )
                )*/
            ),


        )
    )
);