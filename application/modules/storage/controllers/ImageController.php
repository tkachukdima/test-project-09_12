<?php

/**
 * ImageController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Storage_ImageController extends Zend_Controller_Action
{
    private $uploads = '/images/storage/';
    //private $uploads_rel = '/images/uploads/';

    protected $_modelStorage;

    public function init(){
        $this->_modelStorage = new Storage_Model_Image();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction() {

        
    }

    public function listAction() {

        $this->view->images = $this->_modelStorage->getImages(0);
    }

    public function saveAction(){

        if ($this->_request->isPost() && $this->_request->getParam('edit_id', false)) {
            $this->edit();
            exit;
        }

        if ($this->_request->isOptions()) { 
            $this->upload();
        }
        if ($this->_request->isPost()) {
            //die("isPost");

            $this->upload();
        }
        if ($this->_request->isGet()) {

            $relation['key'] = $this->_request->getParam('key', 0);
            $relation['id'] = $this->_request->getParam('id', 0);

            $option['select'] = $this->_request->getParam('select', '');

            $list_image = $this->_modelStorage->getImagesForEdit($relation, $option);

            echo $this->_helper->json($list_image);
            exit;
            //$this->upload();
        }
        if ($this->_request->isDelete() || $_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $this->delete();
        }
        exit;
    }



    public function upload() {

        $key = $this->_request->getParam('key', 0);
        $id = $this->_request->getParam('id', 0);

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $imagePath = $this->uploads . date('Y') . '/' . date('m') . '/' . date('d');
            $user_path = APPLICATION_PATH . '/../www' . $imagePath;

            //$user_path = PUBLIC_PATH. $this->uploads_rel;//. $user_id;
            //die($user_path);
            if (!is_dir($user_path))
                    mkdir($user_path, 0775, true);
            //if (!file_exists($user_path)) mkdir($user_path);

            $adapter->setDestination($user_path);
            $adapter->addValidator('Extension', false, 'jpg,png,gif');

            $files = $adapter->getFileInfo();
            //$datas = array();

            $fileclass = new stdClass();

            foreach ($files as $file => $info) {
                $name = $adapter->getFileName($file);

                // you could apply a filter like this too (if you want), to rename the file:     
                //  $name = ExampleLibrary::generateFilename($name);
                //  $adapter->addFilter('rename', $user_path . '/' .$name);

                // file uploaded & is valid
                if (!$adapter->isUploaded($file)) continue; 
                if (!$adapter->isValid($file)) continue;

                // receive the files into the user directory
                $adapter->receive($file); // this has to be on top


                    //$fileinfo = $files['imagefile'];

                    /*print_r($files);
                    die();

                    $path_parts = pathinfo($fileinfo['name']);
                    $extension = $path_parts['extension'];

                    $filterIdent = new ARTCMF_Filter_Ident();
                    $filepre = $title == '' ? 'file_' : $filterIdent->filter($title);

                    $filename = $filepre . '_' . date('YmdHi') . '.' . $extension;
*/

                    $path = $info['destination'];
                    $filename = $info['name'];

                    

                    /* ----- */
                    $settingModel = new Default_Model_Settings();
                    $image_resize_setting = $settingModel->getImageResizeSettingForModel('storage');// $request->getPost('resize_setting_ident')

                    $imageSizeFilter = new ARTCMF_Filter_ImageSize();

                    $data = array(
                        'title' => $info['name'],
                        'original' => $info['name'],
                        'created_at' => date('Y-m-d H:i:s'),
                        //'main' =>  (int)(isset($_REQUEST['main']))?$_REQUEST['main']:0,
                        'align' =>  (int)(isset($_REQUEST['align']))?$_REQUEST['align']:0                          
                    );

                    foreach (Zend_Registry::get('langList') as $lang) {
                        $data['title_' . $lang->code] = (!empty($_REQUEST['title_' . $lang->code])) ? htmlspecialchars($_REQUEST['title_' . $lang->code]) : $filename;
                        $data['alt_' . $lang->code] = (!empty($_REQUEST['alt_' . $lang->code])) ? htmlspecialchars($_REQUEST['alt_' . $lang->code]) : $filename;
                    }

                    $imageModel = new Storage_Model_Image();
                    $image_id = $imageModel->saveImage($data);

                    if (!is_dir($user_path . "/" . $image_id))
                        mkdir($user_path . "/" . $image_id, 0775, true);

                    copy($info['destination'] . "/". $info['name'], $user_path . "/" . $image_id . "/". $info['name']);


                    //$data = array();
                    $data['image_id'] = $image_id;
                    $data['path'] = $imagePath . "/" . $image_id;
                    $data['shortcode'] = "{\$code-$image_id}";
                    $data['id'] = (int)$id;
                    $data['key'] = $key;
                    
                    $data['main'] = (int)(isset($_REQUEST['main']))?$_REQUEST['main']:0;
                    
                    if ($data['id'] !== 0 && $data['key'] !== 0)
                        $data['embeded'] = 1;

                    $path = $user_path . "/" . $image_id;

                    $path_parts = pathinfo($info['name']);

                    $new_file_name = $path_parts['filename'] . '-' . $data['image_id'] . '.' . $path_parts['extension'];

                    copy($path . '/' . $info['name'], $path . '/' . $new_file_name);

                    if ($image_resize_setting['width_thumbnail'] != '' AND $image_resize_setting['height_thumbnail'] != '') {
                        $thumbnail = $imageSizeFilter->setWidth($image_resize_setting['width_thumbnail'])
                                ->setHeight($image_resize_setting['height_thumbnail'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                                ->filter($path . '/' . $new_file_name);
                        $data['thumbnail'] = basename($thumbnail);
                    }

                    if ($image_resize_setting['width_preview'] != '' AND $image_resize_setting['height_preview'] != '') {
                        $preview = $imageSizeFilter->setWidth($image_resize_setting['width_preview'])
                                ->setHeight($image_resize_setting['height_preview'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_preview']())
                                ->filter($path . '/' . $new_file_name);
                        $data['preview'] = basename($preview);
                    }

                    if ($image_resize_setting['width_full'] != '' AND $image_resize_setting['height_full'] != '') {
                        $full = $imageSizeFilter->setWidth($image_resize_setting['width_full'])
                                ->setHeight($image_resize_setting['height_full'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_full']())
                                ->filter($path . '/' . $new_file_name);
                        $data['full'] = basename($full);
                    }

                    if ($image_resize_setting['width_detail'] != '' AND $image_resize_setting['height_detail'] != '') {
                        $detail = $imageSizeFilter->setWidth($image_resize_setting['width_detail'])
                                ->setHeight($image_resize_setting['height_detail'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_detail']())
                                ->filter($path . '/' . $new_file_name);
                        $data['detail'] = basename($detail);
                    }

                    $image_id = $imageModel->saveImage($data);

                    @unlink($info['destination'] . "/". $info['name']);

                    


                // we stripped out the image thumbnail for our purpose, primarily for security reasons
                // you could add it back in here.
                //PUBLIC_PATH. $this->uploads_rel
                $datas['name'] = $info['name'];//str_replace($user_path, 'New Image Upload Complete:   ', $name);//preg_replace('/\d\//','',)  
                $datas['size'] = $info['size'];//$adapter->getFileSize($file);  
                //$datas['type'] = $adapter->getMimeType($file);  
                $datas['delete_url'] = '/admin/storage/image/save/image_id/'.$image_id; //$info['destination'];
                $datas['delete_type'] = 'DELETE';

                //$datas[''] = ;

                $datas['thumbnail_url'] = $imagePath . '/' . $data['image_id'] . '/' . $data['thumbnail'];
                //$fileclass->error = 'null';
                $datas['url'] = $imagePath . '/' . $data['image_id'] . '/' . $data['full']; 

                $fileclass->files[] = $datas + $data;
            }

            header('Pragma: no-cache');
            header('Cache-Control: private, no-cache');
            header('Content-Disposition: inline; filename="files.json"');
            header('X-Content-Type-Options: nosniff');
            header('Vary: Accept');
            echo $this->_helper->json($fileclass);
      
  }

    public function edit() {

        $request = $this->getRequest();
        
        $edit_id = (int)$this->_request->getParam('edit_id');

        //print_r();
        $data = $request->getPost();
        $data['image_id'] = $edit_id;

        $success = $this->_modelStorage->saveImage($data);
        
        if($success) {
            $out = array('status' => 'success', 'message' => $this->view->translate('Successfully edit image.'));
        }
        else {
            $out = array('status'=>'error');
        }

        echo $this->_helper->json($out);
    }

    public function delete() {
        

        /*$image_id = (int)$this->_request->getParam('image_id');
     
        $image = $this->_modelStorage->getImageById($image_id);

        if (null !== $image) {
            //deleteTranslatedRows
            $fileDestination = realpath(APPLICATION_PATH . '/../www' . $image->path);
            unlink($fileDestination . '/' . $image->thumbnail);
            unlink($fileDestination . '/' . $image->preview);
            unlink($fileDestination . '/' . $image->full);
            unlink($fileDestination . '/' . $image->detail);
            unlink($fileDestination . '/' . $image->original);
            
            //$this->getResource('Slider')->deleteTranslatedRows($id);

            $image->delete();
            $success = true;
        }
        else
        {
            $success = false;
        }*/

        $image_id = (int)$this->_request->getParam('image_id');
        $success = $this->_modelStorage->deleteImage($image_id);
       
        echo $this->_helper->json($success);
    }


    public function cropAction()
    {
        $request = $this->getRequest();
        $this->_helper->layout()->disableLayout();

        $image_id = (int) $request->getParam('image_id');
        
        $this->view->image = $this->_modelStorage->getImageById($image_id);


        
        //$publicationGroup = $this->_modelPublication->getPublicationGroupById($this->view->publication->publication_group_id);

        if (null !== ($this->view->rsettings_ident = $request->getParam('rsettings_ident', null))){
            $settingModel = new Default_Model_Settings();
            $this->view->image_resize_setting = $settingModel->getImageResizeSettingByIdent($this->view->rsettings_ident)->toArray();
        }

        //print_r($this->view->image_resize_setting);

        $request = $this->getRequest();

        if ($request->isPost()) {

            if (null !== ($id = $this->_modelStorage->cropPhoto($this->view->image, $request->getPost()))) {
                /*$redirector = $this->getHelper('redirector');
                return $redirector->gotoRoute(array(
                            'action' => 'index',
                            'controller' => 'image',
                            'module' => 'storage'), 'admin', true);*/
                
                $out = array('status' => 'success', 
                             'message' => $this->view->translate('Successfully cropped image.'),
                             'image_id' => $this->view->image->image_id,
                             'thumbnail_url' => $this->view->image->path . "/" . $this->view->image->thumbnail);
            }
            else
            {
                $out = array('status'=>'error');
            }

            return $this->_helper->json($out);
        }
    }

    public function resizeAction(){
        $request = $this->getRequest();
        $this->_helper->layout()->disableLayout();

        $image_id = (int) $request->getParam('image_id');
        //die("1");
        $this->view->image = $this->_modelStorage->getImageById($image_id);

        if ($request->isPost()) {
            $data['rsettings_ident'] = $request->getParam('rsettings_ident');
            //die($data['rsettings_ident']);
            if (null !== ($id = $this->_modelStorage->resizeImage($this->view->image, $data))) {
                
                $out = array('status' => 'success',
                             'message' => $this->view->translate('Successfully resize image.'),
                             'image_id' => $this->view->image->image_id,
                             'thumbnail_url' => $this->view->image->path . "/" . $this->view->image->thumbnail
                             );
            }
            else
            {
                $out = array('status'=>'error');
            }

            return $this->_helper->json($out);
        }
    }



    /* #################################################################################################################### */


    public function uploadAction()
    {

//        if ($this->getRequest()->isXmlHttpRequest()) {
//            throw new ARTCMF_Exception('Request is not ajax');
//        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            try {

                $title = $request->getPost('imagetitle');

                $imagePath = '/images/' . date('Y') . '/' . date('m') . '/' . date('d');
                $path = APPLICATION_PATH . '/../www' . $imagePath;
                if (!is_dir($path))
                    mkdir($path, 0775, true);

                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, 1)
                        ->addValidator('Size', false, array('max' => 10 * 1024 * 1024))
                        ->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,bmp', 'case' => false));
                $adapter->setDestination($path);
                $files = $adapter->getFileInfo();

                $fileinfo = $files['imagefile'];

                if (($adapter->isUploaded($fileinfo['name'])) && ($adapter->isValid($fileinfo['name']))) {
                    $path_parts = pathinfo($fileinfo['name']);
                    $extension = $path_parts['extension'];

                    $filterIdent = new ARTCMF_Filter_Ident();
                    $filepre = $title == '' ? 'file_' : $filterIdent->filter($title);

                    $filename = $filepre . '_' . date('YmdHi') . '.' . $extension;

                    $adapter->addFilter('Rename', array('target' => $path . "/" . $filename, 'overwrite' => true));
                    $adapter->receive($fileinfo['name']);

                    $settingModel = new Default_Model_Settings();
                    $image_resize_setting = $settingModel->getImageResizeSettingForModel($request->getPost('resize_setting_ident'));

                    $imageSizeFilter = new ARTCMF_Filter_ImageSize();

                    $data = array(
                        'title' => $title,
                        'path' => $imagePath,
                        'original' => $filename,
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    if ($image_resize_setting['width_thumbnail'] != '' AND $image_resize_setting['height_thumbnail'] != '') {
                        $thumbnail = $imageSizeFilter->setWidth($image_resize_setting['width_thumbnail'])
                                ->setHeight($image_resize_setting['height_thumbnail'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                                ->filter($path . '/' . $filename);
                        $data['thumbnail'] = basename($thumbnail);
                    }

                    if ($image_resize_setting['width_preview'] != '' AND $image_resize_setting['height_preview'] != '') {
                        $preview = $imageSizeFilter->setWidth($image_resize_setting['width_preview'])
                                ->setHeight($image_resize_setting['height_preview'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_preview']())
                                ->filter($path . '/' . $filename);
                        $data['preview'] = basename($preview);
                    }

                    if ($image_resize_setting['width_full'] != '' AND $image_resize_setting['height_full'] != '') {
                        $full = $imageSizeFilter->setWidth($image_resize_setting['width_full'])
                                ->setHeight($image_resize_setting['height_full'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_full']())
                                ->filter($path . '/' . $filename);
                        $data['full'] = basename($full);
                    }

                    if ($image_resize_setting['width_detail'] != '' AND $image_resize_setting['height_detail'] != '') {
                        $detail = $imageSizeFilter->setWidth($image_resize_setting['width_detail'])
                                ->setHeight($image_resize_setting['height_detail'])
                                ->setQuality(100)
                                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                                ->setThumnailDirectory($path)
                                ->setStrategy(new $image_resize_setting['strategy_detail']())
                                ->filter($path . '/' . $filename);
                        $data['detail'] = basename($detail);
                    }

                    $imageModel = new Storage_Model_Image();
                    $image_id = $imageModel->saveImage($data);

                    $this->_helper->json(
                            array(
                                'success' => true,
                                'image' => $imagePath . '/' . basename($thumbnail),
                                'title' => $title,
                                'image_id' => $image_id
                            )
                    );
                }

                $this->_helper->json(array('success' => false, 'messages' => $adapter->getMessages()));
            } catch (Exception $ex) {
                $this->_helper->json(array('success' => false, 'messages' => $ex->getMessages()));
            }
        }
    }

    public function updateAction()
    {
        
    }

    public function deleteAction()
    {
        $image_id = $this->getRequest()->getParam('image_id', null);
        if (!is_null($image_id)) {
            $imageModel = new Storage_Model_Image();
            $imageModel->deleteImage($image_id);
        }

        $this->_helper->json(array('success' => true));
    }

}
