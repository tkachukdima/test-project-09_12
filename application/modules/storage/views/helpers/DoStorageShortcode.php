<?php

class Storage_View_Helper_DoStorageShortcode extends Zend_View_Helper_Abstract
{

	// example:
	// echo $this->doStorageShortcode('{$code_1}<b>example: </b>{$code_2}<div align=\"left\">this is a test</div>');

    public function doStorageShortcode($text)
    {
        //$text = '{$wer}<b>example: </b>{$sdfsdf}<div align=\"left\">this is a test</div>';
       	
       	// "|<[^>]+>(.*)</[^>]+>|U"
	    if( preg_match_all('|\{\$[^>]+(.*)[^\}]+\}|U', $text, $out, PREG_SET_ORDER) ) {

	        $_modelStorage = new Storage_Model_Image();
	        
	        $flag = false;
	        $search = array();
	        foreach($out as $m){
	        	
	        	$image = $_modelStorage->getImageByShortcode($m[0]);
	        	if (null !== $image){
	        		$search[] = $m[0];

	        		$align = '';
	        		switch ($image->align) {
	        			case '1':
	        				$align = ' align="left"';
	        				break;

	        			case '2':
	        				$align = ' align="center"';
	        				break;
	        			
	        			case '3':
	        				$align = ' align="right"';
	        				break;
	        		}
	        		$replace[] = '<img src="' . $image->path . '/' . $image->preview . '" title="' . $image->title . '" alt="' . $image->title . '"' . $align . '/>';
	        		$flag = true;
	        	}

	        }

	        if ($flag)
		        $text = str_replace($search, $replace, $text);
	    }
  
        return $text;
    }


}