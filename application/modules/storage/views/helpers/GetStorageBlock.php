<?php

/*
 * Instruction ! (how to add Storage in module)
 * 
 * (1) add helper in view        
 * 
 * => for edit
 *   $relation = array("id"  => $this->productForm->product_id->getValue(),
 *                     "key" => 'catalog',
 *                     "rsettings_ident" => 'catalog_product_main');  
 *   echo $this->getStorageBlock($relation);
 * 
 * => for add
 *   $relation = array("rsettings_ident" => 'publication', 'select' => 'empty');
 *   echo $this->getStorageBlock($relation);
 * 
 * ----------------------------------------------------------------------------
 * (2) add field for form
 *   $this->addElement('hidden', 'images_id', array(
 *                     'filters' => array('StringTrim'),
 *                     'required' => false,
 *                     'id' => "images_id",
 *                     'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
 *   ));
 * 
 * ----------------------------------------------------------------------------
 * (3) add in model - save (save item, record)
 * 
 * //save images relations
 * $modelStorage = new Storage_Model_Image();
 * $modelStorage->saveImageRelations(explode(':',$data['images_id']), 'catalog', $product_id);
 * 
 */



class Storage_View_Helper_GetStorageBlock extends Zend_View_Helper_Abstract
{
    private $_relation = array("rsettings_ident" => "storage",
                               "select" => "",
                               "key" => 0,
                               "id" => 0);

    public function getStorageBlock($relation = null)
    {
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/storage/views/templates/');
        $view->setHelperPath(APPLICATION_PATH . '/../library/ARTCMF/View/Helper/', 'ARTCMF_View_Helper');

        if(null !== $relation){
            foreach ($this->_relation as $key => $value) {
                if(!isset($relation[$key]))
                    $relation[$key] = $value;
            }

        	$view->relation = $relation;
        }

        return $view->render('storage.phtml');
    }


}