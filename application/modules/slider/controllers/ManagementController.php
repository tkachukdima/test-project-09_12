<?php

/*
 * Slider_ManagementController
 * 
 * @category   Slide
 * @package    Slide_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Slider_ManagementController extends Zend_Controller_Action
{

    /**
     * @var Slider_Model_Slider
     */
    protected $_modelSlider;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelSlider = new Slider_Model_Slider();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function listAction()
    {

        $this->view->sliders = $this->_modelSlider->getSliders();
    }

    public function sortAction()
    {

        $redirector = new Zend_Controller_Action_Helper_Redirector();

        if ($this->_getParam('id') != '' AND $this->_getParam('move') != '') {
            $id = (int) $this->_getParam('id');
            $move = $this->_getParam('move');

            $this->_modelSlider->moveSliderItem($id, $move);
        }

        if ($this->getRequest()->isPost()) {
            if (is_array($this->getRequest()->getPost('sort_order')) AND is_array($this->getRequest()->getPost('id'))) {

                $id = $this->getRequest()->getPost('id');
                $sort_order = $this->getRequest()->getPost('sort_order');

                $this->_modelSlider->setNewSortOrder($id, $sort_order);
            }
        }

        $redirector->gotoRoute(array(
            'module' => 'slider',
            'controller' => 'management',
            'action' => 'list'), 'admin', true
        );
    }

    public function addAction()
    {

        $this->view->sliderForm = $this->_getForm('add');
    }

    public function editAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Slider not found')) . ' ' . $this->_getParam('id'));
        }

        $slider_id = (int) $this->_getParam('id');

        $slider = $this->_modelSlider->getSliderByIdForEdit($slider_id);

        $group = $this->_modelSlider->getGroupsBySliderId($slider_id);

        foreach ($group as $val) {
            $slider['group'][] = $val->module_id;
        }
        $this->view->sliderForm = $this->_getForm('edit')->populate($slider);
    }

    public function saveAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelSlider->saveSlider($request->getPost(), $type)) {
            $this->view->sliderForm = $this->_getForm($type);
            $this->view->sliderForm->setDescription($this->view->translate('Error! Completed form is incorrect.'));
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Slide added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Slide updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'module' => 'slider',
                    'controller' => 'management',
                    'action' => 'list'), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($slider_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Slider not found')) . ' ' . $slider_id);
        }

        $this->_modelSlider->deleteSlider($slider_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'module' => 'slider',
                    'controller' => 'management',
                    'action' => 'list',
                        ), 'admin', true);
    }

    protected function _getForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[$type] = $this->_modelSlider->getForm(ucfirst($type));
        $this->_forms[$type]->setAction($urlHelper->url(array(
                    'module' => 'slider',
                    'controller' => 'management',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms[$type]->setMethod('post');

        return $this->_forms[$type];
    }

}