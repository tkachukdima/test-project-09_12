<?php

class Slider_Model_Slider extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{
    
    private $_modules;
    private $_groups;
        
    public function __construct($options = null) {
        parent::__construct($options);
        
        
        $this->_modules = array(
            "10" => array(
                "name" => _('Home page'),
                "module" => "default",
                "controller" => "index",
                "action" => "index"
            ),
            "11" => array(
                "name" => _("Contacts"),
                "module" => "default",
                "controller" => "contact",
                "action" => "index"
            ),
//            "12" => array(
//                "name" => "Каталог",
//                "module" => "catalog",
//                "controller" => "index",
//                "action" => "index"
//            ),
//            "13" => array(
//                "name" => "Каталог/Продукты",
//                "module" => "catalog",
//                "controller" => "index",
//                "action" => "view"
//            ),
             "14" => array(
                "name" => _("Catalog"),
                "module" => "catalog",
                "controller" => "index",
                "action" => "index"
            ),
            "15" => array(
                "name" => _("Catalog/Products"),
                "module" => "catalog",
                "controller" => "index",
                "action" => "view"
            ),
//            "16" => array(
//                "name" => "Фотогалерея/Альбомы",
//                "module" => "default",
//                "controller" => "gallery",
//                "action" => "index"
//            ),
//            "17" => array(
//                "name" => "Фотогалерея/Фотографии",
//                "module" => "default",
//                "controller" => "gallery",
//                "action" => "view"
//            ),
            "18" => array(
                "name" => _("Site map"),
                "module" => "default",
                "controller" => "index",
                "action" => "sitemap"
            )
            /*,
            "11" => array(
                "name" => "",
                "module" => "",
                "controller" => "",
                "action" => ""
            ),*/
        );
        
        $this->_groups = array(
            array(1, _("Main")),
            array(2, _("Modules")),
            array(3, _("Pages")),
//            array(4, "Группы публикаций"),
//            array(5, "Группы файлов")
        );
        
    }
    
    /**
     * Get a slider post by its id
     *
     * @param  string $ident The ident
     * @return Slider_Resource_Slider_Item
     */
    public function getSliderById($id)
    {
        $id = (int) $id;
        return $this->getResource('Slider')->getSliderById($id);
    }
    
    public function getSlidersByArrayId($array_id)
    {
        return $this->getResource('Slider')->getSlidersByArrayId($array_id);
    }
     /**
     * Get a Slider by its id for edit
     *
     * @param  int $id The id
     * @return Slider_Resource_Slider_Item
     */
    public function getSliderByIdForEdit($id)
    {
        $id = (int) $id;

        $page =  $this->getResource('Slider')->getSliderByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $key => $value) {
            $data['title_' . $value['language_code']] = $value['title'];
            $data['url_' . $value['language_code']] = $value['url'];
            $data['description_' . $value['language_code']] = $value['description'];

        }

        return $data;

    }


    /**
     * Get slider
     *
     * @param int|boolean   $sliderd    Whether to slider results
     * @param integer|null  $limit    Order results
     * @param integer       $per_slider    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getSliders($status = null)
    {
        return $this->getResource('Slider')->getSliders($status);
    }

    /**
     * Save a slider post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveSlider($data, $validator = null)
    {
        if (!$this->checkAcl('saveSlider')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $mas = array();
        
        $group = isset($data['group'])? $data['group']:'';
        unset($data['group']);
        
        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm(ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $data['last_edit_id'] = $this->getIdentity()->user_id;

        $slider = array_key_exists('slider_id', $data) ?
                $this->getResource('Slider')->getSliderById($data['slider_id']) : null;

        if ($slider->full != '' AND $data['full'] == '') {
            $data['full'] = $slider->full;
            $data['thumbnail'] = $slider->thumbnail;
            $data['preview'] = $slider->preview;
        }

        if(null == $slider){
            $data['author_id'] = $this->getIdentity()->user_id;
        }


        $new =  $this->getResource('Slider')->saveRow($data, $slider);
        
        //--------------------------------------
        $this->getResource('SliderGroup')->deleteRows((int)$new);
        foreach ($group as $val)
        {
            $mas[]= array('slider_id' => $new,
                          'module_id' => (int)$val);

            $this->getResource('SliderGroup')->saveRow(array('slider_id' => $new,
                          'module_id' => (int)$val));
        }
        //-------------------------------------
        
        $this->getResource('Slider')->deleteTranslatedRows($new);

         foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'slider_id' => (int) $new,
                'language_code' => $lang->code,
                'title' => $data['title_' . $lang->code],
                'url' => $data['url_' . $lang->code],
                'description' => $data['description_' . $lang->code]
            );
  
            $this->getResource('Slider')->saveTranslatedRows($translate_data, 'insert');
        }

        $data['slider_id'] = $new;

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/slider');

        $filter = new ARTCMF_Filter_ImageSize();


        $path_parts = pathinfo($data['full']);
        $new_file_name = 'slider-image-' .  $new . '.' . $path_parts['extension'];
        copy($fileDestination . '/' . $data['full'], $fileDestination . '/' . $new_file_name);


        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel('slider');


        $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                ->setHeight($image_resize_setting['height_thumbnail'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                ->filter($fileDestination . '/' . $new_file_name);

        $preview = $filter->setWidth($image_resize_setting['width_preview'])
                ->setHeight($image_resize_setting['height_preview'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_preview']())
                ->filter($fileDestination . '/' . $new_file_name);

         $full = $filter->setWidth($image_resize_setting['width_full'])
                ->setHeight($image_resize_setting['height_full'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_full']())
                ->filter($fileDestination . '/' . $new_file_name);


        $data['thumbnail'] = basename($thumbnail);
        $data['preview'] = basename($preview);
        $data['full'] = basename($full);

        $new_slider = $this->getResource('Slider')->getSliderById($new);
        
        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('slider')
        );

        return $this->getResource('Slider')->saveRow($data, $new_slider);

    }

    public function deleteSlider($id)
    {
        if (!$this->checkAcl('deleteSlider')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $this->getResource('SliderGroup')->deleteRows((int)$id);
        $slider = $this->getSliderById($id);

        if (null !== $slider) {

            $fileDestination = realpath(APPLICATION_PATH . '/../www/images/slider');
            unlink($fileDestination . '/' . $slider->thumbnail);
            unlink($fileDestination . '/' . $slider->preview);
            unlink($fileDestination . '/' . $slider->full);
            $this->getResource('Slider')->deleteTranslatedRows($id);
            $slider->delete();
            return true;
        }

        return false;
    }

    public function moveSliderItem($id, $move)
    {
       $slider_item = $this->getSliderById($id);

       $near_sort =  $this->getResource('Slider')->getNearSort($slider_item->sort_order, $move);

        if (count($near_sort) != 0) {

            $old_sort_order = $slider_item->sort_order;
            $slider_item->sort_order = $near_sort[0]->sort_order;
            $slider_item->save();


            $slider_item_near = $this->getSliderById($near_sort[0]->slider_id);

            $slider_item_near->sort_order = $old_sort_order;
            $slider_item_near->save();

        }
        return $slider_item;
    }

     public function setNewSortOrder($id, $sort_order)
    {
        foreach ($id as $key => $id) {
            $slider_item =  $this->getSliderById($id);
            $slider_item->sort_order = (int) $sort_order[$key];
            $slider_item->save();
        }

        return $slider_item;
    }

    
    public function getArrayModule()
    {
        $array = $this->_modules;
        
        // $this->_modules + додаткові групи
        
        $modelPage = new Default_Model_Page();
        $pages = $modelPage->getPages();
        $k=0;
        foreach ($pages as $key => $value) {
            $array['2'.$k++] = array("name" => $value->title,
                                     "module" => "default",
                                     "controller" => "page",
                                     "action" => "index",
                                     "pageIdent" => $value->ident);
        }
        
        $modelPublication = new Publication_Model_Publication();
        $groups = $modelPublication->getPublicationGroups();
        $k=0;
        foreach ($groups as $key => $value) {
            //$options['Группы публикаций'][/*'/publication/index/groupIdent/'*/'3'.$k++] = $value->name;
            $array['3'.$k++] = array("name" => $value->name,
                                     "module" => "default",
                                     "controller" => "publication",
                                     "action" => "index",
                                     "groupIdent" => $value->ident);
        }
        
        // Добавити групи File

//        $modelFile = new File_Model_File();
//        $groups = $modelFile->getGroups();
//        $k=0;
//        foreach ($groups as $key => $value) {
//              $array['5'.$k++] = array("name" => $value->title,
//                                     "module" => "file",
//                                     "controller" => "index",
//                                     "action" => "index",
//                                     "group_id" => $value->group_id);
//        }
        
        return $array;
    }

    public function getListModule()
    {
        // формируем список страниц и разделов для выбора в меню

        //$options['/'] = 'Главная';

        $options = array();

        // @TODO перенести настройки списка в базу
        foreach ($this->_modules as $key => $val)
            $options['Модули'][$key] = $val['name'];
        
        $modelPage = new Default_Model_Page();
        $pages = $modelPage->getPages();
        $k=0;
        foreach ($pages as $key => $value) {
            $options['Страницы'][/*'/page/index/pageIdent/'*/'2'.$k++] = $value->title;
        }

        if (class_exists('Publication_Model_Publication')) {
            $modelPublication = new Publication_Model_Publication();
            $groups           = $modelPublication->getPublicationGroups();
            $k                = 0;
            foreach ($groups as $key => $value) {
                $options['Группы публикаций'][/* '/publication/index/groupIdent/' */'3' . $k++] = $value->name;
            }
        }

        if (class_exists('File_Model_File')) {
            // Добавити групи Файлів
            $modelFile = new File_Model_File();
            $groups    = $modelFile->getGroups();
            $k         = 0;
            foreach ($groups as $key => $value) {
                $options['Группы прайсов'][/* '/publication/index/groupIdent/' */'5' . $k++] = $value->title;
            }
        }

        return $options;
    }
    
    // групи слайдера (розділи)
    public function getGroupsBySliderId($slider_id)
    {
        return $this->getResource('SliderGroup')->getGroupsBySliderId($slider_id);
    }
    
    // групи слайдера (розділи)
    public function getSlidersByGroupId($group_id)
    {
        return $this->getResource('SliderGroup')->getSlidersByGroupId($group_id);
    }

     /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Slider';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
