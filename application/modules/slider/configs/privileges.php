<?php

return array(
    'Redactor' => array(
        'allow' => array(
            'management' => array(
                'list',
                'add',
                'edit',
                'save',
                'delete',
                'sort'
            ),
        )
    ),
    'Manager' => array(
    ),
    'Admin' => array(
    ),
);