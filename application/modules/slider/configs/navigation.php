<?php

return array(
    array(
        'label' => _('Slider'),
        'module' => 'slider',
        'controller' => 'management',
        'action' => 'list',
        'route' => 'admin',
        'resource' => 'slider:management',
        'privilege' => 'list',
        'reset_params' => true,
        'class' => 'dashboard-slider',
        'pages' => array(
            array(
                'label' => _('Slider'),
                'module' => 'slider',
                'controller' => 'management',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'slider:management',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit slide'),
                        'module' => 'slider',
                        'controller' => 'management',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'slider:management',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit slide'),
                        'module' => 'slider',
                        'controller' => 'management',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'slider:management',
                        'privilege' => 'save',
                        'reset_params' => true
                    )
                )
            ),
            array(
                'label' => _('Add slide'),
                'module' => 'slider',
                'controller' => 'management',
                'action' => 'add',
                'route' => 'admin',
                'resource' => 'slider:management',
                'privilege' => 'add',
                'reset_params' => true
            )
        )
    )
);