<?php

/**
 * Edit Slider
 *
 * @category   Default
 * @package    Slider_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Slider_Form_Edit extends Slider_Form_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('full')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
