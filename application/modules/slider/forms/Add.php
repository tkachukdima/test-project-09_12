<?php

/**
 * Add new slide
 *
 * @category   Default
 * @package    Slider_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Slider_Form_Add extends Slider_Form_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('slider_id')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
