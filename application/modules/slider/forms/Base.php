<?php

/**
 * Base Slider Form
 *
 * @category   Default
 * @package    Slider_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Slider_Form_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/slider');

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Slider_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Slider_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );

        $this->setMethod('post');
        $this->setAction('');

        foreach (Zend_Registry::get('langList') as $key => $lang) {

            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
                'description' => _('Title Image'),
            ));

            $this->addElement('text', 'url_' . $lang->code, array(
                'label' => _('Url'),
                'filters' => array('StringTrim'),
                'required' => true,
                'description' => _('Example: "/news"'),
            ));


            $this->addElement('textarea', 'description_' . $lang->code, array(
                'label' => _('Text'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => false
            ));

        }

        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingByIdent('slider');

        $this->addElement('file','full', array(
             'label' => _('Image'),
            'required' => true,
            'description' => sprintf(_('Image size must be %s. Supported formats: jpg, jpeg, png, gif.'), $image_resize_setting->width_full . 'x' . $image_resize_setting->height_full),
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576*5)),
                array('Extension', false, array('jpg', 'jpeg', 'png', 'gif')),
                array('ImageSize', false, array('minwidth' => $image_resize_setting->width_full,
                                                'maxwidth' => $image_resize_setting->width_full,
                                                'minheight' => $image_resize_setting->height_full,
                                                'maxheight' => $image_resize_setting->height_full))
            ),
        ));
/*
        $modelSlider = new Slider_Model_Slider();
        $this->addElement('multiselect', 'group', array(
            'label' => 'Група: ',
            'size' => '10',
            'multiple' => "multiple",
            'description' => 'В каком разделе выводить (можно выбрать несколько, используйте Ctrl или Shift)',
            'multiOptions' => $modelSlider->getListModule()
        ));
*/
         $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => 1
        ));


        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));


        $this->addElement('submit', 'submit', array());

         $this->addElement('hidden', 'slider_id', array(
            'filters'    => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper',array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));


          foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'title_' . $lang->code,
                'url_' . $lang->code,
                'description_' . $lang->code
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('category_service_id', 'full', 'group',  'sort_order', 'status', 'submit', 'slider_id'), 'form_all', array('legend' => _('General Settings')));


    }

}
