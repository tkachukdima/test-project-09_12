<?php

/**
 * The application bootstrap used by Zend_Application
 *
 * @category   Bootstrap
 * @package    Bootstrap
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * @var Zend_Log
     */
    protected $_logger;

    /**
     * @var Zend_Application_Module_Autoloader
     */
    protected $_resourceLoader;

    /**
     * @var Zend_Controller_Front
     */
    public $frontController;

    /**
     * Configure the resource autoloader
     */
    protected function _initConfigureResourceAutoloader()
    {
        $this->getResourceLoader()->addResourceTypes(array(
            'modelResource' => array(
                'path' => '/models/resources',
                'namespace' => 'Resource',
            ),
            'document' => array(
                'path' => '/models/document',
                'namespace' => 'Model_Document'
            )
        ));
//         $loader = new Zend_Loader_Autoloader_Resource(
//                        array(
//                            'basePath' => APPLICATION_PATH,
//                            'namespace' => ''
//                        )
//        );
        //$loader->addResourceType('block', 'blocks', 'Block');         
    }

    
    /**
     * Add the config to the registry
     */
    protected function _initConfig()
    {      
        Zend_Registry::set('settings', $this->getOptions());        
    }
    
    /**
     * Add the config to the registry
     */

    /**
     * Add required routes to the router
     */
    protected function _initRoutes()
    {
        $this->bootstrap('frontController');

        $router = $this->frontController->getRouter();

        $router->removeDefaultRoutes();
        $router->addRoute('default', new ARTCMF_Controller_Router_Route_MultilingualModule(
                        array(
                            'lang' => DEFAULT_LANG,
                            'module' => 'default',
                            'controller' => 'index',
                            'action' => 'index',
                        )
                )
        );

        $router->addRoute('admin', new Zend_Controller_Router_Route(
                        'admin/:module/:controller/:action/*',
                        array(
                            'module' => 'default',
                            'controller' => 'admin',
                            'action' => 'index',
                            'isAdmin' => true
                        )
        ));
    }

    /**
     * Add Controller Action Helpers
     */
    protected function _initActionHelpers()
    {
        Zend_Controller_Action_HelperBroker::addHelper(new ARTCMF_Controller_Action_Helper_Acl());
        // Zend_Controller_Action_HelperBroker::addHelper(new ARTCMF_Controller_Action_Helper_RedirectCommon());
        Zend_Controller_Action_HelperBroker::addHelper(new ARTCMF_Controller_Action_Helper_Service());
        Zend_Controller_Action_HelperBroker::addHelper(new ARTCMF_Controller_Action_Helper_CleanInput());
        //Zend_Controller_Action_HelperBroker::addHelper(new ARTCMF_Controller_Action_Helper_Block());
    }

    /**
     * Init the db metadata and paginator caches
     */
    protected function _initDbCaches()
    {
        if ('production' == $this->getEnvironment()) {

            $cache = Zend_Cache::factory('Core', 'File', array(
                        'lifetime' => null,
                        'automatic_serialization' => true,
                        'ignore_user_abort' => true
                            ), array(
                        'cache_dir' => APPLICATION_PATH . '/../data/cache/db',
                        'cache_file_perm' => '0666')
            );
            Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
        }        
    }   

    /**
     * Add gracefull error handling to the bootstrap process
     */
    protected function _bootstrap($resource = null)
    {
        $errorHandling = $this->getOption('errorhandling');
        try {
            parent::_bootstrap($resource);
        } catch (Exception $e) {
            if (true == (bool) $errorHandling['graceful']) {
                $this->__handleErrors($e, $errorHandling['email']);
            } else {
                throw $e;
            }
        }
    }

    /**
     * Add graceful error handling to the dispatch, this will handle
     * errors during Front Controller dispatch.
     */
    public function run()
    {
        $errorHandling = $this->getOption('errorhandling');
        try {
            parent::run();
        } catch (Exception $e) {
            if (true == (bool) $errorHandling['graceful']) {
                $this->__handleErrors($e, $errorHandling['email']);
            } else {
                throw $e;
            }
        }
    }

    /**
     * Handle errors gracefully, this will work as long as the views,
     * and the Zend classes are available
     *
     * @param Exception $e
     * @param string $email
     */
    protected function __handleErrors(Exception $e, $email)
    {
        header('HTTP/1.1 500 Internal Server Error');
        $view = new Zend_View();
        $view->addScriptPath(dirname(__FILE__) . '/layouts/scripts');
        echo $view->render('fatalError.phtml');

        if ('' != $email) {
            $mail = new Zend_Mail();
            $mail->setSubject('Fatal error in application');
            $mail->addTo($email);
            $mail->setBodyText(
                    $e->getFile() . "\n" .
                    $e->getMessage() . "\n" .
                    $e->getTraceAsString() . "\n" .
                    "SERVER: \n" .
                    var_export($_SERVER, true) . "\n" .
                    "SESSION: \n" .
                    var_export($_SESSION, true) . "\n"
            );
            @$mail->send();
        }
    }
          
//    protected function _initZFDebug()
//    {
//
//        if ('production' == $this->getEnvironment())
//        return;
//        
//        $autoloader = Zend_Loader_Autoloader::getInstance();
//        $autoloader->registerNamespace('ZFDebug');
//
//        $options = array(
//            'plugins' => array(
//                'Variables',
//                'File' => array('base_path' => APPLICATION_PATH . "/../"),
//                'Memory',
//                'Time',
//                'Registry',
//                'Exception',
//                'Html')
//        );
//
//        # Instantiate the database adapter and setup the plugin.
//        # Alternatively just add the plugin like above and rely on the autodiscovery feature.
//        if ($this->hasPluginResource('db')) {
//            $this->bootstrap('db');
//            $db = $this->getPluginResource('db')->getDbAdapter();           
//            $options['plugins']['Database']['adapter'] = $db;
//        }
//
//        # Setup the cache plugin
//        if ($this->hasPluginResource('cache')) {
//            $this->bootstrap('cache');
//            $cache = $this->getPluginResource('cache')->getDbAdapter();
//            $options['plugins']['Cache']['backend'] = $cache->getBackend();
//        }
//
//        $debug = new ZFDebug_Controller_Plugin_Debug($options);
//        
//        $this->bootstrap('frontController');
//        $this->frontController->registerPlugin($debug);              
//    }

}