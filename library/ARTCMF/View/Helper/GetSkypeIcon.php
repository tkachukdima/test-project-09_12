<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getRegisterForm
 *
 * @author admin
 */
class ARTCMF_View_Helper_GetSkypeIcon extends Zend_View_Helper_Abstract
{

    public function getSkypeIcon($username)
    {
        
//        if(APPLICATION_ENV != 'production')
//            return '1.png';
        
        $status = $this->getSkypeStatus($username);

        if(2 == $status) {
        // change the path of the icons folder to match your site
            return $status . '.png';
        } else {
            return '1.png';
        }
    }

    public function getSkypeStatus($username)
    {
        /*
          returns:
          0 - unknown
          1 - offline
          2 - online
          3 - away
          4 - not available
          5 - do not disturb
          6 - invisible
          7 - skype me
         */
        $remote_status = @fopen('http://mystatus.skype.com/' . $username . '.num', 'r');
        if (!$remote_status) {
            return '0';            
        }
        while (!feof($remote_status)) {
            $value = fgets($remote_status, 1024);
            return trim($value);
        }
        fclose($remote_status);
    }

}

