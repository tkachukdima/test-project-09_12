<?php

class ARTCMF_View_Helper_Truncate extends Zend_View_Helper_Abstract
{
    /*
     * 
     *
     * @return string
     */

    public function truncate($string, $length = 50, $break='.', $postfix = '...')
    {
        $truncated = trim($string);
        $length = (int) $length;
        if (!$string) {
            return $truncated;
        }

        $fullLength = iconv_strlen($truncated, 'UTF-8');
        if ($fullLength > $length) {
            if (false !== ($breakpoint = iconv_strpos($truncated, $break, $length))) {
                if ($breakpoint < strlen($truncated) - 1) {
                    $truncated = trim(iconv_substr($truncated, 0, $breakpoint)) . $postfix;
                }
            }
        }
        return $truncated;
    }

}
