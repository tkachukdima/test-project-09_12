<?php

/**
 *
 * @category   ARTCMF
 * @package    ARTCMF_View
 * @subpackage Helper
 */
class ARTCMF_View_Helper_MakeMenu
{

    public function makeMenu($identifier = 0, $minDepth = 0, $maxDepth = 50, $partial = null, $type = null)
    {

        if ($identifier !== 0) {
            $this->_identifier = $identifier;
        } else {
            return $this;
        }

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/layouts/scripts');

        $menuModel = new Default_Model_Menu();

        $menuArray = $menuModel/*->getCached('menu')*/->getMenuTree($this->_identifier);

      //   print_r($menuArray); die;

        $container = new Zend_Navigation($menuArray);

        if ($type == 'breadcrumbs') {
            $menu = $view->navigation()
                    ->breadcrumbs($container)
                    ->setMaxDepth($maxDepth)
                    ->setPartial($partial);
        }

        $menu = $view->navigation()
                ->menu($container)
                ->setMaxDepth($maxDepth)
                ->setMinDepth($minDepth);

        if (!is_null($partial))
            $menu->setPartial($partial);
      

        return $menu;
    }

}
