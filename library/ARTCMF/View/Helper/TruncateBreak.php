<?php

class ARTCMF_View_Helper_TruncateBreak extends Zend_View_Helper_Abstract
{
    /*
     *
     *
     * @return string
     */

    public function truncateBreak($string, $length = 50, $postfix = '...')
    {
        $truncated = trim($string);
        $length = (int) $length;
        if (!$string) {
            return $truncated;
        }
        $fullLength = iconv_strlen($truncated, 'UTF-8');
        if ($fullLength > $length) {
            $truncated = trim(iconv_substr($truncated, 0, $length, 'UTF-8')) . $postfix;
        }
        return $truncated;
    }

}
