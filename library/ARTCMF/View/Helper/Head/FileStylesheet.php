<?php

/**
 * Processor for handling operation with css files
 *
 * @package    View
 * @subpackage Helper
 * @version    _
 * @author     _
 * @link       _
 */
class ARTCMF_View_Helper_Head_FileStylesheet
    extends ARTCMF_View_Helper_Head_File
{
    /**
     * Return path to file described in item
     *
     * @param  stdClass $item
     * @return string|null
     */
    protected function _getItemPath($item)
    {
        return empty($item->href) ? null : $item->href;
    }

    /**
     * Return conditional attributes for item
     *
     * @param  stdClass $item
     * @return string|null
     */
    protected function _getItemConditional($item)
    {
        return isset($item->conditionalStylesheet) ? $item->conditionalStylesheet : false;
    }
}