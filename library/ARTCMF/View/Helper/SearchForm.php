<?php
/**
 * ARTCMF_View_Helper_SearchForm
 *
 * Helper for displaying the search form
 *
 * @category   Default
 * @package    ARTCMF_View_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_View_Helper_SearchForm extends Zend_View_Helper_Abstract
{
    public function searchForm()
    {
        $form = new Default_Form_Search_Base();
        $form->setAction($this->view->url(array(           
            'controller' => 'index' ,
            'action' => 'search'
            ),
            'default'
        ));
        $form->setMethod('get');
        return $form;
    }
}