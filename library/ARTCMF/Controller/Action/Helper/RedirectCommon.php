<?php

/**
 * Simple helper to encapsulate our most common redirects
 *
 * @category   Default
 * @package    ARTCMF_Controller_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Controller_Action_Helper_RedirectCommon extends Zend_Controller_Action_Helper_Abstract
{

    protected $_redirector;

    public function __construct()
    {
        $this->_redirector = new Zend_Controller_Action_Helper_Redirector();
    }

    public function gotoLogin()
    {
        $this->_redirector->gotoRoute(array(
            'controller' => 'customer',
            'action' => 'login',
                ),
                'default', true
        );
    }

    public function direct($redirect)
    {
        if (method_exists($this, $redirect)) {
            return $this->$redirect();
        }
        throw new ARTCMF_Exception('Unknown common redirect method');
    }

}