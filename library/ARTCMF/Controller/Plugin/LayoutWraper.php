<?php

/**
 * ARTCMF_Plugin_AdminContext
 * 
 * This plugin detects if we are in the admininstration area
 * and changes the layout to the admin template.
 * 
 * This relies on the admin route found in the initialization plugin
 *
 * @category   Default
 * @package    ARTCMF_Plugin
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Controller_Plugin_LayoutWraper extends Zend_Controller_Plugin_Abstract
{

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

        if ($request->getParam('isAdmin')) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->setLayout('admin');            
        }
    }

}