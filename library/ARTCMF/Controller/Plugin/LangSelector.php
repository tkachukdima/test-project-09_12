<?php

/**
 * Description of LangSelector
 *
 * @author jon
 */
class ARTCMF_Controller_Plugin_LangSelector extends Zend_Controller_Plugin_Abstract
{

    protected $langList;
    protected $site_settings = null;

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {

        $lang = $request->getParam('lang', '');
        $curentLang = '';
        $locale = '';
        $langList = $this->getLangList();

        Zend_Registry::set('langList', $langList);

       // print_r($langList->toArray());die;

        foreach ($langList as $langItem) {
            if ($lang == $langItem->code) {
                $curentLang = $langItem->code;
                $locale = $langItem->locale;
            }
        }

        if ('' == $curentLang)
            $lang = $langList[0]->code;

        if ('' == $locale)
            $locale = $langList[0]->locale;

        $request->setParam('lang', $lang);

        $lang = $request->getParam('lang');

        //@refact временно
//        if ($request->getParam('isAdmin')) {
//            $locale = 'en_EN';
//            $lang = 'en';
//        }
       
        Zend_Registry::set('Current_Lang', $lang);

         
        $zl = new Zend_Locale();
        $zl->setLocale($locale);
        Zend_Registry::set('Zend_Locale', $zl);

         $params = array(
            'scan' => Zend_Translate_Adapter::LOCALE_DIRECTORY,
            'logUntranslated' => true,
//            'logMessage'      => "Неизвестное сообщение '%message%' в локале '%locale%'",
        );

        $log = new Zend_Log();
//        if (APPLICATION_ENV == 'development') {
//            $log->addWriter(new Zend_Log_Writer_Stream(APPLICATION_PATH . '/../data/logs/translate.log'));
//        } else {
//            $log->addWriter(new Zend_Log_Writer_Null());
//        }
        $log->addWriter(new Zend_Log_Writer_Null());
        $params['log'] = $log;



        $translate = new Zend_Translate(
                'gettext',
                APPLICATION_PATH . '/languages/' . $lang . '.mo',
                $lang,
                $params);

        $frontendOptions = array(
            'lifetime' => 7200, // время жизни кэша - 2 часа
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => APPLICATION_PATH . '/../data/cache/' // директория, в которой размещаются файлы кэша
        );

        // получение объекта Zend_Cache_Core
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

        Zend_Translate::setCache($cache);

        Zend_Registry::set('Zend_Translate', $translate);

        Zend_Validate_Abstract::setDefaultTranslator($translate);
        Zend_Form::setDefaultTranslator($translate);
        Zend_Controller_Router_Route::setDefaultTranslator($translate);
        
        Zend_Registry::set('site_settings', $this->getAllSettings());        
    }

    private function getLangList()
    {
        if (is_null($this->langList)) {
            $langModel = new Default_Model_Language();
            $this->langList = $langModel->getCached('languages')->getLanguages();
        }

        return $this->langList;
    }

    private function getAllSettings()
    {

        if (is_null($this->site_settings)) {
            $modelSettings = new Default_Model_Settings();
            $site_settings = $modelSettings->getAllSettings();

            $serviceSettings = new Default_Service_Settings();
            $site_settings = $serviceSettings->setAllSettings($site_settings);

            //print_r($site_settings->toArray());die;
            if(is_null($site_settings))
                throw new ARTCMF_Exception(_('Not exist settings translation for current lang'));

            $this->site_settings = $site_settings;
        }

        return $this->site_settings;
    }

}
