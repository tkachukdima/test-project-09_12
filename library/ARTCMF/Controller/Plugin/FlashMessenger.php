<?php

/**
 * ARTCMF_Plugin_AdminContext
 * 
 * The FlashMessenger plugin will allow you to easily set up flash messaging within your application
 * @category   Default
 * @package    ARTCMF_Plugin
* * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Controller_Plugin_FlashMessenger extends Zend_Controller_Plugin_Abstract
{

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        // Инициализируем помощник FlashMessenger и получаем сообщения
        $actionHelperFlashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');

        $messagesSuccess = $actionHelperFlashMessenger->setNamespace('success')->getMessages();
        $messagesError = $actionHelperFlashMessenger->setNamespace('error')->getMessages();

        // Если сообщений нет, или процес диспетчеризации не закончен успешно, просто выходим из плагина
        if ((empty($messagesSuccess) && empty($messagesError)) || !$request->isDispatched()) {
            return;
        }

        // Получаем объект  Zend_Layout
        $layout = Zend_Layout::getMvcInstance();
        // Получаем объект  вида
        $view = $layout->getView();
        // Добавляем переменную для вида
        $view->messagesSuccess = $messagesSuccess;
        $view->messagesError = $messagesError;
        
    }

}