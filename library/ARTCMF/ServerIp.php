<?php

/**
 * Description of ARTCMF_GetServerIp
 *
 * @category   Default
 * @package    ARTCMF_Core
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_ServerIp
{

    public static function getServerIp()
    {
        return $_SERVER['SERVER_ADDR'];
    }

}