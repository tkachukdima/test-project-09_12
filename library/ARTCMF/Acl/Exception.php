<?php

/**
 * ARTCMF_Acl_Exception
 *
 * @category   Default
 * @package    ARTCMF_Acl
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Acl_Exception extends ARTCMF_Exception
{

}
