<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommentElemetIsset
 *
 * @author admin
 */
class ARTCMF_Validate_CommentElemetIsset extends Zend_Validate_Abstract {
    const ELEMENT_EXISTS = 'element_idExists';

    protected $_messageTemplates = array(
        self::ELEMENT_EXISTS => 'Элемент:"%value%", не существует в системе',
    );
    protected $_modelComment;
    protected $_commentType;

    public function isValid($value, $context = null) {
        $this->_modelComment = new Comment_Model_Comment();
        $this->_commentType = $this->_modelComment->getType();
        $this->_setValue($value);
        $type_value = $context['type'];
        
        if (!isset($this->_commentType[$type_value])) {
            $this->_error(self::ELEMENT_EXISTS);
            return false;
        }
        
        $model_element = new $this->_commentType[$type_value]['model']();
        $metod = $this->_commentType[$type_value]['metod'];
        $element = $model_element->$metod($value);
        if ($element) {
            return true;
        }
        $this->_error(self::ELEMENT_EXISTS);
        return false;
    }

}

