<?php

/**
 * ARTCMF_Model_Cache
 * 
 * Concrete model cache
 * 
 * @category   Default
 * @package    ARTCMF_Model
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Model_Cache extends ARTCMF_Model_Cache_Abstract
{

}
