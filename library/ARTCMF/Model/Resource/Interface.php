<?php

/**
 * ARTCMF_Model_Resource_Interface
 * 
 * General resource interface
 * 
 * @category   Default
 * @package    ARTCMF_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
interface ARTCMF_Model_Resource_Interface
{

}