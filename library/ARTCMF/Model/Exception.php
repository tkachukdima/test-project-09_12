<?php

/**
 * ARTCMF_Model_Exception
 *
 * @category   Default
 * @package    ARTCMF_Model
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Model_Exception extends ARTCMF_Exception
{

}
