<?php

class ARTCMF_Session_SaveHandler_DbTable extends Zend_Session_SaveHandler_DbTable
{

    /**
     * Write session data
     *
     * @param string $id
     * @param string $data
     * @return boolean
     */
    public function write($id, $data)
    {
        $return = false;

        if (Zend_Auth::getInstance()->hasIdentity())
            $userId = Zend_Auth::getInstance()->getIdentity()->user_id;
        else
            $userId = null;

        $data = array(
            'user_id'              => $userId,
            $this->_modifiedColumn => time(),
            $this->_dataColumn     => (string) $data);


        $rows = call_user_func_array(array(&$this, 'find'), $this->_getPrimary($id));

        if (count($rows)) {
            $data[$this->_lifetimeColumn] = $this->_getLifetime($rows->current());

            if ($this->update($data, $this->_getPrimary($id, self::PRIMARY_TYPE_WHERECLAUSE))) {
                $return = true;
            }
        } else {
            $data[$this->_lifetimeColumn] = $this->_lifetime;

            if ($this->insert(array_merge($this->_getPrimary($id, self::PRIMARY_TYPE_ASSOC), $data))) {
                $return = true;
            }
        }

        return $return;
    }

}