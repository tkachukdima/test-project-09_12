<?php

/**
* @see Zend_Filter_ImageSize_Strategy_Interface
*/
require_once 'ARTCMF/Filter/ImageSize/Strategy/Interface.php';

/**
* Strategy for resizing the image so that its smalles edge fits into the frame.
* The rest is cropped.
*/
class ARTCMF_Filter_ImageSize_Strategy_Crop
    implements ARTCMF_Filter_ImageSize_Strategy_Interface
{
    /**
* Return canvas resized according to the given dimensions.
* @param resource $image GD image resource
* @param int $width Output width
* @param int $height Output height
* @return resource GD image resource
*/
    public function resize($image, $width, $height, $coordinates)
    {
        $origWidth = imagesx($image);
        $origHeight = imagesy($image);

         if ($origWidth < $width) {
            $width = $origWidth;
        }

        if ($origHeight < $height) {
            $height = $origHeight;
        }
        
        $cropped = imagecreatetruecolor($width, $height);
              
        imageAlphaBlending($cropped, false);
        imageSaveAlpha($cropped, true);
        
        if (count($coordinates)) {

            imagecopyresampled($cropped, $image, 0, 0, $coordinates['x'], $coordinates['y'], 
                    $width, $height, $coordinates['w'], $coordinates['h']);

            return $cropped;
        }

         // вырезаем квадратную серединку по x, если фото горизонтальное
        if ($origWidth > $origHeight)
            $ratio =  min($origWidth / $width, $origHeight / $height);
            $newWidth =  $origWidth / $ratio;
            $newHeight =  $origHeight / $ratio;        
            imagecopyresampled($cropped, $image, 0, 0,
                    ($origWidth - $width * $ratio) / 2,
                    0, $newWidth, $newHeight, $origWidth, $origHeight);

        // вырезаем квадратную верхушку по y,
        if ($origWidth < $origHeight)
            $ratio =  min($origWidth / $width, $origHeight / $height);
            $newWidth =  $origWidth / $ratio;
            $newHeight =  $origHeight / $ratio;
            
            imagecopyresampled($cropped, $image, 0, 0, 0, 0, 
                    $newWidth, $newHeight, $origWidth, $origHeight);

        // квадратная картинка масштабируется без вырезок
        if ($origWidth == $origHeight)
            imagecopyresampled($cropped, $image, 0, 0, 0, 0, $width, $width, $origWidth, $origWidth);
        
        return $cropped;
    }
} 