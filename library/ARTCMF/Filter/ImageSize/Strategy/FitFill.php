<?php

/**
 * @see ARTCMF_Filter_ImageSize_Strategy_Interface
 */
require_once 'ARTCMF/Filter/ImageSize/Strategy/Interface.php';

/**
 * Strategy for resizing the image by fitting the content into the given
 * dimensions.
 */
class ARTCMF_Filter_ImageSize_Strategy_FitFill implements ARTCMF_Filter_ImageSize_Strategy_Interface
{

    /**
     * Return canvas resized according to the given dimensions.
     * @param resource $image GD image resource
     * @param int $width Output width
     * @param int $height Output height
     * @return resource GD image resource
     */
    public function resize($image, $width, $height, $coordinates)
    {
        $origWidth = imagesx($image);
        $origHeight = imagesy($image);

        if ($origWidth < $width) {
            $width = $origWidth;
        }

        if ($origHeight < $height) {
            $height = $origHeight;
        }


        $ratio = max($origWidth / $width, $origHeight / $height);
        $newWidth = $origWidth / $ratio;
        $newHeight = $origHeight / $ratio;

        $cropped = imagecreatetruecolor($width, $height);
        
        imageAlphaBlending($cropped, false);
        imageSaveAlpha($cropped, true);
        
        $white = imagecolorallocate($cropped, 255, 255, 255);
        imagefill($cropped, 0, 0, $white);


        if ($origWidth > $origHeight) {

            imagecopyresampled($cropped, $image, 0, ($height - $newHeight) / 2, 0, 0, $width, $newHeight, $origWidth, $origHeight);
        }

        if ($origWidth <= $origHeight) {

            imagecopyresampled($cropped, $image, ($width - $newWidth) / 2, 0, 0, 0, $newWidth, $height, $origWidth, $origHeight);
        }


        return $cropped;
    }

}