<?php

/**
 * Application 404 exception
 * 
 * @category   Default
 * @package    ARTCMF_Core
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Exception_404 extends ARTCMF_Exception
{

}