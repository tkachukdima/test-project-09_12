

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features_translation`
--

CREATE TABLE IF NOT EXISTS `shop_product_features_translation` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `full_description` mediumtext NOT NULL,
  `prefix` varchar(128) NOT NULL DEFAULT '',
  `suffix` varchar(128) NOT NULL DEFAULT '',
  `language_id` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`feature_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_features_translation`
--

INSERT INTO `shop_product_features_translation` (`feature_id`, `description`, `full_description`, `prefix`, `suffix`, `lang_code`) VALUES
(16, 'Display', '', '', '', 'EN'),
(17, 'Storage Capacity', '', '', '', 'EN'),
(15, 'Operating System', '', '', '', 'EN'),
(14, 'Electronics', '', '', '', 'EN'),
(16, 'Display', '', '', '', 'ru'),
(17, 'Storage Capacity', '', '', '', 'ru'),
(15, 'Operating System', '', '', '', 'ru'),
(14, 'Electronics', '', '', '', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features_values`
--

CREATE TABLE IF NOT EXISTS `shop_product_features_values` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant_id` mediumint(8) unsigned DEFAULT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  `value_int` int(11) unsigned DEFAULT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  KEY `fl` (`feature_id`,`lang_code`,`variant_id`,`value`,`value_int`),
  KEY `variant_id` (`variant_id`),
  KEY `lang_code` (`lang_code`),
  KEY `product_id` (`product_id`),
  KEY `fpl` (`feature_id`,`product_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_features_values`
--

INSERT INTO `shop_product_features_values` (`feature_id`, `product_id`, `variant_id`, `value`, `value_int`, `lang_code`) VALUES
(15, 240, 39, '', NULL, 'EN'),
(16, 227, 41, '', NULL, 'EN'),
(16, 240, 44, '', NULL, 'EN'),
(15, 241, 39, '', NULL, 'EN'),
(16, 241, 44, '', NULL, 'EN'),
(15, 214, 46, '', NULL, 'EN'),
(17, 214, 47, '', NULL, 'EN'),
(15, 217, 46, '', NULL, 'EN'),
(17, 217, 48, '', NULL, 'EN'),
(15, 218, 49, '', NULL, 'EN'),
(17, 218, 50, '', NULL, 'EN'),
(16, 229, 41, '', NULL, 'EN'),
(16, 228, 51, '', NULL, 'EN'),
(15, 223, 52, '', NULL, 'EN'),
(16, 223, 53, '', NULL, 'EN'),
(17, 223, 54, '', NULL, 'EN'),
(15, 225, 55, '', NULL, 'EN'),
(16, 225, 56, '', NULL, 'EN'),
(17, 225, 45, '', NULL, 'EN'),
(15, 224, 58, '', NULL, 'EN'),
(16, 224, 53, '', NULL, 'EN'),
(17, 224, 45, '', NULL, 'EN'),
(15, 219, 46, '', NULL, 'EN'),
(16, 219, 59, '', NULL, 'EN'),
(17, 219, 60, '', NULL, 'EN'),
(15, 222, 46, '', NULL, 'EN'),
(16, 222, 59, '', NULL, 'EN'),
(17, 222, 60, '', NULL, 'EN'),
(15, 220, 46, '', NULL, 'EN'),
(16, 220, 61, '', NULL, 'EN'),
(17, 220, 62, '', NULL, 'EN'),
(15, 221, 46, '', NULL, 'EN'),
(16, 221, 63, '', NULL, 'EN'),
(17, 221, 64, '', NULL, 'EN'),
(15, 238, 39, '', NULL, 'EN'),
(16, 238, 65, '', NULL, 'EN'),
(17, 238, 45, '', NULL, 'EN'),
(15, 239, 39, '', NULL, 'EN'),
(16, 239, 65, '', NULL, 'EN'),
(17, 239, 45, '', NULL, 'EN'),
(15, 74, 66, '', NULL, 'EN'),
(16, 74, 67, '', NULL, 'EN'),
(17, 74, 45, '', NULL, 'EN'),
(15, 73, 66, '', NULL, 'EN'),
(16, 73, 68, '', NULL, 'EN'),
(17, 73, 45, '', NULL, 'EN'),
(15, 71, 66, '', NULL, 'EN'),
(16, 71, 67, '', NULL, 'EN'),
(17, 71, 45, '', NULL, 'EN'),
(15, 68, 69, '', NULL, 'EN'),
(16, 68, 70, '', NULL, 'EN'),
(17, 68, 45, '', NULL, 'EN'),
(15, 113, 69, '', NULL, 'EN'),
(16, 113, 71, '', NULL, 'EN'),
(17, 113, 72, '', NULL, 'EN'),
(15, 117, 73, '', NULL, 'EN'),
(16, 117, 70, '', NULL, 'EN'),
(17, 117, 54, '', NULL, 'EN'),
(15, 120, 66, '', NULL, 'EN'),
(16, 120, 74, '', NULL, 'EN'),
(17, 120, 45, '', NULL, 'EN'),
(15, 125, 66, '', NULL, 'EN'),
(16, 125, 68, '', NULL, 'EN'),
(17, 125, 45, '', NULL, 'EN'),
(15, 69, 66, '', NULL, 'EN'),
(16, 69, 71, '', NULL, 'EN'),
(17, 69, 75, '', NULL, 'EN'),
(15, 101, 76, '', NULL, 'EN'),
(16, 101, 77, '', NULL, 'EN'),
(17, 101, 45, '', NULL, 'EN'),
(15, 105, 69, '', NULL, 'EN'),
(16, 105, 71, '', NULL, 'EN'),
(17, 105, 45, '', NULL, 'EN'),
(15, 106, 78, '', NULL, 'EN'),
(16, 106, 77, '', NULL, 'EN'),
(17, 106, 79, '', NULL, 'EN'),
(15, 86, 78, '', NULL, 'EN'),
(16, 86, 80, '', NULL, 'EN'),
(17, 86, 81, '', NULL, 'EN'),
(15, 67, 66, '', NULL, 'EN'),
(16, 67, 80, '', NULL, 'EN'),
(17, 67, 82, '', NULL, 'EN'),
(15, 70, 66, '', NULL, 'EN'),
(16, 70, 67, '', NULL, 'EN'),
(17, 70, 75, '', NULL, 'EN'),
(15, 72, 66, '', NULL, 'EN'),
(16, 72, 68, '', NULL, 'EN'),
(17, 72, 45, '', NULL, 'EN'),
(15, 75, 55, '', NULL, 'EN'),
(16, 75, 83, '', NULL, 'EN'),
(17, 75, 75, '', NULL, 'EN'),
(15, 76, 84, '', NULL, 'EN'),
(16, 76, 83, '', NULL, 'EN'),
(17, 76, 81, '', NULL, 'EN'),
(15, 240, 39, '', NULL, 'ru'),
(16, 227, 41, '', NULL, 'ru'),
(16, 240, 44, '', NULL, 'ru'),
(15, 241, 39, '', NULL, 'ru'),
(16, 241, 44, '', NULL, 'ru'),
(15, 214, 46, '', NULL, 'ru'),
(17, 214, 47, '', NULL, 'ru'),
(15, 217, 46, '', NULL, 'ru'),
(17, 217, 48, '', NULL, 'ru'),
(15, 218, 49, '', NULL, 'ru'),
(17, 218, 50, '', NULL, 'ru'),
(16, 229, 41, '', NULL, 'ru'),
(16, 228, 51, '', NULL, 'ru'),
(15, 223, 52, '', NULL, 'ru'),
(16, 223, 53, '', NULL, 'ru'),
(17, 223, 54, '', NULL, 'ru'),
(15, 225, 55, '', NULL, 'ru'),
(16, 225, 56, '', NULL, 'ru'),
(17, 225, 45, '', NULL, 'ru'),
(15, 224, 58, '', NULL, 'ru'),
(16, 224, 53, '', NULL, 'ru'),
(17, 224, 45, '', NULL, 'ru'),
(15, 219, 46, '', NULL, 'ru'),
(16, 219, 59, '', NULL, 'ru'),
(17, 219, 60, '', NULL, 'ru'),
(15, 222, 46, '', NULL, 'ru'),
(16, 222, 59, '', NULL, 'ru'),
(17, 222, 60, '', NULL, 'ru'),
(15, 220, 46, '', NULL, 'ru'),
(16, 220, 61, '', NULL, 'ru'),
(17, 220, 62, '', NULL, 'ru'),
(15, 221, 46, '', NULL, 'ru'),
(16, 221, 63, '', NULL, 'ru'),
(17, 221, 64, '', NULL, 'ru'),
(15, 238, 39, '', NULL, 'ru'),
(16, 238, 65, '', NULL, 'ru'),
(17, 238, 45, '', NULL, 'ru'),
(15, 239, 39, '', NULL, 'ru'),
(16, 239, 65, '', NULL, 'ru'),
(17, 239, 45, '', NULL, 'ru'),
(15, 74, 66, '', NULL, 'ru'),
(16, 74, 67, '', NULL, 'ru'),
(17, 74, 45, '', NULL, 'ru'),
(15, 73, 66, '', NULL, 'ru'),
(16, 73, 68, '', NULL, 'ru'),
(17, 73, 45, '', NULL, 'ru'),
(15, 71, 66, '', NULL, 'ru'),
(16, 71, 67, '', NULL, 'ru'),
(17, 71, 45, '', NULL, 'ru'),
(15, 68, 69, '', NULL, 'ru'),
(16, 68, 70, '', NULL, 'ru'),
(17, 68, 45, '', NULL, 'ru'),
(15, 113, 69, '', NULL, 'ru'),
(16, 113, 71, '', NULL, 'ru'),
(17, 113, 72, '', NULL, 'ru'),
(15, 117, 73, '', NULL, 'ru'),
(16, 117, 70, '', NULL, 'ru'),
(17, 117, 54, '', NULL, 'ru'),
(15, 120, 66, '', NULL, 'ru'),
(16, 120, 74, '', NULL, 'ru'),
(17, 120, 45, '', NULL, 'ru'),
(15, 125, 66, '', NULL, 'ru'),
(16, 125, 68, '', NULL, 'ru'),
(17, 125, 45, '', NULL, 'ru'),
(15, 69, 66, '', NULL, 'ru'),
(16, 69, 71, '', NULL, 'ru'),
(17, 69, 75, '', NULL, 'ru'),
(15, 101, 76, '', NULL, 'ru'),
(16, 101, 77, '', NULL, 'ru'),
(17, 101, 45, '', NULL, 'ru'),
(15, 105, 69, '', NULL, 'ru'),
(16, 105, 71, '', NULL, 'ru'),
(17, 105, 45, '', NULL, 'ru'),
(15, 106, 78, '', NULL, 'ru'),
(16, 106, 77, '', NULL, 'ru'),
(17, 106, 79, '', NULL, 'ru'),
(15, 86, 78, '', NULL, 'ru'),
(16, 86, 80, '', NULL, 'ru'),
(17, 86, 81, '', NULL, 'ru'),
(15, 67, 66, '', NULL, 'ru'),
(16, 67, 80, '', NULL, 'ru'),
(17, 67, 82, '', NULL, 'ru'),
(15, 70, 66, '', NULL, 'ru'),
(16, 70, 67, '', NULL, 'ru'),
(17, 70, 75, '', NULL, 'ru'),
(15, 72, 66, '', NULL, 'ru'),
(16, 72, 68, '', NULL, 'ru'),
(17, 72, 45, '', NULL, 'ru'),
(15, 75, 55, '', NULL, 'ru'),
(16, 75, 83, '', NULL, 'ru'),
(17, 75, 75, '', NULL, 'ru'),
(15, 76, 84, '', NULL, 'ru'),
(16, 76, 83, '', NULL, 'ru'),
(17, 76, 81, '', NULL, 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_feature_variants`
--

CREATE TABLE IF NOT EXISTS `shop_product_feature_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variant_id`),
  KEY `feature_id` (`feature_id`),
  KEY `position` (`position`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Дамп данных таблицы `shop_product_feature_variants`
--

INSERT INTO `shop_product_feature_variants` (`variant_id`, `feature_id`, `url`, `position`) VALUES
(54, 17, '', 0),
(53, 16, '', 0),
(63, 16, '', 0),
(62, 17, '', 0),
(61, 16, '', 0),
(65, 16, '', 0),
(64, 17, '', 0),
(60, 17, '', 0),
(59, 16, '', 0),
(58, 15, '', 0),
(56, 16, '', 0),
(55, 15, '', 0),
(52, 15, '', 0),
(51, 16, '', 0),
(50, 17, '', 0),
(49, 15, '', 0),
(48, 17, '', 0),
(47, 17, '', 0),
(45, 17, '', 0),
(44, 16, '', 0),
(46, 15, '', 0),
(41, 16, '', 0),
(68, 16, '', 0),
(39, 15, '', 40),
(69, 15, '', 0),
(70, 16, '', 0),
(66, 15, '', 0),
(67, 16, '', 0),
(71, 16, '', 0),
(72, 17, '', 0),
(73, 15, '', 0),
(74, 16, '', 0),
(75, 17, '', 0),
(76, 15, '', 0),
(77, 16, '', 0),
(78, 15, '', 0),
(79, 17, '', 0),
(80, 16, '', 0),
(81, 17, '', 0),
(82, 17, '', 0),
(83, 16, '', 0),
(84, 15, '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_feature_variant_translation`
--

CREATE TABLE IF NOT EXISTS `shop_product_feature_variant_translation` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `meta_keywords` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`variant_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_feature_variant_translation`
--

INSERT INTO `shop_product_feature_variant_translation` (`variant_id`, `variant`, `description`, `page_title`, `meta_keywords`, `meta_description`, `lang_code`) VALUES
(51, '23"', '', '', '', '', 'EN'),
(50, '320GB', '', '', '', '', 'EN'),
(58, 'Android 3.2, Honeycomb', '', '', '', '', 'EN'),
(56, '7.0"', '', '', '', '', 'EN'),
(55, 'Android 2.2, Froyo', '', '', '', '', 'EN'),
(54, '32GB', '', '', '', '', 'EN'),
(53, '8.9"', '', '', '', '', 'EN'),
(52, 'Android 3.1, Honeycomb', '', '', '', '', 'EN'),
(49, 'Windows 7 Home Basic', '', '', '', '', 'EN'),
(48, '500GB', '', '', '', '', 'EN'),
(47, '1TB', '', '', '', '', 'EN'),
(45, '16GB', '', '', '', '', 'EN'),
(44, '9.7"', '', '', '', '', 'EN'),
(46, 'Windows 7 Home Premium', '', '', '', '', 'EN'),
(41, '27"', '', '', '', '', 'EN'),
(68, '4.3"', '', '', '', '', 'EN'),
(39, 'iOS 5', '', '', '', '', 'EN'),
(69, 'Windows Phone 7.5, Mango', '', '', '', '', 'EN'),
(70, '4.7"', '', '', '', '', 'EN'),
(71, '3.7"', '', '', '', '', 'EN'),
(59, '15.6"', '', '', '', '', 'EN'),
(60, '640GB', '', '', '', '', 'EN'),
(61, '14.0"', '', '', '', '', 'EN'),
(62, '750GB', '', '', '', '', 'EN'),
(63, '13.3"', '', '', '', '', 'EN'),
(64, '256GB', '', '', '', '', 'EN'),
(65, '3.5"', '', '', '', '', 'EN'),
(66, 'Android 2.3, Gingerbread', '', '', '', '', 'EN'),
(67, '4.0"', '', '', '', '', 'EN'),
(72, '8GB', '', '', '', '', 'EN'),
(73, 'Android 4.0, Ice Cream Sandwich', '', '', '', '', 'EN'),
(74, '4.5"', '', '', '', '', 'EN'),
(75, '4GB', '', '', '', '', 'EN'),
(76, 'Nokia OS', '', '', '', '', 'EN'),
(77, '2.4"', '', '', '', '', 'EN'),
(78, 'Symbian OS', '', '', '', '', 'EN'),
(79, '256MB', '', '', '', '', 'EN'),
(80, '3.2"', '', '', '', '', 'EN'),
(81, '2GB', '', '', '', '', 'EN'),
(82, '512MB', '', '', '', '', 'EN'),
(83, '3.1"', '', '', '', '', 'EN'),
(84, 'Android 2.1', '', '', '', '', 'EN'),
(51, '23"', '', '', '', '', 'ru'),
(50, '320GB', '', '', '', '', 'ru'),
(58, 'Android 3.2, Honeycomb', '', '', '', '', 'ru'),
(56, '7.0"', '', '', '', '', 'ru'),
(55, 'Android 2.2, Froyo', '', '', '', '', 'ru'),
(54, '32GB', '', '', '', '', 'ru'),
(53, '8.9"', '', '', '', '', 'ru'),
(52, 'Android 3.1, Honeycomb', '', '', '', '', 'ru'),
(49, 'Windows 7 Home Basic', '', '', '', '', 'ru'),
(48, '500GB', '', '', '', '', 'ru'),
(47, '1TB', '', '', '', '', 'ru'),
(45, '16GB', '', '', '', '', 'ru'),
(44, '9.7"', '', '', '', '', 'ru'),
(46, 'Windows 7 Home Premium', '', '', '', '', 'ru'),
(41, '27"', '', '', '', '', 'ru'),
(68, '4.3"', '', '', '', '', 'ru'),
(39, 'iOS 5', '', '', '', '', 'ru'),
(69, 'Windows Phone 7.5, Mango', '', '', '', '', 'ru'),
(70, '4.7"', '', '', '', '', 'ru'),
(71, '3.7"', '', '', '', '', 'ru'),
(59, '15.6"', '', '', '', '', 'ru'),
(60, '640GB', '', '', '', '', 'ru'),
(61, '14.0"', '', '', '', '', 'ru'),
(62, '750GB', '', '', '', '', 'ru'),
(63, '13.3"', '', '', '', '', 'ru'),
(64, '256GB', '', '', '', '', 'ru'),
(65, '3.5"', '', '', '', '', 'ru'),
(66, 'Android 2.3, Gingerbread', '', '', '', '', 'ru'),
(67, '4.0"', '', '', '', '', 'ru'),
(72, '8GB', '', '', '', '', 'ru'),
(73, 'Android 4.0, Ice Cream Sandwich', '', '', '', '', 'ru'),
(74, '4.5"', '', '', '', '', 'ru'),
(75, '4GB', '', '', '', '', 'ru'),
(76, 'Nokia OS', '', '', '', '', 'ru'),
(77, '2.4"', '', '', '', '', 'ru'),
(78, 'Symbian OS', '', '', '', '', 'ru'),
(79, '256MB', '', '', '', '', 'ru'),
(80, '3.2"', '', '', '', '', 'ru'),
(81, '2GB', '', '', '', '', 'ru'),
(82, '512MB', '', '', '', '', 'ru'),
(83, '3.1"', '', '', '', '', 'ru'),
(84, 'Android 2.1', '', '', '', '', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filters`
--

CREATE TABLE IF NOT EXISTS `shop_product_filters` (
  `filter_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `categories_path` text NOT NULL,
  `company_id` int(11) unsigned DEFAULT '0',
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `field_type` char(1) NOT NULL DEFAULT '',
  `show_on_home_page` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  `round_to` smallint(5) unsigned NOT NULL DEFAULT '1',
  `display` char(1) NOT NULL DEFAULT 'Y',
  `display_count` smallint(5) unsigned NOT NULL DEFAULT '10',
  PRIMARY KEY (`filter_id`),
  KEY `feature_id` (`feature_id`),
  KEY `company_id` (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `shop_product_filters`
--

INSERT INTO `shop_product_filters` (`filter_id`, `categories_path`, `company_id`, `feature_id`, `position`, `field_type`, `show_on_home_page`, `status`, `round_to`, `display`, `display_count`) VALUES
(1, '', 0, 0, 0, 'P', 'Y', 'A', 1, 'Y', 10),
(7, '165,166,167,168,169,234,235,236,237,238,240', 0, 15, 10, '', 'Y', 'A', 1, 'Y', 10),
(8, '165,166,167,168,169,170,174,190,191,193,234,235,236,237,238,240', 0, 16, 20, '', 'Y', 'A', 1, 'Y', 10),
(9, '165,166,167,168,169,234,235,236,237,238,240', 0, 17, 30, '', 'Y', 'A', 1, 'Y', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filter_translation`
--

CREATE TABLE IF NOT EXISTS `shop_product_filter_translation` (
  `filter_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `filter` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`filter_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_filter_translation`
--

INSERT INTO `shop_product_filter_translation` (`filter_id`, `lang_code`, `filter`) VALUES
(1, 'EN', 'Price'),
(9, 'EN', 'Storage Capacity'),
(8, 'EN', 'Display'),
(7, 'EN', 'Operating System'),
(1, 'ru', 'Price'),
(9, 'ru', 'Storage Capacity'),
(8, 'ru', 'Display'),
(7, 'ru', 'Operating System');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filter_ranges`
--

CREATE TABLE IF NOT EXISTS `shop_product_filter_ranges` (
  `range_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `filter_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `from` decimal(12,2) NOT NULL DEFAULT '0.00',
  `to` decimal(12,2) NOT NULL DEFAULT '0.00',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`range_id`),
  KEY `from` (`from`,`to`),
  KEY `filter_id` (`filter_id`),
  KEY `feature_id` (`feature_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filter_ranges_translation`
--

CREATE TABLE IF NOT EXISTS `shop_product_filter_ranges_translation` (
  `range_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `range_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`range_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_global_option_links`
--

CREATE TABLE IF NOT EXISTS `shop_product_global_option_links` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_global_option_links`
--

INSERT INTO `shop_product_global_option_links` (`option_id`, `product_id`) VALUES
(2, 27),
(2, 28),
(2, 31),
(2, 34),
(2, 38),
(2, 41);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options`
--

CREATE TABLE IF NOT EXISTS `shop_product_options` (
  `option_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `option_type` char(1) NOT NULL DEFAULT 'S',
  `inventory` char(1) NOT NULL DEFAULT 'Y',
  `regexp` varchar(255) NOT NULL DEFAULT '',
  `required` char(1) NOT NULL DEFAULT 'N',
  `multiupload` char(1) NOT NULL DEFAULT 'N',
  `allowed_extensions` varchar(255) NOT NULL DEFAULT '',
  `max_file_size` int(11) NOT NULL DEFAULT '0',
  `missing_variants_handling` char(1) NOT NULL DEFAULT 'M',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`),
  KEY `c_status` (`product_id`,`status`),
  KEY `position` (`position`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `shop_product_options`
--

INSERT INTO `shop_product_options` (`option_id`, `product_id`, `company_id`, `option_type`, `inventory`, `regexp`, `required`, `multiupload`, `allowed_extensions`, `max_file_size`, `missing_variants_handling`, `status`, `position`, `value`) VALUES
(1, 0, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 10, ''),
(2, 0, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 20, ''),
(3, 12, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 20, ''),
(4, 12, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 0, ''),
(5, 11, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 20, ''),
(6, 11, 0, 'S', 'N', '', 'N', 'N', '', 0, 'M', 'A', 0, ''),
(7, 15, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 20, ''),
(8, 7, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 20, ''),
(9, 7, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 0, ''),
(10, 240, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 0, ''),
(11, 240, 0, 'C', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 0, ''),
(12, 241, 0, 'S', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 0, ''),
(13, 241, 0, 'C', 'Y', '', 'N', 'N', '', 0, 'M', 'A', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options_translation`
--

CREATE TABLE IF NOT EXISTS `shop_product_options_translation` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_text` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `comment` varchar(255) NOT NULL DEFAULT '',
  `inner_hint` varchar(255) NOT NULL DEFAULT '',
  `incorrect_message` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_options_translation`
--

INSERT INTO `shop_product_options_translation` (`option_id`, `lang_code`, `option_name`, `option_text`, `description`, `comment`, `inner_hint`, `incorrect_message`) VALUES
(1, 'EN', 'Color', 'Color', '', '', '', ''),
(2, 'EN', 'Size', 'Size', '', '', '', ''),
(3, 'EN', 'Size', 'Size', '', '', '', ''),
(4, 'EN', 'Color', '', '', '', '', ''),
(5, 'EN', 'Size', 'Size', '', '', '', ''),
(6, 'EN', 'Color', '', '', '', '', ''),
(7, 'EN', 'Size', 'Size', '', '', '', ''),
(8, 'EN', 'Size', 'Size', '', '', '', ''),
(9, 'EN', 'Color', '', '', '', '', ''),
(10, 'EN', 'Memory capacity', '', '', '', '', ''),
(11, 'EN', '3G Connectivity', '', '', '', '', ''),
(12, 'EN', 'Memory capacity', '', '', '', '', ''),
(13, 'EN', '3G Connectivity', '', '', '', '', ''),
(1, 'ru', 'Color', 'Color', '', '', '', ''),
(2, 'ru', 'Size', 'Size', '', '', '', ''),
(3, 'ru', 'Size', 'Size', '', '', '', ''),
(4, 'ru', 'Color', '', '', '', '', ''),
(5, 'ru', 'Size', 'Size', '', '', '', ''),
(6, 'ru', 'Color', '', '', '', '', ''),
(7, 'ru', 'Size', 'Size', '', '', '', ''),
(8, 'ru', 'Size', 'Size', '', '', '', ''),
(9, 'ru', 'Color', '', '', '', '', ''),
(10, 'ru', 'Memory capacity', '', '', '', '', ''),
(11, 'ru', '3G Connectivity', '', '', '', '', ''),
(12, 'ru', 'Memory capacity', '', '', '', '', ''),
(13, 'ru', '3G Connectivity', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options_exceptions`
--

CREATE TABLE IF NOT EXISTS `shop_product_options_exceptions` (
  `exception_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `combination` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`exception_id`),
  KEY `product` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options_inventory`
--

CREATE TABLE IF NOT EXISTS `shop_product_options_inventory` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_code` varchar(32) NOT NULL DEFAULT '',
  `combination_hash` int(11) unsigned NOT NULL DEFAULT '0',
  `combination` varchar(255) NOT NULL DEFAULT '',
  `amount` mediumint(8) NOT NULL DEFAULT '0',
  `temp` char(1) NOT NULL DEFAULT 'N',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`combination_hash`),
  KEY `pc` (`product_id`,`combination`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_options_inventory`
--

INSERT INTO `shop_product_options_inventory` (`product_id`, `product_code`, `combination_hash`, `combination`, `amount`, `temp`, `position`) VALUES
(12, '', 342472483, '4_17_3_14', 50, 'N', 2),
(12, '', 2310967194, '4_17_3_13', 50, 'N', 1),
(12, '', 822274303, '4_17_3_12', 50, 'N', 0),
(12, '', 2899693638, '4_17_3_15', 50, 'N', 3),
(12, '', 3193995176, '4_17_3_16', 50, 'N', 4),
(12, '', 2713580910, '4_18_3_12', 50, 'N', 5),
(12, '', 419533323, '4_18_3_13', 50, 'N', 6),
(12, '', 2228660914, '4_18_3_14', 50, 'N', 7),
(12, '', 1013632471, '4_18_3_15', 50, 'N', 8),
(12, '', 786391609, '4_18_3_16', 50, 'N', 9),
(12, '', 3602564600, '4_19_3_12', 50, 'N', 15),
(12, '', 1845928605, '4_19_3_13', 50, 'N', 16),
(12, '', 4090599972, '4_19_3_14', 50, 'N', 17),
(12, '', 1265499457, '4_19_3_15', 50, 'N', 18),
(12, '', 1507349167, '4_19_3_16', 50, 'N', 19),
(7, '', 1432285885, '9_36_8_33', 50, 'N', 2),
(7, '', 3991049688, '9_36_8_32', 50, 'N', 1),
(7, '', 4283906614, '9_36_8_31', 50, 'N', 0),
(7, '', 3364473348, '9_36_8_34', 50, 'N', 3),
(7, '', 1882565985, '9_36_8_35', 50, 'N', 4),
(7, '', 2286946976, '9_37_8_31', 50, 'N', 5),
(7, '', 2598741326, '9_37_8_32', 50, 'N', 6),
(7, '', 576307755, '9_37_8_33', 50, 'N', 7),
(7, '', 3213818514, '9_37_8_34', 50, 'N', 8),
(7, '', 120757751, '9_37_8_35', 50, 'N', 9),
(7, '', 418322225, '9_38_8_31', 50, 'N', 15),
(7, '', 173717727, '9_38_8_32', 50, 'N', 16),
(7, '', 3001475002, '9_38_8_33', 50, 'N', 17),
(7, '', 791799555, '9_38_8_34', 50, 'N', 18),
(7, '', 2542633062, '9_38_8_35', 50, 'N', 19);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_variants`
--

CREATE TABLE IF NOT EXISTS `shop_product_option_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `modifier` decimal(13,3) NOT NULL DEFAULT '0.000',
  `modifier_type` char(1) NOT NULL DEFAULT 'A',
  `weight_modifier` decimal(12,3) NOT NULL DEFAULT '0.000',
  `weight_modifier_type` char(1) NOT NULL DEFAULT 'A',
  `point_modifier` decimal(12,3) NOT NULL DEFAULT '0.000',
  `point_modifier_type` char(1) NOT NULL DEFAULT 'A',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`variant_id`),
  KEY `position` (`position`),
  KEY `status` (`status`),
  KEY `option_id` (`option_id`,`status`),
  KEY `option_id_2` (`option_id`,`variant_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Дамп данных таблицы `shop_product_option_variants`
--

INSERT INTO `shop_product_option_variants` (`variant_id`, `option_id`, `position`, `modifier`, `modifier_type`, `weight_modifier`, `weight_modifier_type`, `point_modifier`, `point_modifier_type`, `status`) VALUES
(1, 1, 10, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(6, 2, 10, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(5, 1, 20, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(10, 2, 20, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(4, 1, 30, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(9, 2, 30, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(3, 1, 40, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(8, 2, 40, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(2, 1, 50, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(7, 2, 50, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(11, 1, 60, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(12, 3, 10, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(13, 3, 20, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(14, 3, 30, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(15, 3, 40, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(16, 3, 50, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(17, 4, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(18, 4, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(19, 4, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(20, 5, 10, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(21, 5, 20, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(22, 5, 30, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(23, 5, 40, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(24, 5, 50, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(25, 6, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(26, 7, 10, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(27, 7, 20, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(28, 7, 30, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(29, 7, 40, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(30, 7, 50, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(31, 8, 10, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(32, 8, 20, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(33, 8, 30, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(34, 8, 40, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(35, 8, 50, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(36, 9, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(37, 9, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(38, 9, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(39, 10, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(40, 10, 0, 100.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(41, 10, 0, 200.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(42, 11, 1, 200.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(43, 11, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(44, 12, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(45, 12, 0, 100.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(46, 12, 0, 200.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(47, 13, 1, 200.000, 'A', 0.000, 'A', 0.000, 'A', 'A'),
(48, 13, 0, 0.000, 'A', 0.000, 'A', 0.000, 'A', 'A');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_variants_translation`
--

CREATE TABLE IF NOT EXISTS `shop_product_option_variants_translation` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `variant_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`variant_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_option_variants_translation`
--

INSERT INTO `shop_product_option_variants_translation` (`variant_id`, `lang_code`, `variant_name`) VALUES
(1, 'EN', 'White'),
(11, 'EN', 'Yellow'),
(2, 'EN', 'Green'),
(3, 'EN', 'Red'),
(4, 'EN', 'Blue'),
(5, 'EN', 'Black'),
(6, 'EN', 'Small'),
(7, 'EN', 'XX Large'),
(8, 'EN', 'X Large'),
(9, 'EN', 'Large'),
(10, 'EN', 'Medium'),
(12, 'EN', 'Small'),
(13, 'EN', 'Medium'),
(14, 'EN', 'Large'),
(15, 'EN', 'X Large'),
(16, 'EN', 'XX Large'),
(17, 'EN', 'Black/White/White'),
(18, 'EN', 'Dark Navy/White/White'),
(19, 'EN', 'White/Prime Green'),
(20, 'EN', 'Small'),
(21, 'EN', 'Medium'),
(22, 'EN', 'Large'),
(23, 'EN', 'X Large'),
(24, 'EN', 'XX Large'),
(25, 'EN', 'White'),
(26, 'EN', 'Small'),
(27, 'EN', 'Medium'),
(28, 'EN', 'Large'),
(29, 'EN', 'X Large'),
(30, 'EN', 'XX Large'),
(31, 'EN', 'Small'),
(32, 'EN', 'Medium'),
(33, 'EN', 'Large'),
(34, 'EN', 'X Large'),
(35, 'EN', 'XX Large'),
(36, 'EN', 'GreyHeather/Core Energy'),
(37, 'EN', 'Cardinal/PRIME YELLOW'),
(38, 'EN', 'Fairway/White'),
(39, 'EN', '16GB'),
(40, 'EN', '32GB'),
(41, 'EN', '64GB'),
(42, 'EN', ''),
(43, 'EN', ''),
(44, 'EN', '16GB'),
(45, 'EN', '32GB'),
(46, 'EN', '64GB'),
(47, 'EN', ''),
(48, 'EN', ''),
(1, 'ru', 'White'),
(11, 'ru', 'Yellow'),
(2, 'ru', 'Green'),
(3, 'ru', 'Red'),
(4, 'ru', 'Blue'),
(5, 'ru', 'Black'),
(6, 'ru', 'Small'),
(7, 'ru', 'XX Large'),
(8, 'ru', 'X Large'),
(9, 'ru', 'Large'),
(10, 'ru', 'Medium'),
(12, 'ru', 'Small'),
(13, 'ru', 'Medium'),
(14, 'ru', 'Large'),
(15, 'ru', 'X Large'),
(16, 'ru', 'XX Large'),
(17, 'ru', 'Black/White/White'),
(18, 'ru', 'Dark Navy/White/White'),
(19, 'ru', 'White/Prime Green'),
(20, 'ru', 'Small'),
(21, 'ru', 'Medium'),
(22, 'ru', 'Large'),
(23, 'ru', 'X Large'),
(24, 'ru', 'XX Large'),
(25, 'ru', 'White'),
(26, 'ru', 'Small'),
(27, 'ru', 'Medium'),
(28, 'ru', 'Large'),
(29, 'ru', 'X Large'),
(30, 'ru', 'XX Large'),
(31, 'ru', 'Small'),
(32, 'ru', 'Medium'),
(33, 'ru', 'Large'),
(34, 'ru', 'X Large'),
(35, 'ru', 'XX Large'),
(36, 'ru', 'GreyHeather/Core Energy'),
(37, 'ru', 'Cardinal/PRIME YELLOW'),
(38, 'ru', 'Fairway/White'),
(39, 'ru', '16GB'),
(40, 'ru', '32GB'),
(41, 'ru', '64GB'),
(42, 'ru', ''),
(43, 'ru', ''),
(44, 'ru', '16GB'),
(45, 'ru', '32GB'),
(46, 'ru', '64GB'),
(47, 'ru', ''),
(48, 'ru', '');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_popularity`
--

CREATE TABLE IF NOT EXISTS `shop_product_popularity` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `added` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `bought` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `total` (`product_id`,`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_popularity`
--

INSERT INTO `shop_product_popularity` (`product_id`, `viewed`, `added`, `deleted`, `bought`, `total`) VALUES
(166, 1, 1, 0, 1, 18),
(219, 1, 0, 0, 0, 3),
(12, 1, 0, 0, 0, 3),
(11, 1, 0, 0, 0, 3),
(7, 1, 0, 0, 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_prices`
--

CREATE TABLE IF NOT EXISTS `shop_product_prices` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `percentage_discount` int(2) unsigned NOT NULL DEFAULT '0',
  `lower_limit` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `usergroup` (`product_id`,`usergroup_id`,`lower_limit`),
  KEY `product_id` (`product_id`),
  KEY `lower_limit` (`lower_limit`),
  KEY `usergroup_id` (`usergroup_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_prices`
--

INSERT INTO `shop_product_prices` (`product_id`, `price`, `percentage_discount`, `lower_limit`, `usergroup_id`) VALUES
(1, 5399.99, 0, 1, 0),
(4, 699.99, 0, 1, 0),
(5, 899.99, 0, 1, 0),
(6, 329.99, 0, 1, 0),
(7, 55.00, 0, 1, 0),
(8, 849.99, 0, 1, 0),
(9, 999.99, 0, 1, 0),
(10, 1199.99, 0, 1, 0),
(11, 30.00, 0, 1, 0),
(12, 30.00, 0, 1, 0),
(13, 11.96, 0, 1, 0),
(14, 499.99, 0, 1, 0),
(15, 150.00, 0, 1, 0),
(16, 349.99, 0, 1, 0),
(17, 11.16, 0, 1, 0),
(18, 299.99, 0, 1, 0),
(19, 79.99, 0, 1, 0),
(20, 19.95, 0, 1, 0),
(21, 29.99, 0, 1, 0),
(22, 799.99, 0, 1, 0),
(23, 599.99, 0, 1, 0),
(24, 449.99, 0, 1, 0),
(25, 599.99, 0, 1, 0),
(26, 34.99, 0, 1, 0),
(27, 55.00, 0, 1, 0),
(28, 100.00, 0, 1, 0),
(29, 199.95, 0, 1, 0),
(30, 349.95, 0, 1, 0),
(31, 32.00, 0, 1, 0),
(32, 299.99, 0, 1, 0),
(33, 169.99, 0, 1, 0),
(34, 25.00, 0, 1, 0),
(35, 31.99, 0, 1, 0),
(36, 51.00, 0, 1, 0),
(37, 159.95, 0, 1, 0),
(38, 16.97, 0, 1, 0),
(39, 419.00, 0, 1, 0),
(40, 229.00, 0, 1, 0),
(41, 35.00, 0, 1, 0),
(42, 79.00, 0, 1, 0),
(43, 369.00, 0, 1, 0),
(44, 25.00, 0, 1, 0),
(45, 74.00, 0, 1, 0),
(46, 21.00, 0, 1, 0),
(47, 29.99, 0, 1, 0),
(48, 180.00, 0, 1, 0),
(49, 120.00, 0, 1, 0),
(50, 220.00, 0, 1, 0),
(51, 180.00, 0, 1, 0),
(52, 139.99, 0, 1, 0),
(53, 38.99, 0, 1, 0),
(54, 269.00, 0, 1, 0),
(55, 359.00, 0, 1, 0),
(56, 220.00, 0, 1, 0),
(57, 309.00, 0, 1, 0),
(58, 779.00, 0, 1, 0),
(59, 599.00, 0, 1, 0),
(60, 1499.00, 0, 1, 0),
(62, 209.95, 0, 1, 0),
(63, 189.95, 0, 1, 0),
(64, 189.95, 0, 1, 0),
(65, 99.95, 0, 1, 0),
(66, 389.95, 0, 1, 0),
(67, 339.99, 0, 1, 0),
(68, 799.99, 0, 1, 0),
(69, 529.99, 0, 1, 0),
(70, 499.99, 0, 1, 0),
(71, 529.99, 0, 1, 0),
(72, 599.99, 0, 1, 0),
(73, 589.99, 0, 1, 0),
(74, 524.99, 0, 1, 0),
(75, 489.99, 0, 1, 0),
(76, 439.99, 0, 1, 0),
(117, 729.99, 0, 1, 0),
(78, 100.00, 0, 1, 0),
(79, 96.00, 0, 1, 0),
(80, 55.00, 0, 1, 0),
(81, 49.50, 0, 1, 0),
(82, 19.99, 0, 1, 0),
(83, 19.99, 0, 1, 0),
(84, 19.99, 0, 1, 0),
(85, 19.99, 0, 1, 0),
(86, 359.00, 0, 1, 0),
(87, 19.99, 0, 1, 0),
(88, 39.99, 0, 1, 0),
(89, 19.99, 0, 1, 0),
(90, 19.99, 0, 1, 0),
(91, 10700.00, 0, 1, 0),
(92, 3225.00, 0, 1, 0),
(93, 19.99, 0, 1, 0),
(94, 59.99, 0, 1, 0),
(95, 19.99, 0, 1, 0),
(96, 99.99, 0, 1, 0),
(97, 14.99, 0, 1, 0),
(112, 8.99, 0, 1, 0),
(113, 449.99, 0, 1, 0),
(100, 22.70, 0, 1, 0),
(101, 188.88, 0, 1, 0),
(102, 295.00, 0, 1, 0),
(103, 23.99, 0, 1, 0),
(104, 29.95, 0, 1, 0),
(105, 169.99, 0, 1, 0),
(106, 179.99, 0, 1, 0),
(107, 465.00, 0, 1, 0),
(108, 12.99, 0, 1, 0),
(109, 140.00, 0, 1, 0),
(110, 15.99, 0, 1, 0),
(111, 6.99, 0, 1, 0),
(114, 14.99, 0, 1, 0),
(115, 4595.00, 0, 1, 0),
(116, 6.99, 0, 1, 0),
(118, 30.99, 0, 1, 0),
(119, 17.99, 0, 1, 0),
(120, 199.99, 0, 1, 0),
(121, 17.99, 0, 1, 0),
(122, 5695.00, 0, 1, 0),
(123, 17.99, 0, 1, 0),
(124, 4595.00, 0, 1, 0),
(125, 149.99, 0, 1, 0),
(126, 129.99, 0, 1, 0),
(127, 4779.00, 0, 1, 0),
(128, 17.99, 0, 1, 0),
(129, 1799.00, 0, 1, 0),
(130, 49.95, 0, 1, 0),
(131, 1249.00, 0, 1, 0),
(132, 269.99, 0, 1, 0),
(133, 229.99, 0, 1, 0),
(134, 89.99, 0, 1, 0),
(135, 0.00, 0, 1, 0),
(136, 0.00, 0, 1, 0),
(137, 0.00, 0, 1, 0),
(138, 0.00, 0, 1, 0),
(139, 0.00, 0, 1, 0),
(140, 99.95, 0, 1, 0),
(141, 99.95, 0, 1, 0),
(142, 399.95, 0, 1, 0),
(143, 79.95, 0, 1, 0),
(144, 99.95, 0, 1, 0),
(145, 79.99, 0, 1, 0),
(146, 44.99, 0, 1, 0),
(147, 44.99, 0, 1, 0),
(148, 219.00, 0, 1, 0),
(149, 89.99, 0, 1, 0),
(150, 600.00, 0, 1, 0),
(151, 500.00, 0, 1, 0),
(152, 700.00, 0, 1, 0),
(153, 49.99, 0, 1, 0),
(154, 399.99, 0, 1, 0),
(155, 79.99, 0, 1, 0),
(156, 299.00, 0, 1, 0),
(157, 499.00, 0, 1, 0),
(158, 4750.00, 0, 1, 0),
(159, 11375.00, 0, 1, 0),
(160, 200.00, 0, 1, 0),
(161, 279.99, 0, 1, 0),
(162, 32750.00, 0, 1, 0),
(163, 899.99, 0, 1, 0),
(164, 249.99, 0, 1, 0),
(165, 599.95, 0, 1, 0),
(166, 749.95, 0, 1, 0),
(167, 599.95, 0, 1, 0),
(168, 1.00, 0, 1, 0),
(169, 749.95, 0, 1, 0),
(170, 145.99, 0, 1, 0),
(171, 499.00, 0, 1, 0),
(172, 299.99, 0, 1, 0),
(173, 349.99, 0, 1, 0),
(174, 100.75, 0, 1, 0),
(175, 179.99, 0, 1, 0),
(176, 648.95, 0, 1, 0),
(177, 400.00, 0, 1, 0),
(178, 6.83, 0, 1, 0),
(179, 299.97, 0, 1, 0),
(180, 199.99, 0, 1, 0),
(181, 899.99, 0, 1, 0),
(182, 6.80, 0, 1, 0),
(183, 249.99, 0, 1, 0),
(184, 299.99, 0, 1, 0),
(185, 139.99, 0, 1, 0),
(186, 299.99, 0, 1, 0),
(187, 299.99, 0, 1, 0),
(188, 10.00, 0, 1, 0),
(189, 1.00, 0, 1, 0),
(190, 899.95, 0, 1, 0),
(191, 11.98, 0, 1, 0),
(192, 15.00, 0, 1, 0),
(205, 149.99, 0, 1, 0),
(194, 10.60, 0, 1, 0),
(195, 29.99, 0, 1, 0),
(196, 17.00, 0, 1, 0),
(197, 14.98, 0, 1, 0),
(198, 17.99, 0, 1, 0),
(199, 29.98, 0, 1, 0),
(200, 26.92, 0, 1, 0),
(201, 12.67, 0, 1, 0),
(202, 34.68, 0, 1, 0),
(203, 34.68, 0, 1, 0),
(204, 14.99, 0, 1, 0),
(206, 179.99, 0, 1, 0),
(207, 42.00, 0, 1, 0),
(208, 82.94, 0, 1, 0),
(209, 109.99, 0, 1, 0),
(210, 89.99, 0, 1, 0),
(211, 299.99, 0, 1, 0),
(212, 129.95, 0, 1, 0),
(213, 295.00, 0, 1, 0),
(214, 974.00, 0, 1, 0),
(215, 1095.00, 0, 1, 0),
(227, 699.00, 0, 1, 0),
(217, 616.99, 0, 1, 0),
(218, 459.99, 0, 1, 0),
(219, 529.99, 0, 1, 0),
(220, 1099.99, 0, 1, 0),
(221, 2049.00, 0, 1, 0),
(222, 529.99, 0, 1, 0),
(223, 499.99, 0, 1, 0),
(224, 479.99, 0, 1, 0),
(225, 199.99, 0, 1, 0),
(226, 269.99, 0, 1, 0),
(228, 349.99, 0, 1, 0),
(229, 299.99, 0, 1, 0),
(230, 125.00, 0, 1, 0),
(231, 99.00, 0, 1, 0),
(232, 79.95, 0, 1, 0),
(233, 47.99, 0, 1, 0),
(234, 59.99, 0, 1, 0),
(235, 79.99, 0, 1, 0),
(236, 299.99, 0, 1, 0),
(237, 299.99, 0, 1, 0),
(238, 552.00, 0, 1, 0),
(239, 552.00, 0, 1, 0),
(240, 499.00, 0, 1, 0),
(241, 499.00, 0, 1, 0),
(242, 249.00, 0, 1, 0),
(243, 249.00, 0, 1, 0);
