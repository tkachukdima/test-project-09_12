-- phpMyAdmin SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 19 2012 г., 20:03
-- Версия сервера: 5.1.61
-- Версия PHP: 5.3.10-1ubuntu0ppa1~oneiric

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `art_cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_type_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `banner_translation`
--

DROP TABLE IF EXISTS `banner_translation`;
CREATE TABLE IF NOT EXISTS `banner_translation` (
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`banner_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `banner_type`
--

DROP TABLE IF EXISTS `banner_type`;
CREATE TABLE IF NOT EXISTS `banner_type` (
  `banner_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_who` int(11) NOT NULL,
  `updated_who` int(11) NOT NULL,
  PRIMARY KEY (`banner_type_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `banner_type`
--

INSERT INTO `banner_type` (`banner_type_id`, `ident`, `name`, `description`, `sort_order`, `status`, `created_at`, `updated_at`, `created_who`, `updated_who`) VALUES
(1, 'TOP', 'Верхний баннер', 'Размеры баннера должны быть 1000 × 96 px', 1, 1, '2011-11-30 22:00:00', '2012-01-17 13:27:04', 4, 1),
(2, 'LEFT', 'Левый баннер', 'Размеры баннеров должны быть 200 × 285 px', 2, 1, '2011-11-30 22:07:51', '2012-01-17 13:29:21', 4, 1),
(3, 'RIGHT', 'Правый баннер', 'Размеры баннеров должны быть 200 × 285 px', 3, 1, '2011-11-30 22:07:51', '2012-01-17 13:29:30', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes`
--

DROP TABLE IF EXISTS `catalog_attributes`;
CREATE TABLE IF NOT EXISTS `catalog_attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(64) NOT NULL,
  `mandatory` smallint(6) NOT NULL,
  `enum_value` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(32) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `catalog_attributes`
--

INSERT INTO `catalog_attributes` (`attribute_id`, `ident`, `mandatory`, `enum_value`, `multiple`, `type`, `sort_order`) VALUES
(1, 'weight', 1, 0, 0, 'string', 1),
(2, 'diagonal', 1, 0, 0, 'text', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes_translation`
--

DROP TABLE IF EXISTS `catalog_attributes_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attributes_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `catalog_attributes_translation`
--

INSERT INTO `catalog_attributes_translation` (`attribute_translation_id`, `attribute_id`, `language_id`, `name`, `display_name`, `measure`) VALUES
(1, 1, 1, 'вес', 'Вес', 'кг'),
(2, 2, 1, 'диагональ', 'Диагональ', '"');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_enum` (
  `enum_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `value` text,
  `description` text,
  `value_id` int(11) NOT NULL,
  PRIMARY KEY (`enum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value`
--

DROP TABLE IF EXISTS `catalog_attribute_value`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value` (
  `attribute_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`attribute_value_id`),
  UNIQUE KEY `attribute_id` (`attribute_id`,`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `catalog_attribute_value`
--

INSERT INTO `catalog_attribute_value` (`attribute_value_id`, `attribute_id`, `product_id`, `flag`, `sort_order`) VALUES
(4, 2, 1, 0, 0),
(3, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_value_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_enum` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`attribute_id`,`attribute_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_value_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_translation` (
  `attribute_value_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `measure` varchar(500) NOT NULL,
  PRIMARY KEY (`attribute_value_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `catalog_attribute_value_translation`
--

INSERT INTO `catalog_attribute_value_translation` (`attribute_value_translation_id`, `attribute_value_id`, `language_id`, `value`, `measure`) VALUES
(3, 3, 1, '1112', 'кг'),
(4, 4, 1, '2222', '"');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category`
--

DROP TABLE IF EXISTS `catalog_category`;
CREATE TABLE IF NOT EXISTS `catalog_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ident` varchar(200) NOT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_category`
--

INSERT INTO `catalog_category` (`category_id`, `name`, `parent_id`, `ident`, `thumbnail`, `preview`, `full`, `sort_order`, `lastmod`) VALUES
(1, '', 0, 'Test-kategory-catalog', NULL, NULL, NULL, 10, '2012-04-06 07:05:38');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_attribute`
--

DROP TABLE IF EXISTS `catalog_category_attribute`;
CREATE TABLE IF NOT EXISTS `catalog_category_attribute` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_category_attribute`
--

INSERT INTO `catalog_category_attribute` (`attribute_id`, `category_id`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_translation`
--

DROP TABLE IF EXISTS `catalog_category_translation`;
CREATE TABLE IF NOT EXISTS `catalog_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_category_translation`
--

INSERT INTO `catalog_category_translation` (`category_translation_id`, `category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 1, 'Test kategory catalog', '<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; ">\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam viverra mi id lacus vehicula fermentum. Sed convallis, arcu sit amet adipiscing vulputate, augue ante tincidunt nunc, vitae dapibus tellus nunc a tortor. Fusce sed mollis eros. Vivamus bibendum, odio sed ullamcorper fringilla, sem nunc hendrerit elit, dapibus dapibus diam nunc eu mauris. Duis feugiat eleifend elit, sed dictum augue varius sollicitudin. Duis id accumsan odio. Nunc enim nisl, accumsan a elementum vel, fermentum mollis quam. Sed laoreet neque ac massa bibendum hendrerit. Nullam sodales adipiscing mauris a consequat. Donec eu arcu eget turpis tempor mattis adipiscing at massa. Curabitur facilisis lacus viverra mi suscipit sed facilisis ligula iaculis. Praesent ut arcu ligula, rhoncus semper est. Aenean non libero mauris.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; ">\r\n	Nulla facilisis massa sed erat tempus suscipit sed vitae purus. Aliquam purus est, sollicitudin ac mollis sed, consequat vitae lacus. Quisque et quam non nisl varius hendrerit quis id dolor. Aenean eu diam at arcu iaculis vehicula. Integer justo neque, iaculis non egestas et, hendrerit a eros. Pellentesque vitae erat dui, quis euismod erat. Quisque in tortor nibh, a hendrerit augue. Phasellus adipiscing molestie cursus. Sed feugiat nunc sed ipsum tempus mattis. Maecenas fermentum tincidunt commodo. Quisque purus urna, feugiat sit amet aliquet eget, tristique sit amet nunc. Vivamus eu urna vel nunc auctor euismod cursus ac arcu.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; ">\r\n	Morbi posuere, quam eu pharetra posuere, felis mauris pretium nisi, ut varius nulla nibh a velit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas fringilla ultrices eros, et egestas massa faucibus vel. Integer sagittis eros ut elit vulputate interdum. Sed elementum nunc ultricies lectus fermentum at hendrerit turpis aliquet. Aenean sit amet tristique mauris. Pellentesque cursus hendrerit egestas. Donec ullamcorper, massa eu vehicula tristique, metus ipsum scelerisque dui, consequat malesuada odio lectus ut eros. Integer libero turpis, dapibus eget molestie a, venenatis quis justo. Aenean gravida posuere tellus non placerat. Integer rutrum nisl sed elit rhoncus volutpat. Fusce nisi risus, viverra ac egestas id, placerat eget metus. Ut rhoncus, nisl eget luctus bibendum, urna nunc mattis nisi, et porttitor quam ante sed nisi.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; ">\r\n	Nunc ornare, nisl at imperdiet bibendum, ante enim dictum quam, vitae rhoncus nisi dui vitae lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi turpis tortor, imperdiet id laoreet eu, venenatis ut dolor. Ut posuere lacus id nunc mollis molestie. Praesent gravida viverra ligula a aliquam. Maecenas vulputate, quam non vulputate semper, massa lectus convallis erat, et ultricies mauris orci at velit. Ut dignissim magna nec lorem hendrerit a faucibus nisi sodales. Pellentesque consequat luctus scelerisque. Sed cursus ipsum et lectus vestibulum id lobortis eros consectetur. Quisque lobortis commodo diam a ultricies. Nulla quis arcu nibh, id imperdiet tellus. Donec ipsum mauris, lacinia quis tristique vel, suscipit eu arcu. Phasellus et leo non nulla adipiscing aliquam sit amet eget lectus.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; ">\r\n	Sed adipiscing, est tincidunt feugiat mattis, ligula magna cursus nisi, blandit hendrerit urna augue quis justo. In elit magna, dapibus ut placerat et, facilisis non augue. Cras ullamcorper lobortis diam, sed vulputate nibh faucibus in. Vestibulum sodales turpis sollicitudin felis varius vitae mollis sapien molestie. Duis eget nisl lacus. Phasellus sodales laoreet urna, vel facilisis est pharetra non. Fusce ipsum odio, fermentum vel placerat aliquam, cursus vitae nibh. Integer urna arcu, gravida a viverra a, consequat a magna.</p>', 'Sed adipiscing, est tincidunt feugiat mattis', 'Sed adipiscing, est tincidunt feugiat mattis', 'Sed adipiscing, est tincidunt feugiat mattis', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product`
--

DROP TABLE IF EXISTS `catalog_product`;
CREATE TABLE IF NOT EXISTS `catalog_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `article` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `special_offers` tinyint(1) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_product`
--

INSERT INTO `catalog_product` (`product_id`, `category_id`, `ident`, `name`, `article`, `quantity`, `special_offers`, `lastmod`, `viewed`, `sort_order`) VALUES
(1, 1, 'Test-produkt', '', '', 0, 1, '2012-04-10 07:10:30', 0, 11);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image`
--

DROP TABLE IF EXISTS `catalog_product_image`;
CREATE TABLE IF NOT EXISTS `catalog_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_product_image`
--

INSERT INTO `catalog_product_image` (`image_id`, `product_id`, `thumbnail`, `preview`, `detail`, `full`, `type`, `changed`) VALUES
(1, 1, 'Test-produkt-1-1-100x100.jpg', 'Test-produkt-1-1-189x189.jpg', 'Test-produkt-1-1-250x250.jpg', 'Test-produkt-1-1-900x700.jpg', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image_translation`
--

DROP TABLE IF EXISTS `catalog_product_image_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_product_image_translation`
--

INSERT INTO `catalog_product_image_translation` (`image_id`, `language_id`, `title`) VALUES
(1, 1, 'test image');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property`
--

DROP TABLE IF EXISTS `catalog_product_property`;
CREATE TABLE IF NOT EXISTS `catalog_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_product_property`
--

INSERT INTO `catalog_product_property` (`property_id`, `name`, `ident`, `sort_order`) VALUES
(1, 'radius', 'rd-us', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property_value`
--

DROP TABLE IF EXISTS `catalog_product_property_value`;
CREATE TABLE IF NOT EXISTS `catalog_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_translation`
--

DROP TABLE IF EXISTS `catalog_product_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_product_translation`
--

INSERT INTO `catalog_product_translation` (`product_translation_id`, `product_id`, `language_id`, `name`, `description`, `meta_keywords`, `meta_title`, `meta_description`) VALUES
(1, 1, 1, 'Test produkt ss', '<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; ">\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis interdum nisl iaculis neque interdum at volutpat est euismod. Nam convallis tellus vel nisl ornare sit amet vestibulum nunc convallis. Maecenas hendrerit dignissim vehicula. Donec ultricies sagittis magna, eu porta eros pulvinar eget. Morbi id ipsum eget nunc porttitor consequat. Nulla sit amet hendrerit urna. Praesent venenatis gravida elit, non pharetra tortor tempor non. Nam malesuada sem ut nisl dignissim tempor. Donec tempor lectus nec lectus aliquam suscipit. Duis tortor turpis, consequat at placerat eu, tristique ac leo. Cras diam urna, varius vel rutrum sed, facilisis nec metus. Nam molestie commodo tellus ullamcorper lobortis.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; ">\r\n	Phasellus vel turpis consectetur augue gravida varius nec ut tellus. Sed risus enim, adipiscing auctor fringilla at, malesuada ac diam. Phasellus pharetra fermentum pellentesque. Maecenas cursus urna et enim tincidunt pharetra. In hac habitasse platea dictumst. Aliquam mattis arcu sed velit fermentum non volutpat orci lacinia. Duis nec nibh arcu, aliquam tincidunt nisi. Nullam mi sapien, luctus eget scelerisque id, egestas sed orci. Donec augue tellus, egestas id cursus a, imperdiet quis nisi. Morbi nibh nunc, sagittis ac congue sit amet, laoreet at quam. Donec nisl dui, imperdiet accumsan porttitor non, vestibulum posuere est. Duis in diam in mauris convallis dignissim sed eget orci. Cras mattis interdum arcu sit amet elementum. Donec sem nibh, convallis adipiscing adipiscing ac, rhoncus a nulla. Sed pulvinar lobortis ornare. Cras ut varius nunc.</p>', 'Phasellus vel turpis', 'Phasellus vel turpis', 'Phasellus vel turpis');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT '0',
  `name` varchar(228) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `body` text,
  `status` int(1) DEFAULT '0',
  `date_post` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting`
--

DROP TABLE IF EXISTS `contact_setting`;
CREATE TABLE IF NOT EXISTS `contact_setting` (
  `contact_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_code` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `fax_code` varchar(255) NOT NULL,
  `fax_number` varchar(255) NOT NULL,
  `public_email` varchar(255) NOT NULL,
  `map_image_full` varchar(255) DEFAULT NULL,
  `map_image_preview` varchar(255) DEFAULT NULL,
  `map_image_thumbnail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `contact_setting`
--

INSERT INTO `contact_setting` (`contact_setting_id`, `phone_code`, `phone_number`, `fax_code`, `fax_number`, `public_email`, `map_image_full`, `map_image_preview`, `map_image_thumbnail`) VALUES
(1, '012', '123 45 67', '0123', '123 456', 'info@site.com', 'map_image-900x700.jpg', 'map_image-705x300.jpg', 'map_image-140x60.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting_translation`
--

DROP TABLE IF EXISTS `contact_setting_translation`;
CREATE TABLE IF NOT EXISTS `contact_setting_translation` (
  `contact_setting_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `work_time` varchar(255) NOT NULL,
  `weekend` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `map_address` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`contact_setting_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact_setting_translation`
--

INSERT INTO `contact_setting_translation` (`contact_setting_id`, `language_id`, `city`, `address`, `work_time`, `weekend`, `title`, `body`, `map_address`, `meta_description`, `meta_keywords`) VALUES
(1, 1, 'Страна, г. Город', 'ул. Улица, 1А', 'с 8:30 до 18:00', 'Понедельник', 'Контакты', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: arial; font-size: 13px; ">Aliquam enim turpis egestas, elit ridiculus tincidunt lectus etiam tortor duis amet. Etiam habitasse odio phasellus natoque amet, mattis phasellus. Phasellus, nisi cum eros, ridiculus hac, purus placerat ut sed tempor non, velit, porttitor eu massa. Magnis auctor placerat porttitor risus tristique magna scelerisque, mus! Sed dapibus lacus, integer non eros. </span></p>\r\n<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: arial; font-size: 13px; ">Elementum facilisis turpis? Habitasse turpis vut in odio adipiscing? Amet rhoncus rhoncus, porta! Ultrices integer mid lectus elementum tincidunt ut ac! Porta in. Sociis sed. Pulvinar. Dolor hac magna! Enim nisi sed lorem, rhoncus integer ultricies nunc augue scelerisque! Ultricies. Mid hac etiam sed a ridiculus tincidunt tristique sed in lorem quis mauris? Parturient, cursus placerat scelerisque, ridiculus sit tristique, et, a lacus nisi pulvinar. Ac pulvinar mid habitasse.</span></p>\r\n<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; border-image: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(0, 0, 0); font-family: arial; ">\r\n	&nbsp;</p>', '<p>\r\n	Kарта проезда</p>\r\n<p>\r\n	Страна, г. Город</p>\r\n<p>\r\n	ул. Улица, 1А</p>', 'Контакты, карта проезда', 'Контакты, карта проезда');

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(10) unsigned NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `query` varchar(1024) NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`faq_id`, `category_faq_id`, `author`, `email`, `query`, `answer`, `created_at`, `status`, `sort_order`) VALUES
(1, 2, '', '', 'Как добавить пункт в Главне меню?', '<p>\r\n	Заходим в раздел &quot;Меню&quot;&nbsp; &gt; &quot;Добавить Пункт меню&quot; &gt; вводим название пункта,&nbsp; выбираем&nbsp; меню, страницу, вводим значение сортировки, ставим статус &quot;Активный&quot;</p>', '2011-07-27 07:12:14', 0, 1),
(2, 2, '', '', 'Как удалить пункт меню?', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<img alt="" src="/upload/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D0%9C%D0%B5%D0%BD%D1%8E%20-%20host_pb%20-%20Mozilla%20Firefox.png" style="width: 686px; height: 414px;" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2011-07-27 08:06:36', 0, 2),
(7, 1, 'Гуцалюк Владимир', 'vovasgm@gmail.com', 'Как работает система поиска?', '<p>\r\n	Очень просто. Водите слова, и ищите информацию по сайту...</p>', '2011-11-30 13:37:33', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

DROP TABLE IF EXISTS `faq_category`;
CREATE TABLE IF NOT EXISTS `faq_category` (
  `category_faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_faq_id`),
  UNIQUE KEY `ident` (`ident`),
  UNIQUE KEY `ident_2` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category_translation`
--

DROP TABLE IF EXISTS `faq_category_translation`;
CREATE TABLE IF NOT EXISTS `faq_category_translation` (
  `category_faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_translation`
--

DROP TABLE IF EXISTS `faq_translation`;
CREATE TABLE IF NOT EXISTS `faq_translation` (
  `faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `file` varchar(1024) NOT NULL,
  `preview` varchar(1024) NOT NULL,
  `full` varchar(1024) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file_group`
--

DROP TABLE IF EXISTS `file_group`;
CREATE TABLE IF NOT EXISTS `file_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('file','image') NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file_group_translation`
--

DROP TABLE IF EXISTS `file_group_translation`;
CREATE TABLE IF NOT EXISTS `file_group_translation` (
  `group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `file_translation`
--

DROP TABLE IF EXISTS `file_translation`;
CREATE TABLE IF NOT EXISTS `file_translation` (
  `file_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album`
--

DROP TABLE IF EXISTS `gallery_album`;
CREATE TABLE IF NOT EXISTS `gallery_album` (
  `album_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`album_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `gallery_album`
--

INSERT INTO `gallery_album` (`album_id`, `parent_id`, `thumbnail`, `preview`, `full`, `original`, `ident`, `status`, `sort_order`, `lastmod`) VALUES
(1, 0, 'album-1-158x158.jpg', 'album-1-200x200.jpg', 'album-1-900x700.jpg', '', 'fotoalbom-1', 1, 1, '2012-03-23 12:46:34'),
(2, 0, 'album-2-158x158.png', 'album-2-200x200.png', 'album-2-900x700.png', '', 'test', 1, 2, '2012-03-28 09:30:39'),
(3, 0, '', '', '', '', 'test88', 1, 3, '2012-03-28 13:17:19'),
(4, 1, 'album-4-158x158.jpg', 'album-4-200x200.jpg', 'album-4-900x700.jpg', 'album-4.jpg', 'sub-album-1', 1, 1, '2012-03-30 08:33:55');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album_translation`
--

DROP TABLE IF EXISTS `gallery_album_translation`;
CREATE TABLE IF NOT EXISTS `gallery_album_translation` (
  `album_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`album_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_album_translation`
--

INSERT INTO `gallery_album_translation` (`album_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 'Фотоальбом 1', 'описание Фотоальбома 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1'),
(2, 1, 'test', 'test', 'test', 'test', 'test', 'test'),
(3, 1, 'test', 'test', 'test', 'test', 'test', 'test'),
(4, 1, 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo`
--

DROP TABLE IF EXISTS `gallery_photo`;
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `photo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `gallery_photo`
--

INSERT INTO `gallery_photo` (`photo_id`, `album_id`, `thumbnail`, `preview`, `full`, `original`, `status`, `sort_order`) VALUES
(19, 1, 'photo-19-158x158.jpg', 'photo-19-200x200.jpg', 'photo-19-900x700.jpg', 'photo-19.jpg', 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo_translation`
--

DROP TABLE IF EXISTS `gallery_photo_translation`;
CREATE TABLE IF NOT EXISTS `gallery_photo_translation` (
  `photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`photo_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_photo_translation`
--

INSERT INTO `gallery_photo_translation` (`photo_id`, `language_id`, `name`, `description_img`) VALUES
(19, 1, 'test', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `guestbook`
--

DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `message` varchar(2048) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `guestbook`
--

INSERT INTO `guestbook` (`id`, `user_id`, `name`, `email`, `website`, `message`, `date`) VALUES
(2, 0, 'dsfgsdg', 'amey@i.ua', '', 'dsfg', '2012-02-16 12:29:44');

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `ident` varchar(128) NOT NULL,
  `code` varchar(2) NOT NULL,
  `locale` varchar(8) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `ident` (`ident`,`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`language_id`, `name`, `ident`, `code`, `locale`, `sort_order`, `status`) VALUES
(1, 'Русский', 'russian', 'ru', 'ru_RU', 0, 1),
(2, 'Українська', 'ukrainian', 'uk', 'uk_UA', 2, 0),
(3, 'English', 'english', 'en', 'en_UK', 3, 0),
(4, 'Norsk', 'norsk', 'nn', 'nn_NO', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`menu_id`, `name`, `type`, `status`, `sort_order`) VALUES
(1, 'Main menu', 'main', 1, 1),
(2, 'secondary', 'secondary', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE IF NOT EXISTS `menu_item` (
  `menu_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `dropdown` tinyint(1) NOT NULL DEFAULT '0',
  `uri` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `menu_item`
--

INSERT INTO `menu_item` (`menu_item_id`, `menu_id`, `parent_id`, `dropdown`, `uri`, `image`, `status`, `sort_order`) VALUES
(1, 1, 5, 0, '/', '', 1, 1),
(2, 2, 0, 0, '/', '', 1, 1),
(3, 1, 0, 0, '/contact', '', 1, 10),
(4, 2, 0, 0, '/', '', 1, 2),
(5, 1, 0, 0, '/page/index/pageIdent/testovaya-stranitsa', '', 1, 3),
(6, 1, 0, 0, '/publication/index/groupIdent/news', '', 1, 3),
(7, 1, 0, 0, '/gallery', '', 1, 6),
(8, 1, 0, 0, '/catalog', '', 1, 20);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n`
--

DROP TABLE IF EXISTS `menu_item_n`;
CREATE TABLE IF NOT EXISTS `menu_item_n` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('uri','mvc') DEFAULT 'uri',
  `params` text,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `target` enum('','_blank','_parent','_self','_top') DEFAULT '',
  `status` tinyint(1) DEFAULT '0',
  `routeType` varchar(40) DEFAULT NULL,
  `module` varchar(40) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `action` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `menu_item_n`
--

INSERT INTO `menu_item_n` (`item_id`, `menu_id`, `parent_id`, `type`, `params`, `sort_order`, `route`, `uri`, `class`, `target`, `status`, `routeType`, `module`, `controller`, `action`) VALUES
(3, 1, 0, 'uri', NULL, 1, 'default', '/', '', '', 1, NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n_translation`
--

DROP TABLE IF EXISTS `menu_item_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_n_translation` (
  `item_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_n_translation`
--

INSERT INTO `menu_item_n_translation` (`item_id`, `language_id`, `title`, `label`) VALUES
(3, 1, 'Главное меню', 'Главная страница сайта');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_translation`
--

DROP TABLE IF EXISTS `menu_item_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_translation` (
  `menu_item_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_translation`
--

INSERT INTO `menu_item_translation` (`menu_item_id`, `language_id`, `name`, `description_img`) VALUES
(1, 1, 'Главная', NULL),
(1, 3, 'Main', NULL),
(2, 1, 'Item 1', NULL),
(3, 1, 'Контакты', NULL),
(4, 1, 'Seo text 2', NULL),
(5, 1, 'Тестовая страница', NULL),
(6, 1, 'Новости', NULL),
(7, 1, 'Фотогалерея', NULL),
(8, 1, 'Каталог', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n`
--

DROP TABLE IF EXISTS `menu_n`;
CREATE TABLE IF NOT EXISTS `menu_n` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `menu_n`
--

INSERT INTO `menu_n` (`menu_id`, `type`, `status`, `sort_order`) VALUES
(1, 'main', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n_translation`
--

DROP TABLE IF EXISTS `menu_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_n_translation` (
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_n_translation`
--

INSERT INTO `menu_n_translation` (`menu_id`, `language_id`, `name`) VALUES
(1, 1, 'Главное меню');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

DROP TABLE IF EXISTS `menu_translation`;
CREATE TABLE IF NOT EXISTS `menu_translation` (
  `menu_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `date_post` datetime NOT NULL,
  `type` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`page_id`, `ident`, `thumbnail`, `preview`, `full`, `date_post`, `type`, `status`, `lastmod`) VALUES
(1, 'testovaya-stranitsa', NULL, NULL, 'wallpaper-113436.jpg', '2012-02-17 17:28:45', 'static', 1, '2012-03-19 16:43:19'),
(2, 'opisanie-fotogalerei', NULL, NULL, NULL, '2012-03-23 14:50:29', 'gallery', 1, '2012-03-23 12:51:57');

-- --------------------------------------------------------

--
-- Структура таблицы `page_translation`
--

DROP TABLE IF EXISTS `page_translation`;
CREATE TABLE IF NOT EXISTS `page_translation` (
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`page_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page_translation`
--

INSERT INTO `page_translation` (`page_id`, `language_id`, `title`, `body`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 'Тестовая страница', '<p>\r\n	Rhoncus egestas etiam integer nascetur? Elit eros arcu integer, ultricies non ac, porttitor in? Duis adipiscing adipiscing elit! Porta scelerisque, nisi porttitor dictumst phasellus, magna rhoncus platea ultrices tincidunt mid mid egestas et nisi, porttitor, ut, purus aliquet porttitor dis massa sit. Dolor porttitor enim arcu? Cursus parturient hac aenean arcu, integer turpis porttitor! Parturient! Vel adipiscing, etiam magnis urna placerat aliquet cursus ut. Magna, nec nec urna risus sit dignissim proin? Rhoncus aliquam ac integer nisi in porttitor duis turpis nec. Rhoncus rhoncus, et ut magna, mid, in integer, aliquet in, magna lacus a magna nisi ut enim turpis, nec montes amet vel cum! Porta ut turpis!</p>\r\n<p>\r\n	Cras aliquam scelerisque ac purus sed a ridiculus integer sed aliquam odio. In porta elit tortor mid hac sit pulvinar pid phasellus magna? Et dictumst. Diam? Tortor, sagittis eros habitasse in, sagittis elementum tincidunt! Facilisis. Nisi, a ut turpis ultrices cursus scelerisque lundium! Facilisis magna ut lundium, risus massa hac lacus! Augue rhoncus! Massa integer et pid auctor a, nascetur magna, diam magna pulvinar enim, tristique rhoncus? Rhoncus, rhoncus! Enim nunc ultrices porttitor ultricies natoque sed vel, elit dignissim et? Dignissim magnis urna, enim ridiculus cras, tincidunt! Dictumst cum vel natoque, mus ac tincidunt augue augue! Turpis sed, odio, cum a sit rhoncus amet turpis arcu porttitor. Sociis ultrices ultrices augue! Tortor porttitor porttitor facilisis. Elit, magna magna sociis, tortor sed cursus in aliquet elit, nec? Adipiscing porta? Porttitor et placerat.</p>', 'Тестовая страница', 'Тестовая страница', 'Тестовая страница', 'Тестовая страница'),
(2, 1, 'Описание фотогалереи', '<p>\r\n	Полное описание фотогалереи</p>', 'Описание фотогалереи', 'Описание фотогалереи', 'Описание фотогалереи', 'Описание фотогалереи');

-- --------------------------------------------------------

--
-- Структура таблицы `poll`
--

DROP TABLE IF EXISTS `poll`;
CREATE TABLE IF NOT EXISTS `poll` (
  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `votes_total` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `poll`
--

INSERT INTO `poll` (`poll_id`, `date_start`, `date_end`, `votes_total`, `status`) VALUES
(2, '2012-02-09 15:19:13', '2012-02-23 22:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices`
--

DROP TABLE IF EXISTS `poll_choices`;
CREATE TABLE IF NOT EXISTS `poll_choices` (
  `choice_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `votes` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`choice_id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `poll_choices`
--

INSERT INTO `poll_choices` (`choice_id`, `poll_id`, `votes`, `sort_order`) VALUES
(33, 2, 2, 0),
(32, 2, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices_translation`
--

DROP TABLE IF EXISTS `poll_choices_translation`;
CREATE TABLE IF NOT EXISTS `poll_choices_translation` (
  `choice_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`choice_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_choices_translation`
--

INSERT INTO `poll_choices_translation` (`choice_id`, `language_id`, `text`) VALUES
(33, 1, 'sss'),
(33, 3, 'sss'),
(32, 1, 'sdsdf'),
(32, 3, 'sdfsdf');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_translation`
--

DROP TABLE IF EXISTS `poll_translation`;
CREATE TABLE IF NOT EXISTS `poll_translation` (
  `poll_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`poll_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_translation`
--

INSERT INTO `poll_translation` (`poll_id`, `language_id`, `text`) VALUES
(2, 1, 'Опрос 1'),
(2, 3, 'Poll 1');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_votes`
--

DROP TABLE IF EXISTS `poll_votes`;
CREATE TABLE IF NOT EXISTS `poll_votes` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `poll_votes`
--

INSERT INTO `poll_votes` (`vote_id`, `poll_id`, `choice_id`, `user_id`, `date`, `host`) VALUES
(7, 2, 33, 1, '2012-03-19 15:01:23', ''),
(6, 2, 33, 0, '2012-02-13 10:09:30', '');

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

DROP TABLE IF EXISTS `price`;
CREATE TABLE IF NOT EXISTS `price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

DROP TABLE IF EXISTS `publication`;
CREATE TABLE IF NOT EXISTS `publication` (
  `publication_id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_group_id` int(11) NOT NULL,
  `publication_category_id` int(11) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `full` varchar(200) DEFAULT NULL,
  `original` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sort_order` int(11) NOT NULL,
  `date_format` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`publication_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `publication`
--

INSERT INTO `publication` (`publication_id`, `publication_group_id`, `publication_category_id`, `ident`, `full`, `original`, `detail`, `preview`, `thumbnail`, `video`, `date_post`, `lastmod`, `sort_order`, `date_format`, `status`) VALUES
(1, 1, 0, 'pervaya-testovaya-novost', 'pervaya-testovaya-novost-1-1-900x700.jpg', '', '', 'pervaya-testovaya-novost-1-1-228x138.jpg', 'pervaya-testovaya-novost-1-1-183x111.jpg', '', '2012-03-19 16:55:15', '2012-03-19 16:59:21', 1, 'd/m/Y', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category`
--

DROP TABLE IF EXISTS `publication_category`;
CREATE TABLE IF NOT EXISTS `publication_category` (
  `publication_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `publication_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `ident` varchar(200) NOT NULL,
  `on_main` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`publication_category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category_translation`
--

DROP TABLE IF EXISTS `publication_category_translation`;
CREATE TABLE IF NOT EXISTS `publication_category_translation` (
  `publication_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_category_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group`
--

DROP TABLE IF EXISTS `publication_group`;
CREATE TABLE IF NOT EXISTS `publication_group` (
  `publication_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`publication_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `publication_group`
--

INSERT INTO `publication_group` (`publication_group_id`, `ident`, `items_per_page`) VALUES
(1, 'news', 10),
(2, 'articles', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group_translation`
--

DROP TABLE IF EXISTS `publication_group_translation`;
CREATE TABLE IF NOT EXISTS `publication_group_translation` (
  `publication_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`publication_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_group_translation`
--

INSERT INTO `publication_group_translation` (`publication_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Новости', 'Модуль предназначен для организации ленты новостей на сайте. \r\nАдминистратор при помощи встроенного в систему визуального редактора имеет возможность без форматировать текст и снабжать его произвольной графикой.'),
(2, 1, 'Статьи', 'Модуль "Статьи" предназначен для ввода, хранения и вывода на сайте различных информационных материалов (статей).\r\nСтатьи могут содержать произвольный текст, картинки, ссылки, таблицы, видео, flash и другие объекты. Для более удобной работы со статьями используется встроенный визуальный редактор, который позволяет с легкостью, как и в MS Word, редактировать содержимое статьи.');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_translation`
--

DROP TABLE IF EXISTS `publication_translation`;
CREATE TABLE IF NOT EXISTS `publication_translation` (
  `publication_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `meta_title` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_translation`
--

INSERT INTO `publication_translation` (`publication_id`, `language_id`, `title`, `body`, `meta_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 'Первая тестовая новость', '<p>\r\n	Scelerisque? Lacus mid rhoncus! Magna. Sed dolor auctor porttitor est ac, ultricies augue enim? Sit, egestas platea magna, lectus. Sagittis scelerisque, pulvinar tincidunt! Scelerisque porta diam natoque mid duis enim. Dis placerat? Adipiscing! Placerat mattis tempor integer duis tempor magna integer nisi magna elementum, tincidunt sociis pulvinar? In, integer dapibus, natoque tincidunt turpis, mauris amet elementum adipiscing eu magnis, phasellus parturient, sed cras velit a montes! Nec, auctor! Cum? Lacus vel mattis nascetur tortor ultricies egestas a habitasse. Ac, sociis urna ultrices! Egestas pellentesque nec adipiscing lundium turpis porttitor magna porttitor ac nisi.</p>\r\n<p>\r\n	Et ac diam ut, pulvinar pulvinar augue a diam ac, phasellus scelerisque porttitor lacus! Eros rhoncus porttitor, lorem elit lundium, massa vut montes nascetur, adipiscing odio. Porttitor in tincidunt vel ac, lundium porta. Pulvinar mid ac tristique enim habitasse eu augue, cras cum hac aliquam nunc pid sociis porta, sed odio cum montes phasellus in? Tortor magna vut nascetur vut etiam, vel ac pulvinar pulvinar? Eu lorem et pulvinar mid! Pid placerat, ac, phasellus!</p>\r\n<p>\r\n	Tristique lundium ut. Turpis dignissim platea mid diam nisi nec. A odio lundium lacus scelerisque tortor sit, sed sociis arcu, etiam! Tincidunt ut! Etiam rhoncus, turpis ridiculus porta urna aliquam lacus lundium, massa mauris! Placerat, enim quis enim rhoncus urna montes est sagittis. Scelerisque elementum dolor enim? Placerat aliquam? Amet risus, turpis porttitor rhoncus est ut nunc mauris integer, proin egestas amet porttitor! Eu auctor rhoncus pulvinar in integer? Montes scelerisque.</p>', 'Первая тестовая новость', 'Первая тестовая новость', 'Первая тестовая новость', 'Первая тестовая новость');

-- --------------------------------------------------------

--
-- Структура таблицы `representation`
--

DROP TABLE IF EXISTS `representation`;
CREATE TABLE IF NOT EXISTS `representation` (
  `representation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `longitude` varchar(128) NOT NULL,
  `latitude` varchar(128) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`representation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `representation_translation`
--

DROP TABLE IF EXISTS `representation_translation`;
CREATE TABLE IF NOT EXISTS `representation_translation` (
  `representation_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `representation_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`representation_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_to` varchar(255) NOT NULL,
  `email_from` varchar(255) NOT NULL,
  `email_reply_to` varchar(255) NOT NULL,
  `watermark` varchar(255) NOT NULL,
  `tracking_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `setting`
--

INSERT INTO `setting` (`setting_id`, `email_to`, `email_from`, `email_reply_to`, `watermark`, `tracking_code`) VALUES
(1, 'office@site.com', 'office@site.com', 'office@site.com', '', 'UA-23496254-5');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize`
--

DROP TABLE IF EXISTS `setting_image_resize`;
CREATE TABLE IF NOT EXISTS `setting_image_resize` (
  `setting_image_resize_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ident` varchar(32) NOT NULL,
  `height_thumbnail` int(11) NOT NULL,
  `width_thumbnail` int(11) NOT NULL,
  `strategy_thumbnail_id` int(11) NOT NULL,
  `height_preview` int(11) NOT NULL,
  `width_preview` int(11) NOT NULL,
  `strategy_preview_id` int(11) NOT NULL,
  `height_detail` int(11) NOT NULL,
  `width_detail` int(11) NOT NULL,
  `strategy_detail_id` int(11) NOT NULL,
  `height_full` int(11) NOT NULL,
  `width_full` int(11) NOT NULL,
  `strategy_full_id` int(11) NOT NULL,
  PRIMARY KEY (`setting_image_resize_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `setting_image_resize`
--

INSERT INTO `setting_image_resize` (`setting_image_resize_id`, `name`, `ident`, `height_thumbnail`, `width_thumbnail`, `strategy_thumbnail_id`, `height_preview`, `width_preview`, `strategy_preview_id`, `height_detail`, `width_detail`, `strategy_detail_id`, `height_full`, `width_full`, `strategy_full_id`) VALUES
(1, 'Каталог / категория', 'catalog_category', 189, 189, 1, 189, 189, 1, 0, 0, 3, 600, 600, 2),
(2, 'Каталог / продукт / основное', 'catalog_product_main', 100, 100, 3, 189, 189, 3, 250, 250, 3, 700, 900, 3),
(3, 'Каталог / продукт / дополнительное', 'catalog_product_additional', 100, 100, 1, 189, 189, 1, 250, 250, 3, 700, 900, 2),
(4, 'Публикации', 'publication', 111, 183, 1, 138, 228, 1, 0, 0, 1, 700, 900, 2),
(5, 'Услуги / категория', 'service_category', 83, 83, 1, 166, 166, 1, 0, 0, 0, 700, 900, 2),
(6, 'Услуги / услуга', 'service_service', 100, 100, 1, 180, 180, 1, 0, 0, 0, 700, 900, 2),
(7, 'Галерея / альбом', 'gallery_album', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(8, 'Галерея / изобрaжение', 'gallery_photo', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(9, 'Слайдер', 'slider', 46, 200, 2, 46, 200, 2, 0, 0, 1, 300, 929, 2),
(10, 'Представители', 'representation', 75, 100, 1, 75, 100, 1, 0, 0, 0, 75, 100, 1),
(11, 'Контакты / схема проезда', 'contact_map', 60, 140, 1, 300, 705, 1, 0, 0, 1, 700, 900, 2),
(12, 'Страницы', 'page', 105, 150, 1, 140, 200, 1, 0, 0, 0, 700, 900, 2),
(13, 'Публикации / Акции', 'publication_actions', 111, 183, 1, 138, 228, 1, 0, 0, 1, 700, 900, 2),
(14, 'Публикации / Новости', 'publication_news', 111, 183, 1, 138, 228, 1, 0, 0, 1, 700, 900, 2),
(16, 'Баннеры', 'banner', 60, 100, 2, 60, 100, 2, 0, 0, 0, 120, 200, 2),
(17, 'Каталог / Производитель', 'catalog_manufacturer', 100, 100, 1, 189, 189, 1, 0, 0, 1, 400, 400, 2),
(18, 'Пункт меню', 'menuItem', 188, 188, 2, 0, 0, 1, 0, 0, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize_strategy`
--

DROP TABLE IF EXISTS `setting_image_resize_strategy`;
CREATE TABLE IF NOT EXISTS `setting_image_resize_strategy` (
  `setting_image_resize_strategy_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `preview_image` varchar(32) NOT NULL,
  PRIMARY KEY (`setting_image_resize_strategy_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `setting_image_resize_strategy`
--

INSERT INTO `setting_image_resize_strategy` (`setting_image_resize_strategy_id`, `ident`, `name`, `description`, `preview_image`) VALUES
(1, 'Crop', 'Обрезка', 'Стратегия для изменения размера изображения таким образом, что ее наименьшим край вписывается в кадр. Остальное обрезается.', ''),
(2, 'Fit', 'Изменения размера', 'Стратегия для изменения размера изображения путем подбора контента в заданных размерах.', ''),
(3, 'FitFill', 'Встраивание', 'Стратегия для изменения размера изображения таким образом, что оно полностью вписывается в кадр. Остальное пространство заливается цветом.', ''),
(4, 'FitStrain', 'Деформация', 'Стратегия для изменения размера без учета пропорций.', '');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_translation`
--

DROP TABLE IF EXISTS `setting_translation`;
CREATE TABLE IF NOT EXISTS `setting_translation` (
  `setting_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_slogan` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  PRIMARY KEY (`setting_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `setting_translation`
--

INSERT INTO `setting_translation` (`setting_id`, `language_id`, `site_name`, `site_slogan`, `meta_description`, `meta_keywords`) VALUES
(1, 1, 'Демо сайт', 'Сайт', 'Демо сайт', 'Демо сайт');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category`
--

DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE IF NOT EXISTS `shop_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `ident` varchar(200) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_translation`
--

DROP TABLE IF EXISTS `shop_category_translation`;
CREATE TABLE IF NOT EXISTS `shop_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency`
--

DROP TABLE IF EXISTS `shop_currency`;
CREATE TABLE IF NOT EXISTS `shop_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(3) NOT NULL DEFAULT '',
  `locale` varchar(255) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `value_non_cash` float(15,8) NOT NULL,
  `status` int(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `shop_currency`
--

INSERT INTO `shop_currency` (`currency_id`, `currency_code`, `locale`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `value_non_cash`, `status`, `sort_order`, `date_modified`) VALUES
(1, 'USD', 'US', '$', '', '0', 1.00000000, 1.00000000, 1, 1, '2011-11-08 11:23:51'),
(2, 'UAH', 'UA', '', 'грн', '0', 8.10000038, 8.50000000, 1, 2, '2011-12-03 09:16:14');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency_translation`
--

DROP TABLE IF EXISTS `shop_currency_translation`;
CREATE TABLE IF NOT EXISTS `shop_currency_translation` (
  `currency_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`currency_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_currency_translation`
--

INSERT INTO `shop_currency_translation` (`currency_id`, `language_id`, `title`) VALUES
(1, 1, 'US Dollar'),
(2, 1, 'Гривна');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers`
--

DROP TABLE IF EXISTS `shop_manufacturers`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers` (
  `manufacturer_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers_translation`
--

DROP TABLE IF EXISTS `shop_manufacturers_translation`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers_translation` (
  `manufacturer_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order`
--

DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE IF NOT EXISTS `shop_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL,
  `telephone` varchar(32) NOT NULL DEFAULT '',
  `mobilephone` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(96) NOT NULL DEFAULT '',
  `address` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `item_count` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `value` decimal(15,8) NOT NULL,
  `total` decimal(15,8) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`order_id`),
  KEY `order_status_id` (`order_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_product`
--

DROP TABLE IF EXISTS `shop_order_product`;
CREATE TABLE IF NOT EXISTS `shop_order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `preorder` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `qty` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_product_id`),
  KEY `product_id` (`product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product`
--

DROP TABLE IF EXISTS `shop_product`;
CREATE TABLE IF NOT EXISTS `shop_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `article` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `new_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discountPercent` smallint(3) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `preorder` tinyint(1) NOT NULL,
  `delivery_period` varchar(255) NOT NULL,
  `newest` tinyint(1) NOT NULL DEFAULT '0',
  `bestseller` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `taxable` enum('Yes','No') NOT NULL,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `album_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image`
--

DROP TABLE IF EXISTS `shop_product_image`;
CREATE TABLE IF NOT EXISTS `shop_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image_translation`
--

DROP TABLE IF EXISTS `shop_product_image_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property`
--

DROP TABLE IF EXISTS `shop_product_property`;
CREATE TABLE IF NOT EXISTS `shop_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property_value`
--

DROP TABLE IF EXISTS `shop_product_property_value`;
CREATE TABLE IF NOT EXISTS `shop_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_translation`
--

DROP TABLE IF EXISTS `shop_product_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`slider_id`, `thumbnail`, `preview`, `full`, `sort_order`, `status`) VALUES
(1, 'slider-image-1-200x46.gif', 'slider-image-1-200x46.gif', 'slider-image-1-929x300.gif', 1, 1),
(2, 'slider-image-2-200x46.gif', 'slider-image-2-200x46.gif', 'slider-image-2-929x300.gif', 2, 1),
(3, 'slider-image-3-200x46.gif', 'slider-image-3-200x46.gif', 'slider-image-3-929x300.gif', 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `slider_group`
--

DROP TABLE IF EXISTS `slider_group`;
CREATE TABLE IF NOT EXISTS `slider_group` (
  `slider_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`slider_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `slider_translation`
--

DROP TABLE IF EXISTS `slider_translation`;
CREATE TABLE IF NOT EXISTS `slider_translation` (
  `slider_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  PRIMARY KEY (`slider_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider_translation`
--

INSERT INTO `slider_translation` (`slider_id`, `language_id`, `url`, `title`, `description`) VALUES
(1, 1, '/', 'Система Управления Сайтом ART-CMF', '<h3><em>Система Управления Сайтом</em></h3>\r\n<h2><em>ART-CMF</em></h2>'),
(2, 1, '/', 'Based on Zend Framework', '<h3><em>Based on</em></h3>\r\n<h2><em>Zend Framework</em></h2>'),
(3, 1, 'test2', 'test2', 'test2');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  `forgot_hash` char(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `role` varchar(100) NOT NULL DEFAULT 'Member',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL,
  `address` varchar(1024) NOT NULL,
  `city` varchar(255) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobilephone` varchar(32) NOT NULL,
  `subscription` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `email_pass` (`email`,`passwd`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `passwd`, `salt`, `forgot_hash`, `created_at`, `role`, `status`, `country_id`, `address`, `city`, `telephone`, `mobilephone`, `subscription`) VALUES
(3, 'Тимур', 'Болотюх', 'timur.bolotyuh@gmail.com', '2e08f67b53ca4de75a8440d9f2cc461e3f52f8c3', '092ea5cc30d93efb3e2858c0c5756672', '', '2012-01-04 15:42:22', 'Admin', 1, 0, '', '', '', '0961404845', 0),
(2, 'Андрей', 'Шайда', 'shayda.andrey@gmail.com', '21a50de6601c8f63b2a3f8db300d2dfdb9c45e2b', '2de7b51589582489e4769d87dd925924', 'dbd27923fdc36df764c3e549e7d002d3', '2011-05-12 02:43:06', 'Admin', 1, 0, '', '', '', '', 0),
(1, 'Andrew', 'Mae', 'amey@i.ua', 'ad85601faf34be559e942ca35873b3a8640c87bd', '4dd065c076f4b049ec0b0018db9b6242', '2727c1328bed4f9daf52504369dece5e', '2011-10-17 15:05:28', 'Admin', 1, 0, 'Каменецкая 69', 'Хмельницкий', '', '+380678607802', 1),
(4, 'Редактор', '', 'redactor@site.com', '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '2011-10-20 11:02:10', 'Editor', 1, 0, '', '', '', '', 0),
(30, 'Владимир', 'Гуцалюк', 'vovasgm@gmail.com', 'f7d81a7006ccdc4d4938c4eaf1e7f8029fdf3a27', '148b2ab9a7e03bdd119ea37ea896f88b', '', '2011-10-20 10:55:09', 'Admin', 1, 0, 'Заречанская 6 кв.14', 'Хмельницкий', '', '0671575912', 0),
(31, 'Владимир', 'Гуцалюк', 'zaz-tank@mail.ru', '627a4f141cefb0543d67e2dd90ebfe24658a3e27', 'e8e6f7afe74e069430f7b0bd0fa0cb32', '', '2011-12-02 11:57:21', 'Moderator', 1, 0, '', 'Хмельницкий', '', '0671575912', 0),
(32, 'Виктор', 'Старанчук', 'vtipok89@gmail.com', 'dde60fac658d9da3de1f5aab26ead1c88e228ef9', 'be82062bb8f4728af413d93a279d1484', '', '2011-11-10 00:00:00', 'Admin', 1, 0, '', '', '', '+380989789064', 0),
(33, 'motochopper', '', 'motochopper@yandex.ru', 'be484aef2f5511cff6e6780e3c02800ab5942acf', '1a7541bd582ee36f479070bc637d0b7e', '', '2011-12-01 15:01:57', 'Moderator', 1, 0, '', 'Хмельницкий', '', '0937183903', 0),
(34, 'dgjfhgf', '', 'ghdsj@djdjd.com.ua', 'a7f84fce27365fc38ad2755dfaf134dd156c5846', '85f6c2351921a20a302a24f63b55c0aa', '', '2011-12-01 15:55:12', 'Member', 1, 0, '', '', '', '0671575912', 0),
(35, 'Andrew', 'jlkjkj', 'amey.pro@gmail.com', 'cb29e25cd6f8ce8dd9acbf2743cca01e73e2153a', 'b97018b380d6f83f513efaf66b476ab3', '', '2011-12-01 15:57:16', 'Member', 1, 0, '', '', '', 'jhgjhgjh', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
