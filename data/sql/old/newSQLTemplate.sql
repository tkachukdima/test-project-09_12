SELECT DISTINCT `shop_product`.*, 
(SELECT COUNT(*) FROM shop_product_review WHERE shop_product_review.product_id = shop_product.product_id AND shop_product_review.status = 1) AS `reviews_count`, 
`shop_product_translation`.*, 
`shop_products_categories`.*, 
`shop_category`.`ident` AS `category_ident`, 
`shop_manufacturers`.`ident` AS `manufacturer_ident`, 
`shop_manufacturers_translation`.`name` AS `manufacturer_name` 
FROM `shop_product` 
LEFT JOIN `shop_product_translation` ON shop_product.product_id = shop_product_translation.product_id 
LEFT JOIN `shop_products_categories` ON shop_products_categories.product_id = shop_product.product_id 
LEFT JOIN `shop_category` ON shop_category.category_id = shop_products_categories.category_id
LEFT JOIN `shop_manufacturers` ON shop_product.manufacturer_id = shop_manufacturers.manufacturer_id
LEFT JOIN `shop_manufacturers_translation` ON shop_manufacturers.manufacturer_id = shop_manufacturers_translation.manufacturer_id 
WHERE (shop_product_translation.language_code = 'ru') 
    AND (shop_products_categories.category_id IN(4, 7, 18, 11, 12, 21, 22, 23, 24, 1)) 
    AND (shop_product.status = 1) 
    AND (shop_product.manufacturer_id IN(5)) 
GROUP BY `shop_product`.`product_id` 
ORDER BY `shop_product_translation`.`name` ASC, 
`shop_products_categories`.`link_type` ASC

SELECT DISTINCT COUNT(*) AS `zend_paginator_row_count`, shop_product.product_id 
FROM `shop_product`
LEFT JOIN `shop_product_translation` ON shop_product.product_id = shop_product_translation.product_id
LEFT JOIN `shop_products_categories` ON shop_products_categories.product_id = shop_product.product_id
LEFT JOIN `shop_category` ON shop_category.category_id = shop_products_categories.category_id
LEFT JOIN `shop_manufacturers` ON shop_product.manufacturer_id = shop_manufacturers.manufacturer_id
LEFT JOIN `shop_manufacturers_translation` ON shop_manufacturers.manufacturer_id = shop_manufacturers_translation.manufacturer_id 
WHERE (shop_product_translation.language_code = 'ru') 
    AND (shop_product.status = 1) 
    AND (shop_product.manufacturer_id IN(5)) 
    AND (shop_product_translation.language_code = 'ru') 
    AND (shop_products_categories.category_id IN(4, 7, 18, 11, 12, 21, 22, 23, 24, 1)) 
GROUP BY `shop_product`.`product_id` 
ORDER BY `shop_product_translation`.`name` ASC, `shop_products_categories`.`link_type` ASC