-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 10 2013 г., 16:46
-- Версия сервера: 5.5.28
-- Версия PHP: 5.4.4-10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `art_art-cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `full` varchar(200) DEFAULT NULL,
  `original` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `event`
--

INSERT INTO `event` (`event_id`, `category_id`, `ident`, `full`, `original`, `detail`, `preview`, `thumbnail`, `sort_order`, `status`) VALUES
(1, 1, 'rusalochka', NULL, '', '', NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `event_category`
--

DROP TABLE IF EXISTS `event_category`;
CREATE TABLE IF NOT EXISTS `event_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `event_category`
--

INSERT INTO `event_category` (`category_id`, `parent_id`, `thumbnail`, `preview`, `full`, `ident`, `sort_order`, `status`, `lastmod`) VALUES
(1, 0, NULL, NULL, NULL, 'kontsertyi', 1, 1, '2013-02-08 14:51:39'),
(2, 1, NULL, NULL, NULL, 'shou', 1, 1, '2013-02-08 16:07:34'),
(3, 0, NULL, NULL, NULL, 'teatryi', 2, 1, '2013-02-08 16:37:54'),
(4, 0, NULL, NULL, NULL, 'sport', 3, 1, '2013-02-08 16:38:22'),
(5, 0, NULL, NULL, NULL, 'detyam', 4, 1, '2013-02-08 16:39:01');

-- --------------------------------------------------------

--
-- Структура таблицы `event_category_translation`
--

DROP TABLE IF EXISTS `event_category_translation`;
CREATE TABLE IF NOT EXISTS `event_category_translation` (
  `category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`category_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `event_category_translation`
--

INSERT INTO `event_category_translation` (`category_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 'ru', 'Концерты', '<p>\r\n	Концерты&nbsp;</p>', 'Концерты', 'Концерты', 'Концерты', 'Концерты'),
(2, 'ru', 'Шоу', '<p>\r\n	Шоу</p>', 'Шоу', 'Шоу', 'Шоу', 'Шоу'),
(3, 'ru', 'Театры', '<p>\r\n	Театры</p>', 'Театры', 'Театры', 'Театры', 'Театры'),
(4, 'ru', 'Спорт', '<p>\r\n	Спорт</p>', 'Спорт', 'Спорт', 'Спорт', 'Спорт'),
(5, 'ru', 'Детям', '<p>\r\n	Детям</p>', 'Детям', 'Детям', 'Детям', 'Детям');

-- --------------------------------------------------------

--
-- Структура таблицы `event_date`
--

DROP TABLE IF EXISTS `event_date`;
CREATE TABLE IF NOT EXISTS `event_date` (
  `event_date_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_date_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `event_date`
--

INSERT INTO `event_date` (`event_date_id`, `event_id`, `place_id`, `date`) VALUES
(1, 1, 1, '2013-03-23 10:00:00'),
(2, 1, 1, '2013-03-24 10:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `event_place`
--

DROP TABLE IF EXISTS `event_place`;
CREATE TABLE IF NOT EXISTS `event_place` (
  `place_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`place_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `category` (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `event_place`
--

INSERT INTO `event_place` (`place_id`, `category_id`, `thumbnail`, `preview`, `full`, `ident`, `sort_order`, `status`, `lastmod`) VALUES
(1, 1, NULL, NULL, NULL, 'olimpiyskiy', 1, 1, '2013-02-08 14:57:08');

-- --------------------------------------------------------

--
-- Структура таблицы `event_place_translation`
--

DROP TABLE IF EXISTS `event_place_translation`;
CREATE TABLE IF NOT EXISTS `event_place_translation` (
  `place_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`place_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `event_place_translation`
--

INSERT INTO `event_place_translation` (`place_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 'ru', 'Олимпийский', '<p>\r\n	В рекордно короткие сроки &ndash; практически за три года был построен спорткомлекс &laquo;ОЛИМПИЙСКИЙ&raquo; - крытый стадион на 35 тысяч зрителей с центральной ареной 10000 кв.м. и плавательный бассейн на 12 тысяч зрителей, который сразу же получил самую высокую оценку спортсменов &ndash; участников Игр XXII Олимпиады в Москве.<br />\r\n	Попадая под своды Крытого Стадиона, поражаешся площади и высоте. А после начала состязаний &ndash; его удобству. Главное &ndash; зритель максимально приближен к арене. Если площадка для проведения соревнований имеет относительно малые размеры (баскетбол, бокс, спортивная или художественная гимнастика, теннис, хоккей с шайбой) то ее окольцовывают блитчерами (передвижными трибунами).<br />\r\n	Специальная звуконепроницаемая перегородка СК Олимпийскиого состоит из 26 панелей и достаточно 10-12 часов, чтобы разделить стадион на два автономных зала. В результате одновременно можно устраивать спортивные программы и цирковые представления, концерты и массовые катания на коньках, выставки, для обслуживания которых имеется пресс-центр площадью 430 кв.м. на 120 посадочных мест, конференц-зал, зал заседаний, пресс-бар. Имеется стоянка на 500 атомобилей и 250 автобусов.<br />\r\n	Быстрая трансформация арены &ndash; отличительная черта стадиона. В течение суток зеленый ковер футбольного поля сменяет самая большая в мире ледовая арена под крышей. (112х72 м.). Кроме Центральной арены, на южном секторе стадиона расположены игровой (42х24 м.) и гимнастический залы, на северном &ndash; тренировочный каток (60х30 м.) и зал художественной гимнастики.<br />\r\n	В плавательном бассейне СК Олимпийского прозрачная акустическая перегородка отделяет демонстрационную ванну (50х25х2,25 м.) с трибунами на 7,5 тысяч зрителей, от прыжковой ванны (33х25х6 м.) с трибунами на 4,5 тысяч зрителей. Тренировочная зона состоит из разминочной ванны (50х25 м.) и четырех залов. Это позволяет проводить в бассейне одновременно соревнования по плаванию и прыжкам в воду, водному поло и синхронному плаванию.</p>', 'Олимпийский', 'Олимпийский', 'Олимпийский', '');

-- --------------------------------------------------------

--
-- Структура таблицы `event_price_type`
--

DROP TABLE IF EXISTS `event_price_type`;
CREATE TABLE IF NOT EXISTS `event_price_type` (
  `price_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `price_fixed` decimal(10,2) DEFAULT NULL,
  `price_from` decimal(10,2) DEFAULT NULL,
  `price_to` decimal(10,2) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`price_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `event_price_type`
--

INSERT INTO `event_price_type` (`price_type_id`, `event_id`, `name`, `price_fixed`, `price_from`, `price_to`, `sort_order`) VALUES
(1, 1, 'Бельэтаж', NULL, 4000.00, NULL, 1),
(2, 1, 'Амфитеатр', NULL, 5000.00, NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `event_translation`
--

DROP TABLE IF EXISTS `event_translation`;
CREATE TABLE IF NOT EXISTS `event_translation` (
  `event_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`event_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `event_translation`
--

INSERT INTO `event_translation` (`event_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 'ru', 'Русалочка', '<p>\r\n	Вас ждут захватывающие приключения любимой героини &ndash; русалочки Ариэль. Вместе с ней вы побываете между двумя прекрасными мирами, в мире морском и на суше. Эта удивительная история ни кого не оставит равнодушным и вы узнаете как коварна зависть и злоба и как велика сила дружбы и любви. Ариэль предстоит пройти не легкий путь к осуществлению свой самой заветной мечты. И только любовь и вера в добро помогут ей преодолеть все уготованные препятствия.<br />\r\n	Мюзикл &laquo;Русалочка&raquo; собрал в себе огромное количество невероятно красивых и неповторимых спецэффектов. Здесь вы увидите головокружительные полеты над зрительным залом, фантастические костюмы и декорации, а уникальное устройство сцены создадут полную иллюзию действия под водой. Прямо на ваших глазах морское дно превратится в дворец принца Эрика и вы в мгновении ока из подводных глубин океана переместитесь на поверхность земли.</p>', 'Русалочка', 'Русалочка', 'Русалочка', 'Русалочка');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
