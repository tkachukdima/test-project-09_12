-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 01 2013 г., 16:50
-- Версия сервера: 5.5.32
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `art_art-cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL,
  `value` text,
  `module` varchar(16) NOT NULL DEFAULT 'default',
  `setting_type` enum('input','textarea','checkbox') DEFAULT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `key` (`key`,`module`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`setting_id`, `key`, `value`, `module`, `setting_type`) VALUES
(1, 'newest_on_main', '5;15', 'catalog', 'input'),
(2, 'hit_on_main', '5;15', 'catalog', 'input'),
(3, 'special_on_main', '5;15', 'catalog', 'input'),
(4, 'site_name', '', 'default', NULL),
(5, 'site_slogan', '', 'default', NULL),
(6, 'meta_description', '', 'default', NULL),
(7, 'meta_keywords', '', 'default', NULL),
(8, 'email_to', 'info@info.com', 'default', NULL),
(9, 'email_from', 'info@info.com', 'default', NULL),
(10, 'reply_to', 'info@info.com', 'default', NULL),
(11, 'watermark', '', 'default', NULL),
(12, 'tracking_code', '', 'default', NULL),
(13, 'payment_privat24_url', '', 'payment', NULL),
(14, 'payment_privat24_merchant', '', 'payment', NULL),
(15, 'payment_privat24_password', '', 'payment', NULL),
(16, 'public_email', 'public@info.com', 'default', NULL),
(17, 'site_name_ru', 'Site name', 'default', NULL),
(18, 'site_slogan_ru', 'Site slogan', 'default', NULL),
(19, 'copyright_ru', 'Copyright 1', 'default', NULL),
(20, 'count_row_album', '2', 'gallery', NULL),
(21, 'count_row_photo', '10', 'gallery', NULL),
(22, 'city_ru', 'Сибирь 12/1', 'default', NULL),
(23, 'address_ru', 'Калифорния 34 офис 5', 'default', NULL),
(24, 'phone_code', '234', 'default', NULL),
(25, 'phone_number', '456-45-34', 'default', NULL),
(26, 'fax_code', '346', 'default', NULL),
(27, 'fax_number', '678-34-23', 'default', NULL),
(28, 'phone2_code', '874', 'default', NULL),
(29, 'phone2_number', '345-23-64', 'default', NULL),
(30, 'skype', 'serdelko', 'default', NULL),
(31, 'work_time_ru', 'с 12 до 13', 'default', NULL),
(32, 'weekend_ru', 'вх, сб', 'default', NULL),
(33, 'count_row_category', '2', 'catalog', NULL),
(34, 'count_row_munufacturer', '3', 'catalog', NULL);
