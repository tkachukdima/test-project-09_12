ALTER TABLE  `publication` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `publication_category` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;


ALTER TABLE  `publication_group` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `storage_images` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `gallery_album` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `gallery_photo` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `menu` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `menu_item` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `page` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `banner` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;


ALTER TABLE  `catalog_product` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `catalog_category` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `catalog_manufacturers` ADD  `author_id` INT( 11 ) NOT NULL ,
ADD  `last_edit_id` INT( 11 ) NOT NULL ;
