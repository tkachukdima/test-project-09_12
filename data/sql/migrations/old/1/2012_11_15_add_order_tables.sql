CREATE TABLE IF NOT EXISTS `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL,
  `telephone` varchar(32) NOT NULL DEFAULT '',
  `mobilephone` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(96) NOT NULL DEFAULT '',
  `address` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `item_count` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `value` decimal(15,8) NOT NULL,
  `total` decimal(15,8) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`order_id`),
  KEY `order_status_id` (`order_status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `preorder` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `qty` int(4) NOT NULL DEFAULT '0',
  `options` text DEFAULT NULL,
  PRIMARY KEY (`order_product_id`),
  KEY `product_id` (`product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` mediumint(8) NOT NULL,
  `order_product_id` mediumint(8) NOT NULL,
  `product_option_id` mediumint(8) NOT NULL,
  `product_option_varint_id` mediumint(8) NOT NULL DEFAULT '0',
  `option_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `value` text COLLATE utf8_bin NOT NULL,
  `option_type` char(1) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

RENAME TABLE  `art_art-cmf`.`shop_product_global_option_links` TO  `art_art-cmf`.`shop_product_option_global_links` ;