
--
-- Структура таблицы `shop_options`
--

CREATE TABLE IF NOT EXISTS `shop_options` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(64) NOT NULL,
  `mandatory` smallint(6) NOT NULL,
  `type` varchar(32) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Структура таблицы `shop_options_translation`
--

CREATE TABLE IF NOT EXISTS `shop_options_translation` (
  `option_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `display_name` text NOT NULL,
  `measure` varchar(16) NOT NULL,
  PRIMARY KEY (`option_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Структура таблицы `shop_option_value`
--

CREATE TABLE IF NOT EXISTS `shop_option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`option_value_id`),
  UNIQUE KEY `option_id` (`option_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_option_value_translation`
--

CREATE TABLE IF NOT EXISTS `shop_option_value_translation` (
  `option_value_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`option_value_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_option`
--

CREATE TABLE IF NOT EXISTS `shop_category_option` (
  `option_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`option_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
