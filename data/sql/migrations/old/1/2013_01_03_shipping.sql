

CREATE TABLE IF NOT EXISTS `shop_payments` (
  `payment_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` smallint(6) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `shop_payments` (`payment_id`, `status`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_payments_translation`
--

CREATE TABLE IF NOT EXISTS `shop_payments_translation` (
  `payment_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_payments_translation`
--

INSERT INTO `shop_payments_translation` (`payment_id`, `language_code`, `name`) VALUES
(1, 'ru', 'Наличными'),
(2, 'ru', 'Privat24');
