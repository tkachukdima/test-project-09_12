-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 21 2012 г., 16:54
-- Версия сервера: 5.5.16
-- Версия PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `art_art-cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `shop_product_review`
--

CREATE TABLE IF NOT EXISTS `shop_product_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_review_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `date_creat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(300) DEFAULT NULL,
  `user_email` varchar(300) DEFAULT NULL,
  `mark` int(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `pros` text NOT NULL,
  `cons` text NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


