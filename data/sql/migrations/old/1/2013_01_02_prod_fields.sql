ALTER TABLE  `shop_product` ADD  `newest_date` TIMESTAMP NULL DEFAULT NULL AFTER  `newest`;
ALTER TABLE  `shop_product` ADD  `hit_date` TIMESTAMP NULL DEFAULT NULL AFTER  `hit`;
ALTER TABLE  `shop_product` ADD  `special_date` TIMESTAMP NULL DEFAULT NULL AFTER  `special`;