ALTER TABLE  `storage_images` ADD  `shortcode` VARCHAR( 1024 ) NOT NULL;

ALTER TABLE  `storage_images` CHANGE  `shortcode`  `shortcode` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE  `storage_images` ADD UNIQUE (`shortcode`)