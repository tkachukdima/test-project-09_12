-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 25 2013 г., 18:45
-- Версия сервера: 5.5.16
-- Версия PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `art_art-cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images`
--

DROP TABLE IF EXISTS `storage_images`;
CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `full` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `main` int(1) NOT NULL,
  `align` int(1) NOT NULL,
  `embeded` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Дамп данных таблицы `storage_images`
--

INSERT INTO `storage_images` (`image_id`, `title`, `path`, `original`, `full`, `detail`, `preview`, `thumbnail`, `updated_at`, `created_at`, `main`, `align`, `embeded`) VALUES
(58, 'restoran5_big.jpg', '/images/storage/2013/06/25/58', 'restoran5_big.jpg', 'restoran5_big-58-500x500.jpg', 'restoran5_big-58-300x300.jpg', 'restoran5_big-58-50x200.jpg', 'restoran5_big-58-100x100.jpg', '2013-06-25 16:17:17', '2013-06-25 16:17:16', 0, 0, 0),
(59, 'restoran_in_odessa.JPG', '/images/storage/2013/06/25/59', 'restoran_in_odessa.JPG', 'restoran_in_odessa-59-500x500.jpg', 'restoran_in_odessa-59-300x300.jpg', 'restoran_in_odessa-59-50x200.jpg', 'restoran_in_odessa-59-100x100.jpg', '2013-06-25 16:17:17', '2013-06-25 16:17:16', 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images_translation`
--

DROP TABLE IF EXISTS `storage_images_translation`;
CREATE TABLE IF NOT EXISTS `storage_images_translation` (
  `image_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`image_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `storage_images_translation`
--

INSERT INTO `storage_images_translation` (`image_id`, `language_code`, `title`) VALUES
(58, 'ru', ''),
(59, 'ru', 'restoran_in_odessa.JPG');
