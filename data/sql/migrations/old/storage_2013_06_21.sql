-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 22 2013 г., 09:26
-- Версия сервера: 5.5.16
-- Версия PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `art_art-cmf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images`
--

DROP TABLE IF EXISTS `storage_images`;
CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `full` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `main` int(1) NOT NULL,
  `align` int(1) NOT NULL,
  `embeded` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `storage_images`
--

INSERT INTO `storage_images` (`image_id`, `title`, `path`, `original`, `full`, `detail`, `preview`, `thumbnail`, `updated_at`, `created_at`, `main`, `align`, `embeded`) VALUES
(24, 'chokolate1.jpg', '/images/storage/2013/06/22', 'chokolate1.jpg', 'chokolate1-500x500.jpg', 'chokolate1-300x300.jpg', 'chokolate1-50x200.jpg', 'chokolate1-100x100.jpg', '2013-06-22 07:22:08', '2013-06-22 07:22:07', 0, 3, 1),
(25, 'chokolate.jpg', '/images/storage/2013/06/22', 'chokolate.jpg', 'chokolate-500x500.jpg', 'chokolate-300x300.jpg', 'chokolate-50x200.jpg', 'chokolate-100x100.jpg', '2013-06-22 07:22:08', '2013-06-22 07:22:06', 0, 2, 1),
(21, 'chokolate3.jpg', '/images/storage/2013/06/22', 'chokolate3.jpg', 'chokolate3-500x500.jpg', 'chokolate3-300x300.jpg', 'chokolate3-50x200.jpg', 'chokolate3-100x100.jpg', '2013-06-22 07:22:07', '2013-06-22 07:22:06', 0, 1, 1),
(22, '390572.jpg', '/images/storage/2013/06/22', '390572.jpg', '390572-500x500.jpg', '390572-300x300.jpg', '390572-50x200.jpg', '390572-100x100.jpg', '2013-06-22 07:22:07', '2013-06-22 07:22:06', 0, 3, 1),
(23, '1352322746_shokolad.jpg', '/images/storage/2013/06/22', '1352322746_shokolad.jpg', '1352322746_shokolad-500x500.jpg', '1352322746_shokolad-300x300.jpg', '1352322746_shokolad-50x200.jpg', '1352322746_shokolad-100x100.jpg', '2013-06-22 07:22:07', '2013-06-22 07:22:06', 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images_translation`
--

DROP TABLE IF EXISTS `storage_images_translation`;
CREATE TABLE IF NOT EXISTS `storage_images_translation` (
  `image_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`image_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `storage_images_translation`
--

INSERT INTO `storage_images_translation` (`image_id`, `language_code`, `title`) VALUES
(25, 'ru', 'Главная1'),
(24, 'ru', 'Контактная информация'),
(23, 'ru', 'Bristot'),
(21, 'ru', 'Careers'),
(22, 'ru', 'Контакты');