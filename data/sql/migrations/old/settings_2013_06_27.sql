ALTER TABLE  `settings` ADD  `value` TEXT NOT NULL;

ALTER TABLE `settings` DROP `name`;

DROP TABLE `settings_translation`;

ALTER TABLE `settings` MODIFY `value` TEXT AFTER `key`;

ALTER TABLE  `settings` CHANGE  `key`  `key` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE  `settings` CHANGE  `module`  `module` VARCHAR( 16 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'default';

ALTER TABLE `settings` DROP INDEX `key`;


ALTER TABLE  `settings` ADD UNIQUE (
`key` ,
`module`
);

