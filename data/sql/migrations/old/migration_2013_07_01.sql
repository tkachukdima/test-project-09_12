
-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` text,
  `module` varchar(255) NOT NULL DEFAULT 'default',
  `setting_type` enum('input','textarea','checkbox') DEFAULT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`setting_id`, `key`, `value`, `module`, `setting_type`) VALUES
(1, 'catalog_newest_on_main', '', 'catalog', 'input'),
(2, 'catalog_hit_on_main', '', 'catalog', 'input'),
(3, 'catalog_special_on_main', '', 'catalog', 'input'),
(4, 'site_name', '', 'default', NULL),
(5, 'site_slogan', '', 'default', NULL),
(6, 'meta_description', '', 'default', NULL),
(7, 'meta_keywords', '', 'default', NULL),
(8, 'email_to', 'info@info.com', 'default', NULL),
(9, 'email_from', 'info@info.com', 'default', NULL),
(10, 'reply_to', 'info@info.com', 'default', NULL),
(11, 'watermark', '', 'default', NULL),
(12, 'tracking_code', '', 'default', NULL),
(13, 'payment_privat24_url', '', 'payment', NULL),
(14, 'payment_privat24_merchant', '', 'payment', NULL),
(15, 'payment_privat24_password', '', 'payment', NULL),
(16, 'public_email', 'public@info.com', 'default', NULL),
(17, 'site_name_ru', 'Site name', 'default', NULL),
(18, 'site_slogan_ru', 'Site slogan', 'default', NULL),
(19, 'copyright_ru', 'Copyright 1', 'default', NULL),
(20, 'count_row_album', '10', 'gallery', NULL),
(21, 'count_row_photo', '10', 'gallery', NULL),
(22, 'city_ru', 'Сибирь 12/1', 'default', NULL),
(23, 'address_ru', 'Калифорния 34 офис 5', 'default', NULL),
(24, 'phone_code', '234', 'default', NULL),
(25, 'phone_number', '456-45-34', 'default', NULL),
(26, 'fax_code', '346', 'default', NULL),
(27, 'fax_number', '678-34-23', 'default', NULL),
(28, 'phone2_code', '874', 'default', NULL),
(29, 'phone2_number', '345-23-64', 'default', NULL),
(30, 'skype', 'serdelko', 'default', NULL),
(31, 'work_time_ru', 'с 12 до 13', 'default', NULL),
(32, 'weekend_ru', 'вх, сб', 'default', NULL);





INSERT INTO `page` (`page_id`, `ident`, `thumbnail`, `preview`, `detail`, `full`, `date_post`, `type`, `status`, `lastmod`) VALUES
(16, 'kontaktyi', NULL, NULL, '', NULL, '2013-07-01 11:47:42', 'contact', 1, '2013-07-01 08:48:10');

INSERT INTO `page_translation` (`page_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(16, 'ru', 'Контакты', '<p>Контакты</p>', 'Контакты', 'Контакты', 'Контакты', 'Контакты');



---------


