ALTER TABLE  `menu` ADD  `parent_id` INT( 11 ) NOT NULL;

DROP TABLE `menu_item_n`, `menu_item_n_translation`, `menu_n`, `menu_n_translation`;

ALTER TABLE `menu` DROP `name`;

DROP TABLE IF EXISTS `menu_translation`;
CREATE TABLE IF NOT EXISTS `menu_translation` (
  `menu_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_translation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `menu_translation`
--

INSERT INTO `menu_translation` (`menu_translation_id`, `menu_id`, `language_code`, `name`, `ident`, `body`, `description`, `keywords`) VALUES
(2, 3, 'ru', 'Нижнее меню1', '', '', '', ''),
(7, 2, 'ru', 'меню2', '', '', '', ''),
(8, 4, 'ru', 'меню4', '', '', '', ''),
(5, 1, 'ru', 'меню1', '', '', '', ''),
(6, 5, 'ru', 'меню5', '', '', '', '');