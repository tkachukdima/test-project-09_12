INSERT INTO  `art_art-cmf`.`setting_image_resize` (
`setting_image_resize_id` ,
`name` ,
`ident` ,
`height_thumbnail` ,
`width_thumbnail` ,
`strategy_thumbnail_id` ,
`height_preview` ,
`width_preview` ,
`strategy_preview_id` ,
`height_detail` ,
`width_detail` ,
`strategy_detail_id` ,
`height_full` ,
`width_full` ,
`strategy_full_id`
)
VALUES (
NULL ,  'Storage',  'storage',  '100',  '100',  '2',  '200',  '200',  '2',  '300',  '300',  '2',  '500',  '500',  '2'
);



DROP TABLE IF EXISTS `storage_images`;
CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `full` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Дамп данных таблицы `storage_images`
--

INSERT INTO `storage_images` (`image_id`, `title`, `path`, `original`, `full`, `detail`, `preview`, `thumbnail`, `updated_at`, `created_at`) VALUES
(43, 'f_sdc11884_20120111214721.jpg', '/images/storage/2013/06/20', 'f_sdc11884_20120111214721.jpg', 'f_sdc11884_20120111214721-500x500.jpg', 'f_sdc11884_20120111214721-300x300.jpg', 'f_sdc11884_20120111214721-200x200.jpg', 'f_sdc11884_20120111214721-100x100.jpg', '2013-06-20 12:27:36', '2013-06-20 12:27:35'),
(42, 'hotel_feride_01.jpg', '/images/storage/2013/06/20', 'hotel_feride_01.jpg', 'hotel_feride_01-500x500.jpg', 'hotel_feride_01-300x300.jpg', 'hotel_feride_01-200x200.jpg', 'hotel_feride_01-100x100.jpg', '2013-06-20 12:27:36', '2013-06-20 12:27:35'),
(41, 'slide167_view_1441499.jpg', '/images/storage/2013/06/20', 'slide167_view_1441499.jpg', 'slide167_view_1441499-500x500.jpg', 'slide167_view_1441499-300x300.jpg', 'slide167_view_1441499-200x200.jpg', 'slide167_view_1441499-100x100.jpg', '2013-06-20 12:27:36', '2013-06-20 12:27:35'),
(39, '4.jpg', '/images/storage/2013/06/20', '4.jpg', '4-500x500.jpg', '4-300x300.jpg', '4-200x200.jpg', '4-100x100.jpg', '2013-06-20 12:26:03', '2013-06-20 12:26:02'),
(40, 'news_detail_480_800_0370a1dc6413255a5eb5e558745d4c20.jpg', '/images/storage/2013/06/20', 'news_detail_480_800_0370a1dc6413255a5eb5e558745d4c20.jpg', 'news_detail_480_800_0370a1dc6413255a5eb5e558745d4c20-500x500.jpg', 'news_detail_480_800_0370a1dc6413255a5eb5e558745d4c20-300x300.jpg', 'news_detail_480_800_0370a1dc6413255a5eb5e558745d4c20-200x200.jpg', 'news_detail_480_800_0370a1dc6413255a5eb5e558745d4c20-100x100.jpg', '2013-06-20 12:27:35', '2013-06-20 12:27:35'),
(37, '01_arbatsky_05.jpg', '/images/storage/2013/06/20', '01_arbatsky_05.jpg', '01_arbatsky_05-500x500.jpg', '01_arbatsky_05-300x300.jpg', '01_arbatsky_05-200x200.jpg', '01_arbatsky_05-100x100.jpg', '2013-06-20 12:26:02', '2013-06-20 12:26:02'),
(38, 'Bier_Platz_7.jpg', '/images/storage/2013/06/20', 'Bier_Platz_7.jpg', 'Bier_Platz_7-500x500.jpg', 'Bier_Platz_7-300x300.jpg', 'Bier_Platz_7-200x200.jpg', 'Bier_Platz_7-100x100.jpg', '2013-06-20 12:26:03', '2013-06-20 12:26:02'),
(36, 'restaurant-gastronomique-nimes.jpg', '/images/storage/2013/06/20', 'restaurant-gastronomique-nimes.jpg', 'restaurant-gastronomique-nimes-500x500.jpg', 'restaurant-gastronomique-nimes-300x300.jpg', 'restaurant-gastronomique-nimes-200x200.jpg', 'restaurant-gastronomique-nimes-100x100.jpg', '2013-06-20 12:23:51', '2013-06-20 12:23:49'),
(33, 'restoran_in_odessa.JPG', '/images/storage/2013/06/20', 'restoran_in_odessa.JPG', 'restoran_in_odessa-500x500.jpg', 'restoran_in_odessa-300x300.jpg', 'restoran_in_odessa-200x200.jpg', 'restoran_in_odessa-100x100.jpg', '2013-06-20 12:23:50', '2013-06-20 12:23:49'),
(34, 'restoran5_big.jpg', '/images/storage/2013/06/20', 'restoran5_big.jpg', 'restoran5_big-500x500.jpg', 'restoran5_big-300x300.jpg', 'restoran5_big-200x200.jpg', 'restoran5_big-100x100.jpg', '2013-06-20 12:23:50', '2013-06-20 12:23:49'),
(35, 'Restoran 1.jpg', '/images/storage/2013/06/20', 'Restoran 1.jpg', 'Restoran 1-500x500.jpg', 'Restoran 1-300x300.jpg', 'Restoran 1-200x200.jpg', 'Restoran 1-100x100.jpg', '2013-06-20 12:23:51', '2013-06-20 12:23:49');



-- 
-- ------------------------------------------------------
-- 


ALTER TABLE  `storage_images` ADD  `main` INT NOT NULL;
ALTER TABLE  `storage_images` CHANGE  `main`  `main` INT( 1 ) NOT NULL;

ALTER TABLE  `storage_images` ADD  `align` INT( 1 ) NOT NULL ,
ADD  `embeded` INT( 1 ) NOT NULL DEFAULT  '1'


-- ------------------------------------------------------

CREATE TABLE IF NOT EXISTS  `storage_images_translation` (
 `image_id` INT( 11 ) NOT NULL ,
 `language_code` VARCHAR( 2 ) NOT NULL ,
 `title` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (  `image_id` ,  `language_code` )
) ENGINE = MYISAM DEFAULT CHARSET = utf8