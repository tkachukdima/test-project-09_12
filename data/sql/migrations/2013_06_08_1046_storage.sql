

DROP TABLE IF EXISTS `storage_images`;
CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `full` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `main` int(1) NOT NULL,
  `align` int(1) NOT NULL,
  `embeded` int(1) NOT NULL DEFAULT '0',
  `shortcode` varchar(200) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Дамп данных таблицы `storage_images`
--

INSERT INTO `storage_images` (`image_id`, `title`, `path`, `original`, `full`, `detail`, `preview`, `thumbnail`, `updated_at`, `created_at`, `main`, `align`, `embeded`, `shortcode`) VALUES
(58, 'restoran5_big.jpg', '/images/storage/2013/06/25/58', 'restoran5_big.jpg', 'restoran5_big-58-500x500.jpg', 'restoran5_big-58-300x300.jpg', 'restoran5_big-58-50x200.jpg', 'restoran5_big-58-100x100.jpg', '2013-06-25 16:17:17', '2013-06-25 16:17:16', 1, 0, 1, 'Shortcode'),
(59, 'restoran_in_odessa.JPG', '/images/storage/2013/06/25/59', 'restoran_in_odessa.JPG', 'restoran_in_odessa-59-500x500.jpg', 'restoran_in_odessa-59-300x300.jpg', 'restoran_in_odessa-59-50x200.jpg', 'restoran_in_odessa-59-100x100.jpg', '2013-06-25 16:17:17', '2013-06-25 16:17:16', 0, 0, 1, 'Shortcode');

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images_relations`
--

DROP TABLE IF EXISTS `storage_images_relations`;
CREATE TABLE IF NOT EXISTS `storage_images_relations` (
  `image_id` int(11) NOT NULL,
  `key` varchar(16) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`image_id`,`key`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `storage_images_relations`
--

INSERT INTO `storage_images_relations` (`image_id`, `key`, `id`) VALUES
(58, 'publication', 32),
(59, 'publication', 32);

-- --------------------------------------------------------

--
-- Структура таблицы `storage_images_translation`
--

DROP TABLE IF EXISTS `storage_images_translation`;
CREATE TABLE IF NOT EXISTS `storage_images_translation` (
  `image_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`image_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `storage_images_translation`
--

INSERT INTO `storage_images_translation` (`image_id`, `language_code`, `title`) VALUES
(58, 'ru', 'wer 1'),
(59, 'ru', 'restoran_in_odessa.JPG');
