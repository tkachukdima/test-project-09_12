CREATE TABLE IF NOT EXISTS `user_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(32) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `user_group` (`group_id`, `color`) VALUES
(1, '#eeeeee'),
(2, '#dddddd');


CREATE TABLE IF NOT EXISTS `user_group_translation` (
  `group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`group_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


INSERT INTO `user_group_translation` (`group_id`, `language_code`, `name`, `description`) VALUES
(1, 'ru', 'Группа 1', 'Описание группы 1'),
(2, 'ru', 'Группа 2', 'Описание группы 2');
