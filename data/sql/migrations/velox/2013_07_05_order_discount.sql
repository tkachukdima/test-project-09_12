CREATE TABLE IF NOT EXISTS `catalog_discount` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_name` varchar(255) DEFAULT NULL,
  `sum_from` decimal(10,2) DEFAULT '0.00',
  `sum_to` decimal(10,2) DEFAULT '0.00',
  `discount` decimal(4,2) DEFAULT '0.00',
  PRIMARY KEY (`discount_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

INSERT INTO `catalog_discount` (`discount_id`, `discount_name`, `sum_from`, `sum_to`, `discount`) VALUES
(1, 'От 100 до 1000', 100.00, 999.00, 1.00),
(2, 'От 1000 до 2000', 1000.00, 1999.00, 2.00),
(3, 'От 2000 до 5000', 2000.00, 4999.00, 3.00),
(4, 'От 5000 до 10000', 5000.00, 9999.00, 5.00),
(5, 'От 10 000 до 25 000', 10000.00, 24999.00, 7.00),
(6, 'От 25 000 до 40 000', 25000.00, 39000.00, 9.00),
(7, 'От 40 000', 40000.00, NULL, 10.00);


ALTER TABLE  `catalog_order` ADD  `sub_total` DECIMAL( 15, 2 ) NOT NULL AFTER  `value` ,
ADD  `discount` DECIMAL( 4, 2 ) NULL DEFAULT  '0' AFTER  `sub_total`;

UPDATE `catalog_order` SET `sub_total`=`total`;