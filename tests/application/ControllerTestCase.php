<?php
require_once 'Zend/Application.php';
require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';
 
/**
 * Controller Test case
 *
 * @category Tests
 */

abstract class ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{
    /**
     * @var Zend_Application
     */
    protected $_application;
 
    public function setUp()
    {
        
         $this->bootstrap = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini'
        );

        parent::setUp();

        $bootstrap = $this->bootstrap->getBootstrap();

        $this->getFrontController()->setParam('bootstrap', $bootstrap);
                       
    }
    
    /**
     * Init Application
     */
    static public function appInit()
    {
        $application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini'
        );

        $application->bootstrap();

    }
    
     /**
     * Shut down Application
     */
    static public function appDown()
    {
       
        try {
            Zend_Db_Table_Abstract::getDefaultAdapter();
        } catch (Exception $e) {

        }
    }
    
    /**
     * debug
     *
     * @return string
     */
    public function debug()
    {
        if ($this->getResponse()->isException()) {
            echo "Exceptions: \n";
            print_r($this->getResponse()->getException());
        }
    }
    
    /**
     * Change environment for user role/status
     * Should be run after setUp() !!!
     *
     * @param string $role
     * @param string $status
     * @return void
     */
    protected static function _doLogin($role = 'Member', $status = 1)
    {
        $authService = new User_Service_Authentication();
        
        $auth = $authService->getAuth();       
        $user = self::_generateFakeIdentity($role, $status);
        $auth->getStorage()->write($user);
                
    }
    
     /**
     * Create user
     *
     * @param string $role
     * @param string $status
     * @return StdClass an identity
     */
    protected static function _generateFakeIdentity($role = 'Member', $status = 1)
    {
        $account = new stdClass();
              
        $account->user_id = 10;
        $account->firstname = 'Auto';       
        $account->lastname = 'Test';
        $account->email = 'autotest' . time() . '@example.org';
        $account->forgot_hash = 'JKD4F&5DFHUI6';
        $account->created_at = '2011-11-21 16:22:22';
        $account->role = $role;
        $account->status = $status;
        $account->country_id = 1;
        $account->address = '1st Street';
        $account->city = 'Hamburg';
        $account->telephone = '+3805684833';
        $account->mobilephone = '';
        $account->subscription = 1;
        
        return $account;
    }
    
     protected static function _setUpLanguages()
    {

        $lang = 'ru';
        $curentLang = '';
        $locale = '';
        $langModel = new Default_Model_Language();
        $langList = $langModel->getLanguages();
        
        Zend_Registry::set('langList', $langList);
      
        foreach ($langList as $langItem) {
            if ($lang == $langItem->code) {
                $curentLang = $langItem->code;
                $locale = $langItem->locale;
            }
        }

        if ('' == $curentLang)
            $lang = $langList[0]->code;

        if ('' == $locale)
            $locale = $langList[0]->locale;

                
        Zend_Registry::set('Current_Lang', $lang);

        $zl = new Zend_Locale();
        $zl->setLocale($locale);
        Zend_Registry::set('Zend_Locale', $zl);
        
        $params = array(
            'scan' => Zend_Translate_Adapter::LOCALE_DIRECTORY,
            'logUntranslated' => true
        );

        $translate = new Zend_Translate(
                'gettext',
                APPLICATION_PATH . '/languages/' . $lang . '.mo',
                $lang,
                $params);
        Zend_Registry::set('Zend_Translate', $translate);

        Zend_Validate_Abstract::setDefaultTranslator($translate);
        Zend_Form::setDefaultTranslator($translate);
        Zend_Controller_Router_Route::setDefaultTranslator($translate);

     
       

    }

    /**
     * Remove environment
     *
     */
    protected function tearDown()
    {
        $dbAdapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbAdapter->closeConnection();

        parent::tearDown();
    }
 
}