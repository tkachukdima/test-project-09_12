<?php

/**
 * Catalog_Model_CartTest
 *
 * @category Tests
 * @package  Catalog
 */
class Catalog_Model_CartTest extends ControllerTestCase {

    protected $user;
    protected $object;

    public function setUp() {
        parent::setUp();
        parent::_setUpLanguages();

        $this->object = new Catalog_Model_Cart();
    }

    public function testCanCreateTestObject() {
        $this->assertNotNull($this->object);
    }

    /**
     * @dataProvider itemsProvider
     */
    public function testCalculateItemId($product_id, $product_options) {
        $this->object->calculateItemId($product_id, $product_options);
    }

    public function itemsProvider() {
        return array(
            array(1, null),
            array(2, null),
            array(3, null),
            array(4, null),
            array(5, null),
            array(6, null),
            array(7, null),
        );
    }

}

