<?php

/**
 * Guestbook_Model_GuestbookTest
 *
 * @category Tests
 * @package  Default
 */
class Guestbook_Model_GuestbookTest extends ControllerTestCase
{

    protected $user;
    protected $object;
    protected $testPageId;

    public function setUp()
    {
        parent::setUp();
        parent::_doLogin('Admin');

        $auth = Zend_Auth::getInstance();
        $this->user = $auth->getIdentity();

        $this->object = new Guestbook_Model_Guestbook();
    }

    public function testCanCreateTestObject()
    {
        $this->assertNotNull($this->object);
    }


}

