<?php
/**
 * Default_Model_PageTest
 *
 * @category Tests
 * @package  Default
 */
class Default_Model_PageTest extends ControllerTestCase
{

    protected $user;
    protected $object;    
    protected $testPageId;
     
    public function setUp()
    {                       
        parent::setUp(); 
        parent::_doLogin('Admin');
        
        $auth = Zend_Auth::getInstance();
        $this->user =  $auth->getIdentity();
        
        $this->object = new Default_Model_Page();   

    }


    public function testCanCreateTestObject()
    {
        $this->assertNotNull($this->object);
    }
    
//    public function testCanCreatePage()
//    {
//        $data = $this->getTestPageData();
//        $pageId = $this->object->savePage($data);
//        
//        $page = $this->object->getPageByIdForEdit($pageId); 
//
//      
//        $this->assertEquals($data['title_ru'], $page->title_ru);
//        $this->assertEquals($data['body_ru'], $page->body_ru);
//        $this->assertEquals($data['status'], $page->status);
//        $this->assertEquals($data['type'], $page->type);
//        $this->assertEquals($data['ident'], $page->ident);
//        $this->assertEquals($data['page_title_ru'], $page->page_title_ru);
//        $this->assertEquals($data['meta_description_ru'], $page->meta_description_ru);
//        $this->assertEquals($data['meta_keywords_ru'], $page->meta_keywords_ru);
//        $this->assertEquals($data['date_post'], $page->date_post);
//    }
//
//    /**
//     * @return int page_id
//     * @todo дописать функцию
//     */
//    private function getTestPageData()
//    {
//       $time = time();
//        return array(
//            'title_ru' => 'Title of test page',
//            'body_ru' => 'Body of test page. Body of test page. 
//                                          Body of test page. Body of test page. 
//                                          Body of test page',
//            'delete_image' => 0,
//            'full' => '',
//            'status' => 1,
//            'type' => 'static',
//            'ident' => 'test-page' .  $time,
//            'page_title_ru' => 'Page Title of test page',
//            'meta_description_ru' => 'Meta description of test page. 
//                                          Meta description of test page',
//            'meta_keywords_ru' => 'Meta, keywords,  test, page',
//            'date_post' => date('Y-m-d H:i:s', $time)
//        );
//    }
//    
     
    
    
}

