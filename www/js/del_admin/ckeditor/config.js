/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
    // Define changes to default configuration here. For example:
    config.language = 'ru';
     
    config.toolbar = 'Custon'; 
    config.toolbar_Custon =
    [  
    ['Undo','Redo'],
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['NumberedList','BulletedList','-','Outdent','Indent'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
    ['Link','Unlink','Anchor'],
    '/',
    ['Format','Font','FontSize'],
    ['SelectAll','RemoveFormat'], 
    ['Image','Flash','Table','HorizontalRule','Smiley'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks'],    
    ['Source']
    ];
 
    config.filebrowserBrowseUrl = '/js/admin/elfinder/elfinder.html';
};

