var AC = {
    langList : []
};


AC.Features = {

    switchFeatureType : function(feature_type_input) {
        var feature_type = $(feature_type_input).val();
                       
        switch(feature_type)
        {
            case 'M':
            case 'S':
            case 'N':
                $('#variants-tab-button').show();
                break;
            case 'C':
            case 'T':
            case 'O':
            case 'D':
                $('#variants-tab-button').hide();
                alert('Текущие варианты будут удалены');
                break;
            default:
                alert('hide variants tab');
        }
    },
        
    deleteCategory: function(link){
           
        $.ajax({
            url: $(link).attr('href'),  
            dataType : "json",                     
            success: function (data) { 
                if(data.success){
                    link.closest('tr').remove();
                } else {
                    alert(data.message);
                }
            } 

        });
        return false;
    },
    
    markVariantAsDeleted: function(link, index){
        $(link).after('<input type="hidden" name="variants['+index+'][deleted]" value="true">');    
        $(link).closest('tr').hide();
        return false;
    },
    
    addNewVariant: function(){
        var new_index = $('#feature-categories-list tr').length;
        $('.feature-add-variant').hide();
        var row =  
        '<tr>' +
            '<td>' +
                '<input type="text" name="variants[' + new_index + '][position]" value="" size="4" class="input-text-short">' +
            '</td>' +
            '<td class=col-first">';
            
            for(var i=0; i<AC.langList.length; i++){
                row += '<input type="text" name="variants[' + new_index + '][variant]['+AC.langList[i]+']" value="" class="input-text-large cm-feature-value input-text-selected">';
            }
            
            row += '</td>'+
            '<td class="row-nav"> '+
                '<a class="table-add-link feature-add-variant" onclick="return AC.Features.addNewVariant();"  href="#">Add</a>'+
                '<a class="feature-delete-variant"  onclick="return AC.Features.markVariantAsDeleted(this, ' + new_index + ');" href="#">Delete</a>'+
            '</td>'+
        '</tr>';
        $('#feature-categories-list').append(row);
        return false;
    }

};