
function commentActivate(id, uri, status)
{
    jQuery.ajax({
        type: 'POST',
        url: uri,
        data: {'comment_id' : id, 'status': status},
        dataType: 'json',
        beforeSend: function(xhr) { },
        success: function(data, status)
        {
            
            if ('error' == data.status) {
                var message = 'Статус комментария не изменен!' + "<br/>";
                alert(message);
            } 
            else if ('success' == data.status) 
            {
                jQuery('tr#comment_'+id).remove();           
            }             
        },
        complete: function(xhr, textStatus) {return false;}
    });
    
    return false;
}

function commentDelete(id, uri)
{
    jQuery.ajax({
        type: 'POST',
        url: uri,
        data: {'comment_id' : id},
        dataType: 'json',
        beforeSend: function(xhr) { },
        success: function(data, status)
        {
            
            if ('error' == data.status) {
                var message = 'Не удалось удалить комментарий!' + "<br/>";
                alert(message);
            } 
            else if ('success' == data.status) 
            {
                jQuery('tr#comment_'+id).remove();            }             
        },
        complete: function(xhr, textStatus) {return false;}
    });
    
    return false;
}

