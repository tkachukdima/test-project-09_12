$(document).ready(function() {

/*------------simple menu-------------*/						   
	$("#menu_top li").hover(function(){
		$(this).addClass('hover');
		$(this).find("ul:first").fadeIn("fast");
	},function(){
		$(this).removeClass('hover');
		$(this).find("ul:first").fadeOut("fast");
	});

	$("div.text").append("<div class='clear'></div>");
	$('.text img').each(function(){
			var img_al = $(this).css('float');
			if ( img_al == 'left' ) { $(this).wrap('<div class="img fll"></div>'); }
				else {
					if ( img_al == 'right' ) { $(this).wrap('<div class="img flr"></div>'); }
						else {
							if ( $(this).attr('align') == 'left' ) { $(this).wrap('<div class="img fll"></div>'); }
								else {$(this).wrap('<div class="img flr"></div>');}							
							}					
				}
			$(this).after('<span class="shadow"></span>');
		});	
	
	$('#tabs').tabs();
	
	$('select').selectBox();
	
	animate1 = function( xtag, xsh, xduration ) {
		xtag.each(function() {
		var xpos = parseInt( $(this).css('left') );
		$(this).animate({left:xpos+xsh},xduration,'easeInSine');
		$(this).animate({left:xpos-xsh},2*xduration,'easeInOutSine');
		$(this).animate({left:xpos},xduration,'easeOutSine',function(){animate1(xtag, xsh, xduration);});
		});
	};	
	
	setTimeout(function() {
			animate1($('.slider_bg'),500,50000);
		}, 700);	
	
	$('#slider').rhinoslider({
		effect: 'fade',
		showTime: 5000,
		controlsMousewheel: false,
		controlsPlayPause: false,
		autoPlay: true,
		pauseOnHover: false,
		showBullets: 'never',
		showControls: 'always',
		callBeforeNext:function(){
				return false;
			},
		callBackNext:function(){
				return false;					
			},
		callBeforePrev:function(){
				return false;
			},
		callBackPrev:function(){
				return false;			
			}
	});	
	
	
	$('#slider li .llink').hover(function(){
		$(this).children('.txt').stop(true,true).animate({right:134,opacity:1},150,'swing',function(){});
	},function(){
		$(this).children('.txt').stop(true,true).animate({right:0,opacity:0},150,'swing',function(){});
	});
	
	$('#slider li .rlink').hover(function(){
		$(this).children('.txt').stop(true,true).animate({left:131,opacity:1},150,'swing',function(){});
	},function(){
		$(this).children('.txt').stop(true,true).animate({left:0,opacity:0},150,'swing',function(){});
	});
	

/*-------simple modal--------------*/
	$('#w_btn1').click(function(){
		$("#window1").modal({
			overlayClose:true,
			closeClass:'modalClose',
			onOpen: function (dialog) {dialog.overlay.fadeIn('fast', function () {dialog.data.hide();dialog.container.fadeIn('fast', function () {dialog.data.fadeIn('fast');});});},
			onClose: function (dialog) {dialog.data.fadeOut('fast', function () {dialog.container.hide('fast', function () {dialog.overlay.fadeOut('fast', function () {$.modal.close();});});})}
		});	
		return false;
    });

});
