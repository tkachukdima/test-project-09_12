(function( $ ) {

    /*** create combobox autocomplite ***/
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.data( "ui-autocomplete" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
    /*** End ***/

  })( jQuery );


/*** add input button disabled ***/
function disabled_input(selector){
      $(selector).prop('readOnly', true).after($("<input>").attr({type:"button", value:"✖", 'class':"disable-input"}));
      $('.disable-input').click(function(){
          var $input = $(this).prev();
          if ($input.is('[readonly]') == false){
              $input.prop('readOnly', true);
              $(this).attr({'value':"✖"});
          }else{
              $input.prop('readOnly', false);
              $(this).attr({'value':"✔"});
          }
      });
}
/*** End ***/

$(document).bind({
    ajaxStart: function() { $(".vY").css({visibility:"visible"});  $('#loader').show(); },
    ajaxStop: function() {$(".vY").css({visibility:"hidden"});$('#loader').hide(); }
});


$(document).ready(function($) {
    
    $("body").append('<div class="vY">'+
                        '<div class="vX UC">' +
                            '<div class="J-J5-Ji">' +
                                '<div class="vh" id="loader">' +
                                    
                                '</div>' +
                            '</div>' +
                        '</div>' +
                     '</div>');
    
    $("#loader").append('<div id="fountainG">' +
        '<div id="fountainG_1" class="fountainG"></div>' +
        '<div id="fountainG_2" class="fountainG"></div>' +
        '<div id="fountainG_3" class="fountainG"></div>' +
        '<div id="fountainG_4" class="fountainG"></div>' +
        '<div id="fountainG_5" class="fountainG"></div>' +
        '<div id="fountainG_6" class="fountainG"></div>' +
        '<div id="fountainG_7" class="fountainG"></div>' +
        '<div id="fountainG_8" class="fountainG"></div>' +
    '</div>');
    
    
    $("body").append($("<div>", {id:"loading_animation"}).html("Loading ..."));
    $("#loading_animation").hide();
    
  /***  create Lang Tabs ***/
  $( "#tabs" ).tabs();
  $( "#tabs_main" ).tabs();
  /*** End ***/
});


function checkSubmit(e)
{
    if(e && e.keyCode == 13)
    {
        serchSubmit();
    }
}

function serchSubmit()
{
    if (jQuery('input#price').val() == 'Поиск по цене...') 
    {
        jQuery('input#price').val('')
        }
    if (jQuery('input#name').val() == 'Поиск по названию...') 
    {
        jQuery('input#name').val('')
        }
    if (jQuery('input#article').val() == 'Поиск по артикулу...') 
    {
        jQuery('input#article').val('')
        }
          
    document.forms[0].submit();
    return true;    
}

$(function() {
    
    
    jQuery('h4.white').corner("top");
    jQuery('h4.yellow').corner("top");
    jQuery('div.box-container').corner("bottom");
    
    
    jQuery.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '<Пред',
        nextText: 'След>',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        changeMonth: true,                
        changeYear: true,
        showAnim: 'fold'
    };
    jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);

    jQuery.timepicker.regional['ru'] = {
        timeOnlyTitle: 'Выберите время',
        timeText: 'Время',
        hourText: 'Часы',
        minuteText: 'Минуты',
        secondText: 'Секунды',
        currentText: 'Теперь',
        closeText: 'Закрыть',
        ampm: false,
        timeFormat: 'hh:mm:ss'
    };
    jQuery.timepicker.setDefaults(jQuery.timepicker.regional['ru']);
    jQuery("#date_post").datetimepicker({});
    jQuery("#date_start").datetimepicker({});
    jQuery("#date_end").datetimepicker({});
    jQuery("#date").datetimepicker({});
    jQuery("#subscription_date_end").datetimepicker({});
    
    jQuery(".date_input").datetimepicker({});
    
    
    updateClock(); 
    setInterval('updateClock()', 1000 );
    
});

function updateClock()
{
    var currentTime = new Date();
    var currentHours = currentTime.getHours();
    var currentMinutes = currentTime.getMinutes();
    var currentSeconds = currentTime.getSeconds();
    currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
    currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;   
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds;
    document.getElementById("today_clock").firstChild.nodeValue = currentTimeString;
}

/*
$(function() {
    $('a.lightbox').lightBox({
        imageLoading: '/layout/lightbox/loading.gif',
        imageBtnClose: '/layout/lightbox/close.gif',
        imageBtnPrev: '/layout/lightbox/prev.gif',
        imageBtnNext: '/layout/lightbox/next.gif'
    });
});*/

