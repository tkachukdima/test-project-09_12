/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'en';
	// config.uiColor = '#AADC6E';




config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'undo', 'document', 'doctools' ]},
		{ name: 'clipboard' },
		{ name: 'insert' },
		'/',
		//{ name: 'forms' },
		{ name: 'links' },
		{ name: 'others' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'align'},
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks' ] },
		'/',
		{ name: 'styles' },
		{ name: 'colors' },
		//{ name: 'about' },
		{ name: 'editing',     groups: [ 'find', 'selection', /*'spellchecker'*/ ] },
		{ name: 'tools' }
		
	];


	config.removeButtons = 'Preview,NewPage';


	config.filebrowserWindowFeatures = 'resizable=yes,scrollbars=yes';
	config.filebrowserWindowHeight = 400;
	config.filebrowserWindowWidth = 910;
	config.filebrowserBrowseUrl = "/system/js/elfinder/elfinder.html";


	//config.contentsCss = '/contents.css';
};
